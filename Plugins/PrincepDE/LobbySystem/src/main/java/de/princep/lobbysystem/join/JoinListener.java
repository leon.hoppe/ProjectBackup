package de.princep.lobbysystem.join;

import de.princep.lobbysystem.LobbySystem;
import de.princep.lobbysystem.annotations.GetAPI;
import de.princep.lobbysystem.apis.CoinAPI;
import de.princep.lobbysystem.gui.PlayerInvGUI;
import de.princep.lobbysystem.utils.Title;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {
    @GetAPI
    public CoinAPI coinAPI;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        coinAPI.loadUser(event.getPlayer().getUniqueId());
        System.out.println(coinAPI.getCoins(event.getPlayer().getUniqueId()));


        Player player = event.getPlayer();

        //Msg
        event.setJoinMessage(null);

        //Gamemode
        player.setGameMode(GameMode.SURVIVAL);

        //XP
        player.setLevel(2021);

        //Inv
        PlayerInvGUI.addPlayerInv(player);

        //spawn loc
        Location l = null;
        try {
            l = LobbySystem.locationManager.getLocation();
        } catch (Exception ignored) { }
        if (!(l == null)) {
            player.teleport(LobbySystem.locationManager.getLocation());
            player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 2);
        } else {
            player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);
            player.sendMessage("§8»");
            player.sendMessage(Title.prefix + "§7Der Spawn wurde noch nicht gesetzt.");
            player.sendMessage(Title.prefix + "§7Bitte wende dich an einen Serveradministrator.");
            player.sendMessage("§8»");
        }
    }
}
