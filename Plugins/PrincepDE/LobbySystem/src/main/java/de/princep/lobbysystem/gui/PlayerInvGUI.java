package de.princep.lobbysystem.gui;

import de.princep.lobbysystem.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import java.util.Objects;

public class PlayerInvGUI implements Listener {

    public static void addPlayerInv(Player player) {
        player.getInventory().clear();
        Inventory inventory = player.getInventory();

        inventory.setItem(0, new ItemBuilder(Material.CHEST).setName("§6Inventar").build());
        inventory.setItem(1, new ItemBuilder(Material.NETHER_STAR).setName("§eShop").build());
        inventory.setItem(4, new ItemBuilder(Material.COMPASS).setName("§c§lNavigator").build());

        //inventory.setItem(7, Hider.getState(player).getItem());

        inventory.setItem(8, new ItemBuilder(player).setName("§b§l" + player.getName()).build());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Player player = event.getPlayer();
            //if (Main.buildMode.contains(player)) return;
            event.setCancelled(true);

            if (event.getItem() == null) return;
            switch (event.getItem().getType()) {
                case CHEST:
                    //InventoryGUI.openInventory(player);
                    break;

                case NETHER_STAR:
                    //ShopGUI
                    break;

                case COMPASS:
                    //NavigationGUI.openNav(player);
                    //TempNavGUI.openTemNav(player);
                    break;

                case SKULL_ITEM:
                    //FRIENDGUI
                    break;
            }
            /*if (event.getItem().getType() == Hider.getState(player).getItem().getType()) {
                if (!Hider.checkCooldown(player)) {
                    //Send message
                    Title.sendActionBar(player, Title.prefix + "§cRUHIG BRUDER! §7wir haben ja Zeit");
                    player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    return;
                }

                int id = Hider.getState(player).getID() + 1;
                if (id > Hider.State.values().length - 1) id = 0;
                Hider.setState(player, Objects.requireNonNull(Hider.State.getByID(id)));
                player.getInventory().setItem(7, Objects.requireNonNull(Hider.State.getByID(id)).getItem());
                Objects.requireNonNull(Hider.State.getByID(id)).manageHiding(player);
                Hider.updateCooldown(player);
            }*/
        }
    }

}
