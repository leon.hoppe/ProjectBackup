package de.princep.lobbysystem;

import de.princep.lobbysystem.annotations.AnnotationManager;
import de.princep.lobbysystem.apis.HiderAPI;
import de.princep.lobbysystem.apis.MySQL;
import de.princep.lobbysystem.apis.CoinAPI;
import de.princep.lobbysystem.apis.WingAPI;
import de.princep.lobbysystem.join.JoinListener;
import de.princep.lobbysystem.utils.LocationManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;

public class LobbySystem extends JavaPlugin {

    public static LocationManager locationManager;

    //Config
    public static File file;
    public static FileConfiguration cfg;
    
    @Override
    public void onEnable() {
        locationManager = new LocationManager();

        //Config
        saveDefaultConfig();
        LobbySystem.file = new File("plugins/Hub", "config.yml");
        LobbySystem.cfg = YamlConfiguration.loadConfiguration(LobbySystem.file);
        
        AnnotationManager am = new AnnotationManager();
        am.addApiObject(new CoinAPI());
        am.addApiObject(new MySQL("localhost", 3306, "princep", "princep", "Fs427SX9TrAWFFx"));
        am.addApiObject(new HiderAPI());
        am.addApiObject(new WingAPI());

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(am.addObject(new JoinListener()), this);

        am.initialiseFields();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
