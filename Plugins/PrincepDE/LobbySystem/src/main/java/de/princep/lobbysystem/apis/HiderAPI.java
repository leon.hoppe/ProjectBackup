package de.princep.lobbysystem.apis;

import de.princep.lobbysystem.annotations.AnnotationAPI;
import de.princep.lobbysystem.annotations.GetAPI;
import de.princep.lobbysystem.utils.Promissions;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;
import java.util.HashMap;

public class HiderAPI implements AnnotationAPI {
    @GetAPI
    public MySQL sql;

    @Override
    public void initialise() {
        sql.insert("CREATE TABLE IF NOT EXISTS Hider (UUID VARCHAR(100), ID INT(10))");
    }

    private interface Executable { void execute(Player p, Player all); }

    public enum State {
        HIDE_ALL(0, Material.SULPHUR, "§cKein Spieler sichtbar §7(Rechtsklick)", Player::hidePlayer),
        ONLY_VIP(1, Material.REDSTONE, "§5Nur VIPs §7(Rechtsklick)", (p, all) -> { if (all.hasPermission(Promissions.VIP)) p.showPlayer(all); else p.hidePlayer(all); }),
        SHOW_ALL(2, Material.GLOWSTONE_DUST, "§aAlle Spieler sichtbar §7(Rechtsklick)", Player::showPlayer);

        private final int id;
        private final Material material;
        private final String name;
        private final Executable exe;

        State(int id, Material material, String name, Executable exe) {
            this.material = material;
            this.name = name;
            this.id = id;
            this.exe = exe;
        }

        public int getID() { return id; }
        public ItemStack getItem() {
            ItemStack item = new ItemStack(material);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(name);
            item.setItemMeta(meta);
            return item;
        }
        public void manageHiding(Player p) { for (Player all : Bukkit.getOnlinePlayers()) exe.execute(p, all); }

        public static State getByID(int id) {
            for (State state : values())
                if (state.getID() == id) return state;
            return null;
        }
    }

    public State getState(Player p) {
        try {
            ResultSet rs = sql.getData("SELECT ID FROM Hider WHERE UUID = \"" + p.getUniqueId() + "\"");
            if (rs == null || !rs.next()) {
                setState(p, State.SHOW_ALL);
                return State.SHOW_ALL;
            }
            int id = rs.getInt("ID");
            return State.getByID(id);
        }catch (Exception e) { e.printStackTrace(); }
        return State.SHOW_ALL;
    }

    public void setState(Player p, State state) {
        sql.insert("DELETE FROM Hider WHERE UUID = \"" + p.getUniqueId() + "\"");
        sql.insert("INSERT INTO Hider (UUID, ID) VALUES (\"" + p.getUniqueId() + "\", " + state.getID() + ")");
    }

    private final HashMap<Player, Long> cooldowns = new HashMap<>();
    private final long cooldownInMilliseconds = 3000;
    public boolean checkCooldown(Player p) {
        if (!cooldowns.containsKey(p)) return true;
        long lastTime = cooldowns.get(p);
        long difference = System.currentTimeMillis() - lastTime;
        return difference >= cooldownInMilliseconds;
    }
    public void updateCooldown(Player p) {
        cooldowns.remove(p);
        cooldowns.put(p, System.currentTimeMillis());
    }

}
