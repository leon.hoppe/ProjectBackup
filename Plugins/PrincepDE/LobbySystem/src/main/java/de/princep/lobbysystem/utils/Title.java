package de.princep.lobbysystem.utils;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;


public class Title {

    public static String prefix = "§c§lPrincepDE §r§8» §r";

    public static String noPerm = prefix + "§7Diesen Befehl gibt es nicht.";


    public static void sendActionBar(Player p, String msg) {
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"\"}").a(msg);
        PacketPlayOutChat chat = new PacketPlayOutChat(base, (byte) 2);

        CraftPlayer cp = (CraftPlayer)p;
        cp.getHandle().playerConnection.sendPacket(chat);
    }

    public static void locTP(Player player, Location location) {
        Location l = null;
        try {
            l = location;
        } catch (Exception ignored) { }
        if (!(l == null)) {
            player.teleport(location);
        } else {
            player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);
            player.sendMessage("§8»");
            player.sendMessage(Title.prefix + "§7Der Punkt wurde noch nicht gesetzt.");
            player.sendMessage(Title.prefix + "§7Bitte wende dich an einen Serveradministrator.");
            player.sendMessage("§8»");
        }
    }

}
