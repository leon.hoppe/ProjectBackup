package de.princep.lobbysystem.annotations;

import io.github.classgraph.*;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class AnnotationManager {

    private final Map<Class<?>, Object> objects = new HashMap<>();
    private final Map<Class<? extends AnnotationAPI>, AnnotationAPI> apiObjects = new HashMap<>();

    public void addApiObject(AnnotationAPI o) {
        apiObjects.put(o.getClass(), o);
        objects.put(o.getClass(), o);
    }

    public AnnotationAPI getApiObject(Class<?> clazz) {
        return apiObjects.get(clazz);
    }

    public <T> T addObject(T o) {
        objects.put(o.getClass(), o);
        return o;
    }

    public Object getObject(Class<?> clazz) {
        return objects.get(clazz);
    }

    public void initialiseFields() {
        try (ScanResult result = new ClassGraph()
                .enableAllInfo()
                .acceptPackages("de.princep.lobbysystem")
                .scan()) {
            ClassInfoList classInfos = result.getAllClasses();
            for (ClassInfo classInfo : classInfos) {
                FieldInfoList fieldInfos = classInfo.getFieldInfo();
                for (FieldInfo fieldInfo : fieldInfos) {
                    if (fieldInfo.hasAnnotation(GetAPI.class.getName())) {
                        Field field = fieldInfo.loadClassAndGetField();
                        AnnotationAPI apiInstance = getApiObject(field.getType());
                        boolean isStatic = Modifier.isStatic(field.getModifiers());
                        field.set(isStatic ? null : getObject(field.getDeclaringClass()), apiInstance);
                    }
                }
            }
        }catch (Exception e) { e.printStackTrace(); }
        for (AnnotationAPI api : apiObjects.values()) {
            api.initialise();
        }
    }

}
