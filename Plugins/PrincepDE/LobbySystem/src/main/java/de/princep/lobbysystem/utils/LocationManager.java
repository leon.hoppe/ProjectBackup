package de.princep.lobbysystem.utils;

import de.princep.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.io.IOException;

public class LocationManager {

    public void save() {
        try {
            LobbySystem.cfg.save(LobbySystem.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLocation(Location location) {
        LobbySystem.cfg.set("Location.Spawn.world", location.getWorld().getName());
        LobbySystem.cfg.set("Location.Spawn.x", location.getX());
        LobbySystem.cfg.set("Location.Spawn.y", location.getY());
        LobbySystem.cfg.set("Location.Spawn.z", location.getZ());
        LobbySystem.cfg.set("Location.Spawn.yaw", location.getYaw());
        LobbySystem.cfg.set("Location.Spawn.pitch", location.getPitch());
        save();
    }

    public void setDeathBoarder(Player player) {
        LobbySystem.cfg.set("Location.Death.border", player.getLocation().getY());
        save();
    }

    public Double getDeathBoarder() {
        return LobbySystem.cfg.getDouble("Location.Death.border");
    }

    public Location getLocation() {
        Location location;
        World w = Bukkit.getWorld(LobbySystem.cfg.getString("Location.Spawn.world"));
        double x = LobbySystem.cfg.getDouble("Location.Spawn.x");
        double y = LobbySystem.cfg.getDouble("Location.Spawn.y");
        double z = LobbySystem.cfg.getDouble("Location.Spawn.z");
        location = new Location(w, x, y, z);
        location.setYaw(LobbySystem.cfg.getInt("Location.Spawn.yaw"));
        location.setPitch(LobbySystem.cfg.getInt("Location.Spawn.pitch"));
        return location;
    }



    public void setWarp(String name, Location location) {
        LobbySystem.cfg.set("Warp." + name + ".world", location.getWorld().getName());
        LobbySystem.cfg.set("Warp." + name + ".x", location.getX());
        LobbySystem.cfg.set("Warp." + name + ".y", location.getY());
        LobbySystem.cfg.set("Warp." + name + ".z", location.getZ());
        LobbySystem.cfg.set("Warp." + name + ".yaw", location.getYaw());
        LobbySystem.cfg.set("Warp." + name + ".pitch", location.getPitch());
        save();
    }

    public void delWarp(String name) {
        LobbySystem.cfg.set("Warp." + name + ".world", null);
        LobbySystem.cfg.set("Warp." + name + ".x", null);
        LobbySystem.cfg.set("Warp." + name + ".y", null);
        LobbySystem.cfg.set("Warp." + name + ".z", null);
        LobbySystem.cfg.set("Warp." + name + ".yaw", null);
        LobbySystem.cfg.set("Warp." + name + ".pitch", null);
        save();
    }

    public Location getWarp(String name) {
        Location location;
        World w = Bukkit.getWorld(LobbySystem.cfg.getString("Warp." + name + ".world"));
        double x = LobbySystem.cfg.getDouble("Warp." + name + ".x");
        double y = LobbySystem.cfg.getDouble("Warp." + name + ".y");
        double z = LobbySystem.cfg.getDouble("Warp." + name + ".z");
        location = new Location(w, x, y, z);
        location.setYaw(LobbySystem.cfg.getInt("Warp." + name + ".yaw"));
        location.setPitch(LobbySystem.cfg.getInt("Warp." + name + ".pitch"));
        return location;
    }

}
