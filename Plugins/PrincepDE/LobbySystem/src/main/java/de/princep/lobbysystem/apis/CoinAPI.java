package de.princep.lobbysystem.apis;

import de.princep.lobbysystem.annotations.AnnotationAPI;
import de.princep.lobbysystem.annotations.GetAPI;

import java.sql.ResultSet;
import java.util.*;

public class CoinAPI implements AnnotationAPI {
    @GetAPI
    public MySQL sql;
    private int startCoins = 0;

    public void initialise() {
        sql.insert("CREATE TABLE IF NOT EXISTS Coins (UUID VARCHAR(20), Coins INT(20))");
    }

    public void loadUser(UUID user) {
        if (getCoins(user) == -1) setCoins(user, startCoins);
    }

    public void setStartCoins(int startCoins) { this.startCoins = startCoins; }

    public void setCoins(UUID user, int coins) {
        sql.insert("DELETE FROM Coins WHERE UUID = \"" + user + "\"");
        if (coins < 0) coins *= -1;
        sql.insert("INSERT INTO Coins VALUES (\"" + user + "\", " + coins + ")");
    }

    public int getCoins(UUID user) {
        try {
            ResultSet rs = sql.getData("SELECT Coins FROM Coins WHERE UUID = \"" + user + "\"");
            if (rs.next()) {
                return rs.getInt("Coins");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void addCoins(UUID user, int coins) {
        if (coins < 0) coins *= -1;
        setCoins(user, getCoins(user) + coins);
    }

    public void removeCoins(UUID user, int coins) {
        if (coins > 0) coins *= -1;
        coins = getCoins(user) - coins;
        if (coins < 0) coins = 0;
        setCoins(user, coins);
    }

    public boolean compare(UUID user, int coins) {
        return getCoins(user) >= coins;
    }
}
