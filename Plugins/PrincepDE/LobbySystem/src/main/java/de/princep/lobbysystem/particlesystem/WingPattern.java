package de.princep.lobbysystem.particlesystem;

public final class WingPattern {
    private WingPattern() {}

    public static final String[] NORMAL = new String[] {
            "----xxx---",
            "---xxxxx--",
            "--xxxxxxx-",
            "-xxxxxxxx-",
            "xxxxxxxxxx",
            "xxxxxxxxxx",
            "xxxxxxxxxx",
            "xxxxxxxxxx",
            "--xxxxxxxx",
            "---xxxxxxx",
            "---xxxxxxx",
            "----xxxxxx",
            "----xxxxxx",
            "-----xxxx-",
            "-----xxxx-",
            "------xxx-",
            "------xxx-",
            "-------xx-",
            "--------x-"
    };

    public static final String[] ADVANCED = new String[] {
            "-------------------x+",
            "------------------x+-",
            "-----------------x+--",
            "---------------xxx---",
            "--------------xxx+---",
            "-------------xxx+----",
            "-----------xxxx+-----",
            "----------xxx+----x+-",
            "---------xxx+---xx+--",
            "-------xxx+----xx+---",
            "------xx+----xx+-----",
            "----xxx+---xxx+------",
            "---xx+---xxx+--------",
            "--xxx--xxx+-----x+---",
            "---xxxxx+----xxx+----",
            "----xxx+--xxxx+------",
            "----xxx--xxx+--------",
            "-----xxxxx+----x+----",
            "-----xxx+---xxx+-----",
            "----xxx+---xx+-------",
            "----xxx--xx+---------",
            "---xxxxxxx+----------",
            "xxxxxxxx-------------",
            "-xx---xx-------------",
            "--xx---xx------------",
            "---xx---xx-----------",
            "----xx---------------"
    };

    public static final float NORMAL_DISTANCE = 0.1f;
    public static final float ADVANCED_DISTANCE = 0.125f;

    public static final float NORMAL_OFFSET = 0.5f;
    public static final float ADVANCED_OFFSET = -1;

}
