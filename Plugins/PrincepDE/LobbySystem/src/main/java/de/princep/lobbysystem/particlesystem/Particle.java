package de.princep.lobbysystem.particlesystem;

import org.bukkit.Location;

public class Particle {
    public Location location;
    public ParticleEffect effect;

    public Particle(Location loc, ParticleEffect e) { location = loc; effect = e; }
}
