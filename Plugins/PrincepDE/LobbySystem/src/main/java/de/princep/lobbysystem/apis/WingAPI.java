package de.princep.lobbysystem.apis;

import de.princep.lobbysystem.annotations.AnnotationAPI;
import de.princep.lobbysystem.annotations.GetAPI;
import de.princep.lobbysystem.particlesystem.Particle;
import de.princep.lobbysystem.particlesystem.ParticleEffect;
import de.princep.lobbysystem.particlesystem.WingPattern;
import de.princep.lobbysystem.utils.Promissions;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WingAPI implements AnnotationAPI {
    public enum Wings {

        NORMAL(0, WingPattern.NORMAL, WingPattern.NORMAL_DISTANCE, ParticleEffect.REDSTONE, ParticleEffect.FLAME, WingPattern.NORMAL_OFFSET),
        ADVANCED(1, WingPattern.ADVANCED, WingPattern.ADVANCED_DISTANCE, ParticleEffect.REDSTONE, ParticleEffect.FLAME, WingPattern.ADVANCED_OFFSET);

        private final String[] pattern;
        private final float space;
        private final ParticleEffect smallPar;
        private final ParticleEffect largePar;
        private final float yOffset;
        private final int id;

        Wings(int id, String[] pattern, float space, ParticleEffect smallPar, ParticleEffect largePar, float yOffset) {
            this.id = id;
            this.pattern = pattern;
            this.space = space;
            this.smallPar = smallPar;
            this.largePar = largePar;
            this.yOffset = yOffset;
        }

        public String[] getPattern() { return pattern; }
        public float getSpace() { return space; }
        public ParticleEffect getSmallPar() { return smallPar; }
        public ParticleEffect getLargePar() { return largePar; }
        public float getYOffset() { return yOffset; }
        public int getId() { return id; }

        public static Wings getById(int id) { return id == 0 ? NORMAL : ADVANCED; }
    }
    private enum WingSite {
        LEFT(0, 0.5f),
        RIGHT(180, 0.5f);

        private final int rot;
        private final float xOffset;
        WingSite(int rot, float xOffset) { this.rot = rot; this.xOffset = xOffset; }
        public int getRot() { return rot; }
        public float getXOffset() { return xOffset; }
    }

    @GetAPI
    public HiderAPI hider;
    @GetAPI
    public MySQL sql;

    @Override
    public void initialise() {
        sql.insert("CREATE TABLE IF NOT EXISTS Wings (UUID VARCHAR(50), id INT(1))");
    }

    private final HashMap<Player, Wings> activatedPlayerWings = new HashMap<>();

    private Location getParticleLoc(Location orig, float x, float y, WingSite side, int animationState) {
        Location particleLoc = orig.clone();
        float yaw = particleLoc.getYaw();

        int state = side.getRot() + animationState;

        if (side == WingSite.LEFT)
            yaw -= state;
        else
            yaw += state;

        double yawRad = Math.toRadians(yaw);
        Vector vector = new Vector(Math.cos(yawRad) * x, y, Math.sin(yawRad) * x);
        particleLoc.add(vector);
        particleLoc.setYaw(yaw);
        return particleLoc;
    }

    private Particle[][] getParticlePositions(String[] pattern, float space, Location orig, ParticleEffect bigParticle, ParticleEffect smallParticle, WingSite site, float yOffset) {
        orig.setY(orig.getY() + yOffset);
        Particle[][] particles = new Particle[pattern.length][pattern[0].length()];

        if (pattern == WingPattern.NORMAL) {
            for (int x = pattern.length - 1; x >= 0; x--)
                for (int y = 0; y < pattern[x].length(); y++) {
                    float xp = space * (pattern[x].length() - x);
                    float yp = space * (pattern[x].length() - y);
                    Location parLoc = getParticleLoc(orig, xp + site.getXOffset(), yp, site, 20);
                    char c = pattern[x].toCharArray()[y];
                    if (c == '-') continue;
                    if (c == '+')
                        particles[x][y] = new Particle(parLoc, smallParticle);
                    if (c == 'x')
                        particles[x][y] = new Particle(parLoc, bigParticle);
                }
            return particles;
        }

        for (int x = 0; x < pattern.length; x++)
            for (int y = 0; y < pattern[x].length(); y++) {
                float xp = space * (pattern[x].length() - x);
                float yp = space * (pattern[x].length() - y);
                Location parLoc = getParticleLoc(orig, xp + site.getXOffset(), yp, site, 20);
                char c = pattern[x].toCharArray()[y];
                if (c == '-') continue;
                if (c == '+')
                    particles[x][y] = new Particle(parLoc, smallParticle);
                if (c == 'x')
                    particles[x][y] = new Particle(parLoc, bigParticle);
            }
        return particles;
    }
    private void spawnParticles(Player[] players, Particle[][] particles) {
        for (Particle[] parX : particles)
            for (Particle particle : parX) {
                if (particle == null) continue;
                particle.effect.display(0, 0, 0, 0, 1, particle.location, players);
            }
    }
    private void showWings(Wings wings, Player p) {
        ArrayList<Player> players = new ArrayList<>();
        players.add(p);
        for (Player all : Bukkit.getOnlinePlayers()) {
            if (all == p) continue;
            if (!all.canSee(p)) continue;
            HiderAPI.State state = hider.getState(all);
            if (state == HiderAPI.State.SHOW_ALL || (p.hasPermission(Promissions.VIP) && state == HiderAPI.State.ONLY_VIP))
                players.add(all);
        }
        Particle[][] left = getParticlePositions(wings.getPattern(), wings.getSpace(), p.getLocation().clone(), wings.getLargePar(), wings.getSmallPar(), WingSite.LEFT, wings.getYOffset());
        Particle[][] right = getParticlePositions(wings.getPattern(), wings.getSpace(), p.getLocation().clone(), wings.getLargePar(), wings.getSmallPar(), WingSite.RIGHT, wings.getYOffset());
        spawnParticles(players.toArray(new Player[0]), left);
        spawnParticles(players.toArray(new Player[0]), right);
    }

    public void activateWings(Player p, Wings wings) {
        deactivateWings(p); activatedPlayerWings.put(p, wings);
    }
    public void deactivateWings(Player p) { activatedPlayerWings.remove(p); }
    public Map<Player, Wings> getActivatedPlayerWings() { return activatedPlayerWings; }

}
