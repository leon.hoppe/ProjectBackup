package de.craftix.runnervshunter;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Compass implements Listener {
    public int schedulerID;

    public void startScheduler(){
        try {
            schedulerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (Player all : Bukkit.getOnlinePlayers()){
                        Player nearest = null;
                        for (Player runner : Main.runner){
                            if (nearest == null){
                                nearest = runner;
                                continue;
                            }
                            if (all.getLocation().distance(runner.getLocation()) < all.getLocation().distance(nearest.getLocation())) nearest = runner;
                        }
                        if (nearest == null) return;
                        all.setCompassTarget(nearest.getLocation());
                    }
                }
            }, 0, 20);
        }catch (Exception e){
            Bukkit.getPlayer("CraftixLP").sendMessage(e.getMessage());
        }
    }

    @EventHandler
    public void onCompassClick(PlayerInteractEvent event){
        try {
            if (!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.COMPASS)) return;
            if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                Player p = event.getPlayer();
                Player nearest = null;
                for (Player runner : Main.runner){
                    if (nearest == null){
                        nearest = runner;
                        continue;
                    }
                    if (p.getLocation().distance(runner.getLocation()) < p.getLocation().distance(nearest.getLocation())) nearest = runner;
                }
                int distance = (int)p.getLocation().distance(nearest.getLocation());
                p.sendMessage("§aDer Spieler " + nearest.getName() + " ist " + distance + " Blöcke entfernt");
            }
        }catch (Exception e){

        }
    }

}
