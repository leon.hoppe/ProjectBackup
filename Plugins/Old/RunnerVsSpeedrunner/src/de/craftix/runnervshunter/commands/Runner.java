package de.craftix.runnervshunter.commands;

import de.craftix.runnervshunter.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Runner implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("challenge.admin")) return true;
        if (args.length != 1) return true;
        Player t = Bukkit.getPlayer(args[0]);
        if (t == null) return true;
        Main.runner.add(t);
        Main.hunter.remove(t);
        sender.sendMessage("§aDer Spieler " + t.getName() + " wurde als Runner registriert");
        return true;
    }
}
