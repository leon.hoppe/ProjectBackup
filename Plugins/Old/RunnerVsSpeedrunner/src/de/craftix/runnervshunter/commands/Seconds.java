package de.craftix.runnervshunter.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Seconds implements CommandExecutor {
    public static int seconds = 15;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("challenge.admin")) return true;
        if (args.length != 1) return true;
        try {
            seconds = Integer.parseInt(args[0]);
            Start.seconds = seconds;
        }catch (Exception e){
            sender.sendMessage("Bitte gebe eine gültige Zahl ein");
        }
        return true;
    }
}
