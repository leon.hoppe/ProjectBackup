package de.craftix.runnervshunter.commands;

import de.craftix.runnervshunter.ActionBar;
import de.craftix.runnervshunter.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Start implements CommandExecutor {
    public static boolean huntersCanMove = false;
    public static int timerID;
    public static int seconds = 15;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("challenge.admin")) return true;
        for (Player all : Bukkit.getOnlinePlayers()){
            all.setGameMode(GameMode.SURVIVAL);
        }
        Main.isStarted = true;
        timerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (seconds == 0){
                    huntersCanMove = true;
                    seconds = Seconds.seconds;
                    Bukkit.getScheduler().cancelTask(timerID);
                }
                for (Player all : Bukkit.getOnlinePlayers()){
                    ActionBar.show(all, "§aDie Hunter dürfen sich in " + seconds + " sekunden bewegen!");
                }
                seconds--;
            }
        }, 0, 20);
        return true;
    }
}
