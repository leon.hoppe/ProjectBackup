package de.craftix.runnervshunter.listener;

import de.craftix.runnervshunter.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class onPvp implements Listener {

    @EventHandler
    public void onPvpEvent(EntityDamageByEntityEvent event){
        if (!Main.isStarted) return;
        if (!(event.getEntity() instanceof Player)) return;
        Player p = (Player)event.getEntity();
        if (!(event.getDamager() instanceof Player)) return;
        Player t = (Player)event.getDamager();
        if (!Main.hunter.contains(p)) return;
        if (!Main.hunter.contains(t)) return;
        event.setCancelled(true);
    }

}
