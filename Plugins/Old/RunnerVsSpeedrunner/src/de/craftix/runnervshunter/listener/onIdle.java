package de.craftix.runnervshunter.listener;

import de.craftix.runnervshunter.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class onIdle implements Listener {

    @EventHandler
    public void onBuild(BlockPlaceEvent event){
        if (Main.isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        if (Main.isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (Main.isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
        if (Main.isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent event){
        if (Main.isStarted) return;
        event.setCancelled(true);
        Player p = (Player)event.getEntity();
        p.setFoodLevel(25);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        if (Main.isStarted) return;
        if (event.getPlayer().hasPermission("challenge.admin")) return;
        event.setCancelled(true);
    }

}
