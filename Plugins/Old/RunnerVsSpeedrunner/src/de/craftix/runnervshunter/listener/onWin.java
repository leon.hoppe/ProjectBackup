package de.craftix.runnervshunter.listener;

import de.craftix.runnervshunter.Main;
import org.bukkit.entity.EnderDragon;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class onWin implements Listener {

    @EventHandler
    public void onDeath(EntityDeathEvent event){
        if (!Main.isStarted) return;
        if (!(event.getEntity() instanceof EnderDragon)) return;
        Main.win();
    }

    @EventHandler
    public void onLoose(PlayerDeathEvent event){
        if (!Main.isStarted) return;
        if (!Main.runner.contains(event.getEntity())) return;
        Main.loose();
    }

}
