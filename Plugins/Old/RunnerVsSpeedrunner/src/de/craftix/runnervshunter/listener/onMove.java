package de.craftix.runnervshunter.listener;

import de.craftix.runnervshunter.Main;
import de.craftix.runnervshunter.commands.Start;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class onMove implements Listener {

    @EventHandler
    public void onMoveEvent(PlayerMoveEvent event){
        if (!Main.hunter.contains(event.getPlayer())) return;
        if (Start.huntersCanMove) return;
        event.setCancelled(true);
    }

}
