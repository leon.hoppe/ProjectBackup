package de.craftix.runnervshunter;

import de.craftix.runnervshunter.commands.Hunter;
import de.craftix.runnervshunter.commands.Runner;
import de.craftix.runnervshunter.commands.Seconds;
import de.craftix.runnervshunter.commands.Start;
import de.craftix.runnervshunter.listener.onIdle;
import de.craftix.runnervshunter.listener.onMove;
import de.craftix.runnervshunter.listener.onPvp;
import de.craftix.runnervshunter.listener.onWin;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    public static ArrayList<Player> runner = new ArrayList<>();
    public static ArrayList<Player> hunter = new ArrayList<>();

    public static boolean isStarted = false;

    private static Main instance;
    private Compass compass;

    @Override
    public void onEnable() {
        instance = this;
        compass = new Compass();
        registerCommands();
        registerListener();
        compass.startScheduler();
    }

    private void registerCommands(){
        getCommand("hunter").setExecutor(new Hunter());
        getCommand("runner").setExecutor(new Runner());
        getCommand("start").setExecutor(new Start());
        getCommand("seconds").setExecutor(new Seconds());
    }

    private void registerListener(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onWin(), this);
        pm.registerEvents(new onIdle(), this);
        pm.registerEvents(new onPvp(), this);
        pm.registerEvents(new onMove(), this);
        pm.registerEvents(compass, this);
    }

    public static Main getInstance() {
        return instance;
    }

    public static void win(){
        for (Player all : Bukkit.getOnlinePlayers()){
            all.setGameMode(GameMode.SPECTATOR);
        }
        isStarted = false;
        Start.huntersCanMove = false;
        Bukkit.broadcastMessage("§aDer Enderdrache wurde getötet");
    }

    public static void loose(){
        for (Player all : Bukkit.getOnlinePlayers()){
            all.setGameMode(GameMode.SPECTATOR);
        }
        isStarted = false;
        Start.huntersCanMove = false;
        Bukkit.broadcastMessage("§cEiner der Runner ist gestorben");
    }
}
