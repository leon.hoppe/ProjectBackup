package de.craftix.community.api;

import de.craftix.community.general.Main;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Rang {

    private static FileConfiguration config = Main.getPlugin().getConfig();

    public static void set(Player p, Rankings rang){
        config.set(Rankings.ADMIN.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.ARRESTED.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.BÜRGERMEISTER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.PLAYER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.RICHTER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.SPECTATOR.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.YOUTUBE.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.TWITCH.name() + "." + p.getUniqueId(), null);
        config.set(rang.name() + "." + p.getUniqueId(), true);
        Main.getPlugin().saveConfig();
    }

    public static void remove(Player p){
        config.set(Rankings.ADMIN.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.ARRESTED.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.BÜRGERMEISTER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.PLAYER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.RICHTER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.SPECTATOR.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.YOUTUBE.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.TWITCH.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.PLAYER + "." + p.getUniqueId(), true);
    }

    public static Rankings get(Player p){
        if (config.contains(Rankings.ADMIN.name() + "." + p.getUniqueId())) return Rankings.ADMIN;
        if (config.contains(Rankings.ARRESTED.name() + "." + p.getUniqueId())) return Rankings.ARRESTED;
        if (config.contains(Rankings.BÜRGERMEISTER.name() + "." + p.getUniqueId())) return Rankings.BÜRGERMEISTER;
        if (config.contains(Rankings.RICHTER.name() + "." + p.getUniqueId())) return Rankings.RICHTER;
        if (config.contains(Rankings.SPECTATOR.name() + "." + p.getUniqueId())) return Rankings.SPECTATOR;
        if (config.contains(Rankings.YOUTUBE.name() + "." + p.getUniqueId())) return Rankings.YOUTUBE;
        if (config.contains(Rankings.TWITCH.name() + "." + p.getUniqueId())) return Rankings.TWITCH;
        return Rankings.PLAYER;
    }

    public static void setCreator(Player p, Rankings rang, String link){
        config.set(Rankings.ADMIN.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.ARRESTED.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.BÜRGERMEISTER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.PLAYER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.RICHTER.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.SPECTATOR.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.YOUTUBE.name() + "." + p.getUniqueId(), null);
        config.set(Rankings.TWITCH.name() + "." + p.getUniqueId(), null);
        config.set(rang.name() + "." + p.getUniqueId(), link);
        Main.getPlugin().saveConfig();
    }

    public static String getLink(Player p){
        String link =  config.getString(Rankings.TWITCH.name() + "." + p.getUniqueId());
        if (link == null) link = config.getString(Rankings.YOUTUBE.name() + "." + p.getUniqueId());
        return link;
    }

    public static boolean isCreator(Player p){
        if (config.contains(Rankings.YOUTUBE.name() + "." + p.getUniqueId())) return true;
        if (config.contains(Rankings.TWITCH.name() + "." + p.getUniqueId())) return true;
        return false;
    }

    public static boolean hasPermission(Player p, String permission){
        if (get(p).equals(Rankings.ADMIN)) return true;
        ArrayList<String> permissions = getRangPermissions(get(p), p);
        for (String s : permissions){
            if (s.equalsIgnoreCase(permission)) return true;
        }
        return false;
    }

    private static ArrayList<String> getRangPermissions(Rankings rang, Player p){
        ArrayList<String> permissions = new ArrayList<>();
        if (rang.equals(Rankings.RICHTER)){
            permissions.add("game.staatsanwalt");
            permissions.add("prison.add");
            permissions.add("prison.remove");
            permissions.add("prison.list");
            permissions.add("world.setprison");
        }
        else if (rang.equals(Rankings.BÜRGERMEISTER)){
            permissions.add("game.richter");
        }
        else if (rang.equals(Rankings.SPECTATOR)){
            permissions.add("game.teleport");
            permissions.add("game.gamemode");
            permissions.add("game.report");
        }
        if (p.hasPermission("game.admin")){
            permissions.add("game.spectator");
            permissions.add("world.setprison");
            permissions.add("game.staatsanwalt");
            permissions.add("prison.add");
            permissions.add("prison.remove");
            permissions.add("prison.list");
            permissions.add("game.richter");
            permissions.add("game.teleport");
            permissions.add("game.buergermeister");
            permissions.add("game.report");
        }
        return permissions;
    }
}


