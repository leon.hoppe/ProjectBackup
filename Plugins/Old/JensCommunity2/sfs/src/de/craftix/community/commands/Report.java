package de.craftix.community.commands;

import de.craftix.community.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Report implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String st, String[] args) {
        Main.getPlugin().reloadConfig();
        if (args.length == 2 && sender.hasPermission("report.edit") && args[0].equalsIgnoreCase("edit")){
            FileConfiguration config = Main.getPlugin().getConfig();
            boolean isFree = false;
            int reports = -1;
            while (!isFree){
                reports++;
                if (!config.contains("Reports." + reports + ".Target")) {
                    isFree = true;
                }
            }
            int targetID = -1;
            boolean detected = false;
            while (!detected){
                targetID++;
                if (targetID >= reports) break;
                if (config.getString("Reports." + targetID + ".Target").equalsIgnoreCase(args[1])) detected = true;
            }
            if (!detected){
                sender.sendMessage("§7[§eReport§7] §cSpieler konnte nicht gefunden werden");
                return true;
            }
            String target = config.getString("Reports." + targetID + ".Target");
            String reportSender = config.getString("Reports." + targetID + ".Sender");
            String reason = config.getString("Reports." + targetID + ".Reason");
            sender.sendMessage("§7[§eReport§7] §aDu bearbeitest den Report §6" + targetID + "§a:");
            sender.sendMessage("§aReporteter Spieler: §6" + target);
            sender.sendMessage("§aSender: §6" + reportSender);
            sender.sendMessage("§aGrund: §6" + reason);
            for (int i = targetID; i < reports; i++){
                int plusOne = i + 1;
                String iTarget = (String) config.get("Reports." + plusOne + ".Target");
                String iSender = (String) config.get("Reports." + plusOne + ".Sender");
                String iReason = (String) config.get("Reports." + plusOne + ".Reason");
                config.set("Reports." + i + ".Target", iTarget);
                config.set("Reports." + i + ".Sender", iSender);
                config.set("Reports." + i + ".Reason", iReason);
                config.set("Reports." + plusOne + ".Target", null);
                config.set("Reports." + plusOne + ".Sender", null);
                config.set("Reports." + plusOne + ".Reason", null);
            }
            Main.getPlugin().saveConfig();
            sender.sendMessage("§7[§eReport§7] §aDer Report wurde automatisch gelöscht");

            try {
                Player PSender = Bukkit.getPlayer(reportSender);
                PSender.sendMessage("§7[§eReport§7] §aDein Report über §6" + target + " §awird gerade bearbeitet");
            }catch (Exception e){}

            return true;
        }
        if (args.length == 1 && sender.hasPermission("report.list") && args[0].equalsIgnoreCase("list")){
            FileConfiguration config = Main.getPlugin().getConfig();
            boolean isFree = false;
            int reports = -1;
            while (!isFree){
                reports++;
                if (!config.contains("Reports." + reports + ".Target")) {
                    isFree = true;
                }
            }
            sender.sendMessage("§7[§eReport§7] §aEs gibt §6" + reports + " §aReports:");
            for (int i = 0; i < reports; i++){
                String target = config.getString("Reports." + i + ".Target");
                sender.sendMessage(target);
            }
            return true;
        }
        if (args.length < 2){
            sender.sendMessage("§7[§eReport§7] §c/report <spieler> <Hacking/Griefing/Beleidigung/Abfucken/Sonstiges>");
            return true;
        }
        if (Bukkit.getPlayer(args[0]) == null){
            sender.sendMessage("§7[§eReport§7] §cSpieler konnte nicht gefunden werden");
            return true;
        }
        Player target = Bukkit.getPlayer(args[0]);
        FileConfiguration config = Main.getPlugin().getConfig();
        boolean isFree = false;
        int number = -1;
        while (!isFree){
            number++;
            if (!config.contains("Reports." + number + ".Target")) isFree = true;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < args.length; i++){
            String subString = args[i] + " ";
            sb.append(subString);
        }
        String reason = sb.toString();
        config.set("Reports." + number + ".Target", target.getName());
        config.set("Reports." + number + ".Sender", sender.getName());
        config.set("Reports." + number + ".Reason", reason);
        Main.getPlugin().saveConfig();
        sender.sendMessage("§7[§eReport§7] §aIhr Report wurde erfolgreich Gesendet");
        for (Player p : Bukkit.getOnlinePlayers()){
            if (p.hasPermission("reports.message")) p.sendMessage("§7[§eReport§7] §aDer user §4"+ target.getName() + "§a wurde Gemeldet. Grund: §4"+ reason);
        }
        return true;
    }
}
