package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Gamemode implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player p = (Player)sender;
        if (Rang.hasPermission(p, "game.gamemode") && args.length == 1){
            if (args[0].equalsIgnoreCase("1")){
                p.setGameMode(GameMode.CREATIVE);
            }else if (args[0].equalsIgnoreCase("0")){
                p.setGameMode(GameMode.SURVIVAL);
            }else if (args[0].equalsIgnoreCase("3")){
                p.setGameMode(GameMode.SPECTATOR);
            }else {
                p.sendMessage("§cNö");
                return false;
            }
            p.sendMessage("§aDu bist nun im Gamemode " + args[0] + "!");
        }
        return false;
    }
}
