package de.craftix.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Invsee implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player && sender.hasPermission("game.invsee")){
            Player p = (Player)sender;
            if (args.length == 1){
                if(Bukkit.getPlayer(args[0]) == null){
                    p.sendMessage("§cSpieler konnte nicht gefunden werden");
                }
                Player t = Bukkit.getPlayer(args[0]);
                p.openInventory(t.getInventory());
            }
        }
        return true;
    }
}
