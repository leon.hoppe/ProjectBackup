package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import de.craftix.community.api.Rankings;
import de.craftix.community.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class YouTube implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String string, String[] args) {
        if (args.length != 1) return false;
        Player p = (Player)sender;
        if (args[0].equalsIgnoreCase("unlink")){
            Rang.set(p, Rankings.TWITCH);
            p.kickPlayer("§aRang wurde entfernt \nBitte reconnecte, um den Spieler Rang zu erhalten");
            return true;
        }
        Rang.setCreator(p, Rankings.YOUTUBE, args[0]);
        Main.getPlugin().saveConfig();
        p.kickPlayer("§aRang wurde Registriert \nBitte reconnecte, um den Rang zu erhalten");
        return true;
    }
}
