package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import de.craftix.community.api.Rankings;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Teleport implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player p = (Player)sender;
        if (Rang.hasPermission(p, "game.teleport") && Rang.get(p).equals(Rankings.SPECTATOR)){
            if (Bukkit.getPlayer(args[0]) != null){
                Player t = Bukkit.getPlayer(args[0]);
                p.teleport(t.getLocation());
                p.sendMessage("§aDu wurdes zu §6" + t.getDisplayName() + " §ateleportiert");
            }
        }else if (args.length == 1){
            p.performCommand("tp " + args[0]);
        }else if (args.length == 2){
            p.performCommand("minecraft:tp " + args[0] + " " + args[1]);
        }
        return true;
    }
}
