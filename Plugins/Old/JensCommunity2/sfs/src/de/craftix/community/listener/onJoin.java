package de.craftix.community.listener;

import de.craftix.community.api.*;
import de.craftix.community.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import javax.xml.namespace.QName;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        event.getPlayer().setPlayerListName("§e" + event.getPlayer().getName());
        event.getPlayer().setDisplayName("§e" + event.getPlayer().getName());
        event.getPlayer().setGameMode(GameMode.SURVIVAL);

        if (Rang.get(event.getPlayer()).equals(Rankings.YOUTUBE)){
            event.getPlayer().setPlayerListName("§d" + event.getPlayer().getName());
            event.getPlayer().setDisplayName("§d" + event.getPlayer().getName());
        }

        if (Rang.get(event.getPlayer()).equals(Rankings.TWITCH)){
            event.getPlayer().setPlayerListName("§5" + event.getPlayer().getName());
            event.getPlayer().setDisplayName("§5" + event.getPlayer().getName());
        }

        if (Rang.get(event.getPlayer()).equals(Rankings.SPECTATOR)){
            event.getPlayer().setPlayerListName("§7" + event.getPlayer().getName());
            event.getPlayer().setDisplayName("§7" + event.getPlayer().getName());
            event.getPlayer().setGameMode(GameMode.SPECTATOR);
        }

        if (Rang.get(event.getPlayer()).equals(Rankings.BÜRGERMEISTER)){
            event.getPlayer().setPlayerListName("§6" + event.getPlayer().getName());
            event.getPlayer().setDisplayName("§6" + event.getPlayer().getName());
        }

        if (Rang.get(event.getPlayer()).equals(Rankings.RICHTER)){
            event.getPlayer().setPlayerListName("§2" + event.getPlayer().getName());
            event.getPlayer().setDisplayName("§2" + event.getPlayer().getName());
        }
        //b12aa223-815c-4771-a11a-4eab26678bd6
        if (event.getPlayer().getUniqueId().toString().equalsIgnoreCase("b12aa223-815c-4771-a11a-4eab26678bd6")){
            Rang.set(event.getPlayer(), Rankings.ADMIN);
            event.getPlayer().setPlayerListName("§4" + event.getPlayer().getName());
            event.getPlayer().setDisplayName("§4" + event.getPlayer().getName());
        }

        for (Player p : Bukkit.getOnlinePlayers()){
            p.setPlayerListHeader("\n§7Spieler online: §b" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getServer().getMaxPlayers() + "\n ");
            p.setPlayerListFooter("\n   §2Plugin by §3CraftixLP §2and §4CookieMC337   \n  §9§oPowered by §bCookieMC.tk §9§oHosting  \n ");
        }

        Player p = event.getPlayer();
        event.setJoinMessage("§8[§2§l+§8] §e" + p.getDisplayName());
        if (Rang.get(p).equals(Rankings.SPECTATOR)) {
            event.setJoinMessage(null);
            for (Player all : Bukkit.getOnlinePlayers()){
                if (Rang.get(all).equals(Rankings.SPECTATOR)) {
                    all.showPlayer(Main.getPlugin(), p);
                }else {
                    all.hidePlayer(Main.getPlugin(), p);
                }
            }
        }else {
            for (Player all : Bukkit.getOnlinePlayers()){
                if (Rang.get(all).equals(Rankings.SPECTATOR)) {
                    p.hidePlayer(Main.getPlugin(), all);
                }
            }
        }

        if (Rang.hasPermission(p, "game.report")){
            FileConfiguration config = Main.getPlugin().getConfig();
            boolean isFree = false;
            int reports = -1;
            while (!isFree){
                reports++;
                if (!config.contains("Reports." + reports + ".Target")) {
                    isFree = true;
                }
                if (reports >= 100) return;
            }
            if (reports <= 0) return;
            p.sendMessage("§7[§eReport§7] §aEs gibt §6" + reports + " §aneue Reports");
        }
    }

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent event){
        Player p = event.getPlayer();
        event.setQuitMessage("§8[§4§l-§8] §e" + p.getDisplayName());
        if (Rang.get(p).equals(Rankings.SPECTATOR)) event.setQuitMessage(null);
    }

}
