package de.craftix.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Enderchest implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player p = (Player) sender;
        if (p.hasPermission("game.ec") && args.length == 1){
            if (Bukkit.getPlayer(args[0]) != null){
                Player t = Bukkit.getPlayer(args[0]);
                p.openInventory(t.getEnderChest());
            }else {
                p.sendMessage("§cDieser Spieler existiert nicht!");
            }
        }else {
            p.sendMessage("§cNo Permission");
        }
        return false;
    }
}
