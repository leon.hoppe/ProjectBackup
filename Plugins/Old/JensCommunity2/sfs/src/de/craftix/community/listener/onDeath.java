package de.craftix.community.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;

public class onDeath implements Listener, CommandExecutor {

    public static ArrayList<Death> deaths = new ArrayList<>();

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event){
        deaths.remove(Death.getDeath(event.getEntity()));
        deaths.add(new Death(event.getEntity(), event.getEntity().getLocation()));
        event.getEntity().sendMessage("§aBenutze §6/back §aum zu deinem Todespunkt zurückzukehren");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Death d = Death.getDeath((Player)sender);
        if (d != null){
            ((Player) sender).teleport(d.loc);
            deaths.remove(d);
            sender.sendMessage("§aTeleprotiere zum Todespunkt...");
        }else sender.sendMessage("§cDu hast keinen letzten Todespunkt");
        return true;
    }
}

class Death{

    public Player p;
    public Location loc;

    public Death(Player p, Location loc){
        this.loc = loc;
        this.p = p;
    }

    public static Death getDeath(Player p){
        for (Death d : onDeath.deaths){
            if (d.p == p) return d;
        }
        return null;
    }

}
