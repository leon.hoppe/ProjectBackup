package de.craftix.community.listener;

import de.craftix.community.api.Rang;
import de.craftix.community.api.Rankings;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;

import java.util.ArrayList;

public class onSleep implements Listener {

    private ArrayList<Player> sleepers = new ArrayList<>();

    @EventHandler
    public void onSleepEvent(PlayerBedEnterEvent event){
        if (event.getBedEnterResult() != PlayerBedEnterEvent.BedEnterResult.OK) return;
        sleepers.add(event.getPlayer());
        double t1 = (double) sleepers.size();
        double t2 = (double) event.getBed().getLocation().getWorld().getPlayers().size();
        World w = event.getBed().getLocation().getWorld();
        for (Player specs : Bukkit.getOnlinePlayers()){
            if (Rang.get(specs).equals(Rankings.SPECTATOR) && specs.getLocation().getWorld().equals(w)) t2--;
        }
        double t3 = (t1 / t2);
        double percent = (t3 * 100);
        double result = t2 / 100;
        result = result * 35;
        if (String.valueOf(result).contains(".")) result++;
        int players = (int) result;
        if (percent >= 35){
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "time set 0");
            Bukkit.broadcastMessage("§7[§6Sleep§7] §aDie Nacht wurde übersprungen");
            sleepers.clear();
        }else {
            Bukkit.broadcastMessage("§7[§6Sleep§7] §aEs schlafen §b" + sleepers.size() + " §a/ §b" + players + " §aSpieler");
        }
    }

    @EventHandler
    public void onStandUpEvent(PlayerBedLeaveEvent event){
        sleepers.remove(event.getPlayer());
    }

}