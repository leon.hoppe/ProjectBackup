package de.craftix.community.general;

import de.craftix.community.api.Rankings;
import de.craftix.community.commands.*;
import de.craftix.community.listener.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main plugin;
    private onDeath death = new onDeath();

    public static Main getPlugin(){return plugin;}

    @Override
    public void onEnable() {
        plugin = this;
        registerListener();
        registerCommands();
        //MySql.Connect();
        //MySql.Update("CREATE TABLE IF NOT EXISTS prison (UUID VARCHAR(100), time INT(200), x INT(10), y INT(10), z INT(10))");
        System.out.println("Irgendwas Geladen");

    }

    @Override
    public void onDisable() {
        //MySql.Disconnect();
        System.out.println("Ich halt jz mein maul");

    }

    private void registerCommands(){
        getCommand("report").setExecutor(new Report());
        getCommand("youtube").setExecutor(new YouTube());
        getCommand("twitch").setExecutor(new Twitch());
        getCommand("spectator").setExecutor(new Spectator());
        getCommand("creator").setExecutor(new Creator());
        getCommand("back").setExecutor(death);
        getCommand("invsee").setExecutor(new Invsee());
        //getCommand("prison").setExecutor(new Prison());
        getCommand("setprison").setExecutor(new Setprison());
        getCommand("yt").setExecutor(new YouTube());
        getCommand("buergermeister").setExecutor(new Buergermeister());
        getCommand("richter").setExecutor(new Richter());
        getCommand("tp").setExecutor(new Teleport());
        getCommand("ec").setExecutor(new Enderchest());
        getCommand("gm").setExecutor(new Gamemode());
        System.out.println("Commands Geladen");
    }

    private void registerListener(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onJoin(), this);
        pm.registerEvents(new onChat(), this);
        pm.registerEvents(new onDrop(), this);
        pm.registerEvents(new onSleep(), this);
        pm.registerEvents(death, this);
        System.out.println("Listener Geladen");
    }
}
