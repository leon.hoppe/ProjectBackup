package de.craftix.community.api;

public enum Rankings{
    ADMIN,
    SPECTATOR,
    RICHTER,
    BÜRGERMEISTER,
    PLAYER,
    ARRESTED,
    YOUTUBE,
    TWITCH
}
