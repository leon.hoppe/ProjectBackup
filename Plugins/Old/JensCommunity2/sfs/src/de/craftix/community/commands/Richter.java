package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import de.craftix.community.api.Rankings;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Richter implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player p = (Player)sender;
        if (Rang.hasPermission(p, "game.richter")){
            if (args.length == 2){
                if (Bukkit.getPlayer(args[1]) != null){
                    Player t = Bukkit.getPlayer(args[1]);
                    if (args[0].equalsIgnoreCase("add")){
                        Rang.set(t, Rankings.RICHTER);
                        t.kickPlayer("§aDu bist nun ein Richter\nReconnecte, um den Rang zu erhalten");
                        sender.sendMessage("§aDer spieler §6" + t.getName() + " §aist nun ein Richter");
                    }
                    else if (args[0].equalsIgnoreCase("remove")){
                        Rang.remove(t);
                        t.kickPlayer("§aDu bist kein Richter mehr\nReconnecte, um den Spieler zu werden");
                        sender.sendMessage("§aDer spieler §6" + t.getName() + " §aist nun kein Richter mehr");
                    }
                    else {
                        sender.sendMessage("§cBitte benutze §6/richter <add/remove> <player>§c!");
                    }
                }else {
                    sender.sendMessage("§cSpieler konnte nicht gefunden werden");
                }
            }else {
                sender.sendMessage("§cBitte benutze §6/richter <add/remove> <player>§c!");
            }
        }
        return true;
    }
}
