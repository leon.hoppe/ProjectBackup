package de.craftix.community.listener;

import de.craftix.community.api.Rang;
import de.craftix.community.api.Rankings;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class onChat implements Listener {

    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent event){
        String message = event.getMessage();
        if (message.contains("%")){
            message = message.replace('%', '§');
        }
        if (Rang.get(event.getPlayer()).equals(Rankings.SPECTATOR)){
            event.setFormat(event.getPlayer().getDisplayName() + " §7>> §4" + message);
        }else {
            event.setFormat(event.getPlayer().getDisplayName() + " §7>> §r" + message);
        }
    }

}
