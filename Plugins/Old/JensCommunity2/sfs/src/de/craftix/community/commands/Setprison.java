package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import de.craftix.community.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Setprison implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String str, String[] args) {
        Player p = (Player)sender;
        if (sender instanceof Player && Rang.hasPermission(p, "world.setprison")){
            FileConfiguration config = Main.getPlugin().getConfig();
            config.set("Prison.World", p.getLocation().getWorld().getName());
            config.set("Prison.X", p.getLocation().getX());
            config.set("Prison.Y", p.getLocation().getY());
            config.set("Prison.Z", p.getLocation().getZ());
            config.set("Prison.Yaw", p.getLocation().getYaw());
            config.set("Prison.Pitch", p.getLocation().getPitch());
            p.sendMessage("§aDu hast die Gefängnisposition gesetzt");
        }
        Main.getPlugin().saveConfig();
        return true;
    }

    public static Location getPrison(){
        FileConfiguration config = Main.getPlugin().getConfig();
        World world = Bukkit.getWorld(config.getString("Prison.World"));
        double x = config.getDouble("Prison.X");
        double y = config.getDouble("Prison.Y");
        double z = config.getDouble("Prison.Z");
        float yaw = (float) config.getDouble("Prison.Yaw");
        float pitch = (float) config.getDouble("Prison.Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }
}
