package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import de.craftix.community.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Creator implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length != 1) return true;
        Player p = null;
        try { p = Bukkit.getPlayer(args[0]); } catch (Exception e) { }
        if (p == null){
            sender.sendMessage("§cSpieler konnte nicht gefunden werden");
            return true;
        }
        if (Rang.isCreator(p)){
            sender.sendMessage("§aDer Link von §6" + p.getDisplayName() + " §alautet: §6" + Rang.getLink(p));
        }else {
            sender.sendMessage("§cDieser Spieler ist kein YouTuber/Streamer");
        }
        return true;
    }
}
