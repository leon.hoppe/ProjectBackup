package de.craftix.community.commands;

import de.craftix.community.api.Rang;
import de.craftix.community.api.Rankings;
import de.craftix.community.general.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Spectator implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player p = (Player)sender;
        if (sender instanceof Player && Rang.hasPermission(p, "game.spectator")){
            if (Rang.get(p).equals(Rankings.SPECTATOR)){
                Rang.set(p, Rankings.PLAYER);
                p.kickPlayer("§aRang wurde entfernt \nBitte reconnecte, um den Spieler Rang zu erhalten");
            }else {
                Rang.set(p, Rankings.SPECTATOR);
                p.kickPlayer("§aRang wurde registriert \nBitte reconnecte, um den Rang zu erhalten");
            }
        }
        return true;
    }
}
