package de.craftix.community.commands;

import de.craftix.community.api.Rankings;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class PrisonPlayer{
    public static ArrayList<PrisonPlayer> players = new ArrayList<>();

    public Player p;
    public Location loc;
    public Rankings rang;
    public PrisonPlayer(Player p, Location loc, Rankings rang){
        this.p = p;
        this.loc = loc;
        this.rang = rang;
        players.add(this);
    }

    public static PrisonPlayer getPlayer(Player p){
        for (PrisonPlayer pp : players){
            if (pp.p == p) return pp;
        }
        return null;
    }

    public static void removePlayer(Player p){
        players.remove(getPlayer(p));
    }
}
