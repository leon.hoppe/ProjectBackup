package de.craftix.bansystem.commands;


import de.craftix.bansystem.utils.Messages;
import de.craftix.bansystem.utils.Reason;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class KickCmd extends Command {
    public KickCmd(String name, String permission, String... aliases) {
        super(name, permission, aliases);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer p = (ProxiedPlayer) sender;
        if (args.length < 1) return;
        ProxiedPlayer t = BungeeCord.getInstance().getPlayer(args[0]);
        if (t == null) {
            p.sendMessage(new TextComponent("§cDieser Spieler existiert nicht!"));
            return;
        }
        if (args.length != 2) {
            p.sendMessage(new TextComponent(Messages.types));
            return;
        }
        int typeID = Integer.parseInt(args[1]);
        Reason type = null;
        for (Reason all : Reason.values()) if (all.getTypeID() == typeID) type = all;
        if (type == null) {
            p.sendMessage(new TextComponent(Messages.syntaxErr));
            p.sendMessage(new TextComponent(Messages.types));
            return;
        }
        t.disconnect(new TextComponent("§cDu wurdest wegen " + type.getMessage() + " §cgekickt!"));
        p.sendMessage(new TextComponent("§aDu hast §7" + t.getName() + " §agekickt"));
    }
}
