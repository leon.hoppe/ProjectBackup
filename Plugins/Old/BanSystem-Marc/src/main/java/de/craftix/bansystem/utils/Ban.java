package de.craftix.bansystem.utils;

import de.craftix.bansystem.MySQL;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

public class Ban {

    public static Ban getPlayerBan(UUID uuid) {
        if (hasBanAccount(uuid)) return new Ban(uuid);
        return new Ban(uuid, null, 0);
    }

    public static void createBan(ProxiedPlayer p, Reason type) {
        Ban ban = new Ban(p.getUniqueId(), type, type.getTime());
        ban.save();
    }

    public static boolean hasBanAccount(UUID uuid) {
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Ban WHERE UUID = \"" + uuid + "\"");
            if (rs == null) return false;
            if (!rs.next()) return false;
            return rs.getString("UUID") != null;
        }catch (Exception ignored) {}
        return false;
    }

    public static Reason getType(int typeID) {
        for (Reason type : Reason.values()) if (type.getTypeID() == typeID) return type;
        return null;
    }

    public static ArrayList<Ban> getAllBans() {
        ArrayList<Ban> bans = new ArrayList<>();
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Ban");
            while (rs.next()) {
                bans.add(new Ban(UUID.fromString(rs.getString("UUID"))));
            }
        }catch (Exception ignored) {}
        return bans;
    }

    //nonStatic
    public ProxiedPlayer player;
    public Reason reason;
    public long time;

    public Ban(UUID uuid) {
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Ban WHERE UUID = \"" + uuid + "\"");
            rs.next();
            player = BungeeCord.getInstance().getPlayer(uuid);
            reason = getType(rs.getInt("Type"));
            time = rs.getInt("Time");
        }catch (Exception e) { e.printStackTrace(); }
    }

    private Ban(UUID uuid, Reason reason, long time) {
        this.player = BungeeCord.getInstance().getPlayer(uuid);
        this.reason = reason;
        this.time = time;
    }

    public void save() {
        UUID uuid = player.getUniqueId();
        MySQL.insert("DELETE FROM Ban WHERE UUID = \"" + uuid + "\"");
        MySQL.insert("INSERT INTO Ban (UUID, Type, Time) VALUES (\"" + uuid + "\", " + reason.getTypeID() + ", " + time + ")");
        MySQL.insert("INSERT INTO BanHistory (UUID, Type) VALUES (\"" + uuid + "\", " + reason.getTypeID() + ")");
    }

    public ArrayList<Ban> getBanHistory() {
        UUID uuid = player.getUniqueId();
        ArrayList<Ban> history = new ArrayList<>();
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM BanHistory WHERE UUID = \"" + uuid + "\"");
            if (rs == null) return history;
            while (rs.next()) {
                history.add(new Ban(uuid, getType(rs.getInt("Type")), 0L));
            }
        }catch (Exception e) { e.printStackTrace(); }
        return history;
    }

}
