package de.craftix.bansystem.utils;

import de.craftix.bansystem.MySQL;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

public class Mute {

    public static Mute getPlayerMute(UUID uuid) {
        if (hasMuteAccount(uuid)) return new Mute(uuid);
        return new Mute(uuid, null, 0);
    }

    public static void createMute(ProxiedPlayer p, Reason type) {
        Mute mute = new Mute(p.getUniqueId(), type, type.getTime());
        mute.save();
    }

    public static boolean hasMuteAccount(UUID uuid) {
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Mute WHERE UUID = \"" + uuid + "\"");
            if (rs == null) return false;
            if (!rs.next()) return false;
            return rs.getString("UUID") != null;
        }catch (Exception ignored) {}
        return false;
    }

    public static ArrayList<Mute> getAllMute() {
        ArrayList<Mute> mutes = new ArrayList<>();
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Mute");
            while (rs.next()) {
                mutes.add(new Mute(UUID.fromString(rs.getString("UUID"))));
            }
        }catch (Exception ignored) {}
        return mutes;
    }

    //nonStatic
    public ProxiedPlayer player;
    public Reason reason;
    public long time;

    public Mute(UUID uuid) {
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Mute WHERE UUID = \"" + uuid + "\"");
            if (rs == null) return;
            if (!rs.next()) return;
            player = BungeeCord.getInstance().getPlayer(uuid);
            reason = Ban.getType(rs.getInt("Type"));
            time = rs.getInt("Time");
        }catch (Exception e) { e.printStackTrace(); }
    }

    private Mute(UUID uuid, Reason reason, long time) {
        this.player = BungeeCord.getInstance().getPlayer(uuid);
        this.reason = reason;
        this.time = time;
    }

    public void save() {
        UUID uuid = player.getUniqueId();
        MySQL.insert("DELETE FROM Mute WHERE UUID = \"" + uuid + "\"");
        MySQL.insert("INSERT INTO Mute (UUID, Type, Time) VALUES (\"" + uuid + "\", " + reason.getTypeID() + ", " + time + ")");
        MySQL.insert("INSERT INTO MuteHistory (UUID, Type) VALUES (\"" + uuid + "\", " + reason.getTypeID() + ")");
    }

    public ArrayList<Mute> getBanHistory() {
        UUID uuid = player.getUniqueId();
        ArrayList<Mute> history = new ArrayList<>();
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM MuteHistory WHERE UUID = \"" + uuid + "\"");
            if (rs == null) return history;
            while (rs.next()) {
                history.add(new Mute(uuid, Ban.getType(rs.getInt("Type")), 0L));
            }
        }catch (Exception e) { e.printStackTrace(); }
        return history;
    }

}
