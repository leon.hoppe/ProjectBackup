package de.craftix.bansystem.commands;

import de.craftix.bansystem.BanSystem;
import de.craftix.bansystem.utils.Ban;
import de.craftix.bansystem.utils.Reason;
import de.craftix.bansystem.utils.Messages;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BanCmd extends Command {
    public BanCmd(String name, String permission, String... aliases) {
        super(name, permission, aliases);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) return;
        ProxiedPlayer p = (ProxiedPlayer) sender;
        if (args.length != 2) {
            p.sendMessage(Messages.syntaxErr);
            return;
        }
        int typeID = Integer.parseInt(args[1]);
        Reason type = null;
        for (Reason all : Reason.values()) if (all.getTypeID() == typeID) type = all;
        if (type == null) {
            p.sendMessage(Messages.syntaxErr);
            p.sendMessage(Messages.types);
            return;
        }
        ProxiedPlayer t = BungeeCord.getInstance().getPlayer(args[0]);
        if (Ban.hasBanAccount(t.getUniqueId())) {
            p.sendMessage(new TextComponent(Messages.prefix + "§cDieser Spieler ist bereits gebannt!"));
            return;
        }
        if (t.equals(p)) {
            p.sendMessage(new TextComponent(Messages.prefix + "§cDu kannst dich nicht selber bannen!"));
            return;
        }
        Ban.createBan(t, type);
        p.sendMessage(Messages.prefix + "§aDer Spieler §6" + t.getName() + " §awurde gebannt");
        BanSystem.kickPlayer(t, type);
    }
}
