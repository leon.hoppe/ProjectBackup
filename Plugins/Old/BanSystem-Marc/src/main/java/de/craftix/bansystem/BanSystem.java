package de.craftix.bansystem;

import de.craftix.bansystem.commands.BanCmd;
import de.craftix.bansystem.commands.BantypesCmd;
import de.craftix.bansystem.commands.KickCmd;
import de.craftix.bansystem.commands.MuteCmd;
import de.craftix.bansystem.utils.Ban;
import de.craftix.bansystem.utils.Reason;
import de.craftix.bansystem.utils.Messages;
import de.craftix.bansystem.utils.Mute;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.event.EventHandler;

import java.sql.Date;
import java.sql.Timestamp;

public final class BanSystem extends Plugin implements Listener {

    @Override
    public void onEnable() {
        MySQL.connect();
        manageSqlTables();
        PluginManager pm = getProxy().getPluginManager();
        pm.registerCommand(this, new BanCmd("ban", "princep.ban", "pban"));
        pm.registerCommand(this, new BantypesCmd("bantypes", "princep.list", "blist"));
        pm.registerCommand(this, new MuteCmd("mute", "princep.mute", "pmute"));
        pm.registerCommand(this, new KickCmd("kick", "princep.kick"));
        pm.registerListener(this, this);
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
    }

    private void manageSqlTables() {
        if (!MySQL.isConnected()) return;
        MySQL.insert("CREATE TABLE IF NOT EXISTS Ban (UUID VARCHAR(100), Type INT(10), Time INT(255))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS BanHistory (UUID VARCHAR(100), Type INT(10))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS Mute (UUID VARCHAR(100), Type INT(10), Time INT(255))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS MuteHistory (UUID VARCHAR(100), Type INT(10))");
    }

    public static void kickPlayer(ProxiedPlayer t, Reason reason) {
        t.disconnect(new TextComponent("§cDu wurdest gebannt!\n§aReason: §6" + reason.getMessage()));
    }

    @EventHandler
    public void onJoin(ServerSwitchEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if (p == null) System.out.println("Player not found");
        if (Ban.hasBanAccount(p.getUniqueId())) kickPlayer(p, Ban.getPlayerBan(p.getUniqueId()).reason);
    }

    @EventHandler
    public void onChat(ChatEvent e) {
        ProxiedPlayer p = (ProxiedPlayer) e.getSender();
        if (p == null) System.out.println("Player not found");
        if (Mute.hasMuteAccount(p.getUniqueId())) {
            e.setCancelled(true);
            p.sendMessage(new TextComponent(Messages.prefix + "§cDu bist gemutet, du kannst in " + convertTimeToString(Mute.getPlayerMute(p.getUniqueId()).time) + " wieder schreiben"));
        }
    }

    public String convertTimeToString(long time) {
        Timestamp ts = new Timestamp(time);
        Date date = new Date(ts.getTime());
        return date.toString();
    }
}
