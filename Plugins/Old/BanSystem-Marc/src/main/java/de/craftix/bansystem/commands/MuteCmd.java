package de.craftix.bansystem.commands;

import de.craftix.bansystem.utils.Reason;
import de.craftix.bansystem.utils.Messages;
import de.craftix.bansystem.utils.Mute;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MuteCmd extends Command {
    public MuteCmd(String name, String permission, String... aliases) {
        super(name, permission, aliases);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) return;
        ProxiedPlayer p = (ProxiedPlayer) sender;
        if (args.length != 2) {
            p.sendMessage(new TextComponent(Messages.syntaxErr));
            return;
        }
        int typeID = Integer.parseInt(args[1]);
        Reason type = null;
        for (Reason all : Reason.values()) if (all.getTypeID() == typeID) type = all;
        if (type == null) {
            p.sendMessage(new TextComponent(Messages.syntaxErr));
            p.sendMessage(new TextComponent(Messages.types));
            return;
        }
        ProxiedPlayer t = BungeeCord.getInstance().getPlayer(args[0]);
        if (t == null) {
            p.sendMessage(new TextComponent(Messages.prefix + "§cDieser Spieler exisitiert nicht"));
            return;
        }
        if (Mute.hasMuteAccount(t.getUniqueId())) {
            p.sendMessage(new TextComponent(Messages.prefix + "§cDieser Spieler ist bereits gemuted!"));
            return;
        }
        if (t.equals(p)) {
            p.sendMessage(new TextComponent(Messages.prefix + "§cDu kannst dich nicht selber gemuted!"));
            return;
        }
        Mute.createMute(t, type);
        p.sendMessage(new TextComponent(Messages.prefix + "§aDer Spieler §6" + t.getName() + " §awurde gemuted"));
    }
}
