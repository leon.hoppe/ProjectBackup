package de.craftix.bansystem.utils;

public enum Reason {

    HACKING(0, 1, "§4Hacking");

    private int typeID;
    private long time;
    private String message;

    Reason(int typeID, long time, String message) {
        this.typeID = typeID;
        this.time = time;
        this.message = message;
    }

    public int getTypeID() {
        return typeID;
    }
    public long getTime() {
        return time;
    }
    public String getMessage() {
        return message;
    }
}
