package de.craftix.pvp.general;

import de.craftix.pvp.commands.*;
import de.craftix.pvp.listener.*;
import de.craftix.pvp.manager.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main plugin;
    public static FileConfiguration config;
    public static Gamestates state;
    public static boolean isConfigured;

    @Override
    public void onEnable() {

        //Listener
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onDamage(), this);
        pm.registerEvents(new onJoin(), this);
        pm.registerEvents(new onQuit(), this);
        pm.registerEvents(new onBuild(), this);
        pm.registerEvents(new onDeath(), this);
        pm.registerEvents(new onDrop(), this);
        pm.registerEvents(new LobbyItems(), this);
        pm.registerEvents(new onFood(), this);

        //Commands
        getCommand("setlobby").setExecutor(new SetlobbyCmd());
        getCommand("addspawn").setExecutor(new AddspawnCmd());
        getCommand("setspec").setExecutor(new SetspecCmd());
        getCommand("start").setExecutor(new StartCmd());

        plugin = this;
        config = this.getConfig();
        state = Gamestates.LOBBY;
        checkIfConfigured();
    }

    @Override
    public void onDisable() {

    }

    public static Main getPlugin(){
        return plugin;
    }

    public void checkIfConfigured(){
        if (config.contains(("Lobby.World")) && config.contains("Spawn.1.World") && config.contains("Spawn.2.World") && config.contains("Spec.World")){
            isConfigured = true;
        }else isConfigured = false;
    }
}
