package de.craftix.pvp.commands;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamemanager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddspawnCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player){
            if (sender.hasPermission("1vs1.setup")){
                Player p = (Player)sender;
                if (Main.config.contains("Spawn.1.World")){
                    Gamemanager.saveSpawn(p.getLocation(), 2);
                    p.sendMessage("§aDer Spawn 2 wurde erfolgreich gesetzt");
                }else {
                    Gamemanager.saveSpawn(p.getLocation(), 1);
                    p.sendMessage("§aDer Spawn 1 wurde erfolgreich gesetzt");
                }
            }
        }
        return true;
    }
}
