package de.craftix.pvp.commands;

import de.craftix.pvp.manager.Gamemanager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetspecCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player){
            if (sender.hasPermission("1vs1.setup")){
                Player p = (Player)sender;
                Gamemanager.saveSpec(p.getLocation());
                p.sendMessage("§aDer Spectatorspawn wurde erfolgreich gesetzt");
            }
        }
        return true;
    }
}
