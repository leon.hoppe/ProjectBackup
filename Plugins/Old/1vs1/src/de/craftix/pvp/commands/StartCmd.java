package de.craftix.pvp.commands;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamemanager;
import de.craftix.pvp.manager.Gamestates;
import de.craftix.pvp.manager.Kitmanager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCmd implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender.hasPermission("1vs1.start")){
            if (Main.state != Gamestates.LOBBY) return true;
            if (Bukkit.getOnlinePlayers().size() < 2) return true;
            Bukkit.getScheduler().cancelAllTasks();
            Main.state = Gamestates.INGAME;
            Player p = Gamemanager.inGame.get(0);
            p.teleport(Gamemanager.getSpawn(1));
            p = Gamemanager.inGame.get(1);
            p.teleport(Gamemanager.getSpawn(2));
            boolean p1 = false, p2 = false;
            for (Kitmanager k : Gamemanager.kitmanagers){
                if (k.p.equals(Gamemanager.inGame.get(0))) p1 = true;
                if (k.p.equals(Gamemanager.inGame.get(1))) p2 = true;
            }
            if (p1 == false){
                new Kitmanager(Gamemanager.inGame.get(0), 2);
            }
            if (p2 == false){
                new Kitmanager(Gamemanager.inGame.get(1), 2);
            }
            for (Kitmanager k : Gamemanager.kitmanagers){
                k.giveItems();
            }
        }
        return true;
    }
}
