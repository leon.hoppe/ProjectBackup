package de.craftix.pvp.manager;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class Kitmanager {

    //0 = UHC
    //1 = SOUP
    //2 = RANDOM
    //3 = SG
    //4 = TANK

    public Player p;
    public int kitID;

    public Kitmanager(Player p, int id){
        this.p = p;
        kitID = id;
        Gamemanager.kitmanagers.add(this);
    }

    public void giveItems(){
        p.getInventory().clear();
        if (kitID == 0){ //UHC
            //Blöcke 128
            p.getInventory().setItem(6, new ItemStack(Material.SANDSTONE, 64));
            p.getInventory().setItem(7, new ItemStack(Material.SANDSTONE, 64));
            //Lava 2
            p.getInventory().setItem(4, new ItemStack(Material.LAVA_BUCKET));
            p.getInventory().setItem(13, new ItemStack(Material.LAVA_BUCKET));
            //Wasser 2
            p.getInventory().setItem(5, new ItemStack(Material.WATER_BUCKET));
            p.getInventory().setItem(14, new ItemStack(Material.WATER_BUCKET));
            //Bogen | 16 Pfeile
            p.getInventory().setItem(1, new ItemStack(Material.BOW));
            p.getInventory().setItem(9, new ItemStack(Material.ARROW, 16));
            //Dia Schwert Schärfe 1
            ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
            sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
            p.getInventory().setItem(0, sword);
            //Eisenrüstung
            p.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
            p.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
            p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
            p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
            //Goldäpfel 4
            p.getInventory().setItem(3, new ItemStack(Material.GOLDEN_APPLE, 4));
            //Essen 32
            p.getInventory().setItem(8, new ItemStack(Material.COOKED_BEEF, 32));
            //Angel
            p.getInventory().setItem(2, new ItemStack(Material.FISHING_ROD));
        }
        if (kitID == 1){ //OP
            //Diamantrüstung Protection 2
            ItemStack helmet = new ItemStack(Material.DIAMOND_HELMET);
            helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

            ItemStack chestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
            chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

            ItemStack leggins = new ItemStack(Material.DIAMOND_LEGGINGS);
            leggins.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

            ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS);
            boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

            p.getInventory().setHelmet(helmet);
            p.getInventory().setChestplate(chestplate);
            p.getInventory().setLeggings(leggins);
            p.getInventory().setBoots(boots);
            //Essen
            p.getInventory().setItem(8, new ItemStack(Material.COOKED_BEEF, 32));
            //Holzschwert Unbreaking 1
            ItemStack sw = new ItemStack(Material.WOOD_SWORD);
            sw.addEnchantment(Enchantment.DURABILITY, 1);
            p.getInventory().setItem(0, sw);
        }
        if (kitID == 2){ //RANDOM
            Random rand = new Random();
            int ram = rand.nextInt(3);
            if (ram == 2) ram = 4;
            kitID = ram;
            giveItems();
        }
        if (kitID == 3){ //SG
            //Rüstung Dia/Eisen
            p.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
            p.getInventory().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
            p.getInventory().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
            p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
            //Eisenschwert Schärfe 3
            ItemStack sword = new ItemStack(Material.IRON_SWORD);
            sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
            p.getInventory().setItem(0, sword);
            //Essen 32
            p.getInventory().setItem(8, new ItemStack(Material.COOKED_BEEF, 32));
            //Goldäpfel 4
            p.getInventory().setItem(4, new ItemStack(Material.GOLDEN_APPLE, 4));
            //Angel
            p.getInventory().setItem(2, new ItemStack(Material.FISHING_ROD));
            //Bogen Power 1 | 4 Pfeile
            ItemStack bow = new ItemStack(Material.BOW);
            bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
            p.getInventory().setItem(1, bow);
            p.getInventory().setItem(9, new ItemStack(Material.ARROW, 4));
        }
        if (kitID == 4){ //TANK
            //Eisenrüstung Protection und so
            ItemStack helmet = new ItemStack(Material.IRON_HELMET);
            helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

            ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
            chestplate.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3);

            ItemStack leggins = new ItemStack(Material.IRON_LEGGINGS);
            leggins.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

            ItemStack boots = new ItemStack(Material.IRON_BOOTS);
            boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);

            p.getInventory().setHelmet(helmet);
            p.getInventory().setChestplate(chestplate);
            p.getInventory().setLeggings(leggins);
            p.getInventory().setBoots(boots);
            //Essen 10
            p.getInventory().setItem(8, new ItemStack(Material.COOKED_BEEF, 32));
            //Steinschwert Schärfe 1
            ItemStack sword = new ItemStack(Material.STONE_SWORD);
            sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
            p.getInventory().setItem(0, sword);
        }
    }

}
