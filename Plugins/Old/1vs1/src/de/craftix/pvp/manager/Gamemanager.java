package de.craftix.pvp.manager;

import de.craftix.pvp.general.Main;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.CloudServer;
import de.dytanic.cloudnet.lib.cloudserver.CloudServerMeta;
import de.dytanic.cloudnet.lib.server.ServerState;
import de.welovemcskript.capi.CoinAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Random;


public class Gamemanager {

    public static Player winner;
    public static Player looser;

    public static ArrayList<Player> inGame = new ArrayList<>();
    public static ArrayList<Player> spectator = new ArrayList<>();
    public static ArrayList<Kitmanager> kitmanagers = new ArrayList<>();

    public static int cache;


    public static Location getLobby(){
        FileConfiguration config = Main.config;
        World world = Bukkit.getWorld(config.getString("Lobby.World"));
        double x = config.getDouble("Lobby.X");
        double y = config.getDouble("Lobby.Y");
        double z = config.getDouble("Lobby.Z");
        float yaw = (float) config.getDouble("Lobby.Yaw");
        float pitch = (float) config.getDouble("Lobby.Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static Location getSpec(){
        FileConfiguration config = Main.config;
        World world = Bukkit.getWorld(config.getString("Spec.World"));
        double x = config.getDouble("Spec.X");
        double y = config.getDouble("Spec.Y");
        double z = config.getDouble("Spec.Z");
        float yaw = (float) config.getDouble("Spec.Yaw");
        float pitch = (float) config.getDouble("Spec.Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static Location getSpawn(int id){
        FileConfiguration config = Main.config;
        World world = Bukkit.getWorld(config.getString("Spawn." + id +".World"));
        double x = config.getDouble("Spawn." + id +".X");
        double y = config.getDouble("Spawn." + id +".Y");
        double z = config.getDouble("Spawn." + id +".Z");
        float yaw = (float) config.getDouble("Spawn." + id +".Yaw");
        float pitch = (float) config.getDouble("Spawn." + id +".Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static void saveLobby(Location loc){
        FileConfiguration config = Main.config;
        config.set("Lobby.World", loc.getWorld().getName());
        config.set("Lobby.X", loc.getBlockX() + 0.5);
        config.set("Lobby.Y", loc.getBlockY() + 2);
        config.set("Lobby.Z", loc.getBlockZ() + 0.5);
        config.set("Lobby.Yaw", loc.getYaw());
        config.set("Lobby.Pitch", loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static void saveSpec(Location loc){
        FileConfiguration config = Main.config;
        config.set("Spec.World", loc.getWorld().getName());
        config.set("Spec.X", loc.getBlockX() + 0.5);
        config.set("Spec.Y", loc.getBlockY());
        config.set("Spec.Z", loc.getBlockZ() + 0.5);
        config.set("Spec.Yaw", loc.getYaw());
        config.set("Spec.Pitch", loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static void saveSpawn(Location loc, int id){
        FileConfiguration config = Main.config;
        config.set("Spawn." + id +".World", loc.getWorld().getName());
        config.set("Spawn." + id +".X", loc.getBlockX() + 0.5);
        config.set("Spawn." + id +".Y", loc.getBlockY() + 1);
        config.set("Spawn." + id +".Z", loc.getBlockZ() + 0.5);
        config.set("Spawn." + id +".Yaw", loc.getYaw());
        config.set("Spawn." + id +".Pitch", loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static void startTimer(){
        cache = 0;
        int maxLobby = 60;
        int maxReset = 15;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), () -> {
            if (Main.state == Gamestates.LOBBY && cache == maxLobby){
                stopTimer();
                Main.state = Gamestates.INGAME;
                Player p = Gamemanager.inGame.get(0);
                p.teleport(Gamemanager.getSpawn(1));
                p = Gamemanager.inGame.get(1);
                p.teleport(Gamemanager.getSpawn(2));
                boolean p1 = false, p2 = false;
                for (Kitmanager k : kitmanagers){
                    if (k.p.equals(inGame.get(0))) p1 = true;
                    if (k.p.equals(inGame.get(1))) p2 = true;
                }
                if (!p1){
                    new Kitmanager(inGame.get(0), 2);
                }
                if (!p2){
                    new Kitmanager(inGame.get(1), 2);
                }
                for (Kitmanager k : kitmanagers){
                    k.giveItems();
                }
            }
            if (Main.state == Gamestates.RESTART && cache == maxReset){
                stopTimer();
                for (Player a : Bukkit.getServer().getOnlinePlayers()) {
                    a.kickPlayer(null);
                }
                Bukkit.getServer().shutdown();
            }
            if (Main.state == Gamestates.LOBBY){
                int ram = maxLobby - cache;
                switch (ram){
                    case 60: case 30: case 15: case 10: case 5: case 4: case 3: case 2: case 1: case  0:
                        Bukkit.broadcastMessage("§aSpiel startet in §6" + ram);
                        break;
                }
            }else if (Main.state == Gamestates.RESTART){
                int ram = maxReset - cache;
                switch (ram){
                    case 15: case 10: case 5: case 4: case 3: case 2: case 1: case  0:
                        Bukkit.broadcastMessage("§aServer stoppt in §6" + ram);
                        break;
                }
            }
            cache++;
        },0, 20);
    }

    public static void stopTimer(){
        Bukkit.getScheduler().cancelAllTasks();
        Bukkit.broadcastMessage("§aDer Timer wurde gestopt");
    }

    public static void giveRewards(Player p){
        CoinAPI.addMoney(p, 10);
        p.sendMessage("§6+10 §aCoins");
    }

}
