package de.craftix.pvp.manager;

import de.craftix.pvp.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LobbyItems implements Listener {

    private static ItemStack kits;

    public static void giveInventory(Player p){
        kits = new ItemStack(Material.CHEST);
        ItemMeta kitsMeta = kits.getItemMeta();
        kitsMeta.setDisplayName("§aKits");
        kits.setItemMeta(kitsMeta);

        p.getInventory().setItem(4, kits);
    }

    @EventHandler
    public void onKlick(PlayerInteractEvent event){
        if (Main.state != Gamestates.LOBBY) return;
        if (event.getItem() == null) return;
        if (event.getItem().getItemMeta().getDisplayName().equals("§aKits")) openInventory(event.getPlayer());
    }

    private ItemStack uhc;
    private ItemStack op;
    private ItemStack sg;
    private ItemStack tank;
    private ItemStack random;

    private void openInventory(Player p){
        Inventory inv = Bukkit.createInventory(null, 9, "§aKits");

        //UHC
        uhc = new ItemStack(Material.GOLDEN_APPLE);
        ItemMeta uhcMeta = uhc.getItemMeta();
        uhcMeta.setDisplayName("§6UHC");
        uhc.setItemMeta(uhcMeta);
        //Coming Soon
        op = new ItemStack(Material.COMMAND);
        ItemMeta csMeta = op.getItemMeta();
        csMeta.setDisplayName("§6OP");
        op.setItemMeta(csMeta);
        //SG
        sg = new ItemStack(Material.FISHING_ROD);
        ItemMeta sgMeta = sg.getItemMeta();
        sgMeta.setDisplayName("§6SG");
        sg.setItemMeta(sgMeta);
        //TANK
        tank = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta tankMeta = tank.getItemMeta();
        tankMeta.setDisplayName("§6TANK");
        tank.setItemMeta(tankMeta);
        //RANDOM
        random = new ItemStack(Material.NETHER_STAR);
        ItemMeta randomMeta = random.getItemMeta();
        randomMeta.setDisplayName("§6RANDOM");
        random.setItemMeta(randomMeta);

        inv.setItem(0, uhc);
        inv.setItem(2, op);
        inv.setItem(4, random);
        inv.setItem(6, sg);
        inv.setItem(8, tank);

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (Main.state != Gamestates.LOBBY) return;
        if (!Gamemanager.inGame.contains(event.getWhoClicked())) return;
        if (!event.getClickedInventory().getTitle().equals("§aKits")) return;
        int kitID = event.getSlot() / 2;
        for (Kitmanager k : Gamemanager.kitmanagers){
            if (k.p == event.getWhoClicked()) Gamemanager.kitmanagers.remove(k);
        }
        new Kitmanager((Player) event.getWhoClicked(), kitID);
        event.getWhoClicked().sendMessage("§aDu hast das Kit §6" + event.getCurrentItem().getItemMeta().getDisplayName() + " §aausgewählt");
    }

}
