package de.craftix.pvp.listener;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamestates;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

public class onDrop implements Listener {

    @EventHandler
    public void onDropEvent(PlayerDropItemEvent event){
        if (Main.state != Gamestates.LOBBY) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInvClickEvent(InventoryClickEvent event){
        if (Main.state != Gamestates.LOBBY) return;
        event.setCancelled(true);
    }

}
