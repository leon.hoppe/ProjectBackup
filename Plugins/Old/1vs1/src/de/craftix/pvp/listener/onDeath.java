package de.craftix.pvp.listener;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamemanager;
import de.craftix.pvp.manager.Gamestates;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class onDeath implements Listener {

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event){
        for (Player all : Bukkit.getOnlinePlayers()){
            all.getInventory().clear();
            all.getInventory().setBoots(null);
            all.getInventory().setLeggings(null);
            all.getInventory().setChestplate(null);
            all.getInventory().setHelmet(null);
        }
        event.setDeathMessage(null);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kill @e[type=Item]");
        Player p = event.getEntity();
        if (Gamemanager.inGame.contains(p)){
            Gamemanager.looser = p;
            Gamemanager.inGame.remove(p);
            Gamemanager.winner = Gamemanager.inGame.get(0);
            Main.state = Gamestates.RESTART;
            Gamemanager.winner.teleport(Gamemanager.getLobby());
            Gamemanager.looser.teleport(Gamemanager.getLobby());
            Bukkit.broadcastMessage("§aHerzlichen Glückwunsch §6" + Gamemanager.winner.getName());
            Gamemanager.startTimer();
            Gamemanager.giveRewards(Gamemanager.winner);
            for (Player a : Bukkit.getServer().getOnlinePlayers()){
                a.setHealth(20);
                a.setFoodLevel(25);
                a.getInventory().clear();
                a.getInventory().setBoots(null);
                a.getInventory().setLeggings(null);
                a.getInventory().setChestplate(null);
                a.getInventory().setHelmet(null);
                a.teleport(Gamemanager.getLobby());
                a.setGameMode(GameMode.SURVIVAL);
            }
        }
    }

}
