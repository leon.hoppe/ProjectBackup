package de.craftix.pvp.listener;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamestates;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class onFood implements Listener {

    @EventHandler
    public void onFoodEvent(FoodLevelChangeEvent event){
        if (Main.state != Gamestates.INGAME){
            event.setCancelled(true);
            Player p  = (Player)event.getEntity();
            p.setFoodLevel(20);
        }
    }

}
