package de.craftix.pvp.listener;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamemanager;
import de.craftix.pvp.manager.Gamestates;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class onQuit implements Listener {

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent event){
        Player p = event.getPlayer();
        event.setQuitMessage("§b1vs1 §7| §cDer Spieler §6" + event.getPlayer().getName() + " §chat das Spiel verlassen");
        if (Main.state == Gamestates.LOBBY){
            Gamemanager.stopTimer();
        }
        if (Main.state == Gamestates.INGAME){
            if (Gamemanager.inGame.contains(p)){
                Gamemanager.looser = p;
                Gamemanager.inGame.remove(p);
                Gamemanager.winner = Gamemanager.inGame.get(0);
                Main.state = Gamestates.RESTART;
                Gamemanager.winner.teleport(Gamemanager.getLobby());
                Gamemanager.looser.teleport(Gamemanager.getLobby());
                Bukkit.broadcastMessage("§aHerzlichen Glückwunsch §6" + Gamemanager.winner.getName());
                Gamemanager.startTimer();
                Gamemanager.giveRewards(Gamemanager.winner);
                for (Player a : Bukkit.getServer().getOnlinePlayers()){
                    a.setHealth(20);
                    a.setFoodLevel(25);
                    a.getInventory().clear();
                    a.getInventory().setBoots(null);
                    a.getInventory().setLeggings(null);
                    a.getInventory().setChestplate(null);
                    a.getInventory().setHelmet(null);
                    a.teleport(Gamemanager.getLobby());
                    a.setGameMode(GameMode.SURVIVAL);
                }
            }
        }
        Gamemanager.inGame.remove(p);
        Gamemanager.spectator.remove(p);
    }

}
