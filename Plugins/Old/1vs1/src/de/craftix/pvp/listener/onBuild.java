package de.craftix.pvp.listener;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamestates;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class onBuild implements Listener {

    @EventHandler
    public void onBuildEvent(BlockPlaceEvent event){
        if (Main.state == Gamestates.INGAME) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onBreakEvent(BlockBreakEvent event){
        if (Main.state == Gamestates.INGAME) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event){
        if (Main.state == Gamestates.INGAME) return;
        event.setCancelled(true);
    }

}
