package de.craftix.pvp.listener;

import de.craftix.pvp.general.Main;
import de.craftix.pvp.manager.Gamemanager;
import de.craftix.pvp.manager.Gamestates;
import de.craftix.pvp.manager.LobbyItems;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        if (!Main.isConfigured){
            Bukkit.broadcastMessage("§7-- §cBitte konfiguriere das Plugin §7          --");
            Bukkit.broadcastMessage("§7-- §c/setlobby §7: §aSetze die Lobby §7        --");
            Bukkit.broadcastMessage("§7-- §c/addspawn §7: §aSetze einen Spawn §7      --");
            Bukkit.broadcastMessage("§7-- §c/setspec §7: §aSetze den Spectatorspawn §7--");
            Bukkit.broadcastMessage("§7-- §arestarte den Server §7                    --");
            Bukkit.broadcastMessage("§7-- §aPlugin by §6§lTube Craft §7               --");
            return;
        }
        event.setJoinMessage("§b1vs1 §7| §aDer Spieler §6" + event.getPlayer().getName() + " §ahat das Spiel betreten");
        Player p = event.getPlayer();
        p.setHealth(20);
        p.setFoodLevel(25);
        p.getInventory().clear();
        p.getInventory().setBoots(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setHelmet(null);
        if (Main.state == Gamestates.LOBBY){
            if (Bukkit.getServer().getOnlinePlayers().size() <= 2){
                p.teleport(Gamemanager.getLobby());
                p.setGameMode(GameMode.SURVIVAL);
                Gamemanager.inGame.add(p);
                if (Gamemanager.inGame.size() == 2){
                    Gamemanager.startTimer();
                }
                LobbyItems.giveInventory(p);
            }else {
                p.setGameMode(GameMode.SPECTATOR);
                Gamemanager.spectator.add(p);
            }
        }
        if (Main.state == Gamestates.INGAME){
            p.teleport(Gamemanager.getSpec());
            p.setGameMode(GameMode.SPECTATOR);
            Gamemanager.spectator.add(p);
        }
        if (Main.state == Gamestates.RESTART){
            p.kickPlayer("§cDer Server startet gerade neu!");
        }
    }

}
