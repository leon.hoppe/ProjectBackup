package de.craftix.lobby.commands;

import de.craftix.lobby.api.WarpAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

public class Warps implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String st, String[] args) {
        ArrayList<String> warps = WarpAPI.getWarps();
        WarpAPI.setup();
        String out = "";
        for (String s : warps){
            if (s == warps.get(warps.size() - 1)) out += s;
            else out += s + ", ";
        }
        sender.sendMessage("§aAlle registerierten Warps:");
        sender.sendMessage("§a" + out);
        return true;
    }
}
