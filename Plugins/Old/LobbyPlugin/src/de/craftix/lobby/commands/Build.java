package de.craftix.lobby.commands;

import de.craftix.lobby.Main;
import de.craftix.lobby.Permissions;
import de.craftix.lobby.api.SpawnAPI;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Build implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player p = (Player)sender;
        if (!p.hasPermission(Permissions.build)) return false;
        if (Main.buildMode.contains(p)) {
            Main.buildMode.remove(p);
            SpawnAPI.giveInventory(p);
            p.setGameMode(GameMode.ADVENTURE);
            p.sendMessage("§aDu hast den Build-Mode verlassen");
        }else {
            Main.buildMode.add(p);
            p.getInventory().clear();
            p.setGameMode(GameMode.CREATIVE);
            p.sendMessage("§aDu bist nun im Build-Mode");
        }
        return true;
    }
}
