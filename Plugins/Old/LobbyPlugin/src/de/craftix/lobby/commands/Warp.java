package de.craftix.lobby.commands;

import de.craftix.lobby.api.WarpAPI;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Warp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length != 1) return false;
        if (!(sender instanceof Player)) return false;
        Player p = (Player) sender;
        WarpAPI.setup();
        Location warp = WarpAPI.getWarp(args[0]);
        if (warp == null) {
            p.sendMessage("§cDieser Warp existert nicht");
            return true;
        }
        p.teleport(warp);
        return true;
    }
}
