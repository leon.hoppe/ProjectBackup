package de.craftix.lobby.commands;

import de.craftix.lobby.Permissions;
import de.craftix.lobby.api.SpawnAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length != 0) return false;
        if (!(sender instanceof Player)) return false;
        Player p = (Player) sender;
        if (!p.hasPermission(Permissions.admin)) return false;
        SpawnAPI.setLocation(p.getLocation());
        p.sendMessage("§aD hast den Spawnpunkt gesetzt");
        return true;
    }
}
