package de.craftix.lobby.commands;

import de.craftix.lobby.api.SpawnAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length != 0) return false;
        if (!(sender instanceof Player)) return false;
        Player p = (Player) sender;
        SpawnAPI.teleport(p);
        return true;
    }
}
