package de.craftix.lobby.commands;

import de.craftix.lobby.Permissions;
import de.craftix.lobby.api.WarpAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetWarp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length != 1) return false;
        if (!(sender instanceof Player)) return false;
        Player p = (Player) sender;
        if (!p.hasPermission(Permissions.admin)) return false;
        if (!WarpAPI.createWarp(args[0], p.getLocation())){
            p.sendMessage("§cDieser Warp existiert bereits");
            return true;
        }
        p.sendMessage("§aDer Warp §b" + args[0] + " §awurde erfolgreich erstellt");
        WarpAPI.save();
        return true;
    }
}
