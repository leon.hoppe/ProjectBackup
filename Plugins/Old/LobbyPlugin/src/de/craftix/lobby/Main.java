package de.craftix.lobby;

import de.craftix.lobby.api.Hider;
import de.craftix.lobby.api.WarpAPI;
import de.craftix.lobby.commands.*;
import de.craftix.lobby.listener.onJoin;
import de.craftix.lobby.protectors.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {
    private static Main instance;
    public static ArrayList<Player> buildMode = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        WarpAPI.setup();
        registerCommands();
        registerListener();
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "time set 6000");
    }

    @Override
    public void onDisable() {
        WarpAPI.save();
    }

    public static Main getInstance() {
        return instance;
    }

    private void registerListener(){
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new Hider(), this);
        pm.registerEvents(new onBuild(), this);
        pm.registerEvents(new onDamage(), this);
        pm.registerEvents(new onWeather(), this);
        pm.registerEvents(new onChat(), this);
        pm.registerEvents(new onDrop(), this);
        pm.registerEvents(new onInteract(), this);
        pm.registerEvents(new onJoin(), this);

        System.out.println("LobbySystem: Listener loaded successfully");
    }

    private void registerCommands(){
        getCommand("setspawn").setExecutor(new SetSpawn());
        getCommand("setwarp").setExecutor(new SetWarp());
        getCommand("spawn").setExecutor(new Spawn());
        getCommand("warp").setExecutor(new Warp());
        getCommand("warps").setExecutor(new Warps());
        getCommand("build").setExecutor(new Build());

        System.out.println("LobbySystem: Commands loaded successfully");
    }
}
