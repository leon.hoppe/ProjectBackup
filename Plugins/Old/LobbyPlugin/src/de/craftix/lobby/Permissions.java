package de.craftix.lobby;

public class Permissions {

    public static final String admin = "lobby.admin";
    public static final String build = "lobby.build";
    public static final String vip = "lobby.vip";
    public static final String canChat = "lobby.chat";

}
