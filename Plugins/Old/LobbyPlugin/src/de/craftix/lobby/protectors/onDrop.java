package de.craftix.lobby.protectors;

import de.craftix.lobby.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

public class onDrop implements Listener {

    @EventHandler
    public void onDropEvent(PlayerDropItemEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onMoveEvent(InventoryClickEvent event){
        Player p = (Player) event.getWhoClicked();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

}
