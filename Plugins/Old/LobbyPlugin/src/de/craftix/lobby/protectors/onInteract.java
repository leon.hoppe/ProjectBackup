package de.craftix.lobby.protectors;

import de.craftix.lobby.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class onInteract implements Listener {

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteractEntityEvent(PlayerInteractEntityEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteractAtEntityEvent(PlayerInteractAtEntityEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }
}
