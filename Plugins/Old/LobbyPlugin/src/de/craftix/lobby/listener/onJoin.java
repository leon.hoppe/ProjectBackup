package de.craftix.lobby.listener;

import de.craftix.lobby.api.SpawnAPI;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        event.setJoinMessage(null);
        SpawnAPI.giveInventory(event.getPlayer());
        SpawnAPI.teleport(event.getPlayer());
        event.getPlayer().setGameMode(GameMode.ADVENTURE);
    }

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent event){
        event.setQuitMessage(null);
    }

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event){
        event.setKeepInventory(true);
        event.getEntity().spigot().respawn();
        SpawnAPI.giveInventory(event.getEntity());
        SpawnAPI.teleport(event.getEntity());
        event.setDroppedExp(0);
        event.setDeathMessage(null);
    }

}
