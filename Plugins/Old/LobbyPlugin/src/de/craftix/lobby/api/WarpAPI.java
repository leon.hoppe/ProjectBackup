package de.craftix.lobby.api;

import de.craftix.lobby.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;

public class WarpAPI {

    private static ArrayList<Warp> warps = new ArrayList<>();
    private static FileConfiguration config;

    public static void setup(){
        config = Main.getInstance().getConfig();
        warps.clear();
        for (int id = 0; id < getFreeID(); id++){
            warps.add(getWarp(id));
        }
    }

    public static void save(){
        for (Warp w : warps){
            saveWarp(w);
        }
    }

    public static Location getWarp(String name){
        for (Warp w : warps){
            if (w.name.equalsIgnoreCase(name)) return w.loc;
        }
        return null;
    }

    public static boolean createWarp(String name, Location loc){
        boolean output = true;
        for (Warp w : warps){
            if (w.name.equalsIgnoreCase(name)) output = false;
        }
        if (output){
            Warp warp = new Warp();
            warp.id = getFreeID();
            warp.name = name;
            warp.loc = loc;
            warps.add(warp);
        }
        return output;
    }

    public static ArrayList<String> getWarps(){
        ArrayList<String> list = new ArrayList<>();
        for (Warp w : warps){
            list.add(w.name);
        }
        return list;
    }

    private static int getFreeID(){
        int id = 0;
        boolean isFree = false;
        while (!isFree){
            if (!config.contains("Warps." + id + ".Name")) isFree = true;
            else id++;
        }
        return id;
    }

    private static Warp getWarp(int id){
        Warp w = new Warp();
        World world = Bukkit.getWorld(config.getString("Warps." + id + ".World"));
        double x = config.getDouble("Warps." + id + ".X");
        double y = config.getDouble("Warps." + id + ".Y");
        double z = config.getDouble("Warps." + id + ".Z");
        float yaw = (float) config.getDouble("Warps." + id + ".Yaw");
        float pitch = (float) config.getDouble("Warps." + id + ".Pitch");

        w.loc = new Location(world, x, y, z, yaw, pitch);
        w.id = id;
        w.name = config.getString("Warps." + w.id + ".Name");
        return w;
    }

    private static void saveWarp(Warp w){
        config.set("Warps." + w.id + ".Name", w.name);
        config.set("Warps." + w.id + ".World", w.loc.getWorld().getName());
        config.set("Warps." + w.id + ".X", w.loc.getBlockX() + 0.5);
        config.set("Warps." + w.id + ".Y", w.loc.getBlockY() + 2);
        config.set("Warps." + w.id + ".Z", w.loc.getBlockZ() + 0.5);
        config.set("Warps." + w.id + ".Yaw", w.loc.getYaw());
        config.set("Warps." + w.id + ".Pitch", w.loc.getPitch());

        Main.getInstance().saveConfig();
    }

}

class Warp {
    public Integer id;
    public String name;
    public Location loc;
}
