package de.craftix.lobby.api;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ActionBarAPI {

    public static void Show(Player p, String msg){
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"\"}").a(msg);
        PacketPlayOutChat chat = new PacketPlayOutChat(base, (byte) 2);

        CraftPlayer cp = (CraftPlayer)p;
        cp.getHandle().playerConnection.sendPacket(chat);
    }

}
