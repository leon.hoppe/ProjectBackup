package de.craftix.lobby.api;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class TeleporterItem {

    public static ItemStack normal(Material mat, String name){
        ItemStack is = new ItemStack(mat);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack normal(Material mat, int subID, String name){
        ItemStack is = new ItemStack(mat, 1, (short) subID);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack skull (String name, Player owner){
        ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta sm = (SkullMeta) is.getItemMeta();
        sm.setOwner(owner.getName());
        sm.setDisplayName(name);
        is.setItemMeta(sm);
        return is;
    }

}
