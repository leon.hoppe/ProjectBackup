package de.craftix.minecraftutils.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;

public class OnFarmlandDestroy implements Listener {

    @EventHandler
    public void onFarmlandDestroy(BlockPhysicsEvent event) {
        if (event.getChangedType() == Material.FARMLAND)
            event.setCancelled(true);
    }

}
