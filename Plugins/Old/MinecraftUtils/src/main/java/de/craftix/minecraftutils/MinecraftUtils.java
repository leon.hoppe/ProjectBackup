package de.craftix.minecraftutils;

import de.craftix.minecraftutils.listeners.OnFarmlandDestroy;
import de.craftix.minecraftutils.listeners.OnZombieBreakDoor;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class MinecraftUtils extends JavaPlugin {
    private static MinecraftUtils instance;

    @Override
    public void onEnable() {
        instance = this;
        // Plugin startup logic
        Bukkit.getPluginManager().registerEvents(new OnFarmlandDestroy(), this);
        Bukkit.getPluginManager().registerEvents(new OnZombieBreakDoor(), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static MinecraftUtils getInstance() {
        return instance;
    }
}
