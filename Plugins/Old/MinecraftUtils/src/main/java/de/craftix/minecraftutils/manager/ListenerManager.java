package de.craftix.minecraftutils.manager;

import de.craftix.minecraftutils.MinecraftUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.function.Consumer;

public class ListenerManager {

    public static <T extends Event> Listener registerEvent(EventPriority priority, Class<T> clazz, Consumer<T> event) {
        Listener listener = new Listener() {};
        Bukkit.getPluginManager().registerEvent(clazz, listener, priority, (l, e) -> {
            if (clazz.isInstance(event)) event.accept((T) e);
        }, MinecraftUtils.getInstance(), false);
        return listener;
    }

}
