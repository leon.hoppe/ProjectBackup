package de.craftix.minecraftutils.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityBreakDoorEvent;

public class OnZombieBreakDoor implements Listener {

    @EventHandler
    public void onZombieBreakDoor(EntityBreakDoorEvent event) {
        if (event.getEntity().getType() == EntityType.ZOMBIE && event.getBlock().getType().name().contains("DOOR"))
            event.setCancelled(true);
    }

}
