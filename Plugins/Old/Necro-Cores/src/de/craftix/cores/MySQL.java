package de.craftix.cores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    public static final String server = "localhost";
    public static final Integer port = 3306;
    public static final String db = "Cores";
    public static final String user = "Cores";
    public static final String pass = "Cores";
    private static Connection con = null;

    public static boolean connect(){
        String conString = "jdbc:mysql://" + server + ":" + port + "/" + db;
        try {
            con = DriverManager.getConnection(conString, user, pass);
            System.out.println("[Cores] MySQL connected successfully");
            return true;
        }catch (SQLException e){
            System.out.println("[Cores] MySQL connection failed");
            return false;
        }
    }

    public static boolean disconnect(){
        if (!isConnected()) return true;
        try {
            con.close();
            con = null;
            System.out.println("[Cores] MySQL disconnected");
            return true;
        }catch (SQLException e){
            System.out.println("[Cores] MySQL disconnecting failed");
            return false;
        }
    }

    public static boolean isConnected(){
        if (con == null) return false;
        else return true;
    }

    public static void insert(String qry){
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static ResultSet getData(String qry){
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}
