package de.craftix.cores.utils;


import de.craftix.cores.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

public class Setup implements CommandExecutor, Listener {

    public static ArrayList<Arena> setupArenas = new ArrayList<>();

    public static void saveLobby(Location loc){
        MySQL.insert("DELETE FROM Spawns WHERE Type = 0");
        String x = "" + loc.getX();
        String y = "" + loc.getY();
        String z = "" + loc.getZ();
        String yaw = "" + loc.getYaw();
        String pitch = "" + loc.getPitch();
        String world = "" + loc.getWorld().getName();
        MySQL.insert("INSERT INTO Spawns (Type, X, Y, Z, YAW, PITCH, World) VALUES (0, \"" + x + "\", \"" + y + "\", \"" + z + "\", \"" + yaw + "\", \"" + pitch + "\", \"" + world + "\")");
    }

    public static void saveArena(Arena arena){
        MySQL.insert("DELETE FROM Arenas WHERE ID = " + arena.id);
        MySQL.insert("DELETE FROM Spawns WHERE ArenaID = " + arena.id);
        arena.id = getNextArenaID();
        String invMat = arena.invItem.getType().name();
        String name = arena.invItem.getItemMeta().getDisplayName();
        if (name == null) name = arena.invItem.getType().name().toLowerCase();
        name.replace('&', '§');
        name.replace('_', ' ');
        String invName = name;
        MySQL.insert("INSERT INTO Arenas (Name, ID, InvMat, InvName, InvDMG) VALUES (\"" + arena.name + "\", " + arena.id + ", \"" + invMat + "\", \"" + invName + "\", " + arena.invItem.getDurability() + ")");
        for (Arena.ArenaSpawn spawn : arena.spawns){
            Location loc = spawn.loc;
            String x = "" + loc.getX();
            String y = "" + loc.getY();
            String z = "" + loc.getZ();
            String yaw = "" + loc.getYaw();
            String pitch = "" + loc.getPitch();
            String world = "" + loc.getWorld().getName();
            MySQL.insert("INSERT INTO Spawns (Type, ArenaID, X, Y, Z, YAW, PITCH, World) VALUES (" + spawn.type + "," + arena.id + ", \"" + x + "\", \"" + y + "\", \"" + z + "\", \"" + yaw + "\", \"" + pitch + "\", \"" + world + "\")");
        }
    }

    private static int getNextArenaID(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas");
            ArrayList<Integer> ids = new ArrayList<>();
            while (rs.next()){
                ids.add(rs.getInt("ID"));
            }
            int id = 0;
            while (ids.contains(id)) id++;
            return id;
        }catch (Exception e) {}
        return -1;
    }

    private HashMap<Player, Arena> setupPlayer = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if ((sender instanceof Player) && sender.hasPermission("cores.setup")){
            Player p = (Player)sender;
            //setup <arena>
            if (command.getName().equals("setup") && args.length <= 1){
                if (setupPlayer.containsKey(p)){ //SaveArena
                    Arena arena = setupPlayer.get(p);
                    if (arena.spawns == null || arena.spawns.size() < 6) {
                        p.sendMessage("§cDas Setup wurde noch nicht abgeschlossen!");
                        return true;
                    }
                    if (arena != null) saveArena(arena);
                    setupPlayer.remove(p);
                    setupArenas.remove(arena);
                    playerInvs.remove(p);
                    p.getInventory().clear();
                    p.sendMessage("§aEinstellungen gespeichert!");
                }else { //giveInv
                    if (args.length == 1){
                        Arena arena = Arena.getArena(args[0]);
                        if (arena == null) for (Arena all : setupArenas) if (all.name.equals(args[0])) arena = all;
                        if (arena == null) {
                            p.sendMessage("§cDiese Arena existiert nicht!");
                            return true;
                        }
                        setupPlayer.put(p, arena);
                        giveSetupInv(p, 0);
                    }else {
                        giveSetupInv(p, 2);
                    }
                }
                return true;
            }
            //addarena <name>
            if (command.getName().equalsIgnoreCase("addarena") && args.length == 1){
                boolean contains = false;
                for (Arena all : setupArenas){
                    if (all.name.equals(args[0])) contains = true;
                }
                if (contains){
                    p.sendMessage("§cDiese Arena wird bereits bearbeitet!");
                    return true;
                }
                setupArenas.add(new Arena(args[0]));
                p.sendMessage("§aDie Arena wurde erstellt!");
                return true;
            }
            //delarena <name>
            if (command.getName().equals("delarena") && args.length == 1){
                Arena arena = null;
                for (Arena all : setupArenas){
                    if (all.name.equals(args[0])) arena = all;
                }
                if (arena == null){
                    arena = Arena.getArena(args[0]);
                }else{
                    setupArenas.remove(arena);
                    p.sendMessage("§aSetup abgebrochen!");
                    return true;
                }
                if (arena != null){
                    MySQL.insert("DELETE FROM Arenas WHERE ID = " + arena.id);
                    MySQL.insert("DELETE FROM Spawns WHERE ArenaID = " + arena.id);
                    p.sendMessage("§aArena gelöscht!");
                    return true;
                }
                p.sendMessage("§cDiese Arena existiert nicht!");
                return true;
            }
            //arenen
            if (command.getName().equals("arenen")){
                ArrayList<Arena> arenas = Arena.getAllArenas();
                if (arenas.size() == 0){
                    p.sendMessage("§cEs gibt noch keine Arenen!");
                    return true;
                }
                Inventory inv = Bukkit.createInventory(null, 9*3, "§aArena Liste");
                if (arenas.size() >= 9*3){
                    for (int i = 0; i < inv.getSize(); i++){
                        inv.setItem(i, arenas.get(i).invItem);
                    }
                }else {
                    for (int i = 0; i < arenas.size(); i++){
                        inv.setItem(i, arenas.get(i).invItem);
                    }
                }
                p.openInventory(inv);
                return true;
            }

            p.sendMessage("§cDu benutzt den Command falsch!");
        }else sender.sendMessage("§cDu hast keine Rechte, diesen Befehl zu benutzen!");
        return true;
    }


    private HashMap<Player, Inventory> playerInvs = new HashMap<>();

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (event.getClickedInventory() == null) return;
        if (event.getCurrentItem() == null) return;
        if (event.getClickedInventory().getTitle().equals("§aArena Liste")) event.setCancelled(true);
        if (event.getClickedInventory().getTitle().equals(invName)){
            Player p = (Player) event.getWhoClicked();
            Arena arena = setupPlayer.get(p);

            if (event.getSlot() == 13 && arena != null && event.getCurrentItem() != null){
                arena.invItem = event.getCursor();
                event.setCurrentItem(null);
                return;
            }else {
                if (event.getCurrentItem() == null) return;
                event.setCancelled(true);
                p.closeInventory();
            }

            if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aLobby")){
                saveLobby(p.getLocation());
            }
            else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aSpawn für Team 1")){
                for (Arena.ArenaSpawn spawn : arena.spawns) if (spawn.type == 1) arena.spawns.remove(spawn);
                arena.spawns.add(new Arena.ArenaSpawn(1, arena, p.getLocation()));
            }
            else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aSpawn für Team 2")){
                for (Arena.ArenaSpawn spawn : arena.spawns) if (spawn.type == 2) arena.spawns.remove(spawn);
                arena.spawns.add(new Arena.ArenaSpawn(2, arena, p.getLocation()));
            }
            else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aRechter Core für Team 1")){
                for (Arena.ArenaSpawn spawn : arena.spawns) if (spawn.type == 3) arena.spawns.remove(spawn);
                arena.spawns.add(new Arena.ArenaSpawn(3, arena, p.getLocation()));
            }
            else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aLinker Core für Team 1")){
                for (Arena.ArenaSpawn spawn : arena.spawns) if (spawn.type == 4) arena.spawns.remove(spawn);
                arena.spawns.add(new Arena.ArenaSpawn(4, arena, p.getLocation()));
            }
            else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aRechter Core für Team 2")){
                for (Arena.ArenaSpawn spawn : arena.spawns) if (spawn.type == 5) arena.spawns.remove(spawn);
                arena.spawns.add(new Arena.ArenaSpawn(5, arena, p.getLocation()));
            }
            else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aLinker Core für Team 2")){
                for (Arena.ArenaSpawn spawn : arena.spawns) if (spawn.type == 6) arena.spawns.remove(spawn);
                arena.spawns.add(new Arena.ArenaSpawn(6, arena, p.getLocation()));
            }
            ItemStack item = event.getCurrentItem();
            ItemMeta meta = item.getItemMeta();
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(meta);
            event.setCurrentItem(item);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (!setupPlayer.containsKey(event.getPlayer())) return;
        if (event.getItem().getItemMeta().getDisplayName().equals(invName)) giveSetupInv(event.getPlayer(), 1);
    }

    private final String invName = "§6Setup";
    public void giveSetupInv(Player p, int type){
        if (type == 0){ //Hotbar
            ItemStack setup = new ItemBuilder(Material.NETHER_STAR).setName(invName).create();
            p.getInventory().setItem(4, setup);
        }
        else if (type == 1){ //Inv
            if (playerInvs.containsKey(p)){
                p.openInventory(playerInvs.get(p));
                return;
            }

            Inventory inv = Bukkit.createInventory(null, 9*3, invName);

            for (int i = 0; i < inv.getSize(); i++){
                inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName("").create());
            }

            ItemStack team1 = new ItemBuilder(Material.ENDER_PEARL).setName("§aSpawn für Team 1").create();
            ItemStack team2 = new ItemBuilder(Material.ENDER_PEARL).setName("§aSpawn für Team 2").create();
            ItemStack core1team1 = new ItemBuilder(Material.BEACON).setName("§aRechter Core für Team 1").create();
            ItemStack core2team1 = new ItemBuilder(Material.BEACON).setName("§aLinker Core für Team 1").create();
            ItemStack core1team2 = new ItemBuilder(Material.BEACON).setName("§aRechter Core für Team 1").create();
            ItemStack core2team2 = new ItemBuilder(Material.BEACON).setName("§aLinker Core für Team 1").create();
            ItemStack replace = new ItemBuilder(Material.BARRIER).setName("§7Placeholder").setLore("§7Ersetzte dieses Item mit dem Item,", "§7was in der GUI angezeigt werden soll.").create();

            inv.setItem(9, core2team1);
            inv.setItem(10, team1);
            inv.setItem(11, core1team1);

            inv.setItem(15, core2team2);
            inv.setItem(16, team2);
            inv.setItem(17, core1team2);

            inv.setItem(13, replace);

            p.openInventory(inv);

            playerInvs.put(p, inv);
        }
        else if (type == 2){
            Inventory inv = Bukkit.createInventory(null, 9*3, invName);

            for (int i = 0; i < inv.getSize(); i++){
                inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName("").create());
            }
            ItemStack lobby = new ItemBuilder(Material.NETHER_STAR).setName("§aLobby").create();
            inv.setItem(13, lobby);
            p.openInventory(inv);
        }
    }
}
