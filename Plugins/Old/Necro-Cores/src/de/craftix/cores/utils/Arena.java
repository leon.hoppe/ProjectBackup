package de.craftix.cores.utils;

import de.craftix.cores.Main;
import de.craftix.cores.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Arena {
    public String name;
    public int id;
    public ItemStack invItem;
    public ArrayList<ArenaSpawn> spawns = new ArrayList<>();
    public Team teamRed;
    public Team teamBlue;

    public static Arena getArena(int id){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas WHERE ID = " + id);
            if (rs.next()){
                String name = rs.getString("Name");
                String invMat = rs.getString("InvMat");
                String invName = rs.getString("InvName");
                int dmg = rs.getInt("InvDMG");
                return new Arena(name, id, invMat, invName, dmg);
            }
        }catch (Exception e) { return null; }
        return null;
    }

    public static Arena getArena(String name){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas WHERE Name = \"" + name + "\"");
            if (rs.next()){
                int id = rs.getInt("ID");
                String invMat = rs.getString("InvMat");
                String invName = rs.getString("InvName");
                int dmg = rs.getInt("InvDMG");
                return new Arena(name, id, invMat, invName, dmg);
            }
        }catch (Exception e) { return null; }
        return null;
    }

    public static ArrayList<Arena> getAllArenas(){
        ArrayList<Arena> arenas = new ArrayList<>();
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas");
            while (rs.next()){
                String name = rs.getString("Name");
                int id = rs.getInt("ID");
                String invMat = rs.getString("InvMat");
                String invName = rs.getString("InvName");
                int dmg = rs.getInt("InvDMG");
                arenas.add(new Arena(name, id, invMat, invName, dmg));
            }
            return arenas;
        }catch (Exception e) { return null; }
    }

    public static ArrayList<ArenaSpawn> getArenaSpawns(Arena arena){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Spawns WHERE ArenaID = " + arena.id);
            ArrayList<ArenaSpawn> spawns = new ArrayList<>();
            while (rs.next()){
                int type = rs.getInt("Type");
                String x = rs.getString("X");
                String y = rs.getString("Y");
                String z = rs.getString("Z");
                String yaw = rs.getString("YAW");
                String pitch = rs.getString("PITCH");
                String world = rs.getString("World");
                spawns.add(new ArenaSpawn(type, arena, x, y, z, yaw, pitch, world).addToArena());
            }
        }catch (Exception e) { return null; }
        return null;
    }

    public static Location getSpawn(SpawnType type, Arena arena){
        if (type == SpawnType.LOBBY){
            try {
                ResultSet rs = MySQL.getData("SELECT * FROM Spawns WHERE Type = 0");
                if (rs.next()){
                    String x = rs.getString("X");
                    String y = rs.getString("Y");
                    String z = rs.getString("Z");
                    String yaw = rs.getString("YAW");
                    String pitch = rs.getString("PITCH");
                    String world = rs.getString("World");
                    return new Location(Bukkit.getWorld(world), Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z), Float.parseFloat(yaw), Float.parseFloat(pitch));
                }
            }catch (Exception e) { return null; }
        }
        if (type == SpawnType.TEAM1){ //Type = 1
            for (ArenaSpawn spawn : arena.spawns){
                if (spawn.type == 1) return spawn.loc;
            }
        }
        if (type == SpawnType.TEAM2){ //Type = 2
            for (ArenaSpawn spawn : arena.spawns){
                if (spawn.type == 2) return spawn.loc;
            }
        }
        if (type == SpawnType.CORE1TEAM1){ //Type = 3
            for (ArenaSpawn spawn : arena.spawns){
                if (spawn.type == 3) return spawn.loc;
            }
        }
        if (type == SpawnType.CORE1TEAM2){ //Type = 4
            for (ArenaSpawn spawn : arena.spawns){
                if (spawn.type == 4) return spawn.loc;
            }
        }
        if (type == SpawnType.CORE2TEAM1){ //Type = 5
            for (ArenaSpawn spawn : arena.spawns){
                if (spawn.type == 5) return spawn.loc;
            }
        }
        if (type == SpawnType.CORE2TEAM2){ //Type = 6
            for (ArenaSpawn spawn : arena.spawns){
                if (spawn.type == 6) return spawn.loc;
            }
        }
        return null;
    }

    public Arena(String name, int id, String invMat, String invName, int dmg){
        this.name = name;
        this.id = id;
        ItemStack item = new ItemStack(Material.getMaterial(invMat), 1, (short) dmg);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(new ArrayList<>());
        meta.setDisplayName(invName);
        item.setItemMeta(meta);
        this.invItem = item;
        this.spawns = getArenaSpawns(this);
        setTeams();
    }

    public Arena(String name){
        this.name = name;
        invItem = new ItemBuilder(Material.BARRIER).setName("§cKein Item vorhanden").create();
        setTeams();
    }

    private void setTeams(){
        teamRed = new Team("§cRot §7| §r", "§cRot");
        teamBlue = new Team("§9Blau §7| §r", "§9Blau");
    }

    public Team getWinnerTeam(){
        teamRed.players = Main.team1;
        teamBlue.players = Main.team2;
        if (teamRed.leftCore || teamRed.rightCore || Main.team1.size() > 0) return teamRed;
        if (teamBlue.leftCore || teamBlue.rightCore || Main.team2.size() > 0) return teamBlue;
        return null;
    }

    public Team getLooserTeam(){
        teamRed.players = Main.team1;
        teamBlue.players = Main.team2;
        if (!teamRed.leftCore && !teamRed.rightCore || Main.team1.size() == 0) return teamRed;
        if (!teamBlue.leftCore && !teamBlue.rightCore || Main.team2.size() == 0) return teamRed;
        return null;
    }


    public static class ArenaSpawn{
        public int type;
        public Arena arena;
        public Location loc;

        public ArenaSpawn(int type, Arena arena, String x, String y, String z, String yaw, String pitch, String world){
            this.type = type;
            this.arena = arena;
            this.loc = new Location(Bukkit.getWorld(world), Double.parseDouble(x), Double.parseDouble(x), Double.parseDouble(x), Float.parseFloat(yaw), Float.parseFloat(pitch));
        }

        public ArenaSpawn(int type, Arena arena, Location loc){
            this.type = type;
            this.arena = arena;
            this.loc = loc;
        }

        public ArenaSpawn addToArena() {
            arena.spawns.add(this);
            return this;
        }
    }

    public class Team {
        public String prefix;
        public String name;
        public boolean rightCore = true;
        public boolean leftCore = true;
        public ArrayList<Player> players;

        public Team(String prefix, String name){
            this.prefix = prefix;
            this.name = name;
        }
    }

    public static enum SpawnType {
        LOBBY,
        TEAM1,
        TEAM2,
        CORE1TEAM1,
        CORE2TEAM1,
        CORE1TEAM2,
        CORE2TEAM2
    }
}
