package de.craftix.cores.utils;

import de.craftix.cores.Main;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.annotation.processing.SupportedSourceVersion;

public class Lobbymanager implements Listener {

    public static void lobbyOptions(){
        if (lobby == null) return;
        World world = lobby.getWorld();
        world.setPVP(false);
        world.setAnimalSpawnLimit(0);
        world.setMonsterSpawnLimit(0);
        world.setDifficulty(Difficulty.PEACEFUL);
        world.setAmbientSpawnLimit(0);
        world.setTime(6000);
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setGameRuleValue("randomTickSpeed", "0");
        world.setGameRuleValue("announceAchievements", "false");
        world.setGameRuleValue("doMobSpawning", "false");
        world.setGameRuleValue("doFireTick", "false");
        for (Entity entity : world.getEntities()) if ((entity instanceof LivingEntity) && !(entity instanceof Player)) entity.remove();
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {event.setCancelled(true);}
    @EventHandler
    public void onBuild(BlockPlaceEvent event) {if (state != Gamestate.INGAME) event.setCancelled(true);}
    @EventHandler
    public void onBreak(BlockBreakEvent event) {if (state != Gamestate.INGAME) event.setCancelled(true);}
    @EventHandler
    public void onExplode(BlockExplodeEvent event) {event.setCancelled(true);}
    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {event.setCancelled(true);}
    @EventHandler
    public void onDamage(EntityDamageEvent event) {if (state != Gamestate.INGAME) event.setCancelled(true);}
    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {if (state != Gamestate.INGAME) event.setCancelled(true);}
    @EventHandler
    public void onPickUp(PlayerPickupItemEvent event) {if (state != Gamestate.INGAME) event.setCancelled(true);}
    @EventHandler
    public void onInvClick(InventoryClickEvent event) {if (state != Gamestate.INGAME && event.getClickedInventory().equals(event.getWhoClicked().getInventory())) event.setCancelled(true);}

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        event.setJoinMessage(null);
        if (state == Gamestate.RESTART) event.getPlayer().kickPlayer(null);
        Player p = event.getPlayer();
        if (state == Gamestate.INGAME){
            Main.spectator.add(p);
            p.setGameMode(GameMode.SPECTATOR);
            Location spawn = Arena.getSpawn(Arena.SpawnType.TEAM1, activeGame);
            p.teleport(spawn);
        }
        if (state == Gamestate.LOBBY){
            p.setGameMode(GameMode.SURVIVAL);
            p.teleport(lobby);
            p.setDisplayName("§7" + p.getDisplayName());
            giveInv(p);
            event.setJoinMessage(Main.prefix + "§aDer Spieler " + p.getDisplayName() + " §ahat das Spiel betreten");
            if (checkPlayerCount()) scheduleTimer(30, 0, "§aDas Spiel startet in §6TIME §aSekunden");
        }
    }
    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        event.setQuitMessage(null);
        Player p = event.getPlayer();
        if (state == Gamestate.LOBBY || Main.team1.contains(p) || Main.team2.contains(p)) event.setQuitMessage(Main.prefix + "§cDer Spieler " + p.getDisplayName() + " §chat das Spiel verlassen!");
        Main.team1.remove(p);
        Main.team2.remove(p);
        Main.spectator.remove(p);
        if (Main.team1.size() == 0 || Main.team2.size() == 0) initialiseEndSequence();
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (state != Gamestate.INGAME) event.setCancelled(true);
        if (state != Gamestate.LOBBY) return;
        switch (event.getItem().getType()){
            case BED:
                openBedGUI(event.getPlayer());
                break;
            case DIAMOND:
                scheduleTimer(30, 0, "§aDas Spiel startet in §6TIME §aSekunden");
                break;
        }
    }
    @EventHandler
    public void onBedInv(InventoryClickEvent event){
        if (!event.getClickedInventory().getTitle().equals("§cTeam wählen")) return;
        Player p = (Player) event.getWhoClicked();
        if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§cTeam Rot")){
            event.setCancelled(true);
            Main.team2.remove(p);
            Main.team1.remove(p);
            Main.team1.add(p);
            p.setDisplayName(activeGame.teamRed.prefix + p.getDisplayName());
            p.closeInventory();
        }else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§9Team Blau")){
            event.setCancelled(true);
            Main.team2.remove(p);
            Main.team1.remove(p);
            Main.team2.add(p);
            p.setDisplayName(activeGame.teamBlue.prefix + p.getDisplayName());
            p.closeInventory();
        }else event.setCancelled(true);
    }

    public static Gamestate state = Gamestate.LOBBY;
    public static Location lobby;
    public static Arena activeGame;
    public static final int teamSize = 4;

    private static void openBedGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, 9*3, "§cTeam wählen");
        for (int i = 0; i < inv.getSize(); i++){
            ItemStack glass = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").create();
            inv.setItem(i, glass);
        }
        ItemStack red = new ItemBuilder(Material.WOOL, 1, 14).setName("§cTeam Rot").create();
        ItemStack blue = new ItemBuilder(Material.WOOL, 1, 11).setName("§9Team Blau").create();
        inv.setItem(11, red);
        inv.setItem(15, blue);
        p.openInventory(inv);
    }

    private static boolean checkPlayerCount(){
        int minPlayer = ((teamSize * 2) / 4) * 2;
        if (lobby.getWorld().getPlayers().size() >= minPlayer) return true;
        else return false;
    }

    private static int timerID;
    private static void scheduleTimer(int seconds, int type, String msg){
        timerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            private int uptime = 0;
            @Override
            public void run() {
                int sec = seconds - uptime;
                if (sec == 0 && type == 0) startGame();
                if (sec == 0 && type == 1) stopGame();
                switch (sec){
                    case 30: case 15: case 10: case 5: case 4: case 3: case 2: case 1:
                        Bukkit.broadcastMessage(msg.replaceAll("TIME", String.valueOf(sec)));
                }
                uptime++;
            }
        }, 1, 20);
    }

    public static void initialiseEndSequence(){
        if (activeGame == null) {
            Bukkit.getServer().shutdown();
            return;
        }
        for (Player all : Bukkit.getOnlinePlayers()){
            all.teleport(lobby);
            all.setGameMode(GameMode.SURVIVAL);
            state = Gamestate.RESTART;
            //set Rewards + Stats
            Arena.Team winner = activeGame.getWinnerTeam();
            if (winner == null) Bukkit.getServer().shutdown();
            all.sendTitle("§aTeam " + winner.name + " §ahat gewonnen", null);
            all.playSound(all.getLocation(), Sound.LEVEL_UP, 100, 1);
            scheduleTimer(15, 1, "§aDer Server startet in §6TIME §aSekunden neu");
        }
    }

    public static void startGame(){
        Bukkit.getScheduler().cancelTask(timerID);
        state = Gamestate.INGAME;
        if (activeGame == null) activeGame = Arena.getArena("default");
        Gamemanager.handleStartGame(activeGame);
    }

    public static void stopGame(){
        Bukkit.getScheduler().cancelTask(timerID);
        for (Player all : Bukkit.getOnlinePlayers()) all.kickPlayer(null);
        Bukkit.getServer().shutdown();
    }

    private static void giveInv(Player p){
        p.getInventory().clear();
        p.getInventory().setHelmet(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setBoots(null);
        ItemStack team = new ItemBuilder(Material.BED).setName("§cTeam wählen").create();
        p.getInventory().setItem(0, team);
        if (p.hasPermission("cores.admin")){
            ItemStack start = new ItemBuilder(Material.DIAMOND).setName("§aSpiel Starten").create();
            p.getInventory().setItem(8, start);
        }
    }

    public enum Gamestate{
        LOBBY,
        INGAME,
        RESTART
    }
}
