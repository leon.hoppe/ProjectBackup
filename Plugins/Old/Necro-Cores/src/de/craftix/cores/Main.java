package de.craftix.cores;

import de.craftix.cores.utils.Arena;
import de.craftix.cores.utils.Lobbymanager;
import de.craftix.cores.utils.Setup;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    public static final String prefix = "§7[§bCores§7] §r";

    public static ArrayList<Player> team1 = new ArrayList<>(); //Rot
    public static ArrayList<Player> team2 = new ArrayList<>(); //Blau
    public static ArrayList<Player> spectator = new ArrayList<>();

    private static Main instance;


    @Override
    public void onEnable() {
        instance = this;
        MySQL.connect();
        createDB(MySQL.isConnected());
        Setup setup = new Setup();
        Lobbymanager.activeGame = Arena.getArena("default");

        Lobbymanager.lobby = Arena.getSpawn(Arena.SpawnType.LOBBY, null);
        Lobbymanager.lobbyOptions();

        getCommand("addarena").setExecutor(setup);
        getCommand("delarena").setExecutor(setup);
        getCommand("arenen").setExecutor(setup);
        getCommand("setup").setExecutor(setup);

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(setup, this);
        pm.registerEvents(new Lobbymanager(), this);
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
        for (Player p : Bukkit.getOnlinePlayers()) p.kickPlayer(null);
    }

    private void createDB(boolean isConnected){
        if (isConnected){
            //Spawns Types: 0=Lobby 1=Team1 2=Team2 3=CoreTeam1 4=CoreTeam2; ArenaID; Location
            MySQL.insert("CREATE TABLE IF NOT EXISTS Spawns (Type INT(10), ArenaID INT(100), X VARCHAR(100), Y VARCHAR(100), Z VARCHAR(100), YAW VARCHAR(100), PITCH VARCHAR(100), World VARCHAR(100))");
            //Arenas Name; ID; InvMat; InvName
            MySQL.insert("CREATE TABLE IF NOT EXISTS Arenas (Name VARCHAR(100), ID VARCHAR(100), InvMat VARCHAR(100), InvName VARCHAR(100), InvDMG INT(10))");
            //Stats UUID; Kills; Deaths; PlayedGames; DestroyedCores
            MySQL.insert("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(100), Kills INT(10), Deaths INT(10), PlayedGames INT(10), DestroyedCores INT(10))");
        }
    }

    public static Main getInstance() {
        return instance;
    }
}
