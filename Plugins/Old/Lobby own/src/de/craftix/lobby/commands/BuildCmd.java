package de.craftix.lobby.commands;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Messages;
import de.craftix.lobby.general.Permissions;
import de.craftix.lobby.utils.Spawn;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {

        if (sender instanceof Player){
            if (sender.hasPermission(Permissions.admin) ||sender.hasPermission(Permissions.builder)){
                Player p = (Player)sender;

                if (Main.buildMode.contains(p)){
                    Main.buildMode.remove(p);
                    p.setGameMode(GameMode.ADVENTURE);
                    Spawn.giveInventory(p);
                    p.sendMessage(Messages.prefix + "§aDer Baumodus wurde verlassen");
                }else {
                    Main.buildMode.add(p);
                    p.getInventory().clear();
                    p.setGameMode(GameMode.CREATIVE);
                    p.sendMessage(Messages.prefix + "§aDer Baumodus wurde betreten");
                }

            }else {
                sender.sendMessage(Messages.noPermission);
            }
        }else {
            sender.sendMessage(Messages.onlyPlayer);
        }

        return false;
    }
}
