package de.craftix.lobby.general;

public class Messages {

    public static final String prefix = "§7[§6Lobby§7] §r";
    public static final String noPermission = prefix + "§cHierzu hast du keine Rechte!";
    public static final String onlyPlayer = prefix + "§cDieser Befehl funktioniert nur als Spieler!";

}
