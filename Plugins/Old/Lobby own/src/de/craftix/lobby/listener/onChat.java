package de.craftix.lobby.listener;

import de.craftix.lobby.general.Permissions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class onChat implements Listener {

    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent event){
        Player p = event.getPlayer();
        if (p.hasPermission(Permissions.canChat)) return;
        p.sendMessage("§cNur VIP's dürfen den Chat in der Lobby benutzen!");
        event.setCancelled(true);
    }

}
