package de.craftix.lobby.listener;

import de.craftix.lobby.general.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class onBuild implements Listener {

    @EventHandler
    public void onBuildEvent(BlockPlaceEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onBreakEvent(BlockBreakEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        event.setCancelled(true);
    }

}
