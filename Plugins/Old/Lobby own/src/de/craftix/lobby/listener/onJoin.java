package de.craftix.lobby.listener;

import de.craftix.lobby.utils.ActionBar;
import de.craftix.lobby.utils.Spawn;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        event.setJoinMessage(null);
        Player p = event.getPlayer();
        p.setMaxHealth(20);
        p.setFoodLevel(25);
        p.setHealth(20);
        p.setGameMode(GameMode.ADVENTURE);
        Spawn.giveInventory(p);
        Spawn.teleport(p);
        ActionBar joinBar = new ActionBar("§aWillkommen §b§l" + p.getName() + "§r§a!");
        joinBar.send(p);
    }

}
