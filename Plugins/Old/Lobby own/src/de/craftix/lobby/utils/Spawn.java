package de.craftix.lobby.utils;

import de.craftix.lobby.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Spawn {

    public static void teleport(Player p){
        FileConfiguration config = Main.getPlugin().getConfig();

        if (!config.contains("Spawn.World")) return;

        World world = Bukkit.getWorld(config.getString("Spawn.World"));
        double x = config.getDouble("Spawn.X");
        double y = config.getDouble("Spawn.Y");
        double z = config.getDouble("Spawn.Z");
        float yaw = (float) config.getDouble("Spawn.Yaw");
        float pitch = (float) config.getDouble("Spawn.Pitch");

        Location spawn = new Location(world, x, y, z, yaw, pitch);

        p.teleport(spawn);
    }

    public static Location getLocation(){
        FileConfiguration config = Main.getPlugin().getConfig();

        if (!config.contains("Spawn.World")) return null;

        World world = Bukkit.getWorld(config.getString("Spawn.World"));
        double x = config.getDouble("Spawn.X");
        double y = config.getDouble("Spawn.Y");
        double z = config.getDouble("Spawn.Z");
        float yaw = (float) config.getDouble("Spawn.Yaw");
        float pitch = (float) config.getDouble("Spawn.Pitch");

        Location spawn = new Location(world, x, y, z, yaw, pitch);

        return spawn;
    }

    public static void giveInventory(Player p){
        p.getInventory().clear();

        ItemStack tele = new ItemStack(Material.COMPASS);
        ItemMeta teleMeta = tele.getItemMeta();
        teleMeta.setDisplayName("§6Teleporter §7(Rechtsklick)");
        tele.setItemMeta(teleMeta);

        ItemStack shop = new ItemStack(Material.EMERALD);
        ItemMeta shopMeta = shop.getItemMeta();
        shopMeta.setDisplayName("§6Shop §7(Rechtsklick)");
        shop.setItemMeta(shopMeta);

        ItemStack inv = new ItemStack(Material.CHEST);
        ItemMeta invMeta = inv.getItemMeta();
        invMeta.setDisplayName("§6Inventar §7(Rechtsklick)");
        inv.setItemMeta(invMeta);

        ItemStack hider = new ItemStack(Material.BLAZE_ROD);
        ItemMeta hiderMeta = hider.getItemMeta();
        hiderMeta.setDisplayName("§6Spieler verstecken §7(Rechtsklick)");
        hider.setItemMeta(hiderMeta);

        /*ItemStack profile = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta profileMeta = (SkullMeta) profile.getItemMeta();
        profileMeta.setDisplayName("§6Profil §7(Rechtsklick)");
        profileMeta.setOwner(p.getName());
        profile.setItemMeta(profileMeta);*/

        p.getInventory().setItem(4, tele);
        p.getInventory().setItem(1, shop);
        p.getInventory().setItem(0, inv);
        p.getInventory().setItem(7, hider);
        //p.getInventory().setItem(8, profile);
    }

}
