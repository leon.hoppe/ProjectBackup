package de.craftix.lobby.utils;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Navigator implements Listener {

    @EventHandler
    public void onKlick(PlayerInteractEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        if (p.getInventory().getHeldItemSlot() != 4) return;
        if (!(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
        openGUI(p);
    }

    private final String GUI_NAME = "§b§lTeleporter";
    private final int SIZE = 3*9;
    private void openGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_NAME);

        // Items
        ItemStack empty = CreateItem.normal(Material.STAINED_GLASS_PANE, 7, " ");
        ItemStack spawn = CreateItem.normal(Material.MAGMA_CREAM, "§aSpawn");
        ItemStack sb = CreateItem.normal(Material.GRASS, "§aSkyBlock");
        ItemStack bw = CreateItem.normal(Material.BED, "§aBedWars");
        ItemStack ovo = CreateItem.normal(Material.DIAMOND_SWORD, "§a1vs1");
        ItemStack training = CreateItem.normal(Material.ARMOR_STAND, "§aTraining");

        // Set Items
        inv.setItem(4, spawn);
        inv.setItem(10, sb);
        inv.setItem(12, bw);
        inv.setItem(14, ovo);
        inv.setItem(16, training);

        int count = 0;
        for (ItemStack all : inv){
            if (all == null) inv.setItem(count, empty);
            count++;
        }

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (event.getClickedInventory() == null) return;
        if (!event.getClickedInventory().getTitle().equals(GUI_NAME)) return;
        event.setCancelled(true);
        Player p = (Player) event.getWhoClicked();
        switch (event.getSlot()){
            case 4:
                Spawn.teleport(p);
                break;
            case 10:
                Server.warp("SkyBlock", p);
                break;
            case 12:
                Server.warp("BedWars", p);
                break;
            case 14:
                //TODO: Connect to 1vs1 Server
                break;
            case 16:
                Server.warp("Training", p);
        }
    }

}
