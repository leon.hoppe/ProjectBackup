package de.craftix.lobby.utils;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Hider implements Listener {

    private ArrayList<Player> hideAll = new ArrayList<>();
    private ArrayList<Player> hideVIP = new ArrayList<>();


    @EventHandler
    public void onClick(PlayerInteractEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        if (p.getInventory().getHeldItemSlot() != 7) return;
        if (!(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
        openGUI(p);
    }

    private final String GUI_NAME = "§b§lSpieler verstecken";
    private final int SIZE = 1*9;
    private void openGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_NAME);

        //Items
        ItemStack empty = CreateItem.normal(Material.STAINED_GLASS_PANE, 7, " ");
        ItemStack hideall = CreateItem.normal(Material.STAINED_GLASS_PANE, 14, "§cSpieler verstecken");
        ItemStack hidevip = CreateItem.normal(Material.STAINED_GLASS_PANE, 10, "§5Nur VIP's");
        ItemStack show = CreateItem.normal(Material.STAINED_GLASS_PANE, 5, "§aSpieler anzeigen");

        //Set Items
        inv.setItem(2, hideall);
        inv.setItem(4, hidevip);
        inv.setItem(6, show);

        int count = 0;
        for (ItemStack all : inv){
            if (all == null) inv.setItem(count, empty);
            count++;
        }

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick (InventoryClickEvent event){
        if (event.getClickedInventory() == null) return;
        if (!event.getClickedInventory().getTitle().equals(GUI_NAME)) return;
        event.setCancelled(true);
        Player p = (Player) event.getWhoClicked();
        if (event.getSlot() == 2) {
            //Hide All
            hideVIP.remove(p);
            hideAll.add(p);
            for (Player all : Bukkit.getOnlinePlayers()) {
                p.hidePlayer(all);
            }
        } else if (event.getSlot() == 4) {
            //Hide VIP
            hideAll.remove(p);
            hideVIP.add(p);
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission(Permissions.vip)) {
                    p.showPlayer(all);
                    continue;
                }
                p.hidePlayer(all);
            }
        } else if (event.getSlot() == 6) {
            //Show All
            hideAll.remove(p);
            hideVIP.remove(p);
            for (Player all : Bukkit.getOnlinePlayers()) {
                p.showPlayer(all);
            }
        }
        update(p);
    }

    @EventHandler
    public void onJoinEvent (PlayerJoinEvent event){
        Player p = event.getPlayer();
        for (Player all : hideAll){
            all.hidePlayer(p);
        }
        if (p.hasPermission(Permissions.vip)){
            for (Player all : hideVIP){
                all.showPlayer(p);
            }
        }
    }

    @EventHandler
    public void  onJoinSelf(PlayerJoinEvent event){
        Player p = event.getPlayer();
        if (hideAll.contains(p)){
            for (Player all : Bukkit.getOnlinePlayers()) {
                p.hidePlayer(all);
            }
        }else if (hideVIP.contains(p)){
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission(Permissions.vip)) {
                    p.showPlayer(all);
                    continue;
                }
                p.hidePlayer(all);
            }
        }else {
            for (Player all : Bukkit.getOnlinePlayers()) {
                p.showPlayer(all);
            }
        }
    }

    private void update(Player p){
        ActionBar ab;
        if (hideAll.contains(p)){
            ab = new ActionBar("§6Player Hider: §cAlle Spieler versteckt");
            ab.send(p);
        }else if (hideVIP.contains(p)){
            ab = new ActionBar("§6Player Hider: §5Nur VIP's sichtbar");
            ab.send(p);
        }else {
            ab = new ActionBar("§6Player Hider: §aAlle Spieler sichtbar");
            ab.send(p);
        }
    }

}
