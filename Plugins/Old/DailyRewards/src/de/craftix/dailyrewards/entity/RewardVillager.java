package de.craftix.dailyrewards.entity;

import de.craftix.dailyrewards.system.TimeControl;
import net.minecraft.server.v1_8_R3.Village;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

public class RewardVillager implements CommandExecutor, Listener {

    ItemStack collect;
    ItemStack collected;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player){
            if (sender.hasPermission("dailyrewards.setup")){
                Player p = (Player)sender;
                Villager v = (Villager) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.VILLAGER);
                v.setCustomName("§6DailyRewards");
                v.setCustomNameVisible(true);
                v.setCanPickupItems(false);
                v.addPotionEffects((Collection<PotionEffect>) new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 500));

            }else {
                sender.sendMessage("§4FICK DICH DU HAST KEINE RECHTE DU LAPPEN");
            }
        }
        return true;
    }

    @EventHandler
    public void onKlick(PlayerInteractEntityEvent event){
        if (!event.getRightClicked().getCustomName().equals("§6DailyRewards")) return;

        Inventory inv = Bukkit.createInventory(null, 9, "§6DailyRewards");
        collect = new ItemStack(Material.STORAGE_MINECART);
        collected = new ItemStack(Material.MINECART);

        ItemMeta colMeta = collect.getItemMeta();
        colMeta.setDisplayName("§aSammle deinen Daily Reward ein!");
        ArrayList<String> lore = new ArrayList<>();
        lore.add("§aReward: §6100 Coins");
        colMeta.setLore(lore);
        collect.setItemMeta(colMeta);

        ItemMeta coldMeta = collected.getItemMeta();
        coldMeta.setDisplayName("§cDu hast deine Daily Rewards heute schon abgeholt");
        collected.setItemMeta(coldMeta);

        Player p = event.getPlayer();

        if (TimeControl.check(p)){
            inv.setItem(4, collected);
        }else {
            inv.setItem(4, collect);
        }
        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (!event.getClickedInventory().getTitle().equals("§6DailyRewards")) return;
        if (event.getCurrentItem().equals(collect)){
            event.getClickedInventory().setItem(4, collected);
            TimeControl.hasCollected.add((Player) event.getWhoClicked());
            //100 Coins Geben
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event){
        if (!event.getEntity().getCustomName().equals("§6DailyRewards")) return;
        if (event.getDamager().hasPermission("dailyrewards.setup")){
            Player p = (Player)event.getDamager();
            if (p.getInventory().getItemInHand().getType().equals(Material.DIAMOND_SWORD)){
                Villager v = (Villager) event.getEntity();
                v.setHealth(0);
            }
        }
        event.setCancelled(true);
    }
}
