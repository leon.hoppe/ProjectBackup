package de.craftix.dailyrewards.general;

import de.craftix.dailyrewards.system.TimeControl;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    private static Main plugin;

    @Override
    public void onEnable() {
        plugin = this;
        FileConfiguration config = this.getConfig();
        TimeControl.hasCollected = (ArrayList<Player>) config.getList("HasCollected");
    }

    @Override
    public void onDisable() {
        FileConfiguration config = this.getConfig();
        config.set("HasCollected", TimeControl.hasCollected);
        this.saveConfig();
    }

    public static Main getPlugin(){
        return plugin;
    }

}
