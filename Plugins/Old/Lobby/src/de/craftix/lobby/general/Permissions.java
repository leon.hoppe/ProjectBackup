package de.craftix.lobby.general;

public class Permissions {

    public static final String admin = "lobby.admin";
    public static final String team = "lobby.team";
    public static final String builder = "lobby.builder";

    public static final String vip = "lobby.vip";
    public static final String canChat = "lobby.chat";

}
