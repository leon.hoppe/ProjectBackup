package de.craftix.lobby.general;

import de.craftix.lobby.commands.BuildCmd;
import de.craftix.lobby.commands.SetspawnCmd;
import de.craftix.lobby.commands.SpawnCmd;
import de.craftix.lobby.listener.*;
import de.craftix.lobby.utils.Hider;
import de.craftix.lobby.utils.Navigator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    private static Main plugin;

    public static ArrayList<Player> buildMode = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        regCommands();
        regEvents();

        System.out.println("Lobby enabled successfully");
    }

    @Override
    public void onDisable() {
        System.out.println("Lobby disabled successfully");
    }

    public static Main getPlugin(){
        return plugin;
    }

    private void regEvents(){
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new onJoin(), this);
        pm.registerEvents(new onQuit(), this);
        pm.registerEvents(new onBuild(), this);
        pm.registerEvents(new onDamage(), this);
        pm.registerEvents(new onWeather(), this);
        pm.registerEvents(new onChat(), this);
        pm.registerEvents(new Hider(), this);
        pm.registerEvents(new Navigator(), this);
        pm.registerEvents(new onDrop(), this);
    }

    private void regCommands(){
        getCommand("setspawn").setExecutor(new SetspawnCmd());
        getCommand("spawn").setExecutor(new SpawnCmd());
        getCommand("build").setExecutor(new BuildCmd());
    }
}
