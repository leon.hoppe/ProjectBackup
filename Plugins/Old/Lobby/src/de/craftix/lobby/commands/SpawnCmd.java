package de.craftix.lobby.commands;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Messages;
import de.craftix.lobby.utils.Spawn;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class SpawnCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (sender instanceof Player){
            Player p = (Player)sender;
            Spawn.teleport(p);
        }else {
            sender.sendMessage(Messages.onlyPlayer);
        }

        return false;
    }
}
