package de.craftix.lobby.commands;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Messages;
import de.craftix.lobby.general.Permissions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class SetspawnCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player){
            if (sender.hasPermission(Permissions.admin)){
                Player p = (Player)sender;
                FileConfiguration config = Main.getPlugin().getConfig();

                config.set("Spawn.World", p.getLocation().getWorld().getName());
                config.set("Spawn.X", p.getLocation().getBlockX() + 0.5);
                config.set("Spawn.Y", p.getLocation().getBlockY() + 2);
                config.set("Spawn.Z", p.getLocation().getBlockZ() + 0.5);
                config.set("Spawn.Yaw", p.getLocation().getYaw());
                config.set("Spawn.Pitch", p.getLocation().getPitch());

                Main.getPlugin().saveConfig();

                p.sendMessage(Messages.prefix + "§aSpawnpunkt wurde gesetzt!");
            }else {
                sender.sendMessage(Messages.noPermission);
            }
        }else {
            sender.sendMessage(Messages.onlyPlayer);
        }

        return true;
    }
}
