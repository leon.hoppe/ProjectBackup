package de.craftix.lobby.utils;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Messages;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class Server {

    public static void connect(String server, Player p){
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(b);

            out.writeUTF("Connect");
            out.writeUTF(server);

            p.sendMessage("§aVerbinde zu " + server + "...");

            p.sendPluginMessage(Main.getPlugin(), "BungeeCord", b.toByteArray());
        } catch (Exception e){
            p.sendMessage(Messages.prefix + "§cEin Fehler ist aufgetreten!");
        }

    }

}
