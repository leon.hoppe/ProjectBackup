package de.craftix.lobby.utils;

import de.craftix.lobby.general.Main;
import de.craftix.lobby.general.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.DataOutputStream;

public class Navigator implements Listener {

    @EventHandler
    public void onKlick(PlayerInteractEvent event){
        Player p = event.getPlayer();
        if (Main.buildMode.contains(p)) return;
        if (p.getInventory().getHeldItemSlot() != 4) return;
        if (!(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
        openGUI(p);
    }

    private final String GUI_NAME = "§b§lTeleporter";
    private final int SIZE = 3*9;
    private void openGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_NAME);

        // Items
        ItemStack empty = CreateItem.normal(Material.STAINED_GLASS_PANE, 7, " ");
        ItemStack spawn = CreateItem.normal(Material.MAGMA_CREAM, "§aSpawn");
        ItemStack skyfarms = CreateItem.normal(Material.GRASS, "§aSkyFarms");

        // Set Items
        inv.setItem(4, spawn);
        inv.setItem(10, skyfarms);

        int count = 0;
        for (ItemStack all : inv){
            if (all == null) inv.setItem(count, empty);
            count++;
        }

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (event.getClickedInventory() == null) return;
        if (!event.getClickedInventory().getTitle().equals(GUI_NAME)) return;
        event.setCancelled(true);
        Player p = (Player) event.getWhoClicked();
        switch (event.getSlot()){
            case 4:
                Spawn.teleport(p);
                break;
            case 10:
                Server.connect("skyfarms", p);
                break;
        }
    }

}
