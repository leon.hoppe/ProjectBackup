package de.craftix.lobby.listener;

import de.craftix.lobby.utils.Spawn;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        event.setJoinMessage(null);
        Player p = event.getPlayer();
        p.setMaxHealth(6);
        p.setFoodLevel(25);
        p.setHealth(6);
        p.setGameMode(GameMode.ADVENTURE);
        Spawn.giveInventory(p);
        Spawn.teleport(p);
    }

}
