package de.craftix.lobby.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class onDamage implements Listener {

    @EventHandler
    public void onDamageEvent(EntityDamageEvent event){
        if (!(event.getEntity() instanceof Player)) return;
        event.setCancelled(true);
        Player p = (Player)event.getEntity();
        p.setHealth(20);
    }

    @EventHandler
    public void onHungerEvent(FoodLevelChangeEvent event){
        if (!(event.getEntity() instanceof Player)) return;
        Player p = (Player)event.getEntity();
        event.setCancelled(true);
        p.setFoodLevel(25);
    }

}
