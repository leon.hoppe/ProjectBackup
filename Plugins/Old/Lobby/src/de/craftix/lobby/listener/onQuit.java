package de.craftix.lobby.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class onQuit implements Listener {

    @EventHandler
    private void onQuitEvent(PlayerQuitEvent event){
        event.setQuitMessage(null);
    }

}
