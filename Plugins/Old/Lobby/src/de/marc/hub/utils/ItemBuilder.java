package de.marc.hub.utils;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ItemBuilder {

    public Material material;
    public String name;
    public int amount = 1;
    public short damage = 0;
    public HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    public ArrayList<String> lore = new ArrayList<>();
    public boolean showEnchantments = true;
    public boolean isSkull = false;
    public OfflinePlayer player;

    public ItemBuilder(Material mat){
        material = mat;
    }

    public ItemBuilder(Material mat, int amount){
        material = mat;
        this.amount = amount;
    }

    public ItemBuilder(Material mat, int amount, int damage){
        material = mat;
        this.amount = amount;
        this.damage = (short) damage;
    }

    public ItemBuilder(OfflinePlayer player) {
        this.player = player;
        isSkull = true;
    }

    public ItemBuilder(OfflinePlayer player, int amount) {
        this.player = player;
        this.amount = amount;
        isSkull = true;
    }

    public ItemBuilder setName(String name){
        this.name = name;
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment e, int level){
        enchantments.put(e, level);
        return this;
    }

    public ItemBuilder showEnchantments(boolean value) {
        showEnchantments = value;
        return this;
    }

    public ItemBuilder setLore(String... lore){
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    public ItemBuilder setGlowing(boolean value) {
        if (value) {
            enchantments.put(Enchantment.DURABILITY, 1);
            showEnchantments = false;
        }else {
            enchantments.clear();
            showEnchantments = true;
        }
        return this;
    }

    public ItemStack build(){
        ItemStack item;
        if (!isSkull) {
            item = new ItemStack(material, amount, damage);
            ItemMeta meta = item.getItemMeta();
            for (Enchantment e : enchantments.keySet()){
                meta.addEnchant(e, enchantments.get(e), true);
            }
            meta.setLore(lore);
            meta.setDisplayName(name);
            if (!showEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(meta);
        }else {
            item = new ItemStack(Material.SKULL_ITEM, amount, (short) 3);
            SkullMeta meta = (SkullMeta) item.getItemMeta();
            meta.setOwner(player.getName());
            for (Enchantment e : enchantments.keySet()){
                meta.addEnchant(e, enchantments.get(e), true);
            }
            meta.setLore(lore);
            meta.setDisplayName(name);
            if (!showEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(meta);
        }
        return item;
    }

}
