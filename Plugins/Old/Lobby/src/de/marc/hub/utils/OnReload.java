package de.marc.hub.utils;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public final class OnReload {

    public static void onReloadKick(){
        if (Bukkit.getOnlinePlayers().size() == 0) return;

        for (Player p : Bukkit.getOnlinePlayers()) {
            p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 1, 1);
            p.kickPlayer("§c§lPrincepDE \n \n §r§cDer Server wird neu gestartet \n §r§7Versuche dich neu zu verbinden.");
        }
    }

}
