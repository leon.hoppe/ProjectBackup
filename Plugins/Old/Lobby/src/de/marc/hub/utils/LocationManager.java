package de.marc.hub.utils;

import de.marc.hub.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.io.IOException;

public class LocationManager {

    public void save() {
        try {
            Main.cfg.save(Main.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLocation(Location location) {
        Main.cfg.set("Location.Spawn.world", location.getWorld().getName());
        Main.cfg.set("Location.Spawn.x", location.getX());
        Main.cfg.set("Location.Spawn.y", location.getY());
        Main.cfg.set("Location.Spawn.z", location.getZ());
        Main.cfg.set("Location.Spawn.yaw", location.getYaw());
        Main.cfg.set("Location.Spawn.pitch", location.getPitch());
        save();
    }

    public void setDeathBoarder(Player player) {
        Main.cfg.set("Location.Death.border", player.getLocation().getY());
        save();
    }

    public Double getDeathBoarder() {
        return Main.cfg.getDouble("Location.Death.border");
    }

    public Location getLocation() {
        Location location;
        World w = Bukkit.getWorld(Main.cfg.getString("Location.Spawn.world"));
        double x = Main.cfg.getDouble("Location.Spawn.x");
        double y = Main.cfg.getDouble("Location.Spawn.y");
        double z = Main.cfg.getDouble("Location.Spawn.z");
        location = new Location(w, x, y, z);
        location.setYaw(Main.cfg.getInt("Location.Spawn.yaw"));
        location.setPitch(Main.cfg.getInt("Location.Spawn.pitch"));
        return location;
    }



    public void setWarp(String name, Location location) {
        Main.cfg.set("Warp." + name + ".world", location.getWorld().getName());
        Main.cfg.set("Warp." + name + ".x", location.getX());
        Main.cfg.set("Warp." + name + ".y", location.getY());
        Main.cfg.set("Warp." + name + ".z", location.getZ());
        Main.cfg.set("Warp." + name + ".yaw", location.getYaw());
        Main.cfg.set("Warp." + name + ".pitch", location.getPitch());
        save();
    }

    public void delWarp(String name) {
        Main.cfg.set("Warp." + name + ".world", null);
        Main.cfg.set("Warp." + name + ".x", null);
        Main.cfg.set("Warp." + name + ".y", null);
        Main.cfg.set("Warp." + name + ".z", null);
        Main.cfg.set("Warp." + name + ".yaw", null);
        Main.cfg.set("Warp." + name + ".pitch", null);
        save();
    }

    public Location getWarp(String name) {
        Location location;
        World w = Bukkit.getWorld(Main.cfg.getString("Warp." + name + ".world"));
        double x = Main.cfg.getDouble("Warp." + name + ".x");
        double y = Main.cfg.getDouble("Warp." + name + ".y");
        double z = Main.cfg.getDouble("Warp." + name + ".z");
        location = new Location(w, x, y, z);
        location.setYaw(Main.cfg.getInt("Warp." + name + ".yaw"));
        location.setPitch(Main.cfg.getInt("Warp." + name + ".pitch"));
        return location;
    }

}
