package de.marc.hub.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;

public class Hider {
    public static final String vipPermission = "princep.vip";
    private interface Executable { void execute(Player p, Player all); }

    enum State {
        HIDE_ALL(0, Material.SULPHUR, "Alle verstecken", (p, all) -> p.hidePlayer(all)),
        HIDE_VIP(1, Material.REDSTONE, "Nur VIPs", (p, all) -> { if (all.hasPermission(vipPermission)) p.showPlayer(all); else p.hidePlayer(all); }),
        SHOW_ALL(2, Material.GLOWSTONE_DUST, "Alle ansehen", (p, all) -> p.showPlayer(all));

        private final int id;
        private final Material material;
        private final String name;
        private final Executable exe;

        State(int id, Material material, String name, Executable exe) {
            this.material = material;
            this.name = name;
            this.id = id;
            this.exe = exe;
        }

        public int getID() { return id; }
        public ItemStack getItem() {
            ItemStack item = new ItemStack(material);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(name);
            item.setItemMeta(meta);
            return item;
        }
        public void manageHiding(Player p) { for (Player all : Bukkit.getOnlinePlayers()) exe.execute(p, all); }

        public static State getByID(int id) {
            for (State state : values())
                if (state.getID() == id) return state;
            return null;
        }
    }

    public static void initialise() {
        if (!MySQL.isConnected()) throw new NullPointerException("MySQL not Connected");
        MySQL.insert("CREATE TABLE IF NOT EXISTS Hider (UUID VARCHAR(100), ID INT(10))");
    }

    public static State getState(Player p) {
        try {
            ResultSet rs = MySQL.getData("SELECT ID FROM Hider WHERE UUID = \"" + p.getUniqueId() + "\"");
            if (rs == null || !rs.next()) {
                setState(p, State.SHOW_ALL);
                return State.SHOW_ALL;
            }
            int id = rs.getInt("ID");
            return State.getByID(id);
        }catch (Exception e) { e.printStackTrace(); }
        return State.SHOW_ALL;
    }

    public static void setState(Player p, State state) {
        MySQL.insert("DELETE FROM Hider WHERE UUID = \"" + p.getUniqueId() + "\"");
        MySQL.insert("INSERT INTO HIDER (UUID, ID) VALUES (\"" + p.getUniqueId() + "\", " + state.getID() + ")");
    }
}
