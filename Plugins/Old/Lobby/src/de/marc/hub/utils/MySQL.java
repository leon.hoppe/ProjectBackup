package de.marc.hub.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class MySQL {

    public static final String server = "localhost";
    public static final Integer port = 3306;
    public static final String database = "Cosmetics";
    public static final String username = "plugin";
    public static final String password = "mYwCh3iXkRlZzj0K";
    private static Connection con = null;

    public static void connect() {
        String conString = "jdbc:mysql://" + server + ":" + port + "/" + database;
        try {
            con = DriverManager.getConnection(conString, username, password);
        }catch (Exception e) { e.printStackTrace(); }
    }

    public static void disconnect() {
        if (!isConnected()) return;
        try {
            con.close();
            con = null;
        }catch (Exception e) { e.printStackTrace(); }
    }

    public static boolean isConnected() {
        return (con != null);
    }

    public static void insert(String qry) {
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (Exception e) { e.printStackTrace(); }
    }

    public static ResultSet getData(String qry) {
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

}
