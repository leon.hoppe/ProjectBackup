package de.marc.hub.commands;

import de.marc.hub.cosmetics.CosmeticManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        CosmeticManager.openInventory((Player) commandSender);
        return false;
    }
}
