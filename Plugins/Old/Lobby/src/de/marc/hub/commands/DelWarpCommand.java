package de.marc.hub.commands;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelWarpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("hub.admin.warp.del")) {
                if (args.length == 1) {

                    Main.locationManager.delWarp(args[0]);
                    player.sendMessage(Title.prefix + "§7Du hast den §e" + args[0] + " §7Punkt gelöscht.");

                } else player.sendMessage(Title.prefix + "§7Benutzt §e/delWarp <Name>§7!");
            }
        }

        return false;
    }
}
