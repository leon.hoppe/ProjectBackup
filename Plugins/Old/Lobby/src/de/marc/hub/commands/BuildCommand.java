package de.marc.hub.commands;

import de.marc.hub.gui.PlayerInvGUI;
import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("hub.build")) {
                if (args.length == 0) {

                    if (Main.buildMode.contains(player)) {
                        Main.buildMode.remove(player);
                        player.setGameMode(GameMode.SURVIVAL);
                        PlayerInvGUI.addPlayerInv(player);
                        player.sendMessage(Title.prefix + "§7Du bist nicht mehr im Builder Modus.");
                    } else {
                        Main.buildMode.add(player);
                        player.setGameMode(GameMode.CREATIVE);
                        player.getInventory().clear();
                        player.sendMessage(Title.prefix + "§7Du bist nun im Builder Modus.");
                    }

                }else if (args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        if (player.hasPermission("hub.build.other")) {

                            if (Main.buildMode.contains(target)) {
                                Main.buildMode.remove(target);
                                target.setGameMode(GameMode.SURVIVAL);
                                PlayerInvGUI.addPlayerInv(target);
                                player.sendMessage(Title.prefix + "§7Du hast §e" + target.getDisplayName() + " §7aus den Builder Modus entfernt.");
                                target.sendMessage(Title.prefix + "§7Du bist nicht mehr im Builder Modus.");
                            } else {
                                Main.buildMode.add(target);
                                target.setGameMode(GameMode.CREATIVE);
                                target.getInventory().clear();
                                player.sendMessage(Title.prefix + "§7Du hast §e" + target.getDisplayName() + " §7in den Builder Modus gesetzt.");
                                target.sendMessage(Title.prefix + "§7Du bist nun im Builder Modus.");
                            }

                        }
                    } else
                        player.sendMessage(Title.prefix + "§e" + args[0] + " §7ist nicht online.");
                }



            } else
                player.sendMessage(Title.noPerm);
        }

        return false;
    }
}
