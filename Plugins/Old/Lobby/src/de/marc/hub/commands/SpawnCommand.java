package de.marc.hub.commands;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 0) {

                Location l = null;

                try {
                    l = Main.locationManager.getLocation();
                } catch (Exception ignored) { }


                if (!(l == null)) {

                    player.teleport(Main.locationManager.getLocation());

                    //Sound
                    player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 2);

                } else {
                    //Sound
                    player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);

                    //msg
                    player.sendMessage("§8»");
                    player.sendMessage(Title.prefix + "§7Der Spawn wurde noch nicht gesetzt.");
                    player.sendMessage(Title.prefix + "§7Bitte wende dich an einen Serveradministrator.");
                    player.sendMessage("§8»");
                }

            } else if (args.length == 1) {

                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    if (player.hasPermission("hub.spawn.other")) {

                        Location l = null;

                        try {
                            l = Main.locationManager.getLocation();
                        } catch (Exception ignored) { }


                        if (!(l == null)) {

                            target.teleport(Main.locationManager.getLocation());

                            //Sound
                            target.playSound(target.getLocation(), Sound.LEVEL_UP, 1, 2);

                        } else {
                            //Sound
                            player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);

                            //msg
                            player.sendMessage("§8»");
                            player.sendMessage(Title.prefix + "§7Der Spawn wurde noch nicht gesetzt.");
                            player.sendMessage(Title.prefix + "§7Bitte wende dich an einen Serveradministrator.");
                            player.sendMessage("§8»");
                        }

                    }
                } else player.sendMessage(Title.prefix + "§e" + args[0] + " §7ist nicht online.");

            } else player.sendMessage(Title.noPerm);
        }

        return false;
    }
}
