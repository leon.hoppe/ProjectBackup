package de.marc.hub.commands;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLocationCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player player = (Player) sender;
        if(player.hasPermission("hub.admin.setLoc")) {
            if(args.length == 1) {
                if(args[0].equalsIgnoreCase("Spawn")) {
                    Main.locationManager.setLocation(player.getLocation());
                    player.sendMessage(Title.prefix + "§7Du hast den §eSpawn §7gesetzt!");

                } else if(args[0].equalsIgnoreCase("Death")) {
                    Main.locationManager.setDeathBoarder(player);
                    player.sendMessage(Title.prefix + "§7Du hast die §cDeathBorder §7gesetzt!");
                } else
                    player.sendMessage(Title.noPerm);
            } else
                player.sendMessage(Title.noPerm);
        }

        return false;
    }
}
