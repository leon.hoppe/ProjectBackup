package de.marc.hub.commands;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("hub.admin.fly")) {
                if (args.length == 0) {

                    if (Main.flyMode.contains(player)) {
                        Main.flyMode.remove(player);
                        player.sendMessage(Title.prefix + "§7Du kannst nicht mehr fliegen.");

                        player.setAllowFlight(false);
                    } else {
                        Main.flyMode.add(player);
                        player.sendMessage(Title.prefix + "§7Du kannst nun fliegen.");

                        player.setAllowFlight(true);
                    }

                } else if (args.length == 1) {

                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        if (player.hasPermission("hub.admin.fly.other")) {

                            if (Main.flyMode.contains(target)) {
                                Main.flyMode.remove(target);
                                target.sendMessage(Title.prefix + "§7Du kannst nicht mehr fliegen.");
                                player.sendMessage(Title.prefix + target.getDisplayName() + "§7kann nicht mehr fliegen.");

                                target.setAllowFlight(false);
                            } else {
                                Main.flyMode.add(target);
                                target.sendMessage(Title.prefix + "§7Du kannst nun fliegen.");
                                player.sendMessage(Title.prefix + target.getDisplayName() + "§7kann nun fliegen.");

                                target.setAllowFlight(true);
                            }

                        }
                    } else player.sendMessage(Title.prefix + "§e" + args[0] + " §7ist nicht online.");
                } else player.sendMessage(Title.noPerm);
            } else player.sendMessage(Title.noPerm);
        }

        return false;
    }
}
