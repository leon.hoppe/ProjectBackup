package de.marc.hub.commands;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetWarpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("hub.admin.warp.set")) {
                if (args.length == 1) {

                    Main.locationManager.setWarp(args[0], player.getLocation());
                    player.sendMessage(Title.prefix + "§7Du hast den §e" + args[0] + " §7Punkt gesetzt.");

                } else player.sendMessage(Title.prefix + "§7Benutzt §e/setWarp <Name>§7!");
            }
        }

        return false;
    }
}
