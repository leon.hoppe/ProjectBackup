package de.marc.hub.commands;

import de.marc.hub.utils.Title;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameModeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("hub.admin.gamemode")) {
                if (args.length == 1) {

                    if (args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s")) {
                        player.setGameMode(GameMode.SURVIVAL);
                        player.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 0");
                    } else if (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c")) {
                        player.setGameMode(GameMode.CREATIVE);
                        player.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 1");
                    } else if (args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a")) {
                        player.setGameMode(GameMode.ADVENTURE);
                        player.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 2");
                    } else if (args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("spec")) {
                        player.setGameMode(GameMode.SPECTATOR);
                        player.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 3");
                    } else player.sendMessage(Title.prefix + "§7Benutze §e/gm <0-3>");

                } else if (args.length == 2) {
                    if (Bukkit.getPlayer(args[1]) != null) {

                        Player target = Bukkit.getPlayer(args[1]);
                        if (args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s")) {
                            target.setGameMode(GameMode.SURVIVAL);
                            target.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 0");
                            player.sendMessage(Title.prefix + "§7Du hast §e" + target.getName() + " §7in den Gamemode 0 gesetzt");
                        } else if (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c")) {
                            target.setGameMode(GameMode.CREATIVE);
                            target.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 1");
                            player.sendMessage(Title.prefix + "§7Du hast §e" + target.getName() + " §7in den Gamemode 1 gesetzt");
                        } else if (args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a")) {
                            target.setGameMode(GameMode.ADVENTURE);
                            target.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 2");
                            player.sendMessage(Title.prefix + "§7Du hast §e" + target.getName() + " §7in den Gamemode 2 gesetzt");
                        } else if (args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("spec")) {
                            target.setGameMode(GameMode.SPECTATOR);
                            target.sendMessage(Title.prefix + "§7Du bist nun im Gamemode 3");
                            player.sendMessage(Title.prefix + "§7Du hast §e" + target.getName() + " §7in den Gamemode 3 gesetzt");
                        } else player.sendMessage(Title.prefix + "§7Benutze §e/gm <0-3>");

                    } else player.sendMessage(Title.prefix + "§e" + args[1] + " §7ist nicht online!");
                }
            } else player.sendMessage(Title.noPerm);
        }

        return false;
    }
}
