package de.marc.hub.banner;

import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;

public enum Banners {

    LIGHTNING(new BannerBuilder().setName("§eLightning").setBaseColor(DyeColor.YELLOW).setPatterns(
            new Pattern(DyeColor.RED, PatternType.SQUARE_TOP_LEFT),
            new Pattern(DyeColor.RED, PatternType.SQUARE_BOTTOM_RIGHT),
            new Pattern(DyeColor.RED, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.RED, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.RED, PatternType.TRIANGLES_TOP),
            new Pattern(DyeColor.RED, PatternType.TRIANGLES_BOTTOM)
    ), 0, 1),
    SKULL(new BannerBuilder().setName("§8Dark Skull").setBaseColor(DyeColor.LIGHT_BLUE).setPatterns(
            new Pattern(DyeColor.CYAN, PatternType.BORDER),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT_UP),
            new Pattern(DyeColor.BLACK, PatternType.SKULL)
    ), 1, 1),
    MOON(new BannerBuilder().setName("§fMoon").setBaseColor(DyeColor.BLUE).setPatterns(
            new Pattern(DyeColor.SILVER, PatternType.MOJANG),
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.GRADIENT_UP),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLES_BOTTOM),
            new Pattern(DyeColor.WHITE, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.SQUARE_BOTTOM_LEFT)
    ), 2, 1),
    PORTAL(new BannerBuilder().setName("§5Portal").setBaseColor(DyeColor.PURPLE).setPatterns(
            new Pattern(DyeColor.MAGENTA, PatternType.STRIPE_SMALL),
            new Pattern(DyeColor.PURPLE, PatternType.BRICKS),
            new Pattern(DyeColor.MAGENTA, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.BLACK, PatternType.BORDER)
    ), 3, 1),
    SMILEY(new BannerBuilder().setName("§eSmiley").setBaseColor(DyeColor.WHITE).setPatterns(
            new Pattern(DyeColor.YELLOW, PatternType.STRAIGHT_CROSS),
            new Pattern(DyeColor.YELLOW, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.YELLOW, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.MOJANG),
            new Pattern(DyeColor.YELLOW, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.YELLOW, PatternType.TRIANGLE_TOP)
    ), 4, 1),
    APPLE(new BannerBuilder().setName("§fApple").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.WHITE, PatternType.MOJANG),
            new Pattern(DyeColor.WHITE, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.MOJANG),
            new Pattern(DyeColor.BLACK, PatternType.MOJANG),
            new Pattern(DyeColor.BLACK, PatternType.MOJANG),
            new Pattern(DyeColor.BLACK, PatternType.MOJANG)
    ), 5, 1),
    BATTERY(new BannerBuilder().setName("§aBattery").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.WHITE, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.RED, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM)
    ), 6, 1),
    UFO(new BannerBuilder().setName("§9UFO").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.YELLOW, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.SILVER, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.CYAN, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT_UP),
            new Pattern(DyeColor.BLACK, PatternType.BORDER)
    ), 6, 1),
    EARTH(new BannerBuilder().setName("§2Earth").setBaseColor(DyeColor.YELLOW).setPatterns(
            new Pattern(DyeColor.GREEN, PatternType.RHOMBUS_MIDDLE),
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.MOJANG),
            new Pattern(DyeColor.BLACK, PatternType.BORDER),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT_UP)
    ), 7, 1),


    PINEAPPLE(new BannerBuilder().setName("§6Pineapple").setBaseColor(DyeColor.ORANGE).setPatterns(
            new Pattern(DyeColor.YELLOW, PatternType.BRICKS),
            new Pattern(DyeColor.CYAN, PatternType.HALF_HORIZONTAL),
            new Pattern(DyeColor.LIME, PatternType.FLOWER),
            new Pattern(DyeColor.CYAN, PatternType.BORDER),
            new Pattern(DyeColor.LIME, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.CYAN, PatternType.CURLY_BORDER)
    ), 8, 2),
    SHEEP(new BannerBuilder().setName("§fSheep").setBaseColor(DyeColor.LIGHT_BLUE).setPatterns(
            new Pattern(DyeColor.LIME, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.WHITE, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.CREEPER),
            new Pattern(DyeColor.WHITE, PatternType.FLOWER)
    ), 9, 2),
    FIRE(new BannerBuilder().setName("§cFire").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.ORANGE, PatternType.MOJANG),
            new Pattern(DyeColor.BLACK, PatternType.BRICKS),
            new Pattern(DyeColor.ORANGE, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.RHOMBUS_MIDDLE),
            new Pattern(DyeColor.ORANGE, PatternType.GRADIENT_UP),
            new Pattern(DyeColor.YELLOW, PatternType.TRIANGLES_BOTTOM)
    ), 10, 2),
    SKULL_MASK(new BannerBuilder().setName("§7Mask").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.WHITE, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_CENTER),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.WHITE, PatternType.CREEPER),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT)
    ), 11, 2),
    ZOMBIE(new BannerBuilder().setName("§2Zombie").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.GREEN, PatternType.SKULL),
            new Pattern(DyeColor.BLUE, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.CREEPER),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.GRADIENT)
    ), 12, 2),
    NUKE(new BannerBuilder().setName("§eNuke").setBaseColor(DyeColor.YELLOW).setPatterns(
            new Pattern(DyeColor.BLACK, PatternType.HALF_HORIZONTAL),
            new Pattern(DyeColor.YELLOW, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.CIRCLE_MIDDLE)
    ), 13, 2),
    CHRISTMAS(new BannerBuilder().setName("§cCHRIST§eMAS").setBaseColor(DyeColor.LIGHT_BLUE).setPatterns(
            new Pattern(DyeColor.YELLOW, PatternType.CREEPER),
            new Pattern(DyeColor.RED, PatternType.SKULL),
            new Pattern(DyeColor.GREEN, PatternType.FLOWER),
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.CIRCLE_MIDDLE)
    ), 14, 2),
    GREEN_SKULL(new BannerBuilder().setName("§aGreen Skull").setBaseColor(DyeColor.LIME).setPatterns(
            new Pattern(DyeColor.RED, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_SMALL),
            new Pattern(DyeColor.BLACK, PatternType.BRICKS),
            new Pattern(DyeColor.LIME, PatternType.SKULL)
    ), 15, 2),
    ENDERMAN(new BannerBuilder().setName("§5Enderman").setBaseColor(DyeColor.PURPLE).setPatterns(
            new Pattern(DyeColor.MAGENTA, PatternType.STRIPE_SMALL),
            new Pattern(DyeColor.MAGENTA, PatternType.STRIPE_SMALL),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.STRAIGHT_CROSS),
            new Pattern(DyeColor.BLACK, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP)
    ), 16, 2),


    RABBIT(new BannerBuilder().setName("§fRabbit").setBaseColor(DyeColor.WHITE).setPatterns(
            new Pattern(DyeColor.BLACK, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.WHITE, PatternType.FLOWER),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.WHITE, PatternType.CROSS),
            new Pattern(DyeColor.BLACK, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.WHITE, PatternType.TRIANGLES_BOTTOM)
    ), 17, 3),
    RICE(new BannerBuilder().setName("§cRice").setBaseColor(DyeColor.BLACK).setPatterns(
            new Pattern(DyeColor.BROWN, PatternType.DIAGONAL_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.CROSS),
            new Pattern(DyeColor.WHITE, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.RED, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM)
    ), 18, 3),
    DRAGON(new BannerBuilder().setName("§5Dragon").setBaseColor(DyeColor.WHITE).setPatterns(
            new Pattern(DyeColor.BLACK, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.PURPLE, PatternType.FLOWER),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.WHITE, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.MOJANG)
    ), 19, 3),
    CHICKEN(new BannerBuilder().setName("§fChicken").setBaseColor(DyeColor.WHITE).setPatterns(
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.WHITE, PatternType.STRAIGHT_CROSS),
            new Pattern(DyeColor.RED, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.WHITE, PatternType.BORDER),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.YELLOW, PatternType.STRIPE_MIDDLE)
    ), 20, 3),
    SQUID(new BannerBuilder().setName("§2Squid").setBaseColor(DyeColor.RED).setPatterns(
            new Pattern(DyeColor.BLUE, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.RED, PatternType.STRIPE_SMALL),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.BLUE, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.RED, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.BLUE, PatternType.BORDER)
    ), 21, 3),
    FLOWER(new BannerBuilder().setName("§6F§9l§6o§9w§6e§9r").setBaseColor(DyeColor.BLUE).setPatterns(
            new Pattern(DyeColor.ORANGE, PatternType.GRADIENT_UP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_SMALL),
            new Pattern(DyeColor.BLACK, PatternType.RHOMBUS_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.ORANGE, PatternType.FLOWER),
            new Pattern(DyeColor.BLACK, PatternType.FLOWER)
    ), 22, 3),
    CAT(new BannerBuilder().setName("§7Cat").setBaseColor(DyeColor.GRAY).setPatterns(
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.TRIANGLES_TOP),
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.SQUARE_TOP_LEFT),
            new Pattern(DyeColor.LIGHT_BLUE, PatternType.CURLY_BORDER),
            new Pattern(DyeColor.LIME, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.GRAY, PatternType.MOJANG),
            new Pattern(DyeColor.BROWN, PatternType.TRIANGLES_BOTTOM)
    ), 23, 3),
    PANDA(new BannerBuilder().setName("§8P§fa§8n§fd§8a").setBaseColor(DyeColor.WHITE).setPatterns(
            new Pattern(DyeColor.BLACK, PatternType.FLOWER),
            new Pattern(DyeColor.WHITE, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLES_TOP),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_CENTER),
            new Pattern(DyeColor.BLACK, PatternType.CREEPER),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_BOTTOM)
    ), 24, 3),
    PIKACHU(new BannerBuilder().setName("§ePikachu").setBaseColor(DyeColor.BLUE).setPatterns(
            new Pattern(DyeColor.RED, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.CIRCLE_MIDDLE),
            new Pattern(DyeColor.YELLOW, PatternType.FLOWER),
            new Pattern(DyeColor.YELLOW, PatternType.CROSS),
            new Pattern(DyeColor.BLUE, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLUE, PatternType.CURLY_BORDER)
    ), 25, 3);

    private final BannerBuilder item;
    private final int id;
    private final int level;

    Banners(BannerBuilder item, int id, int level) {
        this.item = item;
        this.id = id;
        this.level = level;
    }

    public BannerBuilder getItem() { return item; }
    public int getId() { return id; }
    public int getLevel() { return level; }
}
