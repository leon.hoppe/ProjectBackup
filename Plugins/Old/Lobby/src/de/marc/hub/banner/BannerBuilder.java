package de.marc.hub.banner;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class BannerBuilder {

    private final Material material = Material.BANNER;
    private String name;
    private final HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    private final ArrayList<String> lore = new ArrayList<>();
    private boolean showEnchantments = true;

    private DyeColor color;
    private final ArrayList<Pattern> patterns = new ArrayList<>();


    public BannerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public BannerBuilder setLore(String... lore){
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    public BannerBuilder setGlowing(boolean value) {
        if (value) {
            enchantments.put(Enchantment.DURABILITY, 1);
            showEnchantments = false;
        }else {
            enchantments.clear();
            showEnchantments = true;
        }
        return this;
    }


    public BannerBuilder setBaseColor(DyeColor color) {
        this.color = color;
        return this;
    }

    public BannerBuilder setPatterns(Pattern... patterns) {
        this.patterns.addAll(Arrays.asList(patterns));
        return this;
    }


    public ItemStack build(){
        ItemStack item;
        item = new ItemStack(material);
        BannerMeta meta = (BannerMeta) item.getItemMeta();
        for (Enchantment e : enchantments.keySet()){
            meta.addEnchant(e, enchantments.get(e), true);
        }
        meta.setLore(lore);
        meta.setDisplayName(name);
        if (!showEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        meta.setBaseColor(color);
        meta.setPatterns(patterns);

        item.setItemMeta(meta);
        return item;
    }

}
