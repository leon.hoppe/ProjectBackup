package de.marc.hub.gui;

import de.marc.hub.main.Main;
import de.marc.hub.utils.ItemBuilder;
import de.marc.hub.utils.Title;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class TempNavGUI implements Listener {

    private static final String invTitle = "§cNavigation";

    public static void openTemNav(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9*3, invTitle);

        for (int slot = 0; slot < 9; slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        for (int slot = 18; slot < inventory.getSize(); slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        inventory.setItem(4, new ItemBuilder(Material.EYE_OF_ENDER).setName("§6§lSpawn").build());
        inventory.setItem(11, new ItemBuilder(Material.WORKBENCH).setName("§5CityBuild").setLore("§7Handel in der §eWirtschaft §7und", "§7lasse beim bauen deiner", "§eKreativität §7freien Lauf.").build());
        inventory.setItem(15, new ItemBuilder(Material.GOLD_PICKAXE).setName("§2FreeBuild").setLore("§7Baue alleine oder mit deinen", "§7Freunden eine eigene §eStadt §7auf.").build());


        player.openInventory(inventory);
    }


    @EventHandler
    public void handleNavigatorGUIClick(InventoryClickEvent event) {
        //sehr wichtig
        if (event.getClickedInventory() == null) return;
        if(!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals(invTitle)) {
            event.setCancelled(true);

            switch (event.getCurrentItem().getType()) {
                case EYE_OF_ENDER:
                    Location l = null;
                    try {
                        l = Main.locationManager.getLocation();
                    } catch (Exception ignored) { }
                    if (!(l == null)) {
                        player.teleport(Main.locationManager.getLocation());
                        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 2);
                    } else {
                        player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);
                        player.sendMessage("§8»");
                        player.sendMessage(Title.prefix + "§7Der Spawn wurde noch nicht gesetzt.");
                        player.sendMessage(Title.prefix + "§7Bitte wende dich an einen Serveradministrator.");
                        player.sendMessage("§8»");
                    }
                    break;

                case WORKBENCH:
                    Title.locTP(player, Main.locationManager.getWarp("citybuild"));
                    break;

                case GOLD_PICKAXE:
                    Title.locTP(player, Main.locationManager.getWarp("freebuild"));
                    break;
            }
        }
    }

}
