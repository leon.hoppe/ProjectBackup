package de.marc.hub.gui;

import de.marc.hub.cosmetics.CosmeticManager;
import de.marc.hub.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class CosmeticGUI implements Listener {

    private static final String invTitle = "§6Cosmetics";

    public static void openCosmetic(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9*3, invTitle);

        for (int slot = 0; slot < 9; slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        for (int slot = 18; slot < inventory.getSize(); slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        inventory.setItem(11, new ItemBuilder(Material.LEATHER_BOOTS).setName("§bParticle").build());

        inventory.setItem(13, new ItemBuilder(Material.GLASS).setName("§6Heads").build());

        inventory.setItem(15, new ItemBuilder(Material.STRING).setName("§3Flügel").build());


        inventory.setItem(22, new ItemBuilder(Material.BARRIER).setName("§4Alle entfernen").build());

        player.openInventory(inventory);
    }


    @EventHandler
    public void handleNavigatorGUIClick(InventoryClickEvent event) {
        //sehr wichtig
        if (event.getClickedInventory() == null) return;
        if(!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals(invTitle)) {
            event.setCancelled(true);

            switch (event.getCurrentItem().getType()) {
                case LEATHER_BOOTS:
                    ParticleGUI.openParticle(player);
                    break;

                case GLASS:

                    break;

                case STRING:

                    break;

                case BARRIER:
                    CosmeticManager.disableCosmetics(player);

                    player.getInventory().setHelmet(null);

                    player.playSound(player.getLocation(), Sound.PISTON_RETRACT, 1, 2);
                    break;
            }
        }
    }

}
