package de.marc.hub.gui;

import de.marc.hub.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class NavigationGUI implements Listener {

    private static final String invTitle = "§cNavigation f";

    public static void openNav(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9*6, invTitle);

        for (int slot = 0; slot < 9; slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        inventory.setItem(4, new ItemBuilder(Material.EYE_OF_ENDER).setName("§6§lSpawn").build());

        inventory.setItem(10, new ItemBuilder(Material.SANDSTONE).setName("§eBuildFFA").build());
        inventory.setItem(13, new ItemBuilder(Material.SLIME_BLOCK).setName("§aJump and Run").build());
        inventory.setItem(16, new ItemBuilder(Material.DIAMOND_SWORD).setName("§b1 §lvs §r§b1").build());
        inventory.setItem(29, new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).setName("§cArena").build());
        inventory.setItem(31, new ItemBuilder(Material.WORKBENCH).setName("§5CityBuild").build());

        inventory.setItem(33, new ItemBuilder(Material.BARRIER).setName("§4coming soon..").build());
        inventory.setItem(46, new ItemBuilder(Material.BARRIER).setName("§4coming soon..").build());
        inventory.setItem(49, new ItemBuilder(Material.BARRIER).setName("§4coming soon..").build());
        inventory.setItem(52, new ItemBuilder(Material.BARRIER).setName("§4coming soon..").build());


        player.openInventory(inventory);
    }


    @EventHandler
    public void handleNavigatorGUIClick(InventoryClickEvent event) {
        //sehr wichtig
        if (event.getClickedInventory() == null) return;
        if(!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals(invTitle)) {
            event.setCancelled(true);

            switch (event.getCurrentItem().getType()) {
                case CHEST:
                    //InventoryGUI
                    break;
            }
        }
    }
}
