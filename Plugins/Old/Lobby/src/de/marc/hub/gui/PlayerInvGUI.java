package de.marc.hub.gui;

import de.marc.hub.main.Main;
import de.marc.hub.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerInvGUI implements Listener {

    public static void addPlayerInv(Player player) {
        player.getInventory().clear();
        Inventory inventory = player.getInventory();

        inventory.setItem(0, new ItemBuilder(Material.CHEST).setName("§6Inventar").build());
        inventory.setItem(1, new ItemBuilder(Material.NETHER_STAR).setName("§eShop").build());
        inventory.setItem(4, new ItemBuilder(Material.COMPASS).setName("§c§lNavigator").build());

        //hider
        inventory.setItem(7, new ItemStack(Material.GLOWSTONE_DUST));

        inventory.setItem(8, new ItemBuilder(player).setName("§7§l" + player.getName()).build());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Player player = event.getPlayer();
            if (Main.buildMode.contains(player)) return;
            event.setCancelled(true);

            if (event.getItem() == null) return;
            switch (event.getItem().getType()) {
                case CHEST:
                    InventoryGUI.openInventory(player);
                    break;

                case NETHER_STAR:
                    //ShopGUI
                    break;

                case COMPASS:
                    //NavigationGUI.openNav(player);
                    TempNavGUI.openTemNav(player);
                    break;

                //case GLOWSTONE_DUST:
                //hider

                case SKULL_ITEM:
                    //FRIENDGUI
                    break;

                default:
                    break;

            }
        }
    }

}
