package de.marc.hub.gui;

import de.marc.coins.api.CoinAPI;
import de.marc.hub.utils.ItemBuilder;
import de.marc.hub.utils.Title;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class InventoryGUI implements Listener {

    private static final String invTitle = "§6Inventar";

    public static void openInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9*3, invTitle);

        for (int slot = 0; slot < 9; slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        for (int slot = 18; slot < inventory.getSize(); slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        inventory.setItem(10, new ItemBuilder(Material.GOLD_INGOT).setName("§e" + CoinAPI.getCoins(player.getUniqueId().toString()) + " Coins").build());

        inventory.setItem(12, new ItemBuilder(Material.EMERALD).setName("§20 Token").setLore("§ccoming soon...").build());

        inventory.setItem(14, new ItemBuilder(Material.ENDER_CHEST).setName("§50 Chests").setLore("§ccoming soon...").build());

        inventory.setItem(16, new ItemBuilder(Material.FEATHER).setName("§6Cosmetics").build());

        

        player.openInventory(inventory);
    }


    @EventHandler
    public void handleNavigatorGUIClick(InventoryClickEvent event) {
        //sehr wichtig
        if (event.getClickedInventory() == null) return;
        if(!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals(invTitle)) {
            event.setCancelled(true);

            switch (event.getCurrentItem().getType()) {
                case ENDER_CHEST:
                    player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);
                    Title.sendActionBar(player, Title.prefix + "§4coming soon");
                    break;

                case FEATHER:
                    CosmeticGUI.openCosmetic(player);
                    break;
            }
        }
    }

}
