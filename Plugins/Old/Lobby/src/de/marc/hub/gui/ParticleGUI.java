package de.marc.hub.gui;

import de.marc.hub.cosmetics.CosmeticManager;
import de.marc.hub.cosmetics.Cosmetics;
import de.marc.hub.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class ParticleGUI implements Listener {

    private static final String invTitle = "§6Cosmetics §8» §r§bParticle";

    public static void openParticle(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9*3, invTitle);

        for (int slot = 0; slot < 9; slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        for (int slot = 18; slot < inventory.getSize(); slot++) {
            inventory.setItem(slot, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setName(" ").build());
        }

        inventory.setItem(10, Cosmetics.PARTICLE_HEARTS.getItem());

        inventory.setItem(12, Cosmetics.PARTICLE_FIRE.getItem());

        inventory.setItem(14, Cosmetics.PARTICLE_PORNOPUFF.getItem());

        inventory.setItem(16, Cosmetics.PARTICLE_MUSIC.getItem());


        player.openInventory(inventory);
    }


    @EventHandler
    public void handleNavigatorGUIClick(InventoryClickEvent event) {
        //sehr wichtig
        if (event.getClickedInventory() == null) return;
        if(!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals(invTitle)) {
            event.setCancelled(true);

            switch (event.getSlot()) {
                case 10:
                    CosmeticManager.setCosmetics(player, Cosmetics.PARTICLE_HEARTS);
                    player.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 2);
                    player.closeInventory();
                    break;

                case 12:
                    CosmeticManager.setCosmetics(player, Cosmetics.PARTICLE_FIRE);
                    player.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 2);
                    player.closeInventory();
                    break;

                case 14:
                    CosmeticManager.setCosmetics(player, Cosmetics.PARTICLE_PORNOPUFF);
                    player.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 2);
                    player.closeInventory();
                    break;

                case 16:
                    CosmeticManager.setCosmetics(player, Cosmetics.PARTICLE_MUSIC);
                    player.playSound(player.getLocation(), Sound.ANVIL_USE, 1, 2);
                    player.closeInventory();
                    break;
            }
        }
    }
}
