package de.marc.hub.main;

import de.marc.hub.commands.*;
import de.marc.hub.cosmetics.CosmeticManager;
import de.marc.hub.commands.DelWarpCommand;
import de.marc.hub.gui.*;
import de.marc.hub.listener.*;
import de.marc.hub.utils.LocationManager;
import de.marc.hub.utils.MySQL;
import de.marc.hub.utils.OnReload;
import de.marc.hub.utils.WeatherManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;

public class Main extends JavaPlugin {
    private static Main instance;
    public static LocationManager locationManager;

    public static final ArrayList<Player> buildMode = new ArrayList<>();
    public static final ArrayList<Player> flyMode = new ArrayList<>();

    //Config
    public static File file;
    public static FileConfiguration cfg;


    @Override
    public void onEnable() {
        instance = this;
        locationManager = new LocationManager();
        MySQL.connect();
        CosmeticManager.initialize();
        OnReload.onReloadKick();

        //Config
        saveDefaultConfig();
        Main.file = new File("plugins/Hub", "config.yml");
        Main.cfg = YamlConfiguration.loadConfiguration(Main.file);

        //Commands
        getCommand("build").setExecutor(new BuildCommand());
        getCommand("setLoc").setExecutor(new SetLocationCommand());
        getCommand("test").setExecutor(new TestCmd());
        getCommand("spawn").setExecutor(new SpawnCommand());
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("setWarp").setExecutor(new SetWarpCommand());
        getCommand("delWarp").setExecutor(new DelWarpCommand());
        getCommand("gm").setExecutor(new GameModeCommand());

        //Listener
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new WeatherManager(), this);
        pluginManager.registerEvents(new JumpListener(), this);
        pluginManager.registerEvents(new MoveListener(), this);
        pluginManager.registerEvents(new NavigationGUI(), this);
        pluginManager.registerEvents(new PickupListener(), this);
        pluginManager.registerEvents(new FoodListener(), this);
        pluginManager.registerEvents(new LeavesDecayListener(), this);
        pluginManager.registerEvents(new QuitListener(), this);
        pluginManager.registerEvents(new BlockBreakListener(), this);
        pluginManager.registerEvents(new BlockPlaceListener(), this);
        pluginManager.registerEvents(new PlayerDropListener(), this);
        pluginManager.registerEvents(new EntityDamageListener(), this);
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new PlayerInvGUI(), this);
        pluginManager.registerEvents(new DeathListener(), this);
        pluginManager.registerEvents(new PlayerUseListener(), this);
        pluginManager.registerEvents(new TempNavGUI(), this);
        pluginManager.registerEvents(new InventoryGUI(), this);
        pluginManager.registerEvents(new CosmeticGUI(), this);
        pluginManager.registerEvents(new ParticleGUI(), this);
    }

    @Override
    public void onDisable() {
        CosmeticManager.saveCosmetics();
        MySQL.disconnect();
    }

    public static Main getInstance() {
        return instance;
    }
}
