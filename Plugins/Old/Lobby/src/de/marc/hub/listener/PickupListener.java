package de.marc.hub.listener;

import de.marc.hub.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PickupListener implements Listener {

    @EventHandler
    public static void onPickup(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        if (!(Main.buildMode.contains(player))) {
            event.setCancelled(true);
        }
    }

}
