package de.marc.hub.listener;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (!(Main.buildMode.contains(player))) {
            if (player.hasPermission("hub.build")) {
                Title.sendActionBar(player, Title.prefix + "§7Benutze §c/build §7 um zu bauen");
            } else
                Title.sendActionBar(player, Title.prefix + "§cDu darfst nicht bauen!");
            event.setCancelled(true);
        }
    }

}
