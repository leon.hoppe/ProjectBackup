package de.marc.hub.listener;

import de.marc.hub.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        //msg
        event.setQuitMessage("");

        //rem BuilderMode
        Main.buildMode.remove(player);

    }

}
