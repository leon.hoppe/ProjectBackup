package de.marc.hub.listener;

import de.marc.hub.main.Main;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.util.Vector;

import java.util.HashMap;

public class JumpListener implements Listener {

    private static final HashMap<Player, Boolean> hash = new HashMap<>();

    //Double Jump
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        hash.put(event.getPlayer(), true);
    }
    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        hash.remove(event.getPlayer());
    }
    @EventHandler
    public void onGameModeChange(PlayerGameModeChangeEvent event) {
        if (event.getNewGameMode().equals(GameMode.SURVIVAL)) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> event.getPlayer().setAllowFlight(true), 20);
        }
    }
    @EventHandler
    public void onDoubleJump(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode() != GameMode.CREATIVE && player.getGameMode() != GameMode.SPECTATOR) {
            if (Main.flyMode.contains(player)) return;
            event.setCancelled(true);
            Block block = player.getWorld().getBlockAt(player.getLocation().subtract(0, 2, 0));
            if (!block.getType().equals(Material.AIR)) {
                if (hash.get(player)) {
                    Vector v = player.getLocation().getDirection().multiply(1).setY(1);
                    player.setVelocity(v);

                    player.playSound(player.getLocation(), Sound.ENDERDRAGON_WINGS, 2, 1);

                    hash.remove(player);
                    player.setAllowFlight(false);
                    hash.put(player, false);

                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
                        hash.remove(player);
                        hash.put(player, true);
                    }, 2*20);
                }
            }
        }
    }
}
