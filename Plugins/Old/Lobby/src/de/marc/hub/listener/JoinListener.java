package de.marc.hub.listener;

import de.marc.hub.gui.PlayerInvGUI;
import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        //Msg
        event.setJoinMessage("");

        //Gamemode
        player.setGameMode(GameMode.SURVIVAL);

        //Inv
        PlayerInvGUI.addPlayerInv(player);

        //XP
        player.setLevel(2021);

        //spawn loc
        Location l = null;
        try {
            l = Main.locationManager.getLocation();
        } catch (Exception ignored) { }
        if (!(l == null)) {
            player.teleport(Main.locationManager.getLocation());
            player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 2);
        } else {
            player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 1);
            player.sendMessage("§8»");
            player.sendMessage(Title.prefix + "§7Der Spawn wurde noch nicht gesetzt.");
            player.sendMessage(Title.prefix + "§7Bitte wende dich an einen Serveradministrator.");
            player.sendMessage("§8»");
        }
    }

}
