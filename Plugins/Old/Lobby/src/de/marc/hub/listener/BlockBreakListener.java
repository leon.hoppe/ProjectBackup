package de.marc.hub.listener;

import de.marc.hub.main.Main;
import de.marc.hub.utils.Title;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (!(Main.buildMode.contains(player))) {
            if (player.hasPermission("hub.build")) {
                Title.sendActionBar(player, Title.prefix + "§7Benutze §c/build §7 um zu bauen");
            } else
                Title.sendActionBar(player, Title.prefix + "§cDu kannst das nicht abbauen!");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void noUproot(PlayerInteractEvent event)
    {
        if(event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.SOIL)
            event.setCancelled(true);
    }

}
