package de.marc.hub.listener;

import de.marc.hub.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class DeathListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        event.getDrops().clear();
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> player.spigot().respawn(), 1);
    }


    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        //Spawn
        event.setRespawnLocation(Main.locationManager.getLocation());
    }

}
