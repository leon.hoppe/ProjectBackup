package de.marc.hub.listener;

import de.marc.hub.main.Main;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class MoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        //Für JumpListener
        Block block = player.getWorld().getBlockAt(player.getLocation().subtract(0, 1, 0));
        Block block1 = player.getWorld().getBlockAt(player.getLocation().subtract(0, 2, 0));
        if (!(Main.flyMode.contains(player))) {
            if (!(block.getType().equals(Material.AIR)) || !(block1.getType().equals(Material.AIR))) {
                player.setAllowFlight(true);
            } else player.setAllowFlight(player.getGameMode().equals(GameMode.CREATIVE) || player.getGameMode().equals(GameMode.SPECTATOR));
        }

        if (player.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            Vector vector = player.getLocation().getDirection().multiply(4.0).setY(1.5);
            player.setVelocity(vector);
            player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 10, 2);

            for (final Player players : Bukkit.getOnlinePlayers()) {
                players.playEffect(player.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
            }
        }

        //DeathBorder
        double y = player.getLocation().getY();
        if(y < Main.locationManager.getDeathBoarder()) {
            player.teleport(Main.locationManager.getLocation());
            player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 2);
            event.setCancelled(true);
        }

    }

}
