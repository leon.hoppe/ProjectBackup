package de.marc.hub.cosmetics;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public abstract class Cosmetic {

    protected HashMap<UUID, Boolean> enabled = new HashMap<>();
    protected int type;

    public abstract void setup();
    public abstract void enable(Player player);
    public abstract void disable(Player player);
    public abstract void onMove(PlayerMoveEvent event);

    public void setEnabled(Player p, boolean enabled) { this.enabled.remove(p.getUniqueId()); this.enabled.put(p.getUniqueId(), enabled); }
    public void setType(int type) { this.type = type; }
    public boolean isEnabled(Player p) { try { return enabled.get(p.getUniqueId()); } catch (Exception e) { return false; } }
    public int getType() { return type; }
    public UUID[] getEnabledPlayers() {
        ArrayList<UUID> a = new ArrayList<>();
        for (UUID p : enabled.keySet()) if (enabled.get(p) && Bukkit.getPlayer(p) != null) a.add(p);
        return a.toArray(new UUID[0]);
    }
    public ArrayList<UUID> getEnabledPlayersAsList() {
        ArrayList<UUID> a = new ArrayList<>();
        for (UUID p : enabled.keySet()) if (enabled.get(p) && Bukkit.getPlayer(p) != null) a.add(p);
        return a;
    }

}
