package de.marc.hub.cosmetics.particles;

import de.marc.hub.cosmetics.Cosmetic;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

public class Fire extends Cosmetic {
    @Override
    public void setup() {

    }

    @Override
    public void enable(Player player) {

    }

    @Override
    public void disable(Player player) {

    }

    @Override
    public void onMove(PlayerMoveEvent event) {
        if (!getEnabledPlayersAsList().contains(event.getPlayer().getUniqueId())) return;
        Player p = event.getPlayer();
        for (Player all : Bukkit.getOnlinePlayers()) all.playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 1);
    }
}
