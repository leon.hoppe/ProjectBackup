package de.marc.hub.cosmetics.particles;

import de.marc.hub.cosmetics.Cosmetic;
import de.marc.hub.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.UUID;

public class WingsFire extends Cosmetic implements Runnable {
    @Override
    public void setup() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), this, 10, 0);
    }

    @Override
    public void enable(Player player) {

    }

    @Override
    public void disable(Player player) {

    }

    @Override
    public void onMove(PlayerMoveEvent event) {

    }

    @Override
    public void run() {
        for (UUID uuid : getEnabledPlayers()) {
            Player p = Bukkit.getPlayer(uuid);
            Vector[] offsets = new Vector[0]; //Particle Grid
            for (final Vector offset : offsets) {
                Vector pos = p.getLocation().toVector();
                pos.add(new Vector(0, 1.5f, 0));
                pos.add(offset);
                Location result = pos.toLocation(p.getWorld());
                for (Player all : Bukkit.getOnlinePlayers()) {
                    all.playEffect(result, Effect.FLAME, 1);
                }
            }
        }
    }
}
