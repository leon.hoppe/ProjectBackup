package de.marc.hub.cosmetics;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.UUID;

import de.marc.hub.main.Main;
import de.marc.hub.utils.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;

public final class CosmeticManager implements Listener {

    private static final HashMap<UUID, Cosmetics> cosmetics = new HashMap<>();

    public static void initialize() {
        if (!MySQL.isConnected()) throw new NullPointerException("MySQL not connected");
        MySQL.insert("CREATE TABLE IF NOT EXISTS Cosmetics (UUID VARCHAR(100), ID INT(10))");
        Bukkit.getPluginManager().registerEvents(new CosmeticManager(), Main.getInstance());
        for (Cosmetics cosmetics : Cosmetics.values()) cosmetics.getCosmetic().setup();
        loadCosmetics();
    }

    public static void loadCosmetics() {
        cosmetics.clear();
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Cosmetics");
            while (true) {
                assert rs != null;
                if (!rs.next()) break;
                Integer id = rs.getInt("ID");
                cosmetics.put(UUID.fromString(rs.getString("UUID")), Cosmetics.getByID(id));
            }
        }catch (Exception e) { e.printStackTrace(); }
    }

    public static void saveCosmetics() {
        MySQL.insert("DELETE FROM Cosmetics");
        for (UUID p : cosmetics.keySet()) {
            MySQL.insert("INSERT INTO Cosmetics (UUID, ID) VALUES (\"" + p + "\", " + cosmetics.get(p).getID() + ")");
        }
    }

    public static void setCosmetics(Player p, Cosmetics c) {
        disableCosmetics(p);
        cosmetics.put(p.getUniqueId(), c);
        c.getCosmetic().setEnabled(p, true);
        c.getCosmetic().enable(p);
    }

    public static void disableCosmetics(Player p) {
        while (cosmetics.containsKey(p.getUniqueId())) {
            cosmetics.get(p.getUniqueId()).getCosmetic().setEnabled(p, false);
            cosmetics.get(p.getUniqueId()).getCosmetic().disable(p);
            cosmetics.remove(p.getUniqueId());
        }
    }

    public static boolean isEnabled(Player p) {
        if (!cosmetics.containsKey(p.getUniqueId())) return false;
        return cosmetics.get(p.getUniqueId()).getCosmetic().isEnabled(p);
    }

    public static void openInventory(Player p) {
        Inventory inv = Bukkit.createInventory(null, 3*9, "Cosmetics");
        for (Cosmetics c : Cosmetics.values()) inv.setItem(c.getID(), c.getItem());
        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (!event.getView().getTitle().equals("Cosmetics")) return;
        if (event.getCurrentItem() == null || event.getCurrentItem().getItemMeta() == null || event.getCurrentItem().getItemMeta().getDisplayName() == null) return;
        event.setCancelled(true);
        event.getWhoClicked().closeInventory();
        Player p = (Player) event.getWhoClicked();
        for (Cosmetics c : Cosmetics.values()) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equals(c.getName())) {
                if (isEnabled(p)) disableCosmetics(p);
                else setCosmetics(p, c);
            }
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        for (Cosmetics cosmetic : Cosmetics.values()) cosmetic.getCosmetic().onMove(event);
    }

}
