package de.marc.hub.cosmetics;

import de.marc.hub.cosmetics.particles.*;
import de.marc.hub.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Cosmetics {

    PARTICLE_FIRE(new Fire(), "§4Feuer", 0, Material.FIREBALL),
    PARTICLE_HEARTS(new Hearts(), "§cHerzen", 1, Material.REDSTONE),
    PARTICLE_MUSIC(new Music(), "§bNoten", 2, Material.JUKEBOX),
    PARTICLE_PORNOPUFF(new PornoPuff(), "§5Enderman", 3, Material.ENDER_PEARL),

    WINGS_FIRE(new WingsFire(), "FeuerFlügel", 4, Material.BARRIER);

    private final Cosmetic cosmetic;
    private final String name;
    private final Integer id;
    private final Material item;

    Cosmetics(Cosmetic cosmetic, String name, Integer id, Material item) {
        this.cosmetic = cosmetic;
        this.name = name;
        this.id = id;
        this.item = item;
    }

    public Cosmetic getCosmetic() { return cosmetic; }
    public String getName() { return name; }
    public Integer getID() { return id; }
    public ItemStack getItem() { return new ItemBuilder(item).setName(name).build(); }

    public static Cosmetics getByID(Integer id) {
        for (Cosmetics c : values()) {
            if (c.getID().equals(id)) return c;
        }
        return null;
    }

}
