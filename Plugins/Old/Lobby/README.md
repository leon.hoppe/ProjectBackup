# Hub Plugin - 1.8.9

Das ist ein Lobby/Hub plugin für Spigot/Paper Server
**PrincepDE**


## Commands

|Command | Erklärung | Permissions |
|--|--|--|
| /build | Setzt dich in den Bau Modus | hub.build |
| /build [Player] | Setzt andere in den Bau Modus | hub.build.other |
| /setLoc | Setze den Spawnpunkt oder die Deathboarder | hub.admin.setLoc |
| /spawn | Teleportiert dich zu Spawn Punkt | - |
| /spawn [Player] | Teleportiert andere zu Spawn Punkt | hub.spawn.other |
| /fly | Setzt dich in den Fly Modus | hub.admin.fly |
| /fly [Player] | Setzt andere in den Fly Modus | hub.admin.fly.other |
| /setWarp | Setze Warps | hub.admin.warp.set |
| /delWarp | Lösche Warps | hub.admin.warp.del |
| /gamemode [0-3] | Gamemode | hub.admin.gamemode |


> **Note:** Es könnten sich noch Bugs herrausstellen. 
> Bitte kontaktiere dann sofort einem **Admin**!
