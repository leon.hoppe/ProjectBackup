package de.craftix.teamchat;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamChatCmd extends Command {
    public static final String adminPermission = "tc.send";
    private static final String readPerm = "tc.read";
    private static final String prefix = " §8➥§r §4TeamChat §r§8»§r ";

    public TeamChatCmd(String name, String permission, String... aliases) {
        super(name, permission, aliases);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) return;
        ProxiedPlayer p = (ProxiedPlayer) sender;
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if (i < args.length - 1)
                msg.append(args[i]).append(" ");
            else
                msg.append(args[i]);
        }
        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
            if (all.hasPermission(adminPermission) || all.hasPermission(readPerm))
                all.sendMessage(new TextComponent(prefix + "§7" + p.getDisplayName() + " §8×§r " + msg.toString().replaceAll("&", "§")));
    }
}
