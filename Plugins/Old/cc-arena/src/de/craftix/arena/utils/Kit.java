package de.craftix.arena.utils;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.Serializable;
import java.util.HashMap;

public class Kit implements Serializable {

    public String name;
    public String path;
    public int id;
    public int cost;
    public KitItem invItem = new KitItem();
    public KitItem helmet = new KitItem();
    public KitItem chestplate = new KitItem();
    public KitItem leggins = new KitItem();
    public KitItem boots = new KitItem();
    public HashMap<Integer, KitItem> items = new HashMap<>();

    public Kit(String name) {this.name = name;}

    public static class KitItem implements Serializable {
        public String matName;
        public String name;
        public HashMap<String, Integer> enchantments = new HashMap<>();
        public boolean isNull = false;

        public KitItem convert(ItemStack item){
            if (item == null) {
                isNull = true;
                return this;
            }
            this.matName = item.getType().name();
            this.name = item.getItemMeta().getDisplayName();
            for (Enchantment e : item.getEnchantments().keySet()){
                enchantments.put(e.getKey().getKey(), item.getEnchantmentLevel(e));
            }
            return this;
        }

        public ItemStack convert(){
            if (isNull) return null;
            if (Material.getMaterial(matName) == null) return null;
            ItemStack item = new ItemStack(Material.getMaterial(matName));
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(name);
            for (String e : enchantments.keySet()){
                meta.addEnchant(Enchantment.getByKey(NamespacedKey.minecraft(e)), enchantments.get(e), true);
            }
            item.setItemMeta(meta);
            return item;
        }

        @Override
        public String toString() {
            return "KitItem{" +
                    "matName='" + matName + '\'' +
                    ", name='" + name + '\'' +
                    ", enchantments=" + enchantments +
                    ", isNull=" + isNull +
                    '}';
        }
    }

}
