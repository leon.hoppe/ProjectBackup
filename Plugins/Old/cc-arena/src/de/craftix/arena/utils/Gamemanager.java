package de.craftix.arena.utils;

import de.craftix.arena.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

public class Gamemanager implements Listener {

    public static Game activeGame;

    public Gamemanager(Main plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public static class Game {
        public boolean isRunning = false;
        public boolean witherMode = false;
        public Player owner;

        public ArrayList<Player> team1 = new ArrayList<>();
        public ArrayList<Player> team2 = new ArrayList<>();
        public HashMap<UUID, Inventory> playerInvs = new HashMap<>();

        public Game(Player owner) {
            this.owner = owner;
        }

        public boolean playerIsJoined(Player p) {
            if (team1.contains(p)) return true;
            if (team2.contains(p)) return true;
            return false;
        }

        public ArrayList<Player> getAllPlayers() {
            ArrayList<Player> all = new ArrayList<>();
            all.addAll(team1);
            all.addAll(team2);
            return all;
        }

        public void setInv(Player p) {
            Inventory inv = Bukkit.createInventory(p, 54, p.getName());
            inv.setContents(p.getInventory().getContents());
            playerInvs.put(p.getUniqueId(), inv);
        }

        public Inventory getInv(Player p) {
            return playerInvs.get(p.getUniqueId());
        }
    }

    public static void leaveGame(Player p) {
        Inventory inv = activeGame.getInv(p);
        Main.setInv(p, inv);
        p.teleport(SqlSaving.getSpawn(SqlSaving.Spawntypes.lobby));
        p.sendMessage("§cSpiel verlassen");
        activeGame.team1.remove(p);
        activeGame.team2.remove(p);
        if (activeGame.witherMode && activeGame.team1.size() == 0) {
            stopGame(0);
            return;
        }
        if (activeGame.isRunning) {
            if (activeGame.team1.size() == 0) stopGame(2);
            if (activeGame.team1.size() == 0) stopGame(1);
        }

    }

    public static void startGame(Game game) {
        for (Player all : game.getAllPlayers()) if (!SqlSaving.hasStats(all)) SqlSaving.createStats(all);
        if ((game.team1.size() == 0 || game.team2.size() == 0) && !game.witherMode) {
            game.owner.sendMessage("§cSpiel wird nicht gestartet, Ein team hat keine Spieler!");
            return;
        }
        game.isRunning = true;
        ArrayList<Location> team1Spawns = SqlSaving.getSpawns(SqlSaving.Spawntypes.team1);
        ArrayList<Location> team2Spawns = SqlSaving.getSpawns(SqlSaving.Spawntypes.team2);
        Random rand = new Random();
        for (Player t1 : game.team1) t1.teleport(team1Spawns.get(rand.nextInt(team1Spawns.size() - 1)));
        for (Player t2 : game.team1) t2.teleport(team1Spawns.get(rand.nextInt(team2Spawns.size() - 1)));
        for (Player all : game.getAllPlayers()) {
            game.setInv(all);
            SqlSaving.Stats stats = SqlSaving.getStats(all);
            stats.playedGames++;
            SqlSaving.setStats(stats);
            all.sendTitle("§aDas Spiel beginnt...", "", 5, 40, 4);
            Kitmanager.preparePlayer(all, SqlSaving.getPlayerKit(all));
        }
        if (game.witherMode) System.out.println("[Wither gespawnt]"); //spawn Wither
    }

    public static void stopGame(int winnerTeamID) {
        if (activeGame.getAllPlayers().size() == 0) {
            System.out.println("[Arena] Spiel wurde wegen zu wenig Spielern abgebrochen");
        }
        for (Player all : activeGame.getAllPlayers()) {
            Main.setInv(all, activeGame.getInv(all));
            all.teleport(SqlSaving.getSpawn(SqlSaving.Spawntypes.lobby));
        }
        if (activeGame.witherMode) {
            if (winnerTeamID == -1) {
                for (Player all : activeGame.getAllPlayers()) {
                    SqlSaving.Stats stats = SqlSaving.getStats(all);
                    stats.looses++;
                    SqlSaving.setStats(stats);
                    all.sendTitle("§cDer Wither hat gewonnen", "", 10, 100, 10);
                }
            } else if (winnerTeamID == -2) {
                for (Player all : activeGame.getAllPlayers()) {
                    SqlSaving.Stats stats = SqlSaving.getStats(all);
                    stats.wins++;
                    SqlSaving.setStats(stats);
                    all.sendTitle("§aDie Spieler haben gewonnen", "", 10, 100, 10);
                }
            }
        } else {
            if (winnerTeamID == 1) {
                for (Player p : activeGame.team1) {
                    SqlSaving.Stats stats = SqlSaving.getStats(p);
                    stats.wins++;
                    SqlSaving.setStats(stats);
                }
                for (Player p : activeGame.team2) {
                    SqlSaving.Stats stats = SqlSaving.getStats(p);
                    stats.looses++;
                    SqlSaving.setStats(stats);
                }
                for (Player all : activeGame.getAllPlayers())
                    all.sendTitle("§aTeam §4Rot §ahat gewonnen", "", 10, 100, 10);
            } else {
                for (Player p : activeGame.team2) {
                    SqlSaving.Stats stats = SqlSaving.getStats(p);
                    stats.wins++;
                    SqlSaving.setStats(stats);
                }
                for (Player p : activeGame.team1) {
                    SqlSaving.Stats stats = SqlSaving.getStats(p);
                    stats.looses++;
                    SqlSaving.setStats(stats);
                }
                for (Player all : activeGame.getAllPlayers())
                    all.sendTitle("§aTeam §9Blau §ahat gewonnen", "", 10, 100, 10);
            }
        }
        activeGame = null;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        if (!SqlSaving.hasStats(p)) SqlSaving.createStats(p);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if (activeGame == null) return;
        leaveGame(event.getPlayer());
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        if (Main.isInArena(block.getLocation())) event.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (Main.isInArena(block.getLocation())) event.setCancelled(true);
    }

    @EventHandler
    public void onExplode(BlockExplodeEvent event) {
        Block block = event.getBlock();
        if (Main.isInArena(block.getLocation())) event.blockList().clear();
    }

    @EventHandler
    public void onExplodeEntity(EntityExplodeEvent event) {
        if (Main.isInArena(event.getLocation())) event.blockList().clear();
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (Main.isInArena(event.getPlayer().getLocation())) event.setCancelled(true);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        if (!activeGame.getAllPlayers().contains(event.getEntity())) return;
        event.setDeathMessage(null);

    }

}
