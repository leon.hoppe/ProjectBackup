package de.craftix.arena.utils;

import de.craftix.arena.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;

public class Kitmanager implements CommandExecutor, Listener {

    public Kitmanager(Main plugin){
        PluginCommand cmd = plugin.getCommand("arenakit");
        cmd.setExecutor(this::onCommand);
        cmd.setPermission(Setup.setupPermission);
        cmd.setPermissionMessage("§cHierzu hast du keine Rechte!");
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        //arenakit create <name>
        if (args.length == 2 && args[0].equalsIgnoreCase("create")){
            if (SqlSaving.getKitID(args[1]) != -1){
                p.sendMessage("§cDieses Kit existiert bereits!");
                return true;
            }
            SqlSaving.setKit(new Kit(args[1]), true);
            p.sendMessage("§aDas Kit §6" + args[1] + " §awurde erstellt");
            return true;
        }
        //arenakit edit <name>
        if (args.length == 2 && args[0].equalsIgnoreCase("edit")){
            Kit kit = SqlSaving.getKit(SqlSaving.getKitID(args[1]));
            if (kit == null){
                p.sendMessage("§cDieses Kit existiert nicht!");
                return true;
            }
            kit.helmet = new Kit.KitItem().convert(p.getInventory().getHelmet());
            kit.chestplate = new Kit.KitItem().convert(p.getInventory().getChestplate());
            kit.leggins = new Kit.KitItem().convert(p.getInventory().getLeggings());
            kit.boots = new Kit.KitItem().convert(p.getInventory().getBoots());
            kit.items.clear();
            for (int i = 0; i < p.getInventory().getSize(); i++){
                kit.items.put(i, new Kit.KitItem().convert(p.getInventory().getItem(i)));
            }
            SqlSaving.setKit(kit, false);
            p.sendMessage("§aDein Inventar wurde als Kitvorlage für §6" + kit.name + " §averwendet");
            return true;
        }
        //arenakit delete <name>
        if (args.length == 2 && args[0].equalsIgnoreCase("delete")){
            Kit kit = SqlSaving.getKit(SqlSaving.getKitID(args[1]));
            if (kit == null){
                p.sendMessage("§cDieses Kit existiert nicht!");
                return true;
            }
            SqlSaving.deleteKit(kit);
            p.sendMessage("§cDas Kit wurde erfolgreich gelöscht");
            return true;
        }
        //arenakit list
        if (args.length == 1 && args[0].equalsIgnoreCase("list")){
            openListMenu(p);
            return true;
        }
        //arenakit setItem <name>
        if (args.length == 2 && args[0].equalsIgnoreCase("setitem")){
            Kit kit = SqlSaving.getKit(SqlSaving.getKitID(args[1]));
            if (kit == null){
                p.sendMessage("§cDieses Kit existiert nicht!");
                return true;
            }
            kit.invItem = new Kit.KitItem().convert(p.getInventory().getItemInMainHand());
            SqlSaving.setKit(kit, false);
            p.sendMessage("§aDas Item wurde erfolgreich zum Kit §6" + kit.name + " §ahinzugefügt");
            return true;
        }
        return true;
    }

    private static final String listMenuName = "§6Kits";
    public static void openListMenu(Player p){
        ArrayList<Kit> kits = SqlSaving.getAllKits();
        if (kits == null || kits.size() == 0){
            p.sendMessage("§cKeine Kits vorhanden!");
            return;
        }
        int size = 3*9;
        if (kits.size() > 3*9) size = 6*9;
        Inventory inv = Bukkit.createInventory(null, size, listMenuName);
        int slot = 0;
        for (Kit kit : kits){
            inv.setItem(slot, new ItemEditor(kit.invItem.convert()).setName(kit.name));
            slot++;
        }
        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (event.getView() == null || event.getClickedInventory() == null) return;
        if (!event.getView().getTitle().equals(listMenuName)) return;
        Player p = (Player) event.getWhoClicked();
        if (event.getCurrentItem() == null) return;
        String kitName = event.getCurrentItem().getItemMeta().getDisplayName();
        if (kitName == null || kitName.equals(" ")) return;
        SqlSaving.setPlayerKit(p, SqlSaving.getKit(SqlSaving.getKitID(kitName)));
        p.sendMessage("§aDu hast das Kit " + kitName + " §agewählt");
        p.closeInventory();
        event.setCancelled(true);
    }

    public static void preparePlayer(Player p, Kit kit){
        if (kit == null){
            kit = SqlSaving.getKit(0);
            if (kit == null){
                System.out.println("[Arena] Es wurde noch kein Kit erstellt, es könnte zu Fehlern beim Spiel kommen");
            }
        }
        p.getInventory().clear();
        if (kit.items != null && kit.items.size() != 0) for (Integer slot : kit.items.keySet()){
            if (kit.items.get(slot) == null) continue;
            p.getInventory().setItem(slot, kit.items.get(slot).convert());
        }

        p.getInventory().setHelmet(kit.helmet.convert());
        p.getInventory().setChestplate(kit.chestplate.convert());
        p.getInventory().setLeggings(kit.leggins.convert());
        p.getInventory().setBoots(kit.boots.convert());
    }
}
