package de.craftix.arena.utils;

import de.craftix.arena.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class Setup implements CommandExecutor, Listener {

    public static final String setupPermission = "arena.setup";
    private static final String setupItemName = "§2Arena Setup";
    private static final String setupInvName = "§2Arena Setup";


    private Inventory inv;

    public Setup(Main plugin){
        PluginCommand cmd = plugin.getCommand("arenasetup");
        cmd.setExecutor(this::onCommand);
        cmd.setPermission(setupPermission);
        cmd.setPermissionMessage("§cHierzu hast du keine Rechte!");
        Bukkit.getPluginManager().registerEvents(this, plugin);
        inv = Bukkit.createInventory(null, 3*9, setupInvName);
        for (int i = 0; i < inv.getSize(); i++) inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        inv.setItem(10, new ItemBuilder(Material.ENDER_PEARL).setName("§aSpawns für Team 1").create());
        inv.setItem(12, new ItemBuilder(Material.FIREWORK_STAR).setName("§aArenamitte").create());
        inv.setItem(13, new ItemBuilder(Material.HEART_OF_THE_SEA).setName("§aLobby").create());
        inv.setItem(14, new ItemBuilder(Material.FIREWORK_STAR).setName("§aSpawns auf der Tribüne").create());
        inv.setItem(16, new ItemBuilder(Material.ENDER_PEARL).setName("§aSpawns für Team 2").create());
        inv.setItem(inv.getSize() - 1, new ItemBuilder(Material.BARRIER).setName("§cEinstellungen zurücksetzen").create());

        if (SqlSaving.getSpawn(SqlSaving.Spawntypes.lobby) != null) inv.setItem(13, new ItemEditor(inv.getItem(13)).setGlowing(true));
        if (SqlSaving.getSpawn(SqlSaving.Spawntypes.grandstand) != null) inv.setItem(12, new ItemEditor(inv.getItem(12)).setGlowing(true));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player p = (Player)sender;
        p.getInventory().addItem(new ItemBuilder(Material.NETHER_STAR).setName(setupInvName).create());
        p.sendMessage("§aSetup Item erhalten...");
        return true;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (event.getItem() == null) return;
        if (!event.getItem().getItemMeta().getDisplayName().equals(setupItemName)) return;
        if (event.getPlayer().hasPermission(setupPermission)) event.getPlayer().openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (event.getView() == null) return;
        if (!event.getView().getTitle().equals(setupItemName)) return;
        event.setCancelled(true);
        Player p = (Player) event.getWhoClicked();
        switch (event.getSlot()){
            case 10:
                SqlSaving.addSpawn(p.getLocation(), SqlSaving.Spawntypes.team1);
                p.sendMessage("§aSpawn hinzugefügt!");
                p.closeInventory();
                break;
            case 12:
                SqlSaving.setSpawn(p.getLocation(), SqlSaving.Spawntypes.arenaMiddle);
                inv.setItem(event.getSlot(), new ItemEditor(inv.getItem(event.getSlot())).setGlowing(true));
                p.sendMessage("§aSpawn gesetzt!");
                p.closeInventory();
                break;
            case 13:
                SqlSaving.setSpawn(p.getLocation(), SqlSaving.Spawntypes.lobby);
                inv.setItem(event.getSlot(), new ItemEditor(inv.getItem(event.getSlot())).setGlowing(true));
                p.sendMessage("§aSpawn gesetzt!");
                p.closeInventory();
                break;
            case 14:
                SqlSaving.addSpawn(p.getLocation(), SqlSaving.Spawntypes.grandstand);
                p.sendMessage("§aSpawn hinzugefügt!");
                p.closeInventory();
                break;
            case 16:
                SqlSaving.addSpawn(p.getLocation(), SqlSaving.Spawntypes.team2);
                p.sendMessage("§aSpawn hinzugefügt!");
                p.closeInventory();
                break;
        }
        if (event.getSlot() == inv.getSize() - 1){
            SqlSaving.resetSpawns();
            inv.setItem(12, new ItemBuilder(Material.FIREWORK_STAR).setName("§aArenamitte").create());
            inv.setItem(13, new ItemBuilder(Material.HEART_OF_THE_SEA).setName("§aLobby").create());
            p.sendMessage("§aAlle Spawns zurückgesetzt!");
            p.closeInventory();
        }
    }
}
