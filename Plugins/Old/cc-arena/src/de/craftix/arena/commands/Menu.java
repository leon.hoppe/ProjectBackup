package de.craftix.arena.commands;

import de.craftix.arena.Main;
import de.craftix.arena.utils.*;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Content;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Random;

public class Menu implements CommandExecutor, Listener {

    public Menu(Main plugin){
        PluginCommand cmd = plugin.getCommand("arenamenu");
        cmd.setExecutor(this::onCommand);
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) openMainMenu((Player)sender);
        return true;
    }

    public static final String mainMenuName = "§aArena - ClymCity";
    public static void openMainMenu(Player p){
        Inventory inv = Bukkit.createInventory(null, 3*9, mainMenuName);
        for (int i = 0; i < inv.getSize(); i++) inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        ItemStack kits = new ItemBuilder(Material.CHEST).setName("§aKit wählen").create();
        ItemStack create = new ItemBuilder(Material.NAME_TAG).setName("§aSpiel erstellen").create();
        ItemStack join = new ItemBuilder(Material.ENDER_PEARL).setName("§aSpiel beitreten").create();
        ItemStack spec = new ItemBuilder(Material.ENDER_EYE).setName("§aZuschauen").create();
        ItemStack leave = new ItemBuilder(Material.BARRIER).setName("§aSpiel verlassen").create();
        ItemStack start = new ItemBuilder(Material.NETHER_STAR).setName("§aSpiel starten").create();
        ItemStack invite = new ItemBuilder(Material.PLAYER_HEAD).setHeadOwner(p).setName("§aSpieler einladen").create();

        if (Gamemanager.activeGame == null){
            inv.setItem(11, create);
            inv.setItem(15, kits);
        }else {
            if (Gamemanager.activeGame.isRunning){
                if (Gamemanager.activeGame.playerIsJoined(p)){
                    inv.setItem(13, leave);
                }else {
                    inv.setItem(15, kits);
                    inv.setItem(11, spec);
                }
            }else {
                if (Gamemanager.activeGame.playerIsJoined(p)){
                    inv.setItem(15, kits);
                    inv.setItem(11, leave);
                }else {
                    inv.setItem(15, kits);
                    inv.setItem(11, join);
                }
                if (Gamemanager.activeGame.owner.equals(p)){
                    inv.setItem(15, kits);
                    inv.setItem(11, start);
                    inv.setItem(13, invite);
                }
            }
        }

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (event.getClickedInventory() == null) return;
        if (event.getCurrentItem() == null) return;
        Player p = (Player) event.getWhoClicked();
        if (event.getView().getTitle().equals(mainMenuName)){
            event.setCancelled(true);
            switch (event.getCurrentItem().getItemMeta().getDisplayName()){
                case "§aKit wählen":
                    Kitmanager.openListMenu(p);
                    break;
                case "§aSpiel erstellen":
                    openCreateGameInv(p, new Gamemanager.Game(p));
                    break;
                case "§aSpiel beitreten":
                    if (Gamemanager.activeGame.witherMode){
                        Gamemanager.activeGame.team1.add(p);
                        p.sendMessage("§aDu bist dem Spiel beigetreten");
                        p.closeInventory();
                        break;
                    }
                    openJoinGameInv(p, Gamemanager.activeGame);
                    break;
                case "§aZuschauen":
                    p.closeInventory();
                    ArrayList<Location> spawns = SqlSaving.getSpawns(SqlSaving.Spawntypes.grandstand);
                    if (spawns == null) break;
                    p.teleport(spawns.get(new Random().nextInt(spawns.size() - 1)));
                    break;
                case "§aSpiel verlassen":
                    Gamemanager.leaveGame(p);
                    p.closeInventory();
                    break;
                case "§aSpiel starten":
                    Gamemanager.startGame(Gamemanager.activeGame);
                    p.closeInventory();
                    break;
                case "§aSpieler einladen":
                    openInviteInv(p);
                    break;
            }
        }
        if (event.getView().getTitle().equals(joinGameInvName)){
            event.setCancelled(true);
            switch (event.getCurrentItem().getItemMeta().getDisplayName()){
                case "§4Team Rot beitreten":
                    if (Gamemanager.activeGame.team1.size() > Main.checkTeamSize()){
                        p.sendMessage("§cDieses Team ist voll!");
                    }else {
                        Gamemanager.activeGame.team1.add(p);
                        p.sendMessage("§aDu bist dem Team §4Rot §abeigetreten");
                    }
                    p.closeInventory();
                    break;
                case "§9Team Blau beitreten":
                    if (Gamemanager.activeGame.team2.size() > Main.checkTeamSize()){
                        p.sendMessage("§cDieses Team ist voll!");
                    }else {
                        Gamemanager.activeGame.team2.add(p);
                        p.sendMessage("§aDu bist dem Team §9Blau §abeigetreten");
                    }
                    p.closeInventory();
                    break;
            }
        }
        if (event.getView().getTitle().equals(createGameInvName)){
            event.setCancelled(true);
            switch (event.getCurrentItem().getItemMeta().getDisplayName()){
                case "§aWither Modus":
                    Gamemanager.activeGame.witherMode = true;
                    Gamemanager.activeGame.team1.add(p);
                    p.closeInventory();
                    p.sendMessage("§aSpiel erstellt, Lade deine Freunde über das Menü ein");
                    break;
                case "§aNormaler Modus":
                    Gamemanager.activeGame.witherMode = false;
                    openJoinGameInv(p, Gamemanager.activeGame);
                    p.sendMessage("§aSpiel erstellt, Lade deine Freunde über das Menü ein");
                    break;
            }
        }
        if (event.getView().getTitle().equals(invitePlayerInvName)){
            if (event.getCurrentItem().getType().equals(Material.PLAYER_HEAD)){
                Player t = ((SkullMeta) event.getCurrentItem().getItemMeta()).getOwningPlayer().getPlayer();
                p.sendMessage("§aDu hast §6" + t.getName() + " §aeingeladen");
                p.closeInventory();
                TextComponent text = new TextComponent("§7[§aArena§7] §aEinladung erhalten von: §6" + p.getName());
                text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/arenamenu"));
                text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Clicke zum Annehemn").create()));
                t.spigot().sendMessage(text);
                //t.sendMessage("§7[§aArena§7] §aEinladung erhalten von: §6" + p.getName());
            }
        }
    }

    public static final String createGameInvName = "§aSpiel erstellen";
    public void openCreateGameInv(Player p, Gamemanager.Game game){
        Gamemanager.activeGame = game;
        Inventory inv = Bukkit.createInventory(null, 3*9, createGameInvName);
        for (int i = 0; i < inv.getSize(); i++) inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        ItemStack wither = new ItemBuilder(Material.NETHER_STAR).setName("§aWither Modus").setLore("§7Bei diesem Modus bekämpfen alle Spieler den Wither").create();
        ItemStack normal = new ItemBuilder(Material.DIAMOND_SWORD).setName("§aNormaler Modus").setLore("§7Bei diesem Modus bekämpfen sich 2 Teams").create();
        inv.setItem(11, normal);
        inv.setItem(15, wither);
        p.openInventory(inv);
    }

    public static final String joinGameInvName = "§aSpiel beitreten - Team wählen";
    public void openJoinGameInv(Player p, Gamemanager.Game game){
        Inventory inv = Bukkit.createInventory(null, 3*9, joinGameInvName);
        for (int i = 0; i < inv.getSize(); i++) inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        ItemStack red = new ItemBuilder(Material.RED_WOOL).setName("§4Team Rot beitreten").setLore("§7Spieler:").create();
        ItemStack blue = new ItemBuilder(Material.BLUE_WOOL).setName("§9Team Blau beitreten").setLore("§7Spieler:").create();
        for (Player allRed : game.team1) red = new ItemEditor(red).addLore("§7" + allRed.getName());
        for (Player allBlue : game.team2) blue = new ItemEditor(blue).addLore("§7" + allBlue.getName());
        inv.setItem(11, red);
        inv.setItem(15, blue);
        p.openInventory(inv);
    }

    public static final String invitePlayerInvName = "§aSpieler einladen";
    public void openInviteInv(Player p){
        Inventory inv = Bukkit.createInventory(null, 6*9, invitePlayerInvName);
        ArrayList<Player> all = Main.getAllPlayerInArena();
        if (all.size() > inv.getSize()){
            for (int i = 0; i < inv.getSize(); i++){
                if (all.get(i).getUniqueId().equals(p.getUniqueId())) continue;
                inv.setItem(i, new ItemBuilder(Material.PLAYER_HEAD).setHeadOwner(all.get(i)).setName("§a" + all.get(i).getName()).create());
            }
        }else {
            int slot = 0;
            for (Player t : all){
                if (t.getUniqueId().equals(p.getUniqueId())) continue;
                inv.setItem(slot, new ItemBuilder(Material.PLAYER_HEAD).setHeadOwner(t).setName("§a" + t.getName()).create());
                slot++;
            }
        }
        p.openInventory(inv);
    }
}
