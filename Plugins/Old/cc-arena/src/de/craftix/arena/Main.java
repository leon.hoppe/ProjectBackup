package de.craftix.arena;

import de.craftix.arena.commands.Menu;
import de.craftix.arena.utils.Gamemanager;
import de.craftix.arena.utils.Kitmanager;
import de.craftix.arena.utils.Setup;
import de.craftix.arena.utils.SqlSaving;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.ArrayList;

public class Main extends JavaPlugin {

    public static Main instance;

    @Override
    public void onEnable() {
        instance = this;
        MySQL.connect();
        sqlInitialisation(MySQL.isConnected());
        new Setup(this);
        new Kitmanager(this);
        new Menu(this);
        new Gamemanager(this);
        if (!SqlSaving.checkSetup()) System.out.println("[Arena] Setup noch nicht abgeschlossen!");
        File dict = new File(kitFilePrefix);
        if (!dict.exists()) dict.mkdirs();
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
    }

    private void sqlInitialisation(boolean isConnected){
        if (!isConnected) return;
        MySQL.insert("CREATE TABLE IF NOT EXISTS " + MySQL.spawns + " (Type INT(10), World VARCHAR(100), x VARCHAR(100), y VARCHAR(100), z VARCHAR(100), yaw VARCHAR(100), pitch VARCHAR(100))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS " + MySQL.stats + " (UUID VARCHAR(100), Kills INT(100), Deaths INT(100), PlayedGames INT(100), Wins INT(100), Looses INT(100))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS " + MySQL.kits + " (ID INT(10), Name VARCHAR(100), Path VARCHAR(100), Cost INT(10), UUID VARCHAR(100))");
    }

    public static int checkTeamSize(){
        if (!SqlSaving.checkSetup()) return -1;
        int team1size = SqlSaving.getSpawns(SqlSaving.Spawntypes.team1).size();
        int team2size = SqlSaving.getSpawns(SqlSaving.Spawntypes.team2).size();
        if (team1size < team2size) return team1size;
        return team2size;
    }

    public static ArrayList<Player> getAllPlayerInArena(){
        if (!SqlSaving.checkSetup()) return new ArrayList<>();
        ArrayList<Player> all = new ArrayList<>();
        Location middle = SqlSaving.getSpawn(SqlSaving.Spawntypes.arenaMiddle);
        for (Player s : Bukkit.getOnlinePlayers()){
            if (middle.distanceSquared(s.getLocation()) < 50) all.add(s);
        }
        return all;
    }


    public static final String kitFilePrefix = "plugins//CC-Arena//Kits//";

    public static void WriteObjectToFile(Object serObj, String filepath) {

        try {

            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();
            //System.out.println("[Arena] The Object  was succesfully written to a file");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Object ReadObjectFromFile(String filepath) {

        try {

            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Object obj = objectIn.readObject();

            //System.out.println("[Arena] The Object has been read from the file");
            objectIn.close();
            return obj;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void setInv(Player p, Inventory inv){
        p.getInventory().clear();
        for (int i = 0; i < p.getInventory().getSize(); i++){
            p.getInventory().setItem(i, inv.getItem(i));
        }
    }

    public static boolean isInArena(Location loc){
        Location middle = SqlSaving.getSpawn(SqlSaving.Spawntypes.arenaMiddle);
        return middle.distanceSquared(loc) < 50;
    }
}
