package de.craftix.onevone.utils.kit;

public enum Kittype {
    UHC,
    OP,
    SG,
    TANK,
    RANDOM
}
