package de.craftix.onevone.utils.kit;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Kit {

    public HashMap<Integer, ItemStack> items = new HashMap<>();
    public ItemStack helmet;
    public ItemStack chestplate;
    public ItemStack leggins;
    public ItemStack boots;
    public Kittype type;
    public ItemStack invItem;
    public String permission;

    public Kit(Kittype type){
        this.type = type;
        permission = "1vs1.kit." + type.name().toLowerCase();
    }

}
