package de.craftix.onevone.utils;

import de.craftix.onevone.Main;
import de.craftix.onevone.saving.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FightWorld {

    //static
    public static FightWorld getWorld(String cName) {
        ResultSet rs = MySQL.getData("SELECT * FROM worlds WHERE CName = \"" + cName + "\"");
        try {
            if (rs.next()){
                String x1 = rs.getString("x1");
                String y1 = rs.getString("y1");
                String z1 = rs.getString("z1");
                String yaw1 = rs.getString("yaw1");
                String pitch1 = rs.getString("pitch1");
                String x2 = rs.getString("x2");
                String y2 = rs.getString("y2");
                String z2 = rs.getString("z2");
                String yaw2 = rs.getString("yaw2");
                String pitch2 = rs.getString("pitch2");
                String world = rs.getString("Name");
                return new FightWorld(world, x1, y1, z1, yaw1, pitch1, x2, y2, z2, yaw2, pitch2, cName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveWorld(FightWorld world){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM worlds WHERE CName = \"" + world.cName + "\"");
            if (rs.next()) MySQL.insert("DELETE FROM worlds WHERE CName = \"" + world.cName + "\"");
            String name = world.world.getName();
            String x1 = String.valueOf(world.spawn1.getX());
            String y1 = String.valueOf(world.spawn1.getY());
            String z1 = String.valueOf(world.spawn1.getZ());
            String yaw1 = String.valueOf(world.spawn1.getYaw());
            String pitch1 = String.valueOf(world.spawn1.getPitch());
            String x2 = String.valueOf(world.spawn2.getX());
            String y2 = String.valueOf(world.spawn2.getY());
            String z2 = String.valueOf(world.spawn2.getZ());
            String yaw2 = String.valueOf(world.spawn2.getYaw());
            String pitch2 = String.valueOf(world.spawn2.getPitch());
            String qry1 = "INSERT INTO worlds (Name, x1, y1, z1, yaw1, pitch1, x2, y2, z2, yaw2, pitch2, CName) VALUES ";
            String qry2 = "(\"" + world.world.getName() + "\", ";
            String qry3 = "\"" + x1 + "\", \"" + y1 + "\", \"" + z1 + "\", \"" + yaw1 + "\", \"" + pitch1 + "\", ";
            String qry4 = "\"" + x2 + "\", \"" + y2 + "\", \"" + z2 + "\", \"" + yaw2 + "\", \"" + pitch2 + "\", \"" + world.cName + "\")";
            MySQL.insert(qry1 + qry2 + qry3 + qry4);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void deleteWorld(String cName){
        MySQL.insert("DELETE FROM worlds WHERE CName = \"" + cName + "\"");
    }

    public static boolean checkName(String cName){
        String ram = null;
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM worlds WHERE CName = \"" + cName + "\"");
            rs.next();
            try {
                ram = rs.getString("Name");
            }catch (Exception e){}
        }catch (Exception e){
            e.printStackTrace();
        }
        if (Main.getWorld(cName) == null && ram == null) return true;
        return false;
    }

    public static ArrayList<FightWorld> getAllWorlds(){
        ArrayList<FightWorld> worlds = new ArrayList<>();
        ResultSet rs = MySQL.getData("SELECT * FROM worlds");
        try {
            while (rs.next()){
                String x1 = rs.getString("x1");
                String y1 = rs.getString("y1");
                String z1 = rs.getString("z1");
                String yaw1 = rs.getString("yaw1");
                String pitch1 = rs.getString("pitch1");
                String x2 = rs.getString("x2");
                String y2 = rs.getString("y2");
                String z2 = rs.getString("z2");
                String yaw2 = rs.getString("yaw2");
                String pitch2 = rs.getString("pitch2");
                String world = rs.getString("Name");
                String cName = rs.getString("CName");
                worlds.add(new FightWorld(world, x1, y1, z1, yaw1, pitch1, x2, y2, z2, yaw2, pitch2, cName));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return worlds;
    }

    //nonStatic
    public Location spawn1;
    public Location spawn2;
    public World world;
    public String cName;

    public FightWorld() {}
    public FightWorld(String world, String x1, String y1, String z1, String yaw1, String pitch1, String x2, String y2, String z2, String yaw2, String pitch2, String cName){
        this.world = Bukkit.getWorld(world);
        spawn1 = new Location(this.world, Double.valueOf(x1), Double.valueOf(y1), Double.valueOf(z1), Float.valueOf(yaw1), Float.valueOf(pitch1));
        spawn2 = new Location(this.world, Double.valueOf(x2), Double.valueOf(y2), Double.valueOf(z2), Float.valueOf(yaw2), Float.valueOf(pitch2));
        this.cName = cName;
    }
    public FightWorld(World world, Location spawn1, Location spawn2, String cName){
        this.world = world;
        this.cName = cName;
        this.spawn1 = spawn1;
        this.spawn2 = spawn2;
    }

    public void setMap(World map){
        spawn1.setWorld(map);
        spawn2.setWorld(map);
    }

}
