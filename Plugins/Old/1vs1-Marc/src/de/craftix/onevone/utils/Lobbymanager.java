package de.craftix.onevone.utils;

import de.craftix.onevone.saving.MySQL;
import de.craftix.onevone.utils.kit.Kit;
import de.craftix.onevone.utils.kit.Kits;
import de.craftix.onevone.utils.kit.Kittype;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;

public class Lobbymanager implements Listener {

    public static World getLobby(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM lobby");
            rs.next();
            return Bukkit.getWorld(rs.getString("World"));
        }catch (Exception e) {}
        return null;
    }

    public static Location getSpawn(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM lobby");
            rs.next();
            String x = rs.getString("x");
            String y = rs.getString("y");
            String z = rs.getString("z");
            String yaw = rs.getString("yaw");
            String pitch = rs.getString("pitch");
            String world = rs.getString("world");
            Location loc = new Location(Bukkit.getWorld(world), Double.valueOf(x), Double.valueOf(y), Double.valueOf(z), Float.valueOf(yaw), Float.valueOf(pitch));
            return loc;
        }catch (Exception exception){}
        return null;
    }

    public static void giveInv(Player p){
        p.getInventory().clear();
        p.getInventory().setHelmet(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setBoots(null);
        p.setHealth(20);

        ItemStack selectKit = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta sKitMeta = selectKit.getItemMeta();
        sKitMeta.setDisplayName("§eKit wählen");
        selectKit.setItemMeta(sKitMeta);

        p.getInventory().setItem(4, selectKit);
    }

    public static Kittype getKittype(Player p){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Kits WHERE UUID = \"" + p.getUniqueId() + "\"");
            if (rs.next()){
                int kit = rs.getInt("Kit");
                return Kits.getTypeByID(kit);
            }
        }catch (Exception e){}
        return Kittype.RANDOM;
    }

    private static void saveKit(Kittype type, Player p){
        MySQL.insert("DELETE FROM Kits WHERE UUID = \"" + p.getUniqueId() + "\"");
        MySQL.insert("INSERT INTO Kits (UUID, Kit) VALUES (\"" + p.getUniqueId() + "\", " + Kits.getKitID(type) + ")");
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if (!e.getPlayer().getWorld().equals(getLobby())) return;
        e.setCancelled(true);
        if (e.getMaterial().equals(Material.DIAMOND_SWORD)) openKitGUI(e.getPlayer());
    }

    private static final String guiName = "§6§lKits";

    public static void openKitGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, 3*9, guiName);
        for (int slot = 0; slot < inv.getSize(); slot++){
            ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7);
            ItemMeta glassMeta = glass.getItemMeta();
            glassMeta.setDisplayName(" ");
            glass.setItemMeta(glassMeta);
            inv.setItem(slot, glass);
        }

        ItemStack random = new ItemStack(Material.NETHER_STAR);
        ItemMeta randomMeta = random.getItemMeta();
        randomMeta.setDisplayName("§eRANDOM");
        random.setItemMeta(randomMeta);
        inv.setItem(inv.getSize() - 1, random);

        int slot = 0;
        for (Kit kit : Kits.kits){
            inv.setItem(slot, kit.invItem);
            slot++;
        }

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e){
        try {
            if (!e.getView().getTitle().equals(guiName)) return;
            String item = e.getCurrentItem().getItemMeta().getDisplayName();
            if (item.equals("§eRANDOM")){
                saveKit(Kittype.RANDOM, (Player) e.getWhoClicked());
                e.getWhoClicked().sendMessage("§aDu hast das Kit " + item + " §agewählt");
                e.getWhoClicked().closeInventory();
                return;
            }
            for (Kit kit : Kits.kits){
                if (kit.invItem.getItemMeta().getDisplayName().equals(item)){
                    Player p = (Player) e.getWhoClicked();
                    if (p.hasPermission(kit.permission)){
                        saveKit(kit.type, (Player) e.getWhoClicked());
                        e.getWhoClicked().sendMessage("§aDu hast das Kit " + item + " §agewählt");
                    }else {
                        p.sendMessage("§cDu darfts dieses Kit nicht benutzen!");
                    }
                    e.getWhoClicked().closeInventory();
                }
            }
        }catch (Exception exception) {}
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e){
        if (!e.getPlayer().getWorld().equals(getLobby())) return;
        if (e.getPlayer().getInventory().getHeldItemSlot() == 4) return;
        if (!(e.getRightClicked() instanceof Player)) return;
        Player req = e.getPlayer();
        Player t = (Player) e.getRightClicked();

        Acception acc = Acception.getAcception(t, req);
        if (acc == null) {
            if (Acception.getAcception(req, t) == null){
                new Acception(req, t);
            }
        }else {
            Acception.acceptions.remove(acc);
            ArrayList<Acception> as = new ArrayList<>();
            for (Acception a : Acception.acceptions){
                if (a.requester == req) as.add(a);
                else if (a.target == req) as.add(a);
                else if (a.requester == t) as.add(a);
                else if (a.target == t) as.add(a);
            }
            for (Acception a : as){
                Acception.acceptions.remove(a);
            }
            openWorldGUI(t, req);
        }
    }

    public static final String worldGUIName = "§a§lMap auswählen";

    public static void openWorldGUI(Player p, Player p2){
        Inventory inv = Bukkit.createInventory(null, 3*9, worldGUIName);

        for (int slot = 0; slot < inv.getSize(); slot++){
            ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7);
            ItemMeta glassMeta = glass.getItemMeta();
            glassMeta.setDisplayName(" ");
            glass.setItemMeta(glassMeta);
            inv.setItem(slot, glass);
        }

        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skullMeta = (SkullMeta)skull.getItemMeta();
        skullMeta.setOwner(p2.getName());
        skullMeta.setDisplayName("§cGegner: " + p2.getDisplayName());
        skull.setItemMeta(skullMeta);
        inv.setItem(4, skull);

        try {
            int slot = 9;
            for (FightWorld world : FightWorld.getAllWorlds()){
                if (slot >= inv.getSize() - 2) break;
                ItemStack w = new ItemStack(Material.GRASS);
                ItemMeta wMeta = w.getItemMeta();
                wMeta.setDisplayName(world.cName);
                w.setItemMeta(wMeta);
                inv.setItem(slot, w);
                slot++;
            }
        }catch (Exception e) {}

        ItemStack random = new ItemStack(Material.NETHER_STAR);
        ItemMeta randomMeta = random.getItemMeta();
        randomMeta.setDisplayName("§eRANDOM");
        random.setItemMeta(randomMeta);
        inv.setItem(inv.getSize() - 1, random);

        p.openInventory(inv);
    }

    @EventHandler
    public void onWorldGUIClick(InventoryClickEvent e){
        try {
            if (!e.getView().getTitle().equals(worldGUIName)) return;
            e.setCancelled(true);
            Player p2 = Bukkit.getPlayer(((SkullMeta)e.getClickedInventory().getItem(4).getItemMeta()).getOwner());
            if (e.getCurrentItem().getType().equals(Material.NETHER_STAR)){
                ArrayList<FightWorld> worlds = FightWorld.getAllWorlds();
                int rand = new Random().nextInt(worlds.size() - 1);
                createGame((Player) e.getWhoClicked(), p2, worlds.get(rand));
            }
            if (!e.getCurrentItem().getType().equals(Material.GRASS)) return;
            String map = e.getCurrentItem().getItemMeta().getDisplayName();
            createGame((Player) e.getWhoClicked(), p2, FightWorld.getWorld(map));
        }catch (Exception exception){}
    }

    public static void createGame(Player p1, Player p2, FightWorld preset){
        Gamemanager.addGame(p1, p2, preset);
    }
}

class Acception {
    public static ArrayList<Acception> acceptions = new ArrayList<>();

    public static Acception getAcception(Player req, Player t){
        for (Acception a : acceptions){
            if (a.target == t && a.requester == req) return a;
        }
        return null;
    }

    public Player requester;
    public Player target;

    public Acception(Player req, Player t){
        requester = req;
        target = t;
        t.sendMessage("§6" + requester.getDisplayName() + " §ahat dich herausgefordert. Klicke ihn an, um die Herausforderung anzunehmen");
        req.sendMessage("§aDu hast §6" + target.getDisplayName() + " §aherausgefordert. Warte, bis er sie annimmt...");
        acceptions.add(this);
    }
}
