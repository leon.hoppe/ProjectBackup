package de.craftix.onevone.utils;

import de.craftix.onevone.saving.MySQL;
import de.craftix.onevone.utils.kit.Kits;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.io.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Gamemanager implements Listener {

    public static ArrayList<Gamemanager> games = new ArrayList<>();

    private static void copyFileStructure(File source, File target){
        try {
            ArrayList<String> ignore = new ArrayList<>(Arrays.asList("uid.dat", "session.lock"));
            if(!ignore.contains(source.getName())) {
                if(source.isDirectory()) {
                    if(!target.exists())
                        if (!target.mkdirs())
                            throw new IOException("Couldn't create world directory!");
                    String files[] = source.list();
                    for (String file : files) {
                        File srcFile = new File(source, file);
                        File destFile = new File(target, file);
                        copyFileStructure(srcFile, destFile);
                    }
                } else {
                    InputStream in = new FileInputStream(source);
                    OutputStream out = new FileOutputStream(target);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = in.read(buffer)) > 0)
                        out.write(buffer, 0, length);
                    in.close();
                    out.close();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Integer> getStats(Player p){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Stats WHERE UUID = \"" + p.getUniqueId().toString() + "\"");
            if (rs.next()){
                int wins = rs.getInt("Wins");
                int looses = rs.getInt("Looses");
                List<Integer> stats = new ArrayList<>();
                stats.add(wins);
                stats.add(looses);
                return stats;
            }
        }catch (Exception e) {}
        return null;
    }
    public static void setStats(Player p, int wins, int looses){
        MySQL.insert("DELETE FROM Stats WHERE UUID = \"" + p.getUniqueId().toString() + "\"");
        MySQL.insert("INSERT INTO Stats (UUID, Wins, Looses) VALUES (\"" + p.getUniqueId().toString() + "\", " + wins + ", " + looses + ")");
    }

    public static void addWin(Player p){
        List<Integer> stats = getStats(p);
        setStats(p, stats.get(0) + 1, stats.get(1));
    }
    public static void addLoose(Player p){
        List<Integer> stats = getStats(p);
        setStats(p, stats.get(0), stats.get(1) + 1);
    }

    public static void addGame(Player p1, Player p2, FightWorld preset){
        Gamemanager manager = new Gamemanager(p1, p2, preset);
        manager.startGame();
        games.add(manager);
    }

    private static int getFreeGameID(){
        int i = 0;
        for (Gamemanager game : games){
            if (game.id == i) i++;
        }
        return i;
    }

    public static boolean deleteWorld(File path){
        if(path.exists()) {
            File files[] = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    deleteWorld(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return(path.delete());
    }

    //nonStatic
    public Player p1;
    public Player p2;
    public FightWorld preset;
    public World map;
    public int id;

    public Gamemanager() {}
    public Gamemanager(Player p1, Player p2, FightWorld preset){
        this.p1 = p1;
        this.p2 = p2;
        this.preset = preset;
    }

    public void startGame(){
        id = getFreeGameID();
        copyFileStructure(preset.world.getWorldFolder(), new File(Bukkit.getWorldContainer(), preset.cName + "-" + id));
        new WorldCreator(preset.cName + "-" + id).createWorld();
        map = Bukkit.getWorld(preset.cName + "-" + id);
        preset.setMap(map);

        //map settings
        map.setGameRuleValue("randomTickSpeed", "0");
        map.setGameRuleValue("doFireTick", "false");
        map.setGameRuleValue("doMobSpawning", "false");
        map.setGameRuleValue("doDaylightCycle", "false");
        map.setAnimalSpawnLimit(0);
        map.setMonsterSpawnLimit(0);
        map.setAutoSave(false);
        map.setDifficulty(Difficulty.NORMAL);
        for (Entity e : map.getEntities()){
            if (e instanceof Player) continue;
            e.remove();
        }

        p1.teleport(preset.spawn1);
        p2.teleport(preset.spawn2);
        Kits.preparePlayer(Lobbymanager.getKittype(p1), p1);
        Kits.preparePlayer(Lobbymanager.getKittype(p2), p2);
        p1.sendMessage("§aDein gegner hat das kit §6" + Lobbymanager.getKittype(p2).name() + " §agewählt");
        p2.sendMessage("§aDein gegner hat das kit §6" + Lobbymanager.getKittype(p1).name() + " §agewählt");
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event){
        try {
            for (Gamemanager manager : games){
                if (event.getEntity().getWorld().equals(manager.map)){
                    event.setDeathMessage(null);
                    event.setKeepInventory(true);
                    handleWinning(event.getEntity().getWorld(), manager, event.getEntity());
                    break;
                }
            }
            Location spawn = Lobbymanager.getSpawn();
            spawn.setY(spawn.getY() + 1);
            event.getEntity().spigot().respawn();
            event.getEntity().teleport(spawn);
        }catch (Exception e) {}
    }

    public static void handleWinning(World world, Gamemanager manager, Player looser){
        try {
            Location spawn = Lobbymanager.getSpawn();
            spawn.setY(spawn.getY() + 1);
            for (Player p : world.getPlayers()){
                if (!p.getUniqueId().equals(looser.getUniqueId())){
                    p.sendMessage("§aDu hast den Kampf gewonnen");
                    looser.spigot().respawn();
                    looser.teleport(spawn);
                    Lobbymanager.giveInv(looser);
                    addLoose(looser);
                    looser.sendMessage("§6" + p.getDisplayName() + " §ahatte noch §b" + ((int)p.getHealth() / 2) + " §aHerzen");
                    p.teleport(spawn);
                    Lobbymanager.giveInv(p);
                    addWin(p);
                }
            }
            Bukkit.unloadWorld(world, false);
            deleteWorld(world.getWorldFolder());
            games.remove(manager);
        }catch (Exception e){}
    }

}
