package de.craftix.onevone.utils.kit;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Kits {

    public static ArrayList<Kit> kits = new ArrayList<>();

    public static void setup(){
        //UHC
        Kit uhc = new Kit(Kittype.UHC);
        uhc.items.put(6, new ItemStack(Material.SANDSTONE, 64));
        uhc.items.put(7, new ItemStack(Material.SANDSTONE, 64));
        uhc.items.put(4, new ItemStack(Material.LAVA_BUCKET));
        uhc.items.put(13, new ItemStack(Material.LAVA_BUCKET));
        uhc.items.put(5, new ItemStack(Material.WATER_BUCKET));
        uhc.items.put(14, new ItemStack(Material.WATER_BUCKET));
        uhc.items.put(1, new ItemStack(Material.BOW));
        uhc.items.put(9, new ItemStack(Material.ARROW, 16));
        ItemStack uhcSword = new ItemStack(Material.DIAMOND_SWORD);
        uhcSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        uhc.items.put(0, uhcSword);
        uhc.items.put(3, new ItemStack(Material.GOLDEN_APPLE, 4));
        uhc.items.put(8, new ItemStack(Material.COOKED_BEEF, 32));
        uhc.items.put(2, new ItemStack(Material.FISHING_ROD));
        uhc.helmet = new ItemStack(Material.IRON_HELMET);
        uhc.chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        uhc.leggins = new ItemStack(Material.IRON_LEGGINGS);
        uhc.boots = new ItemStack(Material.IRON_BOOTS);
        uhc.invItem = createInvItem(Material.GOLDEN_APPLE, "§eUHC");
        kits.add(uhc);
        //OP
        Kit op = new Kit(Kittype.OP);
        ItemStack opHelmet = new ItemStack(Material.DIAMOND_HELMET);
        opHelmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        ItemStack opChestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
        opChestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        ItemStack opLeggins = new ItemStack(Material.DIAMOND_LEGGINGS);
        opLeggins.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        ItemStack opBoots = new ItemStack(Material.DIAMOND_BOOTS);
        opBoots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        op.helmet = opHelmet;
        op.chestplate = opChestplate;
        op.leggins = opLeggins;
        op.boots = opBoots;
        op.items.put(8, new ItemStack(Material.COOKED_BEEF, 32));
        ItemStack opSword = new ItemStack(Material.WOOD_SWORD);
        opSword.addEnchantment(Enchantment.DURABILITY, 1);
        op.items.put(0, opSword);
        op.invItem = createInvItem(Material.COMMAND, "§cOP");
        kits.add(op);
        //SG
        Kit sg = new Kit(Kittype.SG);
        sg.helmet = new ItemStack(Material.IRON_HELMET);
        sg.chestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
        sg.leggins = new ItemStack(Material.DIAMOND_LEGGINGS);
        sg.boots = new ItemStack(Material.IRON_BOOTS);
        ItemStack sgSword = new ItemStack(Material.IRON_SWORD);
        sgSword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
        sg.items.put(0, sgSword);
        sg.items.put(8, new ItemStack(Material.COOKED_BEEF, 32));
        sg.items.put(4, new ItemStack(Material.GOLDEN_APPLE, 4));
        sg.items.put(2, new ItemStack(Material.FISHING_ROD));
        ItemStack sgBow = new ItemStack(Material.BOW);
        sgBow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        sg.items.put(1, sgBow);
        sg.items.put(9, new ItemStack(Material.ARROW, 4));
        sg.invItem = createInvItem(Material.FISHING_ROD, "§eSG");
        kits.add(sg);
        //TANK
        Kit tank = new Kit(Kittype.TANK);
        ItemStack tankHelmet = new ItemStack(Material.IRON_HELMET);
        tankHelmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        ItemStack tankChestplate = new ItemStack(Material.IRON_CHESTPLATE);
        tankChestplate.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3);
        ItemStack tankLeggins = new ItemStack(Material.IRON_LEGGINGS);
        tankLeggins.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        ItemStack tankBoots = new ItemStack(Material.IRON_BOOTS);
        tankBoots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        tank.helmet = tankHelmet;
        tank.chestplate = tankChestplate;
        tank.leggins = tankLeggins;
        tank.boots = tankBoots;
        tank.items.put(8, new ItemStack(Material.COOKED_BEEF, 32));
        ItemStack tankSword = new ItemStack(Material.STONE_SWORD);
        tankSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        tank.items.put(0, tankSword);
        tank.invItem = createInvItem(Material.IRON_CHESTPLATE, "§eTANK");
        kits.add(tank);
    }

    public static Kit getKit(Kittype type){
        for (Kit kit : kits){
            if (kit.type == type) return kit;
        }
        return null;
    }

    public static Kittype getTypeByID(int id){
        //0 = RANDOM
        if (id == 0) return Kittype.RANDOM;
        //1 = UHC
        if (id == 1) return Kittype.UHC;
        //2 = OP
        if (id == 2) return Kittype.OP;
        //3 = SG
        if (id == 3) return Kittype.SG;
        //4 = TANK
        if (id == 4) return Kittype.TANK;
        return Kittype.RANDOM;
    }

    public static int getKitID(Kittype type){
        if (type == Kittype.RANDOM) return 0;
        if (type == Kittype.UHC) return 1;
        if (type == Kittype.OP) return 2;
        if (type == Kittype.SG) return 3;
        if (type == Kittype.TANK) return 4;
        return 0;
    }

    private static void setItems(Kit kit, Player p){
        p.getInventory().setHelmet(kit.helmet);
        p.getInventory().setChestplate(kit.chestplate);
        p.getInventory().setLeggings(kit.leggins);
        p.getInventory().setBoots(kit.boots);
        for (int slot : kit.items.keySet()){
            p.getInventory().setItem(slot, kit.items.get(slot));
        }
        p.getInventory().setHeldItemSlot(0);
    }

    public static void preparePlayer(Kittype kit, Player p){
        p.getInventory().setHelmet(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setBoots(null);
        p.getInventory().clear();
        if (kit == Kittype.RANDOM){
            List<Kit> allowedKits = new ArrayList<>();
            for (Kit k : kits){
                if (p.hasPermission(k.permission)) allowedKits.add(k);
            }
            int rand = new Random().nextInt(allowedKits.size() - 1);
            setItems(allowedKits.get(rand), p);
        }else setItems(getKit(kit), p);
    }

    private static ItemStack createInvItem(Material type, String name){
        ItemStack item = new ItemStack(type);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }

}
