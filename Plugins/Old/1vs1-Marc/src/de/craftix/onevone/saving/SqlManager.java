package de.craftix.onevone.saving;

public class SqlManager {

    public static void createTables(){
        if (!MySQL.isConnected()) MySQL.connect();
        //World Presets
        MySQL.insert("CREATE TABLE IF NOT EXISTS worlds (Name VARCHAR(100), x1 VARCHAR(100), y1 VARCHAR(100), z1 VARCHAR(100), yaw1 VARCHAR(100), pitch1 VARCHAR(100), x2 VARCHAR(100), y2 VARCHAR(100), z2 VARCHAR(100), yaw2 VARCHAR(100), pitch2 VARCHAR(100), CName VARCHAR(100))");
        //Scores
        MySQL.insert("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(100), Wins INT(100), Looses INT(100))");
        //Lobby Data
        MySQL.insert("CREATE TABLE IF NOT EXISTS Kits (UUID VARCHAR(100), Kit INT(10))");
        //Lobby
        MySQL.insert("CREATE TABLE IF NOT EXISTS lobby (World VARCHAR(100), x VARCHAR(100), y VARCHAR(100), z VARCHAR(100), yaw VARCHAR(100), pitch VARCHAR(100))");
    }

}
