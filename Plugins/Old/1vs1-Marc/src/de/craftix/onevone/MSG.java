package de.craftix.onevone;

public class MSG {

    public static final String onlyPlayer = "§cNur Spieler können diesen Command benutzen!";
    public static final String noPerm = "§cDu hast nicht genug Rechte, um diesen Command zu benutzen!";
    public static final String syntax = "§cFalscher Syntax!";

}
