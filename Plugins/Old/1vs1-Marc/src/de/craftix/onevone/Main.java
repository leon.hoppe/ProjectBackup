package de.craftix.onevone;

import de.craftix.onevone.commands.*;
import de.craftix.onevone.listener.onJoin;
import de.craftix.onevone.listener.onLobby;
import de.craftix.onevone.saving.*;
import de.craftix.onevone.utils.*;
import de.craftix.onevone.utils.kit.Kits;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    public static ArrayList<FightWorld> worlds = new ArrayList<>();

    @Override
    public void onEnable() {
        MySQL.connect();
        SqlManager.createTables();

        Kits.setup();

        World lobby = Lobbymanager.getLobby();
        if (lobby != null) {
            lobby.setGameRuleValue("randomTickSpeed", "0");
            lobby.setGameRuleValue("doFireTick", "false");
            lobby.setGameRuleValue("doMobSpawning", "false");
            lobby.setGameRuleValue("doDaylightCycle", "false");
            lobby.setAnimalSpawnLimit(0);
            lobby.setMonsterSpawnLimit(0);
            lobby.setAutoSave(true);
            lobby.setDifficulty(Difficulty.PEACEFUL);
        }

        getCommand("addworld").setExecutor(new AddWorld());
        getCommand("setspawn").setExecutor(new SetSpawn());
        getCommand("saveworld").setExecutor(new SaveWorld());
        getCommand("setlobby").setExecutor(new SetLobby());
        getCommand("stats").setExecutor(new Stats());

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onJoin(), this);
        pm.registerEvents(new onLobby(), this);
        pm.registerEvents(new Lobbymanager(), this);
        pm.registerEvents(new Gamemanager(), this);
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
        for (Player p : Bukkit.getOnlinePlayers()) p.kickPlayer("Server restart");
        for (Gamemanager games : Gamemanager.games){
            World map = games.map;
            Bukkit.unloadWorld(map, false);
            Gamemanager.deleteWorld(map.getWorldFolder());
        }
    }

    public static FightWorld getWorld(String cName){
        for (FightWorld world : worlds){
            if (world.cName.equalsIgnoreCase(cName)) return world;
        }
        return null;
    }
}
