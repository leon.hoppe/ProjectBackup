package de.craftix.onevone.commands;

import de.craftix.onevone.MSG;
import de.craftix.onevone.Main;
import de.craftix.onevone.Perms;
import de.craftix.onevone.utils.FightWorld;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SaveWorld implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player) sender;
            if (p.hasPermission(Perms.setup)){
                if (args.length == 1){
                    FightWorld world = Main.getWorld(args[0]);
                    if (world != null){
                        if (world.spawn1 != null && world.spawn2 != null && world.world != null && world.cName != null){
                            FightWorld.saveWorld(world);
                            p.sendMessage("§aDie Welt wurde erfolgreich gespeichert!");
                        }else {
                            p.sendMessage("§cDie Welt wurde noch nicht fertig konfiguriert!");
                        }
                    }else {
                        p.sendMessage("§cDiese Welt existiert nicht!");
                    }
                }else {
                    p.sendMessage(MSG.syntax);
                }
            }else {
                p.sendMessage(MSG.noPerm);
            }
        }else {
            sender.sendMessage(MSG.onlyPlayer);
        }
        return true;
    }
}
