package de.craftix.onevone.commands;

import de.craftix.onevone.MSG;
import de.craftix.onevone.utils.Gamemanager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Stats implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player) sender;
            List<Integer> stats = Gamemanager.getStats(p);
            int wins = stats.get(0);
            int looses = stats.get(1);
            p.sendMessage("§6------------§bStats§6------------");
            p.sendMessage("§aGewonnene Matches: §e" + wins);
            p.sendMessage("§cVerlorene Matches: §e" + looses);
            p.sendMessage("§6-----------------------------");
        }else sender.sendMessage(MSG.onlyPlayer);
        return true;
    }
}
