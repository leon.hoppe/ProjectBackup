package de.craftix.onevone.commands;

import de.craftix.onevone.MSG;
import de.craftix.onevone.Perms;
import de.craftix.onevone.saving.MySQL;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLobby implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player)sender;
            if (p.hasPermission(Perms.setup)){
                Location loc = p.getLocation();
                MySQL.insert("DELETE FROM lobby");
                String qry1 = "INSERT INTO lobby (World, x, y, z, yaw, pitch) VALUES (";
                String qry2 = "\"" + loc.getWorld().getName() + "\", \"" + loc.getX() + "\", \"" + loc.getY() + "\", \"" + loc.getZ() + "\", \"" + loc.getYaw() + "\", \"" + loc.getPitch() + "\")";
                MySQL.insert(qry1 + qry2);
                p.sendMessage("§aDie Lobby wurde gesetzt");
            }else p.sendMessage(MSG.noPerm);
        }else sender.sendMessage(MSG.onlyPlayer);
        return true;
    }
}
