package de.craftix.onevone.commands;

import de.craftix.onevone.MSG;
import de.craftix.onevone.Main;
import de.craftix.onevone.Perms;
import de.craftix.onevone.utils.FightWorld;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player) sender;
            if (p.hasPermission(Perms.setup)){
                if (args.length == 2 && (args[1].equalsIgnoreCase("1") || args[1].equalsIgnoreCase("2"))){
                    FightWorld world = Main.getWorld(args[0]);
                    if(world != null){
                        if (args[1].equalsIgnoreCase("1")){
                            world.spawn1 = p.getLocation();
                        }else {
                            world.spawn2 = p.getLocation();
                        }
                        world.world = p.getLocation().getWorld();
                        p.sendMessage("§aDer Spawn §6" + args[1] + " §awurde hinzugefügt");
                    }else {
                        p.sendMessage("§cDiese Welt existiert nicht!");
                    }
                }else {
                    p.sendMessage(MSG.syntax);
                }
            }else {
                p.sendMessage(MSG.noPerm);
            }
        }else {
            sender.sendMessage(MSG.onlyPlayer);
        }
        return true;
    }
}
