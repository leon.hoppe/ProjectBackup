package de.craftix.onevone.commands;

import de.craftix.onevone.MSG;
import de.craftix.onevone.Main;
import de.craftix.onevone.Perms;
import de.craftix.onevone.utils.FightWorld;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddWorld implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player) sender;
            if (p.hasPermission(Perms.setup)){
                if (args.length == 1){
                    if (FightWorld.checkName(args[0])){
                        FightWorld world = new FightWorld();
                        world.cName = args[0];
                        Main.worlds.add(world);
                        p.sendMessage("§aDie Welt wurde erstellt!");
                    }else {
                        p.sendMessage("§cDieser Name ist bereits vergeben!");
                    }
                }else {
                    p.sendMessage(MSG.syntax);
                }
            }else {
                p.sendMessage(MSG.noPerm);
            }
        }else {
            sender.sendMessage(MSG.onlyPlayer);
        }
        return true;
    }
}
