package de.craftix.onevone.listener;

import de.craftix.onevone.utils.Gamemanager;
import de.craftix.onevone.utils.Lobbymanager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent e){
        e.setJoinMessage(null);
        Location spawn = Lobbymanager.getSpawn();
        if (spawn != null) e.getPlayer().teleport(spawn);
        Lobbymanager.giveInv(e.getPlayer());
        if (Gamemanager.getStats(e.getPlayer()) == null){
            Gamemanager.setStats(e.getPlayer(), 0, 0);
        }
    }

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent e){
        e.setQuitMessage(null);
        Player p = e.getPlayer();
        Gamemanager manager = null;
        for (Gamemanager game : Gamemanager.games){
            if (game.p1 == p || game.p2 == p) {
                manager = game;
                break;
            }
        }
        if (manager != null) Gamemanager.handleWinning(manager.map, manager, p);
    }

}
