package de.craftix.onevone.listener;

import de.craftix.onevone.saving.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.sql.ResultSet;

public class onLobby implements Listener {

    private World getLobby(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM lobby");
            rs.next();
            return Bukkit.getWorld(rs.getString("World"));
        }catch (Exception e) {}
        return null;
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e){
        if (e.getBlock().getWorld().equals(getLobby())) e.setCancelled(true);
        else if (!e.getBlock().getType().equals(Material.SANDSTONE)) e.setCancelled(true);
    }

    @EventHandler
    public void onBuild(BlockPlaceEvent e){
        if (e.getBlock().getWorld().equals(getLobby())) e.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        try {
            if (e.getClickedBlock().getWorld().equals(getLobby())) e.setCancelled(true);
        }catch (Exception exception) {}
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if (e.getEntity().getWorld().equals(getLobby())) e.setCancelled(true);
    }

    @EventHandler
    public void onHunger(FoodLevelChangeEvent e){
        if (e.getEntity().getWorld().equals(getLobby())) {
            e.setCancelled(true);
            ((Player) e.getEntity()).setFoodLevel(20);
            ((Player) e.getEntity()).setSaturation(5);
        }
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e){
        if (e.getWhoClicked().getWorld().equals(getLobby())) e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e){
        if (e.getItemDrop().getWorld().equals(getLobby())) e.setCancelled(true);
    }

    @EventHandler
    public void onPickUP(PlayerPickupItemEvent e){
        if (e.getItem().getWorld().equals(getLobby())) e.setCancelled(true);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onMobSpawn(EntitySpawnEvent e){
        if (e.getEntity() instanceof Player) return;
        e.setCancelled(true);
    }

}
