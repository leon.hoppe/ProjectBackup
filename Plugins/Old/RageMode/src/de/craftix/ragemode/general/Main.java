package de.craftix.ragemode.general;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main plugin;
    public static FileConfiguration config;

    @Override
    public void onEnable() {
        plugin = this;
        config = this.getConfig();
    }

    @Override
    public void onDisable() {

    }

    public static Main getPlugin(){
        return plugin;
    }
}
