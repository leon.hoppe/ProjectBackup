package de.craftix.challenge.listener;

import de.craftix.challenge.Main;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;

public class onInv implements Listener {

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (!Main.getTimer().isStarted) return;
        if (event.getWhoClicked().getGameMode().equals(GameMode.SPECTATOR)) return;
        for (Material m : Main.items){
            if (event.getCurrentItem().getType().equals(m)) {
                event.setCancelled(true);
                Main.loose();
            }
        }
    }

    @EventHandler
    public void onPickUp(EntityPickupItemEvent event){
        if (!Main.getTimer().isStarted) return;
        if (event.getEntity() instanceof Player){
            Player p = (Player)event.getEntity();
            if (p.getGameMode().equals(GameMode.SPECTATOR)) return;
            for (Material m : Main.items){
                if (event.getItem().getItemStack().getType().equals(m)) Main.loose();
            }
        }
    }

}
