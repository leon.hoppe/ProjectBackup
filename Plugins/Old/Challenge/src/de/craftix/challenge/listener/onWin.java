package de.craftix.challenge.listener;

import de.craftix.challenge.Main;
import org.bukkit.entity.EnderDragon;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class onWin implements Listener {

    @EventHandler
    public void onDeath(EntityDeathEvent event){
        if (!Main.getTimer().isStarted) return;
        if (!(event.getEntity() instanceof EnderDragon)) return;
        Main.win();
    }

}
