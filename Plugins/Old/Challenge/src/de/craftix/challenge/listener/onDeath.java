package de.craftix.challenge.listener;

import de.craftix.challenge.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class onDeath implements Listener {

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event){
        if (!Main.getTimer().isStarted) return;
        Main.loose();
    }

}
