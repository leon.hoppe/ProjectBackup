package de.craftix.challenge.utils;

import de.craftix.challenge.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Timer implements CommandExecutor {

    private Integer id;
    private Integer stopID;
    public int seconds = 0;
    public int minutes = 0;
    public int hours = 0;
    public boolean isStarted = false;

    public Timer(){
        stopID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Player p : Bukkit.getOnlinePlayers()){
                    ActionBar.show(p, "§6Der Timer ist pausiert");
                }
            }
        }, 1, 20);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player)sender;
            if (p.hasPermission("challenge.timer")){
                if (args[0].equalsIgnoreCase("start") || args[0].equalsIgnoreCase("resume")){
                    start();
                }else if (args[0].equalsIgnoreCase("stop")){
                    stop();
                }else if (args[0].equalsIgnoreCase("reset")){
                    reset();
                }
            }
        }
        return false;
    }

    public void start(){
        isStarted = true;
        for (Player all : Bukkit.getOnlinePlayers()){
            if (!all.getGameMode().equals(GameMode.SPECTATOR)) all.setGameMode(GameMode.SURVIVAL);
        }
        try {
            Bukkit.getScheduler().cancelTask(stopID);
            id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    seconds++;
                    if (seconds >= 60){
                        seconds = 0;
                        minutes++;
                    }
                    if (minutes >= 60){
                        minutes = 0;
                        hours++;
                    }
                    for (Player p : Bukkit.getOnlinePlayers()){
                        String SHours;
                        String SMinutes;
                        String SSeconds;
                        if (seconds <= 9) SSeconds = "0" + seconds;
                        else SSeconds = "" + seconds;
                        if (minutes <= 9) SMinutes = "0" + minutes;
                        else SMinutes = "" + minutes;
                        if (hours <= 9) SHours = "0" + hours;
                        else SHours = "" + hours;

                        ActionBar.show(p, "§aChallenge: §b" + SHours + ":" + SMinutes + ":" + SSeconds);
                    }
                }
            }, 1, 20);
        }catch (Exception e){
            System.err.println(e.toString());
        }
    }

    public void stop(){
        isStarted = false;
        try {
            Bukkit.getScheduler().cancelTask(id);
            stopID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (Player p : Bukkit.getOnlinePlayers()){
                        ActionBar.show(p, "§6Der Timer ist pausiert");
                    }
                }
            }, 1, 20);
        }catch (Exception e){
            System.err.println(e.toString());
        }
    }

    public void reset(){
        seconds = 0;
        minutes = 0;
        hours = 0;
    }
}
