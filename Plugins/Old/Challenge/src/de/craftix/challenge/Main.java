package de.craftix.challenge;

import de.craftix.challenge.commands.Additem;
import de.craftix.challenge.commands.Invsee;
import de.craftix.challenge.commands.Items;
import de.craftix.challenge.commands.Position;
import de.craftix.challenge.listener.onDeath;
import de.craftix.challenge.listener.onInv;
import de.craftix.challenge.listener.onStart;
import de.craftix.challenge.listener.onWin;
import de.craftix.challenge.utils.Timer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {
    public static ArrayList<Material> items = new ArrayList<>();

    private static Main instance;
    private static Timer timer;

    @Override
    public void onEnable() {
        instance = this;
        timer = new Timer();
        PluginManager pm = Bukkit.getPluginManager();

        getCommand("timer").setExecutor(timer);
        getCommand("additem").setExecutor(new Additem());
        getCommand("position").setExecutor(new Position());
        getCommand("invsee").setExecutor(new Invsee());

        pm.registerEvents(new onInv(), this);
        pm.registerEvents(new onDeath(), this);
        pm.registerEvents(new onWin(), this);
        pm.registerEvents(new onStart(), this);

        Items item = new Items();
        getCommand("items").setExecutor(item);
        pm.registerEvents(item, this);

        addItems();
    }

    public static Main getInstance() {
        return instance;
    }

    public static Timer getTimer() {
        return timer;
    }

    public static void loose(){
        timer.stop();
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setGameMode(GameMode.SPECTATOR);
        }
    }

    public static void win(){
        timer.stop();
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setGameMode(GameMode.SPECTATOR);
        }
        Bukkit.broadcastMessage("§6Die Challenge wurde erfolgreich absolviert");
    }

    public static void addItems(){
        items.add(Material.OAK_LOG);
        items.add(Material.SPRUCE_LOG);
        items.add(Material.BIRCH_LOG);
        items.add(Material.JUNGLE_LOG);
        items.add(Material.ACACIA_LOG);
        items.add(Material.DARK_OAK_LOG);
        items.add(Material.CRIMSON_STEM);
        items.add(Material.WARPED_STEM);
        items.add(Material.OAK_PLANKS);
        items.add(Material.SPRUCE_PLANKS);
        items.add(Material.BIRCH_PLANKS);
        items.add(Material.JUNGLE_PLANKS);
        items.add(Material.ACACIA_PLANKS);
        items.add(Material.DARK_OAK_PLANKS);
        items.add(Material.WARPED_PLANKS);
        items.add(Material.CRIMSON_PLANKS);
        items.add(Material.OAK_SLAB);
        items.add(Material.SPRUCE_SLAB);
        items.add(Material.BIRCH_SLAB);
        items.add(Material.JUNGLE_SLAB);
        items.add(Material.ACACIA_SLAB);
        items.add(Material.DARK_OAK_SLAB);
        items.add(Material.WARPED_SLAB);
        items.add(Material.CRIMSON_SLAB);
        items.add(Material.OAK_STAIRS);
        items.add(Material.SPRUCE_STAIRS);
        items.add(Material.BIRCH_STAIRS);
        items.add(Material.JUNGLE_STAIRS);
        items.add(Material.ACACIA_STAIRS);
        items.add(Material.DARK_OAK_STAIRS);
        items.add(Material.WARPED_STAIRS);
        items.add(Material.CRIMSON_STAIRS);
        items.add(Material.OAK_FENCE);
        items.add(Material.SPRUCE_FENCE);
        items.add(Material.BIRCH_FENCE);
        items.add(Material.JUNGLE_FENCE);
        items.add(Material.ACACIA_FENCE);
        items.add(Material.DARK_OAK_FENCE);
        items.add(Material.WARPED_FENCE);
        items.add(Material.CRIMSON_FENCE);
        items.add(Material.OAK_PRESSURE_PLATE);
        items.add(Material.SPRUCE_PRESSURE_PLATE);
        items.add(Material.BIRCH_PRESSURE_PLATE);
        items.add(Material.JUNGLE_PRESSURE_PLATE);
        items.add(Material.ACACIA_PRESSURE_PLATE);
        items.add(Material.DARK_OAK_PRESSURE_PLATE);
        items.add(Material.WARPED_PRESSURE_PLATE);
        items.add(Material.CRIMSON_PRESSURE_PLATE);
        items.add(Material.OAK_BUTTON);
        items.add(Material.SPRUCE_BUTTON);
        items.add(Material.BIRCH_BUTTON);
        items.add(Material.JUNGLE_BUTTON);
        items.add(Material.ACACIA_BUTTON);
        items.add(Material.DARK_OAK_BUTTON);
        items.add(Material.WARPED_BUTTON);
        items.add(Material.CRIMSON_BUTTON);
        items.add(Material.COMPOSTER);
        items.add(Material.CRAFTING_TABLE);
        items.add(Material.CHEST);
        items.add(Material.LADDER);
        items.add(Material.WOODEN_AXE);
        items.add(Material.WOODEN_HOE);
        items.add(Material.WOODEN_PICKAXE);
        items.add(Material.WOODEN_SHOVEL);
        items.add(Material.WOODEN_SWORD);
    }
}
