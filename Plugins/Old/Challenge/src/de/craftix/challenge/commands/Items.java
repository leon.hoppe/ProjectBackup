package de.craftix.challenge.commands;

import de.craftix.challenge.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Items implements CommandExecutor, Listener {
    private final String name = "Items";
    private final int size = 9*3;
    private ArrayList<Inventory> invs = new ArrayList<>();
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player p = (Player)sender;
        ArrayList<ItemStack> items = new ArrayList<>();
        for (Material m : Main.items){
            items.add(new ItemStack(m));
        }
        createInvs(items);
        showInv(1, p);
        return false;
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        try {
            if (!event.getView().getTitle().equalsIgnoreCase(name)) return;
            event.setCancelled(true);
            if (event.getCurrentItem() == null) return;
            ItemStack clickedItem = event.getCurrentItem();
            if (clickedItem.getType().equals(Material.GREEN_STAINED_GLASS_PANE)){
                int invCount = Integer.parseInt(clickedItem.getItemMeta().getLore().get(0)) - 1;
                showInv(invCount + 1, (Player) event.getWhoClicked());
            }
            if (clickedItem.getType().equals(Material.RED_STAINED_GLASS_PANE)){
                int invCount = Integer.parseInt(clickedItem.getItemMeta().getLore().get(0)) - 1;
                showInv(invCount + 1, (Player) event.getWhoClicked());
            }
        }catch (Exception e){

        }
    }

    private void showInv(int id, Player p){
        p.openInventory(invs.get(id - 1));
    }

    private void createInvs(ArrayList<ItemStack> items){
        if (items.size() <= 0) return;
        invs.clear();
        while (true){
            Inventory inv = Bukkit.createInventory(null, size, name);
            inv.setItem(45, getItems(2, invs.size()));
            inv.setItem(46, getItems(3, invs.size()));
            inv.setItem(47, getItems(3, invs.size()));
            inv.setItem(48, getItems(3, invs.size()));
            inv.setItem(49, getItems(3, invs.size()));
            inv.setItem(50, getItems(3, invs.size()));
            inv.setItem(51, getItems(3, invs.size()));
            inv.setItem(52, getItems(3, invs.size()));
            inv.setItem(53, getItems(1, invs.size()));
            for (int i = 0; i < inv.getSize(); i++){
                if (items.size() <= 0) break;
                if (inv.getItem(i) != null) continue;
                inv.setItem(i, items.get(0));
                items.remove(0);
            }
            invs.add(inv);
            if (items.size() <= 0) break;
        }
    }

    private ItemStack getItems(int color, int index){
        if (color == 1){
            ItemStack i = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
            ItemMeta im = i.getItemMeta();
            im.setDisplayName("§aNächste Seite");
            ArrayList<String> lore = new ArrayList<>();
            int ram = index + 2;
            lore.add(String.valueOf(ram));
            im.setLore(lore);
            i.setItemMeta(im);
            return i;
        }
        if (color == 2){
            ItemStack i = new ItemStack(Material.RED_STAINED_GLASS_PANE);
            ItemMeta im = i.getItemMeta();
            im.setDisplayName("§cVorherige Seite");
            ArrayList<String> lore = new ArrayList<>();
            int ram = index;
            lore.add(String.valueOf(ram));
            im.setLore(lore);
            i.setItemMeta(im);
            return i;
        }
        if (color == 3){
            ItemStack i = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
            ItemMeta im = i.getItemMeta();
            im.setDisplayName(" ");
            i.setItemMeta(im);
            return i;
        }
        return null;
    }
}
