package de.craftix.challenge.commands;

import de.craftix.challenge.Main;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Additem implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player p = (Player)sender;
        if (!p.hasPermission("challenge.additem")) return false;
        Main.items.add(p.getInventory().getItemInMainHand().getType());
        p.sendMessage("§aDas Item wurde in die Liste ver Verbotenen Items aufgenommen");
        return false;
    }
}
