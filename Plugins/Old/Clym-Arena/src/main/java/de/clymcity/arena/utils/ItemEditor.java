package de.clymcity.arena.utils;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class ItemEditor {

    public ItemStack item;

    public ItemEditor(ItemStack item){
        this.item = item;
    }

    public ItemStack setGlowing(boolean value){
        if (!value) return item;
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 1, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(meta);
        return item;
    }

    public ItemStack setName(String name){
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name.replace('&', '§'));
        item.setItemMeta(meta);
        return item;
    }

    public ItemStack addLore(String... text){
        ItemMeta meta = item.getItemMeta();
        ArrayList<String> lore = (ArrayList<String>) meta.getLore();
        lore.addAll(Arrays.asList(text));
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

}
