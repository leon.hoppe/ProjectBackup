package de.clymcity.arena;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    public static final String server = "localhost";
    public static final Integer port = 3306;
    public static final String db = "Arena";
    public static final String user = "Arena";
    public static final String pass = "Arena";
    private static Connection con = null;

    public static final String spawns = "arena_Spawns";
    public static final String stats = "arena_Stats";
    public static final String kits = "arena_Kits";

    public static boolean connect(){
        String conString = "jdbc:mysql://" + server + ":" + port + "/" + db;
        try {
            con = DriverManager.getConnection(conString, user, pass);
            System.out.println("[Arena] MySQL connected successfully");
            return true;
        }catch (SQLException e){
            System.out.println("[Arena] MySQL connection failed");
            return false;
        }
    }

    public static boolean disconnect(){
        if (!isConnected()) return true;
        try {
            con.close();
            con = null;
            System.out.println("[Arena] MySQL disconnected");
            return true;
        }catch (SQLException e){
            System.out.println("[Arena] MySQL disconnecting failed");
            return false;
        }
    }

    public static boolean isConnected(){
        return con != null;
    }

    public static void insert(String qry){
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static ResultSet getData(String qry){
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}

