package de.clymcity.arena.utils;

import de.clymcity.arena.Main;
import de.clymcity.arena.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SqlSaving {

    //Spawns
    public static class Spawntypes {
        public static final int lobby = 0;
        public static final int arenaMiddle = 1;
        public static final int grandstand = 2;
        public static final int team1 = 3;
        public static final int team2 = 4;
    }

    public static Location getSpawn(int type){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM " + MySQL.spawns + " WHERE Type = " + type);
            if (rs.next()){
                World world = Bukkit.getWorld(rs.getString("World"));
                double x = Double.parseDouble(rs.getString("x"));
                double y = Double.parseDouble(rs.getString("y"));
                double z = Double.parseDouble(rs.getString("z"));
                float yaw = Float.parseFloat(rs.getString("yaw"));
                float pitch = Float.parseFloat(rs.getString("pitch"));
                return new Location(world, x, y, z, yaw, pitch);
            }
        }catch (Exception e) {}
        return null;
    }

    public static ArrayList<Location> getSpawns(int type){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM " + MySQL.spawns + " WHERE Type = " + type);
            ArrayList<Location> spawns = new ArrayList<>();
            while (rs.next()){
                World world = Bukkit.getWorld(rs.getString("World"));
                double x = Double.parseDouble(rs.getString("x"));
                double y = Double.parseDouble(rs.getString("y"));
                double z = Double.parseDouble(rs.getString("z"));
                float yaw = Float.parseFloat(rs.getString("yaw"));
                float pitch = Float.parseFloat(rs.getString("pitch"));
                spawns.add(new Location(world, x, y, z, yaw, pitch));
            }
            return spawns;
        }catch (Exception e) {}
        return null;
    }

    public static void addSpawn(Location spawn, int type){
        String world = spawn.getWorld().getName();
        String x = String.valueOf(spawn.getX());
        String y = String.valueOf(spawn.getY());
        String z = String.valueOf(spawn.getZ());
        String yaw = String.valueOf(spawn.getYaw());
        String pitch = String.valueOf(spawn.getPitch());
        MySQL.insert("INSERT INTO " + MySQL.spawns + " (Type, World, x, y, z, yaw, pitch) VALUES (" + type + ", \"" + world + "\", \"" + x + "\", \"" + y + "\", \"" + z + "\", \"" + yaw + "\", \"" + pitch + "\")");
    }

    public static void addSpawns(ArrayList<Location> spawns, int type){
        for (Location spawn : spawns){
            String world = spawn.getWorld().getName();
            String x = String.valueOf(spawn.getX());
            String y = String.valueOf(spawn.getY());
            String z = String.valueOf(spawn.getZ());
            String yaw = String.valueOf(spawn.getYaw());
            String pitch = String.valueOf(spawn.getPitch());
            MySQL.insert("INSERT INTO " + MySQL.spawns + " (Type, World, x, y, z, yaw, pitch) VALUES (" + type + ", \"" + world + "\", \"" + x + "\", \"" + y + "\", \"" + z + "\", \"" + yaw + "\", \"" + pitch + "\")");        }
    }

    public static void setSpawn(Location spawn, int type){
        MySQL.insert("DELETE FROM " + MySQL.spawns + " WHERE Type = " + type);
        addSpawn(spawn, type);
    }

    public static void setSpawns(ArrayList<Location> spawns, int type){
        MySQL.insert("DELETE FROM " + MySQL.spawns + " WHERE Type = " + type);
        addSpawns(spawns, type);
    }

    public static void resetSpawns(){
        MySQL.insert("DELETE FROM " + MySQL.spawns);
    }

    public static boolean checkSetup(){
        ArrayList<Location> spawns = new ArrayList<>();
        spawns.add(getSpawn(Spawntypes.lobby));
        spawns.add(getSpawn(Spawntypes.arenaMiddle));
        spawns.add(getSpawn(Spawntypes.grandstand));
        spawns.add(getSpawn(Spawntypes.team1));
        spawns.add(getSpawn(Spawntypes.team2));
        for (Location loc : spawns) if (loc == null) return false;
        return true;
    }

    //Stats
    public static class Stats {
        public Player player;
        public int kills = 0;
        public int deaths = 0;
        public int playedGames = 0;
        public int wins = 0;
        public int looses = 0;

        public Stats(Player p, int k, int d, int pg, int w, int l){
            player = p;
            kills = k;
            deaths = d;
            playedGames = pg;
            wins = w;
            looses = l;
        }
        public Stats(Player p) {
            player = p;
        }
    }

    public static Stats getStats(Player p){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM " + MySQL.stats + " WHERE UUID = \"" + p.getUniqueId() + "\"");
            if (rs.next()){
                int kills = rs.getInt("Kills");
                int deaths = rs.getInt("Deaths");
                int playedgames = rs.getInt("PlayedGames");
                int wins = rs.getInt("Wins");
                int looses = rs.getInt("Looses");
                return new Stats(p, kills, deaths, playedgames, wins, looses);
            }
        }catch (Exception e) {}
        return null;
    }

    public static void setStats(Stats s){
        MySQL.insert("DELETE FROM " + MySQL.stats + " WHERE UUID = \"" + s.player.getUniqueId() + "\"");
        MySQL.insert("INSERT INTO " + MySQL.stats + " (UUID, Kills, Deaths, PlayedGames, Wins, Looses) VALUES (\"" + s.player.getUniqueId() + "\", " + s.kills + ", " + s.deaths + ", " + s.playedGames + ", " + s.wins + ", " + s.looses + ")");
    }

    public static boolean hasStats(Player p){
        return getStats(p) != null;
    }

    public static void createStats(Player p){
        setStats(new Stats(p));
    }

    //Kits
    public static Kit getKit(int id){
        try {
            ResultSet rs = MySQL.getData("SELECT Path FROM " + MySQL.kits + " WHERE ID = " + id);
            if (rs.next()){
                String path = rs.getString("Path");
                Kit kit = (Kit) Main.ReadObjectFromFile(path);
                return kit;
            }
        }catch (Exception e) {}
        return null;
    }

    public static void setKit(Kit kit, boolean isNew){
        if (isNew){
            kit.id = getNextKitID();
            kit.path = Main.kitFilePrefix + kit.name + ".kit";
        }else deleteKit(kit);
        Main.WriteObjectToFile(kit, kit.path);
        MySQL.insert("INSERT INTO " + MySQL.kits + "(ID, Name, Path, Cost) VALUES (" + kit.id + ", \"" + kit.name + "\", \"" + kit.path + "\", " + kit.cost + ")");
    }

    public static void deleteKit(Kit kit){
        MySQL.insert("DELETE FROM " + MySQL.kits + " WHERE ID = " + kit.id);
        File file = new File(kit.path);
        file.delete();
    }

    public static int getKitID(String name){
        try {
            ResultSet rs = MySQL.getData("SELECT ID FROM " + MySQL.kits + " WHERE Name = \"" + name + "\"");
            if (rs.next()){
                return rs.getInt("ID");
            }
        }catch (Exception e) {}
        return -1;
    }

    public static ArrayList<Kit> getAllKits(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM " + MySQL.kits);
            ArrayList<Kit> kits = new ArrayList<>();
            while (rs.next()){
                if (kitEntryIsPlayer(rs)) continue;
                int id = rs.getInt("ID");
                Kit kit = getKit(id);
                kits.add(kit);
            }
            return kits;
        }catch (Exception e) {}
        return null;
    }

    private static boolean kitEntryIsPlayer(ResultSet rs){
        try {
            String uuid = rs.getString("UUID");
            return uuid != null;
        }catch (Exception e) {}
        return false;
    }

    private static int getNextKitID(){
        try {
            ResultSet rs = MySQL.getData("SELECT ID FROM " + MySQL.kits);
            ArrayList<Integer> ids = new ArrayList<>();
            while (rs.next()) ids.add(rs.getInt("ID"));
            int id = 0;
            while (ids.contains(id)) id++;
            return id;
        }catch (Exception e) {}
        return -1;
    }

    public static void setPlayerKit(Player p, Kit kit){
        MySQL.insert("DELETE FROM " + MySQL.kits + " WHERE UUID = \"" + p.getUniqueId() + "\"");
        MySQL.insert("INSERT INTO " + MySQL.kits + " (ID, UUID) VALUES (" + kit.id + ", \"" + p.getUniqueId() + "\")");
    }

    public static Kit getPlayerKit(Player p){
        try {
            ResultSet rs = MySQL.getData("SELECT ID FROM " + MySQL.kits + " WHERE UUID = \"" + p.getUniqueId() + "\"");
            if (rs.next()){
                int id = rs.getInt("ID");
                return getKit(id);
            }
        }catch (Exception e) {}
        return null;
    }

}
