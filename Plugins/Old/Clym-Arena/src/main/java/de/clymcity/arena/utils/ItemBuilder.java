package de.clymcity.arena.utils;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemBuilder {

    public Material material;
    public String name;
    public int amount = 1;
    public int damage = 1;
    public HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    public boolean unbreakable = false;
    public ArrayList<String> lore = new ArrayList<>();

    private boolean isHead = false;
    private OfflinePlayer headOwner;

    public ItemBuilder(Material mat){
        material = mat;
    }

    public ItemBuilder(Material mat, int amount){
        material = mat;
        this.amount = amount;
    }

    public ItemBuilder setName(String name){
        this.name = name;
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment e, int level){
        enchantments.put(e, level);
        return this;
    }

    public ItemBuilder setUnbreakable(boolean value){
        unbreakable = value;
        return this;
    }

    public ItemBuilder setLore(String... lore){
        for (String s : lore){
            this.lore.add(s);
        }
        return this;
    }

    public ItemBuilder setHeadOwner(OfflinePlayer p){
        isHead = true;
        headOwner = p;
        return this;
    }

    public ItemStack create(){
        if (isHead){
            ItemStack head = new ItemStack(material);
            SkullMeta meta = (SkullMeta) head.getItemMeta();
            meta.setOwningPlayer(headOwner);
            if (name != null) meta.setDisplayName(name);
            if (lore.size() != 0) meta.setLore(lore);
            head.setItemMeta(meta);
            return head;
        }
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();
        for (Enchantment e : enchantments.keySet()){
            meta.addEnchant(e, enchantments.get(e), true);
        }
        meta.setLore(lore);
        meta.setDisplayName(name);
        meta.setUnbreakable(unbreakable);
        item.setItemMeta(meta);

        return item;
    }

}
