package de.clymcity.arena.commands;

import de.clymcity.arena.utils.SqlSaving;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = (Player) sender;
        if (args.length == 0) {
            SqlSaving.Stats stats = SqlSaving.getStats(p);
            p.sendMessage("§7--------------------------");
            p.sendMessage("§7Siege: §6" + stats.wins);
            p.sendMessage("§7Niederlagen: §6" + stats.looses);
            p.sendMessage("§7Tode: §6" + stats.deaths);
            p.sendMessage("§7Eliminierungen: §6" + stats.kills);
            p.sendMessage("§7Gespielte Spiele: §6" + stats.playedGames);
            p.sendMessage("§7--------------------------");
        }else {
            Player t = Bukkit.getPlayer(args[0]);
            if (t == null) {
                p.sendMessage("§cDieser Spieler existiert nicht!");
                return true;
            }
            SqlSaving.Stats stats = SqlSaving.getStats(t);
            p.sendMessage("§7--------------------------");
            p.sendMessage("§7Siege: §6" + stats.wins);
            p.sendMessage("§7Niederlagen: §6" + stats.looses);
            p.sendMessage("§7Tode: §6" + stats.deaths);
            p.sendMessage("§7Eliminierungen: §6" + stats.kills);
            p.sendMessage("§7Gespielte Spiele: §6" + stats.playedGames);
            p.sendMessage("§7--------------------------");
        }
        return true;
    }
}
