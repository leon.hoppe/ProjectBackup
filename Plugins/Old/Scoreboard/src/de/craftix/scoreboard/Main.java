package de.craftix.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {
    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;
        ScoreboardAnimation.stopAllAnimations();
        Bukkit.getPluginManager().registerEvents(new onJoin(), this);

        for (Player p : Bukkit.getOnlinePlayers()){
            ScoreboardAnimation.removeAnimation(p);
            new ScoreboardAnimation(p, Main.getAnimation());
        }
    }

    @Override
    public void onDisable() {
        ScoreboardAnimation.stopAllAnimations();
    }

    public static Main getInstance() {
        return instance;
    }

    public static ArrayList<ScoreboardTiles> getAnimation(){
        ArrayList<ScoreboardTiles> tiles = new ArrayList<>();
        ScoreboardTiles tile = new ScoreboardTiles();

        tile.title = "Scoreboard";
        tile.setLine(1, "Test");
        tile.setLine(2, "lol");
        tiles.add(tile);

        tile = new ScoreboardTiles();
        tile.title = "Scoreboard";
        tile.setLine(1, "lol");
        tile.setLine(2, "Test");
        tiles.add(tile);

        tile = new ScoreboardTiles();
        tile.title = "Scoreboard";
        tile.setLine(1, "lol1");
        tile.setLine(2, "Test2");
        tiles.add(tile);
        return tiles;
    }
}
