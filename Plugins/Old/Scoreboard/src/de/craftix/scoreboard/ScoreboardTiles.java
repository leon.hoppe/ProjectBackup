package de.craftix.scoreboard;

import java.util.HashMap;

public class ScoreboardTiles {

    public HashMap<Integer, String> lines = new HashMap<>();
    public String title;

    public void setLine(int line, String content){
        lines.remove(lines.get(line));
        lines.put(line, content);
    }

    public void setTitle(String title){
        this.title = title;
    }

}
