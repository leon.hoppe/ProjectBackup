package de.craftix.scoreboard;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        new ScoreboardAnimation(event.getPlayer(), Main.getAnimation());
    }

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent event){
        ScoreboardAnimation.removeAnimation(event.getPlayer());
    }

}
