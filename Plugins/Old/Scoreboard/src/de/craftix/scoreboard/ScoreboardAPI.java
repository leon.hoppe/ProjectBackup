package de.craftix.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardAPI {

    public static Scoreboard createScoreboard(ScoreboardTiles tiles){
        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective ob = board.registerNewObjective("abc", "abc");
        ob.setDisplaySlot(DisplaySlot.SIDEBAR);
        ob.setDisplayName(tiles.title);
        for (int i = 0; i < 15; i++){
            if (tiles.lines.get(i) == null) continue;
            ob.getScore(tiles.lines.get(i)).setScore(i);
        }
        return board;
    }

    public static void showScoreboard(Scoreboard board, Player p){
        p.setScoreboard(board);
    }

}
