package de.craftix.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ScoreboardAnimation {
    public static ArrayList<ScoreboardAnimation> animations = new ArrayList<>();

    public static void stopAllAnimations(){
        for (ScoreboardAnimation an : animations){
            Bukkit.getScheduler().cancelTask(an.animationID);
        }
    }

    public static void removeAnimation(Player p){
        ScoreboardAnimation animation = null;
        for (ScoreboardAnimation an : animations){
            if (an.p == p) {
                Bukkit.getScheduler().cancelTask(an.animationID);
                animation = an;
            }
        }
        animations.remove(animation);
    }

    public int animationID;
    private Player p;

    public ScoreboardAnimation(Player p, ArrayList<ScoreboardTiles> tiles){
        this.p = p;
        startAnimation(tiles);
        removeAnimation(p);
        animations.add(this);
    }

    private void startAnimation(ArrayList<ScoreboardTiles> tiles){
        animationID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            int state = 0;
            @Override
            public void run() {
                if (state >= tiles.size()) state = 0;
                ScoreboardAPI.showScoreboard(ScoreboardAPI.createScoreboard(tiles.get(state)), p);
                state++;
            }
        }, 10, 10);
    }
}
