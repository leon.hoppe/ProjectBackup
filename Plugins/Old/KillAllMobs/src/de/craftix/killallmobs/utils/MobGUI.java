package de.craftix.killallmobs.utils;

import de.craftix.killallmobs.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;

public class MobGUI implements Listener {

    private static HashMap<EntityType, ItemStack> items = new HashMap<>();

    public static void showGUI(Player p, int invID){
        ArrayList<EntityType> remainingMobs = Main.entitys;
        for (EntityType e : Main.killedEntitys){
            remainingMobs.remove(e);
        }

        if (remainingMobs.size() >= 54){
            Inventory inv1 = Bukkit.createInventory(null, 6*9, "Mobs remaining site 1:");
            Inventory inv2 = Bukkit.createInventory(null, 6*9, "Mobs remaining site 2:");

            for (int i = 0; i < 54; i++){
                inv1.setItem(i, items.get(remainingMobs.get(i)));
            }
            for (int i = 54; i < remainingMobs.size(); i++){
                inv2.setItem(i - 54, items.get(remainingMobs.get(i)));
            }

            if (invID == 1) p.openInventory(inv1);
            else p.openInventory(inv2);
        }else {
            Inventory inv = Bukkit.createInventory(null, 6*9, "Mobs remaining:");
            for (int i = 0; i < remainingMobs.size(); i++){
                inv.setItem(i, items.get(remainingMobs.get(i)));
            }
            p.openInventory(inv);
        }
    }

    public static void setup(){
        setItem(EntityType.BAT, Material.TORCH);
        setItem(EntityType.BEE, Material.HONEY_BOTTLE);
        setItem(EntityType.BLAZE, Material.BLAZE_POWDER);
        setItem(EntityType.CAT, Material.NAME_TAG);
        setItem(EntityType.CAVE_SPIDER, Material.SPIDER_EYE);
        setItem(EntityType.CHICKEN, Material.FEATHER);
        setItem(EntityType.COD, Material.COOKED_COD);
        setItem(EntityType.COW, Material.LEATHER);
        setItem(EntityType.CREEPER, Material.GUNPOWDER);
        setItem(EntityType.DOLPHIN, Material.DRIED_KELP);
        setItem(EntityType.DONKEY, Material.SADDLE);
        setItem(EntityType.DROWNED, Material.TRIDENT);
        setItem(EntityType.ELDER_GUARDIAN, Material.SPONGE);
        setItem(EntityType.ENDER_DRAGON, Material.END_CRYSTAL);
        setItem(EntityType.ENDERMAN, Material.ENDER_PEARL);
        setItem(EntityType.ENDERMITE, Material.ENDER_EYE);
        setItem(EntityType.EVOKER, Material.TOTEM_OF_UNDYING);
        setItem(EntityType.FOX, Material.SWEET_BERRIES);
        setItem(EntityType.GHAST, Material.GHAST_TEAR);
        setItem(EntityType.GUARDIAN, Material.PRISMARINE_SHARD);
        setItem(EntityType.HOGLIN, Material.PORKCHOP);
        setItem(EntityType.HORSE, Material.IRON_HORSE_ARMOR);
        setItem(EntityType.HUSK, Material.ROTTEN_FLESH);
        setItem(EntityType.IRON_GOLEM, Material.POPPY);
        setItem(EntityType.LLAMA, Material.LEATHER);
        setItem(EntityType.MAGMA_CUBE, Material.MAGMA_CREAM);
        setItem(EntityType.MUSHROOM_COW, Material.RED_MUSHROOM);
        setItem(EntityType.MULE, Material.LEATHER);
        setItem(EntityType.OCELOT, Material.VINE);
        setItem(EntityType.PANDA, Material.BAMBOO);
        setItem(EntityType.PARROT, Material.COOKIE);
        setItem(EntityType.PHANTOM, Material.PHANTOM_MEMBRANE);
        setItem(EntityType.PIG, Material.PORKCHOP);
        setItem(EntityType.PIGLIN, Material.GOLD_INGOT);
        setItem(EntityType.PIGLIN_BRUTE, Material.IRON_AXE);
        setItem(EntityType.ZOMBIFIED_PIGLIN, Material.GOLDEN_SWORD);
        setItem(EntityType.PILLAGER, Material.CROSSBOW);
        setItem(EntityType.POLAR_BEAR, Material.SNOWBALL);
        setItem(EntityType.PUFFERFISH, Material.PUFFERFISH);
        setItem(EntityType.RABBIT, Material.RABBIT_HIDE);
        setItem(EntityType.RAVAGER, Material.SADDLE);
        setItem(EntityType.SALMON, Material.COOKED_SALMON);
        setItem(EntityType.SHEEP, Material.COOKED_MUTTON);
        setItem(EntityType.SHULKER, Material.SHULKER_SHELL);
        setItem(EntityType.SILVERFISH, Material.CRACKED_STONE_BRICKS);
        setItem(EntityType.SKELETON, Material.BONE);
        setItem(EntityType.SKELETON_HORSE, Material.BONE);
        setItem(EntityType.SNOWMAN, Material.SNOWBALL);
        setItem(EntityType.SPIDER, Material.COBWEB);
        setItem(EntityType.SQUID, Material.INK_SAC);
        setItem(EntityType.STRAY, Material.SPECTRAL_ARROW);
        setItem(EntityType.STRIDER, Material.STRING);
        setItem(EntityType.TRADER_LLAMA, Material.LEAD);
        setItem(EntityType.TROPICAL_FISH, Material.TROPICAL_FISH);
        setItem(EntityType.TURTLE, Material.TURTLE_HELMET);
        setItem(EntityType.VILLAGER, Material.EMERALD);
        setItem(EntityType.WANDERING_TRADER, Material.TROPICAL_FISH_BUCKET);
        setItem(EntityType.WITHER, Material.NETHER_STAR);
        setItem(EntityType.WITHER_SKELETON, Material.COAL);
        setItem(EntityType.WOLF, Material.BONE);
        setItem(EntityType.ZOGLIN, Material.ROTTEN_FLESH);
        setItem(EntityType.ZOMBIE_VILLAGER, Material.CHARCOAL);
        setItem(EntityType.WITCH, Material.REDSTONE);
        setItem(EntityType.ZOMBIE, Material.ROTTEN_FLESH);
        setItem(EntityType.VINDICATOR, Material.IRON_AXE);
        setItem(EntityType.VEX, Material.IRON_SWORD);
        setItem(EntityType.SLIME, Material.SLIME_BALL);
    }

    private static void setItem(EntityType entity, Material mat){
        ItemStack item = new ItemStack(mat);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(entity.name());
        item.setItemMeta(itemMeta);
        items.put(entity, item);
    }

    @EventHandler
    public void onInv(InventoryClickEvent event){
        if (event.getView().getTitle().contains("Mobs remaining")) event.setCancelled(true);
    }

}
