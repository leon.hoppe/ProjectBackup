package de.craftix.killallmobs.utils;

import de.craftix.killallmobs.Main;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;

public class Config {
    private static FileConfiguration config;

    public static void save(){
        config.set("KilledEntitys", Main.killedEntitys);
        Main.getInstance().saveConfig();
    }

    public static void load(){
        Main.killedEntitys = (ArrayList<EntityType>) config.getList("KilledEntitys");
    }

    public static void setConfig(FileConfiguration cfg){
        config = cfg;
    }

}
