package de.craftix.killallmobs.utils;

import de.craftix.killallmobs.Main;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Timer {
    public boolean isStarted;

    private int seconds;
    private int minutes;
    private int hours;
    private int timerID;


    public Timer(){
        seconds = 0;
        minutes = 0;
        hours = 0;
        isStarted = false;
    }

    public void start(){
        Main.currentEntity = Main.getNewMob();
        Main.setBossbar();
        isStarted = true;
        timerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                seconds++;
                if (seconds >= 60){
                    seconds = 0;
                    minutes++;
                }
                if (minutes >= 60){
                    minutes = 0;
                    hours++;
                }
                for (Player p : Bukkit.getOnlinePlayers()){
                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(getFormat(seconds, minutes, hours)));
                }
            }
        }, 0, 20);
    }

    public void stop(){
        isStarted = false;
        Bukkit.getScheduler().cancelTask(timerID);
        reset();
    }

    public void pause(){
        isStarted = false;
        Bukkit.getScheduler().cancelTask(timerID);
    }

    public void reset(){
        seconds = 0;
        minutes = 0;
        hours = 0;
    }

    private String getFormat(int sec, int min, int hour){
        String secon;
        String minut;
        String hor;
        if (sec < 10){
            secon = "0" + sec;
        }else {
            secon = "" + sec;
        }

        if (min < 10){
            minut = "0" + min;
        }else {
            minut = "" + min;
        }

        if (hour < 10) {
            hor = "0" + hour;
        }else {
            hor = "" + hour;
        }

        return "§6" + hor + ":" + minut + ":" + secon;
    }

}
