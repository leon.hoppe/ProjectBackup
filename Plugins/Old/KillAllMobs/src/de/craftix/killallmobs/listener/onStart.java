package de.craftix.killallmobs.listener;

import de.craftix.killallmobs.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class onStart implements Listener {
    @EventHandler
    public void onBuild(BlockPlaceEvent event){
        if (Main.getTimer().isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        if (Main.getTimer().isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (Main.getTimer().isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
        if (Main.getTimer().isStarted) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent event){
        if (Main.getTimer().isStarted) return;
        event.setCancelled(true);
        Player p = (Player)event.getEntity();
        p.setFoodLevel(25);
    }
}
