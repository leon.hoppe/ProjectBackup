package de.craftix.killallmobs.listener;

import de.craftix.killallmobs.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class onKill implements Listener {

    @EventHandler
    public void onKillEvent(EntityDeathEvent event){
        if (event.getEntity().getType().equals(EntityType.PLAYER)) return;
        if (event.getEntity().getKiller() == null) return;
        EntityType type = event.getEntity().getType();
        if (type == Main.currentEntity){
            Main.killedEntitys.add(type);
            Bukkit.broadcastMessage("§aEin neues Entity wurde registriert: §6" + type.name());
            Main.currentEntity = Main.getNewMob();
            Main.setBossbar();
        }
        if (checkIfFinished()) Main.win();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player p = event.getPlayer();
        if (Main.bar == null) return;
        Main.bar.addPlayer(p);
    }

    private boolean checkIfFinished(){
        if (Main.killedEntitys.size() >= Main.entitys.size()) return true;
        return false;
    }

}
