package de.craftix.killallmobs;

import de.craftix.killallmobs.commands.Mobs;
import de.craftix.killallmobs.commands.Position;
import de.craftix.killallmobs.listener.onDeath;
import de.craftix.killallmobs.listener.onKill;
import de.craftix.killallmobs.listener.onStart;
import de.craftix.killallmobs.utils.Config;
import de.craftix.killallmobs.utils.MobGUI;
import de.craftix.killallmobs.utils.Timer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Random;

public class Main extends JavaPlugin {
    private static Main instance;
    private static Timer timer;

    public static ArrayList<EntityType> entitys = new ArrayList<>();
    public static ArrayList<EntityType> killedEntitys = new ArrayList<>();
    public static EntityType currentEntity;
    public static BossBar bar;

    @Override
    public void onEnable() {
        instance = this;
        timer = new Timer();

        registerCommands();
        registerListener();
        registerMobs();

        MobGUI.setup();

        Config.setConfig(this.getConfig());
        Config.load();

        if (killedEntitys == null) killedEntitys = new ArrayList<>();
    }

    @Override
    public void onDisable() {
        Config.save();
        if (bar != null) bar.setVisible(false);
    }

    private void registerCommands(){
        getCommand("timer").setExecutor(new de.craftix.killallmobs.commands.Timer());
        getCommand("mobs").setExecutor(new Mobs());
        getCommand("position").setExecutor(new Position());
    }

    private void registerListener(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onDeath(), this);
        pm.registerEvents(new onKill(), this);
        pm.registerEvents(new onStart(), this);
        pm.registerEvents(new MobGUI(), this);
    }

    public static Main getInstance() {
        return instance;
    }

    public static Timer getTimer() {
        return timer;
    }

    public static void win(){
        timer.stop();
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setGameMode(GameMode.SPECTATOR);
        }
        Bukkit.broadcastMessage("§6Die Challenge wurde erfolgreich absolviert");
    }

    public static void loose(){
        timer.stop();
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setGameMode(GameMode.SPECTATOR);
        }
        Bukkit.broadcastMessage("§cDie Challenge ist fehlgeschlagen");
    }

    private void registerMobs(){
        entitys.add(EntityType.BAT);
        entitys.add(EntityType.BEE);
        entitys.add(EntityType.BLAZE);
        entitys.add(EntityType.CAT);
        entitys.add(EntityType.CAVE_SPIDER);
        entitys.add(EntityType.CHICKEN);
        entitys.add(EntityType.COD);
        entitys.add(EntityType.COW);
        entitys.add(EntityType.CREEPER);
        entitys.add(EntityType.DOLPHIN);
        entitys.add(EntityType.DONKEY);
        entitys.add(EntityType.DROWNED);
        entitys.add(EntityType.ELDER_GUARDIAN);
        entitys.add(EntityType.ENDER_DRAGON);
        entitys.add(EntityType.ENDERMAN);
        entitys.add(EntityType.ENDERMITE);
        entitys.add(EntityType.EVOKER);
        entitys.add(EntityType.FOX);
        entitys.add(EntityType.GHAST);
        entitys.add(EntityType.GUARDIAN);
        entitys.add(EntityType.HOGLIN);
        entitys.add(EntityType.HORSE);
        entitys.add(EntityType.HUSK);
        entitys.add(EntityType.IRON_GOLEM);
        entitys.add(EntityType.LLAMA);
        entitys.add(EntityType.MAGMA_CUBE);
        entitys.add(EntityType.MUSHROOM_COW);
        entitys.add(EntityType.MULE);
        entitys.add(EntityType.OCELOT);
        entitys.add(EntityType.PANDA);
        entitys.add(EntityType.PARROT);
        entitys.add(EntityType.PHANTOM);
        entitys.add(EntityType.PIG);
        entitys.add(EntityType.PIGLIN);
        entitys.add(EntityType.PIGLIN_BRUTE);
        entitys.add(EntityType.ZOMBIFIED_PIGLIN);
        entitys.add(EntityType.POLAR_BEAR);
        entitys.add(EntityType.PUFFERFISH);
        entitys.add(EntityType.RABBIT);
        entitys.add(EntityType.RAVAGER);
        entitys.add(EntityType.SALMON);
        entitys.add(EntityType.SHEEP);
        entitys.add(EntityType.SHULKER);
        entitys.add(EntityType.SKELETON);
        entitys.add(EntityType.SKELETON_HORSE);
        entitys.add(EntityType.SNOWMAN);
        entitys.add(EntityType.SPIDER);
        entitys.add(EntityType.SQUID);
        entitys.add(EntityType.STRAY);
        entitys.add(EntityType.STRIDER);
        entitys.add(EntityType.TRADER_LLAMA);
        entitys.add(EntityType.TROPICAL_FISH);
        entitys.add(EntityType.TURTLE);
        entitys.add(EntityType.VILLAGER);
        entitys.add(EntityType.WANDERING_TRADER);
        entitys.add(EntityType.WITHER);
        entitys.add(EntityType.WITHER_SKELETON);
        entitys.add(EntityType.WOLF);
        entitys.add(EntityType.ZOGLIN);
        entitys.add(EntityType.ZOMBIE_VILLAGER);
        entitys.add(EntityType.WITCH);
        entitys.add(EntityType.ZOMBIE);
        entitys.add(EntityType.VINDICATOR);
        entitys.add(EntityType.VEX);
        entitys.add(EntityType.SLIME);
        entitys.add(EntityType.SILVERFISH);
        entitys.add(EntityType.PILLAGER);
    }

    public static EntityType getNewMob(){
        ArrayList<EntityType> remainingEntitys = entitys;
        for (EntityType e : killedEntitys){
            remainingEntitys.remove(e);
        }
        int rand = new Random().nextInt(remainingEntitys.size() - 1);
        return remainingEntitys.get(rand);
    }

    public static void setBossbar(){
        if (bar != null) bar.removeAll();
        bar = Bukkit.getServer().createBossBar(currentEntity.name(), BarColor.PURPLE, BarStyle.SOLID);
        bar.setVisible(true);
        bar.setProgress(1);
        for (Player p : Bukkit.getOnlinePlayers()){
            bar.addPlayer(p);
        }
    }
}
