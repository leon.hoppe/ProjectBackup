package de.craftix.killallmobs.commands;

import de.craftix.killallmobs.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Timer implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("challenge.admin")) return true;
        if (args.length != 1) return true;

        if (args[0].equalsIgnoreCase("start") || args[0].equalsIgnoreCase("resume")) Main.getTimer().start();
        if (args[0].equalsIgnoreCase("stop")) Main.getTimer().stop();
        if (args[0].equalsIgnoreCase("reset")) Main.getTimer().reset();
        if (args[0].equalsIgnoreCase("pause")) Main.getTimer().pause();
        return true;
    }
}
