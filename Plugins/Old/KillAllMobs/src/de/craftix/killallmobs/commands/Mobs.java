package de.craftix.killallmobs.commands;

import de.craftix.killallmobs.utils.MobGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Mobs implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender s, Command command, String st, String[] args) {
        if (!(s instanceof Player)) return true;
        Player p = (Player)s;
        if (args.length == 1){
            int invID = Integer.valueOf(args[0]);
            MobGUI.showGUI(p, invID);
        }else MobGUI.showGUI(p, 1);
        return false;
    }
}
