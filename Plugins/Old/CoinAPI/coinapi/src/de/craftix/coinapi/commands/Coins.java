package de.craftix.coinapi.commands;

import de.craftix.coinapi.CoinAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Coins implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player)sender;
        if (args.length == 0){
            p.sendMessage("§aDu hast §6" + CoinAPI.getCoins(p) + " §aCoins!");
        }else if (args.length == 1){
            if (!p.hasPermission("coins.ohter")) return true;
            if (Bukkit.getPlayer(args[0]) == null) return true;
            Player t = Bukkit.getPlayer(args[0]);
            p.sendMessage("§aDer spieler §6" + t.getDisplayName() + " §a hat §6" + CoinAPI.getCoins(t) + " §aCoins");
        }else if (args.length == 2){
            if (!p.hasPermission("coins.admin")) return true;
            if (args[0].equalsIgnoreCase("add")){
                CoinAPI.addCoins(p, Integer.parseInt(args[1]));
                p.sendMessage("§aDu hast nun §6" + CoinAPI.getCoins(p) + " §aCoins");
            }else if (args[0].equalsIgnoreCase("set")){
                CoinAPI.setCoins(p, Integer.parseInt(args[1]));
                p.sendMessage("§aDu hast nun §6" + CoinAPI.getCoins(p) + " §aCoins");
            }else if (args[0].equalsIgnoreCase("remove")){
                CoinAPI.removeCoins(p, Integer.parseInt(args[1]));
                p.sendMessage("§aDu hast nun §6" + CoinAPI.getCoins(p) + " §aCoins");
            }
        }else if (args.length == 3){
            if (!p.hasPermission("coins.admin")) return true;
            if (Bukkit.getPlayer(args[2]) == null) return true;
            Player t = Bukkit.getPlayer(args[0]);
            if (args[0].equalsIgnoreCase("add")){
                CoinAPI.addCoins(t, Integer.parseInt(args[1]));
                p.sendMessage("§aDer spieler §6" + t.getDisplayName() + " §ahat nun §6" + CoinAPI.getCoins(t) + " §aCoins");
            }else if (args[0].equalsIgnoreCase("set")){
                CoinAPI.setCoins(t, Integer.parseInt(args[1]));
                p.sendMessage("§aDer spieler §6" + t.getDisplayName() + " §ahat nun §6" + CoinAPI.getCoins(t) + " §aCoins");
            }else if (args[0].equalsIgnoreCase("remove")){
                CoinAPI.removeCoins(t, Integer.parseInt(args[1]));
                p.sendMessage("§aDer spieler §6" + t.getDisplayName() + " §ahat nun §6" + CoinAPI.getCoins(t) + " §aCoins");
            }
        }
        return true;
    }
}
