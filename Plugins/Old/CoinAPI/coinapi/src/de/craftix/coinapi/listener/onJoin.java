package de.craftix.coinapi.listener;

import de.craftix.coinapi.CoinAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    public void onJoinEvent(PlayerJoinEvent event){
        Player p = event.getPlayer();
        if (!CoinAPI.isUserRegistrated(p)) CoinAPI.setCoins(p, CoinAPI.startCoins);
    }

}
