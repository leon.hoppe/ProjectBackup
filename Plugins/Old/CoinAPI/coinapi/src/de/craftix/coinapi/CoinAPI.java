package de.craftix.coinapi;

import de.craftix.coinapi.commands.Coins;
import de.craftix.coinapi.listener.onJoin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CoinAPI extends JavaPlugin {
    private static CoinAPI instance;

    public static int startCoins = 1000;

    @Override
    public void onEnable() {
        instance = this;
        MySQL.connect();
        MySQL.update("CREATE TABLE IF NOT EXISTS coins (UUID VARCHAR(100), Coins INT(100))");
        getCommand("coins").setExecutor(new Coins());
        Bukkit.getPluginManager().registerEvents(new onJoin(), this);
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
    }

    public static CoinAPI getInstance(){ return instance; }

    public static void setCoins(Player p, int coins){
        if (isUserRegistrated(p)){
            MySQL.update("INSERT INTO coins(UUID, Coins) VALUES (" + p.getUniqueId().toString() + ", " + coins + ")");
        }else {
            MySQL.update("UPDATE coins SET Coins = " + coins + " WHERE UUID = " + p.getUniqueId().toString());
        }
    }

    public static void addCoins(Player p, int coins){
        setCoins(p, (getCoins(p) + coins));
    }

    public static void removeCoins(Player p, int coins){
        int i = getCoins(p);
        if (i - coins > 0) setCoins(p, getCoins(p) - coins);
        else setCoins(p, 0);
    }

    public static int getCoins(Player p){
        Integer i = 0;
        if (isUserRegistrated(p)) {
            try {
                final ResultSet rs = MySQL.get("SELECT * FROM coins WHERE UUID= '" + p.getUniqueId().toString() + "'");
                if (!rs.next() || Integer.valueOf(rs.getInt("Coins")) == null) {}
                i = rs.getInt("Coins");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            setCoins(p, startCoins);
            getCoins(p);
        }
        return i;

    }

    public static boolean checkCoins(Player p, int coins){
        if (getCoins(p) - coins > 0) return true;
        return false;
    }

    public static boolean isUserRegistrated(Player p){
        try{
            ResultSet rs = MySQL.get("SELECT * FROM coins WHERE UUID = " + p.getUniqueId().toString());
            return rs.next() && rs.getString("UUID") != null;
        }catch (SQLException throwables){
            throwables.printStackTrace();
        }
        return false;
    }

}
