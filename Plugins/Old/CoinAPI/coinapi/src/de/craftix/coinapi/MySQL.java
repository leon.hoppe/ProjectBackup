package de.craftix.coinapi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    private static String host = "localhost";
    private static String port = "3306";
    private static String database = "coins";
    private static String username = "Coins";
    private static String password = "1234";
    private static Connection con = null;

    public static void connect(){
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
            System.out.println("[MySQL] Connected");
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void disconnect(){
        if (con == null) return;
        try {
            con.close();
            System.out.println("[MySQL] Disconnected");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void update(String qry){
        try {
            con.createStatement().executeUpdate(qry);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static ResultSet get(String qry){
        try {
            return con.createStatement().executeQuery(qry);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

}
