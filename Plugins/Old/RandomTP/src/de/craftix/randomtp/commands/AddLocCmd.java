package de.craftix.randomtp.commands;

import de.craftix.randomtp.general.Main;
import de.craftix.randomtp.save.Config;
import de.craftix.randomtp.save.Positions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class AddLocCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            if (sender.hasPermission("rtp.admin")){
                if (args.length == 0){
                    Player p = (Player)sender;
                    Main.getPlugin().reloadConfig();
                    FileConfiguration config = Main.getPlugin().getConfig();
                    boolean stop = false;
                    Main.loccount = 0;
                    while (!stop){
                        int id = Main.loccount + 1;
                        if (config.contains(id + ".World")) Main.loccount++;
                        else stop = true;
                    }
                    Positions pos = new Positions(p.getLocation(), Main.loccount + 1);
                    Config.save(pos);
                    p.sendMessage("§aNeue RTP Location gespeichert!");
                }else {
                    sender.sendMessage("§6Usage: §7/addloc");
                }
            }else {
                sender.sendMessage("§cHierzu hast du keine Rechte!");
            }
        }
        return true;
    }
}
