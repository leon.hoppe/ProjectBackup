package de.craftix.randomtp.commands;

import de.craftix.randomtp.general.Main;
import de.craftix.randomtp.save.Config;
import de.craftix.randomtp.save.Positions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.Random;

public class RtpCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player){
            if (args.length == 0){
                Player p = (Player)sender;
                FileConfiguration config = Main.getPlugin().getConfig();
                boolean stop = false;
                Main.loccount = 0;
                while (!stop){
                    int id = Main.loccount + 1;
                    if (config.contains(id + ".World")) Main.loccount++;
                    else stop = true;
                }
                if (Main.loccount == 0){
                    p.sendMessage("§cEs existieren noch keine RTP's!");
                    return true;
                }
                Random rand = new Random();
                int id = rand.nextInt((Main.loccount - 1) + 1) + 1;
                Positions pos = Config.load(id);
                p.teleport(pos.loc);
            }
        }
        return true;
    }
}
