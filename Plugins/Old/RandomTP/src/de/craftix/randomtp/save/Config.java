package de.craftix.randomtp.save;

import de.craftix.randomtp.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {

    public static void save(Positions pos){
        FileConfiguration config = Main.getPlugin().getConfig();
        config.set(pos.id + ".World", pos.loc.getWorld().getName());
        config.set(pos.id + ".X", pos.loc.getX());
        config.set(pos.id + ".Y", pos.loc.getY());
        config.set(pos.id + ".Z", pos.loc.getZ());
        config.set(pos.id + ".Yaw", pos.loc.getYaw());
        config.set(pos.id + ".Pitch", pos.loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static Positions load(int id){
        Main.getPlugin().reloadConfig();
        FileConfiguration config = Main.getPlugin().getConfig();
        World world = Bukkit.getWorld(config.getString(id + ".World"));
        double x = config.getDouble(id + ".X");
        double y = config.getDouble(id + ".Y");
        double z = config.getDouble(id + ".Z");
        float yaw = (float) config.getDouble(id + ".Yaw");
        float pitch = (float) config.getDouble(id + ".Pitch");
        Location loc = new Location(world, x, y, z, yaw, pitch);
        Positions pos = new Positions(loc, id);
        return pos;
    }

    public static boolean test(int id){
        Main.getPlugin().reloadConfig();
        FileConfiguration config = Main.getPlugin().getConfig();
        if (config.contains(id + ".World")) return false;
        return true;
    }
}
