package de.craftix.randomtp.general;

import de.craftix.randomtp.commands.RtpCmd;
import de.craftix.randomtp.commands.AddLocCmd;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin {

    private static Main plugin;
    public static int loccount;

    @Override
    public void onEnable() {
        plugin = this;
        loccount = 0;

        getCommand("rtp").setExecutor(new RtpCmd());
        getCommand("addloc").setExecutor(new AddLocCmd());

        System.out.println("[RandomTP] Plugin wurde geladen!");
    }

    @Override
    public void onDisable() {
        System.out.println("[RandomTP] Plugin wurde entladen!");
    }

    public static Main getPlugin(){
        return plugin;
    }
}
