package de.craftix.teamchat;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public final class Main extends Plugin {
    @Override
    public void onEnable() {
        PluginManager pm = getProxy().getPluginManager();
        pm.registerCommand(this, new TeamChatCmd("tc", TeamChatCmd.adminPermission));
    }
}
