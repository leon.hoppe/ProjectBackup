package de.craftix.corona;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CoronaPlayer {
    public Player p;
    private int timerID;
    public CoronaPlayer(Player p){
        this.p = p;
        startTimer();
    }

    private void startTimer(){
        timerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                CoronaAPI.coronaPlayer.remove(p);
                ActionbarAPI.show(p, "§aDu hast Corona überstanden!");
                Bukkit.getScheduler().cancelTask(timerID);
            }
        }, 12000, 1);
    }
}
