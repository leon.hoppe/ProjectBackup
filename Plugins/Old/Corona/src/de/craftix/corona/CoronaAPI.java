package de.craftix.corona;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Random;

public class CoronaAPI {
    public static ArrayList<Player> coronaPlayer = new ArrayList<>();
    private static ArrayList<PotionEffectType> effects = new ArrayList<>();
    private static int timerID;
    private static int infectionTimerID;

    public static void setup(){
        effects.add(PotionEffectType.BLINDNESS);
        effects.add(PotionEffectType.SLOW);
        effects.add(PotionEffectType.WEAKNESS);
        effects.add(PotionEffectType.CONFUSION);
        startInfectionTimer();
    }

    public static void giveCorona(Player p){
        CoronaAPI.coronaPlayer.add(p);
        new CoronaPlayer(p);
        ActionbarAPI.show(p, "§cDu hast nun Corona!");
    }

    public static void sendEffects(Player p){
        Random rand = new Random();
        int value = rand.nextInt(4);
        PotionEffect effect = new PotionEffect(effects.get(value), 250, 255);
        p.addPotionEffect(effect);
    }

    public static void startTimer(){
        timerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Player p : coronaPlayer){
                    for (PotionEffect pe : p.getActivePotionEffects()){
                        for (PotionEffectType corona : effects){
                            if (pe.getType().equals(corona)) return;
                        }
                    }
                    Random rand = new Random();
                    if (rand.nextInt(5) < 2) continue;
                    ActionbarAPI.show(p, "§cZufällige Corona attacke!");
                    sendEffects(p);
                }
            }
        }, 0, 500);
    }

    public static void stopTimer(){
        Bukkit.getScheduler().cancelTask(timerID);
    }

    private static void startInfectionTimer(){
        infectionTimerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Player all : Bukkit.getOnlinePlayers()){
                    for (Player corona : coronaPlayer){
                        if (corona == all) continue;
                        if (all.getLocation().distance(corona.getLocation()) <= 2){
                            CoronaAPI.coronaPlayer.add(all);
                            new CoronaPlayer(all);
                            ActionbarAPI.show(all, "§cDu wurdest mit Corona infiziert!");
                        }
                    }
                }
            }
        }, 0, 50);
    }

    public static void stopInfectionTimer(){
        Bukkit.getScheduler().cancelTask(infectionTimerID);
    }
}
