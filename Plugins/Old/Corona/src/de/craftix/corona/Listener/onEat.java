package de.craftix.corona.Listener;

import de.craftix.corona.CoronaAPI;
import de.craftix.corona.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import java.util.Random;

public class onEat implements Listener {

    @EventHandler
    public void onEatEvent(PlayerItemConsumeEvent event){
        if (!Main.coronaFood.contains(event.getItem().getType())) return;
        Random rand = new Random();
        if (rand.nextBoolean() && !CoronaAPI.coronaPlayer.contains(event.getPlayer())){
            Player p = event.getPlayer();
            CoronaAPI.giveCorona(p);
        }
    }

}
