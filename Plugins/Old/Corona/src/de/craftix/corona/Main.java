package de.craftix.corona;

import de.craftix.corona.Listener.onEat;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {
    private static Main instance;
    public static ArrayList<Material> coronaFood = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        coronaFood.add(Material.RAW_BEEF);
        coronaFood.add(Material.RAW_CHICKEN);
        coronaFood.add(Material.RAW_FISH);
        coronaFood.add(Material.PORK);
        coronaFood.add(Material.POTATO);
        coronaFood.add(Material.POISONOUS_POTATO);
        coronaFood.add(Material.RABBIT);
        coronaFood.add(Material.MUTTON);
        registerListener();
        CoronaAPI.setup();
        CoronaAPI.startTimer();
    }

    @Override
    public void onDisable() {
        CoronaAPI.stopTimer();
        CoronaAPI.stopInfectionTimer();
    }

    private void registerListener(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onEat(), this);
    }

    public static Main getInstance() {
        return instance;
    }
}
