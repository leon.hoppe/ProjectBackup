package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;

public class CfgLocationManager {

    protected FileConfiguration config;
    protected String prefix;

    public CfgLocationManager(FileConfiguration config, String prefix) {
        this.config = config;
        this.prefix = prefix;
    }

    private int getNextID() {
        int id = 0;
        while (id <= 100) {
            if (config.contains(prefix + "." + id)) id++;
            else return id;
        }
        return -1;
    }

    public void setLocation(Location loc) {
        int id = getNextID();
        String currPrefix = prefix + "." + id + ".";
        config.set(currPrefix + ".World", loc.getWorld().getName());
        config.set(currPrefix + ".X", loc.getX());
        config.set(currPrefix + ".Y", loc.getY());
        config.set(currPrefix + ".Z", loc.getZ());
        config.set(currPrefix + ".Yaw", loc.getYaw());
        config.set(currPrefix + ".Pitch", loc.getPitch());
        try { config.save(config.getCurrentPath()); }
        catch (Exception e) { e.printStackTrace(); }
    }

    public Location getLocation(int id) {
        if (!config.contains(prefix + "." + id)) return null;
        World w = Bukkit.getWorld(config.getString(prefix + "." + id + ".World"));
        double x = config.getDouble(prefix + "." + id + ".X");
        double y = config.getDouble(prefix + "." + id + ".Y");
        double z = config.getDouble(prefix + "." + id + ".Z");
        float yaw = Float.parseFloat(config.getString(prefix + "." + id + ".Yaw"));
        float pitch = Float.parseFloat(config.getString(prefix + "." + id + ".Pitch"));
        return new Location(w, x, y, z, yaw, pitch);
    }

    public Location[] getLocations(int... ids) {
        Location[] locs = new Location[ids.length];
        for (int i = 0; i < ids.length; i++) {
            locs[i] = getLocation(ids[i]);
        }
        return locs;
    }

    public Location[] getLocations() {
        ArrayList<Location> locs = new ArrayList<>();
        int id = -1;
        while (id <= 100) {
            id++;
            if (getLocation(id) == null) continue;
            locs.add(getLocation(id));
        }
        return locs.toArray(new Location[0]);
    }

}
