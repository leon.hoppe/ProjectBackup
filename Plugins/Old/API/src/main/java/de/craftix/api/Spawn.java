package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Spawn implements CommandExecutor {

    public Spawn(JavaPlugin plugin) {
        plugin.getCommand("spawn").setExecutor(this);
        PluginCommand setSpawn = plugin.getCommand("setspawn");
        setSpawn.setPermission("utils.setspawn");
        setSpawn.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (command.getName().equalsIgnoreCase("spawn")) {
            if (!(sender instanceof Player)) return true;
            if (!isSet()) return true;
            teleport((Player) sender);
            return false;
        }else {
            if (!(sender instanceof Player)) return true;
            if (!sender.hasPermission("utils.setspawn")) return true;
            setLocation(((Player) sender).getLocation());
            sender.sendMessage("§aDer Spawn wurde erfolgreich gesetzt!");
        }
        return true;
    }

    public static void teleport(Player p){
        FileConfiguration config = API.getInstance().getConfig();

        if (!config.contains("Spawn.World")) return;

        World world = Bukkit.getWorld(config.getString("Spawn.World"));
        double x = config.getDouble("Spawn.X");
        double y = config.getDouble("Spawn.Y");
        double z = config.getDouble("Spawn.Z");
        float yaw = (float) config.getDouble("Spawn.Yaw");
        float pitch = (float) config.getDouble("Spawn.Pitch");

        Location spawn = new Location(world, x, y, z, yaw, pitch);

        p.teleport(spawn);
    }

    public static Location getLocation(){
        FileConfiguration config = API.getInstance().getConfig();

        if (!config.contains("Spawn.World")) return null;

        World world = Bukkit.getWorld(config.getString("Spawn.World"));
        double x = config.getDouble("Spawn.X");
        double y = config.getDouble("Spawn.Y");
        double z = config.getDouble("Spawn.Z");
        float yaw = (float) config.getDouble("Spawn.Yaw");
        float pitch = (float) config.getDouble("Spawn.Pitch");

        return new Location(world, x, y, z, yaw, pitch);
    }

    public static void setLocation(Location loc){
        FileConfiguration config = API.getInstance().getConfig();
        config.set("Spawn.World", loc.getWorld().getName());
        if (loc.getBlockX() > 0) config.set("Spawn.X", loc.getBlockX() + 0.5);
        else config.set("Spawn.X", loc.getBlockX() - 0.5);
        config.set("Spawn.Y", loc.getBlockY() + 2);
        if (loc.getBlockZ() > 0) config.set("Spawn.Z", loc.getBlockZ() + 0.5);
        else config.set("Spawn.Z", loc.getBlockZ() - 0.5);
        config.set("Spawn.Yaw", loc.getYaw());
        config.set("Spawn.Pitch", loc.getPitch());

        API.getInstance().saveConfig();
    }

    public static boolean isSet() {
        return API.getInstance().getConfig().contains("Spawn.World");
    }
}
