package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.sql.ResultSet;
import java.util.ArrayList;

public class LocationSaver {

    protected MySQL con;
    protected String tableName;

    public LocationSaver(MySQL con, String tableName) {
        this.con = con;
        this.tableName = tableName;
        if (!con.isConnected()) con.connect();
        con.insert("CREATE TABLE IF NOT EXISTS " + tableName + " (ID INT(10), X VARCHAR(100), Y VARCHAR(100), Z VARCHAR(100), Yaw VARCHAR(100), Pitch VARCHAR(100), World VARCHAR(100))");
    }

    protected int getNextID() {
        try {
            ResultSet rs = con.getData("SELECT * FROM " + tableName);
            int id = 0;
            while (rs.next()) if (rs.getInt("ID") == id) id++;
            return id;
        }catch (Exception ignored) {}
        return -1;
    }

    public int saveLocation(Location loc) {
        try {
            int id = getNextID();
            String qry1 = "INSERT INTO " + tableName + " (ID, X, Y, Z, Yaw, Pitch, World) VALUES (";
            String qry2 = id + ", \"" + loc.getX() + "\", \"" + loc.getY() + "\", \"" + loc.getZ() + "\", ";
            String qry3 = "\"" + loc.getYaw() + "\", \"" + loc.getPitch() + "\", \"" + loc.getWorld().getName() + "\")";
            con.insert(qry1 + qry2 + qry3);
            return id;
        }catch (Exception ignored) {}
        return -1;
    }

    public Location getLocation(int id) {
        try {
            ResultSet rs = con.getData("SELECT * FROM " + tableName + " WHERE ID = " + id);
            if (!rs.next()) return null;
            double x = Double.parseDouble(rs.getString("X"));
            double y = Double.parseDouble(rs.getString("Y"));
            double z = Double.parseDouble(rs.getString("Z"));
            float yaw = Float.parseFloat(rs.getString("Yaw"));
            float pitch = Float.parseFloat(rs.getString("Pitch"));
            World world = Bukkit.getWorld(rs.getString("World"));
            return new Location(world, x, y, z, yaw, pitch);
        }catch (Exception ignored) {}
        return null;
    }

    public Location[] getLocations(int... ids) {
        Location[] locs = new Location[ids.length];
        for (int i = 0; i < ids.length; i++) {
            locs[i] = getLocation(ids[i]);
        }
        return locs;
    }

    public Location[] getLocations() {
        try {
            ArrayList<Location> locs = new ArrayList<>();
            ResultSet rs = con.getData("SELECT * FROM " + tableName);
            while (rs.next()) {
                double x = Double.parseDouble(rs.getString("X"));
                double y = Double.parseDouble(rs.getString("Y"));
                double z = Double.parseDouble(rs.getString("Z"));
                float yaw = Float.parseFloat(rs.getString("Yaw"));
                float pitch = Float.parseFloat(rs.getString("Pitch"));
                World world = Bukkit.getWorld(rs.getString("World"));
                locs.add(new Location(world, x, y, z, yaw, pitch));
            }
            return locs.toArray(new Location[0]);
        }catch (Exception ignored) {}
        return new Location[0];
    }

}
