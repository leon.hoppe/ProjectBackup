package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Gamemode implements CommandExecutor {

    public Gamemode(JavaPlugin plugin) {
        PluginCommand cmd = plugin.getCommand("gm");
        cmd.setPermission("minecraft.command.gamemode");
        cmd.setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        Player t = null;
        if (args.length == 1) t = p;
        else {
            t = Bukkit.getPlayer(args[1]);
            if (t == null) {
                p.sendMessage("§cDieser Spieler existiert nicht!");
                return true;
            }
        }
        if (args[0].equals("0") || args[0].equals("s") || args[0].equalsIgnoreCase("survival")) {
            t.setGameMode(GameMode.SURVIVAL);
            t.sendMessage("§aDu bist nun im §6Überlebensmodus§a.");
        }
        if (args[0].equals("1") || args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("creative")) {
            t.setGameMode(GameMode.CREATIVE);
            t.sendMessage("§aDu bist nun im §6Creativmodus§a.");
        }
        if (args[0].equals("2") || args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("adventure")) {
            t.setGameMode(GameMode.ADVENTURE);
            t.sendMessage("§aDu bist nun im §6Adventuremodus§a.");
        }
        if (args[0].equals("3") || args[0].equals("S") || args[0].equalsIgnoreCase("spectator")) {
            t.setGameMode(GameMode.SPECTATOR);
            t.sendMessage("§aDu bist nun im §6Spectatormodus§a.");
        }
        return false;
    }
}
