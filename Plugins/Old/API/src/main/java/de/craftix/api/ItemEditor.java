package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ItemEditor {

    public ItemStack item;
    public Material material;
    public String name;
    public int amount;
    public short damage;
    public HashMap<Enchantment, Integer> enchantments;
    public ArrayList<String> lore;
    public boolean showEnchantments;
    public boolean isSkull;
    public OfflinePlayer player;

    public ItemEditor(ItemStack item) {
        this.item = item;
        this.material = item.getType();
        this.name = item.getItemMeta().getDisplayName();
        this.amount = item.getAmount();
        this.damage = item.getDurability();
        this.enchantments = (HashMap<Enchantment, Integer>) item.getEnchantments();
        this.lore = (ArrayList<String>) item.getItemMeta().getLore();
        this.showEnchantments = !item.getItemMeta().hasItemFlag(ItemFlag.HIDE_ENCHANTS);
        this.isSkull = (item.getType() == Material.SKULL_ITEM);
        if (isSkull) player = Bukkit.getOfflinePlayer(((SkullMeta) item.getItemMeta()).getOwner());
    }

    public ItemEditor setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public ItemEditor setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemEditor setDamage(int damage) {
        this.damage = (short) damage;
        return this;
    }

    public ItemEditor setSkull(OfflinePlayer player) {
        isSkull = true;
        material = Material.SKULL_ITEM;
        damage = 3;
        return this;
    }

    public ItemEditor setName(String name){
        this.name = name;
        return this;
    }

    public ItemEditor addEnchantment(Enchantment e, int level){
        enchantments.put(e, level);
        return this;
    }

    public ItemEditor showEnchantments(boolean value) {
        showEnchantments = value;
        return this;
    }

    public ItemEditor setLore(String... lore){
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    public ItemEditor setGlowing(boolean value) {
        if (value) {
            enchantments.put(Enchantment.DURABILITY, 1);
            showEnchantments = false;
        }else {
            enchantments.clear();
            showEnchantments = true;
        }
        return this;
    }

    public ItemStack edit(){
        if (!isSkull) {
            item = new ItemStack(material, amount, damage);
            ItemMeta meta = item.getItemMeta();
            for (Enchantment e : enchantments.keySet()){
                meta.addEnchant(e, enchantments.get(e), true);
            }
            meta.setLore(lore);
            meta.setDisplayName(name);
            if (!showEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(meta);
        }else {
            item = new ItemStack(Material.SKULL_ITEM, amount, (short) 3);
            SkullMeta meta = (SkullMeta) item.getItemMeta();
            meta.setOwner(player.getName());
            for (Enchantment e : enchantments.keySet()){
                meta.addEnchant(e, enchantments.get(e), true);
            }
            meta.setLore(lore);
            meta.setDisplayName(name);
            if (!showEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(meta);
        }
        return item;
    }

}
