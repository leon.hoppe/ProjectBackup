package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class DefaultListener implements Listener {

    private static boolean weather = true;
    private static String joinMSG = "§ePlayer joined the game";
    private static String quitMSG = "§ePlayer left the game";
    private static String deathMSG = "Player died";
    private static boolean canDestroyBlock = true;
    private static boolean canPlaceBlock = true;
    private static boolean canInteract = true;
    private static boolean canInteractAtEntity = true;

    public static void setWeather(boolean weather) {
        DefaultListener.weather = weather;
        Bukkit.getWorlds().forEach(world -> world.setStorm(false));
        Bukkit.getWorlds().forEach(world -> world.setThundering(false));
    }
    public static void setJoinMSG(String joinMSG) {
        DefaultListener.joinMSG = joinMSG;
    }
    public static void setQuitMSG(String quitMSG) {
        DefaultListener.quitMSG = quitMSG;
    }
    public static void setDeathMSG(String deathMSG) {
        DefaultListener.deathMSG = deathMSG;
    }
    public static void setCanDestroyBlock(boolean canDestroyBlock) {
        DefaultListener.canDestroyBlock = canDestroyBlock;
    }
    public static void setCanPlaceBlock(boolean canPlaceBlock) {
        DefaultListener.canPlaceBlock = canPlaceBlock;
    }
    public static void setCanInteract(boolean canInteract) {
        DefaultListener.canInteract = canInteract;
    }
    public static void setCanInteractAtEntity(boolean canInteractAtEntity) {
        DefaultListener.canInteractAtEntity = canInteractAtEntity;
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent event) { event.setCancelled(!weather); }

    @EventHandler
    public void onBreak(BlockBreakEvent event) { event.setCancelled(!canDestroyBlock); }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) { event.setCancelled(!canPlaceBlock); }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) { event.setCancelled(!canInteract); }

    @EventHandler
    public void onInteractEntity(PlayerInteractAtEntityEvent event) { event.setCancelled(!canInteractAtEntity); }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) { event.setJoinMessage(joinMSG.replace("Player", event.getPlayer().getDisplayName())); }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) { event.setQuitMessage(quitMSG.replace("Player", event.getPlayer().getDisplayName())); }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) { event.setDeathMessage(deathMSG.replace("Player", event.getEntity().getDisplayName())); }
}
