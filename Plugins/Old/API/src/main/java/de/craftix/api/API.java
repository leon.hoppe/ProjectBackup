package de.craftix.api;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public final class API extends JavaPlugin {
    private static API instance;
    private static final ArrayList<MySQL> sqlConnections = new ArrayList<>();
    private static boolean spawnCmd = true;
    private static boolean gamemodeCmd = true;
    private static boolean defaultListener = false;

    public static void setSpawnCmd(boolean spawnCmd) {
        API.spawnCmd = spawnCmd;
    }
    public static void setGamemodeCmd(boolean gamemodeCmd) {
        API.gamemodeCmd = gamemodeCmd;
    }
    public static void setDefaultListener(boolean defaultListener) {
        API.defaultListener = defaultListener;
    }

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> {
            if (spawnCmd) new Spawn(this);
            if (gamemodeCmd) new Gamemode(this);
            if (defaultListener) Bukkit.getPluginManager().registerEvents(new DefaultListener(), this);
        }, 20);
    }

    @Override
    public void onDisable() {
        for (MySQL all : sqlConnections) all.disconnect();
    }

    public static API getInstance() { return instance; }
    public static ArrayList<MySQL> getSqlConnections() { return sqlConnections; }
}
