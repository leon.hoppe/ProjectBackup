package de.craftix.api;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Contact {

    public static void sendActionBar(Player p, String msg) {
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"\"}").a(msg);
        PacketPlayOutChat chat = new PacketPlayOutChat(base, (byte) 2);

        CraftPlayer cp = (CraftPlayer)p;
        cp.getHandle().playerConnection.sendPacket(chat);
    }

    public static void sendTitle(Player p, String title, int fadeIn, int stay, int fadeOut) {
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"\"}").a(title);
        PacketPlayOutTitle packet = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, base);
        PacketPlayOutTitle length = new PacketPlayOutTitle(fadeIn, stay, fadeOut);


        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
    }

    public static void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        sendTitle(p, title, fadeIn, stay, fadeOut);
        sendSubtitle(p, subtitle, fadeIn, stay, fadeOut);
    }

    public static void sendSubtitle(Player p, String subtitle, int fadeIn, int stay, int fadeOut) {
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"\"}").a(subtitle);
        PacketPlayOutTitle packet = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, base);
        PacketPlayOutTitle length = new PacketPlayOutTitle(fadeIn, stay, fadeOut);


        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
    }

}
