package de.craftix.api;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    public String server;
    public int port;
    public String database;
    public String username;
    public String password;
    protected Connection con;

    public MySQL() {
        API.getSqlConnections().add(this);
    }

    public boolean connect(){
        String conString = "jdbc:mysql://" + server + ":" + port + "/" + database;
        try {
            con = DriverManager.getConnection(conString, username, password);
            System.out.println("[ArenaFight] MySQL connected");
            return true;
        }catch (SQLException e){
            System.out.println("[ArenaFight] MySQL connection failed");
            return false;
        }
    }

    public boolean disconnect(){
        if (!isConnected()) return true;
        try {
            con.close();
            con = null;
            System.out.println("[ArenaFight] MySQL disconnected");
            return true;
        }catch (SQLException e){
            System.out.println("[ArenaFight] MySQL disconnecting failed");
            return false;
        }
    }

    public boolean isConnected(){
        return con != null;
    }

    public void insert(String qry){
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public ResultSet getData(String qry){
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}
