package de.craftix.arenafight.utils;

import de.craftix.arenafight.Main;
import net.minecraft.server.v1_16_R3.EnumItemSlot;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Random;

public class Wave {

    //static
    public static ArrayList<Wave> waves = new ArrayList<>();

    public static void setup(){
        //test
        Wave test = new Wave();
        ItemStack hand = new ItemBuilder(Material.DIAMOND_SWORD).addEnchantment(Enchantment.DAMAGE_ALL, 1).create();
        ItemStack helmet = new ItemBuilder(Material.DIAMOND_HELMET).create();
        test.addEntity(EntityType.ZOMBIE, 0).setHand(hand).setArmor(helmet, null, null, null).saveEntity();
        test.save();
        //test 2
        Wave test2 = new Wave();
        ItemStack hand2 = new ItemBuilder(Material.DIAMOND_SWORD).addEnchantment(Enchantment.DAMAGE_ALL, 5).create();
        ItemStack helmet2 = new ItemBuilder(Material.DIAMOND_HELMET).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).create();
        ItemStack chestplate2 = new ItemBuilder(Material.DIAMOND_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).create();
        ItemStack leggins2 = new ItemBuilder(Material.DIAMOND_LEGGINGS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).create();
        ItemStack boots2 = new ItemBuilder(Material.DIAMOND_BOOTS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4).create();
        test2.addEntity(EntityType.ZOMBIE, 0).setHand(hand2).setArmor(helmet2, chestplate2, leggins2, boots2).saveEntity().setTime(18000);
        test2.save();
    }

    //nonStatic

    class WaveEntity{
        public ItemStack helmet;
        public ItemStack chestplate;
        public ItemStack leggins;
        public ItemStack boots;
        public ItemStack hand;
        public EntityType type;
        public Integer id;
        private Wave wave;

        public WaveEntity(Wave wave, EntityType type, int id){
            this.wave = wave;
            this.type = type;
            this.id = id;
        }

        public WaveEntity setArmor(ItemStack helmet, ItemStack chestplate, ItemStack leggins, ItemStack boots){
            this.helmet = helmet;
            this.chestplate = chestplate;
            this.leggins = leggins;
            this.boots = boots;
            return this;
        }

        public WaveEntity setHand(ItemStack hand){
            this.hand = hand;
            return this;
        }

        public Wave saveEntity(){
            entities.add(this);
            return wave;
        }
    }

    public ArrayList<WaveEntity> entities = new ArrayList<>();

    public void save(){
        waves.add(this);
    }

    public WaveEntity addEntity(EntityType type, int id){
        return new WaveEntity(this, type, id);
    }

    public Wave setTime(Integer time){
        this.time = time;
        return this;
    }

    public ArrayList<LivingEntity> summonedEntities = new ArrayList<>();

    public int time = 6000;

    public void summonWave(Arena arena){
        for (Player p : Main.inGame) p.setPlayerTime(time, false);
        for (WaveEntity e : entities){
            Location spawn = arena.mobSpawns.get(new Random().nextInt(arena.mobSpawns.size() - 1));
            CraftLivingEntity entity = (CraftLivingEntity) spawn.getWorld().spawnEntity(spawn, e.type);
            entity.getHandle().setSlot(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(e.hand));
            entity.getHandle().setSlot(EnumItemSlot.FEET, CraftItemStack.asNMSCopy(e.boots));
            entity.getHandle().setSlot(EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(e.leggins));
            entity.getHandle().setSlot(EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(e.chestplate));
            entity.getHandle().setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(e.helmet));
            summonedEntities.add(entity);
        }
    }

}
