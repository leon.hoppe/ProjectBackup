package de.craftix.arenafight.utils;

import de.craftix.arenafight.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

public class Setup implements Listener, CommandExecutor {

    private static Main plugin;
    public Setup(Main pl){
        plugin = pl;
    }

    public ArrayList<Arena> setupArenas = new ArrayList<>();
    public HashMap<Player, Arena> setupPlayers = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command c, String s, String[] args) {
        if (!(sender instanceof Player)) return true;
        if (!sender.hasPermission("arenafight.setup")) return true;
        Player p = (Player)sender;

        //addarena
        if (c.getName().equals("addarena")){
            if (args.length != 1) return true;
            if (Arena.checkName(args[0])){
                Arena arena = new Arena();
                arena.name = args[0];
                arena.id = Arena.getNextID();
                setupArenas.add(arena);
                p.sendMessage("§aDie Arena §6" + arena.name + " §awurde erfolgreich erstellt!");
            }else {
                p.sendMessage("§cDieser Name ist bereits vergeben!");
            }
        }

        //delarena
        if (c.getName().equals("delarena")){
            if (args.length != 1) return true;
            Arena arena = Arena.getArena(args[0]);
            if (arena != null){
                arena.delete();
                p.sendMessage("§aDie Arena §6" + arena.name + " §awurde gelöscht!");
            }else p.sendMessage("§cDiese Arena existiert nicht!");
        }

        //setup
        if (c.getName().equals("setup")){
            if (args.length != 1) return true;
            Arena arena = null;
            for (Arena all : setupArenas){
                if (all.name.equalsIgnoreCase(args[0])) arena = all;
            }
            if (arena != null){
                if (setupPlayers.containsKey(p)){
                    setupPlayers.remove(p);
                    p.getInventory().clear();
                    arena.save();
                    p.sendMessage("§aSetup abgeschlossen!");
                }else {
                    giveInv(p);
                    setupPlayers.put(p, arena);
                    p.sendMessage("§aSetup für §6" + arena.name + " §agestartet!");
                }
            }else p.sendMessage("§cDiese Arena existiert nicht");
        }

        //setlobby
        if (c.getName().equals("setlobby")){
            Location l = p.getLocation();
            MySQL.insert("DELETE FROM Arenas WHERE ID = -1");
            MySQL.insert("INSERT INTO Arenas (Type, x, y, z, yaw, pitch, world) VALUES (0, \"" + l.getX() + "\", \"" + l.getY() + "\", \"" + l.getZ() + "\", \"" + l.getYaw() + "\", \"" + l.getPitch() + "\", \"" + l.getWorld().getName() + "\")");
            p.sendMessage("§aDie Lobby wurde gesetzt");
        }

        return true;
    }

    public void giveInv(Player p){
        p.getInventory().clear();
        p.getInventory().setItem(1, new ItemBuilder(Material.DIAMOND).setName("Spawn").create());
        p.getInventory().setItem(2, new ItemBuilder(Material.GLASS_PANE).setName("Spectator").create());
        p.getInventory().setItem(3, new ItemBuilder(Material.ROTTEN_FLESH).setName("Monsterspawn").create());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (!setupPlayers.containsKey(event.getPlayer())) return;
        event.setCancelled(true);
        Arena arena = setupPlayers.get(event.getPlayer());
        Player p = event.getPlayer();

        switch (p.getInventory().getHeldItemSlot()){
            case 1:
                arena.spawn = p.getLocation();
                break;
            case 2:
                arena.spec = p.getLocation();
                break;
            case 3:
                arena.mobSpawns.add(p.getLocation());
        }

        p.sendMessage("§aPosition hinzugefügt!");
    }

    public Location getLobby(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas WHERE Type = 0");
            if (rs.next()){
                String x = rs.getString("x");
                String y = rs.getString("y");
                String z = rs.getString("z");
                String yaw = rs.getString("yaw");
                String pitch = rs.getString("pitch");
                String world = rs.getString("world");
                return new Location(Bukkit.getWorld(world), Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z), Float.parseFloat(yaw), Float.parseFloat(pitch));
            }
        }catch (Exception ignored) {}
        return null;
    }
}
