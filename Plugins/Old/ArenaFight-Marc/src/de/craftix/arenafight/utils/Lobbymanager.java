package de.craftix.arenafight.utils;

import de.craftix.arenafight.Main;
import de.craftix.arenafight.utils.kit.Kit;
import de.craftix.arenafight.utils.kit.KitAPI;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Lobbymanager implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Stats.createStats(e.getPlayer());
        if (Main.state == Gamestate.LOBBY){
            e.getPlayer().setGameMode(GameMode.SURVIVAL);
            e.getPlayer().setHealth(20);
            e.getPlayer().setSaturation(5);
            e.getPlayer().setFoodLevel(20);
            Location lobby = Main.getSetup().getLobby();
            if (lobby != null) e.getPlayer().teleport(lobby);
            Main.inGame.add(e.getPlayer());
            e.setJoinMessage("§aDer Spieler §6" + e.getPlayer().getDisplayName() + " §ahat das Spiel betreten!");
            checkPlayerAmount(false);
            giveItems(e.getPlayer());
            Gamemanager.playerKits.put(e.getPlayer(), KitAPI.getKits().get(0));
        }
        if (Main.state == Gamestate.RESTART){
            e.setJoinMessage(null);
            e.getPlayer().kickPlayer("§cServer restart!");
        }
        if (Main.state == Gamestate.INGAME){
            e.setJoinMessage(null);
            e.getPlayer().setGameMode(GameMode.SPECTATOR);
            e.getPlayer().teleport(Gamemanager.getRunningGame().spec);
            Main.spec.add(e.getPlayer());
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        if (Main.inGame.contains(e.getPlayer())){
            e.setQuitMessage("§aDer Spieler §6" + e.getPlayer().getDisplayName() + " §ahat das Spiel verlassen!");
            Main.inGame.remove(e.getPlayer());
            if (Main.state == Gamestate.INGAME && Main.inGame.size() == 0) Bukkit.getServer().shutdown();
        }else {
            Main.spec.remove(e.getPlayer());
            e.setQuitMessage(null);
        }
    }

    private static int lobbyID;
    private static boolean isStarting = false;

    private static void checkPlayerAmount(boolean start){
        if (isStarting) return;
        if (Main.inGame.size() >= 4 || start == true){
            isStarting = true;
            lobbyID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
                private int maxTimer = 30;
                private int ramTimer = 0;

                @Override
                public void run() {
                    int sec = maxTimer - ramTimer;
                    switch (sec){
                        case 30: case 15: case 10: case 5: case 4: case 3: case 2: case 1:
                            for (Player all : Main.inGame) all.sendMessage("§aDas Spiel beginnt in §6" + sec + " §aSekunden");
                            break;
                    }
                    if (sec == 0){
                        startGame();
                    }
                    ramTimer++;
                }
            }, 20, 20);
        }
    }

    private static void giveItems(Player p){
        p.getInventory().setHelmet(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setBoots(null);
        p.getInventory().clear();
        ItemStack kit = new ItemBuilder(Material.DIAMOND_SWORD).setName("§aKlasse wählen").create();
        ItemStack map = new ItemBuilder(Material.GRASS_BLOCK).setName("§aMap wählen").create();
        ItemStack start = new ItemBuilder(Material.EMERALD).setName("§aSpiel starten").create();
        ItemStack difficulty = new ItemBuilder(Material.TOTEM_OF_UNDYING).setName("§aSchwierigkeitsgrad").create();
        p.getInventory().setItem(1, kit);
        p.getInventory().setItem(3, map);
        p.getInventory().setItem(5, start);
        p.getInventory().setItem(7, difficulty);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if (Main.state != Gamestate.LOBBY) return;
        if (e.getItem() == null) return;
        switch (e.getItem().getType()){
            case DIAMOND_SWORD:
                openKitGUI(e.getPlayer());
                break;
            case GRASS_BLOCK:
                if (isStarting){
                    e.getPlayer().sendMessage("§aDas Spiel startet bereits, du kannst jetzt nichts mehr einstellen!");
                    break;
                }
                openMapGUI(e.getPlayer());
                break;

            case EMERALD:
                checkPlayerAmount(true);
                break;
            case TOTEM_OF_UNDYING:
                if (isStarting){
                    e.getPlayer().sendMessage("§aDas Spiel startet bereits, du kannst jetzt nichts mehr einstellen!");
                    break;
                }
                openDiffGUI(e.getPlayer());
                break;
        }
    }

    private static final String kitGUI = "§aKit wählen";
    private static final String mapGUI = "§aMap wählen";
    private static final String diffGUI = "§aSchwierigkeitsgrad";

    private static void openKitGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, 3*9, kitGUI);
        for (int i = 0; i < inv.getSize(); i++){
            inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        }
        int slot = 0;
        for (Kit kit : KitAPI.getKits()){
            ItemBuilder builder = new ItemBuilder(kit.invMat).setName(kit.name);
            String helmet = "§7- §a" + kit.helmet.getType();
            builder.setLore(helmet);
            for (Enchantment e : kit.helmet.getEnchantments().keySet()){
                builder.setLore("   §7- §a" + e.getKey().getKey().replaceAll("minecraft:", "") + " - " + kit.helmet.getEnchantments().get(e));
            }
            String chestplate = "§7- §a" + kit.chestplate.getType();
            builder.setLore(chestplate);
            for (Enchantment e : kit.chestplate.getEnchantments().keySet()){
                builder.setLore("   §7- §a" + e.getKey().getKey().replaceAll("minecraft:", "") + " - " + kit.chestplate.getEnchantments().get(e));
            }
            String leggins = "§7- §a" + kit.leggins.getType();
            builder.setLore(leggins);
            for (Enchantment e : kit.leggins.getEnchantments().keySet()){
                builder.setLore("   §7- §a" + e.getKey().getKey().replaceAll("minecraft:", "") + " - " + kit.leggins.getEnchantments().get(e));
            }
            String boots = "§7- §a" + kit.boots.getType();
            builder.setLore(boots);
            for (Enchantment e : kit.boots.getEnchantments().keySet()){
                builder.setLore("   §7- §a" + e.getKey().getKey().replaceAll("minecraft:", "") + " - " + kit.boots.getEnchantments().get(e));
            }
            for (ItemStack i : kit.items.values()){
                builder.setLore("§7- §a" + i.getType());
                for (Enchantment e : i.getEnchantments().keySet()){
                    builder.setLore("   §7- §a" + e.getKey().getKey().replaceAll("minecraft:", "") + " - " + i.getEnchantments().get(e));
                }
            }
            inv.setItem(slot, builder.create());
            slot++;
        }
        p.openInventory(inv);
    }

    private static void openMapGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, 3*9, mapGUI);
        for (int i = 0; i < inv.getSize(); i++){
            inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        }
        int slot = 0;
        for (Arena arena : Arena.getAllArenas()){
            if (arena == null) continue;
            inv.setItem(slot, new ItemBuilder(Material.GRASS_BLOCK).setName(arena.name).create());
            slot++;
        }
        p.openInventory(inv);
    }

    private static void openDiffGUI(Player p){
        Inventory inv = Bukkit.createInventory(null, 3*9, diffGUI);
        for (int i = 0; i < inv.getSize(); i++){
            inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create());
        }
        inv.setItem(10, new ItemBuilder(Material.IRON_INGOT).setName("§aEinfach").setLore("§7- §a3 Leben", "§7- §aKein Hunger").create());
        inv.setItem(13, new ItemBuilder(Material.DIAMOND).setName("§eNormal").setLore("§7- §e1 Leben", "§7- §eKein Hunger").create());
        inv.setItem(16, new ItemBuilder(Material.NETHER_STAR).setName("§4Schwer").setLore("§7- §41 Leben", "§7- §4Hunger").create());
        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory() == null) return;
        if (e.getCurrentItem() == null) return;
        if (e.getView().getTitle().equals(kitGUI)){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.GRAY_STAINED_GLASS_PANE)) return;
            p.closeInventory();
            Kit kit;
            kit = KitAPI.getKit(e.getCurrentItem().getItemMeta().getDisplayName());
            Gamemanager.playerKits.remove(p);
            Gamemanager.playerKits.put(p, kit);
            p.sendMessage("§aDu hast das Kit §6" + kit.name + " §aausgewählt");
        }else if (e.getView().getTitle().equals(mapGUI)){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.GRAY_STAINED_GLASS_PANE)) return;
            p.closeInventory();
            Arena arena;
            arena = Arena.getArena(e.getCurrentItem().getItemMeta().getDisplayName());
            Gamemanager.setArena(arena);
            p.sendMessage("§aDu hast die Map §6" + arena.name + " §aausgewählt");
        }else if (e.getView().getTitle().equals(diffGUI)){
            e.setCancelled(true);
            switch (e.getCurrentItem().getType()){
                case IRON_INGOT:
                    Gamemanager.difficulty = 0;
                    break;
                case DIAMOND:
                    Gamemanager.difficulty = 1;
                    break;
                case NETHER_STAR:
                    Gamemanager.difficulty = 2;
                    break;
            }
            p.closeInventory();
            p.sendMessage("§aDu hast den Modus §r" + e.getCurrentItem().getItemMeta().getDisplayName() + " §agewählt!");
        }
    }

    private static void startGame(){
        Bukkit.getScheduler().cancelTask(lobbyID);
        Main.state = Gamestate.INGAME;
        Arena arena = Gamemanager.getRunningGame();
        for (Player all : Main.inGame) {
            all.teleport(arena.spawn);
            all.sendMessage("§aEinstellungen:");
            all.sendMessage("§aMap: §6" + arena.name);
            if (Gamemanager.difficulty == 0){
                all.sendMessage("§aSchwierigkeitsgrad: §6Einfach");
            }else if (Gamemanager.difficulty == 1){
                all.sendMessage("§aSchwierigkeitsgrad: §6Normal");
            }else if (Gamemanager.difficulty == 2){
                all.sendMessage("§aSchwierigkeitsgrad: §6Schwer");
            }
        }
        Gamemanager.startGame(arena);
    }

    public static void endGame(){
        Main.state = Gamestate.RESTART;
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.teleport(Main.getSetup().getLobby());
            p.setGameMode(GameMode.SURVIVAL);
        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            private int maxTimer = 10;
            private int ramTimer = 0;

            @Override
            public void run() {
                int sec = maxTimer - ramTimer;
                switch (sec){
                    case 10: case 5: case 4: case 3: case 2: case 1:
                        for (Player p : Bukkit.getOnlinePlayers()) p.sendMessage("§aDer Server stoppt in §6" + sec + " §aSekunden!");
                }
                if (sec <= 0) Bukkit.getServer().shutdown();
                ramTimer++;
            }
        }, 20, 20);
    }


    @EventHandler
    public void onBuild(BlockPlaceEvent e){
        if (Main.state != Gamestate.INGAME) e.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e){
        if (Main.state != Gamestate.INGAME) e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if (Main.state != Gamestate.INGAME) e.setCancelled(true);
    }

    @EventHandler
    public void onBlockExplode(EntityExplodeEvent e){
        e.blockList().clear();
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent e){
        if (Main.state == Gamestate.INGAME && Gamemanager.difficulty == 2) return;
        e.setCancelled(true);
        Player p = (Player) e.getEntity();
        p.setFoodLevel(20);
        p.setSaturation(5);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e){
        if (Main.state != Gamestate.INGAME) e.setCancelled(true);
    }

    @EventHandler
    public void onPickUp(EntityPickupItemEvent e){
        if (Main.state != Gamestate.INGAME) e.setCancelled(true);
    }

}
