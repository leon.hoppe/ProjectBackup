package de.craftix.arenafight.utils.kit;

import de.craftix.arenafight.utils.Gamemanager;
import de.craftix.arenafight.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class KitAPI {

    private static ArrayList<Kit> kits = new ArrayList<>();

    public static ArrayList<Kit> getKits() {
        return kits;
    }

    public static Kit getKit(String name){
        for (Kit kit : kits){
            if (kit.name.equalsIgnoreCase(name)) return kit;
        }
        return null;
    }

    public static void addKit(Kit kit){
        kits.add(kit);
    }

    public static void preparePlayer(Player p, Kit kit){
        p.getInventory().clear();
        p.getInventory().setBoots(kit.boots);
        p.getInventory().setLeggings(kit.leggins);
        p.getInventory().setChestplate(kit.chestplate);
        p.getInventory().setHelmet(kit.helmet);
        for (int slot : kit.items.keySet()){
            p.getInventory().setItem(slot, kit.items.get(slot));
        }
        if (Gamemanager.difficulty == 2){
            p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 32));
        }
    }

    public static void setup(){
        //Assasine
        ItemStack aHelmet = new ItemBuilder(Material.LEATHER_HELMET).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setUnbreakable(true).create();
        ItemStack aChestplate = new ItemBuilder(Material.LEATHER_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setUnbreakable(true).create();
        ItemStack aLeggins = new ItemBuilder(Material.LEATHER_LEGGINGS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setUnbreakable(true).create();
        ItemStack aBoots = new ItemBuilder(Material.LEATHER_BOOTS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setUnbreakable(true).create();
        ItemStack aSword = new ItemBuilder(Material.DIAMOND_SWORD).addEnchantment(Enchantment.DAMAGE_ALL, 3).setUnbreakable(true).create();
        ItemStack aShield = new ItemBuilder(Material.SHIELD).setUnbreakable(true).create();
        addKit(new KitAPI("Assasine").setArmor(aHelmet, aChestplate, aLeggins, aBoots).setItem(0, aSword).setItem(40, aShield).createKit(Material.DIAMOND_SWORD));
        //Sniper
        ItemStack sSword = new ItemBuilder(Material.WOODEN_SWORD).addEnchantment(Enchantment.DURABILITY, 1).create();
        ItemStack sBow = new ItemBuilder(Material.BOW).addEnchantment(Enchantment.ARROW_INFINITE, 1).addEnchantment(Enchantment.ARROW_DAMAGE, 3).create();
        ItemStack sArrow = new ItemStack(Material.ARROW);
        ItemStack sHelmet = new ItemBuilder(Material.CHAINMAIL_HELMET).addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4).setUnbreakable(true).create();
        ItemStack sChestplate = new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4).setUnbreakable(true).create();
        ItemStack sLeggins = new ItemBuilder(Material.CHAINMAIL_LEGGINGS).addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4).setUnbreakable(true).create();
        ItemStack sBoots = new ItemBuilder(Material.CHAINMAIL_BOOTS).addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4).setUnbreakable(true).create();
        addKit(new KitAPI("Sniper").setArmor(sHelmet, sChestplate, sLeggins, sBoots).setItem(0, sSword).setItem(1, sBow).setItem(9, sArrow).createKit(Material.BOW));
        //Tank
        ItemStack tHelmet = new ItemBuilder(Material.DIAMOND_HELMET).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2).setUnbreakable(true).create();
        ItemStack tChestplate = new ItemBuilder(Material.DIAMOND_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2).setUnbreakable(true).create();
        ItemStack tLeggins = new ItemBuilder(Material.DIAMOND_LEGGINGS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2).setUnbreakable(true).create();
        ItemStack tBoots = new ItemBuilder(Material.DIAMOND_BOOTS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2).setUnbreakable(true).create();
        ItemStack tSword = new ItemBuilder(Material.IRON_SWORD).addEnchantment(Enchantment.DAMAGE_ALL, 2).setUnbreakable(true).create();
        ItemStack tShield = new ItemBuilder(Material.SHIELD).setUnbreakable(true).create();
        ItemStack tTNT = new ItemBuilder(Material.TNT, 32).create();
        ItemStack tFNS = new ItemBuilder(Material.FLINT_AND_STEEL).setUnbreakable(true).create();
        addKit(new KitAPI("Tank").setArmor(tHelmet, tChestplate, tLeggins, tBoots).setItem(0, tSword).setItem(1, tTNT).setItem(2, tFNS).setItem(40, tShield).createKit(Material.DIAMOND_CHESTPLATE));
    }

    //nonStatic
    public String name;
    public ItemStack helmet;
    public ItemStack chestplate;
    public ItemStack leggins;
    public ItemStack boots;
    public HashMap<Integer, ItemStack> items = new HashMap<>();

    public KitAPI(String name){
        this.name = name;
    }

    public KitAPI setArmor(ItemStack helmet, ItemStack chestplate, ItemStack leggins, ItemStack boots){
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggins = leggins;
        this.boots = boots;
        return this;
    }

    public KitAPI setItem (int slot, ItemStack item){
        items.put(slot, item);
        return this;
    }

    public Kit createKit(Material mat){
        Kit kit = new Kit();
        kit.helmet = helmet;
        kit.chestplate = chestplate;
        kit.leggins = leggins;
        kit.boots = boots;
        kit.name = name;
        kit.items = items;
        kit.invMat = mat;
        return kit;
    }
}
