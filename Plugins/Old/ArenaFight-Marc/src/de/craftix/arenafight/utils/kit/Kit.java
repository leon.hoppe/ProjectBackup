package de.craftix.arenafight.utils.kit;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Kit {

    public String name;
    public ItemStack helmet;
    public ItemStack chestplate;
    public ItemStack leggins;
    public ItemStack boots;
    public HashMap<Integer, ItemStack> items = new HashMap<>();
    public Material invMat;

}
