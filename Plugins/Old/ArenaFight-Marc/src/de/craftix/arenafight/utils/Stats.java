package de.craftix.arenafight.utils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.ResultSet;

public class Stats implements CommandExecutor {

    public static void Setup(){
        MySQL.insert("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(100), Kills INT(10), Deaths INT(10), SW INT(10), PG INT(10), Wins INT(10))");
    }

    public static Stats getStats(Player p){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Stats WHERE UUID = \"" + p.getUniqueId().toString() + "\"");
            rs.next();
            Stats stats = new Stats(p);
            stats.kills = rs.getInt("Kills");
            stats.deaths = rs.getInt("Deaths");
            stats.survivedWaves = rs.getInt("SW");
            stats.playedGames = rs.getInt("PG");
            stats.wins = rs.getInt("Wins");
            return stats;
        }catch (Exception e) {}
        return null;
    }

    public static void setStats(Stats stats){
        MySQL.insert("DELETE FROM Stats WHERE UUID = \"" + stats.p.getUniqueId().toString() + "\"");
        String qry1 = "INSERT INTO Stats (UUID, Kills, Deaths, SW, PG, Wins) VALUES (\"" + stats.p.getUniqueId().toString() + "\", ";
        String qry2 = stats.kills + ", " + stats.deaths + ", " + stats.survivedWaves + ", " + stats.playedGames + ", " + stats.wins + ")";
        MySQL.insert(qry1 + qry2);
    }

    public static void createStats(Player p){
        if (getStats(p) == null) setStats(new Stats(p));
    }


    //nonStatic
    public Player p;
    public int kills = 0;
    public int deaths = 0;
    public int survivedWaves = 0;
    public int playedGames = 0;
    public int wins = 0;

    public Stats(Player p){
        this.p = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof  Player)) return true;
        Player p = (Player) sender;
        createStats(p);
        Stats stats = getStats(p);
        System.out.println(stats);
        p.sendMessage("§a--------------§6Stats§a--------------");
        p.sendMessage("§aKills: §6" + stats.kills);
        p.sendMessage("§aTode: §6" + stats.deaths);
        p.sendMessage("§aÜberlebte Wellen: §6" + stats.survivedWaves);
        p.sendMessage("§aGespielte Spiele: §6" + stats.playedGames);
        p.sendMessage("§aGewonnene Spiele: §6" + stats.wins);
        p.sendMessage("§a---------------------------------");
        return true;
    }
}
