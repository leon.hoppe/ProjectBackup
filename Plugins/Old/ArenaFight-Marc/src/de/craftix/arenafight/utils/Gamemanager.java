package de.craftix.arenafight.utils;

import de.craftix.arenafight.Main;
import de.craftix.arenafight.utils.kit.Kit;
import de.craftix.arenafight.utils.kit.KitAPI;
import org.bukkit.*;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.HashMap;

public class Gamemanager implements Listener {

    public static HashMap<Player, Kit> playerKits = new HashMap<>();
    public static HashMap<Player, Integer> playerDeaths = new HashMap<>();
    private static Arena usedArena = null;
    public static int difficulty;
    private static ArrayList<Wave> waves = (ArrayList<Wave>) Wave.waves.clone();
    private static Wave currentWave;

    public static Arena getRunningGame(){
        return usedArena;
    }

    public static void setArena(Arena arena){usedArena = arena;}

    public static void startGame(Arena arena){
        World world = arena.spawn.getWorld();
        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
        world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        world.setGameRule(GameRule.RANDOM_TICK_SPEED, 0);
        world.setGameRule(GameRule.DO_FIRE_TICK, false);
        for (Entity e : world.getEntities()){
            if (!(e instanceof Player)) e.remove();
        }
        for (Player p : Main.inGame) {
            KitAPI.preparePlayer(p, playerKits.get(p));
            p.setPlayerTime(6000, false);
            Stats stats = Stats.getStats(p);
            stats.playedGames++;
            Stats.setStats(stats);
        }
        world.setTime(18000);
        world.setPVP(false);
        Wave wave = getNewWave();
        wave.summonWave(arena);
        currentWave = wave;
    }

    @EventHandler
    public void onDeath(EntityDeathEvent e){
        if (Main.state != Gamestate.INGAME) return;
        if (currentWave.summonedEntities.contains(e.getEntity())){
            e.setDroppedExp(0);
            e.getDrops().clear();
            Player p = e.getEntity().getKiller();
            Stats stats = Stats.getStats(p);
            stats.kills++;
            Stats.setStats(stats);
            currentWave.summonedEntities.remove(e.getEntity());
            if (currentWave.summonedEntities.size() == 0){
                for (Player all : Main.inGame){
                    Stats s = Stats.getStats(all);
                    s.survivedWaves++;
                    Stats.setStats(s);
                }
                Wave wave = getNewWave();
                if (wave == null){
                    handleWinning();
                    return;
                }
                wave.summonWave(usedArena);
                currentWave = wave;
            }
        }
    }

    @EventHandler
    public void onDeathPlayer(PlayerDeathEvent e){
        Player p = e.getEntity();
        e.setKeepInventory(true);
        e.setDroppedExp(0);
        e.getDrops().clear();
        Stats stats = Stats.getStats(p);
        stats.deaths++;
        Stats.setStats(stats);
        if (difficulty == 0){
            int deaths = 0;
            try {deaths = playerDeaths.get(p) + 1;}
            catch (Exception exception) {}
            playerDeaths.remove(p);
            playerDeaths.put(p, deaths);
            if (deaths >= 3){
                p.getInventory().clear();
                p.setGameMode(GameMode.SPECTATOR);
                Main.inGame.remove(p);
                Main.spec.add(p);
            }else {
                p.teleport(usedArena.spawn);
            }
            if (Main.inGame.size() == 0){
                handleLoosing();
            }
        }else {
            p.getInventory().clear();
            p.setGameMode(GameMode.SPECTATOR);
            Main.inGame.remove(p);
            Main.spec.add(p);
            if (Main.inGame.size() == 0){
                handleLoosing();
            }
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                p.spigot().respawn();
            }
        }, 20);
    }

    private static Wave getNewWave(){
        if (waves.size() == 0) return null;
        Wave wave = waves.get(0);
        waves.remove(0);
        return wave;
    }

    private static void handleWinning(){
        for (Player p : Main.inGame) {
            p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.MASTER, 100, 1);
            p.sendTitle("§aHerzlichen Glückwunsch", "§aDu hast alle Wellen überstanden", 10, 200, 10);
            Stats stats = Stats.getStats(p);
            stats.wins++;
            Stats.setStats(stats);
            p.setPlayerTime(6000, false);
        }
        Lobbymanager.endGame();
    }

    private static void handleLoosing(){
        for (Player p : Bukkit.getOnlinePlayers()) p.sendMessage("§cDu hast den Angriff der Monster nicht überstanden!");
        Lobbymanager.endGame();
    }

}
