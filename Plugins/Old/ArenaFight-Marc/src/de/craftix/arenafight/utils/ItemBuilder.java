package de.craftix.arenafight.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemBuilder {

    public Material material;
    public String name;
    public int amount = 1;
    public HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    public boolean unbreakable = false;
    public ArrayList<String> lore = new ArrayList<>();

    public ItemBuilder(Material mat){
        material = mat;
    }

    public ItemBuilder(Material mat, int amount){
        material = mat;
        this.amount = amount;
    }

    public ItemBuilder setName(String name){
        this.name = name;
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment e, int level){
        enchantments.put(e, level);
        return this;
    }

    public ItemBuilder setUnbreakable(boolean value){
        unbreakable = value;
        return this;
    }

    public ItemBuilder setLore(String... lore){
        for (String s : lore){
            this.lore.add(s);
        }
        return this;
    }

    public ItemStack create(){
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();
        for (Enchantment e : enchantments.keySet()){
            meta.addEnchant(e, enchantments.get(e), true);
        }
        meta.setLore(lore);
        meta.setDisplayName(name);
        meta.setUnbreakable(unbreakable);
        item.setItemMeta(meta);

        return item;
    }

}
