package de.craftix.arenafight.utils;

public enum Gamestate {
    LOBBY,
    INGAME,
    RESTART
}
