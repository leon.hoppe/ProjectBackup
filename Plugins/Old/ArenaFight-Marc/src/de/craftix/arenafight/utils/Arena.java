package de.craftix.arenafight.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Arena {

    public static boolean checkName(String name){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas WHERE Name = \"" + name + "\"");
            rs.next();
            String test = rs.getString("Name");
            if (test != null) return false;
        }catch (Exception e){}
        return true;
    }

    public static int getNextID(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas");
            ArrayList<Integer> ids = new ArrayList<>();
            while (rs.next()){
                ids.add(rs.getInt("ID"));
            }
            int id = 0;
            while (ids.contains(id)) id++;
            return id;
        }catch (Exception e) {}
        return 0;
    }

    static class ArenaTile {
        public String name;
        public int id;
        public int type;
        public Location loc;

        public ArenaTile() {}
        public ArenaTile(String name, int id, int type, Location loc){
            this.name = name;
            this.id = id;
            this.type = type;
            this.loc = loc;
        }
    }

    public static Arena getArena(String name){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas WHERE Name = \"" + name + "\"");
            ArrayList<ArenaTile> tiles = new ArrayList<>();
            while (rs.next()){
                ArenaTile tile = new ArenaTile();
                tile.name = rs.getString("Name");
                tile.id = rs.getInt("ID");
                tile.type = rs.getInt("Type");
                String x = rs.getString("x");
                String y = rs.getString("y");
                String z = rs.getString("z");
                String yaw = rs.getString("yaw");
                String pitch = rs.getString("pitch");
                String world = rs.getString("world");
                tile.loc = new Location(Bukkit.getWorld(world), Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z), Float.parseFloat(yaw), Float.parseFloat(pitch));
                tiles.add(tile);
            }
            Arena arena = new Arena();
            arena.name = tiles.get(0).name;
            arena.id = tiles.get(0).id;
            for (ArenaTile tile : tiles){
                if (tile.type == 2){ //Spawn
                    arena.spawn = tile.loc;
                }else if (tile.type == 3) { //Spectator
                    arena.spec = tile.loc;
                }else if (tile.type == 4) { //MobSpawn
                    arena.mobSpawns.add(tile.loc);
                }
            }
            return arena;
        }catch (Exception e) {}
        return null;
    }
    public static Arena getArena(int id){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas WHERE ID = " + id);
            ArrayList<ArenaTile> tiles = new ArrayList<>();
            while (rs.next()){
                ArenaTile tile = new ArenaTile();
                tile.name = rs.getString("Name");
                tile.id = rs.getInt("ID");
                tile.type = rs.getInt("Type");
                String x = rs.getString("x");
                String y = rs.getString("y");
                String z = rs.getString("z");
                String yaw = rs.getString("yaw");
                String pitch = rs.getString("pitch");
                String world = rs.getString("world");
                tile.loc = new Location(Bukkit.getWorld(world), Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z), Float.parseFloat(yaw), Float.parseFloat(pitch));
                tiles.add(tile);
            }
            Arena arena = new Arena();
            arena.name = tiles.get(0).name;
            arena.id = tiles.get(0).id;
            for (ArenaTile tile : tiles){
                if (tile.type == 2){ //Spawn
                    arena.spawn = tile.loc;
                }else if (tile.type == 3) { //Spectator
                    arena.spec = tile.loc;
                }else if (tile.type == 4) { //MobSpawn
                    arena.mobSpawns.add(tile.loc);
                }
            }
            return arena;
        }catch (Exception e) {}
        return null;
    }

    public static ArrayList<Arena> getAllArenas(){
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Arenas");
            ArrayList<Integer> ids = new ArrayList<>();
            ArrayList<Arena> arenas = new ArrayList<>();
            while (rs.next()){
                if (rs.getInt("Type") == -1) continue;
                int id = rs.getInt("ID");
                if (ids.contains(id)) continue;
                ids.add(id);
            }
            for (int id : ids){
                arenas.add(getArena(id));
            }
            return arenas;
        }catch (Exception e) {}
        return new ArrayList<>();
    }


    public String name;
    public Integer id;
    public Location spawn;
    public Location spec;
    public ArrayList<Location> mobSpawns = new ArrayList<>();
    public ArrayList<Player> players = new ArrayList<>();
    public ArrayList<Player> spectators = new ArrayList<>();

    public void delete(){
        MySQL.insert("DELETE FROM Arenas WHERE ID = " + id);
    }

    public void save(){
        ArrayList<ArenaTile> tiles = convertToTiles();
        for (ArenaTile tile : tiles){
            String qry1 = "INSERT INTO Arenas (Name, ID, Type, x, y, z, yaw, pitch, world) VALUES (\"" + tile.name + "\", " + tile.id + ", " + tile.type + ", \"";
            String qry2 = tile.loc.getX() + "\", \"" + tile.loc.getY() + "\", \"" + tile.loc.getZ() + "\", \"" + tile.loc.getYaw() + "\", \"" + tile.loc.getPitch() + "\", \"";
            MySQL.insert(qry1 + qry2 + tile.loc.getWorld().getName() + "\")");
        }
    }

    private ArrayList<ArenaTile> convertToTiles(){
        ArrayList<ArenaTile> tiles = new ArrayList<>();
        tiles.add(new ArenaTile(name, id, 2, spawn));
        tiles.add(new ArenaTile(name, id, 3, spec));
        for (Location loc : mobSpawns) tiles.add(new ArenaTile(name, id, 4, loc));
        return tiles;
    }

}
