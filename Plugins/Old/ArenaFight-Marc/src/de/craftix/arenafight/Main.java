package de.craftix.arenafight;

import de.craftix.arenafight.utils.*;
import de.craftix.arenafight.utils.kit.KitAPI;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    private static Main instance;
    private static Setup setup;
    public static Gamestate state;

    public static ArrayList<Player> inGame = new ArrayList<>();
    public static ArrayList<Player> spec = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        state = Gamestate.LOBBY;
        MySQL.connect();
        createTables();
        setup = new Setup(getInstance());
        KitAPI.setup();
        Wave.setup();

        Gamemanager.setArena(Arena.getAllArenas().get(0));
        Location lobbyLoc = setup.getLobby();
        if (lobbyLoc != null){
            World lobby = lobbyLoc.getWorld();
            lobby.setMonsterSpawnLimit(100);
            lobby.setAnimalSpawnLimit(100);
            lobby.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            lobby.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
            lobby.setGameRule(GameRule.DO_MOB_SPAWNING, false);
            lobby.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            lobby.setGameRule(GameRule.RANDOM_TICK_SPEED, 0);
            lobby.setTime(6000);
            for (Entity e : lobby.getEntities()) if (!(e instanceof Player)) e.remove();
        }

        Stats.Setup();

        getCommand("setup").setExecutor(setup);
        getCommand("addarena").setExecutor(setup);
        getCommand("delarena").setExecutor(setup);
        getCommand("setlobby").setExecutor(setup);
        getCommand("stats").setExecutor(new Stats(null));

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(setup, getInstance());
        pm.registerEvents(new Lobbymanager(), this);
        pm.registerEvents(new Gamemanager(), this);
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();
        for (Player p : Bukkit.getOnlinePlayers()) p.kickPlayer("§cServer restart");
    }

    public static Main getInstance() {
        return instance;
    }

    public static Setup getSetup() {
        return setup;
    }

    private void createTables(){
        //Arenas = Name, ID, Type, Location | Types: 1 = lobby, 2 = spawn, 3 = spec, 4 = MobSpawn
        MySQL.insert("CREATE TABLE IF NOT EXISTS Arenas (Name VARCHAR(100), ID INT(10), Type INT(10), x VARCHAR(100), y VARCHAR(100), z VARCHAR(100), yaw VARCHAR(100), pitch VARCHAR(100), world VARCHAR(100))");

    }
}
