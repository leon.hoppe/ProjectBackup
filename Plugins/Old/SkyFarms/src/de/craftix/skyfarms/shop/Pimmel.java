package de.craftix.skyfarms.shop;

import de.craftix.skyfarms.general.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Pimmel implements Listener{

    private static ArrayList<ShopItem> items = new ArrayList<>();
    private static final String GUI_NAME = "§b§lBlöcke:";
    private static final int SIZE = 3*9;

    public static void openGUI(Player p){
        p.openInventory(CreateInvs.blockShop);
    }

    public static Inventory createInv(){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_NAME);

        //Items
        items.add(new ShopItem(100, 10, Material.DIRT, "§6Test", 4));
        ShopItem grass = new ShopItem(30, Material.GRASS_BLOCK, "§6Tolles GRASS", 8);
        grass.amount = 10;
        items.add(grass);

        ItemStack back = new ItemStack(Material.BARRIER);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName("§cZurück");
        back.setItemMeta(backMeta);

        //Set Items
        for (ShopItem sitems : items){
            inv.setItem(sitems.slot, sitems.item);
        }
        inv.setItem(18, back);
        for (int i = 19; i <= 26; i++){
            ItemStack glass = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
            ItemMeta glassMeta = glass.getItemMeta();
            glassMeta.setDisplayName(" ");
            glass.setItemMeta(glassMeta);
            inv.setItem(i, glass);
        }

        return inv;
    }

    @EventHandler
    public void guiClick(InventoryClickEvent event){
        if (!event.getClickedInventory().equals(CreateInvs.blockShop)) return;
        event.setCancelled(true);
        // Right or Left Click
        boolean isLeft = false;
        if (event.getAction().equals(InventoryAction.PICKUP_ALL)) isLeft = true;
        Player p = (Player) event.getWhoClicked();
        switch (event.getSlot()){
            case 4:
                handleClick(getItem(4), p, isLeft);
                break;
            case 8:
                handleClick(getItem(8), p, isLeft);
                break;
            case 18:
                p.openInventory(CreateInvs.mainShop);
                break;

            default:
                break;
        }
    }

    private static ShopItem getItem(int slot){
        for (ShopItem i : items){
            if (i.slot == slot){
                return i;
            }
        }
        return null;
    }

    private static void handleClick(ShopItem item, Player p, boolean left){
        if (!item.sellable && !left){
            p.sendMessage(Messages.prefix + "§cDieses Item kann man nicht verkaufen!");
            return;
        }else if (!left) {
            ItemStack currItem = new ItemStack(item.mat, item.amount, item.subID);
            boolean hasItem = false;
            if (p.getInventory().contains(item.mat)){
                for (int i = item.amount; i <= 64; i++){
                    ItemStack testItem = new ItemStack(item.mat, i, item.subID);
                    if (p.getInventory().contains(testItem)){
                        hasItem = true;
                        break;
                    }
                }
                if (hasItem){

                    p.getInventory().removeItem(currItem);
                    p.sendMessage("§8[§6Konto§8] §a§l+ §r§6" + item.sell);

                }else {
                    p.sendMessage(Messages.prefix + "§cDu hast nicht genügend Items!");
                    return;
                }
            }else {
                p.sendMessage(Messages.prefix + "§cDu hast nicht genügend Items!");
                return;
            }
        }
    }
}
