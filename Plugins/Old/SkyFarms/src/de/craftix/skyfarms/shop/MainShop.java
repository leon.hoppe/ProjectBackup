package de.craftix.skyfarms.shop;

import de.craftix.skyfarms.general.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MainShop implements CommandExecutor, Listener {
    private static final String GUI_NAME = "§b§lShop";
    private static final int SIZE = 3*9;
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player){
            Player p = (Player)sender;
            openGUI(p);
        }else {
            sender.sendMessage(Messages.onlyPlayer);
        }
        return true;
    }

    public static void openGUI(Player p){
        p.openInventory(CreateInvs.mainShop);
    }

    public static Inventory createInv(){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_NAME);

        ItemStack empty = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        ItemMeta emptyMeta = empty.getItemMeta();
        emptyMeta.setDisplayName(" ");
        empty.setItemMeta(emptyMeta);

        //Items
        ItemStack blocks = new ItemStack(Material.BRICKS);
        ItemMeta blockMeta = blocks.getItemMeta();
        blockMeta.setDisplayName("§eBaumaterialien");
        blocks.setItemMeta(blockMeta);

        ItemStack flowers = new ItemStack(Material.PEONY);
        ItemMeta flowersMeta = flowers.getItemMeta();
        flowersMeta.setDisplayName("§ePflanzliches");
        flowers.setItemMeta(flowersMeta);

        ItemStack farm = new ItemStack(Material.WHEAT);
        ItemMeta farmMeta = farm.getItemMeta();
        farmMeta.setDisplayName("§eLandwirtschaft");
        farm.setItemMeta(farmMeta);

        ItemStack eggs = new ItemStack(Material.FOX_SPAWN_EGG);
        ItemMeta eggMeta = eggs.getItemMeta();
        eggMeta.setDisplayName("§eSpawneier");
        eggs.setItemMeta(eggMeta);

        ItemStack erze = new ItemStack(Material.DIAMOND);
        ItemMeta erzMeta = erze.getItemMeta();
        erzMeta.setDisplayName("§eErze");
        erze.setItemMeta(erzMeta);

        ItemStack special = new ItemStack(Material.HEART_OF_THE_SEA);
        ItemMeta sMeta = special.getItemMeta();
        sMeta.setDisplayName("§eSeltenes");
        special.setItemMeta(sMeta);

        ItemStack rstone = new ItemStack(Material.REDSTONE);
        ItemMeta rsMeta = rstone.getItemMeta();
        rsMeta.setDisplayName("§eRedstone");
        rstone.setItemMeta(rsMeta);

        ItemStack food = new ItemStack(Material.APPLE);
        ItemMeta foodMeta = food.getItemMeta();
        foodMeta.setDisplayName("§eNahrung");
        food.setItemMeta(foodMeta);

        ItemStack mobs = new ItemStack(Material.SKELETON_SKULL);
        ItemMeta mobMeta = mobs.getItemMeta();
        mobMeta.setDisplayName("§eMobdrops");
        mobs.setItemMeta(mobMeta);

        inv.setItem(1, blocks);
        inv.setItem(4, farm);
        inv.setItem(7, flowers);
        inv.setItem(11, eggs);
        inv.setItem(13, erze);
        inv.setItem(15, special);
        inv.setItem(19, rstone);
        inv.setItem(22, food);
        inv.setItem(25, mobs);

        int count = 0;
        for (ItemStack all : inv){
            if (all == null){
                inv.setItem(count, empty);
            }
            count++;
        }
        return inv;
    }

    @EventHandler
    public void onKlick(InventoryClickEvent event){
        if (!event.getClickedInventory().equals(CreateInvs.mainShop)) return;
        event.setCancelled(true);
        Player p = (Player)event.getWhoClicked();
        switch (event.getSlot()){
            case 1:
                openGUI(p, CreateInvs.blockShop);
                break;

            case 7:
                openGUI(p, CreateInvs.flowerShop);

            default:
                break;
        }
    }

    private void openGUI(Player p, Inventory inv){
        p.openInventory(inv);
    }
}
