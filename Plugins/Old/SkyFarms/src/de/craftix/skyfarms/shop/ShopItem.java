package de.craftix.skyfarms.shop;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ShopItem {

    public int coast;
    public int sell;
    public int slot;
    public Material mat;
    public String name;
    public boolean sellable;
    public ItemStack item;
    public int amount = 1;
    public short subID = 0;

    public ShopItem (int coast, Material mat, String name, int slot){
        sellable = false;
        this.coast = coast;
        this.slot = slot;
        this.mat = mat;
        this.name = name;
        create();
    }

    public ShopItem (int coast, int sell, Material mat, String name, int slot){
        sellable = true;
        this.coast = coast;
        this.slot = slot;
        this.sell = sell;
        this.mat = mat;
        this.name = name;
        create();
    }

    private void create(){
        ItemStack is = new ItemStack(mat, amount, subID);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(setLore());
        is.setItemMeta(im);
        item = is;
    }

    private ArrayList<String> setLore(){
        ArrayList<String> lore = new ArrayList<>();
        if (sellable){
            lore.add("§7--------------");
            lore.add("§bPreis: §6" + coast);
            lore.add("§bVerkaufen: §6" + sell);
            lore.add("§7--------------");
        }else {
            lore.add("§7-------------------------------------");
            lore.add("§bPreis: §6" + coast);
            lore.add("§cDieses Item kann man nicht verkaufen!");
            lore.add("§7-------------------------------------");
        }
        return lore;
    }

}
