package de.craftix.skyfarms.shop;

import de.craftix.skyfarms.shop.baumaterialien.Baumaterialien;
import de.craftix.skyfarms.shop.pflanzliches.Pflanzliches;
import org.bukkit.inventory.Inventory;

public class CreateInvs {

    public static Inventory mainShop;
    public static Inventory blockShop;
    public static Inventory flowerShop;

    public static void Create(){
        mainShop = MainShop.createInv();
        blockShop = Baumaterialien.createInv();
        flowerShop = Pflanzliches.createInv();
    }

}
