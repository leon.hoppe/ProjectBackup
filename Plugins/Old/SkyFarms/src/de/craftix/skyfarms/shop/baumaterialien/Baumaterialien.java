package de.craftix.skyfarms.shop.baumaterialien;

import de.craftix.skyfarms.shop.CreateInvs;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Baumaterialien implements Listener {

    private static final String GUI_TITLE = "§6Baumaterialien";
    private static final int SIZE = 9*4;

    public static Inventory createInv(){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_TITLE);

        ItemStack empty = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        ItemMeta emptyMeta = empty.getItemMeta();
        emptyMeta.setDisplayName(" ");
        empty.setItemMeta(emptyMeta);

        ItemStack back = new ItemStack(Material.BARRIER);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName("§cZurück");
        back.setItemMeta(backMeta);

        ItemStack stein = new ItemStack(Material.STONE);
        ItemMeta steinMeta = stein.getItemMeta();
        steinMeta.setDisplayName("§eStein");
        stein.setItemMeta(steinMeta);

        ItemStack erde = new ItemStack(Material.GRASS_BLOCK);
        ItemMeta erdeMeta = erde.getItemMeta();
        erdeMeta.setDisplayName("§eErdliches");
        erde.setItemMeta(erdeMeta);

        ItemStack holz = new ItemStack(Material.OAK_WOOD);
        ItemMeta holzMeta = holz.getItemMeta();
        holzMeta.setDisplayName("§eHolz");
        holz.setItemMeta(holzMeta);

        ItemStack wolle = new ItemStack(Material.MAGENTA_WOOL);
        ItemMeta wolleMeta = wolle.getItemMeta();
        wolleMeta.setDisplayName("§eWolle");
        wolle.setItemMeta(wolleMeta);

        ItemStack ton = new ItemStack(Material.TERRACOTTA);
        ItemMeta tonMeta = ton.getItemMeta();
        tonMeta.setDisplayName("§eTon");
        ton.setItemMeta(tonMeta);

        ItemStack beton = new ItemStack(Material.BLACK_TERRACOTTA);
        ItemMeta betonMeta = beton.getItemMeta();
        betonMeta.setDisplayName("§eBeton");
        beton.setItemMeta(betonMeta);

        ItemStack glass = new ItemStack(Material.WHITE_STAINED_GLASS);
        ItemMeta glassMeta = glass.getItemMeta();
        glassMeta.setDisplayName("§eGlass");
        glass.setItemMeta(glassMeta);

        ItemStack other = new ItemStack(Material.BRICKS);
        ItemMeta otherMeta = other.getItemMeta();
        otherMeta.setDisplayName("§eAndere Blöcke");
        other.setItemMeta(otherMeta);

        inv.setItem(1, stein);
        inv.setItem(3, erde);
        inv.setItem(5, holz);
        inv.setItem(21, wolle);
        inv.setItem(23, ton);
        inv.setItem(25, beton);
        inv.setItem(19, glass);
        inv.setItem(7, other);
        inv.setItem(27, back);

        int count = 0;
        for (ItemStack all : inv){
            if (all == null){
                inv.setItem(count, empty);
            }
            count++;
        }

        return inv;
    }

    @EventHandler
    public void onKlick(InventoryClickEvent event){
        if (!event.getClickedInventory().equals(CreateInvs.blockShop)) return;
        event.setCancelled(true);
    }


}
