package de.craftix.skyfarms.shop.pflanzliches;

import de.craftix.skyfarms.shop.CreateInvs;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Pflanzliches implements Listener {

    private static final String GUI_NAME = "§6Pflanzliches";
    private static final int SIZE = 9*4;

    public static Inventory createInv(){
        Inventory inv = Bukkit.createInventory(null, SIZE, GUI_NAME);

        ItemStack empty = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        ItemMeta emptyMeta = empty.getItemMeta();
        emptyMeta.setDisplayName(" ");
        empty.setItemMeta(emptyMeta);

        ItemStack back = new ItemStack(Material.BARRIER);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName("§cZurück");
        back.setItemMeta(backMeta);

        ItemStack blumen = new ItemStack(Material.ROSE_BUSH);
        ItemMeta blumenMeta = blumen.getItemMeta();
        blumenMeta.setDisplayName("§eBlumen");
        blumen.setItemMeta(blumenMeta);

        ItemStack setzlinge = new ItemStack(Material.OAK_SAPLING);
        ItemMeta setzlingeMeta = setzlinge.getItemMeta();
        setzlingeMeta.setDisplayName("§eSetzlinge");
        setzlinge.setItemMeta(setzlingeMeta);

        ItemStack laub = new ItemStack(Material.OAK_LEAVES);
        ItemMeta laubMeta = laub.getItemMeta();
        laubMeta.setDisplayName("§eLaub");
        laub.setItemMeta(laubMeta);

        ItemStack other = new ItemStack(Material.VINE);
        ItemMeta otherMeta = other.getItemMeta();
        otherMeta.setDisplayName("§eAndere Pflanzen");
        other.setItemMeta(otherMeta);

        inv.setItem(10, blumen);
        inv.setItem(12, setzlinge);
        inv.setItem(14, laub);
        inv.setItem(16, other);
        inv.setItem(27, back);

        int count = 0;
        for (ItemStack all : inv){
            if (all == null){
                inv.setItem(count, empty);
            }
            count++;
        }

        return inv;
    }

    @EventHandler
    public void onKlick(InventoryClickEvent event){
        if (!event.getClickedInventory().equals(CreateInvs.flowerShop)) return;
        event.setCancelled(true);
    }

}
