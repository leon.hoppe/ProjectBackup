package de.craftix.skyfarms.general;

public class Messages {

    public static final String prefix = "§7[§6SkyFarms§7] §r";
    public static final String onlyPlayer = prefix + "§cDieser Command kann nur als Spieler ausgeführt werden!";
    public static final String noPermission = prefix + "§cDu hast keine Rechte, diesen Befehl auszuführen!";

}
