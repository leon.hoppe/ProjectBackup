package de.craftix.skyfarms.general;

import de.craftix.skyfarms.shop.CreateInvs;
import de.craftix.skyfarms.shop.MainShop;
import de.craftix.skyfarms.shop.baumaterialien.Baumaterialien;
import de.craftix.skyfarms.shop.pflanzliches.Pflanzliches;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main plugin;

    public void onEnable(){
        plugin = this;
        MainShop shop = new MainShop();
        CreateInvs.Create();

        //Commands
        getCommand("shop").setExecutor(shop);

        //Listeners
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(shop, this);
        pm.registerEvents(new Baumaterialien(), this);
        pm.registerEvents(new Pflanzliches(), this);
    }

    public void onDisable(){
        for (Player all : Bukkit.getOnlinePlayers()){
            all.closeInventory();
        }
    }

    public static Main getPlugin(){
        return plugin;
    }

}
