package de.craftix.skywars.commands;

import de.craftix.skywars.maneger.Gamemaneger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Setlobby implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player){
            if (sender.hasPermission("skywars.setup")){
                Player p = (Player)sender;
                Gamemaneger.saveLobby(p.getLocation());
                p.sendMessage("§aDu hast die Lobby gesetzt");
            }
        }
        return true;
    }
}
