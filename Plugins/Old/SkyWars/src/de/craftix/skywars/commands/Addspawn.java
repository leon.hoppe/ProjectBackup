package de.craftix.skywars.commands;

import de.craftix.skywars.maneger.Gamemaneger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Addspawn implements CommandExecutor {

    private int spawncount = 0;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player){
            if (sender.hasPermission("skywars.setup")){
                Player p = (Player)sender;
                Gamemaneger.saveSpawn(p.getLocation(), spawncount);
                spawncount++;
                p.sendMessage("§aDu hast den " + spawncount + ". Spawn gesetzt");
            }
        }
        return true;
    }
}
