package de.craftix.skywars.commands;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.Gamemaneger;
import de.craftix.skywars.maneger.Gamestates;
import de.craftix.skywars.maneger.Timer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Start implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender.hasPermission("skywars.start")){
            if (Main.state == Gamestates.LOBBY){
                if (Bukkit.getOnlinePlayers().size() >= 2){
                    Timer.stop();
                    Gamemaneger.inGame();
                }
            }
        }
        return true;
    }
}
