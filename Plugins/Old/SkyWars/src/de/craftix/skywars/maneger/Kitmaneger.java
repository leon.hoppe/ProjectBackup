package de.craftix.skywars.maneger;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Kitmaneger {
    public static ArrayList<Kitmaneger> kits = new ArrayList<>();

    public static Kitmaneger getKit (String name){
        for (Kitmaneger kit : kits){
            if (kit.name == name) return kit;
        }
        return null;
    }

    public static int getKitID(String name){
        for (Kitmaneger kit : kits){
            if (kit.name == name) return kits.indexOf(kit);
        }
        return 0;
    }

    public static Inventory getLobbyInv(){
        Inventory inv;
        if (kits.size() > 9*3){
            inv = Bukkit.createInventory(null, 9*6, "§aKits");
        }else{
            inv = Bukkit.createInventory(null, 9*3, "§aKits");
        }
        int slot = 0;
        for (Kitmaneger kit : kits){
            inv.setItem(slot, kit.item);
            slot++;
        }
        return inv;
    }

    public String name;
    public ItemStack item;

    public ItemStack boots;
    public ItemStack leggins;
    public ItemStack chestplate;
    public ItemStack helmet;

    public ArrayList<Kititem> items = new ArrayList<>();

    public Kitmaneger(String name, ItemStack item){
        kits.add(this);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(name);
        item.setItemMeta(im);
        this.name = name;
        this.item = item;
    }

    public void setArmor(Material boots, Material leggins, Material chestplate, Material helmet){
        this.boots = new ItemStack(boots);
        this.leggins = new ItemStack(leggins);
        this.chestplate = new ItemStack(chestplate);
        this.helmet = new ItemStack(helmet);
    }

    public void setItem(int slot, Material mat, int count){
        items.add(new Kititem(new ItemStack(mat, count), slot));
    }

    public void setItem(int slot, Material mat, int count, int subID){
        items.add(new Kititem(new ItemStack(mat, count, (short) subID), slot));
    }

    public void setItem(int slot, Material mat, int count, Enchantment enc, int level){
        ItemStack item = new ItemStack(mat, count);
        item.addEnchantment(enc, level);
        items.add(new Kititem(item, slot));
    }

    public void setItem(int slot, Material mat, int count, int subID, Enchantment enc, int level){
        ItemStack item = new ItemStack(mat, count, (short) subID);
        item.addEnchantment(enc, level);
        items.add(new Kititem(item, slot));
    }

    public void setInv(Player p){
        p.getInventory().clear();
        for (Kititem item : items){
            p.getInventory().setItem(item.slot, item.item);
        }
        p.getInventory().setBoots(boots);
        p.getInventory().setLeggings(leggins);
        p.getInventory().setChestplate(chestplate);
        p.getInventory().setHelmet(helmet);
    }
}

