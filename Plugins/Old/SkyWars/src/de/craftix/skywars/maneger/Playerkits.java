package de.craftix.skywars.maneger;

import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Playerkits {

    public static ArrayList<Playerkits> playerkits = new ArrayList<>();

    public static Kitmaneger getKit(Player p){
        Playerkits pk = null;
        for (Playerkits k : playerkits){
            if (k.player == p) pk = k;
        }
        if (pk == null) return null;
        return Kitmaneger.getKit(pk.kit);
    }

    public static void addPlayer(Player p, String kit){
        Playerkits pk = null;
        for (Playerkits k : playerkits){
            if (k.player == p) pk = k;
        }
        playerkits.remove(pk);
        playerkits.add(new Playerkits(p, kit));
    }

    public String kit;
    public Player player;

    public Playerkits(Player p, String kit){
        this.kit = kit;
        this.player = p;
        playerkits.add(this);
    }

}
