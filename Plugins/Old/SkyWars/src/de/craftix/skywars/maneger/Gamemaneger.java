package de.craftix.skywars.maneger;

import de.craftix.skywars.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Gamemaneger {

    public static ArrayList<Player> inGame = new ArrayList<>();
    public static ArrayList<Player> specs = new ArrayList<>();
    public static int spawnCount = 0;

    private static FileConfiguration config = Main.getPlugin().getConfig();

    public static void saveLobby(Location loc){
        config.set("Lobby.World", loc.getWorld().getName());
        config.set("Lobby.X", loc.getX());
        config.set("Lobby.Y", loc.getY());
        config.set("Lobby.Z", loc.getZ());
        config.set("Lobby.Yaw", loc.getYaw());
        config.set("Lobby.Pitch", loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static void saveSpec(Location loc){
        config.set("Spec.World", loc.getWorld().getName());
        config.set("Spec.X", loc.getX());
        config.set("Spec.Y", loc.getY());
        config.set("Spec.Z", loc.getZ());
        config.set("Spec.Yaw", loc.getYaw());
        config.set("Spec.Pitch", loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static void saveSpawn(Location loc, int id){
        spawnCount++;
        config.set("Spawn.Count", spawnCount);
        config.set("Spawn." + id + ".World", loc.getWorld().getName());
        config.set("Spawn." + id + ".X", loc.getX());
        config.set("Spawn." + id + ".Y", loc.getY());
        config.set("Spawn." + id + ".Z", loc.getZ());
        config.set("Spawn." + id + ".Yaw", loc.getYaw());
        config.set("Spawn." + id + ".Pitch", loc.getPitch());
        Main.getPlugin().saveConfig();
    }

    public static Location getLobby(){
        World world = Bukkit.getWorld(config.getString("Lobby.World"));
        double x = config.getDouble("Lobby.X");
        double y = config.getDouble("Lobby.Y");
        double z = config.getDouble("Lobby.Z");
        float yaw = (float) config.getDouble("Lobby.Yaw");
        float pitch = (float) config.getDouble("Lobby.Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static Location getSpec(){
        World world = Bukkit.getWorld(config.getString("Lobby.Spec"));
        double x = config.getDouble("Spec.X");
        double y = config.getDouble("Spec.Y");
        double z = config.getDouble("Spec.Z");
        float yaw = (float) config.getDouble("Spec.Yaw");
        float pitch = (float) config.getDouble("Spec.Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static Location getSpawn(int id){
        World world = Bukkit.getWorld(config.getString("Spawn." + id + ".World"));
        double x = config.getDouble("Spawn." + id + ".X");
        double y = config.getDouble("Spawn." + id + ".Y");
        double z = config.getDouble("Spawn." + id + ".Z");
        float yaw = (float) config.getDouble("Spawn." + id + ".Yaw");
        float pitch = (float) config.getDouble("Spawn." + id + ".Pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static void lobby(){
        Main.state = Gamestates.LOBBY;
    }

    public static void inGame(){
        Main.state = Gamestates.INGAME;
        for (int i = 1; i <= inGame.size(); i++){
            Player p = inGame.get(i - 1);
            p.teleport(getSpawn(i));
            Kitmaneger kit = Playerkits.getKit(p);
            if (kit == null){
                Playerkits.addPlayer(p, "§aStarter Kit");
                kit = Playerkits.getKit(p);
            }
            kit.setInv(p);
        }
        Timer.start(2, 150);
    }

    public static void restart(Player winner){
        Main.state = Gamestates.RESTART;
        Bukkit.broadcastMessage("§aHerzlichen Glückwunsch §6" + winner.getName() + " §a!");
        for (Player all : Bukkit.getOnlinePlayers()){
            all.teleport(getLobby());
            all.setGameMode(GameMode.SURVIVAL);
        }
        Timer.start(1, 15);
    }

}
