package de.craftix.skywars.maneger;

import de.craftix.skywars.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Timer {

    public static int seconds = 0;

    public static boolean pvp = false;
    public static boolean hunger = false;

    public static void start(int id, int time){
        seconds = time;
        if (id == 0){ //Lobby
            Bukkit.getScheduler().cancelAllTasks();
            Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                int cache = seconds;
                public void run() {
                    switch (cache){
                        case 60: case 30: case 15: case 10: case 5: case 4: case 3: case 2: case 1:
                            Bukkit.broadcastMessage("§aDie Runde startet in §6" + cache + " §aSekunden");
                            break;
                    }
                    if (cache <= 0){
                        Gamemaneger.inGame();
                        Bukkit.getScheduler().cancelAllTasks();
                    }
                    cache--;
                }
            }, 0, 20);
        }
        if (id == 1){ //Restart
            Bukkit.getScheduler().cancelAllTasks();
            Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                int cache = seconds;
                public void run() {
                    switch (cache){
                        case 15: case 10: case 5: case 4: case 3: case 2: case 1:
                            Bukkit.broadcastMessage("§aDer Server stoppt in §6" + cache + " §aSekunden");
                            break;
                    }
                    if (cache <= 0){
                        for (Player all : Bukkit.getOnlinePlayers()) all.kickPlayer(null);
                        Bukkit.getServer().shutdown();
                    }
                    cache--;
                }
            }, 0, 20);
        }
        if (id == 2){ //InGame
            Bukkit.getScheduler().cancelAllTasks();
            Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                int cache = seconds;
                public void run() {
                    switch (cache){
                        case 150:
                            Bukkit.broadcastMessage("§aDie Schutzzeit endet in §62:30 §aMinuten");
                            break;

                        case 120:
                            Bukkit.broadcastMessage("§aDie Schutzzeit endet in §62:00 §aMinuten");
                            break;
                        case 90:
                            Bukkit.broadcastMessage("§aDie Schutzzeit endet in §61:30 §aMinuten");
                            break;
                        case 60: case 30: case 15: case 10: case 5: case 4: case 3: case 2: case 1:
                            Bukkit.broadcastMessage("§aDie Schutzzeit endet in §6" + cache + " §aSekunden");
                            break;
                    }
                    if (cache <= 0){
                        go();
                        Bukkit.getScheduler().cancelAllTasks();
                    }
                    cache--;
                }
            }, 0, 20);
        }
    }

    public static void stop(){
        Bukkit.getScheduler().cancelAllTasks();
    }

    private static void go(){
        pvp = true;
        hunger = true;
        Bukkit.broadcastMessage("§aDas Spiel beginnt!");
    }

}
