package de.craftix.skywars.maneger;

import de.craftix.skywars.general.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class Chestmaneger {

    public static ArrayList<Chestmaneger> chests = new ArrayList<>();

    public static Chestmaneger getRandom(){
        Random rand = new Random();
        if (chests.size() <= 1) return chests.get(0);
        return chests.get(rand.nextInt(chests.size() - 1));
    }

    public static Chestmaneger getChest(Location loc){
        for (Chestmaneger chest : Main.chests){
            if (chest.loc == loc) return chest;
        }
        return null;
    }

    public Location loc;
    private int itemCount = 0;
    private ArrayList<ChestItem> items = new ArrayList<>();

    public Chestmaneger(){
        chests.add(this);
    }

    public void addItem(Material mat, int count, int subID){
        if (itemCount >= 9*3) return;
        boolean isFree = false;
        int slot = 0;
        while (!isFree){
            Random rand = new Random();
            slot = rand.nextInt(26);
            //if (inv.getItem(slot) == null) isFree = true;
            isFree = true;
            for (ChestItem item : items){
                if (item.slot == slot) isFree = false;
            }
        }
        items.add(new ChestItem(slot, new ItemStack(mat, count, (short) subID)));
        itemCount++;
    }

    public void addItem(Material mat, int count, int subID, Enchantment enc, int level){
        if (itemCount >= 9*3) return;
        boolean isFree = false;
        int slot = 0;
        while (!isFree){
            Random rand = new Random();
            slot = rand.nextInt(23);
            isFree = true;
            for (ChestItem item : items){
                if (item.slot == slot) isFree = false;
            }
        }
        ItemStack i = new ItemStack(mat, count, (short) subID);
        i.addEnchantment(enc, level);
        items.add(new ChestItem(slot, i));
        itemCount++;
    }

    public Chest getInv(Chest chest){
        for (ChestItem item : items){
            chest.getInventory().setItem(item.slot, item.item);
        }
        return chest;
    }

}
