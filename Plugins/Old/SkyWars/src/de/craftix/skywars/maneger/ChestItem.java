package de.craftix.skywars.maneger;

import org.bukkit.inventory.ItemStack;

public class ChestItem {

    public ItemStack item;
    public int slot;

    public ChestItem(int slot, ItemStack item){
        this.slot = slot;
        this.item = item;
    }

}
