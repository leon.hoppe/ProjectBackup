package de.craftix.skywars.listener;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.Gamemaneger;
import de.craftix.skywars.maneger.Gamestates;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class onQuit implements Listener {

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent event){
        event.setQuitMessage(null);
        if (Gamemaneger.inGame.contains(event.getPlayer())){
            event.setQuitMessage("§c<< §6" + event.getPlayer().getName());
        }
        Gamemaneger.inGame.remove(event.getPlayer());
        Gamemaneger.specs.remove(event.getPlayer());
        if (Gamemaneger.inGame.size() <= 1 && Main.state == Gamestates.INGAME) Gamemaneger.restart(Gamemaneger.inGame.get(0));
    }

}
