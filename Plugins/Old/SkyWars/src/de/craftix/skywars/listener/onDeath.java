package de.craftix.skywars.listener;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.Gamemaneger;
import de.craftix.skywars.maneger.Gamestates;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class onDeath implements Listener {

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event) {
        event.setDeathMessage(null);
        if (Main.state != Gamestates.INGAME) return;
        if (!Gamemaneger.inGame.contains(event.getEntity())) return;
        event.setDeathMessage("§aDer Spieler §6" + event.getEntity().getName() + " §a ist gestorben");
        Gamemaneger.inGame.remove(event.getEntity());
        if (Gamemaneger.inGame.size() > 1){
            event.getEntity().setGameMode(GameMode.SPECTATOR);
            event.getEntity().teleport(Gamemaneger.getSpec());
        }
        if (Gamemaneger.inGame.size() <= 1){
            event.getEntity().teleport(Gamemaneger.getLobby());
            Gamemaneger.restart(Gamemaneger.inGame.get(0));
        }
    }

}
