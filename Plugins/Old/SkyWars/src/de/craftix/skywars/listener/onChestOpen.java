package de.craftix.skywars.listener;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.Chestmaneger;
import de.craftix.skywars.maneger.Gamemaneger;
import de.craftix.skywars.maneger.Gamestates;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class onChestOpen implements Listener {

    @EventHandler
    public void onChestOpenEvent(PlayerInteractEvent event){
        if (Main.state != Gamestates.INGAME) return;
        if (event.getClickedBlock().getType() != Material.CHEST) return;
        if (!Gamemaneger.inGame.contains(event.getPlayer())) return;
        Chest chest = (Chest) event.getClickedBlock();
        if (Chestmaneger.getChest(event.getClickedBlock().getLocation()) != null){
            Chestmaneger chestmaneger = Chestmaneger.getChest(event.getClickedBlock().getLocation());
            chest = chestmaneger.getInv(chest);
        }else {
            Chestmaneger chestmaneger = Chestmaneger.getRandom();
            chestmaneger.loc = event.getClickedBlock().getLocation();
            Main.chests.add(chestmaneger);
            chest = chestmaneger.getInv(chest);
        }
    }

}
