package de.craftix.skywars.listener;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        if (!Main.checkIfConfigured()) return;
        event.getPlayer().getInventory().clear();
        event.getPlayer().getInventory().setHelmet(null);
        event.getPlayer().getInventory().setLeggings(null);
        event.getPlayer().getInventory().setChestplate(null);
        event.getPlayer().getInventory().setBoots(null);
        if (Main.state != Gamestates.INGAME){
            if (Main.state == Gamestates.LOBBY && Gamemaneger.inGame.size() < Gamemaneger.spawnCount){
                Gamemaneger.inGame.add(event.getPlayer());
                event.getPlayer().teleport(Gamemaneger.getLobby());
                event.setJoinMessage("§a>> §6" + event.getPlayer().getName());
                ItemStack kits = new ItemStack(Material.CHEST);
                ItemMeta kitsMeta = kits.getItemMeta();
                kitsMeta.setDisplayName("§aKits");
                kits.setItemMeta(kitsMeta);
                event.getPlayer().getInventory().setItem(4, kits);
                event.getPlayer().setGameMode(GameMode.SURVIVAL);
            }else {
                event.setJoinMessage(null);
                event.getPlayer().kickPlayer("§cDie Runde ist voll!");
            }
        }else {
            event.setJoinMessage(null);
            Gamemaneger.specs.add(event.getPlayer());
            event.getPlayer().setGameMode(GameMode.SPECTATOR);
        }
        double ram = Gamemaneger.spawnCount;
        if (Gamemaneger.spawnCount % 2 == 1) ram += 1;
        double count = ram / 2;
        if (Gamemaneger.inGame.size() >= count) Timer.start(0, 60);
    }

    @EventHandler
    public void onKlick(PlayerInteractEvent event){
        if (Main.state != Gamestates.LOBBY) return;
        if (event.getItem() == null) return;
        if (event.getItem().getItemMeta().getDisplayName().equals("§aKits")) event.getPlayer().openInventory(Kitmaneger.getLobbyInv());
    }

    @EventHandler
    public void onInvKlick(InventoryClickEvent event){
        if (Main.state != Gamestates.LOBBY) return;
        if (event.getClickedInventory() == null) return;
        if (!event.getClickedInventory().getTitle().equals("§aKits")) return;
        Playerkits.addPlayer((Player) event.getWhoClicked(), event.getCurrentItem().getItemMeta().getDisplayName());
        event.getWhoClicked().sendMessage("§aDu hast das Kit " + event.getCurrentItem().getItemMeta().getDisplayName() + " §a ausgewählt");
        event.setCancelled(true);
    }

}
