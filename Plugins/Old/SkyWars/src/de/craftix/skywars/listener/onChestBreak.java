package de.craftix.skywars.listener;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.Gamestates;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class onChestBreak implements Listener {

    @EventHandler
    public void onChestBreakEvent(BlockBreakEvent event){
        if (Main.state != Gamestates.INGAME) return;
        if (event.getBlock().getType().equals(Material.CHEST)) event.setCancelled(true);
    }

}
