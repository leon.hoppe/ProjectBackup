package de.craftix.skywars.listener;

import de.craftix.skywars.general.Main;
import de.craftix.skywars.maneger.Gamestates;
import de.craftix.skywars.maneger.Timer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class onLobby implements Listener {

    @EventHandler
    public void onBreakEvent(BlockBreakEvent event){
        if (Main.state != Gamestates.INGAME) event.setCancelled(true);
    }

    @EventHandler
    public void onBuildEvent(BlockPlaceEvent event){
        if (Main.state != Gamestates.INGAME) event.setCancelled(true);
    }

    @EventHandler
    public void onFoodEvent(FoodLevelChangeEvent event){
        if (Main.state != Gamestates.INGAME) event.setFoodLevel(25);
        if (Main.state == Gamestates.INGAME && Timer.hunger == false) event.setFoodLevel(25);
    }

    @EventHandler
    public void onDamageEvent(EntityDamageEvent event){
        if (Main.state != Gamestates.INGAME) event.setCancelled(true);
        if (Main.state == Gamestates.INGAME && Timer.pvp == false) event.setCancelled(true);
    }

    @EventHandler
    public void onWeatherEvent(WeatherChangeEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event){
        if (Main.state != Gamestates.INGAME) event.setCancelled(true);
    }

    @EventHandler
    public void onDropEvent(PlayerDropItemEvent event){
        if (Main.state != Gamestates.INGAME) event.setCancelled(true);
    }

}
