package de.craftix.skywars.general;

import de.craftix.skywars.commands.Addspawn;
import de.craftix.skywars.commands.Setlobby;
import de.craftix.skywars.commands.Setspec;
import de.craftix.skywars.commands.Start;
import de.craftix.skywars.listener.*;
import de.craftix.skywars.maneger.Chestmaneger;
import de.craftix.skywars.maneger.Gamemaneger;
import de.craftix.skywars.maneger.Gamestates;
import de.craftix.skywars.maneger.Kitmaneger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    private static Main plugin;
    public static Gamestates state;

    public static ArrayList<Chestmaneger> chests = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;
        Gamemaneger.lobby();
        if (checkIfConfigured()) Gamemaneger.spawnCount = this.getConfig().getInt("Spawn.Count");
        setKits();
        setChests();

        //Listeners
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onChestOpen(), this);
        pm.registerEvents(new onJoin(), this);
        pm.registerEvents(new onQuit(), this);
        pm.registerEvents(new onDeath(), this);
        pm.registerEvents(new onLobby(), this);
        pm.registerEvents(new onChestBreak(), this);

        //Commands
        getCommand("setlobby").setExecutor(new Setlobby());
        getCommand("addspawn").setExecutor(new Addspawn());
        getCommand("setspec").setExecutor(new Setspec());
        getCommand("start").setExecutor(new Start());
    }

    public static Main getPlugin() { return plugin; }

    public static boolean checkIfConfigured(){
        if (!Main.getPlugin().getConfig().contains("Lobby.World")){
            Bukkit.broadcastMessage("§6/setlobby");
            Bukkit.broadcastMessage("§6/addspawn");
            Bukkit.broadcastMessage("§6/setspec");
            Bukkit.broadcastMessage("§6Restarte den Server");
            return false;
        }
        return true;
    }

    private void setKits(){
        //Starter Kit
        Kitmaneger sKit = new Kitmaneger("§aStarter Kit", new ItemStack(Material.WOOD_PICKAXE));
        sKit.setArmor(Material.LEATHER_BOOTS, Material.LEATHER_LEGGINGS, Material.LEATHER_CHESTPLATE, Material.LEATHER_HELMET);
        sKit.setItem(0, Material.STONE_SWORD, 1, Enchantment.DAMAGE_ALL, 3);
        sKit.setItem(1, Material.STONE_PICKAXE, 1);
        sKit.setItem(2, Material.STONE_AXE, 1);
        sKit.setItem(8, Material.COOKED_BEEF, 32);
    }

    private void setChests(){
        //Example
        Chestmaneger exa = new Chestmaneger();
        exa.addItem(Material.STONE, 64, 0);
        exa.addItem(Material.PUMPKIN_PIE, 16, 0);
    }
}
