package de.welovemcskript.autostart;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

public class Autostart extends JavaPlugin {

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {
        if(Bukkit.getServer().getPluginManager().getPlugins() == null) {
            startServer();
        }
    }

    public static void startServer() {
        Process p = null;
        try {
            p = Runtime.getRuntime().exec("cmd /c ../start.sh");
            InputStream is = p.getInputStream();

            int i = 0;
            StringBuffer sb = new StringBuffer();
            while ((i = is.read()) != -1) {
                sb.append((char)i);
            }
            System.out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
