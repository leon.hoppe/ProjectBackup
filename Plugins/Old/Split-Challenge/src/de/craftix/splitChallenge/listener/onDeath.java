package de.craftix.splitChallenge.listener;

import de.craftix.splitChallenge.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class onDeath implements Listener {

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event){
        if (!Main.getTimer().isStarted) return;
        Main.loose();
    }

}
