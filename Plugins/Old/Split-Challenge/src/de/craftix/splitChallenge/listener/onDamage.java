package de.craftix.splitChallenge.listener;

import de.craftix.splitChallenge.Main;
import de.craftix.splitChallenge.commands.Settings;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class onDamage implements Listener {
    private static int id;

    @EventHandler
    public void onDamageEvent(EntityDamageEvent event){
        if (!Main.getTimer().isStarted) return;
        if (!Settings.splitHerzen) return;
        if (!(event.getEntity() instanceof Player)) return;
        Player p = (Player)event.getEntity();
        for (Player all : Bukkit.getOnlinePlayers()){
            if (all.getGameMode().equals(GameMode.SPECTATOR)) continue;
            if (all.getUniqueId().equals(p.getUniqueId())) continue;
            all.setHealth(p.getHealth());
        }
        try{
            Bukkit.getScheduler().cancelTask(id);
        }catch (Exception e){}
        refresh();
    }

    public static void refresh(){
        id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                Player p = Bukkit.getPlayer("xNekr0las");
                for (Player all : Bukkit.getOnlinePlayers()){
                    if (all.getGameMode().equals(GameMode.SPECTATOR)) continue;
                    if (all.getUniqueId().equals(p.getUniqueId())) continue;
                    all.setHealth(p.getHealth());
                }
            }
        }, 0, 50);
    }

}
