package de.craftix.splitChallenge;

import de.craftix.splitChallenge.commands.Backpack;
import de.craftix.splitChallenge.commands.Invsee;
import de.craftix.splitChallenge.commands.Position;
import de.craftix.splitChallenge.commands.Settings;
import de.craftix.splitChallenge.listener.onDamage;
import de.craftix.splitChallenge.listener.onDeath;
import de.craftix.splitChallenge.listener.onStart;
import de.craftix.splitChallenge.listener.onWin;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static Main instance;
    private static Timer timer;

    @Override
    public void onEnable() {
        instance = this;
        timer = new Timer();
        Backpack.bp = Bukkit.createInventory(null, 3*9, "Backpack");

        Settings settings = new Settings();
        Bukkit.getPluginManager().registerEvents(settings, this);
        getCommand("settings").setExecutor(settings);
        getCommand("timer").setExecutor(timer);
        getCommand("position").setExecutor(new Position());
        getCommand("bp").setExecutor(new Backpack());
        getCommand("invsee").setExecutor(new Invsee());

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new onDamage(), this);
        pm.registerEvents(new onStart(), this);
        pm.registerEvents(new onWin(), this);
        pm.registerEvents(new onDeath(), this);
    }

    public static void win(){
        timer.stop();
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setGameMode(GameMode.SPECTATOR);
        }
        Bukkit.broadcastMessage("§6Die Challenge wurde erfolgreich absolviert");
    }

    public static void loose(){
        timer.stop();
        for (Player p : Bukkit.getOnlinePlayers()){
            p.setGameMode(GameMode.SPECTATOR);
        }
    }

    public static Main getInstance() {
        return instance;
    }

    public static Timer getTimer() {
        return timer;
    }
}
