package de.craftix.splitChallenge.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Invsee implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("challenge.invsee")) return false;
        if (!(sender instanceof Player)) return false;
        if (args.length != 1) return false;
        Player p = (Player)sender;
        Player t = Bukkit.getPlayer(args[0]);
        if (t == null){
            p.sendMessage("§cSpieler konnte nicht gefunden werden");
            return false;
        }
        p.openInventory(t.getInventory());
        return false;
    }
}
