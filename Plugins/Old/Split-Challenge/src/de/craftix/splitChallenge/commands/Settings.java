package de.craftix.splitChallenge.commands;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Settings implements CommandExecutor, Listener {
    public static boolean splitHerzen = false;

    private ItemStack split;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("challenge.settings")) return true;
        Player p = (Player)sender;
        p.openInventory(getInv(1));
        return true;
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event){
        if (!event.getView().getTitle().equalsIgnoreCase("Settings")) return;
        if (event.getCurrentItem() == null) return;
        event.setCancelled(true);
        if (event.getCurrentItem().equals(split)){
            splitHerzen = !splitHerzen;
            Bukkit.broadcastMessage("§aSplit-Herzen wurden auf " + splitHerzen + " gesetzt");
        }
    }

    private Inventory getInv(int type){
        if (type == 1){
            Inventory inv = Bukkit.createInventory(null, 3*9, "Settings");
            split = new ItemStack(Material.CRAFTING_TABLE);
            ItemMeta splitMeta = split.getItemMeta();
            splitMeta.setDisplayName("§aSplit Herzen");
            split.setItemMeta(splitMeta);
            inv.setItem(13, split);
            for (int i = 0; i < inv.getSize(); i++){
                if (inv.getItem(i) != null) continue;
                ItemStack glass = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
                ItemMeta glassMeta = glass.getItemMeta();
                glassMeta.setDisplayName(" ");
                glass.setItemMeta(glassMeta);
                inv.setItem(i, glass);
            }
            return inv;
        }
        return null;
    }
}
