package de.craftix.splitChallenge.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Position implements CommandExecutor {
    private HashMap<String, Location> positions = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player p = (Player)sender;
        if (args.length != 1) return false;
        if (positions.containsKey(args[0])){
            Location loc = positions.get(args[0]);
            p.sendMessage("§aDie Position " + args[0] + " Befindet sich bei §b" + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
        }else {
            positions.put(args[0], p.getLocation());
            p.sendMessage("§aDie Position §6" + args[0] + " §awurde erfolgreich gespeichert");
        }
        return false;
    }
}
