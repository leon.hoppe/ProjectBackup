package de.marc.iu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

public final class ItsUtilities extends JavaPlugin {

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new KillListener(), this);

        getServer().addRecipe(new FurnaceRecipe(new ItemStack(Material.RABBIT_HIDE), Material.ROTTEN_FLESH));
        getServer().addRecipe(new FurnaceRecipe(new ItemStack(Material.BLACK_DYE), Material.CHARCOAL));

        //-- Recipe --
        //Horse Armor
        ShapedRecipe DHorseArmorRecipe = new ShapedRecipe(new NamespacedKey(this, "horse_d"), new ItemStack(Material.DIAMOND_HORSE_ARMOR));
        DHorseArmorRecipe.shape("  O", "OOO", "OSO");
        DHorseArmorRecipe.setIngredient('O', Material.DIAMOND);
        DHorseArmorRecipe.setIngredient('S', Material.SADDLE);
        Bukkit.addRecipe(DHorseArmorRecipe);

        ShapedRecipe GHorseArmorRecipe = new ShapedRecipe(new NamespacedKey(this, "horse_g"), new ItemStack(Material.GOLDEN_HORSE_ARMOR));
        GHorseArmorRecipe.shape("  O", "OOO", "OSO");
        GHorseArmorRecipe.setIngredient('O', Material.GOLD_INGOT);
        GHorseArmorRecipe.setIngredient('S', Material.SADDLE);
        Bukkit.addRecipe(GHorseArmorRecipe);

        ShapedRecipe IHorseArmorRecipe = new ShapedRecipe(new NamespacedKey(this, "horse_i"), new ItemStack(Material.IRON_HORSE_ARMOR));
        IHorseArmorRecipe.shape("  O", "OOO", "OSO");
        IHorseArmorRecipe.setIngredient('O', Material.IRON_INGOT);
        IHorseArmorRecipe.setIngredient('S', Material.SADDLE);
        Bukkit.addRecipe(IHorseArmorRecipe);

        //Enchanted Golden Apple
        ShapedRecipe EnchantedAppleRecipe = new ShapedRecipe(new NamespacedKey(this, "enchanted_golden_apple"), new ItemStack(Material.ENCHANTED_GOLDEN_APPLE));
        EnchantedAppleRecipe.shape("GGG", "GAG", "GGG");
        EnchantedAppleRecipe.setIngredient('G', Material.GOLD_BLOCK);
        EnchantedAppleRecipe.setIngredient('A', Material.APPLE);
        Bukkit.addRecipe(EnchantedAppleRecipe);

        //Name Tag
        ShapedRecipe NameTagRecipe = new ShapedRecipe(new NamespacedKey(this, "name_tag"), new ItemStack(Material.NAME_TAG));
        NameTagRecipe.shape(" IS", " PI", "P  ");
        NameTagRecipe.setIngredient('I', Material.IRON_INGOT);
        NameTagRecipe.setIngredient('S', Material.STRING);
        NameTagRecipe.setIngredient('P', Material.PAPER);
        Bukkit.addRecipe(NameTagRecipe);

        //blossom
        ShapedRecipe BlossomRecipe = new ShapedRecipe(new NamespacedKey(this, "blossom"), new ItemStack(Material.SPORE_BLOSSOM));
        BlossomRecipe.shape("MMM", "MAM", "MMM");
        BlossomRecipe.setIngredient('M', Material.MOSS_BLOCK);
        BlossomRecipe.setIngredient('A', Material.ALLIUM);
        Bukkit.addRecipe(BlossomRecipe);
    }
}
