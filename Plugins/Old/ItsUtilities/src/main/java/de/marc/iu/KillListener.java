package de.marc.iu;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class KillListener implements Listener {

    @EventHandler
    public void onPlayerKill(PlayerDeathEvent event) {
        Player player = event.getEntity().getPlayer();
        Player killer = event.getEntity().getKiller();

        if (killer != null) {

            assert player != null;
            if (player.getName().equalsIgnoreCase("ItsMarc")) return;

            ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
            SkullMeta meta = (SkullMeta) head.getItemMeta();
            assert meta != null;
            meta.setOwningPlayer(player);
            meta.setDisplayName("§e" + player.getName() + "'s Head");
            head.setItemMeta(meta);

            killer.getInventory().addItem(head);
            killer.playSound(killer.getLocation(), Sound.BLOCK_ENDER_CHEST_OPEN, 2, 1);
        }

    }

}
