package de.craftix.essentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EcCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (sender instanceof Player){
            if (!sender.hasPermission("essentials.ec")) return true;
            Player p = (Player)sender;
            if (args.length == 0){
                p.openInventory(p.getEnderChest());
            }else if (args.length == 1){
                Player t = Bukkit.getPlayer(args[0]);
                if (t == null) return true;
                p.openInventory(t.getEnderChest());
            }
        }

        return true;
    }
}
