package de.craftix.essentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InvseeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (sender instanceof Player){
            if (!sender.hasPermission("essentials.invsee")) return true;
            Player p = (Player)sender;
            if (args.length == 1){
                Player t = Bukkit.getPlayer(args[0]);
                if (t == null) return true;
                if (t.getName().equals(p.getName())) return true;
                p.openInventory(t.getInventory());
            }
        }

        return true;
    }
}
