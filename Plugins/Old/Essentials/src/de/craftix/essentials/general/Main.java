package de.craftix.essentials.general;

import de.craftix.essentials.commands.EcCommand;
import de.craftix.essentials.commands.InvseeCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        getCommand("ec").setExecutor(new EcCommand());
        getCommand("invsee").setExecutor(new InvseeCommand());
    }
}
