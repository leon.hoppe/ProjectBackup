package de.craftix.signtp;

import de.craftix.signtp.commands.*;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static Main plugin;

    @Override
    public void onEnable() {
        plugin = this;
        registerCommands();
        registerListener();
    }

    private void registerCommands(){
        getCommand("createsign").setExecutor(new Createsign());
    }

    private void registerListener(){

    }

    public static Main getPlugin(){
        return plugin;
    }
}
