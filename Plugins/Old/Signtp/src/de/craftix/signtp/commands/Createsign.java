package de.craftix.signtp.commands;

import de.craftix.signtp.SignAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Createsign implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player){
            Player p = (Player)sender;
            if (!p.hasPermission("signtp.admin")){
                p.sendMessage("§cHierzu hast du keine Rechte");
                return true;
            }
            if (args.length != 1){
                p.sendMessage("§cFalscher Syntax");
                return true;
            }
            Location loc = p.getEyeLocation().getBlock().getLocation();
            if (!loc.getBlock().getType().equals(Material.SIGN)){
                p.sendMessage("§cDu musst ein Schild angucken du spaßt!");
                return true;
            }
            if (Bukkit.getWorld(args[0]) == null){
                p.sendMessage("§cdiese Welt existeiert nicht");
            }
            World world = Bukkit.getWorld(args[0]);
            SignAPI.createSign(loc, world);
        }else {
            sender.sendMessage("§cNur Spieler du Spaßt!");
        }
        return true;
    }
}
