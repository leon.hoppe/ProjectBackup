package de.craftix.signtp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;

public class SignAPI {
    public static ArrayList<WarpSign> signs = new ArrayList<>();

    public static void createSign(Location loc, World world){
        signs.add(new WarpSign(WarpSign.getFreeID(), loc, world));
    }

    public static void removeSign(WarpSign warpSign){
        signs.remove(warpSign);
    }

    public static WarpSign getSign(int id){
        for (WarpSign all : signs){
            if (all.id == id) return all;
        }
        return null;
    }

    public static WarpSign getSign(Location loc){
        for (WarpSign all : signs){
            if (all.loc == loc) return all;
        }
        return null;
    }

    public static void loadSigns(){
        signs.clear();
        FileConfiguration config = Main.getPlugin().getConfig();
        int ids = WarpSign.getFreeID() - 1;
        for (int i = 0; i <= ids; i++){
            WarpSign ws = new WarpSign();
            ws.id = i;
            ws.world = Bukkit.getWorld(config.getString("Sign." + i + "World"));
            double x = config.getDouble("Sign." + i + "Loc.X");
            double y = config.getDouble("Sign." + i + "Loc.Y");
            double z = config.getDouble("Sign." + i + "Loc.Z");
            World w = Bukkit.getWorld(config.getString("Sign." + i + "Loc.World"));
            ws.loc = new Location(w, x, y, z);
            signs.add(ws);
        }
    }

    public static void saveSigns(){
        FileConfiguration config = Main.getPlugin().getConfig();
        for (WarpSign all : signs){
           config.set("Sign." + all.id + "World", all.world.getName());
           config.set("Sign." + all.id + "Loc.X", all.loc.getX());
           config.set("Sign." + all.id + "Loc.Y", all.loc.getY());
           config.set("Sign." + all.id + "Loc.Z", all.loc.getZ());
           config.set("Sign." + all.id + "Loc.World", all.loc.getWorld().getName());
        }
        signs.clear();
    }

}
