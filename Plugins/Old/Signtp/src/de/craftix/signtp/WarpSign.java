package de.craftix.signtp;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;


public class WarpSign {
    public int id;
    public Location loc;
    public World world;

    public WarpSign() {}
    public WarpSign(int id, Location loc, World world){
        this.id = id;
        this.loc = loc;
        this.world = world;
    }

    public static int getFreeID(){
        FileConfiguration config = Main.getPlugin().getConfig();
        int id = -1;
        boolean isFree = false;
        while (!isFree){
            id++;
            if (config.contains("Sign." + id + "World")) isFree = true;
        }
        return id;
    }
}
