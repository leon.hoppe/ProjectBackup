package de.craftix.signtp.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent event){
        Player p = event.getPlayer();

        //Ranks
        if (p.hasPermission("rank.owner")){
            p.setDisplayName("§4Owner §7| §c" + p.getName());
        }else if (p.hasPermission("rank.admin")){
            p.setDisplayName("§cAdmin §7| §c" + p.getName());
        }else {
            p.setDisplayName("§7Spieler §7| §7" + p.getName());
        }

        p.setPlayerListName(p.getDisplayName());
        p.setCustomName(p.getDisplayName());
        p.setCustomNameVisible(true);
    }

}
