package de.cookie.citybuild.main;

import de.cookie.citybuild.manager.*;
import org.bukkit.plugin.PluginManager;
import de.cookie.citybuild.listener.NofallListener;
import de.cookie.citybuild.listener.ChatListener;
import de.cookie.citybuild.listener.DeathListener;
import de.cookie.citybuild.listener.NoHungerListener;
import de.cookie.citybuild.listener.ClickListener;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.Listener;
import de.cookie.citybuild.listener.JQListener;
import de.cookie.citybuild.commands.Vote_CMD;
import de.cookie.citybuild.commands.RemoveFreeItem_CMD;
import de.cookie.citybuild.commands.SetFreeItem_CMD;
import de.cookie.citybuild.commands.Sign_CMD;
import de.cookie.citybuild.commands.RemovePerk_CMD;
import de.cookie.citybuild.commands.AddPerk_CMD;
import de.cookie.citybuild.commands.RemoveGlow_CMD;
import de.cookie.citybuild.commands.SetGlow_CMD;
import de.cookie.citybuild.commands.Rename_CMD;
import de.cookie.citybuild.commands.Repair_CMD;
import de.cookie.citybuild.commands.Perks_CMD;
import de.cookie.citybuild.commands.Globalmute_CMD;
import de.cookie.citybuild.commands.Worlds_CMD;
import de.cookie.citybuild.commands.Settings_CMD;
import de.cookie.citybuild.commands.Enderchest_CMD;
import de.cookie.citybuild.commands.Workbench_CMD;
import de.cookie.citybuild.commands.DelWarp_CMD;
import de.cookie.citybuild.commands.Warp_CMD;
import de.cookie.citybuild.commands.SetWarp_CMD;
import de.cookie.citybuild.commands.Hat_CMD;
import de.cookie.citybuild.commands.Nether_CMD;
import de.cookie.citybuild.commands.SetNether_CMD;
import de.cookie.citybuild.commands.DelHome_CMD;
import de.cookie.citybuild.commands.Home_CMD;
import de.cookie.citybuild.commands.SetHome_CMD;
import de.cookie.citybuild.commands.Invsee_CMD;
import de.cookie.citybuild.commands.DropEvent_CMD;
import de.cookie.citybuild.commands.StartEvent_CMD;
import de.cookie.citybuild.commands.R_CMD;
import de.cookie.citybuild.commands.Msg_CMD;
import de.cookie.citybuild.commands.TpaHere_CMD;
import de.cookie.citybuild.commands.TpaAccept_CMD;
import de.cookie.citybuild.commands.Tpa_CMD;
import de.cookie.citybuild.commands.TpALL_CMD;
import de.cookie.citybuild.commands.TpHere_CMD;
import de.cookie.citybuild.commands.Tp_CMD;
import de.cookie.citybuild.commands.Broadcast_CMD;
import de.cookie.citybuild.commands.Pay_CMD;
import de.cookie.citybuild.commands.RemoveCoins_CMD;
import de.cookie.citybuild.commands.SetCoins_CMD;
import de.cookie.citybuild.commands.AddCoins_CMD;
import de.cookie.citybuild.commands.Coins_CMD;
import de.cookie.citybuild.commands.Kopf_CMD;
import de.cookie.citybuild.commands.Kill_CMD;
import de.cookie.citybuild.commands.ChatClear_CMD;
import de.cookie.citybuild.commands.Clear_CMD;
import de.cookie.citybuild.commands.Night_CMD;
import de.cookie.citybuild.commands.Day_CMD;
import de.cookie.citybuild.commands.Sun_CMD;
import de.cookie.citybuild.commands.Feed_CMD;
import de.cookie.citybuild.commands.Heal_CMD;
import de.cookie.citybuild.commands.Vanish_CMD;
import de.cookie.citybuild.commands.Boost_CMD;
import de.cookie.citybuild.commands.Fly_CMD;
import de.cookie.citybuild.commands.GameMode_CMD;
import de.cookie.citybuild.commands.Spawn_CMD;
import de.cookie.citybuild.commands.SetSpawn_CMD;
import org.bukkit.command.CommandExecutor;
import de.cookie.citybuild.commands.CBS_CMD;
import org.bukkit.inventory.ItemStack;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
    public static Main instance;
    public static MySQL mysql;
    private FileConfiguration cfg;
    private PerksManager perksManager;
    private LocationManager locationManager;
    private FreeItemManager freeItemManager;
    private SettingsManager settingsManager;
    
    public Main() {
        this.cfg = this.getConfig();
        this.perksManager = new PerksManager();
        this.locationManager = new LocationManager();
        this.freeItemManager = new FreeItemManager();
        this.settingsManager = new SettingsManager();
    }
    
    public void onEnable() {
        Main.instance = this;
        FileManager.loadConfig();
        this.connectMySQL();
        this.registerCommands();
        this.registerEvents();
        Bukkit.getConsoleSender().sendMessage(Var.prefix + "§aDas Plugin wurde erfolgreich aktiviert");
        for (final Player all : Bukkit.getOnlinePlayers()) {
            this.perksManager.checkPerks(all);
        }
        if (this.freeItemManager.exists()) {
            Var.freeitem = true;
            this.freeItemManager.startFreeItem((ItemStack)FreeItemManager.fb.getObject("FreeItem.Item"));
        }
        else {
            Var.freeitem = false;
        }
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }
    
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(Var.prefix + "§cDas Plugin wurde erfolgreich deatkiviert");
    }
    
    private void registerCommands() {
        this.getCommand("cbs").setExecutor((CommandExecutor)new CBS_CMD());
        this.getCommand("setspawn").setExecutor((CommandExecutor)new SetSpawn_CMD());
        this.getCommand("spawn").setExecutor((CommandExecutor)new Spawn_CMD());
        this.getCommand("gamemode").setExecutor((CommandExecutor)new GameMode_CMD());
        this.getCommand("fly").setExecutor((CommandExecutor)new Fly_CMD());
        this.getCommand("booster").setExecutor((CommandExecutor)new Boost_CMD());
        this.getCommand("vanish").setExecutor((CommandExecutor)new Vanish_CMD());
        this.getCommand("heal").setExecutor((CommandExecutor)new Heal_CMD());
        this.getCommand("feed").setExecutor((CommandExecutor)new Feed_CMD());
        this.getCommand("sun").setExecutor((CommandExecutor)new Sun_CMD());
        this.getCommand("day").setExecutor((CommandExecutor)new Day_CMD());
        this.getCommand("night").setExecutor((CommandExecutor)new Night_CMD());
        this.getCommand("clear").setExecutor((CommandExecutor)new Clear_CMD());
        this.getCommand("chatclear").setExecutor((CommandExecutor)new ChatClear_CMD());
        this.getCommand("kill").setExecutor((CommandExecutor)new Kill_CMD());
        this.getCommand("kopf").setExecutor((CommandExecutor)new Kopf_CMD());
        this.getCommand("cookies").setExecutor((CommandExecutor)new Coins_CMD());
        this.getCommand("addcookies").setExecutor((CommandExecutor)new AddCoins_CMD());
        this.getCommand("setcookies").setExecutor((CommandExecutor)new SetCoins_CMD());
        this.getCommand("removecookies").setExecutor((CommandExecutor)new RemoveCoins_CMD());
        this.getCommand("pay").setExecutor((CommandExecutor)new Pay_CMD());
        this.getCommand("broadcast").setExecutor((CommandExecutor)new Broadcast_CMD());
        this.getCommand("tp").setExecutor((CommandExecutor)new Tp_CMD());
        this.getCommand("tphere").setExecutor((CommandExecutor)new TpHere_CMD());
        this.getCommand("tpall").setExecutor((CommandExecutor)new TpALL_CMD());
        this.getCommand("tpa").setExecutor((CommandExecutor)new Tpa_CMD());
        this.getCommand("tpaccept").setExecutor((CommandExecutor)new TpaAccept_CMD());
        this.getCommand("tpahere").setExecutor((CommandExecutor)new TpaHere_CMD());
        this.getCommand("msg").setExecutor((CommandExecutor)new Msg_CMD());
        this.getCommand("r").setExecutor((CommandExecutor)new R_CMD());
        this.getCommand("startevent").setExecutor((CommandExecutor)new StartEvent_CMD());
        this.getCommand("dropevent").setExecutor((CommandExecutor)new DropEvent_CMD());
        this.getCommand("invsee").setExecutor((CommandExecutor)new Invsee_CMD());
        this.getCommand("sethome").setExecutor((CommandExecutor)new SetHome_CMD());
        this.getCommand("home").setExecutor((CommandExecutor)new Home_CMD());
        this.getCommand("delhome").setExecutor((CommandExecutor)new DelHome_CMD());
        this.getCommand("setnether").setExecutor((CommandExecutor)new SetNether_CMD());
        this.getCommand("nether").setExecutor((CommandExecutor)new Nether_CMD());
        this.getCommand("hat").setExecutor((CommandExecutor)new Hat_CMD());
        this.getCommand("setwarp").setExecutor((CommandExecutor)new SetWarp_CMD());
        this.getCommand("loc").setExecutor((CommandExecutor)new Warp_CMD());
        this.getCommand("delwarp").setExecutor((CommandExecutor)new DelWarp_CMD());
        this.getCommand("workbench").setExecutor((CommandExecutor)new Workbench_CMD());
        this.getCommand("enderchest").setExecutor((CommandExecutor)new Enderchest_CMD());
        this.getCommand("settings").setExecutor((CommandExecutor)new Settings_CMD());
        this.getCommand("warps").setExecutor((CommandExecutor)new Worlds_CMD());
        this.getCommand("globalmute").setExecutor((CommandExecutor)new Globalmute_CMD());
        this.getCommand("perks").setExecutor((CommandExecutor)new Perks_CMD());
        this.getCommand("repair").setExecutor((CommandExecutor)new Repair_CMD());
        this.getCommand("rename").setExecutor((CommandExecutor)new Rename_CMD());
        this.getCommand("setglow").setExecutor((CommandExecutor)new SetGlow_CMD());
        this.getCommand("removeglow").setExecutor((CommandExecutor)new RemoveGlow_CMD());
        this.getCommand("addperk").setExecutor((CommandExecutor)new AddPerk_CMD());
        this.getCommand("removeperk").setExecutor((CommandExecutor)new RemovePerk_CMD());
        this.getCommand("sign").setExecutor((CommandExecutor)new Sign_CMD());
        this.getCommand("setfreeitem").setExecutor((CommandExecutor)new SetFreeItem_CMD());
        this.getCommand("removefreeitem").setExecutor((CommandExecutor)new RemoveFreeItem_CMD());
        this.getCommand("vote").setExecutor((CommandExecutor)new Vote_CMD());
    }
    
    private void registerEvents() {
        final PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents((Listener)new JQListener(), (Plugin)this);
        pm.registerEvents((Listener)new ClickListener(), (Plugin)this);
        pm.registerEvents((Listener)new NoHungerListener(), (Plugin)this);
        pm.registerEvents((Listener)new DeathListener(), (Plugin)this);
        pm.registerEvents((Listener)new ChatListener(), (Plugin)this);
        pm.registerEvents((Listener)new NofallListener(), (Plugin)this);
        pm.registerEvents((Listener)new VoteManager(), (Plugin)this);
    }
    
    public PerksManager getPerksManager() {
        return this.perksManager;
    }
    
    public LocationManager getLocationManager() {
        return this.locationManager;
    }
    
    public FreeItemManager getFreeItemManager() {
        return this.freeItemManager;
    }
    
    public SettingsManager getSettingsManager() {
        return this.settingsManager;
    }
    
    private void connectMySQL() {
        (Main.mysql = new MySQL(this.cfg.getString("MySQL.HOST"), this.cfg.getString("MySQL.DATABASE"), this.cfg.getString("MySQL.USER"), this.cfg.getString("MySQL.PASSWORD"))).connect();
        Main.mysql.update("CREATE TABLE IF NOT EXISTS Coins(UUID VARCHAR(100), COINS int);");
        Main.mysql.update("CREATE TABLE IF NOT EXISTS Perks(UUID VARCHAR(100), FLY boolean DEFAULT false, NOFALL boolean DEFAULT false, NOHUNGER boolean DEFAULT false, FASTDIGGING boolean DEFAULT false);");
    }

    public static Main getInstance(){
        return instance;
    }
}
