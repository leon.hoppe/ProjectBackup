// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Sound;
import java.util.List;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Sign_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("sign")) {
            if (p.hasPermission("cbs.sign")) {
                if (args.length < 1) {
                    p.sendMessage(Var.use + "/sign <Nachricht>");
                    return true;
                }
                if (p.getItemInHand().getType().equals((Object)Material.AIR)) {
                    p.sendMessage(Var.prefix + "Du musst ein Item in der Hand halten");
                    return true;
                }
                String msg = "§7";
                for (int i = 0; i < args.length; ++i) {
                    msg = msg + " " + args[i].replaceAll("&", "§");
                }
                final ItemStack item = p.getItemInHand();
                final ItemMeta itemStackMeta = item.getItemMeta();
                final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                final String date = dateFormat.format(new Date());
                itemStackMeta.setLore((List)Arrays.asList("§7§m-----------------------", "" + msg, " ", "§6Signiert von: §e" + p.getDisplayName(), "§6Am: §e" + date, "§7§m-----------------------"));
                item.setItemMeta(itemStackMeta);
                p.sendMessage(Var.prefix + "Du hast das Item §aerfolgreich signiert");
                p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 10.0f, 10.0f);
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
