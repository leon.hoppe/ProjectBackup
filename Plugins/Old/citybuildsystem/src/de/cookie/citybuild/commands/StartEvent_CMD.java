
package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Location;
import org.bukkit.Sound;
import de.domedd.developerapi.messagebuilder.ChatMessageBuilder;
import org.bukkit.Bukkit;
import de.cookie.citybuild.manager.EventTimer;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class StartEvent_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("startevent")) {
            if (p.hasPermission("cbs.startevent")) {
                if (args.length == 1) {
                    final Location location = p.getLocation();
                    if (Var.event_drop) {
                        p.sendMessage(Var.prefix + "Es leuft bereits ein §eDropevent");
                        return true;
                    }
                    Var.dropevent.put("dropevent", location);
                    Var.event_drop = true;
                    EventTimer.DropEventTimer();
                    for (final Player all : Bukkit.getOnlinePlayers()) {
                        final ChatMessageBuilder cmb = new ChatMessageBuilder().sendClickableMessage(all, "", Var.prefix + "§e" + p.getName() + " §7hat §7ein §eDropEvent §7gestartet! §7Klicke §7auf §7die §7Nachricht, §7um §7dich §7zu §7teleportieren §7oder §7nutze §e/dropevent", "dropevent");
                        all.playSound(all.getLocation(), Sound.LEVEL_UP, 3.0f, 3.0f);
                        all.sendTitle("§e§lDropEvent", "§7von §e" + p.getName());
                    }
                }
                else {
                    p.sendMessage(Var.use + "/startevent <Dropevent>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
