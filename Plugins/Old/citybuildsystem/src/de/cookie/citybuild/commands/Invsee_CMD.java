// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import org.bukkit.Sound;
import org.bukkit.inventory.Inventory;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Invsee_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final Player target = Bukkit.getPlayer(args[0]);
        if (cmd.getName().equalsIgnoreCase("invsee")) {
            if (p.hasPermission("cbs.invsee")) {
                if (args.length == 1) {
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    p.openInventory((Inventory)target.getInventory());
                    p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1.0f, 1.0f);
                }
                else {
                    p.sendMessage(Var.use + "/invsee <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
