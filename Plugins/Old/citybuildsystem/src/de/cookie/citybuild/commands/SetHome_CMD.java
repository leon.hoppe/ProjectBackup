// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.HomeManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class SetHome_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("sethome")) {
            if (args.length == 1) {
                final String name = args[0];
                final HomeManager manager = new HomeManager(p.getUniqueId());
                manager.addHome(p.getLocation(), name);
                p.sendMessage(Var.prefix + "Du hast das Home §e" + name + " §7erfolgreich gesetzt");
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0f, 3.0f);
            }
            else {
                p.sendMessage(Var.use + "/sethome <Name>");
            }
        }
        return false;
    }
}
