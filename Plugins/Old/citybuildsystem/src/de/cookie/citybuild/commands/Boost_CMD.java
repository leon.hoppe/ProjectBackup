package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import de.cookie.citybuild.manager.BoosterManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Boost_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player) sender;

        long lastUsed = 0;
        if (cmd.getName().equalsIgnoreCase("booster")) {
            if (p.hasPermission("cbs.booster")) {
                if (args.length == 0) {
                        BoosterManager.openBoosterGUI(p);

                } else {
                    p.sendMessage(Var.use + "/booster");
                }
            } else {
                p.sendMessage(Var.noperms);
            }
        }
return false;
    }
}





