

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.SettingsManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Msg_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final SettingsManager settingsManager = new SettingsManager();
        if (cmd.getName().equalsIgnoreCase("msg") && args.length >= 2) {
            if (Main.instance.getConfig().getBoolean("PrivateMessages")) {
                final Player target = Bukkit.getPlayer(args[0]);
                String message = "";
                if (target == null) {
                    p.sendMessage(Var.prefix + "§7Der Spieler §e" + args[0] + " §7ist nicht online");
                    return true;
                }
                if (!settingsManager.getMsg(target.getUniqueId())) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7empf\u00e4ngt keine privaten Nachrichten");
                    return true;
                }
                for (int i = 1; i < args.length; ++i) {
                    message = message + " " + args[i];
                }
                p.sendMessage("§7[§6§lMSG§7] §7Du §8-> §e§l" + target.getName() + "§8: §e" + message);
                target.sendMessage("§7[§6§lMSG§7] §e§l" + p.getName() + " §8-> §7Dir§8: §e" + message);
                Var.messager.put(p, target);
                Var.messager.put(target, p);
            }
            else {
                p.sendMessage(Var.use + "/msg <Spieler> <Nachricht>");
            }
        }
        return false;
    }
}
