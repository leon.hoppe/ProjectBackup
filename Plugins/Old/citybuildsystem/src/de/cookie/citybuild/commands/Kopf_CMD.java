// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.List;
import java.util.ArrayList;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Kopf_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("kopf")) {
            if (p.hasPermission("cbs.kopf")) {
                if (args.length == 1) {
                    final String target = args[0];
                    final ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
                    final SkullMeta skullm = (SkullMeta)skull.getItemMeta();
                    skullm.setDisplayName("§b§l" + target);
                    final ArrayList<String> lore = new ArrayList<String>();
                    lore.add("§7Kopf von §b§l" + target);
                    skullm.setOwner(target);
                    skullm.setLore(lore);
                    skull.setItemMeta((ItemMeta)skullm);
                    p.getInventory().addItem(new ItemStack[] { skull });
                    p.updateInventory();
                    p.sendMessage(Var.prefix + "§7Du hast den Kopf von §e" + args[0] + " §7bekommen");
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 2.0f, 2.0f);
                }
                else {
                    p.sendMessage(Var.use + "/kopf <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
