// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.LocationManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Spawn_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final LocationManager manager = new LocationManager();
        if (cmd.getName().equalsIgnoreCase("spawn")) {
            if (args.length == 0) {
                if (!manager.exists("Spawn")) {
                    p.sendMessage(Var.prefix + "§cDer Spawn wurde noch nicht gesetzt! Setze ihn mit §e/setspawn");
                }
                else {
                    p.teleport(manager.getLocation("Spawn"));
                    p.sendMessage(Var.prefix + "Du wurdest erfolgreich zum §eSpawn §7teleportiert");
                    p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
                }
            }
            else {
                p.sendMessage(Var.use + "/spawn");
            }
        }
        return false;
    }
}
