// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Feed_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("feed")) {
            if (p.hasPermission("cbs.feed")) {
                if (args.length == 0) {
                    p.setFoodLevel(20);
                    p.setSaturation(20.0f);
                    p.sendMessage(Var.prefix + "Du hast nun §ekeinen Hunger mehr");
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 4.0f, 4.0f);
                }
                else if (args.length == 1) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    target.setFoodLevel(20);
                    target.setSaturation(20.0f);
                    target.sendMessage(Var.prefix + "Der Spieler §e" + p.getName() + " §7hat dir deinen Hunger mit Keksen aufgef\u00fcllt");
                }
                else {
                    p.sendMessage(Var.use + "/feed");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
