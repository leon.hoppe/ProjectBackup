// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Location;
import de.cookie.citybuild.manager.LocationManager;
import org.bukkit.Sound;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class SetSpawn_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final LocationManager manager = Main.instance.getLocationManager();
        if (cmd.getName().equalsIgnoreCase("setspawn")) {
            if (p.hasPermission("cbs.admin")) {
                if (args.length == 0) {
                    final Location location = p.getLocation();
                    manager.setLocation("Spawn", location);
                    p.sendMessage(Var.prefix + "Du hast den §eSpawn §7erfolgreich gesetzt");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 3.0f, 3.0f);
                }
                else {
                    p.sendMessage(Var.use + "/setspawn");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
