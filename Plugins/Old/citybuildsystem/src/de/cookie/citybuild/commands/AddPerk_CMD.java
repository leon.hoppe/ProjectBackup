

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.PerksManager;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class AddPerk_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final PerksManager perksManager = Main.instance.getPerksManager();
        if (cmd.getName().equalsIgnoreCase("addperk")) {
            if (p.hasPermission("cbs.addperk")) {
                if (args.length == 2) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("fly")) {
                        if (perksManager.hasPerk(target.getUniqueId(), "FLY")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7hat das Perk bereits");
                            return true;
                        }
                        perksManager.addPerk(target.getUniqueId(), "FLY");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eFly §7gegeben");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eFly §7gegeben");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                    }
                    else if (args[1].equalsIgnoreCase("nofall")) {
                        if (perksManager.hasPerk(target.getUniqueId(), "NOFALL")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7hat das Perk bereits");
                            return true;
                        }
                        perksManager.addPerk(target.getUniqueId(), "NOFALL");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eNofall §7gegeben");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eNofall §7gegeben");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                    }
                    else if (args[1].equalsIgnoreCase("nohunger")) {
                        if (perksManager.hasPerk(target.getUniqueId(), "NOHUNGER")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7hat das Perk bereits");
                            return true;
                        }
                        perksManager.addPerk(target.getUniqueId(), "NOHUNGER");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eNoHunger §7gegeben");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eNoHunger §7gegeben");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                    }
                    else if (args[1].equalsIgnoreCase("fastdigging")) {
                        if (perksManager.hasPerk(target.getUniqueId(), "FASTDIGGING")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7hat das Perk bereits");
                            return true;
                        }
                        perksManager.addPerk(target.getUniqueId(), "FASTDIGGING");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eFastdigging §7gegeben");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eFastdigging §7gegeben");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                    }
                    else {
                        p.sendMessage(Var.use + "/addperk <Spieler> <Fly, Nofall, Nohunger, Fastdigging");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/addperk <Spieler> <Fly, Nofall, Nohunger, Fastdigging");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
