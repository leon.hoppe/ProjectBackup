// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Sound;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Rename_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("rename") && p.hasPermission("cbs.rename")) {
            if (args.length == 0) {
                p.sendMessage(Var.use + "/rename <Name>");
                return true;
            }
            if (p.getItemInHand().getType().equals((Object)Material.AIR)) {
                p.sendMessage(Var.prefix + "Du musst ein Item in der Hand halten");
                return true;
            }
            final ItemStack item = p.getItemInHand();
            String name = "";
            for (int i = 0; i < args.length; ++i) {
                name = name + " " + args[i].replaceAll("&", "§");
            }
            final ItemMeta itemStackMeta = item.getItemMeta();
            itemStackMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            item.setItemMeta(itemStackMeta);
            p.sendMessage(Var.prefix + "Du hast das Item §aerfolgreich umbenannt");
            p.playSound(p.getLocation(), Sound.NOTE_PLING, 2.0f, 2.0f);
        }
        return false;
    }
}
