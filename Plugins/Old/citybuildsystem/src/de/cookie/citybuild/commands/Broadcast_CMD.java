package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Broadcast_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("broadcast")) {
            if (p.hasPermission("cbs.broadcast")) {
                String msg = "";
                for (int i = 0; i < args.length; ++i) {
                    msg = msg + " " + args[i].replaceAll("&", "§");
                }
                Bukkit.broadcastMessage("§8[§a§lRUNDRUF§8]§b§l" + msg);
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
