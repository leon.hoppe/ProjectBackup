// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Hat_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("hat")) {
            if (p.hasPermission("cbs.hat")) {
                if (args.length == 0) {
                    if (p.getInventory().getItemInHand().equals((Object)Material.AIR)) {
                        p.sendMessage(Var.prefix + "Du musst ein §eItem in der Hand halten");
                        return true;
                    }
                    p.getInventory().setHelmet(p.getItemInHand());
                    p.getInventory().setItemInHand((ItemStack)null);
                    p.sendMessage(Var.prefix + "Du hast nun den Block als Kopf");
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 4.0f, 4.0f);
                }
                else {
                    p.sendMessage(Var.use + "/hat");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
