// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Vanish_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("vanish")) {
            if (p.hasPermission("cbs.vanish")) {
                if (args.length == 0) {
                    if (!Var.vanish.contains(p.getName())) {
                        for (final Player all : Bukkit.getOnlinePlayers()) {
                            all.hidePlayer(p);
                        }
                        Var.vanish.add(p.getName());
                        Bukkit.broadcastMessage("§7» §e" + p.getName() + " §7hat das Spiel §cverlassen");
                        p.sendMessage(Var.prefix + "Du bist nun §eunsichtbar");
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                    }
                    else {
                        Var.vanish.remove(p.getName());
                        for (final Player all : Bukkit.getOnlinePlayers()) {
                            all.showPlayer(p);
                        }
                        Bukkit.broadcastMessage("§7» §e" + p.getName() + " §7hat das Spiel §abetreten");
                        p.sendMessage(Var.prefix + "Du bist nun §cnicht mehr §eunsichtbar");
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                    }
                }
                else if (args.length == 1) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nicht online");
                        return true;
                    }
                    if (!Var.vanish.contains(target.getName())) {
                        Var.vanish.add(target.getName());
                        for (final Player all2 : Bukkit.getOnlinePlayers()) {
                            all2.hidePlayer(target);
                        }
                        target.sendMessage(Var.prefix + "Du bist nun §eunsichtbar");
                        Bukkit.broadcastMessage("§7» §e" + target.getName() + " §7hat das Spiel §cverlassen");
                        target.playSound(target.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nun §eunsichtbar");
                    }
                    else {
                        Var.vanish.remove(target.getName());
                        for (final Player all2 : Bukkit.getOnlinePlayers()) {
                            all2.showPlayer(target);
                        }
                        target.sendMessage(Var.prefix + "Du bist nun §cnicht mehr §eunsichtbar");
                        Bukkit.broadcastMessage("§7» §e" + target.getName() + " §7hat das Spiel §abetreten");
                        target.playSound(target.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nun §cnicht mehr §eunsichtbar");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/vanish <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
