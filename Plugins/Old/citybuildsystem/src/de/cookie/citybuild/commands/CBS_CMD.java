// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class CBS_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("cbs")) {
            if (p.hasPermission("cbs.help")) {
                if (args.length == 0) {
                    p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l1 §7\u259c§7§m----");
                    p.sendMessage("§e/cbs <1-8>: §7Zeige die Hilfeseiten");
                    p.sendMessage("§e/cookies <Spieler>: §7Zeige die Coins eines Spielers an");
                    p.sendMessage("§e/addcookies <Spieler> <Anzahl>: §7Adde einem Spieler Coins");
                    p.sendMessage("§e/setcookies <Spieler> <Anzahl>: §7Setze einem Spieler eine bestimmte Anzahl an Coins");
                    p.sendMessage("§e/removecookies <Spieler> <Anzahl>: §7Ziehe einem Spieler eine bestimmte Anzahl an Coins ab");
                    p.sendMessage("§e/booster: §7Aktiviere Booster");
                    p.sendMessage("§e/broadcast [bc] <Nachricht>: §7Mache einen Rundruf");
                    p.sendMessage("§e/chatclear [cc] <Spieler>: §7Leere den Chat");
                    p.sendMessage(Var.prefix + "§7§m---------------------------");
                }
                else if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("1")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l1 §7\u259c§7§m----");
                        p.sendMessage("§e/cbs <1-7>: §7Zeige die Hilfeseiten");
                        p.sendMessage("§e/cookies <Spieler>: §7Zeige die Coins eines Spielers an");
                        p.sendMessage("§e/addcookies <Spieler> <Anzahl>: §7Adde einem Spieler Coins");
                        p.sendMessage("§e/setcookies <Spieler> <Anzahl>: §7Setze einem Spieler eine bestimmte Anzahl an Coins");
                        p.sendMessage("§e/removecookies <Spieler> <Anzahl>: §7Ziehe einem Spieler eine bestimmte Anzahl an Coins ab");
                        p.sendMessage("§e/booster: §7Aktiviere Booster");
                        p.sendMessage("§e/broadcast [bc] <Nachricht>: §7Mache einen Rundruf");
                        p.sendMessage("§e/chatclear [cc] <Spieler>: §7Leere den Chat");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("2")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l2 §7\u259c§7§m----");
                        p.sendMessage("§e/clear <Spieler>: §7Leere das Inventar eines Spielers");
                        p.sendMessage("§e/day: §7\u00c4ndere die Tageszeit");
                        p.sendMessage("§e/night: §7\u00c4ndere die Tageszeit");
                        p.sendMessage("§e/home | /home <Name>: §7Zeige dir deine Homes an und teleportiere dich");
                        p.sendMessage("§e/sethome <Name>: §7Setze das Home");
                        p.sendMessage("§e/delhome <Name>: §7L\u00f6sche ein Home");
                        p.sendMessage("§e/heal <Spieler>: §7Heile dich oder einen Spieler");
                        p.sendMessage("§e/feed <Sieler>: §7F\u00fclle den Hunger eines Spielers auf");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("3")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l3 §7\u259c§7§m----");
                        p.sendMessage("§e/fly <Spieler>: §7Gib einem Spieler Fly");
                        p.sendMessage("§e/gamemode [gm] <Spieler> <GameMode>: §7\u00c4ndere den GameMode eines Spielers");
                        p.sendMessage("§e/hat: §7Setze dir den Block in deiner Hand auf");
                        p.sendMessage("§e/invsee <Spieler>: §7Schaue in das Inventar von anderen Spielern");
                        p.sendMessage("§e/kill <Spieler>: §7T\u00f6te einen Spieler");
                        p.sendMessage("§e/kopf <Spieler>: §7Gebe dir einen Spielerkopf");
                        p.sendMessage("§e/msg <Spieler> <Nachricht>: §7Schreibe einem Spieler eine Nachricht");
                        p.sendMessage("§e/r <Nachricht>: §7Antworte auf eine Nachricht");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("4")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l4 §7\u259c§7§m----");
                        p.sendMessage("§e/pay <Spieler> <Betrag>: §7Zahle einem Spieler Geld");
                        p.sendMessage("§e/setfarmwelt: §7Setze die Farmwelt");
                        p.sendMessage("§e/farmwelt: §7Teleportiere dich in die Farmwelt");
                        p.sendMessage("§e/setnether: §7Setze den Nether");
                        p.sendMessage("§e/nether: §7Teleportiere dich in den Nether");
                        p.sendMessage("§e/setspawn: §7Setze den Spawn");
                        p.sendMessage("§e/spawn: §7Teleportiere dich zum Spawn");
                        p.sendMessage("§e/sun: §7Lasse die Sonne scheinen");
                        p.sendMessage("§e/vanish [v] <Spieler>: §7Mache dich unsichtbar f\u00fcr andere Spieler");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("5")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l5 §7\u259c§7§m----");
                        p.sendMessage("§e/startevent <Dropevent>: §7Starte ein Dropevent");
                        p.sendMessage("§e/dropevent: §7Teleportiere dich zu dem Dropevent");
                        p.sendMessage("§e/tp <Spieler> | /tp <Spieler> <Spieler>: §7Teleportiere dich zu einem Spieler");
                        p.sendMessage("§e/tphere <Spieler>: §7Teleportiere einen Spieler zu dir");
                        p.sendMessage("§e/tpall: §7Teleportiere alle Spieler zu dir");
                        p.sendMessage("§e/tpa <Spieler>: §7Frage, ob du dich zu einem Spieler teleportieren kannst");
                        p.sendMessage("§e/tpahere <Spieler>: §7Frage, ob ein Spieler sich zu dir teleportieren m\u00f6chte");
                        p.sendMessage("§e/tpaccept: §7Akzeptiere eine Teleportanfrage");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("6")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l6 §7\u259c§7§m----");
                        p.sendMessage("§e/loc <Name>: §7Zeige alle Location/Warps an und teleportiere dich zu den Locations/Warps");
                        p.sendMessage("§e/setwarp <Name>: §7Setze eine Location");
                        p.sendMessage("§e/delwarp <Name>: §7Enfferne eine Location");
                        p.sendMessage("§e/ec <Spieler>: §7\u00d6ffne die Enderchest eines Spielers");
                        p.sendMessage("§e/wb: §7\u00d6ffne eine Werkbank");
                        p.sendMessage("§e/settings: §7Stelle deine Settings ein");
                        p.sendMessage("§e/warp: §7Teleportiere dich zu den Welten");
                        p.sendMessage("§e/globalmute: §7Schalte den kompletten Chat aus");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("7")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l7 §7\u259c§7§m----");
                        p.sendMessage("§e/perks: §7\u00d6ffnet das Perkmen\u00fc");
                        p.sendMessage("§e/repair: §7Repariere das Item in deiner Hand");
                        p.sendMessage("§e/rename: §7\u00c4ndere den Namen des Items in deiner Hand");
                        p.sendMessage("§e/glow: §7Gib einem Item ein Glow Effekt");
                        p.sendMessage("§e/removeglow: §7Entferne den Glow Effekt von dem Item in deiner Hand");
                        p.sendMessage("§e/addperk: §7Gib einem Spieler ein Perk");
                        p.sendMessage("§e/removeperk: §7Entferne einem Spieler ein Perk");
                        p.sendMessage("§e/sign: §7Signiere ein Item");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                    else if (args[0].equalsIgnoreCase("8")) {
                        p.sendMessage("§7§m----§7\u259b §6Commands Seite §e§l8 §7\u259c§7§m----");
                        p.sendMessage("§e/setfreeitem: §7Setze das FreeItem");
                        p.sendMessage("§e/removefreeitem: §7Entferne das FreeItem");
                        p.sendMessage("");
                        p.sendMessage("Programmiert von CookieMC337");
                        p.sendMessage("");
                        p.sendMessage("Bei Problemem beim System Bitte eine Mail an");
                        p.sendMessage("Admin@cookiemc.tk");
                        p.sendMessage("Senden");
                        p.sendMessage("");
                        p.sendMessage(Var.prefix + "§7§m---------------------------");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/cbs <1, 2, 3, 4, 5, 6, 7>");
                }
            }
            else {
                p.sendMessage(Var.use + "/cbs <1, 2, 3, 4, 5, 6, 7>");
            }
        }
        else {
            p.sendMessage(Var.noperms);
        }
        return false;
    }
}
