// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Location;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.WarpManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class SetWarp_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final WarpManager manager = new WarpManager();
        if (cmd.getName().equalsIgnoreCase("setwarp")) {
            if (p.hasPermission("cbs.setwarp")) {
                if (args.length == 1) {
                    final String name = args[0];
                    final Location location = p.getLocation();
                    if (manager.getWarps().contains(name)) {
                        p.sendMessage(Var.prefix + "Du hast die Location §e" + name + " §7erfolgreich neu gesetzt");
                        manager.setWarp(location, name);
                        p.playSound(p.getLocation(), Sound.NOTE_PLING, 5.0f, 5.0f);
                    }
                    else {
                        manager.setWarp(location, name);
                        p.sendMessage(Var.prefix + "Du hast die Location §e" + name + " §7erfolgreich gesetzt");
                        p.playSound(p.getLocation(), Sound.NOTE_PLING, 5.0f, 5.0f);
                    }
                }
                else {
                    p.sendMessage(Var.use + "/setloc <Name>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
