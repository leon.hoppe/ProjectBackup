// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class TpHere_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final Player target = Bukkit.getPlayer(args[0]);
        if (cmd.getName().equalsIgnoreCase("tphere")) {
            if (p.hasPermission("cbs.tphere")) {
                if (args.length == 1) {
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    target.teleport(p.getLocation());
                    target.playSound(target.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
                    p.sendMessage(Var.prefix + "Du hast §e" + target.getName() + " §7zu dir teleportiert");
                    target.sendMessage(Var.prefix + "Der Spieler §e" + p.getName() + " §7hat dich zu sich teleportiert");
                }
                else {
                    p.sendMessage(Var.use + "/tphere <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
