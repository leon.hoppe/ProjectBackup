

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class DropEvent_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("dropevent")) {
            if (args.length == 0) {
                if (Var.event_drop) {
                    p.teleport((Location)Var.dropevent.get("dropevent"));
                    p.sendMessage(Var.prefix + "Du wurdest erfolgreich zum §eDropEvent §7teleportiert");
                    p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 3.0f, 3.0f);
                }
                else {
                    p.sendMessage(Var.prefix + "Es l\u00e4uft kein DropEvent");
                }
            }
            else {
                p.sendMessage(Var.use + "/dropevent");
            }
        }
        return false;
    }
}
