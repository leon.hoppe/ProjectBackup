// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class GameMode_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("gamemode")) {
            if (p.hasPermission("cbs.gamemode")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("0")) {
                        p.setGameMode(GameMode.SURVIVAL);
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Du bist nun im §eSurvivalmode");
                    }
                    else if (args[0].equalsIgnoreCase("1")) {
                        p.setGameMode(GameMode.CREATIVE);
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Du bist nun im §eCreativemode");
                    }
                    else if (args[0].equalsIgnoreCase("2")) {
                        p.setGameMode(GameMode.ADVENTURE);
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Du bist nun im §eAdventuremode");
                    }
                    else if (args[0].equalsIgnoreCase("3")) {
                        p.setGameMode(GameMode.SPECTATOR);
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Du bist nun im §eSpectatormode");
                    }
                }
                else if (args.length == 2) {
                    final Player target = Bukkit.getPlayer(args[1]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[1] + " §7ist nicht online");
                        return true;
                    }
                    if (args[0].equalsIgnoreCase("0")) {
                        target.setGameMode(GameMode.SURVIVAL);
                        target.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        target.sendMessage(Var.prefix + "Du bist nun im §eSurvivalmode");
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nun im §eSurvivalmode");
                    }
                    else if (args[0].equalsIgnoreCase("1")) {
                        target.setGameMode(GameMode.CREATIVE);
                        target.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        target.sendMessage(Var.prefix + "Du bist nun im §eCreativemode");
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nun im §eCreativemode");
                    }
                    else if (args[0].equalsIgnoreCase("2")) {
                        target.setGameMode(GameMode.ADVENTURE);
                        target.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        target.sendMessage(Var.prefix + "Du bist nun im §eAdventuremode");
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nun im §eAdventuremode");
                    }
                    else if (args[0].equalsIgnoreCase("3")) {
                        target.setGameMode(GameMode.SPECTATOR);
                        target.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
                        target.sendMessage(Var.prefix + "Du bist nun im §eSpectatormode");
                        p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7ist nun im §eSpectatormode");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/gm <0, 1, 2, 3> <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
