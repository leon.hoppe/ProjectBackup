// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import de.cookie.citybuild.manager.FreeItemManager;
import org.bukkit.Sound;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class RemoveFreeItem_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final FreeItemManager freeItemManager = Main.instance.getFreeItemManager();
        if (cmd.getName().equalsIgnoreCase("removefreeitem")) {
            if (p.hasPermission("cbs.freeitem")) {
                if (args.length == 0) {
                    Var.freeitem = false;
                    freeItemManager.removeFreeItem();
                    p.sendMessage(Var.prefix + "Du hast das §eFreeItem §7erfolgreich §centfernt");
                    p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 8.0f, 8.0f);
                }
                else {
                    p.sendMessage(Var.prefix + "/removefreeitem");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
