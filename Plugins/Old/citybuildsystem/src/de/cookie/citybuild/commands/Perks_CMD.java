// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.PerksManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Perks_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final PerksManager perksManager = new PerksManager();
        if (cmd.getName().equalsIgnoreCase("perks")) {
            if (args.length == 0) {
                perksManager.openPerksInventory(p);
            }
            else {
                p.sendMessage(Var.use + "/perks");
            }
        }
        return false;
    }
}
