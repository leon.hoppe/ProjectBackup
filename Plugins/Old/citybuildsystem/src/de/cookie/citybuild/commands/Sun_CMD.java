// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Sun_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("sun")) {
            if (p.hasPermission("cbs.sun")) {
                if (args.length == 0) {
                    p.getWorld().setThundering(false);
                    p.getWorld().setStorm(false);
                    p.sendMessage(Var.prefix + "Du hast nun f\u00fcr alle Spieler die §eSonne scheinen §7lassen");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 3.0f, 3.0f);
                }
                else {
                    p.sendMessage(Var.use + "/sun");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
