// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class SetGlow_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("setglow")) {
            if (p.hasPermission("cbs.glow")) {
                if (args.length == 0) {
                    final ItemStack item = p.getItemInHand();
                    final ItemMeta itemStackMeta = item.getItemMeta();
                    itemStackMeta.addEnchant(Enchantment.LURE, 0, true);
                    itemStackMeta.addItemFlags(new ItemFlag[] { ItemFlag.HIDE_ENCHANTS });
                    item.setItemMeta(itemStackMeta);
                    p.sendMessage(Var.prefix + "Du hast dem Item den Glow Effekt §aerfolgreich gegeben");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 2.0f, 2.0f);
                }
                else {
                    p.sendMessage(Var.use + "/setglow");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
