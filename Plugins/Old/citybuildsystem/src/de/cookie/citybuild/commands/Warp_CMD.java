// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.WarpManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Warp_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final WarpManager manager = new WarpManager();
        if (cmd.getName().equalsIgnoreCase("loc")) {
            if (args.length == 0) {
                if (manager.getWarps().size() == 0) {
                    p.sendMessage(Var.prefix + "Es gibt noch keine Locations! Setze Warps mit §e/setloc <Name>");
                    return true;
                }
                String out = "";
                for (final String s : manager.getWarps()) {
                    out = "§e" + s + "§7, " + out;
                }
                out = out.trim();
                out = out.substring(0, out.length() - 1);
                p.sendMessage(Var.prefix + "§6Alle Locations: " + out);
            }
            else if (args.length == 1) {
                if (!manager.exists(args[0])) {
                    p.sendMessage(Var.prefix + "Diese Location existiert nicht");
                    return true;
                }
                p.teleport(manager.getWarpLocation(args[0]));
                p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0f, 1.0f);
                p.sendMessage(Var.prefix + "Du wurdest zur Location §e" + args[0] + " §7teleportiert");
            }
            else {
                p.sendMessage(Var.use + "/loc | /loc <Name>");
            }
        }
        return false;
    }
}
