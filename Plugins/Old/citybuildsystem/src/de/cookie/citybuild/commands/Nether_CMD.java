package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.LocationManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Nether_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final LocationManager manager = new LocationManager();
        if (cmd.getName().equalsIgnoreCase("nether")) {
            if (args.length == 0) {
                if (!manager.exists("Nether")) {
                    p.sendMessage(Var.prefix + "Der §eNether §7wurde noch §cnicht gesetzt§7! Setze ihn mit §e/setnether");
                }
                else {
                    p.teleport(manager.getLocation("Nether"));
                    p.sendMessage(Var.prefix + "Du wurdest in den §eNether §7teleportiert");
                    p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 3.0f, 3.0f);
                }
            }
            else {
                p.sendMessage(Var.use + "/nether");
            }
        }
        return false;
    }
}
