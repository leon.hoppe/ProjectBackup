// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import java.util.HashMap;

import de.cookie.citybuild.manager.SettingsManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import de.cookie.citybuild.manager.TeleportType;
import java.util.UUID;
import java.util.Map;
import org.bukkit.command.CommandExecutor;

public class Tpa_CMD implements CommandExecutor
{
    public static Map<UUID, UUID> tpRequest;
    public static Map<UUID, TeleportType> tpType;
    
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final SettingsManager settingsManager = new SettingsManager();
        if (cmd.getName().equalsIgnoreCase("tpa")) {
            if (args.length == 0) {
                p.sendMessage(Var.use + "/tpa <Spieler>");
                return true;
            }
            final Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                p.sendMessage(Var.prefix + "§7Der Spieler §e" + args[0] + " §7ist nicht online");
                return true;
            }
            if (!settingsManager.getTpa(target.getUniqueId())) {
                p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7nimmt keine Tpa-Anfragen an");
                return true;
            }
            Tpa_CMD.tpRequest.put(target.getUniqueId(), p.getUniqueId());
            Tpa_CMD.tpType.put(target.getUniqueId(), TeleportType.NORMAL);
            p.sendMessage(Var.prefix + "Die Anfrage wurde an §e" + target.getName() + " §7gesendet");
            target.sendMessage(Var.prefix + "§e" + p.getName() + " §7m\u00f6chte sich zu dir teleportieren");
            target.sendMessage(Var.prefix + "§7Annehmen mit §e/tpaccept");
        }
        return false;
    }
    
    static {
        Tpa_CMD.tpRequest = new HashMap<UUID, UUID>();
        Tpa_CMD.tpType = new HashMap<UUID, TeleportType>();
    }
}
