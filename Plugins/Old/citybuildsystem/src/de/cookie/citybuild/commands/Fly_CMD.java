package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Fly_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("fly") && p.hasPermission("cbs.fly")) {
            if (args.length == 0) {
                if (!Var.fly.contains(p.getName())) {
                    Var.fly.add(p.getName());
                    p.setAllowFlight(true);
                    p.sendMessage(Var.prefix + "Du kannst nun §afliegen");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 6.0f, 6.0f);
                }
                else if (Var.fly.contains(p.getName())) {
                    Var.fly.remove(p.getName());
                    p.setAllowFlight(false);
                    p.setFlying(false);
                    p.sendMessage(Var.prefix + "Du kannst nun §cnicht mehr fliegen");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 6.0f, 6.0f);
                }
            }
            else if (args.length == 1) {
                final Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                    return true;
                }
                if (!Var.fly.contains(target.getName())) {
                    Var.fly.add(target.getName());
                    target.setAllowFlight(true);
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7kann nun §afliegen");
                    target.sendMessage(Var.prefix + "Du kannst nun §afliegen");
                }
                else if (Var.fly.contains(target.getName())) {
                    Var.fly.remove(target.getName());
                    target.setAllowFlight(false);
                    target.setFlying(false);
                    target.sendMessage(Var.prefix + "Du kannst nun §cnicht mehr fliegen");
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7kann nun §cnicht mehr fliegen");
                }
            }
            else {
                p.sendMessage(Var.use + "/fly <Spieler>");
            }
        }
        return false;
    }
}
