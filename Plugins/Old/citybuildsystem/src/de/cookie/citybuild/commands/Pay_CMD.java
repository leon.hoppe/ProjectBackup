package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import de.cookie.citybuild.manager.ScoreBoardManager;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.CoinsAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Pay_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("pay")) {
            if (args.length == 2) {
                final Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                    return true;
                }
                if (target == p) {
                    p.sendMessage(Var.prefix + "Du kannst dir selbst keine Cookies \u00fcberweisen");
                    return true;
                }
                if (args[1].contains("-")) {
                    p.sendMessage(Var.prefix + "Du kannst keinem Spieler Cookies Weg Nehmen");
                    return true;
                }
                try {
                    final int i = Integer.parseInt(args[1]);
                    if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= i) {
                        CoinsAPI.addCoins(target.getUniqueId().toString(), i);
                        CoinsAPI.removeCoins(p.getUniqueId().toString(), i);
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + target.getName() + " " + i + " §7Cookies gegeben");
                        target.sendMessage(Var.prefix + "Der Spieler §e" + p.getName() + " §7hat dir §e" + i + " §7Cookies gegeben");
                        p.playSound(p.getLocation(), Sound.NOTE_PLING, 3.0f, 3.0f);
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 3.0f, 3.0f);
                        ScoreBoardManager.updateScoreboard(p);
                        ScoreBoardManager.updateScoreboard(target);
                    }
                    else {
                        p.sendMessage(Var.prefix + "Du hast nicht genug Cookies, um §e" + target.getName() + " §6" + i + " Cookies §7zu bezahlen");
                    }
                }
                catch (NumberFormatException e) {
                    p.sendMessage(Var.prefix + "§e" + args[1] + " §7muss eine ganze Zahl sein");
                }
            }
            else {
                p.sendMessage(Var.use + "/pay <Spieler> <Anzahl>");
            }
        }
        return false;
    }
}
