

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.ScoreBoardManager;
import de.cookie.citybuild.manager.CoinsAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class AddCoins_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("addcookies")) {
            if (p.hasPermission("cbs.addcookies")) {
                if (args.length == 2) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    try {
                        final int i = Integer.parseInt(args[1]);
                        if (i > 999999999) {
                            p.sendMessage(Var.prefix + "Die Zahl darf nicht \u00fcber §e999.999.999 §7sein");
                            return true;
                        }
                        if (i < 0) {
                            p.sendMessage(Var.prefix + "Du kannst dem Spieler §e" + target.getName() + " §7keine Minus-Cookies geben");
                            return true;
                        }
                        CoinsAPI.addCoins(target.getUniqueId().toString(), i);
                        if (target.getName().equals(p.getName())) {
                            p.sendMessage(Var.prefix + "Du hast dir §e" + i + " §7Cookies gegeben");
                            ScoreBoardManager.updateScoreboard(target);
                        }
                        else {
                            p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + target.getName() + " " + i + " §7Cookies gegeben");
                            target.sendMessage(Var.prefix + "Der Spieler §e" + p.getName() + " §7hat dir §e" + i + " §7Cookies geaddet");
                            ScoreBoardManager.updateScoreboard(target);
                        }
                    }
                    catch (NumberFormatException e) {
                        p.sendMessage(Var.prefix + "§e" + args[1] + " §7muss eine ganze Zahl sein!");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/addcookies <Spieler> <Anzahl>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
