// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.CoinsAPI;
import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Sound;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Repair_CMD implements CommandExecutor
{
    private final int repairCost = 1000;

    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("repair")) {
            if (p.hasPermission("cbs.repair")) {
                if (args.length == 0) {
                    final ItemStack itemStack = p.getItemInHand();
                    if (itemStack != null || itemStack.getType() != Material.AIR) {
                        itemStack.setDurability((short)0);
                        p.getInventory().setItemInHand(itemStack);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 3.0f, 3.0f);
                        p.sendMessage(Var.prefix + "Das Item wurde §aerfolgreich repariert");
                    }
                    else {
                        p.sendMessage(Var.prefix + "Du musst ein Item in der Hand halten");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/repair");
                }
            }else if (p.hasPermission("cbs.repair.buy")){
                if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= repairCost){
                    if (args.length == 0) {
                        final ItemStack itemStack = p.getItemInHand();
                        if (itemStack != null || itemStack.getType() != Material.AIR) {
                            itemStack.setDurability((short)0);
                            p.getInventory().setItemInHand(itemStack);
                            p.playSound(p.getLocation(), Sound.WOOD_CLICK, 3.0f, 3.0f);
                            CoinsAPI.removeCoins(p.getUniqueId().toString(), repairCost);
                            p.sendMessage(Var.prefix + "Das Item wurde §aerfolgreich repariert");
                        }
                        else {
                            p.sendMessage(Var.prefix + "Du musst ein Item in der Hand halten");
                        }
                    }
                }else {
                    p.sendMessage(Var.notEnoughCoins);
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
