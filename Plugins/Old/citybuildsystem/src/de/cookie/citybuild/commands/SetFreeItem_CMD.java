// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.Location;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.FreeItemManager;
import org.bukkit.inventory.ItemStack;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class SetFreeItem_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final FreeItemManager freeItemManager = Main.instance.getFreeItemManager();
        if (cmd.getName().equalsIgnoreCase("setfreeitem")) {
            if (p.hasPermission("cbs.freeitem")) {
                if (args.length == 2) {
                    try {
                        final int itemID = Integer.parseInt(args[0]);
                        final short subID = Short.parseShort(args[1]);
                        final Location location = p.getLocation();
                        final ItemStack itemStack = new ItemStack(itemID, 1, subID);
                        final ItemMeta itemMeta = itemStack.getItemMeta();
                        itemMeta.setDisplayName(Main.instance.getConfig().getString("FreeItems.Name"));
                        itemStack.setItemMeta(itemMeta);
                        FreeItemManager.bukkitTask.cancel();
                        freeItemManager.setFreeItem(location, itemStack);
                        p.sendMessage(Var.prefix + "Du hast das §eFreeItem §7erfolgreich gesetzt");
                        p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 8.0f, 8.0f);
                        Var.freeitem = true;
                        freeItemManager.startFreeItem((ItemStack)FreeItemManager.fb.getObject("FreeItem.Item"));
                    }
                    catch (NumberFormatException e) {
                        p.sendMessage(Var.prefix + "§7Du musst eine ganze Zahl angeben");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/setfreeitem <ID> <SubID>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
