// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Workbench_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("workbench")) {
            if (p.hasPermission("cbs.workbench")) {
                if (args.length == 0) {
                    final Location location = p.getLocation();
                    p.openWorkbench(location, true);
                    p.playSound(p.getLocation(), Sound.CHEST_OPEN, 2.0f, 2.0f);
                }
                else {
                    p.sendMessage(Var.use + "/wb");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
