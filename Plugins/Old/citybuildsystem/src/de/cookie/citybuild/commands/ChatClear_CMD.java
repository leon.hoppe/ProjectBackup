
package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class ChatClear_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("chatclear")) {
            if (p.hasPermission("cbs.chatclear")) {
                if (args.length == 0) {
                    for (int i = 0; i < 200; ++i) {
                        Bukkit.broadcastMessage(" ");
                    }
                    for (final Player all : Bukkit.getOnlinePlayers()) {
                        all.playSound(all.getLocation(), Sound.CHICKEN_EGG_POP, 5.0f, 5.0f);
                        all.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat den Chat §egeleert");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/chatclear");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
