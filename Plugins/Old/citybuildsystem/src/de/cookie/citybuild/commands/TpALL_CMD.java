// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class TpALL_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("tpall")) {
            if (p.hasPermission("cbs.tpall")) {
                if (args.length == 0) {
                    for (final Player all : Bukkit.getOnlinePlayers()) {
                        if (all != p) {
                            all.teleport(p.getLocation());
                            all.sendMessage(Var.prefix + "Du wurdest zu §e" + p.getName() + " §7teleportiert");
                            all.playSound(all.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
                        }
                    }
                    p.sendMessage(Var.prefix + "Du hast §ealle Spieler §7zu dir teleportiert");
                }
                else {
                    p.sendMessage(Var.use + "/tpall");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
