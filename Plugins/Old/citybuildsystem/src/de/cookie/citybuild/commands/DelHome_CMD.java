

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.HomeManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class DelHome_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final HomeManager manager = new HomeManager(p.getUniqueId());
        if (cmd.getName().equalsIgnoreCase("delhome")) {
            if (args.length == 1) {
                if (!manager.exists(args[0])) {
                    p.sendMessage(Var.prefix + "Dieses Home existiert nicht");
                    return true;
                }
                manager.delHome(args[0]);
                p.sendMessage(Var.prefix + "Das Home §e" + args[0] + " §7wurde erfolgreich gel\u00f6scht");
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0f, 3.0f);
            }
            else {
                p.sendMessage(Var.use + "/delhome <Name>");
            }
        }
        return false;
    }
}
