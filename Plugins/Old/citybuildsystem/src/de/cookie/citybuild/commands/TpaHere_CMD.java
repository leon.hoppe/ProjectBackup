// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.SettingsManager;
import de.cookie.citybuild.manager.Var;
import de.cookie.citybuild.manager.TeleportType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class TpaHere_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final SettingsManager settingsManager = new SettingsManager();
        if (cmd.getName().equalsIgnoreCase("tpahere")) {
            if (args.length == 0) {
                p.sendMessage(Var.use + "/tpahere <Spieler>");
                return true;
            }
            final Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                p.sendMessage(Var.prefix + "§7Der Spieler §e" + args[0] + " §7ist nicht online");
                return true;
            }
            if (!settingsManager.getTpa(target.getUniqueId())) {
                p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7nimmt keine Tpa-Anfragen an");
                return true;
            }
            Tpa_CMD.tpRequest.put(target.getUniqueId(), p.getUniqueId());
            Tpa_CMD.tpType.put(target.getUniqueId(), TeleportType.HERE);
            p.sendMessage(Var.prefix + "§7Die Anfrage wurde an §e" + target.getName() + " §7gesendet");
            target.sendMessage(Var.prefix + "§e" + p.getName() + " §7m\u00f6chte, dass du dich zu ihm teleportierst");
            target.sendMessage(Var.prefix + "§7Annehmen mit §e/tpaccept");
        }
        return false;
    }
}
