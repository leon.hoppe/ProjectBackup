

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.WarpManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class DelWarp_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final WarpManager manager = new WarpManager();
        if (cmd.getName().equalsIgnoreCase("delwarp")) {
            if (p.hasPermission("cbs.delloc")) {
                if (args.length == 1) {
                    if (!manager.exists(args[0])) {
                        p.sendMessage(Var.prefix + "Diese Location existiert nicht");
                        return true;
                    }
                    manager.delWarp(args[0]);
                    p.sendMessage(Var.prefix + "Die Location §e" + args[0] + " §7wurde erfolgreich gel\u00f6scht");
                    p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0f, 3.0f);
                }
                else {
                    p.sendMessage(Var.use + "/delloc <Name>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
