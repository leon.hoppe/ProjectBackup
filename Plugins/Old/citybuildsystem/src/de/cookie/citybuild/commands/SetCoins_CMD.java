// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.ScoreBoardManager;
import de.cookie.citybuild.manager.CoinsAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class SetCoins_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("setcookies")) {
            if (p.hasPermission("cbs.setcookies")) {
                if (args.length == 2) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    try {
                        final int i = Integer.parseInt(args[1]);
                        if (i > 999999999) {
                            p.sendMessage(Var.prefix + "Die Zahl darf nicht \u00fcber §e999.999.999 §7sein");
                            return true;
                        }
                        if (i < 0) {
                            p.sendMessage(Var.prefix + "Du kannst dem Spieler §e" + target.getName() + " §7keine Minus-Cooies geben");
                            return true;
                        }
                        CoinsAPI.setCoins(target.getUniqueId().toString(), i);
                        if (target.getName().equals(p.getName())) {
                            p.sendMessage(Var.prefix + "Du hast deine Cookies auf §e" + i + " §7gesetzt");
                            ScoreBoardManager.updateScoreboard(p);
                            p.playSound(p.getLocation(), Sound.NOTE_PIANO, 3.0f, 3.0f);
                        }
                        else {
                            p.sendMessage(Var.prefix + "Du hast die Cookies von §e" + target.getName() + " §7auf §e" + i + " §7gesetzt");
                            target.sendMessage(Var.prefix + "Deine Cookies wurden von §e" + p.getName() + " §7auf §e" + i + " §7gesetzt");
                            ScoreBoardManager.updateScoreboard(target);
                            p.playSound(p.getLocation(), Sound.NOTE_PIANO, 3.0f, 3.0f);
                            target.playSound(target.getLocation(), Sound.NOTE_PIANO, 3.0f, 3.0f);
                        }
                    }
                    catch (NumberFormatException e) {
                        p.sendMessage(Var.prefix + "§e" + args[1] + " §7muss eine ganze Zahl sein!");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/setcookies <Spieler> <Anzahl>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
