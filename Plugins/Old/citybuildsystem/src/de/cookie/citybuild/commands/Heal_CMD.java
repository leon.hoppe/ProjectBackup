// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Heal_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("heal")) {
            if (p.hasPermission("cbs.heal")) {
                if (args.length == 0) {
                    p.setHealth(p.getMaxHealth());
                    p.sendMessage(Var.prefix + "Du wurdest erfolgreich §egeheilt");
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 4.0f, 4.0f);
                }
                else if (args.length == 1) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    target.setHealth(target.getMaxHealth());
                    target.sendMessage(Var.prefix + "Dir wurden von §e" + p.getName() + " Kekse gegeben Jetzt geht es dir wieder Besser <3");
                    target.playSound(target.getLocation(), Sound.NOTE_PIANO, 4.0f, 4.0f);
                    p.sendMessage(Var.prefix + "Du hast §e" + target.getName() + " §7erfolgreich §egeheilt");
                }
                else {
                    p.sendMessage(Var.use + "/heal <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
