// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.SettingsManager;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Settings_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final SettingsManager manager = new SettingsManager();
        if (cmd.getName().equalsIgnoreCase("settings")) {
            if (args.length == 0) {
                manager.openSettings(p);
                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 2.0f, 2.0f);
            }
            else {
                p.sendMessage(Var.use + "/settings");
            }
        }
        return false;
    }
}
