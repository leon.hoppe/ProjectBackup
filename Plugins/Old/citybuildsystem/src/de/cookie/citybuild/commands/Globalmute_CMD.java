// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Globalmute_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("globalmute")) {
            if (p.hasPermission("cbs.globalmute")) {
                if (args.length == 0) {
                    if (!Var.globalmute) {
                        Var.globalmute = true;
                        Bukkit.broadcastMessage(Var.prefix + "Der §eGlobalmute §7wurde nun §aaktiviert");
                        for (final Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.NOTE_PIANO, 4.0f, 4.0f);
                        }
                    }
                    else {
                        Var.globalmute = false;
                        Bukkit.broadcastMessage(Var.prefix + "Der §eGlobalmute §7wurde nun §cdeaktiviert");
                        for (final Player all : Bukkit.getOnlinePlayers()) {
                            all.playSound(all.getLocation(), Sound.NOTE_PIANO, 4.0f, 4.0f);
                        }
                    }
                }
                else {
                    p.sendMessage(Var.use + "/globalmute");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
