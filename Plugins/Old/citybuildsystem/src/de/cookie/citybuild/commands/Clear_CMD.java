
package de.cookie.citybuild.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.Var;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Clear_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("clear")) {
            if (p.hasPermission("cbs.clear")) {
                if (args.length == 0) {
                    p.getInventory().clear();
                    p.sendMessage(Var.prefix + "Du hast dein Inventar §egeleert");
                    p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 6.0f, 6.0f);
                }
                else if (args.length == 1) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    target.getInventory().clear();
                    target.sendMessage(Var.prefix + "Dein Inventar wurde von §e" + p.getName() + " §7geleert");
                    p.sendMessage(Var.prefix + "Du hast das Inventar von §e" + target.getName() + " §7geleert");
                    target.playSound(target.getLocation(), Sound.ANVIL_BREAK, 6.0f, 6.0f);
                }
                else {
                    p.sendMessage(Var.use + "/clear <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
