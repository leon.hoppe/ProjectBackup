// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Kill_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("kill")) {
            if (p.hasPermission("cbs.kill")) {
                if (args.length == 0) {
                    p.setHealth(0.0);
                    p.sendMessage(Var.prefix + "Du hast dich erfolgreich §eget\u00f6tet");
                }
                else if (args.length == 1) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    if (args[0].equalsIgnoreCase("all") || args[0].equalsIgnoreCase("*")) {
                        final Player player2 = null;
                        Bukkit.getOnlinePlayers().forEach(player -> {
                            player.setHealth(0.0);
                            player.sendMessage(Var.prefix + "Du wurdest von §e" + player2.getName() + " §7get\u00f6tet");
                            return;
                        });
                        return true;
                    }
                    target.setHealth(0.0);
                    target.sendMessage(Var.prefix + "Du wurdest von §e" + p.getName() + " §7get\u00f6tet");
                    p.sendMessage(Var.prefix + "Du hast §e" + target.getName() + " §7erfolgreich get\u00f6tet");
                }
                else {
                    p.sendMessage(Var.use + "/kill <Spieler/all/*>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
