// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.plugin.Plugin;
import de.cookie.citybuild.main.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;
import de.cookie.citybuild.manager.TeleportType;
import org.bukkit.Bukkit;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class TpaAccept_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("tpaccept")) {
            if (!Tpa_CMD.tpRequest.containsKey(p.getUniqueId())) {
                p.sendMessage(Var.prefix + "§7Du hast keine Anfrage erhalten");
                return true;
            }
            final Player target = Bukkit.getPlayer((UUID)Tpa_CMD.tpRequest.get(p.getUniqueId()));
            if (target == null) {
                p.sendMessage(Var.prefix + "§7Der Spieler §b" + args[0] + " §7ist nicht mehr online");
                Tpa_CMD.tpRequest.remove(p.getUniqueId());
                Tpa_CMD.tpType.remove(p.getUniqueId());
                return true;
            }
            final TeleportType tpt = Tpa_CMD.tpType.get(p.getUniqueId());
            target.sendMessage(Var.prefix + "Der Spieler §e" + p.getName() + " §7hat deine Anfrage angenommen");
            target.sendMessage(Var.prefix + "Du wirst in §e3 Sekunden §7teleportiert");
            new BukkitRunnable() {
                public void run() {
                    if (tpt == TeleportType.NORMAL) {
                        target.teleport((Entity)p);
                        target.playSound(target.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0f, 1.0f);
                        p.sendMessage(Var.prefix + "§7Der Spieler §e" + target.getName() + " §7hat deine Anfrage angenommen");
                    }
                    else {
                        p.teleport((Entity)target);
                        p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0f, 1.0f);
                        p.sendMessage(Var.prefix + "§7Der Spieler §e" + target.getName() + " §7hat deine Anfrage angenommen");
                    }
                    Tpa_CMD.tpRequest.remove(p.getUniqueId());
                    Tpa_CMD.tpType.remove(p.getUniqueId());
                }
            }.runTaskLater((Plugin)Main.instance, 60L);
        }
        return false;
    }
}
