// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Night_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("night")) {
            if (p.hasPermission("cbs.night")) {
                if (args.length == 0) {
                    p.getWorld().setTime(15000L);
                    p.sendMessage(Var.prefix + "Du hast f\u00fcr alle Spieler §eNacht §7gemacht");
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 3.0f, 3.0f);
                }
                else {
                    p.sendMessage(Var.use + "/night");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
