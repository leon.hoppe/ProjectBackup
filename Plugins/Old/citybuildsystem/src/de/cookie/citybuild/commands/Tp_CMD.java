// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Tp_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("tp")) {
            if (p.hasPermission("cbs.tp")) {
                if (args.length == 1) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    p.teleport(target.getLocation());
                    p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
                    p.sendMessage(Var.prefix + "Du hast dich zu §e" + target.getName() + " §7teleportiert");
                }
                else if (args.length == 2) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    final Player target2 = Bukkit.getPlayer(args[1]);
                    if (target2 == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[1] + " §7ist nicht online");
                        return true;
                    }
                    target.teleport(target2.getLocation());
                    target.sendMessage(Var.prefix + "Du wurdest von §e" + p.getName() + " §7zu §e" + target2.getName() + " §7teleportiert");
                    target.playSound(target.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
                    p.sendMessage(Var.prefix + "Du hast §e" + target.getName() + " §7zu §e" + target2.getName() + " §7teleportiert");
                }
                else {
                    p.sendMessage(Var.use + "/tp <Spieler> | /tp <Spieler> <Spieler>");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
