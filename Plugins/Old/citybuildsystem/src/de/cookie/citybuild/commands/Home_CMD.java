// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import java.util.Iterator;

import de.cookie.citybuild.manager.HomeManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Home_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final HomeManager manager = new HomeManager(p.getUniqueId());
        if (cmd.getName().equalsIgnoreCase("home")) {
            if (args.length == 0) {
                if (manager.getHomes().size() == 0) {
                    p.sendMessage(Var.prefix + "Du hast noch keine Homes! Setze ein Home mit §e/sethome <Name>");
                    return true;
                }
                String out = "";
                for (final String s : manager.getHomes()) {
                    out = "§e" + s + "§7, " + out;
                }
                out = out.trim();
                out = out.substring(0, out.length() - 1);
                p.sendMessage(Var.prefix + "§6Deine Homes: " + out);
            }
            else if (args.length == 1) {
                if (!manager.exists(args[0])) {
                    p.sendMessage(Var.prefix + "Dieses Home existiert nicht");
                    return true;
                }
                p.teleport(manager.getLocation(args[0]));
                p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0f, 1.0f);
                p.sendMessage(Var.prefix + "Du wurdest zu deinem Home §e" + args[0] + " §7teleportiert");
            }
            else {
                p.sendMessage(Var.use + "/home <Name>");
            }
        }
        return false;
    }
}
