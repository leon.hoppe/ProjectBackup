// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.SettingsManager;
import de.cookie.citybuild.manager.Var;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class R_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final Player target = Var.messager.get(p);
        final Player reciever = Var.messager.get(target);
        String message = "";
        final SettingsManager settingsManager = new SettingsManager();
        if (cmd.getName().equalsIgnoreCase("r") && args.length >= 1 && Main.instance.getConfig().getBoolean("PrivateMessages")) {
            if (Var.messager.containsKey(p)) {
                if (target == null || reciever == null) {
                    p.sendMessage(Var.prefix + "Der Spieler ist nicht mehr online");
                    return true;
                }
                if (!settingsManager.getMsg(target.getUniqueId())) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7empf\u00e4ngt keine privaten Nachrichten");
                    return true;
                }
                for (int i = 0; i < args.length; ++i) {
                    message = message + " " + args[i];
                }
                reciever.sendMessage("§7[§6§lMSG§7] §7Du §8-> §e§l" + target.getName() + "§8: §e" + message);
                target.sendMessage("§7[§6§lMSG§7] §e§l" + reciever.getName() + " §8-> §7Dir§8: §e" + message);
            }
            else {
                p.sendMessage(Var.prefix + "Du hast keine Nachrichten erhalten");
            }
        }
        return false;
    }
}
