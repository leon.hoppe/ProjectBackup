// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import de.cookie.citybuild.manager.CoinsAPI;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Coins_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("cookies")) {
            if (args.length == 0) {
                p.sendMessage(Var.prefix + "Du hast §e" + CoinsAPI.getCoins(p.getUniqueId().toString()) + " §7Cookies");
                p.playSound(p.getLocation(), Sound.NOTE_PIANO, 6.0f, 6.0f);
            }
            else if (args.length == 1) {
                final Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                    return true;
                }
                if (!CoinsAPI.playerExists(target.getUniqueId().toString())) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7war noch nie auf dem Server");
                }
                if (CoinsAPI.getCoins(target.getUniqueId().toString()) == 1) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7hat §e" + CoinsAPI.getCoins(target.getUniqueId().toString()) + " §7Cookies");
                }
                else {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + target.getName() + " §7hat §e" + CoinsAPI.getCoins(target.getUniqueId().toString()) + " §7Cookies");
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 6.0f, 6.0f);
                }
            }
            else {
                p.sendMessage(Var.use + "/cookies <Spieler>");
            }
        }
        return false;
    }
}
