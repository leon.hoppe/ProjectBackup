

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Day_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("day")) {
            if (p.hasPermission("cbs.day")) {
                if (args.length == 0) {
                    p.getWorld().setTime(0L);
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 3.0f, 3.0f);
                    p.sendMessage(Var.prefix + "Du hast nun für alle Spieler §eTag gemacht");
                }
                else {
                    p.sendMessage(Var.use + "");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
