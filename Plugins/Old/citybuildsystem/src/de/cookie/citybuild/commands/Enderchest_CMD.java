// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.Inventory;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Enderchest_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("enderchest")) {
            if (args.length == 0) {
                final Inventory inventory = p.getEnderChest();
                p.openInventory(inventory);
                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 2.0f, 2.0f);
            }
            else if (args.length == 1) {
                final Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                    return true;
                }
                final Inventory inventory2 = target.getEnderChest();
                p.openInventory(inventory2);
            }
            else {
                p.sendMessage(Var.use + "/ec <Spieler>");
            }
        }
        return false;
    }
}
