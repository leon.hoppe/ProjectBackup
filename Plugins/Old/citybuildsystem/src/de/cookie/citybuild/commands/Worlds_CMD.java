package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.Var;
import org.bukkit.inventory.Inventory;
import de.omel.api.itemstack.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class Worlds_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("warp")) {
            if (args.length == 0) {
                this.openInventory(p);
            }
            else {
                p.sendMessage(Var.use + "/warp");
            }
        }
        return false;
    }
    
    private void openInventory(final Player player) {
        final Inventory inventory = Bukkit.createInventory((InventoryHolder)null, 27, "§e§lWarps");
        for (int i = 0; i < inventory.getSize(); ++i) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)15).setDisplayname(" ").build());
        }
      //  inventory.setItem(0, new ItemBuilder(Material.BARRIER).setDisplayname("§c§lIn Arbeit").build());
      //  inventory.setItem(8, new ItemBuilder(Material.BARRIER).setDisplayname("§c§lIn Arbeit").build());
        inventory.setItem(11, new ItemBuilder(Material.GRASS).setDisplayname("§e§lFarmwelt").build());
        inventory.setItem(13, new ItemBuilder(Material.NETHER_STAR).setDisplayname("§e§lSpawn").build());
        inventory.setItem(15, new ItemBuilder(Material.NETHER_BRICK).setDisplayname("§c§lNether").build());
      //  inventory.setItem(18, new ItemBuilder(Material.BARRIER).setDisplayname("§c§lIn Arbeit").build());
      //  inventory.setItem(26, new ItemBuilder(Material.BARRIER).setDisplayname("§c§lIn Arbeit").build());
        player.openInventory(inventory);
    }

}
