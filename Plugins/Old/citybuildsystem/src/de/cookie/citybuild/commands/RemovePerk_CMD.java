// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.commands;

import de.cookie.citybuild.manager.PerksManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class RemovePerk_CMD implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        final Player p = (Player)sender;
        final PerksManager perksManager = Main.instance.getPerksManager();
        if (cmd.getName().equalsIgnoreCase("removeperk")) {
            if (p.hasPermission("cbs.removeperk")) {
                if (args.length == 2) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7ist nicht online");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("fly")) {
                        if (!perksManager.hasPerk(target.getUniqueId(), "FLY")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7besitzt dieses Perk nicht");
                            return true;
                        }
                        perksManager.removePerk(target.getUniqueId(), "FLY");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eFly entfernt");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eFly §7entfernt");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                        target.setAllowFlight(false);
                        target.setFlying(false);
                    }
                    else if (args[1].equalsIgnoreCase("nofall")) {
                        if (!perksManager.hasPerk(target.getUniqueId(), "NOFALL")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7besitzt dieses Perk nicht");
                            return true;
                        }
                        perksManager.removePerk(target.getUniqueId(), "NOFALL");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eNofall entfernt");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eNofall §7entfernt");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                    }
                    else if (args[1].equalsIgnoreCase("nohunger")) {
                        if (!perksManager.hasPerk(target.getUniqueId(), "NOHUNGER")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7besitzt dieses Perk nicht");
                            return true;
                        }
                        perksManager.removePerk(target.getUniqueId(), "NOHUNGER");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eNoHunger entfernt");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eNoHunger §7entfernt");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                    }
                    else if (args[1].equalsIgnoreCase("fastdigging")) {
                        if (!perksManager.hasPerk(target.getUniqueId(), "FASTDIGGING")) {
                            p.sendMessage(Var.prefix + "Der Spieler §e" + args[0] + " §7besitzt dieses Perk nicht");
                            return true;
                        }
                        perksManager.removePerk(target.getUniqueId(), "FASTDIGGING");
                        p.sendMessage(Var.prefix + "Du hast dem Spieler §e" + args[0] + " §7das Perk §eFastdigging entfernt");
                        target.sendMessage(Var.prefix + "§e" + p.getName() + " §7hat dir das Perk §eFastdigging §7entfernt");
                        target.playSound(target.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        p.playSound(p.getLocation(), Sound.WOOD_CLICK, 4.0f, 4.0f);
                        perksManager.checkPerks(target);
                        target.removePotionEffect(PotionEffectType.FAST_DIGGING);
                    }
                    else {
                        p.sendMessage(Var.use + "/removeperk <Name> <Fly, Nofall, Nohunger, Fastdigging");
                    }
                }
                else {
                    p.sendMessage(Var.use + "/removeperk <Name> <Fly, Nofall, Nohunger, Fastdigging");
                }
            }
            else {
                p.sendMessage(Var.noperms);
            }
        }
        return false;
    }
}
