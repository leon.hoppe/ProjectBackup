// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.listener;

import de.cookie.citybuild.manager.Var;
import org.bukkit.event.EventHandler;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.Listener;

public class ChatListener implements Listener
{
    @EventHandler
    public void onChat(final AsyncPlayerChatEvent e) {
        if (Var.globalmute && !e.getPlayer().hasPermission("cbs.globalmute.bypass")) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(Var.prefix + "Der §eGlobale Chat §7ist §aaktiviert");
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.NOTE_PLING, 3.0f, 3.0f);
        }
        e.setMessage(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
    }
}
