// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.listener;

import de.cookie.citybuild.manager.PerksManager;
import de.cookie.citybuild.manager.SettingsManager;
import de.cookie.citybuild.manager.Var;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.EventHandler;
import java.util.Iterator;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import de.cookie.citybuild.manager.ScoreBoardManager;
import de.cookie.citybuild.manager.CoinsAPI;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.event.player.PlayerJoinEvent;
import de.cookie.citybuild.main.Main;
import de.cookie.citybuild.manager.LocationManager;
import org.bukkit.event.Listener;

public class JQListener implements Listener
{
    private static int startcoins;
    private LocationManager manager;
    private PerksManager perksManager;
    private SettingsManager settingsManager;
    
    public JQListener() {
        this.manager = Main.instance.getLocationManager();
        this.perksManager = Main.instance.getPerksManager();
        this.settingsManager = Main.instance.getSettingsManager();
    }
    
    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final Player p = event.getPlayer();
        event.setJoinMessage("§7» §e" + event.getPlayer().getName() + " §7hat das Spiel §abetreten");
        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 4.0f, 4.0f);
        event.getPlayer().setGameMode(GameMode.SURVIVAL);
        event.getPlayer().setFlySpeed(0.1f);
        if (!this.manager.exists("Spawn")) {
            if (p.hasPermission("cbs.admin")) {
                p.sendMessage(Var.prefix + "§cDer Spawn wurde noch nicht gesetzt! Setze ihn mit §e/setspawn");
            }
        }
        else {
            p.teleport(this.manager.getLocation("Spawn"));
        }
        if (!this.settingsManager.exists(p.getUniqueId())) {
            this.settingsManager.createPlayer(p.getUniqueId());
        }
        if (!CoinsAPI.playerExists(p.getUniqueId().toString())) {
            CoinsAPI.createPlayer(p.getUniqueId().toString());
            CoinsAPI.setCoins(p.getUniqueId().toString(), JQListener.startcoins);
        }
        ScoreBoardManager.sendScoreboard(p);
        for (final Player all : Bukkit.getOnlinePlayers()) {
            ScoreBoardManager.updateScoreboard(all);
        }
        for (final Player all : Bukkit.getOnlinePlayers()) {
            if (Var.vanish.contains(all.getName())) {
                p.hidePlayer(all);
            }
        }
        this.perksManager.createPlayer(p.getUniqueId());
        this.perksManager.checkPerks(p);
        this.checkBoosts(p);
        PerksManager pm = new PerksManager();
        pm.checkPerks(p);
    }
    
    @EventHandler
    public void onQuit(final PlayerQuitEvent event) {
        event.setQuitMessage("§7» §e" + event.getPlayer().getName() + " §7hat das Spiel §cverlassen");
        Var.messager.remove(event.getPlayer());
        new BukkitRunnable() {
            public void run() {
                for (final Player all : Bukkit.getOnlinePlayers()) {
                    ScoreBoardManager.updateScoreboard(all);
                }
            }
        }.runTaskLater((Plugin)Main.instance, 1L);
    }
    
    private void checkBoosts(final Player p) {
        if (Var.flyboost) {
            p.setAllowFlight(true);
            p.sendMessage(Var.prefix + "Es l\u00e4uft ein §bFly-Booster§7. Du kannst nun §efliegen§7.");
        }
        if (Var.breakboost) {
            p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 12000, 2, false, false));
            p.sendMessage(Var.prefix + "Es l\u00e4uft ein §bBreak-Booster§7. Du kannst nun §eschneller abbauen§7.");
        }
        if (Var.nohunger) {
            p.sendMessage(Var.prefix + "Es l\u00e4uft ein §bNoHunger-Booster§7. Du verlierst nun §ekeinen Hunger");
        }
    }
    
    static {
        JQListener.startcoins = Main.instance.getConfig().getInt("StandardCoins");
    }
}
