// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.listener;

import de.cookie.citybuild.manager.Var;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.Listener;

public class NofallListener implements Listener
{
    @EventHandler
    public void onFall(final EntityDamageEvent e) {
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && Var.nofall.contains(e.getEntity().getName())) {
            e.setCancelled(true);
        }
    }
}
