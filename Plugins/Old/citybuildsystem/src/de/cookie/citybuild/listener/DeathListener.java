// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.listener;

import de.cookie.citybuild.manager.PerksManager;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import de.cookie.citybuild.manager.LocationManager;
import org.bukkit.event.Listener;

public class DeathListener implements Listener
{
    private LocationManager manager;
    
    public DeathListener() {
        this.manager = new LocationManager();
    }
    
    @EventHandler
    public void onDeath(final PlayerDeathEvent e) {
        final Player p = e.getEntity();
        e.setDeathMessage((String)null);
        p.spigot().respawn();
        PerksManager pm = new PerksManager();
        pm.checkPerks(p);
    }
    
    @EventHandler
    public void onRespawn(final PlayerRespawnEvent e) {
        e.setRespawnLocation(this.manager.getLocation("Spawn"));
    }
}
