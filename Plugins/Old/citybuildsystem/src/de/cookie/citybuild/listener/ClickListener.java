// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.listener;

import de.cookie.citybuild.manager.*;
import org.bukkit.event.EventHandler;
import java.util.Iterator;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import de.omel.api.itemstack.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import de.cookie.citybuild.main.Main;
import org.bukkit.event.Listener;

public class ClickListener implements Listener
{
    LocationManager locationManager;
    
    public ClickListener() {
        this.locationManager = Main.instance.getLocationManager();
    }
    
    @EventHandler
    public void onClick(final InventoryClickEvent e) {
        final PerksManager perksManager = Main.instance.getPerksManager();
        final Player p = (Player)e.getWhoClicked();
        final SettingsManager settingsManager = new SettingsManager();
        if (e.getCurrentItem() == null) {
            return;
        }
        if (e.getCurrentItem().getItemMeta() == null) {
            return;
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName() == null) {
            return;
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(" ")) {
            e.setCancelled(true);
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aAN") || e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cAUS")) {
            e.setCancelled(true);
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aFly-Boost")) {
            e.setCancelled(true);
            if (!Var.flyboost) {
                Var.flyboost = true;
                for (final Player all : Bukkit.getOnlinePlayers()) {
                    all.setAllowFlight(true);
                    all.playSound(all.getLocation(), Sound.LEVEL_UP, 5.0f, 5.0f);
                }
                Bukkit.broadcastMessage(Var.prefix + "§e" + p.getName() + " §7hat den §bFly-Boost §aaktiviert§7. Du kannst nun f\u00fcr §e" + Main.instance.getConfig().getInt("Booster.Fly.Zeit") + " Sekunden fliegen");
                p.getOpenInventory().setItem(19, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
                p.updateInventory();
                BoosterTimer.startFlyBoost();
            }
            else {
                p.closeInventory();
                p.sendMessage(Var.prefix + "§cEs l\u00e4uft bereits ein §eFly-Booster");
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aBreak-Boost")) {
            e.setCancelled(true);
            if (!Var.breakboost) {
                Var.breakboost = true;
                for (final Player all : Bukkit.getOnlinePlayers()) {
                    all.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 12000, 2, false, false));
                    all.playSound(all.getLocation(), Sound.LEVEL_UP, 5.0f, 5.0f);
                }
                Bukkit.broadcastMessage(Var.prefix + "§e" + p.getName() + " §7hat den §bBreak-Booster §aaktiviert§7. Du kannst nun f\u00fcr §e" + Main.instance.getConfig().getInt("Booster.SchnellerAbbauen.Zeit") + " Sekunden schneller abbauen");
                p.getOpenInventory().setItem(21, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
                p.updateInventory();
                BoosterTimer.startBreakBoost();
            }
            else {
                p.closeInventory();
                p.sendMessage(Var.prefix + "§cEs l\u00e4uft bereits ein §eBreak-Booster");
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aNoHunger-Boost")) {
            e.setCancelled(true);
            if (!Var.nohunger) {
                Var.nohunger = true;
                for (final Player all : Bukkit.getOnlinePlayers()) {
                    all.playSound(all.getLocation(), Sound.LEVEL_UP, 5.0f, 5.0f);
                }
                Bukkit.broadcastMessage(Var.prefix + "§e" + p.getName() + " §7hat den §bNoHunger-Booster §aaktiviert§7. Du hast nun f\u00fcr §e" + Main.instance.getConfig().getInt("Booster.Nohunger.Zeit") + " Sekunden keinen Hunger mehr");
                p.getOpenInventory().setItem(23, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
                p.updateInventory();
                BoosterTimer.startNoHungerBoost();
            }
            else {
                p.closeInventory();
                p.sendMessage(Var.prefix + "§cEs l\u00e4uft bereits ein §eNoHunger-Booster");
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cAlle Booster stoppen")) {
            e.setCancelled(true);
            p.getOpenInventory().setItem(19, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
            p.getOpenInventory().setItem(21, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
            p.getOpenInventory().setItem(23, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
            p.updateInventory();
            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7werden in §e10 Sekunden §cdeaktiviert");
            new BukkitRunnable() {
                public void run() {
                    if (Var.cancelallbooster > 0) {
                        --Var.cancelallbooster;
                        if (Var.cancelallbooster == 10) {
                            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7enden in §e10 Sekunden");
                        }
                        if (Var.cancelallbooster == 5) {
                            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7enden in §e5 Sekunden");
                        }
                        if (Var.cancelallbooster == 4) {
                            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7enden in §e4 Sekunden");
                        }
                        if (Var.cancelallbooster == 3) {
                            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7enden in §e3 Sekunden");
                        }
                        if (Var.cancelallbooster == 2) {
                            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7enden in §e2 Sekunden");
                        }
                        if (Var.cancelallbooster == 1) {
                            Bukkit.broadcastMessage(Var.prefix + "§eAlle Booster §7enden in §e1 Sekunden");
                        }
                    }
                    else {
                        Var.cancelallbooster = 10;
                        Var.flyboost = false;
                        Var.nohunger = false;
                        Var.breakboost = false;
                        Var.flyboostcount = 0;
                        Var.breakboostcount = 0;
                        Var.nohungerboostcount = 0;
                        this.cancel();
                    }
                }
            }.runTaskTimer((Plugin)Main.instance, 0L, 20L);
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lMSG")) {
            e.setCancelled(true);
            if (settingsManager.getMsg(p.getUniqueId())) {
                settingsManager.setMsg(p.getUniqueId(), false);
                p.getOpenInventory().setItem(20, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
                p.updateInventory();
                p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
            }
            else {
                settingsManager.setMsg(p.getUniqueId(), true);
                p.getOpenInventory().setItem(20, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
                p.updateInventory();
                p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lTpaAnfragen")) {
            e.setCancelled(true);
            if (settingsManager.getTpa(p.getUniqueId())) {
                settingsManager.setTpa(p.getUniqueId(), false);
                p.getOpenInventory().setItem(24, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
                p.updateInventory();
                p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
            }
            else {
                settingsManager.setTpa(p.getUniqueId(), true);
                p.getOpenInventory().setItem(24, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
                p.updateInventory();
                p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 3.0f, 3.0f);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lFarmwelt")) {
            e.setCancelled(true);
            Server.connect("FarmServer-1", p);
                p.sendMessage(Var.prefix + "Du wurdest zur §eFarmwelt §7Gesendet");
                p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);

        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lSpawn")) {
            e.setCancelled(true);
            if (this.locationManager.exists("Spawn")) {
                p.teleport(this.locationManager.getLocation("Spawn"));
                p.sendMessage(Var.prefix + "Du wurdest zum §eSpawn §7teleportiert");
                p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
            }
            else {
                p.sendMessage(Var.prefix + "§cDer Spawn wurde noch nicht gesetzt! Setze sie mit §e/setspawn");
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lNether")) {
            e.setCancelled(true);
            if (this.locationManager.exists("Nether")) {
                p.teleport(this.locationManager.getLocation("Nether"));
                p.sendMessage(Var.prefix + "Du wurdest zum §cNether §7teleportiert");
                p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2.0f, 2.0f);
            }
            else {
                p.sendMessage(Var.prefix + "§cDer Nether wurde noch nicht gesetzt! Setze sie mit §e/setnether");
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lFLY-PERK")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "FLY")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.Fly.Preis")) {
                CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.Fly.Preis"));
                p.sendMessage(Var.prefix + "Du hast das §e§lFLY-PERK §7erfolgreich gekauft!");
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                perksManager.addPerk(p.getUniqueId(), "FLY");
                perksManager.openPerksInventory(p);
                ScoreBoardManager.updateScoreboard(p);
            }
            else {
                p.sendMessage(Var.notEnoughCoins);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Fly-Perk kaufen")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "FLY")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.Fly.Preis")) {
                CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.Fly.Preis"));
                p.sendMessage(Var.prefix + "Du hast das §e§lFLY-PERK §7erfolgreich gekauft!");
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                perksManager.addPerk(p.getUniqueId(), "FLY");
                perksManager.openPerksInventory(p);
                ScoreBoardManager.updateScoreboard(p);
            }
            else {
                p.sendMessage(Var.notEnoughCoins);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lNOFALL-PERK")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "NOFALL")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (!perksManager.hasPerk(p.getUniqueId(), "NOFALL")) {
                if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.Nofall.Preis")) {
                    CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.Nofall.Preis"));
                    p.sendMessage(Var.prefix + "Du hast das §e§lNOFALL-PERK §7erfolgreich gekauft!");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                    perksManager.addPerk(p.getUniqueId(), "NOFALL");
                    perksManager.openPerksInventory(p);
                    ScoreBoardManager.updateScoreboard(p);
                }
                else {
                    p.sendMessage(Var.notEnoughCoins);
                }
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Nofall-Perk kaufen")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "NOFALL")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (!perksManager.hasPerk(p.getUniqueId(), "NOFALL")) {
                if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.Nofall.Preis")) {
                    CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.Nofall.Preis"));
                    p.sendMessage(Var.prefix + "Du hast das §e§lNOFALL-PERK §7erfolgreich gekauft!");
                    p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                    perksManager.addPerk(p.getUniqueId(), "NOFALL");
                    perksManager.openPerksInventory(p);
                    ScoreBoardManager.updateScoreboard(p);
                }
                else {
                    p.sendMessage(Var.notEnoughCoins);
                }
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lNOHUNGER-PERK")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "NOHUNGER")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.Nohunger.Preis")) {
                CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.Nohunger.Preis"));
                p.sendMessage(Var.prefix + "Du hast das §e§lNOHUNGER-PERK §7erfolgreich gekauft!");
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                perksManager.addPerk(p.getUniqueId(), "NOHUNGER");
                perksManager.openPerksInventory(p);
                ScoreBoardManager.updateScoreboard(p);
            }
            else {
                p.sendMessage(Var.notEnoughCoins);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6NoHunger-Perk kaufen")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "NOHUNGER")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.Nohunger.Preis")) {
                CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.Nohunger.Preis"));
                p.sendMessage(Var.prefix + "Du hast das §e§lNOHUNGER-PERK §7erfolgreich gekauft!");
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                perksManager.addPerk(p.getUniqueId(), "NOHUNGER");
                perksManager.openPerksInventory(p);
                ScoreBoardManager.updateScoreboard(p);
            }
            else {
                p.sendMessage(Var.notEnoughCoins);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lSCHNELLER ABBAUEN-PERK")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "FASTDIGGING")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.SchnellerAbbauen.Preis")) {
                CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.SchnellerAbbauen.Preis"));
                p.sendMessage(Var.prefix + "Du hast das §e§lSCHNELLER ABBAUEN-PERK §7erfolgreich gekauft!");
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                perksManager.addPerk(p.getUniqueId(), "FASTDIGGING");
                perksManager.openPerksInventory(p);
                ScoreBoardManager.updateScoreboard(p);
                p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 999999, 10));
            }
            else {
                p.sendMessage(Var.notEnoughCoins);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Schneller Abbauen-Perk kaufen")) {
            e.setCancelled(true);
            if (perksManager.hasPerk(p.getUniqueId(), "FASTDIGGING")) {
                p.sendMessage(Var.prefix + "Du hast dieses Perk bereits gekauft");
                return;
            }
            if (CoinsAPI.getCoins(p.getUniqueId().toString()) >= Main.instance.getConfig().getInt("Perks.SchnellerAbbauen.Preis")) {
                CoinsAPI.removeCoins(p.getUniqueId().toString(), Main.instance.getConfig().getInt("Perks.SchnellerAbbauen.Preis"));
                p.sendMessage(Var.prefix + "Du hast das §e§lSCHNELLER ABBAUEN-PERK §7erfolgreich gekauft!");
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 4.0f, 4.0f);
                perksManager.addPerk(p.getUniqueId(), "FASTDIGGING");
                perksManager.openPerksInventory(p);
                ScoreBoardManager.updateScoreboard(p);
                p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 999999, 10));
            }
            else {
                p.sendMessage(Var.notEnoughCoins);
            }
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lGEKAUFT")) {
            e.setCancelled(true);
        }
    }
}
