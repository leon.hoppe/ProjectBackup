// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.listener;

import de.cookie.citybuild.manager.Var;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.Listener;

public class NoHungerListener implements Listener
{
    @EventHandler
    public void onHunger(final FoodLevelChangeEvent e) {
        if (Var.nohunger) {
            e.setCancelled(true);
        }
        if (Var.nohunger_players.contains(e.getEntity().getName())) {
            e.setCancelled(true);
        }
    }
}
