// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import de.cookie.citybuild.main.Main;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import java.util.HashMap;
import java.util.ArrayList;

public class Var
{
    public static String prefix;
    public static String noperms;
    public static String use;
    public static String notEnoughCoins;
    public static ArrayList<String> fly;
    public static ArrayList<String> vanish;
    public static ArrayList<String> nofall;
    public static ArrayList<String> nohunger_players;
    public static ArrayList<String> fastdigging;
    public static HashMap<Player, Player> messager;
    public static HashMap<String, Location> dropevent;
    public static boolean flyboost;
    public static boolean breakboost;
    public static boolean nohunger;
    public static boolean freeitem;
    public static boolean event_drop;
    public static boolean globalmute;
    public static int flyboostcount;
    public static int breakboostcount;
    public static int nohungerboostcount;
    public static int cancelallbooster;
    
    static {
        Var.prefix = Main.instance.getConfig().getString("Prefix");
        Var.noperms = Var.prefix + Main.instance.getConfig().getString("NoPermissions");
        Var.use = Var.prefix + Main.instance.getConfig().getString("Use");
        Var.notEnoughCoins = Var.prefix + Main.instance.getConfig().getString("NotEnoughCoins");
        Var.fly = new ArrayList<String>();
        Var.vanish = new ArrayList<String>();
        Var.nofall = new ArrayList<String>();
        Var.nohunger_players = new ArrayList<String>();
        Var.fastdigging = new ArrayList<String>();
        Var.messager = new HashMap<Player, Player>();
        Var.dropevent = new HashMap<String, Location>();
        Var.flyboost = false;
        Var.breakboost = false;
        Var.nohunger = false;
        Var.freeitem = false;
        Var.event_drop = false;
        Var.globalmute = false;
        Var.flyboostcount = Main.instance.getConfig().getInt("Booster.Fly.Zeit");
        Var.breakboostcount = Main.instance.getConfig().getInt("Booster.SchnellerAbbauen.Zeit");
        Var.nohungerboostcount = Main.instance.getConfig().getInt("Booster.Nohunger.Zeit");
        Var.cancelallbooster = 10;
    }
}
