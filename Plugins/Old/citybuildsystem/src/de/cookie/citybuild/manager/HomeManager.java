package de.cookie.citybuild.manager;

import org.bukkit.Bukkit;
import java.util.Set;
import org.bukkit.Location;
import java.util.UUID;
import de.omel.api.file.FileBuilder;

public class HomeManager
{
    private FileBuilder fb;
    
    public HomeManager(final UUID uuid) {
        this.fb = new FileBuilder("plugins/CityBuildSystem/homes", uuid.toString() + ".yml");
    }
    
    public void addHome(final Location loc, final String name) {
        this.fb.setValue(name + ".world", loc.getWorld().getName());
        this.fb.setValue(name + ".x", loc.getX());
        this.fb.setValue(name + ".y", loc.getY());
        this.fb.setValue(name + ".z", loc.getZ());
        this.fb.setValue(name + ".yaw", loc.getYaw());
        this.fb.setValue(name + ".pitch", loc.getPitch());
        this.fb.save();
    }
    
    public boolean exists(final String name) {
        return this.fb.getString(name) != null;
    }
    
    public Set<String> getHomes() {
        return this.fb.getKeys(false);
    }
    
    public Location getLocation(final String name) {
        return new Location(Bukkit.getWorld(this.fb.getString(name + ".world")), this.fb.getDouble(name + ".x"), this.fb.getDouble(name + ".y"), this.fb.getDouble(name + ".z"), (float)this.fb.getDouble(name + ".yaw"), (float)this.fb.getDouble(name + ".pitch"));
    }
    
    public void delHome(final String name) {
        this.fb.setValue(name, null);
        this.fb.save();
    }
}
