// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import de.omel.api.file.FileBuilder;

public class LocationManager
{
    private FileBuilder fb;
    
    public LocationManager() {
        this.fb = new FileBuilder("plugins/CityBuildSystem", "locations.yml");
    }
    
    public void setLocation(final String name, final Location location) {
        this.fb.setValue(name + ".world", location.getWorld().getName());
        this.fb.setValue(name + ".x", location.getX());
        this.fb.setValue(name + ".y", location.getY());
        this.fb.setValue(name + ".z", location.getZ());
        this.fb.setValue(name + ".yaw", location.getYaw());
        this.fb.setValue(name + ".pitch", location.getPitch());
        this.fb.save();
    }
    
    public Location getLocation(final String name) {
        return new Location(Bukkit.getWorld(this.fb.getString(name + ".world")), this.fb.getDouble(name + ".x"), this.fb.getDouble(name + ".y"), this.fb.getDouble(name + ".z"), (float)this.fb.getDouble(name + ".yaw"), (float)this.fb.getDouble(name + ".pitch"));
    }
    
    public boolean exists(final String name) {
        return this.fb.getString(name) != null;
    }
}
