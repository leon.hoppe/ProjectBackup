// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;

public class MySQL
{
    private String HOST;
    private String DATABASE;
    private String USER;
    private String PASSWORD;
    private Connection con;
    
    public MySQL(final String host, final String database, final String user, final String password) {
        this.HOST = "";
        this.DATABASE = "";
        this.USER = "";
        this.PASSWORD = "";
        this.HOST = host;
        this.DATABASE = database;
        this.USER = user;
        this.PASSWORD = password;
        this.connect();
    }
    
    public void connect() {
        try {
            this.con = DriverManager.getConnection("jdbc:mysql://" + this.HOST + ":3306/" + this.DATABASE + "?autoReconnect=true", this.USER, this.PASSWORD);
            System.out.println("[MySQL] Die Verbindung zur MySQL wurde hergestellt! - CityBuildSystem");
        }
        catch (SQLException e) {
            System.out.println("[MySQL] Die Verbindung zur MySQL ist fehlgeschlagen! Fehler: " + e.getMessage());
        }
    }
    
    public void close() {
        try {
            if (this.con != null) {
                this.con.close();
                System.out.println("[MySQL] Die Verbindung zur MySQL wurde Erfolgreich beendet! - MySQLSystem");
            }
        }
        catch (SQLException e) {
            System.out.println("[MySQL] Fehler beim beenden der Verbindung zur MySQL! Fehler: " + e.getMessage());
        }
    }
    
    public void update(final String qry) {
        try {
            this.con.createStatement().executeUpdate(qry);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ResultSet getResults(final String qry) {
        try {
            return this.con.createStatement().executeQuery(qry);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
