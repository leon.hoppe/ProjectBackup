package de.cookie.citybuild.manager;

import org.bukkit.inventory.Inventory;
import org.bukkit.Sound;
import java.util.Arrays;
import de.omel.api.itemstack.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BoosterManager
{
    public static void openBoosterGUI(final Player player) {
        final Inventory inventory = Bukkit.createInventory((InventoryHolder)null, 36, "§6§lBooster");
        for (int i = 0; i < inventory.getSize(); ++i) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)15).setDisplayname(" ").build());
        }
        if (player.hasPermission("cbs.admin")) {
            inventory.setItem(35, new ItemBuilder(Material.REDSTONE_BLOCK).setDisplayname("§cAlle Booster stoppen").build());
        }
        inventory.setItem(10, new ItemBuilder(Material.DIAMOND_BOOTS).setDisplayname("§aFly-Boost").setLore(Arrays.asList("§8§m------------------", "§7Aktiviere f\u00fcr §aalle Spieler auf", "§adem Server §7den §b§lFlugmodus", "§8§m------------------")).build());
        if (!Var.flyboost) {
            inventory.setItem(19, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
        }
        else {
            inventory.setItem(19, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
        }
        inventory.setItem(12, new ItemBuilder(Material.IRON_PICKAXE).setDisplayname("§aBreak-Boost").setLore(Arrays.asList("§8§m------------------", "§7Lasse §aalle Spieler auf", "§adem Server §b§lschneller Abbauen", "§8§m------------------")).build());
        if (!Var.breakboost) {
            inventory.setItem(21, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
        }
        else {
            inventory.setItem(21, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
        }
        inventory.setItem(14, new ItemBuilder(Material.COOKED_BEEF).setDisplayname("§aNoHunger-Boost").setLore(Arrays.asList("§8§m------------------", "§7Aktiviere f\u00fcr §aalle Spieler", "§aauf dem Server §bkeinen Hunger mehr haben", "§8§m-------------------")).build());
        if (!Var.nohunger) {
            inventory.setItem(23, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
        }
        else {
            inventory.setItem(23, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
        }
        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1.0f, 1.0f);
        player.openInventory(inventory);
    }
}
