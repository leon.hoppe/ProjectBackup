package de.cookie.citybuild.manager;


import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class Server {

    public static void connect(String server, Player p){
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(b);

            out.writeUTF("Connect");
            out.writeUTF(server);

            p.sendMessage("§aVerbinde zu " + server + "...");

            p.sendPluginMessage(Main.getInstance(), "BungeeCord", b.toByteArray());
        } catch (Exception e){
            p.sendMessage(Var.prefix + "§cEin Fehler ist aufgetreten!");
        }

    }

}
