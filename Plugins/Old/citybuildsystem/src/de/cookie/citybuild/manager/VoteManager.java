package de.cookie.citybuild.manager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.Sound;
import de.omel.api.itemstack.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class VoteManager implements Listener {

    public static void openMainVoteGUI(Player p) {
        final Inventory inventory = Bukkit.createInventory((InventoryHolder) null, 27, "§e§lVote");
        for (int i = 0; i < inventory.getSize(); ++i) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build());
        }
        inventory.setItem(10, new ItemBuilder(Material.NETHER_STAR).setDisplayname("§bWetter").build());
        inventory.setItem(12, new ItemBuilder(Material.NETHER_STAR).setDisplayname("§aBelohnungs-Vote").build());
        inventory.setItem(14, new ItemBuilder(Material.BARRIER).setDisplayname("§cOffen").build());
        inventory.setItem(16, new ItemBuilder(Material.BARRIER).setDisplayname("§cOffen").build());
        p.openInventory(inventory);
        p.playSound(p.getLocation(), Sound.CHEST_OPEN, 2.0f, 2.0f);
    }


    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();
        if (event.getClickedInventory().getTitle().equalsIgnoreCase("§e§lVote")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aBelohnungs-Vote")) {
                p.closeInventory();
                p.sendMessage("§b----------------------------------------------------");
                p.sendMessage("");
                p.sendMessage("§aHier Hast du die Möglichkeit zum Voten");
                p.sendMessage("");
                p.sendMessage("§aMinecraft-Server.eu:");
                p.sendMessage("");
                p.sendMessage("§aLink: https://minecraft-server.eu/vote/index/20EC9");
                p.sendMessage("");
                p.sendMessage("§b----------------------------------------------------");
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cOffen")) {
                p.closeInventory();
                p.sendMessage("§b----------------------------------------------------");
                p.sendMessage("");
                p.sendMessage("");
                p.sendMessage("");
                p.sendMessage("§aHier Werden Noch weitere Möglichkeiten Folgen.");
                p.sendMessage("");
                p.sendMessage("");
                p.sendMessage("");
                p.sendMessage("§b----------------------------------------------------");
            }
        }
    }
}