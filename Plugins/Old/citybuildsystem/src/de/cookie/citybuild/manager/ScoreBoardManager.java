// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import de.cookie.citybuild.main.Main;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ScoreBoardManager
{
    public static void sendScoreboard(final Player p) {
        Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective obj = sb.registerNewObjective("score", "board");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(Main.instance.getConfig().getString("ScoreBoard.Title"));
        obj.getScore("§7§m------------").setScore(16);
        Team t15 = sb.registerNewTeam("t15");
        t15.addEntry("§7");
        t15.setPrefix(" ");
        obj.getScore("§7").setScore(15);
        Team t16 = sb.registerNewTeam("t14");
        t16.addEntry("§b");
        t16.setPrefix(Main.instance.getConfig().getString("ScoreBoard.Line.1"));
        obj.getScore("§b").setScore(14);
        Team t17 = sb.registerNewTeam("t13");
        t17.addEntry("§c");
        t17.setPrefix("§8» §6§l" + Bukkit.getServer().getOnlinePlayers().size());
        obj.getScore("§c").setScore(13);
        Team t18 = sb.registerNewTeam("t12");
        t18.addEntry("§d");
        t18.setPrefix("  ");
        obj.getScore("§d").setScore(12);
        Team t19 = sb.registerNewTeam("t11");
        t19.addEntry("§e");
        t19.setPrefix(Main.instance.getConfig().getString("ScoreBoard.Line.2"));
        obj.getScore("§e").setScore(11);
        Team t20 = sb.registerNewTeam("t10");
        t20.addEntry("§f");
        t20.setPrefix("§8» ");
        t20.setSuffix("§6§l" + CoinsAPI.getCoins(p.getUniqueId().toString()).toString());
        obj.getScore("§f").setScore(10);
        Team t21 = sb.registerNewTeam("t9");
        t21.addEntry("§1");
        t21.setPrefix("   ");
        obj.getScore("§1").setScore(9);
        Team t22 = sb.registerNewTeam("t8");
        t22.addEntry("§2");
        t22.setPrefix(Main.instance.getConfig().getString("ScoreBoard.Line.3"));
        obj.getScore("§2").setScore(8);
        Team t23 = sb.registerNewTeam("t7");
        t23.addEntry("§3");
        t23.setPrefix("§8» ");
        t23.setSuffix(Main.instance.getConfig().getString("ScoreBoard.Line.4"));
        obj.getScore("§3").setScore(7);
        Team t24 = sb.registerNewTeam("t6");
        t24.addEntry("§4");
        t24.setPrefix("       ");
        obj.getScore("§4").setScore(6);
        Team t25 = sb.registerNewTeam("t5");
        t25.addEntry("§5");
        t25.setPrefix("§7§m------------");
        obj.getScore("§5").setScore(5);
        p.setScoreboard(sb);
    }
    
    public static void updateScoreboard(final Player p) {
        if (p.getScoreboard() == null) {
            sendScoreboard(p);
        }
        Scoreboard board = p.getScoreboard();
        Team t13 = board.getTeam("t13");
        t13.setPrefix("§8» §6§l" + Bukkit.getServer().getOnlinePlayers().size());
        Team t14 = board.getTeam("t10");
        t14.setPrefix("§8» ");
        t14.setSuffix("§6§l" + CoinsAPI.getCoins(p.getUniqueId().toString()).toString());
    }
}
