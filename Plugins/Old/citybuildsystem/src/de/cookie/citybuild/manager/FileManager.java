// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import de.cookie.citybuild.main.Main;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.IOException;
import org.bukkit.configuration.file.YamlConfiguration;
import java.io.File;

public class FileManager
{

    public static void loadConfig() {
        final File file = new File("plugins/CityBuildSystem", "config.yml");
        final FileConfiguration cfg = (FileConfiguration)YamlConfiguration.loadConfiguration(file);
        if (!file.exists()) {
            try {
                file.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            cfg.save(file);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        loadDefaults();
        Main.instance.getConfig().options().copyDefaults(true);
        Main.instance.saveConfig();
    }
    
    private static void loadDefaults() {
        Main.instance.getConfig().addDefault("MySQL.HOST", (Object)"Host");
        Main.instance.getConfig().addDefault("MySQL.DATABASE", (Object)"Database");
        Main.instance.getConfig().addDefault("MySQL.USER", (Object)"User");
        Main.instance.getConfig().addDefault("MySQL.PASSWORD", (Object)"Passwort");
        Main.instance.getConfig().addDefault("Prefix", (Object)"§6§lCBS §7» ");
        Main.instance.getConfig().addDefault("NoPermissions", (Object)"§cDu hast keine Berechtigung, um diesen Command ausf\u00fchren zu k\u00f6nnen");
        Main.instance.getConfig().addDefault("Use", (Object)"Bitte benutze: §e");
        Main.instance.getConfig().addDefault("NotEnoughCoins", (Object)"§cDu hast nicht genug Coins!");
        Main.instance.getConfig().addDefault("Title.Top", (Object)"§eWillkommen");
        Main.instance.getConfig().addDefault("Title.Bottom", (Object)"§7auf CityBuild");
        Main.instance.getConfig().addDefault("StandardCoins", (Object)1000);
        Main.instance.getConfig().addDefault("PrivateMessages", (Object)true);
        Main.instance.getConfig().addDefault("ScoreBoard.Title", (Object)"§eDeinServer§6.DE");
        Main.instance.getConfig().addDefault("ScoreBoard.Line.1", (Object)"§e§lSpieler");
        Main.instance.getConfig().addDefault("ScoreBoard.Line.2", (Object)"§e§lCookies");
        Main.instance.getConfig().addDefault("ScoreBoard.Line.3", (Object)"§e§lTeamSpeak");
        Main.instance.getConfig().addDefault("ScoreBoard.Line.4", (Object)"§6DeinServer.de");
        Main.instance.getConfig().addDefault("Booster.Fly.Zeit", (Object)600);
        Main.instance.getConfig().addDefault("Booster.Nohunger.Zeit", (Object)600);
        Main.instance.getConfig().addDefault("Booster.SchnellerAbbauen.Zeit", (Object)600);
        Main.instance.getConfig().addDefault("Einstellungen.InventarName", (Object)"§6§lSettings");
        Main.instance.getConfig().addDefault("Perks.Fly.Preis", (Object)150000);
        Main.instance.getConfig().addDefault("Perks.Nofall.Preis", (Object)100000);
        Main.instance.getConfig().addDefault("Perks.Nohunger.Preis", (Object)80000);
        Main.instance.getConfig().addDefault("Perks.SchnellerAbbauen.Preis", (Object)90000);
        Main.instance.getConfig().addDefault("FreeItems.Delay", (Object)2);
        Main.instance.getConfig().addDefault("FreeItems.Name", (Object)"§6§lFree§e§lItems");
        Main.instance.getConfig().addDefault("NoHitDelay.Delay", (Object)5);
    }
}
