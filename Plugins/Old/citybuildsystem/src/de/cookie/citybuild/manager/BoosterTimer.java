// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import de.cookie.citybuild.main.Main;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Player;
import org.bukkit.GameMode;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class BoosterTimer extends Thread
{
    static PerksManager perksManager;
    
    public static void startFlyBoost() {
        new BukkitRunnable() {
            public void run() {
                if (Var.flyboostcount > 0) {
                    --Var.flyboostcount;
                    if (Var.flyboostcount == 10) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7endet in §e10 Sekunden");
                    }
                    if (Var.flyboostcount == 5) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7endet in §e5 Sekunden");
                    }
                    if (Var.flyboostcount == 4) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7endet in §e4 Sekunden");
                    }
                    if (Var.flyboostcount == 3) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7endet in §e3 Sekunden");
                    }
                    if (Var.flyboostcount == 2) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7endet in §e2 Sekunden");
                    }
                    if (Var.flyboostcount == 1) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7endet in §e1 Sekunden");
                    }
                }
                else {
                    Bukkit.broadcastMessage(Var.prefix + "§7Der §eFly-Booster §7wurde §cdeaktiviert");
                    Bukkit.getOnlinePlayers().forEach(all -> {
                        if (!all.getGameMode().equals((Object)GameMode.CREATIVE) || Var.fly.contains(all.getName())) {
                            all.setAllowFlight(false);
                            all.setFlying(false);
                        }
                        return;
                    });
                    Var.flyboost = false;
                    this.cancel();
                    Var.flyboostcount = Main.instance.getConfig().getInt("Booster.Fly.Zeit");
                }
            }
        }.runTaskTimer((Plugin)Main.instance, 0L, 20L);
    }
    
    public static void startBreakBoost() {
        new BukkitRunnable() {
            public void run() {
                if (Var.breakboostcount > 0) {
                    --Var.breakboostcount;
                    if (Var.breakboostcount == 10) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7endet in §e10 Sekunden");
                    }
                    if (Var.breakboostcount == 5) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7endet in §e5 Sekunden");
                    }
                    if (Var.breakboostcount == 4) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7endet in §e4 Sekunden");
                    }
                    if (Var.breakboostcount == 3) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7endet in §e3 Sekunden");
                    }
                    if (Var.breakboostcount == 2) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7endet in §e2 Sekunden");
                    }
                    if (Var.breakboostcount == 1) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7endet in §e1 Sekunden");
                    }
                }
                else {
                    Bukkit.broadcastMessage(Var.prefix + "§7Der §eBreak-Booster §7wurde §cdeaktiviert");
                    Bukkit.getOnlinePlayers().forEach(all -> {
                        if (!BoosterTimer.perksManager.hasPerk(all.getUniqueId(), "FASTDIGGING")) {
                            all.removePotionEffect(PotionEffectType.FAST_DIGGING);
                        }
                        return;
                    });
                    Var.breakboost = false;
                    this.cancel();
                    Var.breakboostcount = Main.instance.getConfig().getInt("Booster.SchnellerAbbauen.Zeit");
                }
            }
        }.runTaskTimer((Plugin)Main.instance, 0L, 20L);
    }
    
    public static void startNoHungerBoost() {
        new BukkitRunnable() {
            public void run() {
                if (Var.nohungerboostcount > 0) {
                    --Var.nohungerboostcount;
                    if (Var.nohungerboostcount == 10) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7endet in §e10 Sekunden");
                    }
                    if (Var.nohungerboostcount == 5) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7endet in §e5 Sekunden");
                    }
                    if (Var.nohungerboostcount == 4) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7endet in §e4 Sekunden");
                    }
                    if (Var.nohungerboostcount == 3) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7endet in §e3 Sekunden");
                    }
                    if (Var.nohungerboostcount == 2) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7endet in §e2 Sekunden");
                    }
                    if (Var.nohungerboostcount == 1) {
                        Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7endet in §e1 Sekunden");
                    }
                }
                else {
                    Bukkit.broadcastMessage(Var.prefix + "§7Der §eNoHunger-Booster §7wurde §cdeaktiviert");
                    Var.nohunger = false;
                    this.cancel();
                    Var.nohungerboostcount = Main.instance.getConfig().getInt("Booster.Nohunger.Zeit");
                }
            }
        }.runTaskTimer((Plugin)Main.instance, 0L, 20L);
    }
    
    static {
        BoosterTimer.perksManager = new PerksManager();
    }
}
