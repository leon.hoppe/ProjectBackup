// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import org.bukkit.inventory.Inventory;
import de.omel.api.itemstack.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.Bukkit;
import de.cookie.citybuild.main.Main;
import org.bukkit.entity.Player;
import java.util.UUID;
import de.omel.api.file.FileBuilder;

public class SettingsManager
{
    private FileBuilder fb;
    
    public SettingsManager() {
        this.fb = new FileBuilder("plugins/CityBuildSystem/Settings", "settings.yml");
    }
    
    public boolean exists(final UUID uuid) {
        return this.fb.getString(uuid.toString()) != null;
    }
    
    public void createPlayer(final UUID uuid) {
        this.fb.setValue(uuid.toString() + ".Msg", true);
        this.fb.setValue(uuid.toString() + ".TpaAnfragen", true);
        this.fb.save();
    }
    
    public void openSettings(final Player player) {
        final Inventory inventory = Bukkit.createInventory((InventoryHolder)null, 36, Main.instance.getConfig().getString("Einstellungen.InventarName"));
        for (int i = 0; i < inventory.getSize(); ++i) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)15).setDisplayname(" ").build());
        }
        inventory.setItem(11, new ItemBuilder(Material.BOOK_AND_QUILL).setDisplayname("§e§lMSG").build());
        inventory.setItem(15, new ItemBuilder(Material.ENDER_PEARL).setDisplayname("§e§lTpaAnfragen").build());
        if (this.fb.getBoolean(player.getUniqueId() + ".Msg")) {
            inventory.setItem(20, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
        }
        else {
            inventory.setItem(24, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
        }
        if (this.fb.getBoolean(player.getUniqueId() + ".TpaAnfragen")) {
            inventory.setItem(24, new ItemBuilder(Material.STAINED_CLAY, 1, (short)5).setDisplayname("§aAN").build());
        }
        else {
            inventory.setItem(24, new ItemBuilder(Material.STAINED_CLAY, 1, (short)14).setDisplayname("§cAUS").build());
        }
        player.openInventory(inventory);
    }
    
    public boolean getMsg(final UUID uuid) {
        return this.fb.getBoolean(uuid + ".Msg");
    }
    
    public void setMsg(final UUID uuid, final boolean enabledOrDisabled) {
        this.fb.setValue(uuid + ".Msg", enabledOrDisabled);
        this.fb.save();
    }
    
    public boolean getTpa(final UUID uuid) {
        return this.fb.getBoolean(uuid + ".TpaAnfragen");
    }
    
    public void setTpa(final UUID uuid, final boolean enabledOrDisabled) {
        this.fb.setValue(uuid + ".TpaAnfragen", enabledOrDisabled);
        this.fb.save();
    }
}
