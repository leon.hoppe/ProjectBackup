// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import de.omel.api.file.FileBuilder;

public class WarpManager
{
    private FileBuilder fb;
    
    public WarpManager() {
        this.fb = new FileBuilder("plugins/CityBuildSystem", "warps.yml");
    }
    
    public void setWarp(final Location location, final String name) {
        this.fb.setValue(name + ".world", location.getWorld().getName());
        this.fb.setValue(name + ".x", location.getX());
        this.fb.setValue(name + ".y", location.getY());
        this.fb.setValue(name + ".z", location.getZ());
        this.fb.setValue(name + ".yaw", location.getYaw());
        this.fb.setValue(name + ".pitch", location.getPitch());
        this.fb.save();
    }
    
    public Location getWarpLocation(final String name) {
        return new Location(Bukkit.getWorld(this.fb.getString(name + ".world")), this.fb.getDouble(name + ".x"), this.fb.getDouble(name + ".y"), this.fb.getDouble(name + ".z"), (float)this.fb.getDouble(name + ".yaw"), (float)this.fb.getDouble(name + ".pitch"));
    }
    
    public Set<String> getWarps() {
        return this.fb.getKeys(false);
    }
    
    public boolean exists(final String name) {
        return this.fb.getString(name) != null;
    }
    
    public void delWarp(final String name) {
        this.fb.setValue(name, null);
        this.fb.save();
    }
}
