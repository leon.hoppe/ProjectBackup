package de.cookie.citybuild.manager;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.cookie.citybuild.main.Main;

public class CoinsAPI
{
    public static Integer getCoins(final String uuid) {
        Integer i = 0;
        if (playerExists(uuid)) {
            try {
                final ResultSet rs = Main.mysql.getResults("SELECT * FROM Coins WHERE UUID= '" + uuid + "'");
                if (!rs.next() || Integer.valueOf(rs.getInt("coins")) == null) {}
                i = rs.getInt("coins");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            createPlayer(uuid);
            getCoins(uuid);
        }
        return i;
    }
    
    public static void createPlayer(final String uuid) {
        if (!playerExists(uuid)) {
            Main.mysql.update("INSERT INTO Coins(UUID, coins) VALUES ('" + uuid + "', '0');");
        }
    }
    
    public static boolean playerExists(final String uuid) {
        try {
            final ResultSet rs = Main.mysql.getResults("SELECT * FROM Coins WHERE UUID= '" + uuid + "'");
            return rs.next() && rs.getString("UUID") != null;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static void setCoins(final String uuid, final Integer coins) {
        if (playerExists(uuid)) {
            Main.mysql.update("UPDATE Coins SET COINS= '" + coins + "' WHERE UUID= '" + uuid + "';");
        }
        else {
            createPlayer(uuid);
            setCoins(uuid, coins);
        }
    }
    
    public static void addCoins(final String uuid, final Integer coins) {
        if (playerExists(uuid)) {
            setCoins(uuid, getCoins(uuid) + coins);
        }
        else {
            createPlayer(uuid);
            addCoins(uuid, coins);
        }
    }
    
    public static void removeCoins(final String uuid, final Integer coins) {
        if (playerExists(uuid)) {
            setCoins(uuid, getCoins(uuid) - coins);
        }
        else {
            createPlayer(uuid);
            removeCoins(uuid, coins);
        }
    }
}
