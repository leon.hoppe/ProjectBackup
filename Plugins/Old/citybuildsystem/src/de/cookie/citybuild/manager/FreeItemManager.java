// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import de.cookie.citybuild.main.Main;
import org.bukkit.plugin.Plugin;
import org.bukkit.Effect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;
import de.omel.api.file.FileBuilder;

public class FreeItemManager
{
    public static FileBuilder fb;
    public static BukkitTask bukkitTask;
    
    public FreeItemManager() {
        FreeItemManager.fb = new FileBuilder("plugins/CityBuildSystem", "freeitem.yml");
    }
    
    public void setFreeItem(final Location location, final ItemStack itemStack) {
        FreeItemManager.fb.setValue("FreeItem.Location.world", location.getWorld().getName());
        FreeItemManager.fb.setValue("FreeItem.Location.x", location.getX());
        FreeItemManager.fb.setValue("FreeItem.Location.y", location.getY());
        FreeItemManager.fb.setValue("FreeItem.Location.z", location.getZ());
        FreeItemManager.fb.setValue("FreeItem.Location.yaw", location.getYaw());
        FreeItemManager.fb.setValue("FreeItem.Location.pitch", location.getPitch());
        FreeItemManager.fb.setValue("FreeItem.Item", itemStack);
        FreeItemManager.fb.save();
    }
    
    public Location getLocation(final String name) {
        return new Location(Bukkit.getWorld(FreeItemManager.fb.getString(name + ".world")), FreeItemManager.fb.getDouble(name + ".x"), FreeItemManager.fb.getDouble(name + ".y"), FreeItemManager.fb.getDouble(name + ".z"), (float)FreeItemManager.fb.getDouble(name + ".yaw"), (float)FreeItemManager.fb.getDouble(name + ".pitch"));
    }
    
    public void removeFreeItem() {
        FreeItemManager.fb.setValue("FreeItem.Location.world", null);
        FreeItemManager.fb.setValue("FreeItem.Location.x", null);
        FreeItemManager.fb.setValue("FreeItem.Location.y", null);
        FreeItemManager.fb.setValue("FreeItem.Location.z", null);
        FreeItemManager.fb.setValue("FreeItem.Location.yaw", null);
        FreeItemManager.fb.setValue("FreeItem.Location.pitch", null);
        FreeItemManager.fb.setValue("FreeItem.Item", null);
        FreeItemManager.fb.save();
    }
    
    public boolean exists() {
        return FreeItemManager.fb.getString("FreeItem.Item") != null;
    }
    
    public void startFreeItem(final ItemStack itemStack) {
        FreeItemManager.bukkitTask = new BukkitRunnable() {
            public void run() {
                if (Var.freeitem) {
                    final Location location = Main.instance.getFreeItemManager().getLocation("FreeItem.Location");
                    location.getWorld().dropItem(location, itemStack);
                    location.getWorld().playEffect(location, Effect.HAPPY_VILLAGER, 30);
                }
                else {
                    this.cancel();
                }
            }
        }.runTaskTimer((Plugin)Main.instance, 0L, (long)(20 * Main.instance.getConfig().getInt("FreeItems.Delay")));
    }
}
