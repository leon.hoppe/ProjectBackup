// 
// Decompiled by Procyon v0.5.36
// 

package de.cookie.citybuild.manager;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.inventory.Inventory;
import java.util.Arrays;
import de.cookie.citybuild.main.Main;
import de.omel.api.itemstack.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.util.UUID;
import de.omel.api.file.FileBuilder;

public class PerksManager
{
    private FileBuilder fb;
    
    public PerksManager() {
        this.fb = new FileBuilder("plugins/CityBuildSystem/Perks", "perks.yml");
    }
    
    public void createPlayer(final UUID uuid) {
        if (!this.fb.contains(uuid.toString())) {
            this.fb.setValue(uuid + ".FLY", false);
            this.fb.setValue(uuid + ".NOFALL", false);
            this.fb.setValue(uuid + ".NOHUNGER", false);
            this.fb.setValue(uuid + ".FASTDIGGING", false);
            this.fb.save();
        }
    }
    
    public void addPerk(final UUID uuid, final String name) {
        this.fb.setValue(uuid + "." + name, true);
        this.fb.save();
    }
    
    public void removePerk(final UUID uuid, final String name) {
        this.fb.setValue(uuid + "." + name, false);
        this.fb.save();
    }
    
    public boolean hasPerk(final UUID uuid, final String name) {
        return this.fb.getBoolean(uuid + "." + name);
    }
    
    public void openPerksInventory(final Player player) {
        final Inventory inventory = Bukkit.createInventory((InventoryHolder)null, 36, "§6§lPERKS");
        for (int i = 0; i < inventory.getSize(); ++i) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short)15).setDisplayname(" ").build());
        }
        inventory.setItem(10, new ItemBuilder(Material.FEATHER).setDisplayname("§e§lFLY-PERK").build());
        inventory.setItem(12, new ItemBuilder(Material.DIAMOND_BOOTS).setDisplayname("§e§lNOFALL-PERK").build());
        inventory.setItem(14, new ItemBuilder(Material.COOKED_BEEF).setDisplayname("§e§lNOHUNGER-PERK").build());
        inventory.setItem(16, new ItemBuilder(Material.DIAMOND_PICKAXE).setDisplayname("§e§lSCHNELLER ABBAUEN-PERK").build());
        if (!this.hasPerk(player.getUniqueId(), "FLY")) {
            inventory.setItem(19, new ItemBuilder(Material.PAPER).setDisplayname("§6Fly-Perk kaufen").setLore(Arrays.asList("§7§m--------------", "§e" + Main.instance.getConfig().getInt("Perks.Fly.Preis"))).build());
        }
        else {
            inventory.setItem(19, new ItemBuilder(Material.INK_SACK, 1, (short)10).setDisplayname("§a§lGEKAUFT").build());
        }
        if (!this.hasPerk(player.getUniqueId(), "NOFALL")) {
            inventory.setItem(21, new ItemBuilder(Material.PAPER).setDisplayname("§6Nofall-Perk kaufen").setLore(Arrays.asList("§7§m--------------", "§e" + Main.instance.getConfig().getInt("Perks.Nofall.Preis"))).build());
        }
        else {
            inventory.setItem(21, new ItemBuilder(Material.INK_SACK, 1, (short)10).setDisplayname("§a§lGEKAUFT").build());
        }
        if (!this.hasPerk(player.getUniqueId(), "NOHUNGER")) {
            inventory.setItem(23, new ItemBuilder(Material.PAPER).setDisplayname("§6NoHunger-Perk kaufen").setLore(Arrays.asList("§7§m--------------", "§e" + Main.instance.getConfig().getInt("Perks.Nohunger.Preis"))).build());
        }
        else {
            inventory.setItem(23, new ItemBuilder(Material.INK_SACK, 1, (short)10).setDisplayname("§a§lGEKAUFT").build());
        }
        if (!this.hasPerk(player.getUniqueId(), "FASTDIGGING")) {
            inventory.setItem(25, new ItemBuilder(Material.PAPER).setDisplayname("§6Schneller Abbauen-Perk kaufen").setLore(Arrays.asList("§7§m--------------", "§e" + Main.instance.getConfig().getInt("Perks.SchnellerAbbauen.Preis"))).build());
        }
        else {
            inventory.setItem(25, new ItemBuilder(Material.INK_SACK, 1, (short)10).setDisplayname("§a§lGEKAUFT").build());
        }
        player.openInventory(inventory);
    }
    
    public void checkPerks(final Player player) {
        if (this.hasPerk(player.getUniqueId(), "FLY")) {
            Var.fly.add(player.getName());
            player.setAllowFlight(true);
        }
        else {
            Var.fly.remove(player.getName());
            player.setAllowFlight(false);
        }
        if (this.hasPerk(player.getUniqueId(), "NOFALL")) {
            Var.nofall.add(player.getName());
        }
        else {
            Var.nofall.remove(player.getName());
        }
        if (this.hasPerk(player.getUniqueId(), "NOHUNGER")) {
            Var.nohunger_players.add(player.getName());
        }
        else {
            Var.nohunger_players.remove(player.getName());
        }
        if (this.hasPerk(player.getUniqueId(), "FASTDIGGING")) {
            Var.fastdigging.add(player.getName());
            player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 999999, 10));
        }
        else if (!Var.breakboost) {
            player.removePotionEffect(PotionEffectType.FAST_DIGGING);
        }
    }
}
