package de.leon.network.lobbysystem.commands;

import de.leon.network.lobbysystem.LobbySystem;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class SpawnCmd extends Command {
    public SpawnCmd() {
        super("spawn");
    }

    @Override
    public boolean execute(@NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] strings) {
        if (commandSender instanceof Player player) {
            player.teleport(LobbySystem.SPAWN);
        }
        return true;
    }
}
