package de.leon.network.lobbysystem.listeners;

import de.leon.network.lobbysystem.LobbySystem;
import io.papermc.paper.event.player.AsyncChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;

public class ProtectionHandler implements Listener {

    @EventHandler
    public void OnBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnChat(AsyncChatEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnDamage(EntityDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnFood(FoodLevelChangeEvent event) {
        event.setCancelled(true);
        event.setFoodLevel(25);
    }

    @EventHandler
    public void OnDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnWeather(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnDeath(PlayerDeathEvent event) {
        event.getDrops().clear();
        Bukkit.getScheduler().scheduleSyncDelayedTask(LobbySystem.instance, () -> event.getPlayer().spigot().respawn());
    }

    @EventHandler
    public void OnRespawn(PlayerRespawnEvent event) {
        event.setRespawnLocation(LobbySystem.SPAWN);
    }

    @EventHandler
    public void OnMove(PlayerMoveEvent event) {
        if (event.getPlayer().getLocation().getY() < 0) {
            event.getPlayer().teleport(LobbySystem.SPAWN);
        }
    }

    @EventHandler
    public void OnPickup(PlayerAttemptPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void OnInteract(PlayerInteractEvent event) {
        event.setCancelled(true);
    }

}
