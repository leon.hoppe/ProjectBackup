package de.leon.network.lobbysystem.listeners;

import de.leon.network.lobbysystem.LobbySystem;
import net.kyori.adventure.text.Component;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectHandler implements Listener {

    @EventHandler
    public void OnJoin(PlayerJoinEvent event) {
        event.joinMessage(Component.empty());
        event.getPlayer().teleport(LobbySystem.SPAWN);
    }

    @EventHandler
    public void OnQuit(PlayerQuitEvent event) {
        event.quitMessage(Component.empty());
    }

}
