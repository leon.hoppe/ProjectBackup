package de.leon.network.lobbysystem;

import de.leon.network.lobbysystem.commands.SpawnCmd;
import de.leon.network.lobbysystem.listeners.ConnectHandler;
import de.leon.network.lobbysystem.listeners.ProtectionHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class LobbySystem extends JavaPlugin {

    public static final Location SPAWN = new Location(Bukkit.getWorld("world"), 26.5, 62, -23.5, 0, 0);
    public static LobbySystem instance;

    @Override
    public void onEnable() {
        instance = this;
        // Plugin startup logic
        PluginManager manager = getServer().getPluginManager();
        manager.registerEvents(new ConnectHandler(), this);
        manager.registerEvents(new ProtectionHandler(), this);

        new SpawnCmd().register(getServer().getCommandMap());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
