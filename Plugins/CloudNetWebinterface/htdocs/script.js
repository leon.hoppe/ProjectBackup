initialize = async function () {
    await loadPlayers();

    await loadConsole();
    const console = document.getElementById("console");
    console.scrollTop = console.scrollHeight;

    document.getElementById("send").onclick = async function () {
        const cmd = document.getElementById("command");
        await sendPostRequest(hostname + "/api/console/send?command=" + cmd.value, "");
        cmd.value = "";
        await loadConsole();
    }

    document.getElementById("stop").onclick = async function () {
        await sendGetRequest(hostname + "/api/console/stop")
    }
    document.getElementById("reload").onclick = async function () {
        await sendGetRequest(hostname + "/api/console/reload")
    }

    setInterval(async () => {
        await loadConsole();
        await loadPlayers();
    }, 5000);
}

async function loadPlayers() {
    const players = await sendGetRequest(hostname + "/api/players", true);
    const playerContainer = document.getElementById("players");

    let html = "";
    for (const player of players) {
        const name = player["name"];
        const server = player["server"];
        html += `
            <div class="player">
                <img src="https://minotar.net/avatar/${name}/200" alt="Player Head">
                <span class="playerName">${name}</span>
                <br>
                <span class="playerServer">Server: ${server}</span>
            </div>
        `;
    }

    playerContainer.innerHTML = html;
}

async function loadConsole() {
    const consoleLog = await sendGetRequest(hostname + "/api/console");
    const console = document.getElementById("console");
    console.innerText = consoleLog;
}