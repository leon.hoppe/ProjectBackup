const hostname = location.protocol + "//" + location.host;
if (!location.href.replace(hostname, "").includes(".") && !location.href.endsWith("/")) location.href += "/";

let sessionKey;

window.onload = async function () {
    sessionKey = sessionStorage.getItem("sessionKey");
    const valid = await sendGetRequest(hostname + "/api/users/validate");
    if (valid === "false" && !location.href.includes("login")) location.href = hostname + "/login/";
    else {
        const componentClients = document.getElementsByClassName("component");
        for (let element of componentClients) {
            const componentName = element.id.replace("component-", "");
            element.innerHTML = await sendGetRequest(hostname + "/components/" + componentName + ".html");
        }
        await initialize();
        initializeNavBar();
    }
}

async function sendHttpRequest(url, body = "", method = "GET") {
    return await fetch(url, {
        method: method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': sessionKey
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: body
    });
}

async function sendGetRequest(url, asJson = false) {
    const response = await sendHttpRequest(url, null, "GET");
    return asJson ? await response.json() : await response.text();
}

async function sendPutRequest(url, body, asJson = false) {
    const response = await sendHttpRequest(url, body, "PUT");
    return asJson ? await response.json() : await response.text();
}

async function sendPostRequest(url, body, asJson = false) {
    const response = await sendHttpRequest(url, body, "POST");
    return asJson ? await response.json() : await response.text();
}

async function sendDeleteRequest(url, asJson = false) {
    const response = await sendHttpRequest(url, null, "DELETE");
    return asJson ? await response.json() : await response.text();
}

function initializeNavBar() {
    const buttons = document.getElementsByClassName("navigation-button");
    for (let button of buttons) {
        button.onclick = function () {
            location.href = hostname + button.id.replace("navigation-", "");
        }
    }
}

let initialize = function () {}