const submit = document.getElementById("submit");

submit.onclick = async function() {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    const response = await sendGetRequest(hostname + "/api/users/login?username=" + username + "&password=" + password);

    if (response !== "false") {
        sessionStorage.setItem("sessionKey", response);
        sessionKey = response;
        location.href = hostname;
    }else {
        showSnackbar();
    }
}

function showSnackbar() {
    document.getElementById('snackbar')?.classList.add('show');
    setTimeout(() => {document.getElementById('snackbar')?.classList.remove('show');}, 5000);
}