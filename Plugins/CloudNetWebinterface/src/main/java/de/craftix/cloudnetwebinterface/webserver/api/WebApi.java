package de.craftix.cloudnetwebinterface.webserver.api;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import de.craftix.cloudnetwebinterface.webserver.WebServerManager;

import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;

public class WebApi {

    protected void sendDataAsJson(HttpExchange exchange, Serializable data) {
        if (data instanceof String) sendData(exchange, ((String) data).getBytes());
        else sendData(exchange, new Gson().toJson(data).getBytes());
    }

    protected void sendData(HttpExchange exchange, byte[] data) {
        sendData(exchange, LogicResult.OK, data);
    }

    protected <T> T getDataFromBody(HttpExchange exchange, Type type) {
        byte[] data = WebServerManager.getBytesFromStream(exchange.getRequestBody());
        Gson gson = new Gson();
        return gson.fromJson(new String(data), type);
    }

    protected void sendData(HttpExchange exchange, LogicResult code, byte[] data) {
        try {
            if (data == null || data.length == 0) {
                exchange.sendResponseHeaders(code.getCode(), -1);
                OutputStream out = exchange.getResponseBody();
                out.write(new byte[0]);
                out.close();
            }
            exchange.sendResponseHeaders(code.getCode(), data.length);
            OutputStream out = exchange.getResponseBody();
            out.write(data);
            out.close();
        }catch (Exception e) { e.printStackTrace(); }
    }


}
