package de.craftix.cloudnetwebinterface.utils;

import de.craftix.cloudnetwebinterface.CloudNetWebinterface;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.permission.IPermissionGroup;
import de.dytanic.cloudnet.driver.permission.IPermissionUser;
import de.dytanic.cloudnet.driver.permission.PermissionUserGroupInfo;
import de.dytanic.cloudnet.ext.bridge.player.ICloudOfflinePlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;

import java.sql.ResultSet;
import java.util.Base64;
import java.util.UUID;

public class DBManager {
    private static SQLite sqLite;

    public static void initialize() {
        sqLite = CloudNetWebinterface.getSQLite();
        sqLite.insert("CREATE TABLE IF NOT EXISTS Users (UUID VARCHAR(100), Password VARCHAR(50))");
        sqLite.insert("CREATE TABLE IF NOT EXISTS Sessions (UUID VARCHAR(100), SessionKey VARCHAR(100), CreationDate INT(255))");
    }

    public static void addUser(UUID uuid, String password) {
        password = Base64.getEncoder().encodeToString(password.getBytes());
        sqLite.insert("DELETE FROM Users WHERE UUID = \"" + uuid + "\"");
        sqLite.insert("INSERT INTO Users VALUES (\"" + uuid + "\", \"" + password + "\")");
    }

    public static boolean checkLoginData(String username, String password) {
        ICloudOfflinePlayer player = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class).getFirstOfflinePlayer(username);
        if (player == null) return false;
        return password.equals(getUserPassword(player.getUniqueId()));
    }

    private static String getUserPassword(UUID uuid) {
        try {
            ResultSet rs = sqLite.getData("SELECT Password FROM Users WHERE UUID = \"" + uuid + "\"");
            if (rs.next()) {
                return new String(Base64.getDecoder().decode(rs.getString("Password")));
            }
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    public static String createSessionKey(UUID user) {
        String session = UUID.randomUUID().toString();
        sqLite.insert("DELETE FROM Sessions WHERE UUID = \"" + user + "\"");
        sqLite.insert("INSERT INTO Sessions VALUES (\"" + user + "\", \"" + session + "\", " + System.currentTimeMillis() + ")");
        return session;
    }

    public static boolean validateSession(String session) {
        try {
            ResultSet rs = sqLite.getData("SELECT CreationDate FROM Sessions WHERE SessionKey = \"" + session + "\"");
            if (rs.next()) {
                long creationDate = rs.getLong("CreationDate");
                double time = System.currentTimeMillis() - creationDate;
                time = time / 1000 / 60 / 60;
                return time < 12;
            }
        }catch (Exception e) { e.printStackTrace(); }
        return false;
    }

    public static boolean validateForPermission(String sessionKey, String permission) {
        try {
            ResultSet rs = sqLite.getData("SELECT UUID FROM Sessions WHERE SessionKey = \"" + sessionKey + "\"");
            if (rs.next()) {
                UUID uuid = UUID.fromString(rs.getString("UUID"));
                IPermissionUser player = CloudNetDriver.getInstance().getPermissionManagement().getUser(uuid);
                if (player == null) return false;
                boolean hasPermission = player.hasPermission(permission).asBoolean();
                if (!hasPermission) {
                    for (IPermissionGroup group : CloudNetDriver.getInstance().getPermissionManagement().getGroups(player)) {
                        if (group.hasPermission(permission).asBoolean()) return true;
                    }
                }
                return hasPermission;
            }
        }catch (Exception e) { e.printStackTrace(); }
        return false;
    }

}
