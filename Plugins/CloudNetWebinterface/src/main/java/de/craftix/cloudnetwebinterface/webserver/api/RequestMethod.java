package de.craftix.cloudnetwebinterface.webserver.api;

public enum RequestMethod {
    GET("GET"),
    POST("POST"),
    PUT("PUT"),
    DELETE("DELETE");

    private final String type;

    RequestMethod(String type) { this.type = type; }

    public String getType() { return type; }

    @Override
    public String toString() { return type; }
}
