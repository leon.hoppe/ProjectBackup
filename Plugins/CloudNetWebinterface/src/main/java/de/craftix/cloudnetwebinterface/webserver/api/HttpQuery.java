package de.craftix.cloudnetwebinterface.webserver.api;

import com.sun.net.httpserver.HttpExchange;

import java.util.HashMap;
import java.util.Map;

public class HttpQuery {
    private final Map<String, String> variables = new HashMap<>();

    public HttpQuery(HttpExchange exchange) {
        if (!exchange.getRequestURI().toString().contains("?")) return;
        String raw = exchange.getRequestURI().getQuery();
        String[] vars = raw.split("&");
        for (String var : vars) {
            String[] param = var.split("=");
            if (param.length == 0) return;
            if (param.length == 1) variables.put(param[0].toLowerCase(), "");
            else variables.put(param[0].toLowerCase(), param[1]);
        }
    }

    public String getVariable(String name) { return variables.get(name.toLowerCase()) != null ? variables.get(name.toLowerCase()) : ""; }
}
