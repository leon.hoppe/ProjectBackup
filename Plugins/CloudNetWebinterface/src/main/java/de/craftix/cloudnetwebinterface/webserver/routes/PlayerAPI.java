package de.craftix.cloudnetwebinterface.webserver.routes;

import com.sun.net.httpserver.HttpExchange;
import de.craftix.cloudnetwebinterface.webserver.api.*;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.BridgeServiceProperty;
import de.dytanic.cloudnet.ext.bridge.player.ServicePlayer;

import java.io.Serializable;
import java.util.*;

@Route("/api/players")
public class PlayerAPI extends WebApi {

    @Authorized
    @SetMethod(RequestMethod.GET)
    public void getPlayers(HttpExchange exchange) {
        Collection<ServiceInfoSnapshot> servers = CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices();
        List<Player> processedPlayers = new ArrayList<>();

        for (ServiceInfoSnapshot service : servers) {
            Collection<ServicePlayer> players = service.getProperty(BridgeServiceProperty.PLAYERS).orElse(new ArrayList<>());
            for (ServicePlayer player : players) {
                if (processedPlayers.contains(new Player(player.getName(), null))) continue;
                String server = player.asBungee().getServer();
                processedPlayers.add(new Player(player.getName(), server != null ? server : "ausstehend..."));
            }
        }

        sendDataAsJson(exchange, processedPlayers.toArray(new Player[0]));
    }

    private static class Player implements Serializable {
        public final String name;
        public final String server;

        public Player(String name, String server) {
            this.name = name;
            this.server = server;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Player player = (Player) o;
            return Objects.equals(name, player.name);
        }

        @Override public int hashCode() { return Objects.hash(name, server); }
    }

}
