package de.craftix.cloudnetwebinterface.commands;

import de.craftix.cloudnetwebinterface.utils.DBManager;
import de.dytanic.cloudnet.command.DriverCommandSender;
import de.dytanic.cloudnet.command.ICommandSender;
import de.dytanic.cloudnet.command.sub.SubCommandHandler;
import de.dytanic.cloudnet.common.Properties;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;

public class MainCmd extends SubCommandHandler {

    public MainCmd() {
        super("register");
        this.permission = "webinterface.admin";
        this.description = "Manage the Webinterface";
        this.usage = "/cloud register <username> <password>";
    }

    @Override
    public void execute(ICommandSender sender, String command, String[] args, String commandLine, Properties properties) {
        if (!(sender instanceof DriverCommandSender)) sender.sendMessage("Dieser Command kann nur als Spieler ausgeführt werden!");
        else if (args.length != 2) sender.sendMessage(getUsage());
        else {
            ICloudPlayer player = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class).getFirstOnlinePlayer(args[0]);
            if (player == null) throw new NullPointerException("Player cannot be null");
            String password = args[1];
            DBManager.addUser(player.getUniqueId(), password);
            sender.sendMessage("Erfolgreich im Webinterface registriert!");
        }
    }
}
