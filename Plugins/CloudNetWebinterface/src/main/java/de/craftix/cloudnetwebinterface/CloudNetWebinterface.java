package de.craftix.cloudnetwebinterface;

import de.craftix.cloudnetwebinterface.commands.MainCmd;
import de.craftix.cloudnetwebinterface.utils.DBManager;
import de.craftix.cloudnetwebinterface.utils.SQLite;
import de.craftix.cloudnetwebinterface.webserver.WebServerManager;
import de.craftix.cloudnetwebinterface.webserver.routes.ConsoleAPI;
import de.craftix.cloudnetwebinterface.webserver.routes.PlayerAPI;
import de.craftix.cloudnetwebinterface.webserver.routes.UserAPI;
import de.dytanic.cloudnet.driver.module.ModuleLifeCycle;
import de.dytanic.cloudnet.driver.module.ModuleTask;
import de.dytanic.cloudnet.module.NodeCloudNetModule;

public class CloudNetWebinterface extends NodeCloudNetModule {
    private static CloudNetWebinterface instance;
    private static SQLite sqLite;
    private static WebServerManager webServerManager;

    @ModuleTask(event = ModuleLifeCycle.STARTED)
    public void onEnable() {
        instance = this;
        sqLite = new SQLite("modules/Webinterface/data.db");
        DBManager.initialize();

        //Start Server
        webServerManager = new WebServerManager(4041);
        webServerManager.addApi(new UserAPI());
        webServerManager.addApi(new PlayerAPI());
        webServerManager.addApi(new ConsoleAPI());

        registerCommand(new MainCmd());
    }

    @ModuleTask(event = ModuleLifeCycle.STOPPED)
    public void onDisable() {
        webServerManager.stop();
    }

    public static CloudNetWebinterface getInstance() { return instance; }
    public static SQLite getSQLite() { return sqLite; }
    public static WebServerManager getWebServerManager() { return webServerManager; }

}
