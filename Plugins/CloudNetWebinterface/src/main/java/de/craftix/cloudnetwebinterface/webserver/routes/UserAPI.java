package de.craftix.cloudnetwebinterface.webserver.routes;

import com.sun.net.httpserver.HttpExchange;
import de.craftix.cloudnetwebinterface.utils.DBManager;
import de.craftix.cloudnetwebinterface.webserver.api.HttpQuery;
import de.craftix.cloudnetwebinterface.webserver.api.Route;
import de.craftix.cloudnetwebinterface.webserver.api.WebApi;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudOfflinePlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;

@Route("/api/users")
public class UserAPI extends WebApi {

    @Route("/validate")
    public void validate(HttpExchange exchange) {
        String sessionKey = exchange.getRequestHeaders().getFirst("Authorization");
        boolean valid = DBManager.validateSession(sessionKey);
        sendData(exchange, (valid ? "true" : "false").getBytes());
    }

    @Route("/login")
    public void login(HttpExchange exchange) {
        HttpQuery query = new HttpQuery(exchange);
        String username = query.getVariable("username");
        String password = query.getVariable("password");
        boolean valid = DBManager.checkLoginData(username, password);
        if (valid) {
            ICloudOfflinePlayer player = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class).getFirstOfflinePlayer(username);
            if (player == null) sendData(exchange, "false".getBytes());
            else sendData(exchange, DBManager.createSessionKey(player.getUniqueId()).getBytes());
        }else {
            sendData(exchange, "false".getBytes());
        }
    }

}
