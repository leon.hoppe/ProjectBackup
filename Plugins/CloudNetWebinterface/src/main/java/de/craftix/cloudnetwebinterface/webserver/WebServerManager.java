package de.craftix.cloudnetwebinterface.webserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import de.craftix.cloudnetwebinterface.utils.DBManager;
import de.craftix.cloudnetwebinterface.webserver.api.*;

import java.io.*;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebServerManager implements HttpHandler {
    private WebFileManager webFileManager;

    private HttpServer server;

    private final Map<String, Method> apis = new HashMap<>();
    private final Map<Method, WebApi> apiObjects = new HashMap<>();

    public WebServerManager(int port) {
        try {
            server = HttpServer.create(new InetSocketAddress(port), 0);
            server.createContext("/api").setHandler(this);
            webFileManager = new WebFileManager(this);
            server.start();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() { server.stop(0); }

    public HttpServer getServer() { return server; }
    public WebFileManager getWebFileManager() { return webFileManager; }

    public static byte[] getApiTester() {
        return ("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Http Request</title>\n" +
                "    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">\n" +
                "    <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js\" integrity=\"sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB\" crossorigin=\"anonymous\"></script>\n" +
                "    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\" crossorigin=\"anonymous\"></script>\n" +
                "\n" +
                "    <style>\n" +
                "        .hover:hover {\n" +
                "            cursor: pointer;\n" +
                "        }\n" +
                "\n" +
                "        .unselectable {\n" +
                "            -webkit-touch-callout: none;\n" +
                "            -webkit-user-select: none;\n" +
                "            -moz-user-select: none;\n" +
                "            -ms-user-select: none;\n" +
                "            user-select: none;\n" +
                "        }\n" +
                "\n" +
                "        #url_start {\n" +
                "            border-top-right-radius: 0;\n" +
                "            border-bottom-right-radius: 0;\n" +
                "        }\n" +
                "\n" +
                "        #url, #apikey {\n" +
                "            border-top-left-radius: 0;\n" +
                "            border-bottom-left-radius: 0;\n" +
                "        }\n" +
                "    </style>\n" +
                "<body>\n" +
                "\n" +
                "<section style=\"margin-top: 20px\" id=\"options\">\n" +
                "    <p class=\"text-center unselectable\" style=\"font-size: 50px\">Send Http Request</p>\n" +
                "\n" +
                "    <form class=\"container\">\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col\">\n" +
                "                <label for=\"url\">URL</label>\n" +
                "                <div class=\"input-group mb-3\">\n" +
                "                    <div class=\"input-group-prepend\">\n" +
                "                        <span class=\"input-group-text\" id=\"url_start\"></span>\n" +
                "                    </div>\n" +
                "                    <input type=\"text\" class=\"form-control\" id=\"url\" aria-describedby=\"url_start\">\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"dropdown col col-md-2\">\n" +
                "                <label for=\"method\" class=\"unselectable\">Select Method</label>\n" +
                "                <input class=\"btn btn-secondary dropdown-toggle form-control unselectable\" type=\"button\" id=\"method\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\" value=\"GET\">\n" +
                "                <ul class=\"dropdown-menu\" aria-labelledby=\"method\">\n" +
                "                    <li><a class=\"dropdown-item hover unselectable\" id=\"GET\">GET</a></li>\n" +
                "                    <li><a class=\"dropdown-item hover unselectable\" id=\"POST\">POST</a></li>\n" +
                "                    <li><a class=\"dropdown-item hover unselectable\" id=\"PUT\">PUT</a></li>\n" +
                "                    <li><a class=\"dropdown-item hover unselectable\" id=\"DELETE\">DELETE</a></li>\n" +
                "                </ul>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"row\">\n" +
                "            <label for=\"apikey\">Session Key</label>\n" +
                "            <input type=\"text\" class=\"form-control\" id=\"apikey\">\n" +
                "        </div>\n" +
                "        <button type=\"button\" class=\"btn btn-primary unselectable\" id=\"send\">Send</button>\n" +
                "    </form>\n" +
                "</section>\n" +
                "\n" +
                "<br><br>\n" +
                "\n" +
                "<section id=\"response\" class=\"container\"></section>\n" +
                "\n" +
                "<script>\n" +
                "    const url = new URL(window.location.href);\n" +
                "    const startUrl = url.protocol + \"//\" + url.hostname + \":\" + url.port + \"/\";\n" +
                "    document.getElementById(\"url_start\").innerHTML = startUrl;\n" +
                "\n" +
                "    let method = \"GET\";\n" +
                "\n" +
                "    const dropdowns = document.getElementsByClassName(\"dropdown-item\");\n" +
                "    for (let i = 0; i < dropdowns.length; i++) {\n" +
                "        const dropdown = dropdowns[i];\n" +
                "        dropdown.onclick = function () {\n" +
                "            method = dropdown.id;\n" +
                "            document.getElementById(\"method\").value = method;\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    document.getElementById(\"send\").onclick = async function () {\n" +
                "        const url = startUrl + document.getElementById(\"url\").value;\n" +
                "        const sessionKey = document.getElementById(\"apikey\").value;\n" +
                "        const response = await (await fetch(url, {\n" +
                "            method: method,\n" +
                "            mode: 'cors',\n" +
                "            cache: 'no-cache',\n" +
                "            credentials: 'same-origin',\n" +
                "            headers: {\n" +
                "                'Content-Type': 'application/json',\n" +
                "                'Authorization': sessionKey\n" +
                "            },\n" +
                "            redirect: 'follow',\n" +
                "            referrerPolicy: 'no-referrer',\n" +
                "        })).text();\n" +
                "        document.getElementById(\"response\").innerHtml = response;\n" +
                "    }\n" +
                "</script>\n" +
                "\n" +
                "</body>\n" +
                "</html>").getBytes();
    }
    public static byte[] getErrorMessage() {
        return  "ERROR: API-Route does not exsist".getBytes();
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String path = exchange.getRequestURI().getPath();
        if (path.equalsIgnoreCase("/api/tester")) {
            byte[] response = getApiTester();
            exchange.sendResponseHeaders(200, response.length);
            OutputStream out = exchange.getResponseBody();
            out.write(response);
            out.close();
        } else if (apis.containsKey(path)) {
            Method api = apis.get(path);
            String method = api.isAnnotationPresent(SetMethod.class) ? api.getAnnotation(SetMethod.class).value().toString() : RequestMethod.GET.toString();
            if (!exchange.getRequestMethod().equals(method)) {
                byte[] response = getErrorMessage();
                exchange.sendResponseHeaders(200, response.length);
                OutputStream out = exchange.getResponseBody();
                out.write(response);
                out.close();
                return;
            }

            boolean authorized = api.isAnnotationPresent(Authorized.class);
            if (authorized) {
                Authorized auth = api.getAnnotation(Authorized.class);
                String sessionKey = exchange.getRequestHeaders().getFirst("Authorization");
                if (DBManager.validateSession(sessionKey) && DBManager.validateForPermission(sessionKey, auth.permission())) {
                    try {
                        api.invoke(apiObjects.get(api), exchange);
                    } catch (Exception e) { e.printStackTrace(); }
                }else {
                    exchange.sendResponseHeaders(401, -1);
                }
            }else {
                try {
                    api.invoke(apiObjects.get(api), exchange);
                }catch (Exception e) { e.printStackTrace(); }
            }
        }else {
            byte[] response = getErrorMessage();
            exchange.sendResponseHeaders(200, response.length);
            OutputStream out = exchange.getResponseBody();
            out.write(response);
            out.close();
        }
    }

    public static byte[] getBytesFromStream(InputStream in) {
        try {
            if (in == null) throw new NullPointerException("InputStream cannot be null");
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = in.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            return buffer.toByteArray();
        }catch (Exception e) { e.printStackTrace(); }
        return new byte[0];
    }

    public static List<String> getFileLines(File file) {
        try {
            return Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        }catch (Exception e) { e.printStackTrace(); }
        return new ArrayList<>();
    }

    public void addApi(WebApi api) {
        if (!api.getClass().isAnnotationPresent(Route.class)) throw new IllegalStateException("API does not have a Route");
        String route = api.getClass().getAnnotation(Route.class).value();

        for (Method m : api.getClass().getMethods()) {
            if (m.getParameters().length == 0) continue;
            if (!m.getParameters()[0].getType().equals(HttpExchange.class)) continue;
            String r = route + (m.isAnnotationPresent(Route.class) ? m.getAnnotation(Route.class).value() : "");
            apis.put(r, m);
            apiObjects.put(m, api);
        }
    }
}
