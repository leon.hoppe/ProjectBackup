package de.craftix.cloudnetwebinterface.webserver.routes;

import com.sun.net.httpserver.HttpExchange;
import de.craftix.cloudnetwebinterface.webserver.WebServerManager;
import de.craftix.cloudnetwebinterface.webserver.api.*;
import de.dytanic.cloudnet.CloudNet;

import java.io.File;
import java.util.List;

@Route("/api/console")
public class ConsoleAPI extends WebApi {
    public static final int LOG_LENGTH = 500;

    @Authorized
    public void getConsole(HttpExchange exchange) {
        String logFolder = "local/logs/";
        int logID = 0;
        while (true) {
            File log = new File(logFolder + "cloudnet." + logID + ".log");
            if (!log.exists()) {
                logID--;
                break;
            }
            logID++;
        }

        File log = new File(logFolder + "cloudnet." + logID + ".log");
        List<String> lines = WebServerManager.getFileLines(log);
        if (lines.size() > LOG_LENGTH) lines = lines.subList(lines.size() - LOG_LENGTH, lines.size());
        StringBuilder out = new StringBuilder();
        lines.forEach((line) -> out.append("\n").append(line));
        sendData(exchange, out.toString().getBytes());
    }

    @Authorized(permission = "web.command.main")
    @Route("/send")
    @SetMethod(RequestMethod.POST)
    public void sendConsoleCommand(HttpExchange exchange) {
        String cmd = new HttpQuery(exchange).getVariable("command");
        CloudNet.getInstance().getCommandMap().dispatchCommand(CloudNet.getInstance().getConsoleCommandSender(), cmd);
        sendData(exchange, LogicResult.OK, "Success".getBytes());
    }

    @Authorized(permission = "web.reload")
    @Route("/reload")
    public void reload(HttpExchange exchange) {
        sendData(exchange, "Success".getBytes());
        CloudNet.getInstance().reload();
    }

    @Authorized(permission = "web.stop")
    @Route("/stop")
    public void stop(HttpExchange exchange) {
        sendData(exchange, "Success".getBytes());
        CloudNet.getInstance().stop();
    }

}
