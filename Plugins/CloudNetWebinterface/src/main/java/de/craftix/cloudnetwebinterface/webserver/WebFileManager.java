package de.craftix.cloudnetwebinterface.webserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

public class WebFileManager implements HttpHandler {
    public WebFileManager(WebServerManager manager) {
        manager.getServer().createContext("/", this);
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String path = exchange.getRequestURI().getPath();
        byte[] response = getFileContent(path);

        exchange.sendResponseHeaders(200, response.length);
        OutputStream out = exchange.getResponseBody();
        out.write(response);
        out.close();
    }

    private byte[] getFileContent(String fileName) {
        try {
            File file = new File("modules/Webinterface/htdocs/" + fileName + "/");
            if (file.isDirectory()) file = new File("modules/Webinterface/htdocs/" + fileName + "/index.html");
            return Files.readAllBytes(file.toPath());
        }catch (Exception e) {
            System.err.println("Client tried to access invalid path: " + fileName);
        }
        return getFileContent("index.html");
    }
}
