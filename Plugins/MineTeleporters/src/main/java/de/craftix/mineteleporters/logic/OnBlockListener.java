package de.craftix.mineteleporters.logic;

import de.craftix.mineteleporters.MineTeleporters;
import de.craftix.mineteleporters.database.DBHandler;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Arrays;
import java.util.UUID;

public class OnBlockListener implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!event.getPlayer().getInventory().getItemInMainHand().getItemMeta().hasLore()) return;
        if (!event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore().containsAll(Arrays.asList(MineTeleporters.getLore()))) return;

        String name = event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName();
        UUID teleporter = DBHandler.saveLocation(name, event.getBlockPlaced());
        DBHandler.addPlayer(event.getPlayer(), teleporter);
    }

    @EventHandler
    public void onBlockDestroy(BlockBreakEvent event) {
        if (!event.getBlock().getType().equals(Material.EMERALD_BLOCK)) return;
        DBHandler.delLocation(event.getBlock());
    }

    @EventHandler
    public void onInteractPortableTeleporter(PlayerInteractEvent event) {
        if (event.getPlayer().getInventory().getItemInMainHand().getItemMeta() == null) return;
        if (!event.getPlayer().getInventory().getItemInMainHand().getItemMeta().hasLore()) return;
        if (!event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore().containsAll(Arrays.asList(MineTeleporters.getLore())) ||
            !event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.MAGMA_CREAM)) return;

        Menu.openMenu(event.getPlayer());
    }

    @EventHandler
    public void onBlockClick(PlayerInteractEvent event) {
        if (!event.hasBlock() || event.getClickedBlock() == null) return;
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (event.getClickedBlock().getType() != Material.EMERALD_BLOCK) return;

        TeleportLocation location = DBHandler.getLocation(event.getClickedBlock());
        if (location == null) return;
        boolean isNew = DBHandler.addPlayer(event.getPlayer(), location.uuid);
        if (isNew) event.getPlayer().sendMessage("§aDu kannst dich nun zu diesem Teleporter teleportieren!");
    }

}
