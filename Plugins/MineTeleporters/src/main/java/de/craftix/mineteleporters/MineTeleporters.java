package de.craftix.mineteleporters;

import de.craftix.mineteleporters.crafting.RecipeLoader;
import de.craftix.mineteleporters.database.DBHandler;
import de.craftix.mineteleporters.logic.Menu;
import de.craftix.mineteleporters.logic.OnBlockListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class MineTeleporters extends JavaPlugin {
    private static MineTeleporters instance;
    private static String[] lore;

    @Override
    public void onEnable() {
        // Plugin startup logic
        instance = this;
        lore = new String[] {"§7Teleportiere dich überall hin"};

        RecipeLoader.loadRecipes();
        DBHandler.initialize();

        Bukkit.getPluginManager().registerEvents(new OnBlockListener(), this);
        Bukkit.getPluginManager().registerEvents(new Menu(), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        DBHandler.disconnect();
    }

    public static MineTeleporters getInstance() { return instance; }
    public static String[] getLore() { return lore; }
}
