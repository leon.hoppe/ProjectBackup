package de.craftix.mineteleporters.logic;

import de.craftix.mineteleporters.database.DBHandler;
import de.craftix.mineteleporters.items.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class Menu implements Listener {
    private static final String TITLE = "§6Teleporters";

    public static void openMenu(Player player) {
        Inventory inv = Bukkit.createInventory(null, 6 * 9, TITLE);

        for (int i = 0; i < 9; i++) {
            inv.setItem(i, ItemCreator.createItem(Material.GRAY_STAINED_GLASS_PANE, " "));
        }
        for (int i = 9*5; i < inv.getSize(); i++) {
            inv.setItem(i, ItemCreator.createItem(Material.GRAY_STAINED_GLASS_PANE, " "));
        }

        TeleportLocation[] locations = DBHandler.getLocations(player);
        int index = 9;
        for (TeleportLocation loc : locations) {
            ItemStack item = ItemCreator.createItem(Material.EMERALD_BLOCK, "§r" + loc.name.replaceAll("&", "§"), "§7Click to teleport", "", "§7" + loc.uuid.toString());
            inv.setItem(index, item);
            index++;
            if (index >= 5*9) break;
        }

        player.openInventory(inv);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null || !event.getView().getTitle().equals(TITLE) || event.getCurrentItem() == null) return;
        if (event.getClickedInventory() == event.getView().getBottomInventory()) return;
        event.setCancelled(true);
        if (event.getSlot() < 9 || event.getSlot() > 9*5) return;
        Player p = (Player) event.getWhoClicked();
        TeleportLocation location = DBHandler.getLocation(UUID.fromString(event.getCurrentItem().getLore().get(2).replace("§7", "")));
        if (location == null) return;
        location.location.setY(location.location.getY() + 2);
        location.location.setX(location.location.getX() + 0.5);
        location.location.setZ(location.location.getZ() + 0.5);
        p.teleport(location.location);
    }

}
