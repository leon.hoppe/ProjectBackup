package de.craftix.mineteleporters.logic;

import org.bukkit.Location;

import java.util.UUID;

public class TeleportLocation {

    public final String name;
    public final Location location;
    public final UUID uuid;

    public TeleportLocation(String name, Location location, UUID uuid) {
        this.uuid = uuid;
        this.name = name;
        this.location = location;
    }

}
