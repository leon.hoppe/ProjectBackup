package de.craftix.mineteleporters.database;

import de.craftix.mineteleporters.logic.TeleportLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class DBHandler {
    private static SQLite sqLite;

    public static void initialize() {
        sqLite = new SQLite("plugins/MineTeleporters/data.db");
        sqLite.connect();

        sqLite.insert("CREATE TABLE IF NOT EXISTS Data (Name VARCHAR(100), X INT(100), Y INT(100), Z INT(100), Direction VARCHAR(100), World VARCHAR(100), UUID VARCHAR(100))");
        sqLite.insert("CREATE TABLE IF NOT EXISTS Userdata (UUID VARCHAR(100), Teleporter VARCHAR(100))");
    }

    public static void disconnect() {
        sqLite.disconnect();
    }

    public static UUID saveLocation(String name, Block block) {
        int x = block.getLocation().getBlockX();
        int y = block.getLocation().getBlockY();
        int z = block.getLocation().getBlockZ();
        float dir = (float) Math.atan2(block.getLocation().getDirection().getZ(), block.getLocation().getDirection().getX());
        String world = block.getWorld().getName();
        UUID uuid = UUID.randomUUID();
        sqLite.insert("INSERT INTO Data VALUES (\"" + name + "\", " + x + ", " + y + ", " + z + ", \"" + dir + "\", \"" + world + "\", \"" + uuid + "\")");
        return uuid;
    }

    public static void delLocation(Block block) {
        TeleportLocation teleportLocation = getLocation(block);
        if (teleportLocation == null) return;
        sqLite.insert("DELETE FROM Data WHERE UUID = \"" + teleportLocation.uuid + "\"");
        sqLite.insert("DELETE FROM Userdata WHERE Teleporter = \"" + teleportLocation.uuid + "\"");
    }

    public static TeleportLocation getLocation(Block block) {
        int x = block.getLocation().getBlockX();
        int y = block.getLocation().getBlockY();
        int z = block.getLocation().getBlockZ();
        try {
            ResultSet rs = sqLite.getData("SELECT * FROM Data WHERE X = " + x + " AND Y = " + y + " AND Z = " + z);
            if (rs.next()) {
                float yaw = Float.parseFloat(rs.getString("Direction"));
                World world = Bukkit.getWorld(rs.getString("World"));
                UUID uuid = UUID.fromString(rs.getString("UUID"));

                return new TeleportLocation(rs.getString("Name"), new Location(world, x, y, z, yaw, 0), uuid);
            }
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    public static TeleportLocation[] getLocations(Player p) {
        ArrayList<TeleportLocation> locations = new ArrayList<>();
        List<UUID> teleporters = Arrays.asList(getPlayerTeleporters(p));
        try {
            ResultSet rs = sqLite.getData("SELECT * FROM Data");
            while (rs.next()) {
                int x = rs.getInt("X");
                int y = rs.getInt("Y");
                int z = rs.getInt("Z");
                float yaw = Float.parseFloat(rs.getString("Direction"));
                World world = Bukkit.getWorld(rs.getString("World"));
                UUID uuid = UUID.fromString(rs.getString("UUID"));

                if (teleporters.contains(uuid))
                    locations.add(new TeleportLocation(rs.getString("Name"), new Location(world, x, y, z, yaw, 0), uuid));
            }
        }catch (Exception e) { e.printStackTrace(); }
        return locations.toArray(new TeleportLocation[0]);
    }

    public static TeleportLocation getLocation(UUID uuid) {
        try {
            ResultSet rs = sqLite.getData("SELECT * FROM Data WHERE UUID = \"" + uuid + "\"");
            if (rs.next()) {
                int x = rs.getInt("X");
                int y = rs.getInt("Y");
                int z = rs.getInt("Z");
                float yaw = Float.parseFloat(rs.getString("Direction"));
                World world = Bukkit.getWorld(rs.getString("World"));

                return new TeleportLocation(rs.getString("Name"), new Location(world, x, y, z, yaw, 0), uuid);
            }
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    public static boolean addPlayer(Player p, UUID teleporter) {
        boolean isNew = true;
        List<TeleportLocation> locations = Arrays.asList(getLocations(p));
        for (TeleportLocation location : locations) {
            if (location.uuid.equals(teleporter)) {
                isNew = false;
                break;
            }
        }

        sqLite.insert("DELETE FROM Userdata WHERE UUID = \"" + p.getUniqueId() + "\" AND Teleporter = \"" + teleporter + "\"");
        sqLite.insert("INSERT INTO Userdata VALUES (\"" + p.getUniqueId() + "\", \"" + teleporter + "\")");
        return isNew;
    }

    public static UUID[] getPlayerTeleporters(Player p) {
        ArrayList<UUID> teleporters = new ArrayList<>();
        try {
            ResultSet rs = sqLite.getData("SELECT Teleporter FROM Userdata WHERE UUID = \"" + p.getUniqueId() + "\"");
            while (rs.next()) {
                teleporters.add(UUID.fromString(rs.getString("Teleporter")));
            }
        }catch (Exception e) { e.printStackTrace(); }
        return teleporters.toArray(new UUID[0]);
    }

}
