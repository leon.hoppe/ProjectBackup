package de.craftix.mineteleporters.crafting;

import de.craftix.mineteleporters.MineTeleporters;
import de.craftix.mineteleporters.items.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ShapedRecipe;

public class RecipeLoader {

    public static void loadRecipes() {
        ShapedRecipe teleporter = new ShapedRecipe(new NamespacedKey(MineTeleporters.getInstance(), "teleporter"), ItemCreator.createTeleporter());
        teleporter.shape("EEE", "EPE", "EEE");
        teleporter.setIngredient('E', Material.EMERALD_BLOCK);
        teleporter.setIngredient('P', Material.ENDER_PEARL);

        ShapedRecipe portableTeleporter = new ShapedRecipe(new NamespacedKey(MineTeleporters.getInstance(), "portable_teleporter"), ItemCreator.createPortableTeleporter());
        portableTeleporter.shape("DDD", "DPD", "DDD");
        portableTeleporter.setIngredient('D', Material.DIAMOND_BLOCK);
        portableTeleporter.setIngredient('P', Material.ENDER_PEARL);

        Bukkit.addRecipe(teleporter);
        Bukkit.addRecipe(portableTeleporter);
    }

}
