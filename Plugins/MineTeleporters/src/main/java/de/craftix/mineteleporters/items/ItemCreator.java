package de.craftix.mineteleporters.items;

import de.craftix.mineteleporters.MineTeleporters;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class ItemCreator {

    public static ItemStack createItem(Material material, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material, String name, boolean glow, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();

        if (glow) {
            meta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }

        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createTeleporter() {
        return createTeleporter("§rTeleporter");
    }

    public static ItemStack createTeleporter(String name) {
        return ItemCreator.createItem(Material.EMERALD_BLOCK, name, MineTeleporters.getLore());
    }

    public static ItemStack createPortableTeleporter() {
        return ItemCreator.createItem(Material.MAGMA_CREAM, "§rPortable Teleporter", true, MineTeleporters.getLore());
    }

}
