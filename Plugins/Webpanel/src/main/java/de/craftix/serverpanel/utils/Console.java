package de.craftix.serverpanel.utils;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.craftix.serverpanel.Serverpanel;

import java.io.IOException;

public class Console implements HttpHandler {

    public static String getLog() { return Serverpanel.getLogContents(); }

    public static void sendCommand(String command) { Serverpanel.log.add(command); }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if (exchange.getRequestURI().getPath().equals("/console_send")) {
            sendCommand(exchange.getRequestURI().getQuery());
        }
    }
}
