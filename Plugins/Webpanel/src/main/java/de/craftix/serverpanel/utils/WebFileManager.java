package de.craftix.serverpanel.utils;

import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.craftix.serverpanel.Serverpanel;

import java.io.IOException;
import java.io.OutputStream;

public class WebFileManager implements HttpHandler {
    private BasicAuthenticator authenticator;

    public WebFileManager() {
        load();
        authenticator = new BasicAuthenticator("get") {
            @Override
            public boolean checkCredentials(String username, String password) {
                return username.equals("leon.hoppe") && password.equals("1234567890");
            }
        };
    }

    public void load() {
        Serverpanel.getWebServerManager().getServer().createContext("/", this).setAuthenticator(authenticator);
        Serverpanel.getWebServerManager().getServer().createContext("/console_send", new Console()).setAuthenticator(authenticator);
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String path = exchange.getRequestURI().getPath();
        byte[] response = Serverpanel.getFileContent(exchange.getRequestURI().getPath());
        if (!path.endsWith(".png")) {
            String out = new String(response);
            out = out.replace("%console_view%", Console.getLog());
            response = out.getBytes();
        }
        exchange.sendResponseHeaders(200, response.length);
        OutputStream out = exchange.getResponseBody();
        out.write(response);
        out.close();
    }
}
