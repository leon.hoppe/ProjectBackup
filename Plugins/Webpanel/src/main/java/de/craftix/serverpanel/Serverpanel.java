package de.craftix.serverpanel;

import de.craftix.serverpanel.utils.WebFileManager;
import de.craftix.serverpanel.utils.WebServerManager;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public final class Serverpanel {
    public static void main(String[] args) {
        new Serverpanel().onEnable();
    }

    private static Serverpanel instance;
    private static WebServerManager webServerManager;
    private static WebFileManager webFileManager;

    public static List<String> log = new ArrayList<>();

    public void onEnable() {
        instance = this;
        webServerManager = new WebServerManager(40);
        webFileManager = new WebFileManager();
        webServerManager.getServer().start();
        log.add("Server started successfully");
    }

    public static byte[] getFileContent(String fileName) {
        try {
            return Files.readAllBytes(new File("src/main/resources/" + fileName).toPath());
        }catch (Exception e) {
            System.err.println("Client tried to access invalid path: " + fileName);
        }
        return getFileContent("index.html");
    }

    public static String getLogContents() {
        StringBuilder out = new StringBuilder();
        for (String s : log) {
            out.insert(0, s + "<br>");
        }
        return out.toString();
    }

    public static Serverpanel getInstance() { return instance; }

    public static WebServerManager getWebServerManager() { return webServerManager; }

    public static WebFileManager getWebFileManager() { return webFileManager; }
}
