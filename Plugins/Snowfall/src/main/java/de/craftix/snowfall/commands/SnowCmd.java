package de.craftix.snowfall.commands;

import com.comphenix.protocol.PacketType;
import de.craftix.snowfall.utils.FlakesHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SnowCmd implements CommandExecutor, Listener {
    private static final int INV_SIZE = 9*5;
    private static final String INV_NAME = "Scheepartikel - Einstellungen";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) sender.sendMessage("§cDieser Command kann nur als spieler ausgeführt werden!");
        else openGui((Player) sender);
        return true;
    }

    private void openGui(Player player) {
        Inventory inv = Bukkit.createInventory(null, INV_SIZE, INV_NAME);

        //Placeholders
        ItemStack placeholder = createItem(Material.GRAY_STAINED_GLASS_PANE, " ");
        for (int i = 0; i < INV_SIZE; i++) {
            inv.setItem(i, placeholder);
        }

        ItemStack enable = createItem(Material.GREEN_STAINED_GLASS_PANE, "§aScheeflocken einschalten");
        ItemStack disable = createItem(Material.RED_STAINED_GLASS_PANE, "§cScheeflocken ausschalten");

        ItemStack always = createItem(Material.CLOCK, "§aModus: " + FlakesHandler.SnowModes.ALWAYS.getName());
        ItemStack onlyClear = createItem(Material.SUNFLOWER, "§aModus: " + FlakesHandler.SnowModes.ONLY_CLEAR_WEATHER.getName());
        ItemStack snowBiome = createItem(Material.SNOWBALL, "§aModus: " + FlakesHandler.SnowModes.ONLY_NIGHT_OR_SNOWBIOME.getName());

        inv.setItem(12, enable);
        inv.setItem(14, disable);

        inv.setItem(29, always);
        inv.setItem(31, onlyClear);
        inv.setItem(33, snowBiome);

        player.openInventory(inv);
    }

    private ItemStack createItem(Material material, String name) {
        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        item.setItemMeta(itemMeta);
        return item;
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null || !event.getView().getTitle().equals(INV_NAME)) return;
        event.setCancelled(true);
        Player player = (Player) event.getWhoClicked();

        switch (event.getCurrentItem().getType()) {
            case GREEN_STAINED_GLASS_PANE:
                player.performCommand("togglesnow true");
                break;

            case RED_STAINED_GLASS_PANE:
                player.performCommand("togglesnow false");
                break;

            case CLOCK:
                player.performCommand("setsnowmode ALWAYS");
                break;

            case SUNFLOWER:
                player.performCommand("setsnowmode ONLY_CLEAR_WEATHER");
                break;

            case SNOWBALL:
                player.performCommand("setsnowmode ONLY_NIGHT_OR_SNOWBIOME");
        }
    }
}
