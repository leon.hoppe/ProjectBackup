package de.craftix.snowfall.utils;

import java.sql.ResultSet;
import java.util.UUID;

public class DBHandler {
    private SQLite sqLite;

    public DBHandler() {
        sqLite = new SQLite("./plugins/Snowfall/data.db");
        sqLite.insert("CREATE TABLE IF NOT EXISTS Userdata (UUID STRING, Enabled BOOLEAN, Mode INTEGER)");
    }

    public boolean showSnow(UUID uuid) {
        try {
            ResultSet rs = sqLite.getData("SELECT * FROM Userdata WHERE UUID = \"" + uuid + "\"");
            if (rs.next()) {
                return rs.getBoolean("Enabled");
            }
        }catch (Exception e) { e.printStackTrace(); }
        return true;
    }

    public void setShowSnow(UUID uuid, boolean value) {
        try {
            int mode = getMode(uuid);
            sqLite.insert("DELETE FROM Userdata WHERE UUID = \"" + uuid + "\"");
            sqLite.insert("INSERT INTO Userdata VALUES (\"" + uuid + "\", " + value + ", " + mode + ")");
        }catch (Exception e) { e.printStackTrace(); }
    }

    public int getMode(UUID uuid) {
        try {
            ResultSet rs = sqLite.getData("SELECT * FROM Userdata WHERE UUID = \"" + uuid + "\"");
            if (rs.next()) {
                return rs.getInt("Mode");
            }
        }catch (Exception e) { e.printStackTrace(); }
        return 0;
    }

    public void setMode(UUID uuid, int mode) {
        try {
            boolean enabled = showSnow(uuid);
            sqLite.insert("DELETE FROM Userdata WHERE UUID = \"" + uuid + "\"");
            sqLite.insert("INSERT INTO Userdata VALUES (\"" + uuid + "\", " + enabled + ", " + mode + ")");
        }catch (Exception e) { e.printStackTrace(); }
    }

    public void deactivate() {
        sqLite.disconnect();
    }
}
