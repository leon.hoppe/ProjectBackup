package de.craftix.snowfall.utils;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import de.craftix.snowfall.Snowfall;
import net.minecraft.network.protocol.game.ClientboundLevelChunkPacketData;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

public class PacketListener extends PacketAdapter {

    public PacketListener() {
        super(Snowfall.getPlugin(), PacketType.Play.Server.MAP_CHUNK);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        PacketContainer packet = event.getPacket();
        PacketType type = event.getPacketType();
        Player player = event.getPlayer();

        if (type == PacketType.Play.Server.MAP_CHUNK && player.getWorld().getEnvironment() == World.Environment.NORMAL) {
            modifyChunkPacket(player, packet, this::handle);
        }
    }

    private void handle(BiomeBridge biomeBridge) {
        World world = biomeBridge.getPlayer().getWorld();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = -64; y < 320; y++) {
                    biomeBridge.setBiome(x, y, z);
                }
            }
        }
    }

    private void modifyChunkPacket(Player player, PacketContainer packet, Consumer<BiomeBridge> biomeBridgeConsumer) {
        int chunkX = packet.getIntegers().readSafely(0);
        int chunkZ = packet.getIntegers().readSafely(1);
        ClientboundLevelChunkPacketData data = packet.getSpecificModifier(ClientboundLevelChunkPacketData.class).readSafely(0);

        biomeBridgeConsumer.accept(new BiomeBridge() {
            @Override
            public int getChunkX() {
                return chunkX;
            }

            @Override
            public int getChunkZ() {
                return chunkZ;
            }

            @Override
            public Player getPlayer() {
                return player;
            }

            @Override
            public void setBiome(int x, int z) {
                throw new UnsupportedOperationException("Not support 2D Biome Operations in this version!");
            }

            @Override
            public void setBiome(int x, int y, int z) {
                //TODO: Modify packet
            }
        });
    }

    private static class Maths {
        private static final int e = (int)Math.round(Math.log(16.0D) / Math.log(2.0D)) - 2;
        private static final int f = (int)Math.round(Math.log(256.0D) / Math.log(2.0D)) - 2;
        public static final int a;
        public static final int b;
        public static final int c;
        static {
            a = 1 << e + e + f;
            b = (1 << e) - 1;
            c = (1 << f) - 1;
        }

        public static int biomeIndex(int i, int j, int k){
            int l = i & b;
            int i1 = clamp(j, 0, c);
            int j1 = k & b;
            return i1 << e + e | j1 << e | l;
        }

        private static int clamp(int var0, int var1, int var2) {
            if (var0 < var1) {
                return var1;
            } else {
                return Math.min(var0, var2);
            }
        }
    }

    private interface BiomeBridge {
        int getChunkX();
        int getChunkZ();
        Player getPlayer();

        void setBiome(int x, int z);
        void setBiome(int x, int y, int z);
    }

}
