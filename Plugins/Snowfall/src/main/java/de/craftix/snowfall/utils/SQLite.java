package de.craftix.snowfall.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class SQLite {

    protected String path;
    protected Connection con;

    public SQLite(String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.path = file.getAbsolutePath();
        connect();
    }

    public void connect() {
        if (isConnected()) return;
        try {
            con = DriverManager.getConnection("jdbc:sqlite:" + path);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if (!isConnected()) return;
        try {
            con.close();
            con = null;
        }catch (Exception e) { e.printStackTrace(); }
    }

    public boolean isConnected() { return con != null; }

    public void insert(String qry) {
        if (!isConnected()) throw new NullPointerException("SQLite not connected");
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (Exception e) { e.printStackTrace(); }
    }

    public ResultSet getData(String qry) {
        if (!isConnected()) throw new NullPointerException("SQLite not connected");
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

}
