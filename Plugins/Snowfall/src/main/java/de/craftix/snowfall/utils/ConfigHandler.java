package de.craftix.snowfall.utils;

import de.craftix.snowfall.Snowfall;

import java.io.File;
import java.util.List;

public class ConfigHandler {

    public ConfigHandler() {
        this.load();
    }

    private void load() {
        if (!Snowfall.getPlugin().getDataFolder().exists()) {
            Snowfall.getPlugin().getDataFolder().mkdir();
        }

        if (!(new File(Snowfall.getPlugin().getDataFolder(), "config.yml")).exists()) {
            Snowfall.getPlugin().saveDefaultConfig();
        }

    }

    public boolean isRealistic() {
        return Snowfall.getPlugin().getConfig().getBoolean("realistic");
    }

    public int getAmount() {
        return Snowfall.getPlugin().getConfig().getInt("amount");
    }

    public int getRadius() {
        return Snowfall.getPlugin().getConfig().getInt("radius");
    }

    public List<String> getAllowedWorlds() {
        return Snowfall.getPlugin().getConfig().getStringList("worlds");
    }

    public List<String> getForbiddenBiomes() {
        return Snowfall.getPlugin().getConfig().getStringList("biomes");
    }
}
