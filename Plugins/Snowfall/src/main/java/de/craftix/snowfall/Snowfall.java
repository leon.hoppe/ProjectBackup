package de.craftix.snowfall;

import com.comphenix.protocol.ProtocolLibrary;
import de.craftix.snowfall.commands.*;
import de.craftix.snowfall.utils.DBHandler;
import de.craftix.snowfall.utils.PacketListener;
import de.craftix.snowfall.utils.StarterpackHandler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Snowfall extends JavaPlugin {
    private static Snowfall plugin;
    private DBHandler handler;
    private TabCompletion tabCompletion;

    public void onEnable() {
        plugin = this;
        handler = new DBHandler();
        tabCompletion = new TabCompletion();
        getCommand("togglesnow").setExecutor(new ToggleSnowCmd(handler));
        getCommand("togglesnow").setTabCompleter(tabCompletion);
        getCommand("setsnowmode").setExecutor(new SetSnowModeCmd(handler));
        getCommand("setsnowmode").setTabCompleter(tabCompletion);
        new StarterpackHandler(handler);

        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketListener());

        SnowCmd snowCmd = new SnowCmd();
        getCommand("snow").setExecutor(snowCmd);
        Bukkit.getPluginManager().registerEvents(snowCmd, this);
    }

    @Override
    public void onDisable() {
        handler.deactivate();
        ProtocolLibrary.getProtocolManager().removePacketListeners(this);
    }

    public static Snowfall getPlugin() {
        return plugin;
    }

}
