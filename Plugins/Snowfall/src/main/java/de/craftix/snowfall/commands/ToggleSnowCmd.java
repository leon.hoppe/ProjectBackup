package de.craftix.snowfall.commands;

import de.craftix.snowfall.utils.DBHandler;
import de.craftix.snowfall.utils.FlakesHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleSnowCmd implements CommandExecutor {
    private DBHandler handler;

    public ToggleSnowCmd(DBHandler handler) {
        this.handler = handler;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) sender.sendMessage("§cDieser Command kann nur als spieler ausgeführt werden!");
        else if (args.length > 1) return false;
        else {
            try {
                Player p = (Player) sender;
                boolean showSnow = handler.showSnow(p.getUniqueId());
                if (args.length == 0) {
                    showSnow = !showSnow;
                }else {
                    showSnow = Boolean.parseBoolean(args[0]);
                }
                handler.setShowSnow(p.getUniqueId(), showSnow);

                FlakesHandler.refreshPlayerData();

                String message = "§7Schneeflocken " + (showSnow ? "§aeingeschaltet" : "§causgeschaltet");
                p.sendMessage(message);
            }catch (Exception ignored) {}
        }
        return true;
    }
}
