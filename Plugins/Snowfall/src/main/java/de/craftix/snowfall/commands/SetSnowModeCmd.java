package de.craftix.snowfall.commands;

import de.craftix.snowfall.utils.DBHandler;
import de.craftix.snowfall.utils.FlakesHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSnowModeCmd implements CommandExecutor {
    private final DBHandler handler;

    public SetSnowModeCmd(DBHandler handler) { this.handler = handler; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) sender.sendMessage("§cDieser Command kann nur als spieler ausgeführt werden!");
        else if (args.length > 1) return false;
        else {
            try {
                Player p = (Player) sender;

                if (args.length == 0) {
                    p.sendMessage("§7Aktueller Modus: '§a" + FlakesHandler.SnowModes.getByMode(handler.getMode(p.getUniqueId())).getName() + "§7'");
                    return true;
                }

                FlakesHandler.SnowModes mode = FlakesHandler.getSnowModeByName(args[0]);

                if (mode != null) {
                    handler.setMode(p.getUniqueId(), mode.getMode());
                    FlakesHandler.refreshPlayerData();
                    p.sendMessage("§7Du hast den Modus '§a" + mode.getName() + "§7' aktiviert");
                }else {
                    p.sendMessage("§cDieser Modus existiert nicht!");
                    return true;
                }
            }catch (Exception ignored) {}
        }
        return true;
    }
}
