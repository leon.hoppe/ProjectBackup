package de.craftix.snowfall.utils;

import de.craftix.snowfall.Snowfall;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class FlakesHandler {
    private int version = Integer.valueOf(Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3].split("_")[1]);
    private ConfigHandler config;
    float radius;
    int amount;
    boolean realistic;
    List<String> worlds;
    List<String> biomes;
    Random random;

    private static DBHandler handler;
    private static final Map<UUID, Boolean> showFlakes = new HashMap<>();

    public FlakesHandler(ConfigHandler config, DBHandler handler) {
        this.config = config;
        this.radius = (float)config.getRadius();
        this.amount = config.getAmount() / 4;
        this.realistic = config.isRealistic();
        this.worlds = config.getAllowedWorlds();
        this.biomes = config.getForbiddenBiomes();
        this.handler = handler;
        this.createFlakes();
    }

    private void createFlakes() {
        this.random = new Random();
        (new BukkitRunnable() {
            public void run() {
                refreshPlayerData();
            }
        }).runTaskTimer(Snowfall.getPlugin(), 0L, 100L);

        (new BukkitRunnable() {
            public void run() {
                for(int i = 0; i < FlakesHandler.this.amount; ++i) {
                    float xAdditive = (FlakesHandler.this.random.nextFloat() - 0.5F) * FlakesHandler.this.radius * 2.0F;
                    float max = (float)Math.sqrt((double)(FlakesHandler.this.radius * FlakesHandler.this.radius - xAdditive * xAdditive)) * 2.0F;
                    float yAdditive = (FlakesHandler.this.random.nextFloat() - 0.5F) * max;
                    float zAdditive = (FlakesHandler.this.random.nextFloat() - 0.5F) * max;
                    Bukkit.getOnlinePlayers().forEach((player) -> {
                        if (!showFlakes.containsKey(player.getUniqueId()) || !showFlakes.get(player.getUniqueId())) return;
                        Location playerLoc = player.getLocation();
                        if (FlakesHandler.this.worlds.contains(playerLoc.getWorld().getName()) && !FlakesHandler.this.biomes.contains(playerLoc.getBlock().getBiome().toString())) {
                            Location loc = new Location(player.getWorld(), playerLoc.getX() + (double)xAdditive, playerLoc.getY() + (double)yAdditive, playerLoc.getZ() + (double)zAdditive);
                            if (!FlakesHandler.this.realistic || (double)loc.getWorld().getHighestBlockYAt(loc) < loc.getY()) {
                                try {
                                    if (13 <= FlakesHandler.this.version) {
                                        FlakesHandler.this.sendParticles(player, loc.getX(), loc.getY(), loc.getZ(), 0.0D, 10);
                                    } else {
                                        FlakesHandler.this.sendParticles(player, "FIREWORKS_SPARK", (float)loc.getX(), (float)loc.getY(), (float)loc.getZ(), new int[]{1});
                                    }
                                } catch (SecurityException | NoSuchMethodException | IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchFieldException | ClassNotFoundException var8) {
                                    var8.printStackTrace();
                                }
                            }
                        }

                    });
                }

            }
        }).runTaskTimer(Snowfall.getPlugin(), 0L, 2L);
    }

    public static void refreshPlayerData() {
        Bukkit.getOnlinePlayers().forEach((player -> {
            if (showFlakes.containsKey(player.getUniqueId()))
                showFlakes.remove(player.getUniqueId());
            showFlakes.put(player.getUniqueId(), handler.showSnow(player.getUniqueId()) && checkMode(player));
        }));
    }

    private Class<?> getNMSClass(String nmsClassString) throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
        String name = "net.minecraft.server." + version + nmsClassString;
        Class<?> nmsClass = Class.forName(name);
        return nmsClass;
    }

    private Class<?> getCraftPlayerClass() throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        String name = "org.bukkit.craftbukkit." + version + ".entity.CraftPlayer";
        Class<?> nmsClass = Class.forName(name.replace("/", "."));
        return nmsClass;
    }

    private Object getConnection(Player player) throws SecurityException, NoSuchMethodException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Method getHandle = player.getClass().getMethod("getHandle");
        Object nmsPlayer = getHandle.invoke(player);
        Field conField = nmsPlayer.getClass().getField("playerConnection");
        Object con = conField.get(nmsPlayer);
        return con;
    }

    public void sendParticles(Player player, double x, double y, double z, double data, int amount) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        Class<?> cpClass = this.getCraftPlayerClass();
        Class<?> particleClass = Class.forName("org.bukkit.Particle");
        Method valueOf = particleClass.getMethod("valueOf", String.class);
        Method spawnParticle = cpClass.getMethod("spawnParticle", particleClass, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE);
        spawnParticle.invoke(cpClass.cast(player), valueOf.invoke(particleClass, "FIREWORKS_SPARK"), x, y, z, amount, 0.0D, 0.0D, 0.0D, data);
    }

    public void sendParticles(Player player, String particle, float x, float y, float z, int[] amount) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        Class<?> packetClass = this.getNMSClass("PacketPlayOutWorldParticles");
        Class<?> particleClass = this.getNMSClass("EnumParticle");
        Constructor<?> packetConstructor = packetClass.getConstructors()[1];
        Method valueOf = particleClass.getMethod("valueOf", String.class);
        Object packet = packetConstructor.newInstance(valueOf.invoke(particleClass, "FIREWORKS_SPARK"), true, x, y, z, 0.0F, 0.0F, 0.0F, 0.0F, 0, amount);
        Method sendPacket = this.getNMSClass("PlayerConnection").getMethod("sendPacket", this.getNMSClass("Packet"));
        sendPacket.invoke(this.getConnection(player), packet);
    }

    private static boolean checkMode(Player player) {
        SnowModes mode = SnowModes.getByMode(handler.getMode(player.getUniqueId()));

        switch (mode) {
            case ALWAYS:
                return true;

            case ONLY_CLEAR_WEATHER:
                return player.getWorld().isClearWeather();

            case ONLY_NIGHT_OR_SNOWBIOME:
                if ((player.getWorld().getBiome(player.getLocation()).toString().contains("FROZEN") ||
                    player.getWorld().getBiome(player.getLocation()).toString().contains("SNOW") ||
                    (player.getWorld().getTime() % 24000) > 12000) && player.getWorld().isClearWeather())
                    return true;

            default:
                return false;
        }
    }

    public enum SnowModes {
        ONLY_NIGHT_OR_SNOWBIOME(0, "Nur nachts oder in Schneebiomen"),
        ONLY_CLEAR_WEATHER(1, "Nur bei klarem Wetter"),
        ALWAYS(2, "Immer");

        private final int mode;
        private final String name;
        SnowModes(int mode, String name) { this.mode = mode; this.name = name; }
        public int getMode() { return mode; }
        public String getName() { return name; }

        public static SnowModes getByMode(int modeId) {
            for (SnowModes mode : values()) {
                if (mode.getMode() == modeId)
                    return mode;
            }
            return ONLY_NIGHT_OR_SNOWBIOME;
        }
    }

    public static SnowModes getSnowModeByName(String name) {
        for (SnowModes mode : SnowModes.values()) {
            if (mode.name().equalsIgnoreCase(name))
                return mode;
        }
        return null;
    }
}
