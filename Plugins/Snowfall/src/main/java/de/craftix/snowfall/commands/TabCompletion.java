package de.craftix.snowfall.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.Arrays;
import java.util.List;

public class TabCompletion implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (command.getName().equalsIgnoreCase("togglesnow")) return Arrays.asList("true", "false");
        if (command.getName().equalsIgnoreCase("setsnowmode")) return Arrays.asList("ONLY_NIGHT_OR_SNOWBIOME", "ONLY_CLEAR_WEATHER", "ALWAYS");
        return null;
    }
}
