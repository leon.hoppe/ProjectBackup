import 'package:flutter/material.dart';

Widget superHeroDetails(String _superHeroName) {
  return Scaffold(
      appBar: AppBar(
        title: Text(_superHeroName),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(_superHeroName),
          Row(
              children: [
                Image.network('https://picsum.photos/100'),
                Image.network('https://picsum.photos/100'),
                Image.network('https://picsum.photos/100'),
              ]
          ),
          Row(
              children: [
                Image.network('https://picsum.photos/100'),
                Image.network('https://picsum.photos/100'),
                Image.network('https://picsum.photos/100'),
              ]
          ),
        ],
      )
  );
}

List<String> superheros = [
  "Aaren",
  "Aarika",
  "Abagael",
  "Abagail",
  "Abbe",
  "Abbey",
  "Abbi",
  "Abbie",
  "Abby",
  "Abbye",
  "Abigael",
  "Abigail",
  "Abigale",
  "Abra",
  "Ada",
  "Adah",
  "Adaline",
  "Adan",
  "Adara",
  "Adda",
  "Addi",
  "Addia",
  "Addie",
  "Addy",
  "Adel",
  "Adela",
  "Adelaida",
  "Adelaide",
  "Adele",
  "Adelheid",
  "Adelice",
  "Adelina",
  "Adelind",
  "Adeline",
  "Adella",
  "Adelle",
  "Adena",
  "Adey",
  "Adi",
  "Adiana",
  "Adina",
  "Adora",
  "Adore",
  "Adoree",
  "Adorne",
  "Adrea",
  "Adria",
  "Adriaens",
  "Adrian",
  "Adriana",
  "Adriane",
  "Adrianna",
  "Adrianne",
  "Adriena",
  "Adrienne",
  "Aeriel",
  "Aeriela",
  "Aeriell",
  "Afton"
];