import time
import threading

start_number = 1
end_number = 50000

start = time.time()

threads = []
primes = []

def calculatePrime(candidate_number):
    found_prime = True
    for div_number in range(2, candidate_number):
        if candidate_number % div_number == 0:
            found_prime = False
            break
    if found_prime:
        primes.append(candidate_number)

for candidate_number in range(start_number, end_number, 1):
    calculatePrime(candidate_number)
    #thread = threading.Thread(target=calculatePrime, args=(candidate_number,))
    #threads.append(thread)
    #thread.start()

#threads[threads.__len__() - 1].join()

end = round(time.time() - start, 2)

print('Find all primes up to: ' + str(end_number))
print('Time elasped: ' + str(end) + ' seconds')
print('Number of primes found ' + str(primes.__len__()))