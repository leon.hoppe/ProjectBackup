weight = float(input("Your weight: "))
unit = input("(K)g or (L)bs: ")

if unit.lower() == "l":
    print("Weight in Kg: " + str(weight / 2.2))
elif unit.lower() == "k":
    print("Weight in Lbs: " + str(weight * 2.2))
else:
    print("Accepted values: [k, l]")
