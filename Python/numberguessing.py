# Requirements
import tensorflow as tf
#tf.enable_eager_execution()

mnist = tf.keras.datasets.mnist
(training_data, training_labels), (test_data, test_labels) = mnist.load_data()
training_data, test_data = training_data / 255, test_data / 255

import numpy as np



# Neuronal Network
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation=tf.nn.relu),
    tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])

model.compile(
    optimizer=tf.optimizers.Adam(),
    loss='sparse_categorical_crossentropy',
    #metrics={'accuracy'}
)



# Train Network
model.fit(training_data, training_labels, epochs=5)



# Test Network
model.evaluate(test_data, test_labels)
predictions = model.predict(test_data)

image_index = 2
print('True: {} \nPredict: {}'.format(test_labels[image_index], np.argmax(predictions[image_index])))