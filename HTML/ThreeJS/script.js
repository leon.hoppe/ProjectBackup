import * as THREE from '/node_modules/three/build/three.module.js'

function main() {
    const scene = new THREE.Scene();

    const box = generateBox(1, 1, 1);
    box.translateZ(-5);
    box.translateY(0.5);
    scene.add(box);

    const floor = generateFloor(20, 20);
    floor.rotation.x = Math.PI / 2;
    scene.add(floor);

    const camera = new THREE.PerspectiveCamera(
        45,
        window.innerWidth / window.innerHeight,
        1, 1000
    );
    camera.position.set(1, 5, 5);
    camera.lookAt(box.position);

    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);

    document.getElementById("webgl").appendChild(renderer.domElement);
    update(renderer, scene, camera);
}

function generateFloor(w, d) {
    const geo = new THREE.PlaneGeometry(w, d);
    const mat = new THREE.MeshBasicMaterial({
        color: 0x00ff00,
        side: THREE.DoubleSide
    });
    return new THREE.Mesh(geo, mat);
}

function generateBox(w, h, d) {
    const geo = new THREE.BoxGeometry(w, h, d);
    const mat = new THREE.MeshBasicMaterial({
        color: 0xffffff
    });
    return new THREE.Mesh(geo, mat);
}

function update(renderer, scene, camera) {
    renderer.render(scene, camera);
    requestAnimationFrame(() => update(renderer, scene, camera));

    scene.children[0].rotateY(0.002);
}

main();