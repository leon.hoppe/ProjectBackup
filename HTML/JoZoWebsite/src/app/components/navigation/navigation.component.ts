import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  public links: {name: string, link: string, sublinks?: {name: string, link: string}[]}[] = [
    {name: "Home", link: "/"},
    {name: "Musik", link: "/music"},
    {name: "Images", link: "/images"},
    {name: "Bus und Bahn", link: "/bus-und-bahn"},
    {name: "Nachrichten", link: "/nachrichten", sublinks: [
        {name: "Nachrichtenarchiv", link: "/nachrichten/archiv"}
    ]}
  ];

  public constructor(public router: Router) {}

  public cleanUrl(url: string): string {
    try {
      url = location.origin + url;
      const urlObj = new URL(url);

      urlObj.search = '';
      urlObj.hash = '';

      let str = urlObj.toString().replace(location.origin, "");
      const split = str.split("/");
      return "/" + split[1];
    } catch {
      return "";
    }
  }
}
