import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { HomeComponent } from './sites/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShellComponent } from './components/shell/shell.component';
import { MusicComponent } from './sites/music/music.component';
import { ArchiveComponent } from './sites/archive/archive.component';
import { ArticleComponent } from './sites/article/article.component';
import { TrafficComponent } from './sites/traffic/traffic.component';
import { NewsComponent } from './sites/news/news.component';
import { ArticlePreviewComponent } from './components/atricle-preview/article-preview.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    ShellComponent,
    MusicComponent,
    ArchiveComponent,
    ArticleComponent,
    TrafficComponent,
    NewsComponent,
    ArticlePreviewComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
