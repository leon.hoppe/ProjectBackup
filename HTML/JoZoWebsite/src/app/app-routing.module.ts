import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./sites/home/home.component";
import {MusicComponent} from "./sites/music/music.component";
import {ArchiveComponent} from "./sites/archive/archive.component";
import {ArticleComponent} from "./sites/article/article.component";
import {TrafficComponent} from "./sites/traffic/traffic.component";
import {NewsComponent} from "./sites/news/news.component";

const routes: Routes = [
  {path: "music", component: MusicComponent},
  {path: "nachrichten", component: NewsComponent},
  {path: "nachrichten/archiv", component: ArchiveComponent},
  {path: "nachrichten/:id", component: ArticleComponent},
  {path: "bus-und-bahn", component: TrafficComponent},
  {path: "**", component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
