export interface Article {
  id: string;
  title: string;
  image?: string;
  subtitle?: string;
  text: string;
  summary: string;
  references: string;
  footer?: string;
  warning: boolean;
  showWarning: boolean;
  more: boolean;
  created: Date;
  updated: Date;
}
