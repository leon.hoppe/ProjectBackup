export interface ConstructionSide {
  lines: Line[];
  startTime?: Date;
  endTime?: Date;
  type: string;
  description: string;
  article?: string;
}

export interface Line {
  name: string;
  color: string;
  id: string;
}
