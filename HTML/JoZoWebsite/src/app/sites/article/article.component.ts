import {Component, OnInit} from '@angular/core';
import {Article} from "../../entities/article";
import {ActivatedRoute} from "@angular/router";
import {BackendService} from "../../services/backend.service";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  public article: Article | undefined;

  public constructor(private route: ActivatedRoute, private backend: BackendService) {}

  ngOnInit() {
    this.route.params.subscribe(async params => {
      this.article = await this.backend.getArticle(params['id']);
    })
  }
}
