import {Component, OnInit} from '@angular/core';
import {BackendService} from "../../services/backend.service";
import {Article} from "../../entities/article";

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
export class ArchiveComponent implements OnInit {

  public articles: Article[] = [];

  public constructor(private backend: BackendService) {}

  async ngOnInit() {
    this.articles = await this.backend.getArticles();
  }

}
