import {Component, OnInit} from '@angular/core';
import {ConstructionSide} from "../../entities/constructionSide";
import {BackendService} from "../../services/backend.service";
import {Article} from "../../entities/article";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  public sides: ConstructionSide[] = [];
  public articles: Article[] = []

  public constructor(private backend: BackendService) {}

  async ngOnInit() {
    this.sides = await this.backend.getConstructionSides();
    this.articles = await this.backend.getArticles();
  }

  public getWarnings(): Article[] {
    return this.articles.filter(article => article.showWarning);
  }

  public getRecentArticles(): Article[] {
    return this.articles.filter(article => !article.warning).slice(0, 6);
  }

  public getMoreArticles(): Article[] {
    return this.articles.filter(article => article.more);
  }

}
