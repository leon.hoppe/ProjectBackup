import { Injectable } from '@angular/core';
import PocketBase from 'pocketbase';
import {Article} from "../entities/article";
import {ConstructionSide, Line} from "../entities/constructionSide";

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private pb: PocketBase;

  constructor() {
    this.pb = new PocketBase('https://8da0ff06-6e45-4bc7-a502-940e92c29908.api.leon-hoppe.de');
  }

  public async getArticles(): Promise<Article[]> {
    return (await this.pb.collection('articles').getFullList({
      sort: "-created"
    }) as Article[]).map<Article>((entry: Article): Article => {
      entry.updated = new Date(entry.updated);
      entry.created = new Date(entry.created);
      return entry;
    });
  }

  public async getArticle(id: string): Promise<Article> {
    const article = await this.pb.collection('articles').getOne<Article>(id);
    article.updated = new Date(article.updated);
    article.created = new Date(article.created);
    return article;
  }

  public async getConstructionSides(): Promise<ConstructionSide[]> {
    const sides = await this.pb.collection('construction_sites').getFullList<any>({
      sort: 'startTime'
    });
    const allLines = await this.pb.collection('lines').getFullList<Line>();

    for (let side of sides) {
      const lines = side.lines as string[];

      side.lines = new Array<Line>();
      side.startTime = new Date(side.startTime);
      side.endTime = new Date(side.endTime);

      for (let line of lines) {
        side.lines.push(allLines.filter(l => l.id == line)[0]);
      }
    }

    return sides as ConstructionSide[];
  }
}
