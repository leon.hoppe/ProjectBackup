Locales['en'] = {
    ["mission_row"] = "Mission Row Police",
    ["phone_booth"] = "Telefonzelle",
    ["key_answer"] = "Drücke ~INPUT_PICKUP~ um den Anruf anzunehmen",
    ["new_message"] = "~o~Neue Nachricht",
    ["new_message_from"] = "~o~Neue Nachricht von ~y~%s",
    ["new_message_transmitter"] = "~o~Neue Nachricht von ~g~%s",
    ["use_fixed"] = "~g~%s ~o~(%s) ~n~~INPUT_PICKUP~~w~ Benutzen"
}
