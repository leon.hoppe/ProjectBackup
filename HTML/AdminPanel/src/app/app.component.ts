import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'AdminPanel';

  ngOnInit(): void {
    AppComponent.createClickable(".sites");
  }

  public static createClickable(querySelector: string) {
    const container = document.querySelector(querySelector) as HTMLDivElement;

    for (let index = 0; index < container.children.length; index++) {
      const element = container.children[index] as HTMLDivElement;

      element.onclick = () => {
        for (let i = 0; i < container.children.length; i++)
          container.children[i].classList.remove("selected");

        element.classList.add("selected");
      }
    }
  }
}
