var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class FiveM {
    constructor() {
        this.hostname = "https://princep.de/";
    }
    onInit() {
        this.loadComponents();
    }
    loadComponents() {
        return __awaiter(this, void 0, void 0, function* () {
            const components = document.querySelectorAll('[component]');
            for (let i = 0; i < components.length; i++) {
                const element = components[i];
                const component = element.getAttribute("component");
                element.innerHTML = yield this.sendHttpRequest(this.hostname + "assets/components/" + component + "/index.html");
                try {
                    const script = yield this.sendHttpRequest(this.hostname + "js/assets/components/" + component + "/script.js");
                    const scriptElement = document.createElement("script");
                    scriptElement.type = "text/javascript";
                    scriptElement.appendChild(document.createTextNode(script));
                    element.appendChild(scriptElement);
                }
                catch (e) { }
            }
        });
    }
    sendHttpRequest(url) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(url);
            return yield response.text();
        });
    }
    getCurrentSite() {
        return location.href.replace(this.hostname, "");
    }
    getPlayerInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getJsonFromUrl("https://api.princep.de/players.json");
        });
    }
    getServerInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getJsonFromUrl("https://api.princep.de/dynamic.json");
        });
    }
    getExtendedServerInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getJsonFromUrl("https://api.princep.de/info.json");
        });
    }
    getJsonFromUrl(url) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(((resolve, reject) => {
                const request = new XMLHttpRequest();
                request.open("GET", url);
                request.onreadystatechange = () => {
                    if (request.status === 200) {
                        try {
                            resolve(JSON.parse(request.response));
                        }
                        catch (e) { }
                    }
                    else {
                        reject({
                            status: request.status,
                            statusText: request.statusText
                        });
                    }
                };
                request.onerror = () => reject({
                    status: request.status,
                    statusText: request.statusText
                });
                request.send();
            }));
        });
    }
}
const fivem = new FiveM();
fivem.onInit();
//# sourceMappingURL=main.js.map