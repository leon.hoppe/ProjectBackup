const title = document.getElementById("currentSite");
const site = fivem.getCurrentSite();
switch (site) {
    case "rules":
        title.innerText = "Regeln";
        break;
    case "cars":
        title.innerText = "Autos";
        break;
    case "team":
        title.innerText = "Team";
        break;
    default:
        title.innerText = "Startseite";
        break;
}
const header_buttons = document.getElementsByClassName("navigation_button");
for (let button of header_buttons) {
    button.onclick = () => {
        location.href = fivem.hostname + button.id.replace("redirect_", "");
    };
}
//# sourceMappingURL=script.js.map