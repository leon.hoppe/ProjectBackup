const footer_buttons = document.getElementsByClassName("footer_buttons")[0].children;
for (let button of footer_buttons) {
    button.onclick = () => {
        location.href = button.id;
    };
}
//# sourceMappingURL=script.js.map