var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let slideCount = 0;
const simpleSlides = function () {
    let i;
    const slides = document.querySelectorAll(".slides");
    for (i = 0; i < slides.length; i++) {
        slides[i].setAttribute("style", "display:none");
    }
    slideCount++;
    if (slideCount > slides.length) {
        slideCount = 1;
    }
    slides[slideCount - 1].setAttribute("style", "display:block");
    setTimeout(simpleSlides, 8000);
};
const init = () => __awaiter(this, void 0, void 0, function* () {
    const players = document.getElementById("players");
    const serverName = document.getElementById("servername");
    const status = document.getElementById("serverStatus");
    try {
        const serverInfo = yield fivem.getServerInfo();
        players.innerText = `Spieler: ${serverInfo.clients} / ${serverInfo.sv_maxclients}`;
        serverName.innerText = serverInfo.hostname;
        status.innerHTML = "Serverstatus: <span style='color: var(--green)'>Online</span>";
        localStorage.setItem("hostname", serverInfo.hostname);
    }
    catch (e) {
        players.innerText = "Spieler: 0 / 0";
        serverName.innerText = localStorage.getItem("hostname") === null ? "[GER] PrincepRP | 🎓Beta-Release🎓 | 🔐Soft-Allowlist🔐 | 🎙Ingame Voice🎙 | 👥Multi-Char👥 | 💸Startgeld: 10k💸 |🚗Echte Automarken🚗" : localStorage.getItem("hostname");
        status.innerHTML = "Serverstatus: <span style='color: var(--red)'>Offline</span>";
    }
});
init();
simpleSlides();
setInterval(init, 30000);
//# sourceMappingURL=script.js.map