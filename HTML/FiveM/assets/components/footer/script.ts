const footer_buttons = document.getElementsByClassName("footer_buttons")[0].children as HTMLCollectionOf<HTMLInputElement>;
for (let button of footer_buttons) {
    button.onclick = () => {
        location.href = button.id;
    }
}