<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url" content="https://princep.de/">
    <meta property="og:title" content="PrincepRP - Startseite">
    <meta property="og:description" content="[GER] PrincepRP | 🎓Beta-Release🎓 | 🔐Soft-Allowlist🔐 | 🎙Ingame Voice🎙 | 👥Multi-Char👥 | 💸Startgeld: 10k💸 |🚗Echte Automarken🚗">
    <meta property="og:image" content="https://princep.de/assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" href="/assets/img/favicon.png">
    <script src="js/main.js" defer></script>
    <script src="js/script.js" defer></script>
    <title>PrincepRP - Startseite</title>
</head>
<body>
<section component="navbar"></section>

<section id="serverInfo" class="unselectable">
    <span id="players"></span>
    <a id="servername" href="https://cfx.re/join/dyklgj" target="_blank" rel="noopener noreferrer"></a>
    <span id="serverStatus"></span>
</section>

<section id="whatIsPrincep">
    <h1>Was ist Princep</h1>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid dolores dolorum earum error facilis, ipsum officiis pariatur quos sequi tenetur. Beatae cupiditate deleniti ea error ex inventore iure, quod veniam!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem ipsam magni necessitatibus nesciunt nihil non voluptas voluptatum! Animi aperiam commodi dignissimos eveniet fugiat optio quis ratione reiciendis sed sunt.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto, assumenda consequatur enim perferendis quisquam repudiandae! A deleniti eius error itaque laborum libero, nobis perferendis quaerat repellendus repudiandae ut vel!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur, culpa cumque distinctio, eaque harum hic incidunt natus necessitatibus quae quibusdam soluta ut. Alias, odio placeat possimus ratione tenetur voluptas?
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eius laudantium, provident quia repudiandae veniam! Aspernatur, atque dignissimos ea eaque eum excepturi harum natus nobis quibusdam reprehenderit temporibus voluptate voluptates!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci amet animi atque debitis dicta distinctio illum itaque neque, nulla odio quae qui quo recusandae rerum sapiente tempore voluptas voluptatem?
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam exercitationem fugit ipsum libero quaerat! Blanditiis cumque earum nulla quis quod, totam vel. Accusantium deleniti illum minima molestias possimus sapiente.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci blanditiis consectetur culpa dignissimos eos excepturi hic illum incidunt ipsa nemo, nihil nobis qui recusandae saepe voluptates? Ea officia rem voluptatum.
    </p>
</section>

<section id="diashow" class="unselectable">
    <img class="slides animate" alt="slide-1.jpg" src="assets/img/slide-1.jpg" draggable="false">
    <img class="slides animate" alt="slide-2.jpg" src="assets/img/slide-2.jpg" draggable="false">
    <img class="slides animate" alt="slide-3.jpg" src="assets/img/slide-3.jpg" draggable="false">
    <img class="slides animate" alt="slide-4.jpg" src="assets/img/slide-4.jpg" draggable="false">
</section>

<section id="jobs" class="unselectable">
    <div class="jobs">
        <div>
            <img src="/assets/img/jobs/police.png" alt="icon">
            <h1>Polizei</h1>
            <span>Gehe auf Streife und schütze Zivilisten vor der Kriminalität. Sorge für Gerechtigkeit und verteidige das Gesetz.</span>
        </div>

        <div>
            <img src="/assets/img/jobs/medic.png" alt="icon">
            <h1>Medic</h1>
            <span>In einer Großstadt passieren viele Unfälle, deshalb ist dieser Job wichtig. Ärztliche Versorgung ist das A und O.</span>
        </div>

        <div>
            <img src="/assets/img/jobs/crimelife.png" alt="icon">
            <h1>Crimelife</h1>
            <span>Mit illegalen Geschäften lässt sich viel Geld verdienen, doch lass dich nicht von der Polizei erwischen.</span>
        </div>

        <div>
            <img src="/assets/img/jobs/legallife.png" alt="icon">
            <h1>Legallife</h1>
            <span>Legale Geschäfte sind nicht so profitabel wie illegale, aber du verstöst nicht gegen das Gesetz und hast nichts zu befürchten.</span>
        </div>
    </div>
</section>

<section id="changelogDiscord">
    <section id="changelog">
        <h2>Changelog</h2>
        <br>
        <ul>
            <li class="feature">
                <h1>Beta 0.0.1</h1>
                <ul class="features">
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                </ul>
            </li>

            <li class="feature">
                <h1>Beta 0.0.1</h1>
                <ul class="features">
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                </ul>
            </li>

            <li class="feature">
                <h1>Beta 0.0.1</h1>
                <ul class="features">
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                </ul>
            </li>

            <li class="feature">
                <h1>Beta 0.0.1</h1>
                <ul class="features">
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                </ul>
            </li>

            <li class="feature">
                <h1>Beta 0.0.1</h1>
                <ul class="features">
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus dolore excepturi expedita in inventore minus mollitia optio quam quasi quidem recusandae, rem velit? Ab laboriosam laborum modi nobis vero.</li>
                </ul>
            </li>
        </ul>
    </section>
    <section id="discord">
        <iframe src="https://discord.com/widget?id=942183216759078963&theme=dark" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
    </section>
</section>

<section component="footer"></section>
</body>
</html>