interface Component {
    onInit();
}

interface Player {
    endpoint: string;
    id: number;
    identifiers: string[];
    name: string;
    ping: number;
}

interface ServerInfo {
    clients: number;
    gametype: string;
    hostname: string;
    iv: string;
    mapname: string;
    sv_maxclients: string;
}

interface ExtendedInfo {
    enhancedHostSupport: boolean;
    icon: string;
    resources: string[];
    server: string;
    vars: any;
    version: number;
}

class FiveM {
    public readonly hostname = "https://princep.de/";

    public onInit() {
        this.loadComponents();
    }

    private async loadComponents() {
        const components = document.querySelectorAll('[component]');
        for (let i = 0; i < components.length; i++) {
            const element = components[i] as HTMLElement;
            const component = element.getAttribute("component");
            element.innerHTML = await this.sendHttpRequest(this.hostname + "assets/components/" + component + "/index.html");
            try {
                const script = await this.sendHttpRequest(this.hostname + "js/assets/components/" + component + "/script.js");
                const scriptElement = document.createElement("script") as HTMLScriptElement;
                scriptElement.type = "text/javascript";
                scriptElement.appendChild(document.createTextNode(script));
                element.appendChild(scriptElement);
            }catch (e) {}
        }
    }

    public async sendHttpRequest(url: string): Promise<string> {
        const response = await fetch(url);
        return await response.text();
    }

    public getCurrentSite(): string {
        return location.href.replace(this.hostname, "");
    }

    public async getPlayerInfo(): Promise<Player[]> {
        return await this.getJsonFromUrl("https://api.princep.de/players.json");
    }

    public async getServerInfo(): Promise<ServerInfo> {
        return await this.getJsonFromUrl("https://api.princep.de/dynamic.json");
    }

    public async getExtendedServerInfo(): Promise<ExtendedInfo> {
        return await this.getJsonFromUrl("https://api.princep.de/info.json");
    }

    private async getJsonFromUrl<T>(url: string): Promise<T> {
        return new Promise<T>(((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open("GET", url);
            request.onreadystatechange = () => {
                if (request.status === 200) {
                    try {
                        resolve(JSON.parse(request.response) as T);
                    }catch (e) {}
                }else {
                    reject({
                        status: request.status,
                        statusText: request.statusText
                    });
                }
            };
            request.onerror = () => reject({
                status: request.status,
                statusText: request.statusText
            });
            request.send();
        }));
    }
}

const fivem: FiveM = new FiveM();
fivem.onInit();