import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  public sites: {label: string, href: string, icon: string}[] = [
    {label: "Featured", href: "/featured", icon: "/assets/icons/featured.png"},
    {label: "Projekte", href: "/projects", icon: "/assets/icons/projects.png"},
    {label: "Skills", href: "/skills", icon: "/assets/icons/skills.png"},
    {label: "Workflow", href: "/workflow", icon: "/assets/icons/workflow.png"},
    {label: "Kontakt", href: "/contact", icon: "/assets/icons/contact.png"}
  ];

  constructor(public router: Router) {}
}
