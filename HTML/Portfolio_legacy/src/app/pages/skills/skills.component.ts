import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {defTitle} from "../../app.component";
import {SkillTable} from "../../entitys/skillTable";
import {BackendService} from "../../services/backend.service";



@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  @ViewChild('left') left: ElementRef;
  @ViewChild('right') right: ElementRef;
  @ViewChild('bottom') bottom: ElementRef;

  public readonly displayedColumns: string[] = ['label', 'basic', 'normal', 'advanced'];

  public languages: SkillTable[];
  public frameworks: SkillTable[];

  constructor(private title: Title, private backend: BackendService) { }

  async ngOnInit() {
    this.title.setTitle(defTitle + " - Skills");

    this.languages = await this.backend.getSkills(1);
    this.frameworks = await this.backend.getSkills(2);

    this.left.nativeElement.classList.add("skills-left-in");
    this.right.nativeElement.classList.add("skills-right-in");
    this.bottom.nativeElement.classList.add("skills-bottom-in");
  }

}
