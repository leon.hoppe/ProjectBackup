import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {defTitle} from "../../app.component";
import {Router} from "@angular/router";
import {Project} from "../../entitys/project";
import {BackendService} from "../../services/backend.service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  public projects: Project[];

  constructor(private title: Title, public router: Router, public backend: BackendService) { }

  async ngOnInit() {
    this.title.setTitle(defTitle + " - Projekte");
    this.projects = await this.backend.getProjects();
  }

}
