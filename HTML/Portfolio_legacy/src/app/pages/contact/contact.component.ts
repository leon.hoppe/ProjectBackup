import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import Swal from 'sweetalert2';
import {defTitle} from "../../app.component";
import {BackendService} from "../../services/backend.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public readonly name: FormControl = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]);
  public readonly email: FormControl = new FormControl('', [Validators.required, Validators.email, Validators.maxLength(50)]);
  public readonly message: FormControl = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]);

  constructor(private title: Title, private backend: BackendService) { }

  getErrorMessage(control: FormControl): string {
    if (control.hasError('required')) {
      return 'Geben sie einen Wert ein';
    }

    if (control.hasError('maxlength'))
      return 'Der eingegebene Text ist zu lang'

    if (control.hasError('minlength'))
      return 'Der eingegebene Text ist zu kurz'

    return control.hasError('email') ? 'Keine gültige E-Mail' : '';
  }

  ngOnInit(): void {
    this.title.setTitle(defTitle + " - Kontakt");
  }

  async sendMessage(valid: boolean, name: string, email: string, message: string, form: HTMLFormElement) {
    if (!valid) return;
    await this.backend.sendMessage({name, email, message});

    form.reset();
    await Swal.fire({
      title: 'Nachricht wurde versendet!',
      icon: "success"
    });
  }

}
