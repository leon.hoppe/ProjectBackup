import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {defTitle} from "../../app.component";
import {Workflow} from "../../entitys/workflow";
import {BackendService} from "../../services/backend.service";

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {
  public workflows: Workflow[];

  constructor(private title: Title, private backend: BackendService) { }

  async ngOnInit() {
    this.title.setTitle(defTitle + " - Workflow");
    this.workflows = await this.backend.getWorkflow();
  }
}
