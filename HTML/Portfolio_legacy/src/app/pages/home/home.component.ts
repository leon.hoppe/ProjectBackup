import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {defTitle} from "../../app.component";
import {BackendService} from "../../services/backend.service";
import {Home} from "../../entitys/home";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('welcome') welcome: ElementRef;
  @ViewChild('about') about: ElementRef;

  public home: Home;

  constructor(private title: Title, private backend: BackendService) { }

  async ngOnInit() {
    this.title.setTitle(defTitle);

    this.home = await this.backend.getHomeContent();

    this.welcome.nativeElement.classList.add("home-welcome-in");
    this.about.nativeElement.classList.add("home-about-in");
  }

}
