import { Injectable } from '@angular/core';
// @ts-ignore
import PocketBase from 'pocketbase';
import {Home} from "../entitys/home";
import {environment} from "../../environments/environment";
import {Project} from "../entitys/project";
import {SkillTable} from "../entitys/skillTable";
import {Workflow} from "../entitys/workflow";
import {Message} from "../entitys/message";

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private client: PocketBase;

  constructor() {
    this.client = new PocketBase(environment.backendHost);
  }

  public async getHomeContent(): Promise<Home> {
    return await this.client.records.getOne('home', 'voh3zcasdgobidb') as Home;
  }

  public async getProjects(): Promise<Project[]> {
    const projects = await this.client.records.getFullList('projects', 200, {
      sort: '-order'
    });

    return projects as Project[];
  }

  public async getSkills(type: number): Promise<SkillTable[]> {
    const skills = await this.client.records.getFullList('skills', 200, {
      filter: `type = ${type}`,
      sort: 'created'
    });

    return skills as SkillTable[];
  }

  public async getWorkflow(): Promise<Workflow[]> {
    const workflow = await this.client.records.getFullList('workflow', 200, {
      sort: 'created'
    });

    return workflow as Workflow[];
  }

  public async sendMessage(message: Message) {
    await this.client.records.create('messages', message);
  }
}
