import { Pipe, PipeTransform } from '@angular/core';
import {Project} from "../entitys/project";
import {Router} from "@angular/router";

@Pipe({
  name: 'projects'
})
export class ProjectsPipe implements PipeTransform {

  constructor(private router: Router) {}

  transform(objects: Project[]): Project[] {
    if (this.router.url != "/featured")
      return objects;
    else {
      const newObjects: Project[] = [];
      objects?.forEach(obj => {
        if (obj?.featured)
          newObjects.push(obj);
      })
      return newObjects;
    }
  }

}
