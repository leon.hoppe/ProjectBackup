export interface SkillTable {
  label: string;
  level: number;
}
