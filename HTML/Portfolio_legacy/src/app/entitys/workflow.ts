export interface Workflow {
  name: string;
  description: string;
  img: string;
  logo: string;
  link: string;
}
