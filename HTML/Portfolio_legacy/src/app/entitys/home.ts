export interface Home {
  header: string;
  description: string;
  about1: string;
  about2: string;
}
