﻿using System;
using System.Speech.Recognition;
using System.Globalization;

namespace SpeechRecognision {
    class Program {
        private static SpeechRecognitionEngine _recogniser;
        
        static void Main(string[] args) {
            Console.Write("Initializing Engine...");
            _recogniser = new SpeechRecognitionEngine(new CultureInfo("de-DE"));
            _recogniser.LoadGrammar(new DictationGrammar());
            _recogniser.SpeechRecognized += OnSpeechRecognized;
            _recogniser.SetInputToDefaultAudioDevice();
            _recogniser.RecognizeAsync(RecognizeMode.Multiple);
            Console.WriteLine("Done");
            Console.WriteLine("Awaiting Input");
            while (true) {
                Console.ReadLine();
            }
        }

        private static void OnSpeechRecognized(object sender, SpeechRecognizedEventArgs e) {
            Console.WriteLine(e.Result.Text);
        }
    }
}