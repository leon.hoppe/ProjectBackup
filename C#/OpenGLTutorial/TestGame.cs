﻿using System.Numerics;
using GLFW;
using static OpenGL.GL;
using OpenGLTutorial.GameLoop;
using OpenGLTutorial.Rendering.Cameras;
using OpenGLTutorial.Rendering.Display;
using OpenGLTutorial.Rendering.Objects;
using OpenGLTutorial.Rendering.Objects.Components;
using OpenGLTutorial.Rendering.Shaders;

namespace OpenGLTutorial {
    public class TestGame : Game {
        private Camera2D _camera;
        private GameObject _text;
        private GameObject _test;
        
        protected override void Initialize() {
            DEBUG_CopyFolderToOutput(@"D:\Programmierstuff\C#\OpenGLTutorial\Resources");
            DEBUG_CopyFileToOutput(@"D:\Programmierstuff\C#\OpenGLTutorial\glfw.dll");
        }

        protected override void LoadContent() {
            DisplayManager.Window.SetWindowIcon(@"Resources\logo.png");
            
            Shader shader = new Shader(@"Resources\shader.glsl");
            _text = new GameObject(shader, null, null);
            _text.AddComponent(new TextComponent(@"Resources\font.png", "", null, GL_DYNAMIC_DRAW));
            _text.Scale = new Vector2(0.2f, 0.2f);
            _text.Position = new Vector2(-4, 3);

            Texture texture = new Texture(@"Resources\bricks.png");
            Shape shape = new Shape(Shapes.Rectangle);
            _test = new GameObject(shader, shape, texture) { Scale = new Vector2(3, 3) };
            
            _camera = new Camera2D(Vector2.Zero, 100.0f);
            
            _text.Load();
            _test.Load();
        }

        private float _fpsUpdate;
        protected override void Update() {

            if (_fpsUpdate >= 0.2f) {
                _text.GetComponent<TextComponent>().Text = "FPS: " + (int)(1f / GameTime.DeltaTime);
                _fpsUpdate = 0;
            }
            _fpsUpdate += GameTime.DeltaTime;
            
            _text.Update();
            
            const float speed = 2.0f;
            float x = Input.GetAxis(Axis.Horizontal) * speed * GameTime.DeltaTime + _test.Position.X;
            float y = Input.GetAxis(Axis.Vertical) * speed * GameTime.DeltaTime + _test.Position.Y;
            _test.Position = new Vector2(x, y);
            
            if (Input.Keys[Keys.Escape])
                DisplayManager.Window.Close();
        }

        protected override void Render() {
            glClearColor(0, 0, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT);
            
            _test.Render(_camera);
            _text.Render(_camera);

            DisplayManager.Window.SwapBuffers();
        }

        protected override void Destroy() {
            _text.Destroy();
        }
    }
}