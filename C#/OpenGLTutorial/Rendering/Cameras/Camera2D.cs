﻿using System.Numerics;
using OpenGLTutorial.Rendering.Display;

namespace OpenGLTutorial.Rendering.Cameras {
    public class Camera2D {
        public Vector2 FocusPosition { get; set; }
        public float Zoom { get; set; }

        public Camera2D(Vector2 focusPosition, float zoom) {
            FocusPosition = focusPosition;
            Zoom = zoom;
        }

        public Matrix4x4 GetProjectionMatrix() {
            float left = FocusPosition.X - DisplayManager.WindowSize.Width / 2f;
            float right = FocusPosition.X + DisplayManager.WindowSize.Width / 2f;
            float top = FocusPosition.Y - DisplayManager.WindowSize.Height / 2f;
            float bottom = FocusPosition.Y + DisplayManager.WindowSize.Height / 2f;
            
            Matrix4x4 orthoMatrix = Matrix4x4.CreateOrthographicOffCenter(left, right, bottom, top, 0.01f, 100.0f);
            Matrix4x4 zoomMatrix = Matrix4x4.CreateScale(Zoom);

            return orthoMatrix * zoomMatrix;
        }
    }
}