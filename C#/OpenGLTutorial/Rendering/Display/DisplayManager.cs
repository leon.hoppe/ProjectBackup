﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using GLFW;
using OpenGLTutorial.GameLoop;
using StbiSharp;
using static OpenGL.GL;
using Image = GLFW.Image;

namespace OpenGLTutorial.Rendering.Display {
    public static class DisplayManager {
        
        public static NativeWindow Window { get; set; }
        public static Size WindowSize { get; set; }

        public static void CreateWindow(int width, int height, string title, bool vsync) {
            WindowSize = new Size(width, height);
            Glfw.Init();
            
            Glfw.WindowHint(Hint.ContextVersionMajor, 3);
            Glfw.WindowHint(Hint.ContextVersionMinor, 3);
            Glfw.WindowHint(Hint.OpenglProfile, Profile.Core);
            
            Glfw.WindowHint(Hint.Focused, true);
            Glfw.WindowHint(Hint.Resizable, true);

            Window = new NativeWindow(width, height, title);
            Window.CenterOnScreen();
            Window.MakeCurrent();
            Import(Glfw.GetProcAddress);
            glViewport(0, 0, width, height);
            Glfw.SwapInterval(vsync ? 1 : 0);

            Window.SizeChanged += OnResize;
            Window.KeyAction += OnKey;
            Window.MouseButton += OnMouse;
            Window.MouseMoved += OnMouseMove;
            Window.MouseScroll += OnMouseScroll;

        }

        private static void OnResize(object sender, SizeChangeEventArgs e) {
            WindowSize = e.Size;
            glViewport(0, 0, WindowSize.Width, WindowSize.Height);
        }

        private static void OnKey(object sender, KeyEventArgs e) => Input.Keys[e.Key] = e.State != InputState.Release;

        private static void OnMouse(object sender, MouseButtonEventArgs e) => Input.MouseButtons[e.Button] = e.Action != InputState.Release;
        private static void OnMouseMove(object sender, MouseMoveEventArgs e) => Input.MousePosition = e.Position;
        private static void OnMouseScroll(object sender, MouseMoveEventArgs e) => Input.MouseScroll = e.Position.Y;

        public static unsafe void SetWindowIcon(this NativeWindow window, string file) {
            using var stream = File.OpenRead(file);
            using var memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);
            StbiImage image = Stbi.LoadFromMemory(memoryStream, 0);

            fixed (byte* data = &image.Data[0]) {
                window.SetIcons(new Image(image.Width, image.Height, new IntPtr(data)));
            }
        }

    }
}