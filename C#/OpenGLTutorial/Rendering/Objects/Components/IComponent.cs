﻿namespace OpenGLTutorial.Rendering.Objects.Components {
    public interface IComponent {
        public void Load(GameObject thisObject);
        public void Update(GameObject thisObject);
        public void Destroy(GameObject thisObject);
        public void Render(GameObject thisObject);
    }
}