﻿using System.IO;
using GLFW;
using OpenGLTutorial.Rendering.Display;

namespace OpenGLTutorial.GameLoop {
    public abstract class Game {
        
        /// <summary>
        /// Initializes the Game and starts the GameLoop
        /// </summary>
        /// <param name="width">The initial width of the game window</param>
        /// <param name="height">The initial height of the game window</param>
        /// <param name="title">The title of the game window</param>
        /// <param name="vsync">Sync the game FPS to the Monitor FPS</param>
        public void Run(int width, int height, string title, bool vsync = false) {
            Initialize();

            DisplayManager.CreateWindow(width, height, title, vsync);

            LoadContent();

            while (!DisplayManager.Window.IsClosing) {
                GameTime.DeltaTime = (float)Glfw.Time - GameTime.TotalElapsedSeconds;
                GameTime.TotalElapsedSeconds = (float)Glfw.Time;

                Glfw.PollEvents();
                Input.Update();

                Update();
                Render();

                Input.MouseScroll = 0.0f;
            }

            Destroy();
            DisplayManager.Window.Close();
        }

        /// <summary>
        /// Gets called at the very start of the GameLoop
        /// </summary>
        protected abstract void Initialize();
        
        /// <summary>
        /// Gets called after the GLFW Window inititalizes
        /// </summary>
        protected abstract void LoadContent();
        
        /// <summary>
        /// Gets called every frame before Render
        /// </summary>
        protected abstract void Update();
        
        /// <summary>
        /// Gets called every frame after Update
        /// </summary>
        protected abstract void Render();
        
        /// <summary>
        /// Gets called right before the Game ends
        /// </summary>
        protected abstract void Destroy();

        /// <summary>
        /// Copys a folder to the Game output (DEBUG ONLY)
        /// </summary>
        /// <param name="folder">The folder to copy</param>
        protected void DEBUG_CopyFolderToOutput(string folder) {
            #if DEBUG
            var info = new DirectoryInfo(folder);
            info.CopyTo(Directory.GetCurrentDirectory() + "/" + info.Name, true, true);
            #endif
        }
        
        /// <summary>
        /// Copys a file to the Game output (DEBUG ONLY)
        /// </summary>
        /// <param name="file">The file to copy</param>
        protected void DEBUG_CopyFileToOutput(string file) {
            #if DEBUG
            var info = new FileInfo(file);
            info.CopyTo(Directory.GetCurrentDirectory() + "/" + info.Name, true);
            #endif
        }
    }

    public static class DirectoryExtensions {
        public static void CopyTo(this DirectoryInfo dir, string destinationDir, bool overwrite, bool recursive) {
            if (overwrite && Directory.Exists(destinationDir))
                Directory.Delete(destinationDir, true);
            
            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            DirectoryInfo[] dirs = dir.GetDirectories();
            Directory.CreateDirectory(destinationDir);

            foreach (FileInfo file in dir.GetFiles()) {
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
            }

            if (recursive) {
                foreach (DirectoryInfo subDir in dirs) {
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyTo(subDir, newDestinationDir, true, overwrite);
                }
            }
        }
    }
}