﻿namespace OpenGLTutorial.GameLoop {
    public static class GameTime {
        
        /// <summary>
        /// The time in seconds that passed between frames
        /// </summary>
        public static float DeltaTime { get; set; }
        
        /// <summary>
        /// The total elapsed time sinse the game started
        /// </summary>
        public static float TotalElapsedSeconds { get; set; }
        
    }
}