﻿using OpenGLTutorial.GameLoop;

namespace OpenGLTutorial {
    class Program {
        public static void Main(string[] args) {
            Game game = new TestGame();
            game.Run(800, 600, "Test Game", true);
        }
    }
}