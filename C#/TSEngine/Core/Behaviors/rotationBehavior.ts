///<reference path="behavior.ts"/>

namespace TSE {

    export class RotationBehaviorData implements IBehaviorData {
        public name: string;
        public rotation: Vector3 = Vector3.zero;

        public setFromJson(json: any): void {
            if (json?.name === undefined) return;
            this.name = String(json.name);
            this.rotation.setFromJson(json.rotation);
        }
    }

    export class RotationBehaviorBuilder implements IBehaviorBuilder {
        public buildFromJson(json: any): TSE.BaseBehavior {
            let data = new RotationBehaviorData();
            data.setFromJson(json);
            return new RotationBehavior(data);
        }

        public get type(): string { return "rotation"; }
    }

    export class RotationBehavior extends BaseBehavior{

        private _rotation: Vector3;

        public constructor(data: RotationBehaviorData) {
            super(data);

            this._rotation = data.rotation;
        }

        public update(time: number) {
            this._owner.transform.rotation.add(this._rotation);

            super.update(time);
        }

    }

    BehaviorManager.registerBuilder(new RotationBehaviorBuilder());

}