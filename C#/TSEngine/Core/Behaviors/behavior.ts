namespace TSE {

    export class BaseBehavior {
        public name: string;
        protected _data: IBehaviorData;
        protected _owner: SimObject;

        public constructor(data: IBehaviorData) {
            this._data = data;
            this.name = data.name;
        }

        public set owner(value: SimObject) { this._owner = value; }

        public update(time: number): void {}
        public apply(userData: any): void {}

    }

    export interface IBehaviorBuilder {
        get type(): string;
        buildFromJson(json: any): BaseBehavior;
    }

    export interface IBehaviorData {
        name: string;
        setFromJson(json: any): void;
    }

    export class BehaviorManager {

        private static _registeredBuilders: {[type: string]: IBehaviorBuilder} = {};

        public static registerBuilder(builder: IBehaviorBuilder): void {
            BehaviorManager._registeredBuilders[builder.type] = builder;
        }

        public static extractComponent(json: any): BaseBehavior {
            if (BehaviorManager._registeredBuilders[json?.type] === undefined) return undefined;
            return BehaviorManager._registeredBuilders[json.type].buildFromJson(json);
        }

    }

}