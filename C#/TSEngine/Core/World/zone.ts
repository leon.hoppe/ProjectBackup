﻿namespace TSE {

    export enum ZoneState {
        UNINITIALIZED,
        LOADING,
        UPDATING
    }

    export class Zone {

        private _name: string;
        private _description: string;
        private _id: number;
        private _scene: Scene;
        private _state: ZoneState = ZoneState.UNINITIALIZED;
        private _globalId: number = -1;

        public constructor(id: number, name: string, descripton?: string) {
            this._id = id;
            this._name = name;
            this._description = descripton;
            this._scene = new Scene();
        }

        public get id(): number { return this._id; }
        public get name(): string { return this._name; }
        public get description(): string { return this._description; }
        public get scene(): Scene { return this._scene; }
        public get state(): ZoneState { return this._state; }

        public initialize(zoneData: any): void {
            if (zoneData.objects === undefined) return;

            for (let object of zoneData.objects) this.loadSimObject(object, this._scene.root);
        }

        public load(): void {
            if (this._state !== ZoneState.UNINITIALIZED) return;
            this._state = ZoneState.LOADING;
            this._scene.load();
            this._state = ZoneState.UPDATING;
        }
        public unload(): void {
            if (this._state === ZoneState.UNINITIALIZED) return;
            this._state = ZoneState.UNINITIALIZED;
        }
        public update(time: number): void {
            if (this._state === ZoneState.UPDATING)
                this._scene.update(time);
        }
        public render(shader: Shader): void {
            if (this._state === ZoneState.UPDATING)
                this._scene.render(shader);
        }

        public onActivated(): void { }
        public onDeactivated(): void { }

        private loadSimObject(dataSection: any, parent: SimObject): void {
            if (dataSection.name === undefined) throw new Error("Invalid Object data!");
            let name: string = String(dataSection.name);

            this._globalId++;
            let simObject = new SimObject(this._globalId, name, this._scene);

            if (dataSection.transform !== undefined) simObject.transform.setFromJson(dataSection.transform);

            if (dataSection.components !== undefined) {
                for (let component of dataSection.components)
                    simObject.addComponent(ComponentManager.extractComponent(component));
            }

            if (dataSection.behaviors !== undefined) {
                for (let behavior of dataSection.behaviors)
                    simObject.addBehavior(BehaviorManager.extractComponent(behavior));
            }

            if (dataSection.children !== undefined) {
                for (let object of dataSection.children) this.loadSimObject(object, simObject);
            }

            parent.addChild(simObject);
        }
    }

}