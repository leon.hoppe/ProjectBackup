﻿namespace TSE {

    export class ZoneManager implements IMessageHanlder{

        private static _globalZoneId: number = -1;
        //private static _zones: { [id: number]: Zone } = {};
        private static _registeredZones: {[id: number]: string} = {};
        private static _activeZone: Zone;
        private static _instance: ZoneManager;

        private constructor() { }

        public static initialize(): void {
            ZoneManager._instance = new ZoneManager();

            ZoneManager._registeredZones[0] = "assets/zones/testZone.json";
        }

        public static changeZone(id: number): void {
            if (ZoneManager._activeZone !== undefined) {
                ZoneManager._activeZone.onDeactivated();
                ZoneManager._activeZone.unload();
                delete ZoneManager._activeZone;
            }
            let zonePath = ZoneManager._registeredZones[id];
            if (zonePath === undefined) throw new Error("Zone Id does not exist!");

            if (AssetManager.isAssetLoaded(zonePath)) ZoneManager.loadZone(AssetManager.getAsset(zonePath));
            else {
                Message.subscribe(MESSAGE_ASSET_LOADER_ASSET_LOADED + zonePath, ZoneManager._instance);
                AssetManager.loadAsset(zonePath);
            }
        }

        public static update(time: number): void {
            if (ZoneManager._activeZone === undefined) return;
            ZoneManager._activeZone.update(time);
        }
        public static render(shader: Shader): void {
            if (ZoneManager._activeZone === undefined) return;
            ZoneManager._activeZone.render(shader);
        }

        private static loadZone(asset: JsonAsset): void {
            let zoneData = asset.data;
            if (zoneData.id === undefined) throw new Error("Zone file format exception: Zone id not present.")
            let zoneId: number = Number(zoneData.id);
            let zoneName: string = String(zoneData.name);
            let zoneDescription: string = String(zoneData.description);

            let zone = new Zone(zoneId, zoneName, zoneDescription);
            zone.initialize(zoneData);
            ZoneManager._activeZone = zone;
            zone.load();
            zone.onActivated();
        }

        public onMessage(message: Message): void {
            if (message.code.indexOf(MESSAGE_ASSET_LOADER_ASSET_LOADED) !== -1) {
                let asset = message.context as JsonAsset;
                ZoneManager.loadZone(asset);
            }
        }

        public static get zone(): Zone { return this._activeZone; }

    }

}