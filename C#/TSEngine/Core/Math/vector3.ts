﻿namespace TSE {

    export class Vector3 {
        public static get zero(): Vector3 { return new Vector3(0, 0, 0); }
        public static get one(): Vector3 { return new Vector3(1, 1, 1); }

        protected _x: number;
        protected _y: number;
        protected _z: number;

        public constructor(x: number = 0, y: number = 0, z: number = 0) {
            this._x = x;
            this._y = y;
            this._z = z;
        }

        public get x() { return this._x; }
        public set x(value: number) { this._x = value; }

        public get y() { return this._y; }
        public set y(value: number) { this._y = value; }

        public get z() { return this._z; }
        public set z(value: number) { this._z = value; }

        public toArray(): number[] { return [this._x, this._y, this._z]; }
        public toFloat32Array(): Float32Array { return new Float32Array(this.toArray()); }

        public copyFrom(vector: Vector3): void {
            this._x = vector._x;
            this._y = vector._y;
            this._z = vector._z;
        }

        public setFromJson(json: any): void {
            this._x = json.x !== undefined ? Number(json.x) : 0;
            this._y = json.y !== undefined ? Number(json.y) : 0;
            this._z = json.z !== undefined ? Number(json.z) : 0;
        }

        public add(vector: Vector3): Vector3 {
            this._x += vector._x;
            this._y += vector._y;
            this._z += vector._z;
            return this;
        }
        public subtract(vector: Vector3): Vector3 {
            this._x -= vector._x;
            this._y -= vector._y;
            this._z -= vector._z;
            return this;
        }
        public multiply(vector: Vector3): Vector3 {
            this._x *= vector._x;
            this._y *= vector._y;
            this._z *= vector._z;
            return this;
        }
        public divide(vector: Vector3): Vector3 {
            this._x /= vector._x;
            this._y /= vector._y;
            this._z /= vector._z;
            return this;
        }

    }

}