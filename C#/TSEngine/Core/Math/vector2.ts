﻿namespace TSE {

    export class Vector2 {
        protected _x: number;
        protected _y: number;

        public constructor(x: number = 0, y: number = 0) {
            this._x = x;
            this._y = y;
        }

        public get x() { return this._x; }
        public set x(value: number) { this._x = value; }

        public get y() { return this._y; }
        public set y(value: number) { this._y = value; }

        public toArray(): number[] { return [this._x, this._y]; }
        public toFloat32Array(): Float32Array { return new Float32Array(this.toArray()); }

        public setFromJson(json: any): void {
            this._x = json.x !== undefined ? Number(json.x) : 0;
            this._y = json.y !== undefined ? Number(json.y) : 0;
        }

        public copyFrom(vector: Vector2): Vector2 {
            this._x = vector._x;
            this._y = vector._y;
            return this;
        }

    }

}