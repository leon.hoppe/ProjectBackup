﻿namespace TSE {

    /**
     * The main GameEngine class.
     * */
    export class Engine {
        private _canvas: HTMLCanvasElement;
        private _shader: Shader;
        private _projection: Matrix4x4;
        private _previousTime: number = 0;

        /**
         * Creates a new Engine.
         * */
        public constructor(canvasId: string) {
            this._canvas = GLUtilities.initialize(canvasId);
        }

        /**
         * Starts this Engine.
         * */
        public start(): void {
            AssetManager.initialize();
            ZoneManager.initialize();

            gl.clearColor(0, 0, 0, 1);
            gl.enable(gl.BLEND);
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

            //LoadMaterials
            MaterialManager.registerMaterial(new Material("crate", "/assets/textures/crate.jpg"));
            MaterialManager.registerMaterial(new Material("goblin", "/assets/textures/goblin.png"));

            //Load Shader
            this._shader = new BasicShader();
            this._shader.use();

            //Load
            this._projection = Matrix4x4.orthographic(0, this._canvas.width, this._canvas.height, 0, -100.0, 100.0);
            ZoneManager.changeZone(0);

            this.loop();
        }

        private loop(): void {
            let delta = performance.now() - this._previousTime;

            MessageBus.update(delta);
            ZoneManager.update(delta);

            this._previousTime = performance.now();

            gl.clear(gl.COLOR_BUFFER_BIT);

            ZoneManager.render(this._shader);

            //Set Uniforms
            let projectionPosition = this._shader.getUniformLocation("u_projection");
            gl.uniformMatrix4fv(projectionPosition, false, new Float32Array(this._projection.data));

            requestAnimationFrame(this.loop.bind(this));
        }

        public resize(): void {
            this._projection = Matrix4x4.orthographic(0, this._canvas.width, this._canvas.height, 0, -100.0, 100.0);
            gl.viewport(0, 0, this._canvas.width, this._canvas.height);
        }

    }

}