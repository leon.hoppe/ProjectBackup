﻿namespace TSE {

    export abstract class BaseComponent {

        public name: string;
        protected _owner: SimObject;
        protected _data: IComponentData;

        public constructor(data: IComponentData) {
            this._data = data;
            this.name = data.name;
        }

        public get owner(): SimObject { return this._owner; }
        public set owner(value: SimObject) { this._owner = value; }

        public load(): void { }
        public update(time: number): void { }
        public render(shader: Shader): void { }

    }

    export interface IComponentData {
        name: string;

        setFromJson(json: any): void;
    }

    export interface IComponentBuilder {
        get type(): string;

        buildFromJson(json: any): BaseComponent;
    }

}