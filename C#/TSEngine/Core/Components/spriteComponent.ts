﻿///<reference path="componentManager.ts"/>

namespace TSE {

    export class SpriteComponentData implements IComponentData {
        public name: string;
        public materialName: string;

        public setFromJson(json: any): void {
            this.name = json.name !== undefined ? String(json.name) : undefined;
            this.materialName = json.materialName !== undefined ? String(json.materialName) : undefined;
        }
    }

    export class SpriteComponentBuilder implements IComponentBuilder {
        public get type(): string { return "sprite"; }

        public buildFromJson(json: any): BaseComponent {
            let data = new SpriteComponentData();
            data.setFromJson(json);
            return new SpriteComponent(data);
        }

    }

    export class SpriteComponent extends BaseComponent {

        protected _sprite: Sprite;

        public constructor(data: SpriteComponentData) {
            super(data);

            this._sprite = new Sprite(data.name, data.materialName);
        }

        public load(): void {
            this._sprite.load();
        }

        public render(shader: Shader): void {
            this._sprite.draw(shader, this.owner.worldMatrix);

            super.render(shader);
        }

    }



    export class AnimatedSpriteComponentData extends SpriteComponentData {
        public frameWidth: number;
        public frameHeight: number;
        public frameCount: number;
        public frameSequence: number[];

        public setFromJson(json: any): void {
            super.setFromJson(json);

            this.frameWidth = Number(json.frameWidth);
            this.frameHeight = Number(json.frameHeight);
            this.frameCount = Number(json.frameCount);
            this.frameSequence = json.frameSequence;
        }
    }

    export class AnimatedSpriteComponentBuilder extends SpriteComponentBuilder {
        public get type(): string { return "animated_sprite"; }

        public buildFromJson(json: any): BaseComponent {
            let data = new AnimatedSpriteComponentData();
            data.setFromJson(json);
            return new AnimatedSpriteComponent(data);
        }

    }

    export class AnimatedSpriteComponent extends SpriteComponent {

        public constructor(data: AnimatedSpriteComponentData) {
            super(data);
            this._sprite = new AnimatedSprite(this.name, data.materialName, data.frameWidth, data.frameHeight, data.frameCount, data.frameSequence);
        }

        public update(time: number) {
            this._sprite.update(time);
            super.update(time);
        }

    }

    ComponentManager.registerBuilder(new SpriteComponentBuilder());
    ComponentManager.registerBuilder(new AnimatedSpriteComponentBuilder());

}