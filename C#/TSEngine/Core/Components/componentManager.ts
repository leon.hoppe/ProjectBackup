namespace TSE {

    export class ComponentManager {

        private static _registeredBuilders: {[type: string]: IComponentBuilder} = {};

        public static registerBuilder(builder: IComponentBuilder): void {
            ComponentManager._registeredBuilders[builder.type] = builder;
        }

        public static extractComponent(json: any): BaseComponent {
            if (ComponentManager._registeredBuilders[json?.type] === undefined) return undefined;
            return ComponentManager._registeredBuilders[json.type].buildFromJson(json);
        }

    }

}