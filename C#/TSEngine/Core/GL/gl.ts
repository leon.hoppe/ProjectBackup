﻿namespace TSE {
    /** The WebGL rendering context. */
    export var gl: WebGLRenderingContext;

    /**
     * Responsible for setting up a WebGL rendering context.
     * */
    export class GLUtilities {

        /**
         * Initialize WebGL
         * @param elementId The id of the canvas element for rendering the game output.
         */
        public static initialize(elementId?: string): HTMLCanvasElement {
            let canvas: HTMLCanvasElement;

            if (elementId !== undefined)
                canvas = document.getElementById(elementId) as HTMLCanvasElement;
            if (elementId === undefined) {
                canvas = document.createElement("canvas") as HTMLCanvasElement;
                document.body.appendChild(canvas);
            }

            gl = canvas.getContext("webgl");
            if (gl === undefined)
                throw new Error("Unable to initialize WebGL");

            return canvas;
        }

    }
}