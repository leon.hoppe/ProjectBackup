namespace TSE {

    export class Vertex {

        public static vertexArray(vertices: Vertex[]): number[] {
            let array: number[] = [];
            for (let vertex of vertices)
                array = array.concat(vertex.toArray());
            return array;
        }

        public position: Vector3;
        public texCoords: Vector2;

        public constructor(x: number = 0, y: number = 0, z: number = 0, u: number = 0, v: number = 0) {
            this.position = new Vector3(x, y, z);
            this.texCoords = new Vector2(u, v);
        }

        public toArray(): number[] {
            let array: number[] = [];
            array = array.concat(this.position.toArray(), this.texCoords.toArray());
            return array;
        }

        public toFloat32Array(): Float32Array { return new Float32Array(this.toArray()); }

    }

}