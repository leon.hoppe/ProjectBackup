﻿namespace TSE {

    export class Color {
        public static get white(): Color { return new Color(255, 255, 255, 255); }
        public static get black(): Color { return new Color(0, 0, 0, 255) };
        public static get red(): Color { return new Color(255, 0, 0, 255) };
        public static get green(): Color { return new Color(0, 255, 0, 255) };
        public static get blue(): Color { return new Color(0, 0, 255, 255) };

        private _red;
        private _green;
        private _blue;
        private _alpha;

        public constructor(red: number = 255, green: number = 255, blue: number = 255, alpha: number = 255) {
            this._red = red;
            this._green = green;
            this._blue = blue;
            this._alpha = alpha;
        }

        public get red(): number { return this._red; }
        public get green(): number { return this._green; }
        public get blue(): number { return this._blue; }
        public get alpha(): number { return this._alpha; }

        public get redFload(): number { return this._red / 255; }
        public get greenFload(): number { return this._green / 255; }
        public get blueFload(): number { return this._blue / 255; }
        public get alphaFload(): number { return this._alpha / 255; }

        public toArray(): number[] { return [this._red, this._green, this._blue, this._alpha] }
        public toFloatArray(): number[] { return [this.redFload, this.greenFload, this.blueFload, this.alphaFload]; }
        public toFloat32Array(): Float32Array { return new Float32Array(this.toFloatArray()); }
    }
    
}