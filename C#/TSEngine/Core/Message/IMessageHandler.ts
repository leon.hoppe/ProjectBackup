﻿namespace TSE {

    export interface IMessageHanlder {

        onMessage(message: Message): void;

    }

}