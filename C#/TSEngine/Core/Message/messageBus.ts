﻿namespace TSE {

    export class MessageBus {

        private static _subscriptions: { [code: string]: IMessageHanlder[] } = {};

        private static _normalQueueMessagePerUpdate: number = 10;
        private static _normalMessageQueue: MessageSubscriptionNode[] = [];

        private constructor() { }

        public static addSubscription(code: string, handler: IMessageHanlder): void {
            if (MessageBus._subscriptions[code] === undefined)
                MessageBus._subscriptions[code] = [];

            if (MessageBus._subscriptions[code].indexOf(handler) !== -1)
                console.warn("Attempting to add a duplicate hanlder to code: '" + code + "'. Subscription not added.");
            else
                MessageBus._subscriptions[code].push(handler);
        }

        public static removeSubscription(code: string, handler: IMessageHanlder): void {
            if (MessageBus._subscriptions[code] === undefined) {
                console.warn("Cannot unsubscribe handler from code: '" + code + "'. Because that code is not subscribed to.");
                return;
            }

            let nodeIndex = MessageBus._subscriptions[code].indexOf(handler);
            if (nodeIndex !== -1)
                MessageBus._subscriptions[code].splice(nodeIndex, 1);
        }

        public static post(message: Message): void {
            console.log("Message posted:", message);
            let handlers = MessageBus._subscriptions[message.code];
            if (handlers === undefined) return;

            for (let handler of handlers) {
                if (message.priority === MessagePriority.HIGH) handler.onMessage(message);
                else MessageBus._normalMessageQueue.push(new MessageSubscriptionNode(message, handler));
            }
        }

        public static update(time: number): void {
            if (MessageBus._normalMessageQueue.length === 0) return;

            let limit = Math.min(MessageBus._normalQueueMessagePerUpdate, MessageBus._normalMessageQueue.length);
            for (let i = 0; i < limit; i++) {
                let node = MessageBus._normalMessageQueue.pop();
                node.handler.onMessage(node.message);
            }
        }

    }

}