﻿namespace TSE {

    export class MessageSubscriptionNode {

        public message: Message;
        public handler: IMessageHanlder;

        public constructor(message: Message, handler: IMessageHanlder) {
            this.message = message;
            this.handler = handler;
        }

    }

}