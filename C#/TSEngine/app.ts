﻿var engine: TSE.Engine;

var handleSize = function () {
    const canvas = document.getElementById("GameEngine") as HTMLCanvasElement;
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    engine.resize();
}

/**
 * The main entrypoint to the Application.
 * */
window.onload = function () {
    engine = new TSE.Engine("GameEngine");
    handleSize();
    engine.start();
}

window.onresize = handleSize;

