var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var engine;
var handleSize = function () {
    var canvas = document.getElementById("GameEngine");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    engine.resize();
};
window.onload = function () {
    engine = new TSE.Engine("GameEngine");
    handleSize();
    engine.start();
};
window.onresize = handleSize;
var TSE;
(function (TSE) {
    var Engine = (function () {
        function Engine(canvasId) {
            this._previousTime = 0;
            this._canvas = TSE.GLUtilities.initialize(canvasId);
        }
        Engine.prototype.start = function () {
            TSE.AssetManager.initialize();
            TSE.ZoneManager.initialize();
            TSE.gl.clearColor(0, 0, 0, 1);
            TSE.gl.enable(TSE.gl.BLEND);
            TSE.gl.blendFunc(TSE.gl.SRC_ALPHA, TSE.gl.ONE_MINUS_SRC_ALPHA);
            TSE.MaterialManager.registerMaterial(new TSE.Material("crate", "/assets/textures/crate.jpg"));
            TSE.MaterialManager.registerMaterial(new TSE.Material("goblin", "/assets/textures/goblin.png"));
            this._shader = new TSE.BasicShader();
            this._shader.use();
            this._projection = TSE.Matrix4x4.orthographic(0, this._canvas.width, this._canvas.height, 0, -100.0, 100.0);
            TSE.ZoneManager.changeZone(0);
            this.loop();
        };
        Engine.prototype.loop = function () {
            var delta = performance.now() - this._previousTime;
            TSE.MessageBus.update(delta);
            TSE.ZoneManager.update(delta);
            this._previousTime = performance.now();
            TSE.gl.clear(TSE.gl.COLOR_BUFFER_BIT);
            TSE.ZoneManager.render(this._shader);
            var projectionPosition = this._shader.getUniformLocation("u_projection");
            TSE.gl.uniformMatrix4fv(projectionPosition, false, new Float32Array(this._projection.data));
            requestAnimationFrame(this.loop.bind(this));
        };
        Engine.prototype.resize = function () {
            this._projection = TSE.Matrix4x4.orthographic(0, this._canvas.width, this._canvas.height, 0, -100.0, 100.0);
            TSE.gl.viewport(0, 0, this._canvas.width, this._canvas.height);
        };
        return Engine;
    }());
    TSE.Engine = Engine;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    TSE.MESSAGE_ASSET_LOADER_ASSET_LOADED = "MESSAGE_ASSET_LOADER_ASSET_LOADED::";
    var AssetManager = (function () {
        function AssetManager() {
        }
        AssetManager.initialize = function () {
            this.registerLoader(new TSE.ImageAssetLoader());
            this.registerLoader(new TSE.JsonAssetLoader());
        };
        AssetManager.registerLoader = function (loader) {
            AssetManager._loaders.push(loader);
        };
        AssetManager.onAssetLoaded = function (asset) {
            AssetManager._loadedAssets[asset.name] = asset;
            TSE.Message.send(TSE.MESSAGE_ASSET_LOADER_ASSET_LOADED + asset.name, this, asset);
        };
        AssetManager.loadAsset = function (assetName) {
            var extention = assetName.split('.').pop().toLowerCase();
            for (var _i = 0, _a = AssetManager._loaders; _i < _a.length; _i++) {
                var loader = _a[_i];
                if (loader.supportedExtensions.indexOf(extention) !== -1) {
                    loader.loadAsset(assetName);
                    return;
                }
            }
            console.warn("Unable to load asset with extention: '" + extention + "'. no loader was found.");
        };
        AssetManager.isAssetLoaded = function (assetName) {
            return AssetManager._loadedAssets[assetName] !== undefined;
        };
        AssetManager.getAsset = function (assetName) {
            if (AssetManager.isAssetLoaded(assetName)) {
                return AssetManager._loadedAssets[assetName];
            }
            else {
                AssetManager.loadAsset(assetName);
            }
            return undefined;
        };
        AssetManager._loaders = [];
        AssetManager._loadedAssets = {};
        return AssetManager;
    }());
    TSE.AssetManager = AssetManager;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var ImageAsset = (function () {
        function ImageAsset(name, data) {
            this.name = name;
            this.data = data;
        }
        Object.defineProperty(ImageAsset.prototype, "width", {
            get: function () {
                return this.data.width;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ImageAsset.prototype, "height", {
            get: function () {
                return this.data.height;
            },
            enumerable: false,
            configurable: true
        });
        return ImageAsset;
    }());
    TSE.ImageAsset = ImageAsset;
    var ImageAssetLoader = (function () {
        function ImageAssetLoader() {
        }
        Object.defineProperty(ImageAssetLoader.prototype, "supportedExtensions", {
            get: function () { return ["png", "gif", "jpg", "jpeg"]; },
            enumerable: false,
            configurable: true
        });
        ImageAssetLoader.prototype.loadAsset = function (assetName) {
            var image = new Image();
            image.onload = this.onImageLoaded.bind(this, assetName, image);
            image.src = assetName;
        };
        ImageAssetLoader.prototype.onImageLoaded = function (assetName, image) {
            console.log("onImageLoaded: assetName/image", assetName, image);
            var asset = new ImageAsset(assetName, image);
            TSE.AssetManager.onAssetLoaded(asset);
        };
        return ImageAssetLoader;
    }());
    TSE.ImageAssetLoader = ImageAssetLoader;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var JsonAsset = (function () {
        function JsonAsset(name, data) {
            this.name = name;
            this.data = data;
        }
        return JsonAsset;
    }());
    TSE.JsonAsset = JsonAsset;
    var JsonAssetLoader = (function () {
        function JsonAssetLoader() {
        }
        Object.defineProperty(JsonAssetLoader.prototype, "supportedExtensions", {
            get: function () { return ["json"]; },
            enumerable: false,
            configurable: true
        });
        JsonAssetLoader.prototype.loadAsset = function (assetName) {
            var request = new XMLHttpRequest();
            request.open("GET", assetName);
            request.addEventListener("load", this.onJsonLoaded.bind(this, assetName, request));
            request.send();
        };
        JsonAssetLoader.prototype.onJsonLoaded = function (assetName, request) {
            console.log("onJsonLoaded: assetName/request", assetName, request);
            if (request.readyState === request.DONE) {
                var json = JSON.parse(request.responseText);
                var asset = new JsonAsset(assetName, json);
                TSE.AssetManager.onAssetLoaded(asset);
            }
        };
        return JsonAssetLoader;
    }());
    TSE.JsonAssetLoader = JsonAssetLoader;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var BaseBehavior = (function () {
        function BaseBehavior(data) {
            this._data = data;
            this.name = data.name;
        }
        Object.defineProperty(BaseBehavior.prototype, "owner", {
            set: function (value) { this._owner = value; },
            enumerable: false,
            configurable: true
        });
        BaseBehavior.prototype.update = function (time) { };
        BaseBehavior.prototype.apply = function (userData) { };
        return BaseBehavior;
    }());
    TSE.BaseBehavior = BaseBehavior;
    var BehaviorManager = (function () {
        function BehaviorManager() {
        }
        BehaviorManager.registerBuilder = function (builder) {
            BehaviorManager._registeredBuilders[builder.type] = builder;
        };
        BehaviorManager.extractComponent = function (json) {
            if (BehaviorManager._registeredBuilders[json === null || json === void 0 ? void 0 : json.type] === undefined)
                return undefined;
            return BehaviorManager._registeredBuilders[json.type].buildFromJson(json);
        };
        BehaviorManager._registeredBuilders = {};
        return BehaviorManager;
    }());
    TSE.BehaviorManager = BehaviorManager;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var RotationBehaviorData = (function () {
        function RotationBehaviorData() {
            this.rotation = TSE.Vector3.zero;
        }
        RotationBehaviorData.prototype.setFromJson = function (json) {
            if ((json === null || json === void 0 ? void 0 : json.name) === undefined)
                return;
            this.name = String(json.name);
            this.rotation.setFromJson(json.rotation);
        };
        return RotationBehaviorData;
    }());
    TSE.RotationBehaviorData = RotationBehaviorData;
    var RotationBehaviorBuilder = (function () {
        function RotationBehaviorBuilder() {
        }
        RotationBehaviorBuilder.prototype.buildFromJson = function (json) {
            var data = new RotationBehaviorData();
            data.setFromJson(json);
            return new RotationBehavior(data);
        };
        Object.defineProperty(RotationBehaviorBuilder.prototype, "type", {
            get: function () { return "rotation"; },
            enumerable: false,
            configurable: true
        });
        return RotationBehaviorBuilder;
    }());
    TSE.RotationBehaviorBuilder = RotationBehaviorBuilder;
    var RotationBehavior = (function (_super) {
        __extends(RotationBehavior, _super);
        function RotationBehavior(data) {
            var _this = _super.call(this, data) || this;
            _this._rotation = data.rotation;
            return _this;
        }
        RotationBehavior.prototype.update = function (time) {
            this._owner.transform.rotation.add(this._rotation);
            _super.prototype.update.call(this, time);
        };
        return RotationBehavior;
    }(TSE.BaseBehavior));
    TSE.RotationBehavior = RotationBehavior;
    TSE.BehaviorManager.registerBuilder(new RotationBehaviorBuilder());
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var BaseComponent = (function () {
        function BaseComponent(data) {
            this._data = data;
            this.name = data.name;
        }
        Object.defineProperty(BaseComponent.prototype, "owner", {
            get: function () { return this._owner; },
            set: function (value) { this._owner = value; },
            enumerable: false,
            configurable: true
        });
        BaseComponent.prototype.load = function () { };
        BaseComponent.prototype.update = function (time) { };
        BaseComponent.prototype.render = function (shader) { };
        return BaseComponent;
    }());
    TSE.BaseComponent = BaseComponent;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var ComponentManager = (function () {
        function ComponentManager() {
        }
        ComponentManager.registerBuilder = function (builder) {
            ComponentManager._registeredBuilders[builder.type] = builder;
        };
        ComponentManager.extractComponent = function (json) {
            if (ComponentManager._registeredBuilders[json === null || json === void 0 ? void 0 : json.type] === undefined)
                return undefined;
            return ComponentManager._registeredBuilders[json.type].buildFromJson(json);
        };
        ComponentManager._registeredBuilders = {};
        return ComponentManager;
    }());
    TSE.ComponentManager = ComponentManager;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var SpriteComponentData = (function () {
        function SpriteComponentData() {
        }
        SpriteComponentData.prototype.setFromJson = function (json) {
            this.name = json.name !== undefined ? String(json.name) : undefined;
            this.materialName = json.materialName !== undefined ? String(json.materialName) : undefined;
        };
        return SpriteComponentData;
    }());
    TSE.SpriteComponentData = SpriteComponentData;
    var SpriteComponentBuilder = (function () {
        function SpriteComponentBuilder() {
        }
        Object.defineProperty(SpriteComponentBuilder.prototype, "type", {
            get: function () { return "sprite"; },
            enumerable: false,
            configurable: true
        });
        SpriteComponentBuilder.prototype.buildFromJson = function (json) {
            var data = new SpriteComponentData();
            data.setFromJson(json);
            return new SpriteComponent(data);
        };
        return SpriteComponentBuilder;
    }());
    TSE.SpriteComponentBuilder = SpriteComponentBuilder;
    var SpriteComponent = (function (_super) {
        __extends(SpriteComponent, _super);
        function SpriteComponent(data) {
            var _this = _super.call(this, data) || this;
            _this._sprite = new TSE.Sprite(data.name, data.materialName);
            return _this;
        }
        SpriteComponent.prototype.load = function () {
            this._sprite.load();
        };
        SpriteComponent.prototype.render = function (shader) {
            this._sprite.draw(shader, this.owner.worldMatrix);
            _super.prototype.render.call(this, shader);
        };
        return SpriteComponent;
    }(TSE.BaseComponent));
    TSE.SpriteComponent = SpriteComponent;
    var AnimatedSpriteComponentData = (function (_super) {
        __extends(AnimatedSpriteComponentData, _super);
        function AnimatedSpriteComponentData() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        AnimatedSpriteComponentData.prototype.setFromJson = function (json) {
            _super.prototype.setFromJson.call(this, json);
            this.frameWidth = Number(json.frameWidth);
            this.frameHeight = Number(json.frameHeight);
            this.frameCount = Number(json.frameCount);
            this.frameSequence = json.frameSequence;
        };
        return AnimatedSpriteComponentData;
    }(SpriteComponentData));
    TSE.AnimatedSpriteComponentData = AnimatedSpriteComponentData;
    var AnimatedSpriteComponentBuilder = (function (_super) {
        __extends(AnimatedSpriteComponentBuilder, _super);
        function AnimatedSpriteComponentBuilder() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Object.defineProperty(AnimatedSpriteComponentBuilder.prototype, "type", {
            get: function () { return "animated_sprite"; },
            enumerable: false,
            configurable: true
        });
        AnimatedSpriteComponentBuilder.prototype.buildFromJson = function (json) {
            var data = new AnimatedSpriteComponentData();
            data.setFromJson(json);
            return new AnimatedSpriteComponent(data);
        };
        return AnimatedSpriteComponentBuilder;
    }(SpriteComponentBuilder));
    TSE.AnimatedSpriteComponentBuilder = AnimatedSpriteComponentBuilder;
    var AnimatedSpriteComponent = (function (_super) {
        __extends(AnimatedSpriteComponent, _super);
        function AnimatedSpriteComponent(data) {
            var _this = _super.call(this, data) || this;
            _this._sprite = new TSE.AnimatedSprite(_this.name, data.materialName, data.frameWidth, data.frameHeight, data.frameCount, data.frameSequence);
            return _this;
        }
        AnimatedSpriteComponent.prototype.update = function (time) {
            this._sprite.update(time);
            _super.prototype.update.call(this, time);
        };
        return AnimatedSpriteComponent;
    }(SpriteComponent));
    TSE.AnimatedSpriteComponent = AnimatedSpriteComponent;
    TSE.ComponentManager.registerBuilder(new SpriteComponentBuilder());
    TSE.ComponentManager.registerBuilder(new AnimatedSpriteComponentBuilder());
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var GLUtilities = (function () {
        function GLUtilities() {
        }
        GLUtilities.initialize = function (elementId) {
            var canvas;
            if (elementId !== undefined)
                canvas = document.getElementById(elementId);
            if (elementId === undefined) {
                canvas = document.createElement("canvas");
                document.body.appendChild(canvas);
            }
            TSE.gl = canvas.getContext("webgl");
            if (TSE.gl === undefined)
                throw new Error("Unable to initialize WebGL");
            return canvas;
        };
        return GLUtilities;
    }());
    TSE.GLUtilities = GLUtilities;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var AttributeInfo = (function () {
        function AttributeInfo(location, size) {
            this.location = location;
            this.size = size;
            this.offset = 0;
        }
        return AttributeInfo;
    }());
    TSE.AttributeInfo = AttributeInfo;
    var GLBuffer = (function () {
        function GLBuffer(dataType, targetBufferType, mode) {
            if (dataType === void 0) { dataType = TSE.gl.FLOAT; }
            if (targetBufferType === void 0) { targetBufferType = TSE.gl.ARRAY_BUFFER; }
            if (mode === void 0) { mode = TSE.gl.TRIANGLES; }
            this._hasAttributeLocation = false;
            this._data = [];
            this._attributes = [];
            this._elementSize = 0;
            this._dataType = dataType;
            this._targetBufferType = targetBufferType;
            this._mode = mode;
            switch (this._dataType) {
                case TSE.gl.FLOAT:
                case TSE.gl.INT:
                case TSE.gl.UNSIGNED_INT:
                    this._typeSize = 4;
                    break;
                case TSE.gl.SHORT:
                case TSE.gl.UNSIGNED_SHORT:
                    this._typeSize = 2;
                    break;
                case TSE.gl.BYTE:
                case TSE.gl.UNSIGNED_BYTE:
                    this._typeSize = 1;
                    break;
                default:
                    throw new Error("Unrecognized data type: " + dataType.toString());
            }
            this._buffer = TSE.gl.createBuffer();
        }
        GLBuffer.prototype.destroy = function () {
            TSE.gl.deleteBuffer(this._buffer);
        };
        GLBuffer.prototype.bind = function (normalized) {
            if (normalized === void 0) { normalized = false; }
            TSE.gl.bindBuffer(this._targetBufferType, this._buffer);
            if (this._hasAttributeLocation) {
                for (var _i = 0, _a = this._attributes; _i < _a.length; _i++) {
                    var it = _a[_i];
                    TSE.gl.vertexAttribPointer(it.location, it.size, this._dataType, normalized, this._stride, it.offset * this._typeSize);
                    TSE.gl.enableVertexAttribArray(it.location);
                }
            }
        };
        GLBuffer.prototype.unbind = function () {
            if (this._hasAttributeLocation) {
                for (var _i = 0, _a = this._attributes; _i < _a.length; _i++) {
                    var it = _a[_i];
                    TSE.gl.disableVertexAttribArray(it.location);
                }
            }
            TSE.gl.bindBuffer(this._targetBufferType, undefined);
        };
        GLBuffer.prototype.addAttributeLocation = function (info) {
            this._hasAttributeLocation = true;
            info.offset = this._elementSize;
            this._attributes.push(info);
            this._elementSize += info.size;
            this._stride = this._elementSize * this._typeSize;
        };
        GLBuffer.prototype.pushBackData = function (data) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var d = data_1[_i];
                this._data.push(d);
            }
        };
        GLBuffer.prototype.clearData = function () {
            this._data.length = 0;
        };
        GLBuffer.prototype.setData = function (data) {
            this.clearData();
            this.pushBackData(data);
        };
        GLBuffer.prototype.upload = function () {
            TSE.gl.bindBuffer(this._targetBufferType, this._buffer);
            var bufferData;
            switch (this._dataType) {
                case TSE.gl.FLOAT:
                    bufferData = new Float32Array(this._data);
                    break;
                case TSE.gl.INT:
                    bufferData = new Int32Array(this._data);
                    break;
                case TSE.gl.UNSIGNED_INT:
                    bufferData = new Uint32Array(this._data);
                    break;
                case TSE.gl.SHORT:
                    bufferData = new Int16Array(this._data);
                    break;
                case TSE.gl.UNSIGNED_SHORT:
                    bufferData = new Uint16Array(this._data);
                    break;
                case TSE.gl.BYTE:
                    bufferData = new Int8Array(this._data);
                    break;
                case TSE.gl.UNSIGNED_BYTE:
                    bufferData = new Uint8Array(this._data);
                    break;
            }
            TSE.gl.bufferData(this._targetBufferType, bufferData, TSE.gl.STATIC_DRAW);
        };
        GLBuffer.prototype.draw = function () {
            if (this._targetBufferType === TSE.gl.ARRAY_BUFFER)
                TSE.gl.drawArrays(this._mode, 0, this._data.length / this._elementSize);
            if (this._targetBufferType === TSE.gl.ELEMENT_ARRAY_BUFFER)
                TSE.gl.drawElements(this._mode, this._data.length, this._dataType, 0);
        };
        return GLBuffer;
    }());
    TSE.GLBuffer = GLBuffer;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Shader = (function () {
        function Shader(name) {
            this._attributes = {};
            this._uniforms = {};
            this._name = name;
        }
        Shader.prototype.setSource = function (vertexSource, fragementSource) {
            var vertexShader = this.loadShader(vertexSource, TSE.gl.VERTEX_SHADER);
            var fragmentShader = this.loadShader(fragementSource, TSE.gl.FRAGMENT_SHADER);
            this.createProgram(vertexShader, fragmentShader);
            this.detectAtrributes();
            this.detectUniforms();
        };
        Shader.prototype.use = function () {
            TSE.gl.useProgram(this._program);
        };
        Shader.prototype.loadShader = function (source, shaderType) {
            var shader = TSE.gl.createShader(shaderType);
            TSE.gl.shaderSource(shader, source);
            TSE.gl.compileShader(shader);
            var error = TSE.gl.getShaderInfoLog(shader);
            if (error != "")
                throw new Error("Error compiling shader '" + this._name + "': " + error);
            return shader;
        };
        Object.defineProperty(Shader.prototype, "name", {
            get: function () { return this._name; },
            enumerable: false,
            configurable: true
        });
        Shader.prototype.getAttributeLocation = function (name) {
            if (this._attributes[name] === undefined)
                throw new Error("Unable to find Attribute '".concat(name, "' in shader '").concat(this._name, "'"));
            return this._attributes[name];
        };
        Shader.prototype.getUniformLocation = function (name) {
            if (this._uniforms[name] === undefined)
                throw new Error("Unable to find Uniform '".concat(name, "' in shader '").concat(this._name, "'"));
            return this._uniforms[name];
        };
        Shader.prototype.createProgram = function (vertexShader, fragmentShader) {
            this._program = TSE.gl.createProgram();
            TSE.gl.attachShader(this._program, vertexShader);
            TSE.gl.attachShader(this._program, fragmentShader);
            TSE.gl.linkProgram(this._program);
            var error = TSE.gl.getProgramInfoLog(this._program);
            if (error != "")
                throw new Error("Error linking shader '" + this._name + "': " + error);
        };
        Shader.prototype.detectAtrributes = function () {
            var attributeCount = TSE.gl.getProgramParameter(this._program, TSE.gl.ACTIVE_ATTRIBUTES);
            for (var i = 0; i < attributeCount; i++) {
                var attributeInfo = TSE.gl.getActiveAttrib(this._program, i);
                if (!attributeInfo)
                    break;
                this._attributes[attributeInfo.name] = TSE.gl.getAttribLocation(this._program, attributeInfo.name);
            }
        };
        Shader.prototype.detectUniforms = function () {
            var uniformCount = TSE.gl.getProgramParameter(this._program, TSE.gl.ACTIVE_UNIFORMS);
            for (var i = 0; i < uniformCount; i++) {
                var uniformInfo = TSE.gl.getActiveUniform(this._program, i);
                if (!uniformInfo)
                    break;
                this._uniforms[uniformInfo.name] = TSE.gl.getUniformLocation(this._program, uniformInfo.name);
            }
        };
        return Shader;
    }());
    TSE.Shader = Shader;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Color = (function () {
        function Color(red, green, blue, alpha) {
            if (red === void 0) { red = 255; }
            if (green === void 0) { green = 255; }
            if (blue === void 0) { blue = 255; }
            if (alpha === void 0) { alpha = 255; }
            this._red = red;
            this._green = green;
            this._blue = blue;
            this._alpha = alpha;
        }
        Object.defineProperty(Color, "white", {
            get: function () { return new Color(255, 255, 255, 255); },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color, "black", {
            get: function () { return new Color(0, 0, 0, 255); },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(Color, "red", {
            get: function () { return new Color(255, 0, 0, 255); },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(Color, "green", {
            get: function () { return new Color(0, 255, 0, 255); },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(Color, "blue", {
            get: function () { return new Color(0, 0, 255, 255); },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(Color.prototype, "red", {
            get: function () { return this._red; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "green", {
            get: function () { return this._green; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "blue", {
            get: function () { return this._blue; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "alpha", {
            get: function () { return this._alpha; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "redFload", {
            get: function () { return this._red / 255; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "greenFload", {
            get: function () { return this._green / 255; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "blueFload", {
            get: function () { return this._blue / 255; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Color.prototype, "alphaFload", {
            get: function () { return this._alpha / 255; },
            enumerable: false,
            configurable: true
        });
        Color.prototype.toArray = function () { return [this._red, this._green, this._blue, this._alpha]; };
        Color.prototype.toFloatArray = function () { return [this.redFload, this.greenFload, this.blueFload, this.alphaFload]; };
        Color.prototype.toFloat32Array = function () { return new Float32Array(this.toFloatArray()); };
        return Color;
    }());
    TSE.Color = Color;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Material = (function () {
        function Material(name, diffuseTextureName, tint) {
            if (tint === void 0) { tint = TSE.Color.white; }
            this._name = name;
            this._diffuseTextureName = diffuseTextureName;
            this._tint = tint;
            if (this._diffuseTextureName !== undefined)
                this._diffuseTexture = TSE.TextureManager.getTexture(this._diffuseTextureName);
        }
        Object.defineProperty(Material.prototype, "name", {
            get: function () { return this._name; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Material.prototype, "diffuseTextureName", {
            get: function () { return this._diffuseTextureName; },
            set: function (value) {
                if (this._diffuseTexture !== undefined)
                    TSE.TextureManager.releaseTexture(this._diffuseTextureName);
                if (value === undefined)
                    return;
                this.diffuseTextureName = value;
                this._diffuseTexture = TSE.TextureManager.getTexture(this._diffuseTextureName);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Material.prototype, "diffuseTexture", {
            get: function () { return this._diffuseTexture; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Material.prototype, "tint", {
            get: function () { return this._tint; },
            enumerable: false,
            configurable: true
        });
        Material.prototype.destroy = function () {
            TSE.TextureManager.releaseTexture(this._diffuseTextureName);
            this._diffuseTexture = undefined;
        };
        return Material;
    }());
    TSE.Material = Material;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var MaterialReferenceNode = (function () {
        function MaterialReferenceNode(material) {
            this.referenceCount = 1;
            this.material = material;
        }
        return MaterialReferenceNode;
    }());
    var MaterialManager = (function () {
        function MaterialManager() {
        }
        MaterialManager.registerMaterial = function (material) {
            if (MaterialManager._materials[material.name] === undefined)
                MaterialManager._materials[material.name] = new MaterialReferenceNode(material);
        };
        MaterialManager.getMaterial = function (materialName) {
            if (MaterialManager._materials[materialName] === undefined)
                return undefined;
            MaterialManager._materials[materialName].referenceCount++;
            return MaterialManager._materials[materialName].material;
        };
        MaterialManager.releaseMaterial = function (materialName) {
            if (MaterialManager._materials[materialName] === undefined)
                console.warn("Cannot release an undefined Material!");
            else {
                MaterialManager._materials[materialName].referenceCount--;
                if (MaterialManager._materials[materialName].referenceCount < 1) {
                    MaterialManager._materials[materialName].material.destroy();
                    delete MaterialManager._materials[materialName];
                }
            }
        };
        MaterialManager._materials = {};
        return MaterialManager;
    }());
    TSE.MaterialManager = MaterialManager;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Sprite = (function () {
        function Sprite(name, materialName, width, height) {
            if (width === void 0) { width = 100; }
            if (height === void 0) { height = 100; }
            this._vertices = [];
            this._name = name;
            this._width = width;
            this._height = height;
            this._material = TSE.MaterialManager.getMaterial(materialName);
        }
        Sprite.prototype.load = function () {
            this._buffer = new TSE.GLBuffer();
            this._buffer.addAttributeLocation(new TSE.AttributeInfo(0, 3));
            this._buffer.addAttributeLocation(new TSE.AttributeInfo(1, 2));
            this._vertices = [
                new TSE.Vertex(0, 0, 0, 0, 0),
                new TSE.Vertex(0, this._height, 0, 0, 1.0),
                new TSE.Vertex(this._width, this._height, 0, 1.0, 1.0),
                new TSE.Vertex(this._width, this._height, 0, 1.0, 1.0),
                new TSE.Vertex(this._width, 0, 0, 1.0, 0),
                new TSE.Vertex(0, 0, 0, 0, 0)
            ];
            this._buffer.pushBackData(TSE.Vertex.vertexArray(this._vertices));
            this._buffer.upload();
            this._buffer.unbind();
        };
        Sprite.prototype.destroy = function () {
            this._buffer.destroy();
            TSE.MaterialManager.releaseMaterial(this._material.name);
            delete this._material;
        };
        Sprite.prototype.update = function (time) {
        };
        Sprite.prototype.draw = function (shader, model) {
            var modelLocation = shader.getUniformLocation("u_model");
            TSE.gl.uniformMatrix4fv(modelLocation, false, model.toFloat32Array());
            var colorLocation = shader.getUniformLocation("u_tint");
            TSE.gl.uniform4fv(colorLocation, this._material.tint.toFloat32Array());
            if (this._material.diffuseTexture !== undefined) {
                this._material.diffuseTexture.activateAndBind(0);
                var diffuseLocation = shader.getUniformLocation("u_diffuse");
                TSE.gl.uniform1i(diffuseLocation, 0);
            }
            this._buffer.bind();
            this._buffer.draw();
        };
        Object.defineProperty(Sprite.prototype, "name", {
            get: function () { return this._name; },
            enumerable: false,
            configurable: true
        });
        return Sprite;
    }());
    TSE.Sprite = Sprite;
    var AnimatedSprite = (function (_super) {
        __extends(AnimatedSprite, _super);
        function AnimatedSprite(name, materialName, frameWidth, frameHeight, frameCount, frameSequence) {
            if (frameWidth === void 0) { frameWidth = 10; }
            if (frameHeight === void 0) { frameHeight = 10; }
            if (frameCount === void 0) { frameCount = 1; }
            if (frameSequence === void 0) { frameSequence = []; }
            var _this = _super.call(this, name, materialName, frameWidth, frameHeight) || this;
            _this._currentFrame = 0;
            _this.currentTime = 0;
            _this._frameTime = 300;
            _this._loaded = false;
            _this._frameWidth = frameWidth;
            _this._frameHeight = frameHeight;
            _this._frameCount = frameCount;
            _this._frameSequence = frameSequence;
            return _this;
        }
        AnimatedSprite.prototype.load = function () {
            _super.prototype.load.call(this);
            this.calculateUVs();
        };
        AnimatedSprite.prototype.calculateUVs = function () {
            var _this = this;
            setTimeout(function () {
                _this._frameUVs = [];
                var totalWidth = 0;
                var totalHeight = 0;
                for (var i = 0; i < _this._frameCount; i++) {
                    totalWidth += _this._frameWidth;
                    if (totalWidth > _this._material.diffuseTexture.width) {
                        totalWidth = 0;
                        totalHeight++;
                    }
                    var u = (i * _this._frameWidth) / _this._material.diffuseTexture.width;
                    var v = (totalHeight * _this._frameHeight) / _this._material.diffuseTexture.height;
                    var um = ((i * _this._frameWidth) + _this._frameWidth) / _this._material.diffuseTexture.width;
                    var vm = ((totalHeight * _this._frameHeight) + _this._frameHeight) / _this._material.diffuseTexture.height;
                    _this._frameUVs.push([new TSE.Vector2(u, v), new TSE.Vector2(um, vm)]);
                }
                _this._loaded = true;
            }, 0);
        };
        AnimatedSprite.prototype.update = function (time) {
            if (!this._loaded)
                return;
            this.currentTime += time;
            if (this.currentTime > this._frameTime) {
                this._currentFrame++;
                this.currentTime = 0;
                if (this._currentFrame >= this._frameSequence.length)
                    this._currentFrame = 0;
                var frameUVs = this._frameUVs[this._frameSequence[this._currentFrame]];
                this._vertices[0].texCoords.copyFrom(frameUVs[0]);
                this._vertices[1].texCoords = new TSE.Vector2(frameUVs[0].x, frameUVs[1].y);
                this._vertices[2].texCoords.copyFrom(frameUVs[1]);
                this._vertices[3].texCoords.copyFrom(frameUVs[1]);
                this._vertices[4].texCoords = new TSE.Vector2(frameUVs[1].x, frameUVs[0].y);
                this._vertices[5].texCoords.copyFrom(frameUVs[0]);
                this._buffer.setData(TSE.Vertex.vertexArray(this._vertices));
                this._buffer.upload();
                this._buffer.unbind();
            }
            _super.prototype.update.call(this, time);
        };
        return AnimatedSprite;
    }(Sprite));
    TSE.AnimatedSprite = AnimatedSprite;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var LEVEL = 0;
    var BORDER = 0;
    var TEMP_IMAGE_DATA = new Uint8Array([255, 255, 255, 255]);
    var Texture = (function () {
        function Texture(name, width, height) {
            if (width === void 0) { width = 1; }
            if (height === void 0) { height = 1; }
            this._isLoaded = false;
            this._name = name;
            this._width = width;
            this._height = height;
            this._handle = TSE.gl.createTexture();
            TSE.Message.subscribe(TSE.MESSAGE_ASSET_LOADER_ASSET_LOADED + this._name, this);
            this.bind();
            TSE.gl.texImage2D(TSE.gl.TEXTURE_2D, LEVEL, TSE.gl.RGBA, 1, 1, BORDER, TSE.gl.RGBA, TSE.gl.UNSIGNED_BYTE, TEMP_IMAGE_DATA);
            var asset = TSE.AssetManager.getAsset(this._name);
            if (asset !== undefined) {
                this.loadTextureFromAsset(asset);
            }
        }
        Texture.prototype.onMessage = function (message) {
            if (message.code === TSE.MESSAGE_ASSET_LOADER_ASSET_LOADED + this._name) {
                this.loadTextureFromAsset(message.context);
            }
        };
        Object.defineProperty(Texture.prototype, "name", {
            get: function () { return this._name; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Texture.prototype, "isLoaded", {
            get: function () { return this._isLoaded; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Texture.prototype, "width", {
            get: function () { return this._width; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Texture.prototype, "height", {
            get: function () { return this._height; },
            enumerable: false,
            configurable: true
        });
        Texture.prototype.activateAndBind = function (textureUnit) {
            if (textureUnit === void 0) { textureUnit = 0; }
            TSE.gl.activeTexture(TSE.gl.TEXTURE0 + textureUnit);
            this.bind();
        };
        Texture.prototype.bind = function () {
            TSE.gl.bindTexture(TSE.gl.TEXTURE_2D, this._handle);
        };
        Texture.prototype.unbind = function () {
            TSE.gl.bindTexture(TSE.gl.TEXTURE_2D, undefined);
        };
        Texture.prototype.destroy = function () {
            TSE.gl.deleteTexture(this._handle);
        };
        Texture.prototype.loadTextureFromAsset = function (asset) {
            this._width = asset.width;
            this._height = asset.height;
            this.bind();
            TSE.gl.texImage2D(TSE.gl.TEXTURE_2D, LEVEL, TSE.gl.RGBA, TSE.gl.RGBA, TSE.gl.UNSIGNED_BYTE, asset.data);
            if (this.isPowerOf2()) {
                TSE.gl.generateMipmap(TSE.gl.TEXTURE_2D);
            }
            else {
                TSE.gl.texParameteri(TSE.gl.TEXTURE_2D, TSE.gl.TEXTURE_WRAP_S, TSE.gl.CLAMP_TO_EDGE);
                TSE.gl.texParameteri(TSE.gl.TEXTURE_2D, TSE.gl.TEXTURE_WRAP_T, TSE.gl.CLAMP_TO_EDGE);
            }
            TSE.gl.texParameteri(TSE.gl.TEXTURE_2D, TSE.gl.TEXTURE_MIN_FILTER, TSE.gl.NEAREST);
            TSE.gl.texParameteri(TSE.gl.TEXTURE_2D, TSE.gl.TEXTURE_MAG_FILTER, TSE.gl.NEAREST);
            this._isLoaded = true;
        };
        Texture.prototype.isPowerOf2 = function () {
            return (this.isValuePowerOf2(this.width) && this.isValuePowerOf2(this.height));
        };
        Texture.prototype.isValuePowerOf2 = function (value) {
            return (value & (value - 1)) == 0;
        };
        return Texture;
    }());
    TSE.Texture = Texture;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var TextureReferenceNode = (function () {
        function TextureReferenceNode(texture) {
            this.referenceCount = 1;
            this.texture = texture;
        }
        return TextureReferenceNode;
    }());
    var TextureManager = (function () {
        function TextureManager() {
        }
        TextureManager.getTexture = function (textureName) {
            if (TextureManager._textures[textureName] === undefined)
                TextureManager._textures[textureName] = new TextureReferenceNode(new TSE.Texture(textureName));
            else
                TextureManager._textures[textureName].referenceCount++;
            return TextureManager._textures[textureName].texture;
        };
        TextureManager.releaseTexture = function (textureName) {
            if (TextureManager._textures[textureName] === undefined) {
                console.warn("A Texture named ".concat(textureName, " does not exist and cannot be released!"));
            }
            else {
                TextureManager._textures[textureName].referenceCount--;
                if (TextureManager._textures[textureName].referenceCount < 1) {
                    TextureManager._textures[textureName].texture.destroy();
                    TextureManager._textures[textureName] = undefined;
                    delete TextureManager._textures[textureName];
                }
            }
        };
        TextureManager._textures = {};
        return TextureManager;
    }());
    TSE.TextureManager = TextureManager;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Vertex = (function () {
        function Vertex(x, y, z, u, v) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (z === void 0) { z = 0; }
            if (u === void 0) { u = 0; }
            if (v === void 0) { v = 0; }
            this.position = new TSE.Vector3(x, y, z);
            this.texCoords = new TSE.Vector2(u, v);
        }
        Vertex.vertexArray = function (vertices) {
            var array = [];
            for (var _i = 0, vertices_1 = vertices; _i < vertices_1.length; _i++) {
                var vertex = vertices_1[_i];
                array = array.concat(vertex.toArray());
            }
            return array;
        };
        Vertex.prototype.toArray = function () {
            var array = [];
            array = array.concat(this.position.toArray(), this.texCoords.toArray());
            return array;
        };
        Vertex.prototype.toFloat32Array = function () { return new Float32Array(this.toArray()); };
        return Vertex;
    }());
    TSE.Vertex = Vertex;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Matrix4x4 = (function () {
        function Matrix4x4() {
            this._data = [];
            this._data = [
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            ];
        }
        Object.defineProperty(Matrix4x4.prototype, "data", {
            get: function () {
                return this._data;
            },
            enumerable: false,
            configurable: true
        });
        Matrix4x4.identity = function () {
            return new Matrix4x4();
        };
        Matrix4x4.orthographic = function (left, right, bottom, top, nearClip, farClip) {
            var matrix = new Matrix4x4();
            var lr = 1.0 / (left - right);
            var bt = 1.0 / (bottom - top);
            var nf = 1.0 / (nearClip - farClip);
            matrix._data[0] = -2.0 * lr;
            matrix._data[5] = -2.0 * bt;
            matrix._data[10] = 2.0 * nf;
            matrix._data[12] = (left + right) * lr;
            matrix._data[13] = (top + bottom) * bt;
            matrix._data[14] = (farClip + nearClip) * nf;
            return matrix;
        };
        Matrix4x4.translation = function (position) {
            var matrix = new Matrix4x4();
            matrix._data[12] = position.x;
            matrix._data[13] = position.y;
            matrix._data[14] = position.z;
            return matrix;
        };
        Matrix4x4.rotationX = function (angle) {
            var matrix = new Matrix4x4();
            var c = Math.cos(angle);
            var s = Math.sin(angle);
            matrix._data[5] = c;
            matrix._data[6] = s;
            matrix._data[9] = -s;
            matrix._data[10] = c;
            return matrix;
        };
        Matrix4x4.rotationY = function (angle) {
            var matrix = new Matrix4x4();
            var c = Math.cos(angle);
            var s = Math.sin(angle);
            matrix._data[0] = c;
            matrix._data[2] = -s;
            matrix._data[8] = s;
            matrix._data[10] = c;
            return matrix;
        };
        Matrix4x4.rotationZ = function (angle) {
            var matrix = new Matrix4x4();
            var c = Math.cos(angle);
            var s = Math.sin(angle);
            matrix._data[0] = c;
            matrix._data[1] = s;
            matrix._data[4] = -s;
            matrix._data[5] = c;
            return matrix;
        };
        Matrix4x4.rotationXYZ = function (x, y, z) {
            return Matrix4x4.multiply3(Matrix4x4.rotationZ(z), Matrix4x4.rotationY(y), Matrix4x4.rotationX(x));
        };
        Matrix4x4.scale = function (scale) {
            var matrix = new Matrix4x4();
            matrix._data[0] = scale.x;
            matrix._data[5] = scale.y;
            matrix._data[10] = scale.z;
            return matrix;
        };
        Matrix4x4.multiply = function (a, b) {
            var matrix = new Matrix4x4();
            var b00 = b._data[0 * 4 + 0];
            var b01 = b._data[0 * 4 + 1];
            var b02 = b._data[0 * 4 + 2];
            var b03 = b._data[0 * 4 + 3];
            var b10 = b._data[1 * 4 + 0];
            var b11 = b._data[1 * 4 + 1];
            var b12 = b._data[1 * 4 + 2];
            var b13 = b._data[1 * 4 + 3];
            var b20 = b._data[2 * 4 + 0];
            var b21 = b._data[2 * 4 + 1];
            var b22 = b._data[2 * 4 + 2];
            var b23 = b._data[2 * 4 + 3];
            var b30 = b._data[3 * 4 + 0];
            var b31 = b._data[3 * 4 + 1];
            var b32 = b._data[3 * 4 + 2];
            var b33 = b._data[3 * 4 + 3];
            var a00 = a._data[0 * 4 + 0];
            var a01 = a._data[0 * 4 + 1];
            var a02 = a._data[0 * 4 + 2];
            var a03 = a._data[0 * 4 + 3];
            var a10 = a._data[1 * 4 + 0];
            var a11 = a._data[1 * 4 + 1];
            var a12 = a._data[1 * 4 + 2];
            var a13 = a._data[1 * 4 + 3];
            var a20 = a._data[2 * 4 + 0];
            var a21 = a._data[2 * 4 + 1];
            var a22 = a._data[2 * 4 + 2];
            var a23 = a._data[2 * 4 + 3];
            var a30 = a._data[3 * 4 + 0];
            var a31 = a._data[3 * 4 + 1];
            var a32 = a._data[3 * 4 + 2];
            var a33 = a._data[3 * 4 + 3];
            matrix._data[0] = b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30;
            matrix._data[1] = b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31;
            matrix._data[2] = b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32;
            matrix._data[3] = b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33;
            matrix._data[4] = b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30;
            matrix._data[5] = b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31;
            matrix._data[6] = b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32;
            matrix._data[7] = b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33;
            matrix._data[8] = b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30;
            matrix._data[9] = b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31;
            matrix._data[10] = b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32;
            matrix._data[11] = b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33;
            matrix._data[12] = b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30;
            matrix._data[13] = b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31;
            matrix._data[14] = b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32;
            matrix._data[15] = b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33;
            return matrix;
        };
        Matrix4x4.multiply3 = function (a, b, c) {
            return Matrix4x4.multiply(Matrix4x4.multiply(a, b), c);
        };
        Matrix4x4.prototype.toFloat32Array = function () { return new Float32Array(this._data); };
        Matrix4x4.prototype.copyFrom = function (matrix) {
            for (var i = 0; i < this._data.length; i++) {
                this._data[i] = matrix._data[i];
            }
        };
        return Matrix4x4;
    }());
    TSE.Matrix4x4 = Matrix4x4;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Transform = (function () {
        function Transform() {
            this.position = TSE.Vector3.zero;
            this.rotation = TSE.Vector3.zero;
            this.scale = TSE.Vector3.one;
        }
        Transform.prototype.copyFrom = function (transform) {
            this.position.copyFrom(transform.position);
            this.rotation.copyFrom(transform.rotation);
            this.scale.copyFrom(transform.scale);
        };
        Object.defineProperty(Transform.prototype, "transformationMatrix", {
            get: function () {
                var translation = TSE.Matrix4x4.translation(this.position);
                var rotation = TSE.Matrix4x4.rotationXYZ(this.rotation.x, this.rotation.y, this.rotation.z);
                var scale = TSE.Matrix4x4.scale(this.scale);
                return TSE.Matrix4x4.multiply3(translation, rotation, scale);
            },
            enumerable: false,
            configurable: true
        });
        Transform.prototype.setFromJson = function (json) {
            if (json.position !== undefined)
                this.position.setFromJson(json.position);
            if (json.rotation !== undefined)
                this.rotation.setFromJson(json.rotation);
            if (json.scale !== undefined)
                this.scale.setFromJson(json.scale);
        };
        return Transform;
    }());
    TSE.Transform = Transform;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Vector2 = (function () {
        function Vector2(x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            this._x = x;
            this._y = y;
        }
        Object.defineProperty(Vector2.prototype, "x", {
            get: function () { return this._x; },
            set: function (value) { this._x = value; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Vector2.prototype, "y", {
            get: function () { return this._y; },
            set: function (value) { this._y = value; },
            enumerable: false,
            configurable: true
        });
        Vector2.prototype.toArray = function () { return [this._x, this._y]; };
        Vector2.prototype.toFloat32Array = function () { return new Float32Array(this.toArray()); };
        Vector2.prototype.setFromJson = function (json) {
            this._x = json.x !== undefined ? Number(json.x) : 0;
            this._y = json.y !== undefined ? Number(json.y) : 0;
        };
        Vector2.prototype.copyFrom = function (vector) {
            this._x = vector._x;
            this._y = vector._y;
            return this;
        };
        return Vector2;
    }());
    TSE.Vector2 = Vector2;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Vector3 = (function () {
        function Vector3(x, y, z) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (z === void 0) { z = 0; }
            this._x = x;
            this._y = y;
            this._z = z;
        }
        Object.defineProperty(Vector3, "zero", {
            get: function () { return new Vector3(0, 0, 0); },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Vector3, "one", {
            get: function () { return new Vector3(1, 1, 1); },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Vector3.prototype, "x", {
            get: function () { return this._x; },
            set: function (value) { this._x = value; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Vector3.prototype, "y", {
            get: function () { return this._y; },
            set: function (value) { this._y = value; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Vector3.prototype, "z", {
            get: function () { return this._z; },
            set: function (value) { this._z = value; },
            enumerable: false,
            configurable: true
        });
        Vector3.prototype.toArray = function () { return [this._x, this._y, this._z]; };
        Vector3.prototype.toFloat32Array = function () { return new Float32Array(this.toArray()); };
        Vector3.prototype.copyFrom = function (vector) {
            this._x = vector._x;
            this._y = vector._y;
            this._z = vector._z;
        };
        Vector3.prototype.setFromJson = function (json) {
            this._x = json.x !== undefined ? Number(json.x) : 0;
            this._y = json.y !== undefined ? Number(json.y) : 0;
            this._z = json.z !== undefined ? Number(json.z) : 0;
        };
        Vector3.prototype.add = function (vector) {
            this._x += vector._x;
            this._y += vector._y;
            this._z += vector._z;
            return this;
        };
        Vector3.prototype.subtract = function (vector) {
            this._x -= vector._x;
            this._y -= vector._y;
            this._z -= vector._z;
            return this;
        };
        Vector3.prototype.multiply = function (vector) {
            this._x *= vector._x;
            this._y *= vector._y;
            this._z *= vector._z;
            return this;
        };
        Vector3.prototype.divide = function (vector) {
            this._x /= vector._x;
            this._y /= vector._y;
            this._z /= vector._z;
            return this;
        };
        return Vector3;
    }());
    TSE.Vector3 = Vector3;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var MessagePriority;
    (function (MessagePriority) {
        MessagePriority[MessagePriority["NORMAL"] = 0] = "NORMAL";
        MessagePriority[MessagePriority["HIGH"] = 1] = "HIGH";
    })(MessagePriority = TSE.MessagePriority || (TSE.MessagePriority = {}));
    var Message = (function () {
        function Message(code, sender, context, priority) {
            if (priority === void 0) { priority = MessagePriority.NORMAL; }
            this.code = code;
            this.context = context;
            this.sender = sender;
            this.priority = priority;
        }
        Message.send = function (code, sender, context) {
            TSE.MessageBus.post(new Message(code, sender, context));
        };
        Message.sendPriority = function (code, sender, context) {
            TSE.MessageBus.post(new Message(code, sender, context, MessagePriority.HIGH));
        };
        Message.subscribe = function (code, handler) {
            TSE.MessageBus.addSubscription(code, handler);
        };
        Message.unsubscribe = function (code, handler) {
            TSE.MessageBus.removeSubscription(code, handler);
        };
        return Message;
    }());
    TSE.Message = Message;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var MessageBus = (function () {
        function MessageBus() {
        }
        MessageBus.addSubscription = function (code, handler) {
            if (MessageBus._subscriptions[code] === undefined)
                MessageBus._subscriptions[code] = [];
            if (MessageBus._subscriptions[code].indexOf(handler) !== -1)
                console.warn("Attempting to add a duplicate hanlder to code: '" + code + "'. Subscription not added.");
            else
                MessageBus._subscriptions[code].push(handler);
        };
        MessageBus.removeSubscription = function (code, handler) {
            if (MessageBus._subscriptions[code] === undefined) {
                console.warn("Cannot unsubscribe handler from code: '" + code + "'. Because that code is not subscribed to.");
                return;
            }
            var nodeIndex = MessageBus._subscriptions[code].indexOf(handler);
            if (nodeIndex !== -1)
                MessageBus._subscriptions[code].splice(nodeIndex, 1);
        };
        MessageBus.post = function (message) {
            console.log("Message posted:", message);
            var handlers = MessageBus._subscriptions[message.code];
            if (handlers === undefined)
                return;
            for (var _i = 0, handlers_1 = handlers; _i < handlers_1.length; _i++) {
                var handler = handlers_1[_i];
                if (message.priority === TSE.MessagePriority.HIGH)
                    handler.onMessage(message);
                else
                    MessageBus._normalMessageQueue.push(new TSE.MessageSubscriptionNode(message, handler));
            }
        };
        MessageBus.update = function (time) {
            if (MessageBus._normalMessageQueue.length === 0)
                return;
            var limit = Math.min(MessageBus._normalQueueMessagePerUpdate, MessageBus._normalMessageQueue.length);
            for (var i = 0; i < limit; i++) {
                var node = MessageBus._normalMessageQueue.pop();
                node.handler.onMessage(node.message);
            }
        };
        MessageBus._subscriptions = {};
        MessageBus._normalQueueMessagePerUpdate = 10;
        MessageBus._normalMessageQueue = [];
        return MessageBus;
    }());
    TSE.MessageBus = MessageBus;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var MessageSubscriptionNode = (function () {
        function MessageSubscriptionNode(message, handler) {
            this.message = message;
            this.handler = handler;
        }
        return MessageSubscriptionNode;
    }());
    TSE.MessageSubscriptionNode = MessageSubscriptionNode;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var BasicShader = (function (_super) {
        __extends(BasicShader, _super);
        function BasicShader() {
            var _this = _super.call(this, "basic") || this;
            _this.setSource(_this.getVertexSource(), _this.getFragmentSource());
            return _this;
        }
        BasicShader.prototype.getVertexSource = function () {
            return "\n                attribute vec3 a_position;\n                attribute vec2 a_texCoord;\n\n                uniform mat4 u_projection;\n                uniform mat4 u_model;\n\n                varying vec2 v_texCoord;\n\n                void main() {\n                    gl_Position = u_projection * u_model * vec4(a_position, 1.0);\n                    v_texCoord = a_texCoord;\n                }\n            ";
        };
        BasicShader.prototype.getFragmentSource = function () {
            return "\n                precision mediump float;\n\n                uniform vec4 u_tint;\n                uniform sampler2D u_diffuse;\n\n                varying vec2 v_texCoord;\n\n                void main() {\n                    gl_FragColor = u_tint * texture2D(u_diffuse, v_texCoord);\n                }\n            ";
        };
        return BasicShader;
    }(TSE.Shader));
    TSE.BasicShader = BasicShader;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var Scene = (function () {
        function Scene() {
            this._root = new TSE.SimObject(0, "__ROOT__", this);
        }
        Object.defineProperty(Scene.prototype, "root", {
            get: function () { return this._root; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Scene.prototype, "isLoaded", {
            get: function () { return this._root.isLoaded; },
            enumerable: false,
            configurable: true
        });
        Scene.prototype.addObject = function (object) { this._root.addChild(object); };
        Scene.prototype.getObjectByName = function (name) { return this._root.getObjectByName(name); };
        Scene.prototype.load = function () { this._root.load(); };
        Scene.prototype.update = function (time) { this._root.update(time); };
        Scene.prototype.render = function (shader) { this._root.render(shader); };
        return Scene;
    }());
    TSE.Scene = Scene;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var SimObject = (function () {
        function SimObject(id, name, scene) {
            this.transform = new TSE.Transform();
            this._children = [];
            this._isLoaded = false;
            this._components = [];
            this._behaviors = [];
            this._localMatrix = TSE.Matrix4x4.identity();
            this._worldMatrix = TSE.Matrix4x4.identity();
            this._id = id;
            this.name = name;
            this._scene = scene;
        }
        Object.defineProperty(SimObject.prototype, "id", {
            get: function () { return this._id; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(SimObject.prototype, "parent", {
            get: function () { return this._parent; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(SimObject.prototype, "worldMatrix", {
            get: function () { return this._worldMatrix; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(SimObject.prototype, "isLoaded", {
            get: function () { return this._isLoaded; },
            enumerable: false,
            configurable: true
        });
        SimObject.prototype.addChild = function (child) {
            child._parent = this;
            this._children.push(child);
            child.load();
            child.onAdded(this._scene);
        };
        SimObject.prototype.removeChild = function (child) {
            var index = this._children.indexOf(child);
            if (index == -1)
                return;
            child._parent = undefined;
            this._children.splice(index, 1);
        };
        SimObject.prototype.getObjectByName = function (name) {
            if (this.name === name)
                return this;
            for (var _i = 0, _a = this._children; _i < _a.length; _i++) {
                var child = _a[_i];
                var result = child.getObjectByName(name);
                if (result !== undefined)
                    return result;
            }
            return undefined;
        };
        SimObject.prototype.addComponent = function (component) {
            this._components.push(component);
            component.owner = this;
        };
        SimObject.prototype.addBehavior = function (behavior) {
            this._behaviors.push(behavior);
            behavior.owner = this;
        };
        SimObject.prototype.load = function () {
            this._isLoaded = true;
            for (var _i = 0, _a = this._components; _i < _a.length; _i++) {
                var c = _a[_i];
                c.load();
            }
            for (var _b = 0, _c = this._children; _b < _c.length; _b++) {
                var child = _c[_b];
                child.load();
            }
        };
        SimObject.prototype.update = function (time) {
            var _a;
            this._localMatrix = this.transform.transformationMatrix;
            this.updateWorldMatrix((_a = this._parent) === null || _a === void 0 ? void 0 : _a._worldMatrix);
            for (var _i = 0, _b = this._components; _i < _b.length; _i++) {
                var c = _b[_i];
                c.update(time);
            }
            for (var _c = 0, _d = this._behaviors; _c < _d.length; _c++) {
                var c = _d[_c];
                c.update(time);
            }
            for (var _e = 0, _f = this._children; _e < _f.length; _e++) {
                var child = _f[_e];
                child.update(time);
            }
        };
        SimObject.prototype.render = function (shader) {
            for (var _i = 0, _a = this._components; _i < _a.length; _i++) {
                var c = _a[_i];
                c.render(shader);
            }
            for (var _b = 0, _c = this._children; _b < _c.length; _b++) {
                var child = _c[_b];
                child.render(shader);
            }
        };
        SimObject.prototype.onAdded = function (scene) {
            this._scene = scene;
        };
        SimObject.prototype.updateWorldMatrix = function (parentWorldMatrix) {
            if (parentWorldMatrix !== undefined)
                this._worldMatrix = TSE.Matrix4x4.multiply(parentWorldMatrix, this._localMatrix);
            else
                this._worldMatrix.copyFrom(this._localMatrix);
        };
        return SimObject;
    }());
    TSE.SimObject = SimObject;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var ZoneState;
    (function (ZoneState) {
        ZoneState[ZoneState["UNINITIALIZED"] = 0] = "UNINITIALIZED";
        ZoneState[ZoneState["LOADING"] = 1] = "LOADING";
        ZoneState[ZoneState["UPDATING"] = 2] = "UPDATING";
    })(ZoneState = TSE.ZoneState || (TSE.ZoneState = {}));
    var Zone = (function () {
        function Zone(id, name, descripton) {
            this._state = ZoneState.UNINITIALIZED;
            this._globalId = -1;
            this._id = id;
            this._name = name;
            this._description = descripton;
            this._scene = new TSE.Scene();
        }
        Object.defineProperty(Zone.prototype, "id", {
            get: function () { return this._id; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Zone.prototype, "name", {
            get: function () { return this._name; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Zone.prototype, "description", {
            get: function () { return this._description; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Zone.prototype, "scene", {
            get: function () { return this._scene; },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(Zone.prototype, "state", {
            get: function () { return this._state; },
            enumerable: false,
            configurable: true
        });
        Zone.prototype.initialize = function (zoneData) {
            if (zoneData.objects === undefined)
                return;
            for (var _i = 0, _a = zoneData.objects; _i < _a.length; _i++) {
                var object = _a[_i];
                this.loadSimObject(object, this._scene.root);
            }
        };
        Zone.prototype.load = function () {
            if (this._state !== ZoneState.UNINITIALIZED)
                return;
            this._state = ZoneState.LOADING;
            this._scene.load();
            this._state = ZoneState.UPDATING;
        };
        Zone.prototype.unload = function () {
            if (this._state === ZoneState.UNINITIALIZED)
                return;
            this._state = ZoneState.UNINITIALIZED;
        };
        Zone.prototype.update = function (time) {
            if (this._state === ZoneState.UPDATING)
                this._scene.update(time);
        };
        Zone.prototype.render = function (shader) {
            if (this._state === ZoneState.UPDATING)
                this._scene.render(shader);
        };
        Zone.prototype.onActivated = function () { };
        Zone.prototype.onDeactivated = function () { };
        Zone.prototype.loadSimObject = function (dataSection, parent) {
            if (dataSection.name === undefined)
                throw new Error("Invalid Object data!");
            var name = String(dataSection.name);
            this._globalId++;
            var simObject = new TSE.SimObject(this._globalId, name, this._scene);
            if (dataSection.transform !== undefined)
                simObject.transform.setFromJson(dataSection.transform);
            if (dataSection.components !== undefined) {
                for (var _i = 0, _a = dataSection.components; _i < _a.length; _i++) {
                    var component = _a[_i];
                    simObject.addComponent(TSE.ComponentManager.extractComponent(component));
                }
            }
            if (dataSection.behaviors !== undefined) {
                for (var _b = 0, _c = dataSection.behaviors; _b < _c.length; _b++) {
                    var behavior = _c[_b];
                    simObject.addBehavior(TSE.BehaviorManager.extractComponent(behavior));
                }
            }
            if (dataSection.children !== undefined) {
                for (var _d = 0, _e = dataSection.children; _d < _e.length; _d++) {
                    var object = _e[_d];
                    this.loadSimObject(object, simObject);
                }
            }
            parent.addChild(simObject);
        };
        return Zone;
    }());
    TSE.Zone = Zone;
})(TSE || (TSE = {}));
var TSE;
(function (TSE) {
    var ZoneManager = (function () {
        function ZoneManager() {
        }
        ZoneManager.initialize = function () {
            ZoneManager._instance = new ZoneManager();
            ZoneManager._registeredZones[0] = "assets/zones/testZone.json";
        };
        ZoneManager.changeZone = function (id) {
            if (ZoneManager._activeZone !== undefined) {
                ZoneManager._activeZone.onDeactivated();
                ZoneManager._activeZone.unload();
                delete ZoneManager._activeZone;
            }
            var zonePath = ZoneManager._registeredZones[id];
            if (zonePath === undefined)
                throw new Error("Zone Id does not exist!");
            if (TSE.AssetManager.isAssetLoaded(zonePath))
                ZoneManager.loadZone(TSE.AssetManager.getAsset(zonePath));
            else {
                TSE.Message.subscribe(TSE.MESSAGE_ASSET_LOADER_ASSET_LOADED + zonePath, ZoneManager._instance);
                TSE.AssetManager.loadAsset(zonePath);
            }
        };
        ZoneManager.update = function (time) {
            if (ZoneManager._activeZone === undefined)
                return;
            ZoneManager._activeZone.update(time);
        };
        ZoneManager.render = function (shader) {
            if (ZoneManager._activeZone === undefined)
                return;
            ZoneManager._activeZone.render(shader);
        };
        ZoneManager.loadZone = function (asset) {
            var zoneData = asset.data;
            if (zoneData.id === undefined)
                throw new Error("Zone file format exception: Zone id not present.");
            var zoneId = Number(zoneData.id);
            var zoneName = String(zoneData.name);
            var zoneDescription = String(zoneData.description);
            var zone = new TSE.Zone(zoneId, zoneName, zoneDescription);
            zone.initialize(zoneData);
            ZoneManager._activeZone = zone;
            zone.load();
            zone.onActivated();
        };
        ZoneManager.prototype.onMessage = function (message) {
            if (message.code.indexOf(TSE.MESSAGE_ASSET_LOADER_ASSET_LOADED) !== -1) {
                var asset = message.context;
                ZoneManager.loadZone(asset);
            }
        };
        Object.defineProperty(ZoneManager, "zone", {
            get: function () { return this._activeZone; },
            enumerable: false,
            configurable: true
        });
        ZoneManager._globalZoneId = -1;
        ZoneManager._registeredZones = {};
        return ZoneManager;
    }());
    TSE.ZoneManager = ZoneManager;
})(TSE || (TSE = {}));
//# sourceMappingURL=main.js.map