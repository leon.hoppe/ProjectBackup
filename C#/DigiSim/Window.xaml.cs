﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DigiSim.Logic;
using DigiSim.Logic.Gates;

namespace DigiSim {
    public partial class Window {
        public const String FillColor = "#37323E";
        public const String PoweredColor = "#E55934";
        
        public static Canvas Board { get; private set; }
        private static readonly List<Gate> Gates = new();

        public Window() {
            ConsoleManager.Show();
            InitializeComponent();
            Board = Canvas;

            Canvas.MouseMove += (sender, e) => {
                if (CurrentConnection != null) {
                    CurrentConnection.ConnectShapeToPoint(CurrentConnection.Site1.OutputShapes[CurrentConnection.OutputIndex], Mouse.GetPosition(Canvas));
                }
            };
            Canvas.MouseUp += (sender, e) => {
                if (CurrentConnection != null) {
                    foreach (var gate in Gates) {
                        if (gate.InputShapes == null) continue;
                        for (int i = 0; i < gate.InputShapes.Length; i++) {
                            if (Gate.IsMouseOver(gate.InputShapes[i])) {
                                ConnectGates(CurrentConnection.Site1, gate, i, CurrentConnection.OutputIndex);
                                Canvas.Children.Remove(CurrentConnection.Connection);
                                CurrentConnection = null;
                                return;
                            }
                        }
                    }
                    CurrentConnection.Delete();
                    CurrentConnection = null;
                }
            };
        }
        
        private void GateButtonClick(object sender, RoutedEventArgs e) {
            if (sender.Equals(BtnInput))
                CreateGate(new Input(0, 0));
            
            if (sender.Equals(BtnOutput))
                CreateGate(new Output(0, 0));
            
            if (sender.Equals(BtnButton))
                CreateGate(new ButtonInput(0, 0));
            
            if (sender.Equals(BtnSplitter))
                CreateGate(new Splitter(0, 0));
            
            if (sender.Equals(BtnAnd))
                CreateGate(new AndGate(0, 0));
            
            if (sender.Equals(BtnNand))
                CreateGate(new NandGate(0, 0));
            
            if (sender.Equals(BtnOr))
                CreateGate(new OrGate(0, 0));
            
            if (sender.Equals(BtnNor))
                CreateGate(new NorGate(0, 0));
            
            if (sender.Equals(BtnNot))
                CreateGate(new NotGate(0, 0));
            
            if (sender.Equals(BtnXor))
                CreateGate(new XorGate(0, 0));
            
            if (sender.Equals(BtnXnor))
                CreateGate(new XnorGate(0, 0));
        }

        private void CreateGate(Gate gate) {
            gate.AttachToMouse();
            Gates.Add(gate);
            gate.Update(gate);
        }

        public static Wire CurrentConnection;
        public static void StartConnectingGates(Gate gate, int outputIndex) {
            CurrentConnection = new Wire();
            CurrentConnection.Site1 = gate;
            CurrentConnection.OutputIndex = outputIndex;
        }

        public static void DeleteGate(Gate gate) {
            gate.Delete();
            Gates.Remove(gate);
        }

        private void ConnectGates(Gate gate1, Gate gate2, int inputIndex, int outputIndex) {
            gate1.ConnectedOutputs[outputIndex].Delete();
            gate2.ConnectedInputs[inputIndex].Delete();
            Wire wire = new Wire();
            wire.Site1 = gate1;
            wire.Site2 = gate2;
            wire.OutputIndex = outputIndex;
            wire.InputIndex = inputIndex;
            gate1.ConnectedOutputs[outputIndex] = wire;
            gate2.ConnectedInputs[inputIndex] = wire;
            wire.Update();
            gate1.Update(gate1);
        }

        private void NewFile(object sender, RoutedEventArgs e) {
            Gates.ForEach((gate) => gate.Delete());
            Gates.Clear();
        }

        private void SaveAsGate(object sender, RoutedEventArgs e) {
            CustomGate gate = CustomGate.CreateCustomGateFromBoard(Gates.ToArray(), "C");
            NewFile(sender, e);
            Gates.Add(gate);
            gate.AttachToMouse();
        }
    }
}