﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic {
    public class ButtonInput : Gate {
        private bool _powered;
        
        protected override void InternalSetup(Canvas canvas) {
            Shape = new Ellipse();
            SetOutputs(1);
            SetInputs(0);
            CreateConnections();
            
            Shape.MouseLeftButtonDown += (sender, args) => {
                if (IsAttached) return;
                _powered = true;
                Update(this);
            };
            Shape.MouseLeftButtonUp += (sender, args) => {
                if (IsAttached) return;
                _powered = false;
                Update(this);
            };
            Text = "B";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            ConnectedOutputs[0].IsPowered = _powered;
            Shape.Fill = _powered ? FromHex(Window.PoweredColor) : FromHex(Window.FillColor);
            
            UpdateShapes();
            UpdateConnectedGates(source);
        }

        public ButtonInput(double x, double y) : base(x, y, 20, 20) { }
    }
}