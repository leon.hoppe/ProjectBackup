﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DigiSim.Logic {
    public class Wire {
        public Gate Site1 { get; set; }
        public Gate Site2 { get; set; }
        
        public int OutputIndex { get; set; }
        public int InputIndex { get; set; }
        
        public bool IsPowered { get; set; }
        
        public Line Connection { get; }

        public Wire() {
            Connection = new Line();
            Window.Board.Children.Add(Connection);
        }

        public void Update() {
            if (Site1 == null || Site2 == null) return;
            
            Shape site1 = Site1.OutputShapes[OutputIndex];
            Shape site2 = Site2.InputShapes[InputIndex];
            
            Point p1 = new Point(Canvas.GetLeft(site1) + site1.Width / 2, 
                Canvas.GetTop(site1) + site1.Height / 2);
            Point p2 = new Point(Canvas.GetLeft(site2) + site2.Width / 2, 
                Canvas.GetTop(site2) + site2.Height / 2);

            Connection.Stroke = IsPowered ? Gate.FromHex(Window.PoweredColor) : Gate.FromHex(Window.FillColor);
            Connection.StrokeThickness = 1;
            Connection.X1 = p1.X;
            Connection.X2 = p2.X;
            Connection.Y1 = p1.Y;
            Connection.Y2 = p2.Y;
        }

        public void ConnectShapeToPoint(Shape shape, Point point) {
            
            Point p1 = new Point(Canvas.GetLeft(shape) + shape.Width / 2, 
                Canvas.GetTop(shape) + shape.Height / 2);

            Connection.Stroke = Gate.FromHex(Window.FillColor);
            Connection.StrokeThickness = 1;
            Connection.X1 = p1.X;
            Connection.X2 = point.X;
            Connection.Y1 = p1.Y;
            Connection.Y2 = point.Y;
        }

        public void Delete() {
            Window.Board.Children.Remove(Connection);

            if (Site1 != null) {
                if (!Site1.IsDeleted) {
                    Site1.ConnectedOutputs[OutputIndex] = new Wire();
                    Site1.Update(Site1);
                }
            }

            if (Site2 != null) {
                if (!Site2.IsDeleted) {
                    Site2.ConnectedInputs[InputIndex] = new Wire();
                    Site2.Update(Site2);
                }
            }
        }
    }
}