﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic {
    public class Splitter : Gate {
        public Splitter(double x, double y) : base(x, y, 25, 25) { }
        protected override void InternalSetup(Canvas canvas) {
            Shape = new Rectangle();
            SetOutputs(2);
            SetInputs(1);
            CreateConnections();

            Text = "S";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            bool input = ConnectedInputs[0].IsPowered;
            ConnectedOutputs[0].IsPowered = input;
            ConnectedOutputs[1].IsPowered = input;

            UpdateShapes();
            UpdateConnectedGates(source);
        }
    }
}