﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic {
    public class Input : Gate {
        public bool Powered;
        
        protected override void InternalSetup(Canvas canvas) {
            Shape = new Ellipse();
            SetOutputs(1);
            SetInputs(0);
            CreateConnections();
            
            Shape.MouseLeftButtonUp += (sender, args) => {
                if (IsAttached) return;
                Powered = !Powered;
                Update(this);
            };
            Text = "I";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            ConnectedOutputs[0].IsPowered = Powered;
            Shape.Fill = Powered ? FromHex(Window.PoweredColor) : FromHex(Window.FillColor);
            
            UpdateShapes();
            UpdateConnectedGates(source);
        }

        public Input(double x, double y) : base(x, y, 20, 20) { }
    }
}