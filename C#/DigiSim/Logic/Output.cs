﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic {
    public class Output : Gate {
        public Output(double x, double y) : base(x, y, 20, 20) { }
        
        protected override void InternalSetup(Canvas canvas) {
            Shape = new Ellipse();
            SetInputs(1);
            SetOutputs(0);
            CreateConnections();

            Text = "O";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            Shape.Fill = ConnectedInputs[0].IsPowered ? FromHex(Window.PoweredColor) : FromHex(Window.FillColor);
            
            UpdateShapes();
            UpdateConnectedGates(source);
        }
    }
}