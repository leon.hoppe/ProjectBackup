﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic.Gates {
    public class AndGate : Gate {
        public AndGate(double x, double y) : base(x, y, 50, 50) { }
        protected override void InternalSetup(Canvas canvas) {
            TruthTable.Add(false, false, false);
            TruthTable.Add(false, true, false);
            TruthTable.Add(true, false, false);
            TruthTable.Add(true, true, true);
            
            Shape = new Rectangle();
            SetInputs(2);
            SetOutputs(1);
            CreateConnections();

            Text = "&";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            ConnectedOutputs[0].IsPowered = TruthTable[ConnectedInputs[0].IsPowered, ConnectedInputs[1].IsPowered];
            
            UpdateShapes();
            UpdateConnectedGates(source);
        }
    }
}