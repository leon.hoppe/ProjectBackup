﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic.Gates {
    public class NotGate : Gate {
        public NotGate(double x, double y) : base(x, y, 25, 25) { }
        protected override void InternalSetup(Canvas canvas) {
            Shape = new Rectangle();
            SetInputs(1);
            SetOutputs(1);
            CreateConnections();

            Text = "!1";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            ConnectedOutputs[0].IsPowered = !ConnectedInputs[0].IsPowered;
            
            UpdateShapes();
            UpdateConnectedGates(source);
        }
    }
}