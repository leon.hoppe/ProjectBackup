﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DigiSim.Logic.Gates {
    public class OrGate : Gate {
        public OrGate(double x, double y) : base(x, y, 50, 50) { }
        protected override void InternalSetup(Canvas canvas) {
            TruthTable.Add(false, false, false);
            TruthTable.Add(false, true, true);
            TruthTable.Add(true, false, true);
            TruthTable.Add(true, true, true);

            Shape = new Rectangle();
            SetInputs(2);
            SetOutputs(1);
            CreateConnections();

            Text = ">1";
            
            Instantiate();
        }

        public override void Update(Gate source) {
            ConnectedOutputs[0].IsPowered = TruthTable[ConnectedInputs[0].IsPowered, ConnectedInputs[1].IsPowered];
            
            UpdateShapes();
            UpdateConnectedGates(source);
        }
    }
}