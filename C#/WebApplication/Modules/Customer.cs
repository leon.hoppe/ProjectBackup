﻿using System;

namespace WebApplication.Modules {
    public class Customer : CustomerEditor {
        public string Guid { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public class CustomerEditor {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public float Balance { get; set; }

        public void EditCustomer(Customer customer) {
            customer.FirstName = FirstName;
            customer.LastName = LastName;
            customer.Email = Email;
            customer.Username = Username;
            customer.Password = Password;
            customer.Balance = Balance;
        }
    }

    public class CustomerLogin {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}