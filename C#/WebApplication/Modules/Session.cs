﻿using System;

namespace WebApplication.Modules {
    public class Session {
        public string Guid { get; set; }
        public string CustomerId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}