﻿public enum LogicResultState
{
    Ok,
    BadRequest,
    Forbidden,
    NotFound,
    Conflict
}