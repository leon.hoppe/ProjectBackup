﻿using System.Collections.Generic;
using System.Linq;
using WebApplication.Modules;
using WebApplication.Repositorys.Interfaces;

namespace WebApplication.Logic {
    public interface ICustomerLogic {
        ILogicResult<Customer> AddCustomer(CustomerEditor editor);
        ILogicResult RemoveCustomer(string id, string session);
        ILogicResult EditCustomer(string id, CustomerEditor editor, string session);
        ILogicResult<Customer> GetCustomer(string id, string session);
        ILogicResult<IEnumerable<Customer>> GetCustomers();
        ILogicResult<Session> CreateCustomerSession(string customer);
        ILogicResult DeleteCustomerSession(string customer, string session);
        ILogicResult<Session> Login(CustomerLogin login);
    }
    
    public class CustomerLogic : ICustomerLogic {
        private readonly ICustomerRepository _customers;
        private readonly ISessionRepository _sessions;

        public CustomerLogic(ICustomerRepository customers, ISessionRepository sessions) {
            _customers = customers;
            _sessions = sessions;
        }
        
        public ILogicResult<Customer> AddCustomer(CustomerEditor editor) {
            return LogicResult<Customer>.Ok(_customers.AddCustomer(editor));
        }

        public ILogicResult RemoveCustomer(string id, string session) {
            if (!ValidateSession(session, id)) return LogicResult.Forbidden();
            _customers.RemoveCustomer(id);
            return LogicResult.Ok();
        }

        public ILogicResult EditCustomer(string id, CustomerEditor editor, string session) {
            if (!ValidateSession(session, id)) return LogicResult.Forbidden();
            _customers.EditCustomer(id, editor);
            return LogicResult.Ok();
        }

        public ILogicResult<Customer> GetCustomer(string id, string session) {
            if (!ValidateSession(session, id)) return LogicResult<Customer>.Forbidden();
            return LogicResult<Customer>.Ok(_customers.GetCustomer(id));
        }

        public ILogicResult<IEnumerable<Customer>> GetCustomers() {
            return LogicResult<IEnumerable<Customer>>.Ok(_customers.GetCustomers());
        }

        public ILogicResult<Session> CreateCustomerSession(string customer) {
            return LogicResult<Session>.Ok(_sessions.CreateSession(customer));
        }

        public ILogicResult DeleteCustomerSession(string customer, string session) {
            if (!ValidateSession(session, customer)) return LogicResult.Forbidden();
            _sessions.RemoveSession(_sessions.GetSession(session));
            return LogicResult.Ok();
        }

        public ILogicResult<Session> Login(CustomerLogin login) {
            var customer = _customers.GetCustomers().FirstOrDefault(c => c.Email == login.Email);
            if (customer is null) return LogicResult<Session>.NotFound();
            return !customer.Password.Equals(login.Password) ? LogicResult<Session>.Forbidden() : CreateCustomerSession(customer.Guid);
        }

        private bool ValidateSession(string session, string customerToAccess) {
            return _sessions.ValidateSession(_sessions.GetSession(session), customerToAccess);
        }
    }
}