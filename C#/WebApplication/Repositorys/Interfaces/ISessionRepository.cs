﻿using WebApplication.Modules;

namespace WebApplication.Repositorys.Interfaces {
    public interface ISessionRepository {
        Session CreateSession(string customer);
        void RemoveSession(Session session);
        Session GetSession(string id);
        bool ValidateSession(Session session, string customerToAccess);
    }
}