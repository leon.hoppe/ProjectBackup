﻿using System.Collections.Generic;
using WebApplication.Modules;

namespace WebApplication.Repositorys.Interfaces {
    public interface ICustomerRepository {
        Customer AddCustomer(CustomerEditor editor);
        void RemoveCustomer(string id);
        void EditCustomer(string id, CustomerEditor editor);
        Customer GetCustomer(string id);
        IEnumerable<Customer> GetCustomers();
    }
}