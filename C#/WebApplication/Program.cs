using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace WebApplication {
    public class Program {
        public static void Main(string[] args) {
            MySQL.Connect();
            CreateTables();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

        private static void CreateTables() {
            if (!MySQL.IsConnected()) throw new NullReferenceException("MySQL not connected!");
            MySQL.Insert("CREATE TABLE IF NOT EXISTS Customers (Guid VARCHAR(50), CreationDate VARCHAR(50), FirstName VARCHAR(50), LastName VARCHAR(50), Email VARCHAR(50), Username VARCHAR(50), Password VARCHAR(50), Balance VARCHAR(50))");
            MySQL.Insert("CREATE TABLE IF NOT EXISTS Sessions (Guid VARCHAR(50), CustomerId VARCHAR(50), CreationDate VARCHAR(50))");
        }
    }
}