﻿using System;
using MySqlConnector;

namespace WebApplication {
    public class MySQL {
        private const string Server = "leon-hoppe.de";
        private const string Database = "WebApplication";
        private const string Username = "WebApplication";
        private const string Password = "/q/cIGlBK.FgNphs";

        private static MySqlConnection _con;
        private static MySqlCommand _command;
        
        public static void Connect() {
            var builder = new MySqlConnectionStringBuilder { Server = Server, Database = Database, UserID = Username, Password = Password };
            _con = new MySqlConnection(builder.ConnectionString);
            _con.Open();
            _command = _con.CreateCommand();
            Console.WriteLine("MySQL connected Successfully");
        }

        public static bool IsConnected() {
            return _con != null;
        }

        public static void Insert(string qry) {
            _command.CommandText = qry;
            _command.ExecuteNonQuery();
        }

        public static MySqlDataReader GetData(string qry) {
            _command.CommandText = qry;
            return _command.ExecuteReader();
        }
    }
}