﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Logic;
using WebApplication.Modules;

namespace WebApplication.Controllers {
    [Route("api/customers")]
    public class CustomerController : ControllerBase {
        private readonly ICustomerLogic _logic;

        public CustomerController(ICustomerLogic logic) {
            _logic = logic;
        }
        
        [HttpPost]
        public ILogicResult<Customer> AddCustomer([FromBody] CustomerEditor editor) {
            return _logic.AddCustomer(editor);
        }

        [HttpDelete]
        [Route("{id}")]
        public ILogicResult RemoveCustomer(string id) {
            return _logic.RemoveCustomer(id, GetSessionKey());
        }

        [HttpPut]
        [Route("{id}")]
        public ILogicResult EditCustomer(string id, [FromBody] CustomerEditor editor) {
            return _logic.EditCustomer(id, editor, GetSessionKey());
        }

        [HttpGet]
        [Route("{id}")]
        public ILogicResult<Customer> GetCustomer(string id) {
            return _logic.GetCustomer(id, GetSessionKey());
        }

        [HttpGet]
        public ILogicResult<IEnumerable<Customer>> GetCustomers() {
            return _logic.GetCustomers();
        }

        [HttpPost]
        [Route("register")]
        public ILogicResult<Session> Register([FromBody] CustomerEditor editor) {
            Customer customer = _logic.AddCustomer(editor).Data;
            return _logic.CreateCustomerSession(customer.Guid);
        }

        [HttpGet]
        [Route("{id}/logout")]
        public ILogicResult Logout(string id) {
            return _logic.DeleteCustomerSession(id, GetSessionKey());
        }

        [HttpPut]
        [Route("login")]
        public ILogicResult<Session> Login([FromBody] CustomerLogin login) {
            return _logic.Login(login);
        }

        private string GetSessionKey() {
            return HttpContext.Request.Headers["Authorization"];
        }
    }
}