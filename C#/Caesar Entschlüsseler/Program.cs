﻿// See https://aka.ms/new-console-template for more information

using System;
using System.Collections.Generic;

List<char> alphabet = new (new[] {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
    'x', 'y', 'z'
});

Console.WriteLine("Caesar Entschlüsseler");
Console.Write("Gebe die Nachricht ein: ");

string message = Console.ReadLine().ToLower();
char[] chars = message.ToCharArray();

while (true) {
    Console.Clear();
    Console.Write("Gebe den Token ein: ");

    int token = Convert.ToInt32(Console.ReadLine());

    string newMsg = "";
    
    for (var i = 0; i < message.Length; i++) {
        if (chars[i] == ' ') {
            newMsg += " ";
            continue;
        }
        
        int index = alphabet.FindIndex(ch => ch == chars[i]) + token;
        if (index < 0)
            index = alphabet.Count - index;
        if (index >= alphabet.Count)
            index = index - alphabet.Count;
        

        newMsg += alphabet[index];
    }

    Console.WriteLine("Nachricht: " + newMsg.ToUpper());
    Console.ReadKey();
}