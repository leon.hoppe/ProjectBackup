﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebDesktopUpdater.Controllers {
    
    [ApiController]
    [Route("update")]
    public class UpdateController : ControllerBase {
        private readonly IConfiguration _configuration;

        public UpdateController(IConfiguration configuration) {
            _configuration = configuration;
        }

        [HttpGet]
        public ContentResult Update() {
            FileInfo file = new FileInfo(_configuration.GetValue<string>("UpdaterScript"));
            if (file.DirectoryName == null) return new ContentResult();
                
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "/bin/bash";
            startInfo.WorkingDirectory = file.DirectoryName;
            startInfo.Arguments = "./" + file.Name;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
                
            process.StartInfo = startInfo;
            process.Start();
                
            string error = process.StandardError.ReadToEnd();
            string info = process.StandardOutput.ReadToEnd();
                
            string html = System.IO.File.ReadAllText(Environment.CurrentDirectory + "/response.html")
                .Replace("{error}", error.Replace("\\n", "<br>"))
                .Replace("{info}", info.Replace("\\n", "<br>"));
                
            return new ContentResult {
                ContentType = "text/html",
                Content = html
            };
        }
        
    }
    
}