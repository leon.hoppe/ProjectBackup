using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace WebDesktopUpdater {
    public class Program {
        public static void Main(string[] args) {
            CreateHostBuilder(args)
                .Build()
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .Build();

            /*var backendLocaton = tempConfig.GetValue<string>("Backend");
            var config = new ConfigurationBuilder()
                .AddJsonFile(backendLocaton + "appsettings.json", optional: false)
                .Build().GetSection("WebServer").GetSection("SSL");*/
            
            return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => {
                webBuilder.UseStartup<Startup>();
                webBuilder.UseUrls("http://0.0.0.0:" + config.GetValue<int>("Port"));

                /*webBuilder.UseKestrel(options => {
                    options.Listen(IPAddress.Any, tempConfig.GetValue<int>("Port"), listenOptions => {
                        listenOptions.UseHttps(backendLocaton + config.GetValue<string>("Certificate"), config.GetValue<string>("Password"));
                    });
                });*/
            });
        }
    }
}