﻿using System;
using System.Collections.Generic;

namespace DynamicParser {
    public sealed class Person {
        public string Name { get; set; }
        public int Height { get; set; }
        public DateTime Birth { get; set; }
        public List<string> Friends { get; set; }
        public Job Job { get; set; }
        public Dictionary<int, Person> Dict { get; set; }
    }

    public sealed class Job {
        public string Name { get; set; }
        public List<Person> Employees { get; set; }
    }
}