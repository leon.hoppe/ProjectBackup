﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace DynamicParser {
    internal sealed class Program {
        public static void Main(string[] args) {

            var person = new Person {
                Name = "Hans Müller",
                Birth = new DateTime(1995, 4, 20),
                Height = 188,
                Friends = new List<string> {
                    "Peter Lustig",
                    "Hein Blöd",
                    "Max Mustermann"
                },
                Job = new Job {
                    Name = "Banker",
                    Employees = new List<Person> {
                        new Person {
                            Name = "Peter Lustig",
                            Friends = new List<string> {
                                "Mark Forster"
                            }
                        },
                        new Person {
                            Name = "Max Mustermann",
                            Height = 187,
                            Birth = new DateTime(2005, 8, 23),
                            Friends = new List<string>()
                        },
                        new Person(),
                        new Person {
                            Job = new Job()
                        }
                    }
                },
                Dict = new Dictionary<int, Person> {
                    {0, new Person()},
                    {187, new Person()}
                }
            };

            var dynamicPerson = ExpandoParser.ToExpando(person);
            ExpandoParser.PrintDynamic(dynamicPerson);

            Console.WriteLine("\n");

            var parsedPerson = ExpandoParser.ToObject<Person>(dynamicPerson);
            Console.WriteLine(JsonConvert.SerializeObject(parsedPerson, Formatting.Indented));

            Console.WriteLine("\n\n");
            ExecuteBenchmarkTest(person);

        }

        private static void ExecuteBenchmarkTest(Person person) {
            var stream = Console.Out;
            Console.SetOut(new StreamWriter(new MemoryStream()));

            stream.WriteLine("Small person object");
            RunBenchmarks(person, stream);

            var person2 = new Person {
                Job = new Job {
                    Employees = new List<Person>()
                },
                Dict = new Dictionary<int, Person>()
            };
            for (int i = 0; i < 50; i++) {
                person2.Job.Employees.Add(new Person() {
                    Name = Guid.NewGuid().ToString()
                });
                person2.Dict.Add(i, new Person());
            }

            stream.WriteLine("\nLarge person object");
            RunBenchmarks(person2, stream);

            List<(long, long, long, long, long)> results = new List<(long, long, long, long, long)>();
            for (int i = 0; i < 500; i++) {
                results.Add(RunBenchmarks(person, null, false));
            }
            
            long w1 = 0, w2 = 0, w3 = 0, w4 = 0, w5 = 0;
            foreach ((long ww1, long ww2, long ww3, long ww4, long ww5) in results) {
                w1 += ww1;
                w2 += ww2;
                w3 += ww3;
                w4 += ww4;
                w5 += ww5;
            }
            
            stream.WriteLine("\nMass results for small person");
            stream.WriteLine($"Benchmarked function [{nameof(ExpandoParser.ToExpando)}]: {((double)w1 / results.Count) / 10000}ms");
            stream.WriteLine($"Benchmarked function [{nameof(ExpandoParser.ToObject)}]: {((double)w2 / results.Count) / 10000}ms");
            stream.WriteLine($"Benchmarked function [{nameof(ExpandoParser.PrintDynamic)}]: {((double)w3 / results.Count) / 10000}ms");

            stream.WriteLine($"Benchmarked function [{nameof(JsonConvert.SerializeObject)}]: {((double)w4 / results.Count) / 10000}ms");
            stream.WriteLine($"Benchmarked function [{nameof(JsonConvert.DeserializeObject)}]: {((double)w5 / results.Count) / 10000}ms");
        }

        private static (long, long, long, long, long) RunBenchmarks(Person person, TextWriter stream, bool print = true) {
            var watch1 = new Stopwatch();
            watch1.Start();
            var expandoObject = ExpandoParser.ToExpando(person);
            watch1.Stop();
            
            var watch2 = new Stopwatch();
            watch2.Start();
            ExpandoParser.ToObject<Person>(expandoObject);
            watch2.Stop();

            var watch3 = new Stopwatch();
            watch3.Start();
            ExpandoParser.PrintDynamic(expandoObject);
            watch3.Stop();

            var watch4 = new Stopwatch();
            watch4.Start();
            var json = JsonConvert.SerializeObject(person);
            watch4.Stop();
            
            var watch5 = new Stopwatch();
            watch5.Start();
            JsonConvert.DeserializeObject<Person>(json);
            watch5.Stop();

            if (print) {
                stream.WriteLine($"Benchmarked function [{nameof(ExpandoParser.ToExpando)}]: {(double)watch1.ElapsedTicks / 10000}ms");
                stream.WriteLine($"Benchmarked function [{nameof(ExpandoParser.ToObject)}]: {(double)watch2.ElapsedTicks / 10000}ms");
                stream.WriteLine($"Benchmarked function [{nameof(ExpandoParser.PrintDynamic)}]: {(double)watch3.ElapsedTicks / 10000}ms");

                stream.WriteLine($"Benchmarked function [{nameof(JsonConvert.SerializeObject)}]: {(double)watch4.ElapsedTicks / 10000}ms");
                stream.WriteLine($"Benchmarked function [{nameof(JsonConvert.DeserializeObject)}]: {(double)watch5.ElapsedTicks / 10000}ms");
            }

            return (watch1.ElapsedTicks, watch2.ElapsedTicks, watch3.ElapsedTicks, watch4.ElapsedTicks, watch5.ElapsedTicks);
        }
    }
}