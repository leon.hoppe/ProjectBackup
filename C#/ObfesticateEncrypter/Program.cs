﻿var file = args.Length > 1 ? args[0] : Console.ReadLine()!;

var data = File.ReadAllText(file);
var bytes = Convert.FromBase64String(data);
File.WriteAllBytes("./output.lua", bytes);
