﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFProject {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void HomeClick(object button, EventArgs e) {
            Title.Content = "Startseite";
            SwitchSide(Home);
        }
        
        private void ProductsClick(object button, EventArgs e) {
            Title.Content = "Produkte";
            SwitchSide(Products);
        }
        
        private void ContactClick(object button, EventArgs e) {
            Title.Content = "Kontakt";
            SwitchSide(Contact);
        }

        private void SwitchSide(Grid grid) {
            Home.Visibility = Visibility.Hidden;
            Products.Visibility = Visibility.Hidden;
            Contact.Visibility = Visibility.Hidden;

            grid.Visibility = Visibility.Visible;
        }
    }
}