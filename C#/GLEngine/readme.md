# GLEngine
GLEngine is an high level C# OpenGL game engine with low level control.

## Features
- [ ] Gameloop
- [ ] Shader support (GLSL)
- [ ] Easy Texture loading
- [ ] Easy Mesh creation
- [ ] 2D and 3D