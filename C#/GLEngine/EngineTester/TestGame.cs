﻿using System.Drawing;
using System.Numerics;
using GLEngine;
using GLEngine.Animations;
using GLEngine.Objects;
using GLEngine.Objects.Components;
using GLEngine.Rendering;
using GLEngine.Resources;

namespace EngineTester {
    public class TestGame : Engine {

        public TestGame() {
            DEBUG_CopyFileToOutput(@"D:\Programmierstuff\C#\GLEngine\glfw.dll");
            DEBUG_CopyFolderToOutput(@"D:\Programmierstuff\C#\GLEngine\EngineTester\Assets");
            Start(800, 600, "TestGame", true);
        }

        protected override void Initialize() {}

        protected override void InitResources() {
            ResourceLoader.InitializeResource("tex_bricks", new Texture("Assets/bricks.png"));
            ResourceLoader.InitializeResource("tex_logo", new Texture("Assets/logo.png"));
            ResourceLoader.InitializeResource("tex_font", new Texture("Assets/font.png"));
        }

        protected override void AfterInit() {
            Window.SetWindowIcon("Assets/logo.png");
            
            GameObject test = new("test"); 
            test.AddComponent(new MeshRenderer(texture: "tex_logo"));
            test.AddComponent(new Animator(new[]{
                new Animation("pulse", true, true, new[]{
                    new Keyframe<Vector3>(new Vector3( 0.3f), 1.0f, 0.0f, KeyframeType.Scale, AnimationFunctions.Smooth),
                    new Keyframe<Vector3>(new Vector3(-0.3f), 1.0f, 0.0f, KeyframeType.Scale, AnimationFunctions.Smooth)
                }),
                
                new Animation("pulseSprite", true, true, new [] {
                    new Keyframe<string>("tex_bricks", 1.0f, 0.0f, KeyframeType.Sprite, AnimationFunctions.Smooth),
                    new Keyframe<string>("tex_logo",   1.0f, 0.0f, KeyframeType.Sprite, AnimationFunctions.Smooth),
                })
            }));

            GameObject testText = new("text");
            testText.AddComponent(new TextRenderer("tex_font", "TestText", Color.White));
            testText.Transform.Position = new Vector3(0f, 2f, 0f);
            testText.Transform.Scale = new Vector3(0.5f);
            
            Instantiate(test);
            Instantiate(testText);
        }

        protected override void SetWindowHints() {}
        
    }
}