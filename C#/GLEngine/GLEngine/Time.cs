﻿namespace GLEngine {
    public class Time {
        
        public static float DeltaTime { get; set; }
        public static float ElapsedTime { get; set; }
        
    }
}