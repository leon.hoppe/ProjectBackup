﻿using System.Collections.Generic;

namespace GLEngine.Resources {
    public class ResourceLoader {

        private static Dictionary<string, IResource> LoadedResources { get; } = new();

        public static T InitializeResource<T>(in string name, in T resource) where T : IResource {
            LoadedResources.Add(name, resource);
            return resource;
        }

        public static T LoadResource<T>(in string name) where T : IResource {
            IResource resource = LoadedResources[name];
            resource.Load();
            return (T)resource;
        }

        public static T GetResource<T>(in string name) where T : IResource {
            return (T)LoadedResources[name];
        }

        public static string GetResourceName(in IResource resource) {
            foreach (var res in LoadedResources) {
                if (res.Value == resource)
                    return res.Key;
            }

            return null;
        }

        public static void DeleteResource(in string name) {
            IResource resource = LoadedResources[name];
            resource.Delete();
            LoadedResources.Remove(name);
        }

        public static void DeleteAllResources() {
            foreach (var resource in LoadedResources.Values) {
                resource.Delete();
            }
            LoadedResources.Clear();
        }

    }
}