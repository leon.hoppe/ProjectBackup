﻿namespace GLEngine.Resources {
    public interface IResource {
        public void Load();

        public void Delete();

        public void Bind();

        public void Unbind();
    }
}