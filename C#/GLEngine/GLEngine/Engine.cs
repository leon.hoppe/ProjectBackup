﻿using System.Drawing;
using System.IO;
using System.Numerics;
using GLEngine.Extensions;
using GLEngine.Objects;
using GLEngine.Rendering;
using GLEngine.Resources;
using GLFW;
using static OpenGL.GL;

namespace GLEngine {
    public abstract class Engine {
        
        public static Engine Instance { get; private set; }
        
        public GameWindow Window { get; private set; }
        public Scene CurrentScene { get; private set; }

        protected void Start(int width, int height, string title, bool vsync) {
            Instance = this;
            CurrentScene = new Scene();
            Initialize();
            
            Glfw.Init();
            SetWindowHints();
            Glfw.WindowHint(Hint.ContextVersionMajor, 3);
            Glfw.WindowHint(Hint.ContextVersionMinor, 3);
            Glfw.WindowHint(Hint.OpenglProfile, Profile.Core);
            Window = new GameWindow(width, height, title, vsync);
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            
            LoadDefaultResources();
            InitResources();
            
            AfterInit();

            Awake();

            while (!Window.IsClosing) {
                Time.DeltaTime = (float)Glfw.Time - Time.ElapsedTime;
                Time.ElapsedTime = (float)Glfw.Time;
                
                Glfw.PollEvents();
                
                Update();
                Render();
            }
            
            Destroy();
            ResourceLoader.DeleteAllResources();
            Window.Close();
        }

        public void SwapScene(Scene newScene) {
            CurrentScene.Destroy();
            CurrentScene = newScene;
            CurrentScene.Awake();
        }

        public GameObject Instantiate(GameObject gameObject) => CurrentScene.Instantiate(gameObject);
        public void Destory(GameObject gameObject) => CurrentScene.Destory(gameObject);

        protected abstract void Initialize();
        protected abstract void InitResources();
        protected abstract void AfterInit();
        protected abstract void SetWindowHints();

        private void Awake() => CurrentScene.Awake();

        private void Update() => CurrentScene.Update();

        private void Render() {
            glClear(GL_COLOR_BUFFER_BIT);
            
            CurrentScene.Render();
            
            Window.SwapBuffers();
        }

        private void Destroy() => CurrentScene.Destroy();

        private void LoadDefaultResources() {
            ResourceLoader.InitializeResource(Shader.Default, new Shader(@"
#type vertex
#version 330 core
layout (location = 0) in vec2 aPosition;
layout (location = 1) in vec4 aColor;
layout (location = 2) in vec2 aUV;
out vec4 vertexColor;
out vec2 uv;

uniform mat4 projection;
uniform mat4 model;

void main() {
    vertexColor = aColor;
    uv = aUV;
    gl_Position = projection * model * vec4(aPosition.xy, 0, 1.0);
}

#type fragment
#version 330 core
out vec4 FragColor;
in vec4 vertexColor;
in vec2 uv;

uniform sampler2D texture0;

void main() {
    FragColor = texture(texture0, uv) * vertexColor;
}
            ", false));
            
            ResourceLoader.InitializeResource(Mesh.Default, new Mesh {
                Vertices = new [] {
                    new Vector3(-0.5f,  0.5f, 1.0f),
                    new Vector3( 0.5f,  0.5f, 1.0f),
                    new Vector3(-0.5f, -0.5f, 1.0f),
                    new Vector3( 0.5f, -0.5f, 1.0f)
                },
                
                Colors = new []{ Color.White },
                
                UVs = new [] {
                    new Vector2(0.0f, 1.0f),
                    new Vector2(1.0f, 1.0f),
                    new Vector2(0.0f, 0.0f),
                    new Vector2(1.0f, 0.0f)
                },
                
                Triangles = new [] {
                    0, 1, 2,
                    1, 3, 2
                }
            });
            
            ResourceLoader.InitializeResource(Texture.Default, new Texture(null));
        }
        
        protected void DEBUG_CopyFolderToOutput(string folder) {
            #if DEBUG
            var info = new DirectoryInfo(folder);
            info.CopyTo(Directory.GetCurrentDirectory() + "/" + info.Name, true, true);
            #endif
        }
        protected void DEBUG_CopyFileToOutput(string file) {
            #if DEBUG
            var info = new FileInfo(file);
            info.CopyTo(Directory.GetCurrentDirectory() + "/" + info.Name, true);
            #endif
        }
    }

    public interface IGameEvents {
        
        public void Awake();
        public void Update();
        public void Render();
        public void Destroy();

    }
}