﻿using System.Numerics;

namespace GLEngine {
    public class Transform {
        
        public Vector3 Position { get; set; }
        public Vector3 Scale { get; set; }
        public Vector3 Rotation { get; set; }
        
    }
}