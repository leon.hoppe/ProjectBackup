﻿using System.IO;

namespace GLEngine.Extensions {
    public static class DirectoryInfoExtensions {
        
        public static void CopyTo(this DirectoryInfo dir, string destinationDir, bool overwrite, bool recursive) {
            if (overwrite && Directory.Exists(destinationDir))
                Directory.Delete(destinationDir, true);
            
            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            DirectoryInfo[] dirs = dir.GetDirectories();
            Directory.CreateDirectory(destinationDir);

            foreach (FileInfo file in dir.GetFiles()) {
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
            }

            if (recursive) {
                foreach (DirectoryInfo subDir in dirs) {
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyTo(subDir, newDestinationDir, true, overwrite);
                }
            }
        }
        
    }
}