﻿using System.Drawing;

namespace GLEngine.Extensions {
    public static class ColorExtensions {
        private static float Map(float value, float min, float max, float mMin, float mMax) {
            float norm = (value - min) / (max - min);
            return (mMax - mMin) * norm + mMin;
        }

        public static float GetRed(this Color color) {
            return Map(color.R, 0, 255, 0, 1);
        }
        
        public static float GetGreen(this Color color) {
            return Map(color.G, 0, 255, 0, 1);
        }
        
        public static float GetBlue(this Color color) {
            return Map(color.B, 0, 255, 0, 1);
        }
        
        public static float GetAlpha(this Color color) {
            return Map(color.A, 0, 255, 0, 1);
        }
    }
}