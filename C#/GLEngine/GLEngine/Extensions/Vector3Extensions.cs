﻿using System.Numerics;

namespace GLEngine.Extensions {
    public static class Vector3Extensions {

        public static Vector3 CreateCopy(this Vector3 vector) {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }
        
    }
}