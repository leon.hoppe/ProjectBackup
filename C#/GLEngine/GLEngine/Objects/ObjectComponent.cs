﻿namespace GLEngine.Objects.Components {
    public abstract class ObjectComponent {
        
        public int Order { get; set; }
        
        public GameObject GameObject { get; set; }

        protected Transform Transform => GameObject.Transform;

        protected GameObject Instantiate(GameObject gameObject) => GameObject.Scene.Instantiate(gameObject);
        protected void Destroy(GameObject gameObject) => GameObject.Scene.Destory(gameObject);

        public abstract void Awake();
        public abstract void Update();
        public abstract void Render();
        public abstract void Destroy();
    }
}