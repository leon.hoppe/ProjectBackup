﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using GLEngine.Objects.Components;
using GLEngine.Rendering;

namespace GLEngine.Objects {
    public class GameObject : IGameEvents {
        
        public string Name { get; }
        public int Layer { get; set; }
        public Transform Transform { get; }
        public Scene Scene { get; set; }
        private bool _loaded = false;
        
        private List<ObjectComponent> Components { get; set; }
        public Matrix4x4 Model { get; private set; }

        public GameObject(string name, int layer = 0) {
            Name = name;
            Layer = layer;
            Transform = new Transform {
                Position = Vector3.Zero,
                Scale = Vector3.One,
                Rotation = Vector3.Zero
            };
            Components = new List<ObjectComponent>();
        }

        public void Awake() {
            if (_loaded) return;
            _loaded = true;
            Components.ForEach(c => c.Awake());
        }

        public void Update() {
            Matrix4x4 trans = Matrix4x4.CreateTranslation(Transform.Position.X, -Transform.Position.Y, Transform.Position.Z);
            Matrix4x4 sca = Matrix4x4.CreateScale(Transform.Scale);
            Matrix4x4 rotX = Matrix4x4.CreateRotationX(Transform.Rotation.X);
            Matrix4x4 rotY = Matrix4x4.CreateRotationY(Transform.Rotation.Y);
            Matrix4x4 rotZ = Matrix4x4.CreateRotationZ(Transform.Rotation.Z);
            Model = sca * rotX * rotY * rotZ * trans;
            
            Components = Components.OrderBy(c => c.Order).ToList();
            Components.ForEach(c => c.Update());
        }

        public void Render() => Components.ForEach(c => c.Render());

        public void Destroy() => Components.ForEach(c => c.Destroy());

        public void AddComponent(ObjectComponent component) {
            component.GameObject = this;
            if (_loaded)
                component.Awake();
            Components.Add(component);
        }
        public T GetComponent<T>() where T : ObjectComponent {
            foreach (var component in Components) {
                if (component.GetType() == typeof(T))
                    return (T)component;
            }

            throw new NullReferenceException("Object does not contain a component of type " + typeof(T).Name);
        }
        public void RemoveComponent<T>() where T : ObjectComponent {
            foreach (var component in Components) {
                if (component.GetType() == typeof(T)) {
                    Components.Remove(component);
                    return;
                }
            }

            throw new NullReferenceException("Object does not contain a component of type " + typeof(T).Name);
        }
        public bool HasComponent<T>() where T : ObjectComponent {
            foreach (var component in Components) {
                if (component.GetType() == typeof(T))
                    return true;
            }

            return false;
        }

        public static GameObject GetByName(string name) {
            foreach (var o in Engine.Instance.CurrentScene.Objects) {
                if (o.Name.Equals(name))
                    return o;
            }
            return null;
        }
    }
}