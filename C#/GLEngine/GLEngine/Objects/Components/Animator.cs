﻿using System.Collections.Generic;
using GLEngine.Animations;

namespace GLEngine.Objects.Components {
    public class Animator : ObjectComponent {

        private readonly Dictionary<string, Animation> _animations = new();
        private bool _initialized = false;

        public Animator(params Animation[] animations) {
            foreach (var animation in animations) {
                _animations.Add(animation.Name, animation);
            }
        }

        public override void Awake() {
            foreach (var animation in _animations.Values) {
                animation.Initialize(GameObject);
            }

            _initialized = true;
        }

        public override void Update() {
            foreach (var animation in _animations.Values) {
                animation.Update();
            }
        }

        public override void Render() {
            foreach (var animation in _animations.Values) {
                animation.Render();
            }
        }

        public override void Destroy() {
            foreach (var animation in _animations.Values) {
                animation.Stop();
            }
        }

        public Animation this[string name] {
            get => _animations[name];
            set {
                _animations.Remove(value.Name);
                if (!_initialized)
                    value.Initialize(GameObject);
                _animations.Add(value.Name, value);
            }
        }
    }
}