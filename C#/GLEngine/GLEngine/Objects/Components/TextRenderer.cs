﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Numerics;
using System.Text;
using System.Text.Json;
using GLEngine.Rendering;
using GLEngine.Resources;
using static OpenGL.GL;

namespace GLEngine.Objects.Components {
    public class TextRenderer : ObjectComponent {
        
        public string Text { get; set; }
        public TextAlign TextAlign { get; set; }
        public Vector2 Size { get; private set; }
        
        public Color TextColor { get; set; }
        public MeshRenderer Renderer { get; }
        private TextSettings Settings { get; }
        private string CurrentText { get; set; }
        
        public TextRenderer(string fontImage, string text, Color textColor, TextSettings settings = null, DrawMode renderingMode = DrawMode.Static) {
            settings ??= new TextSettings();
            Settings = settings;
            TextColor = textColor;
            Text = text;
            TextAlign = TextAlign.Center;

            string meshRes = "mesh_" + Guid.NewGuid();
            ResourceLoader.InitializeResource(meshRes, new Mesh(renderingMode));
            Renderer = new MeshRenderer(meshRes, Shader.Default, fontImage);
        }
        
        public override void Awake() {
            Renderer.GameObject = GameObject;
            Renderer.Awake();
        }

        public override void Update() {
            if (CurrentText == Text) return;
            CurrentText = Text;
            float x = 0;
            float y = 0;
            float uW = Settings.GlyphWidth / (float)Renderer.Texture.Width;
            float vH = Settings.GlyphHeight / (float)Renderer.Texture.Height;
            float w = Settings.GlyphWidth / (float)Settings.GlyphHeight;
            const float h = 1.0f;
            
            //Calculate Size
            for (int n = 0; n < Text.Length; n++) {
                char idx = Text[n];
                
                if (idx == '\n') {
                    y += 1;
                    x = 0;
                    continue;
                }
                if (idx == '\t') {
                    x += (w + Settings.CharXSpacing) * 4;
                    continue;
                }

                x += w + Settings.CharXSpacing;
            }
            Size = new Vector2(x, y + h);
            
            //Apply TextAlign
            switch (TextAlign) {
                case TextAlign.Center:
                    x = -(Size.X / 2);
                    y = -(Size.Y / 2);
                    break;
                
                case TextAlign.Left:
                    x = 0;
                    y = -(Size.Y / 2);
                    break;
                    
                case TextAlign.Right:
                    x = -Size.X;
                    y = -(Size.Y / 2);
                    break;
                
                case TextAlign.Top:
                    x = -(Size.X / 2);
                    y = 0;
                    break;
                
                case TextAlign.Bottom:
                    x = -(Size.X / 2);
                    y = -Size.Y;
                    break;
                
                case TextAlign.TopLeft:
                    x = 0;
                    y = 0;
                    break;
                
                case TextAlign.TopRight:
                    x = -Size.X;
                    y = 0;
                    break;
                
                case TextAlign.BottomLeft:
                    x = 0;
                    y = -Size.Y;
                    break;
                
                case TextAlign.BottomRight:
                    x = -Size.X;
                    y = -Size.Y;
                    break;
                
                default:
                    throw new ArgumentException("The TextAlign " + TextAlign + " does not exist!");
            }
            
            //Generate Mesh
            List<float> vertices = new();
            for (int n = 0; n < Text.Length; n++) {
                char idx = Text[n];
                
                if (idx == '\n') {
                    y += 1;
                    x = 0;
                    continue;
                }
                if (idx == '\t') {
                    x += (w + Settings.CharXSpacing) * 4;
                    continue;
                }
                
                float u = (idx % Settings.GlyphsPerLine) * uW + Settings.TexCoordOffset;
                float v = (idx / Settings.GlyphsPerLine) * vH + Settings.TexCoordOffset;

               vertices.AddRange(new[] {
                    x,     y + h, 1.0f, TextColor.R, TextColor.G, TextColor.B, TextColor.A, u,      v + vH,
                    x + w, y + h, 1.0f, TextColor.R, TextColor.G, TextColor.B, TextColor.A, u + uW, v + vH,
                    x,     y,     1.0f, TextColor.R, TextColor.G, TextColor.B, TextColor.A, u,      v,
                    
                    x + w, y,     1.0f, TextColor.R, TextColor.G, TextColor.B, TextColor.A, u + uW, v,
                    x + w, y + h, 1.0f, TextColor.R, TextColor.G, TextColor.B, TextColor.A, u + uW, v + vH,
                    x,     y,     1.0f, TextColor.R, TextColor.G, TextColor.B, TextColor.A, u,      v
               });
                x += w + Settings.CharXSpacing;
            }

            Renderer.Mesh.UpdateMesh(vertices.ToArray());
        }

        public override void Render() {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            Renderer.Render();
        }

        public override void Destroy() {
            Renderer.Destroy();
        }
        
        public static TextSettings CompileFont(string fontNameOrUrl, string output, TextSettings settings = null) {
            #pragma warning disable CA1416
            if (settings is null)
                settings = new TextSettings();
            
            int bitmapWidth = settings.GlyphsPerLine * settings.GlyphWidth;
            int bitmapHeight = settings.GlyphLineCount * settings.GlyphHeight;

            using Bitmap texture = new Bitmap(bitmapWidth, bitmapHeight, PixelFormat.Format32bppArgb);
            Font font;
            if (File.Exists(fontNameOrUrl))
            {
                var collection = new PrivateFontCollection();
                collection.AddFontFile(fontNameOrUrl);
                var fontFamily = new FontFamily(Path.GetFileNameWithoutExtension(fontNameOrUrl), collection);
                font = new Font(fontFamily, settings.FontSize);
            }
            else {
                font = new Font(new FontFamily(fontNameOrUrl), settings.FontSize);
            }

            using (var g = Graphics.FromImage(texture)) {
                if (settings.BitmapFont) {
                    g.SmoothingMode = SmoothingMode.None;
                    g.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;
                }
                else {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
                }

                for (int p = 0; p < settings.GlyphLineCount; p++) {
                    for (int n = 0; n < settings.GlyphsPerLine; n++) {
                        char c = (char)(n + p * settings.GlyphsPerLine);
                        g.DrawString(c.ToString(), font, Brushes.White,
                            n * settings.GlyphWidth + settings.AtlasOffsetX,
                            p * settings.GlyphHeight + settings.AtlasOffsetY);
                    }
                }
            }

            texture.Save(output);

            return settings;
            #pragma warning restore CA1416
        }
    }

    [Serializable]
    public class TextSettings {
        public int GlyphsPerLine { get; set; } = 16;
        public int GlyphLineCount { get; set; } = 16;
        public int GlyphWidth { get; set; } = 30;
        public int GlyphHeight { get; set; } = 48;

        public float CharXSpacing { get; set; } = 0.0f;

        // Used to offset rendering glyphs to bitmap
        public int AtlasOffsetX { get; set; } = -3;
        public int AtlasOffsetY { get; set; } = -1;
        public int FontSize { get; set; } = 35;
        public bool BitmapFont { get; set; } = false;
        public float TexCoordOffset { get; set; } = 0.005f;

        public void Save(string file) {
            string json = JsonSerializer.Serialize(this, new JsonSerializerOptions {WriteIndented = true});
            File.WriteAllText(file, json, Encoding.UTF8);
        }

        public static TextSettings Load(string file) {
            string text = File.ReadAllText(file, Encoding.UTF8);
            return JsonSerializer.Deserialize<TextSettings>(text);
        }
    }

    public enum TextAlign : byte {
        Center = 0x00,
        Left = 0x01,
        Right = 0x02,
        Top = 0x03,
        Bottom = 0x04,
        TopLeft = 0x05,
        TopRight = 0x06,
        BottomLeft = 0x07,
        BottomRight = 0x08
    }
}