﻿using GLEngine.Rendering;
using GLEngine.Resources;

namespace GLEngine.Objects.Components {
    public class MeshRenderer : ObjectComponent {
        
        public Shader Shader { get; set; }
        public Texture Texture { get; set; }
        public Mesh Mesh { get; set; }

        public MeshRenderer(string mesh = Mesh.Default, string shader = Shader.Default, string texture = Texture.Default) {
            Texture = ResourceLoader.GetResource<Texture>(texture);
            Shader = ResourceLoader.GetResource<Shader>(shader);
            Mesh = ResourceLoader.GetResource<Mesh>(mesh);
        }
        
        public override void Awake() {
            Shader.Load();
            Texture.Load();
            Mesh.Load();
        }

        public override void Update() {}

        public override void Render() {
            Texture.Bind();
            Shader.Bind();
            
            Shader.SetMatrix4X4("model", GameObject.Model);
            Shader.SetMatrix4X4("projection", GameObject.Scene.Camera.Projection);

            Mesh.Render();
            
            Shader.Unbind();
            Texture.Unbind();
        }

        public override void Destroy() {
            Texture.Delete();
            Shader.Delete();
            Mesh.Delete();
        }
    }
}