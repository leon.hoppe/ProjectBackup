﻿namespace GLEngine.Objects.Components {
    public abstract class MonoBehavior : ObjectComponent {
        public override void Render() {}
        public override void Awake() {}
        public override void Update() {}
        public override void Destroy() {}
    }
}