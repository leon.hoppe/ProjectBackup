﻿using System.Collections.Generic;
using System.Linq;
using GLEngine.Objects;

namespace GLEngine.Rendering {
    public class Scene : IGameEvents {

        public List<GameObject> Objects { get; private set; } = new();
        public Camera Camera { get; set; } = new();
        public ISceneEventListener EventListener { get; set; }

        public void Awake() {
            EventListener?.Awake();
            Objects.ForEach(o => o.Awake());
        }

        public void Update() {
            Camera.UpdateMatirx();
            Objects = Objects.OrderBy(o => o.Layer).ToList();
            
            EventListener?.Update();
            
            Objects.ForEach(o => o.Update());
        }

        public void Render() {
            Objects.ForEach(o => o.Render());
            EventListener?.Render();
        }

        public void Destroy() {
            Objects.ForEach(o => o.Destroy());
            EventListener?.Destroy();
        }

        public GameObject Instantiate(GameObject gameObject) {
            gameObject.Scene = this;
            if (Engine.Instance.CurrentScene == this)
                gameObject.Awake();
            Objects.Add(gameObject);
            return gameObject;
        }

        public void Destory(GameObject gameObject) {
            if (gameObject.Scene != this) return;
            Objects.Remove(gameObject);
            gameObject.Destroy();
        }
    }

    public interface ISceneEventListener : IGameEvents { }
}