﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using GLEngine.Extensions;
using GLEngine.Resources;
using static OpenGL.GL;

namespace GLEngine.Rendering {
    public class Mesh : IResource {
        public const string Default = "mesh_default";
        
        public Vector3[] Vertices { get; set; }
        public Color[] Colors { get; set; }
        public Vector2[] UVs { get; set; }
        public int[] Triangles { get; set; }
        public float[] Data { get; private set; }
        public int Size { get; private set; }
        
        private uint Address { get; set; }
        private uint Vbo { get; set; }
        private int Mode { get; }
        private bool _loaded = false;

        public Mesh(DrawMode mode = DrawMode.Static) {
            Vertices = Array.Empty<Vector3>();
            Colors = Array.Empty<Color>();
            UVs = Array.Empty<Vector2>();
            Triangles = Array.Empty<int>();
            Mode = (int)mode;
        }
        
        public void Load() {
            if (_loaded || Triangles.Length == 0) return;
            UpdateMesh();
        }

        public void Delete() {
            if (!_loaded) return;
            Unbind();
            
            glDeleteBuffer(Vbo);
            glDeleteVertexArray(Address);
        }

        public void Bind() {
            glBindBuffer(GL_ARRAY_BUFFER, Vbo);
            glBindVertexArray(Address);
        }

        public void Unbind() {
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        }

        public void Render(int renderMode = GL_TRIANGLES) {
            Bind();
            glDrawArrays(renderMode, 0, Size);
            Unbind();
        }

        public void UpdateMesh() {
            CompileMeshData();
            UpdateMesh(Data);
        }
        
        public unsafe void UpdateMesh(float[] rawData) {
            Data = rawData;
            Size = rawData.Length / 9;

            if (!_loaded) {
                Address = glGenVertexArray();
                Vbo = glGenBuffer();
            }
            
            Bind();

            fixed (float* v = &Data[0]) {
                glBufferData(GL_ARRAY_BUFFER, sizeof(float) * Data.Length, v, Mode);
            }

            if (!_loaded) {
                glVertexAttribPointer(0, 3, GL_FLOAT, false, 9 * sizeof(float), (void*)0);
                glEnableVertexAttribArray(0);
            
                glVertexAttribPointer(1, 4, GL_FLOAT, false, 9 * sizeof(float), (void*)(3 * sizeof(float)));
                glEnableVertexAttribArray(1);
            
                glVertexAttribPointer(2, 2, GL_FLOAT, false, 9 * sizeof(float), (void*)(7 * sizeof(float)));
                glEnableVertexAttribArray(2);
                _loaded = true;
            }
            
            Unbind();
        }

        private void CompileMeshData() {
            List<float> data = new();

            for (int i = 0; i < Triangles.Length; i++) {
                int index = Triangles[i];
                Vector3 vertex = Vertices[index % Vertices.Length];
                Color color = Colors[index % Colors.Length];
                Vector2 uv = UVs[index % UVs.Length];
                
                data.AddRange(new [] {
                    vertex.X,
                    vertex.Y,
                    vertex.Z,
                    
                    color.GetRed(),
                    color.GetGreen(),
                    color.GetBlue(),
                    color.GetAlpha(),
                    
                    uv.X,
                    uv.Y
                });
            }

            Data = data.ToArray();
            Size = Triangles.Length;
        }
    }

    public enum DrawMode {
        Static = GL_STATIC_DRAW,
        Dynamic = GL_DYNAMIC_DRAW,
        Stream = GL_STREAM_DRAW
    }
}