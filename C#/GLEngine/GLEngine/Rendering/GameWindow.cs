﻿using System;
using System.IO;
using GLFW;
using StbiSharp;
using static OpenGL.GL;

namespace GLEngine.Rendering {
    public class GameWindow : NativeWindow {

        public GameWindow(int width, int height, string title, bool vsync) : base(width, height, title) {
            CenterOnScreen();
            MakeCurrent();
            Import(Glfw.GetProcAddress);
            glViewport(0, 0, width, height);
            Glfw.SwapInterval(vsync ? 1 : 0);
            SizeChanged += OnResize;
        }
        
        private void OnResize(object sender, SizeChangeEventArgs e) {
            glViewport(0, 0, Size.Width, Size.Height);
        }
        
        public unsafe void SetWindowIcon(string file) {
            using var stream = File.OpenRead(file);
            using var memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);
            StbiImage image = Stbi.LoadFromMemory(memoryStream, 0);

            fixed (byte* data = &image.Data[0]) {
                SetIcons(new Image(image.Width, image.Height, new IntPtr(data)));
            }
        }
        
    }
}