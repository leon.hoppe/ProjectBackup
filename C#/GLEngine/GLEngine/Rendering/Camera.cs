﻿using System.Drawing;
using System.Numerics;

namespace GLEngine.Rendering {
    public class Camera {
        private const float GameScale = 100.0f;

        public static Camera Main => Engine.Instance.CurrentScene.Camera;
        
        public Transform Transform { get; }
        public RenderingMode Mode { get; set; }
        public float NearClip { get; set; }
        public float FarClip { get; set; }
        
        public Matrix4x4 Projection { get; private set; }

        public Camera(RenderingMode mode = RenderingMode.Orthographic) {
            Transform = new Transform {
                Position = Vector3.Zero,
                Scale = Vector3.One,
                Rotation = Vector3.Zero
            };
            Mode = mode;
            NearClip = 0.01f;
            FarClip = 100.0f;
        }
        
        public void UpdateMatirx() {
            Size windowSize = Engine.Instance.Window.Size;
            float left = Transform.Position.X - windowSize.Width / 2.0f;
            float right = Transform.Position.X + windowSize.Width / 2.0f;
            float top = Transform.Position.Y - windowSize.Height / 2.0f;
            float bottom =Transform.Position.Y + windowSize.Height / 2.0f;

            Matrix4x4 matrix;

            switch (Mode) {
                case RenderingMode.Orthographic:
                    matrix = Matrix4x4.CreateOrthographicOffCenter(left, right, bottom, top, NearClip * GameScale, FarClip * GameScale);
                    break;
                case RenderingMode.Perspective:
                    matrix = Matrix4x4.CreatePerspectiveOffCenter(left, right, bottom, top, NearClip * GameScale, FarClip * GameScale);
                    break;
                default:
                    matrix = Matrix4x4.Identity;
                    break;
            }

            Matrix4x4 scale = Matrix4x4.CreateScale(GameScale * Transform.Scale.Z);
            Matrix4x4 rot = Matrix4x4.Identity; //TODO: Implement Rotation

            Projection = matrix * rot * scale;
        }
    }

    public enum RenderingMode {
        Orthographic,
        Perspective
    }
}