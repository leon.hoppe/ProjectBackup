﻿namespace GLEngine.Animations {
    public readonly struct Keyframe<T> {

        public readonly T Value;
        public readonly float Time;
        public readonly float Offset;
        public readonly KeyframeType Type;
        public readonly IAnimationFunction AnimationFunction;

        public Keyframe(T value, float time, float offset, KeyframeType type, IAnimationFunction animationFunction = null) {
            animationFunction ??= AnimationFunctions.Linear;
            Value = value;
            Time = time;
            Offset = offset;
            Type = type;
            AnimationFunction = animationFunction;
        }

    }

    public enum KeyframeType : byte {
        Position = 0x00,
        Scale = 0x01,
        Rotation = 0x02,
        Sprite = 0x03
    }
    
}