﻿using System;
using System.Collections.Generic;
using System.Numerics;
using GLEngine.Extensions;
using GLEngine.Objects;
using GLEngine.Objects.Components;
using GLEngine.Rendering;
using GLEngine.Resources;
using OpenGL;

namespace GLEngine.Animations {
    public class Animation {

        public readonly string Name;
        public readonly bool Autostart;
        public bool Loop;

        protected GameObject Object;

        protected Animatable<Vector3> Position = new();
        protected Animatable<Vector3> Scale = new();
        protected Animatable<Vector3> Rotation = new();
        protected Animatable<Texture> Sprite = new();

        protected bool Started = false;
        
        protected MeshRenderer Renderer;
        protected Mesh Mesh;

        public Animation(string name, bool loop, bool autostart, params Keyframe<Vector3>[] keyframes) {
            List<Keyframe<Vector3>> vectors = new();
            List<Keyframe<Vector3>> scales = new();
            List<Keyframe<Vector3>> rots = new();
            
            foreach (var keyframe in keyframes) {
                switch (keyframe.Type) {
                    case KeyframeType.Position:
                        vectors.Add(keyframe);
                        break;
                    
                    case KeyframeType.Scale:
                        scales.Add(keyframe);
                        break;
                    
                    case KeyframeType.Rotation:
                        rots.Add(keyframe);
                        break;
                    
                    default:
                        throw new ArgumentException(keyframe.Type + " is not animatable!");
                }
            }

            Position.Keyframes = vectors.ToArray();
            Scale.Keyframes = scales.ToArray();
            Rotation.Keyframes = rots.ToArray();

            Name = name;
            Autostart = autostart;
            Loop = loop;
        }
        
        public Animation(string name, bool loop, bool autostart, params Keyframe<Texture>[] keyframes) {
            Sprite.Keyframes = keyframes;
            Name = name;
            Autostart = autostart;
            Loop = loop;
        }
        
        public Animation(string name, bool loop, bool autostart, params Keyframe<string>[] keyframes) {
            Keyframe<Texture>[] textures = new Keyframe<Texture>[keyframes.Length];
            
            for (int i = 0; i < keyframes.Length; i++) {
                Keyframe<string> keyframe = keyframes[i];
                Texture texture = ResourceLoader.GetResource<Texture>(keyframe.Value);
                textures[i] = new Keyframe<Texture>(texture, keyframe.Time, keyframe.Offset, keyframe.Type,
                    keyframe.AnimationFunction);
            }

            Sprite.Keyframes = textures;

            Name = name;
            Autostart = autostart;
            Loop = loop;
        }

        public void Initialize(GameObject gameObject) {
            Object = gameObject;
            if (Autostart) Start();
        }

        public bool Running => Position.Running | Scale.Running | Rotation.Running | Sprite.Running;

        public void Start() {
            Stop();

            Position.Reset(Object.Transform.Position.CreateCopy());
            Scale.Reset(Object.Transform.Scale.CreateCopy());
            Rotation.Reset(Object.Transform.Rotation.CreateCopy());

            if (Sprite.Keyframes.Length != 0) {
                if (!Object.HasComponent<MeshRenderer>())
                    throw new Exception($"Object {Object.Name} does not have a MeshRenderer but a Sprite animation attached!");

                Renderer = Object.GetComponent<MeshRenderer>();
                Mesh = new Mesh(DrawMode.Stream);
                Mesh.Load();
                
                foreach (var frame in Sprite.Keyframes) {
                    frame.Value.Load();
                }
                
                Sprite.Reset(Renderer.Texture);
            }

            Started = true;
        }

        public void Stop() {
            Position.Running = false;
            Scale.Running = false;
            Rotation.Running = false;
            
            if (Mesh != null) {
                Mesh.Delete();
                Mesh = null;
            }

            Started = false;
        }

        public void Update() {
            if (Loop && Started && !Running) Start();
            if (!Running && Started) Stop();
            
            if (Position.Running)
                Object.Transform.Position = UpdateAnimatableVector(Position, Object.Transform.Position);

            if (Scale.Running)
                Object.Transform.Scale = UpdateAnimatableVector(Scale, Object.Transform.Scale);

            if (Rotation.Running)
                Object.Transform.Rotation = UpdateAnimatableVector(Rotation, Object.Transform.Rotation);
        }

        public void Render() {
            if (!Sprite.Running) return;

            Keyframe<Texture> frame = Sprite.CurrentFrame;
            float deltaTime = Time.ElapsedTime - Sprite.StartTime - frame.Offset;
            
            if (deltaTime >= frame.Time) {
                Sprite.CurrentFrameIndex++;
                Sprite.Origin = frame.Value;
                Sprite.StartTime = Time.ElapsedTime;

                if (Sprite.CurrentFrameIndex >= Sprite.Keyframes.Length)
                    Sprite.Running = false;

                Renderer.Texture = frame.Value;
                
                if (frame.Time == 0) return;
            }
            
            if (deltaTime <= 0) return;
            
            float progress = deltaTime / frame.Time;
            float lerp = frame.AnimationFunction.GetProgres(progress);
            Vector4 color = new Vector4(Vector3.One, lerp);
            
            float[] meshVerts = Renderer.Mesh.Data;
            float[] verts = new float[meshVerts.Length];
            for (int i = 0; i < verts.Length; i += 9) {
                verts[i]     = meshVerts[i];
                verts[i + 1] = meshVerts[i + 1];
                verts[i + 2] = meshVerts[i + 2];

                verts[i + 3] = color.X;
                verts[i + 4] = color.Y;
                verts[i + 5] = color.Z;
                verts[i + 6] = color.W;
                
                verts[i + 7] = meshVerts[i + 7];
                verts[i + 8] = meshVerts[i + 8];
            }
            Mesh.UpdateMesh(verts);
            
            GL.glEnable(GL.GL_BLEND);
            GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
            frame.Value.Bind();
            Renderer.Shader.Bind();
            
            Mesh.Render();
            
            Renderer.Shader.Unbind();
            frame.Value.Unbind();
        }

        protected Vector3 UpdateAnimatableVector(Animatable<Vector3> animatable, Vector3 origin) {
            Keyframe<Vector3> frame = animatable.CurrentFrame;
            float deltaTime = Time.ElapsedTime - animatable.StartTime - frame.Offset;

            if (deltaTime >= frame.Time) {
                animatable.CurrentFrameIndex++;
                animatable.Origin = animatable.Origin + frame.Value;
                animatable.StartTime = Time.ElapsedTime;

                if (animatable.CurrentFrameIndex >= animatable.Keyframes.Length)
                    animatable.Running = false;
                
                return animatable.Origin;
            }
            
            if (deltaTime <= 0) return origin;

            Vector3 motion = frame.Value;
            float progress = deltaTime / frame.Time;
            motion *= frame.AnimationFunction.GetProgres(progress);
            return animatable.Origin + motion;
        }
        
        protected class Animatable<T> {
            public Keyframe<T>[] Keyframes { get; set; } = Array.Empty<Keyframe<T>>();
            public bool Running { get; set; }
            public int CurrentFrameIndex { get; set; }
            public float StartTime { get; set; }
            public T Origin { get; set; }

            public Keyframe<T> CurrentFrame => Keyframes[CurrentFrameIndex];

            public void Reset(T original) {
                Running = Keyframes.Length != 0;
                CurrentFrameIndex = 0;
                StartTime = Time.ElapsedTime;
                Origin = original;
            }
        }
    }
}