﻿namespace GLEngine.Animations {
    public interface IAnimationFunction {
        public float GetProgres(float state);
    }
}