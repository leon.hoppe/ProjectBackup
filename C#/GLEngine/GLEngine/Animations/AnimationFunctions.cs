﻿namespace GLEngine.Animations {
    public static class AnimationFunctions {

        public static readonly IAnimationFunction Linear = new LinearAnimation();
        public static readonly IAnimationFunction Smooth = new SmoothAnimation();

        private class LinearAnimation : IAnimationFunction {
            public float GetProgres(float state) => state;
        }
        private class SmoothAnimation : IAnimationFunction {
            public float GetProgres(float state) => state * state * (3.0f - 2.0f * state);
        }
    }
}