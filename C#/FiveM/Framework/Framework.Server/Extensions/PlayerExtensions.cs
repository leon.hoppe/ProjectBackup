﻿using System;
using CitizenFX.Core;
using Framework.Server.Handlers;
using Framework.Shared.Models;

namespace Framework.Server.Extensions {
    public static class PlayerExtensions {

        public static int ServerId(this Player player) => Convert.ToInt32(player.Handle);

        public static PlayerData GetPlayerData(this Player player) => JoinHandler.PlayerDatas[player.ServerId()];

    }
}