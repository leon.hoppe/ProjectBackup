﻿using System;
using System.Collections.Generic;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Framework.Server.Extensions;
using Framework.Shared.Extensions;
using Framework.Shared.Models;

namespace Framework.Server.Handlers {
    public static class JoinHandler {

        public static Vector3 Spawn = new Vector3(258.7932f, -942.7487f, 29.3940f);
        public static readonly IDictionary<int, PlayerData> PlayerDatas = new Dictionary<int, PlayerData>();

        public static async void OnPlayerJoining([FromSource] Player player) {
            API.SetPlayerRoutingBucket(player.Handle, player.ServerId());
            API.SetRoutingBucketPopulationEnabled(player.ServerId(), false);
            API.SetRoutingBucketEntityLockdownMode(player.ServerId(), "strict");
            API.SetPlayerInvincible(player.Handle, true);
            
            var identifier = "license:" + player.Identifiers["license"];
            var result = await MySql.FetchAll<PlayerData>($"SELECT * FROM players WHERE owner = '{identifier}'");

            if (result.Count == 0) player.TriggerEvent("client:spawn:register");
            else player.TriggerEvent("client:spawn:choose", result);
        }

        public static async void OnRegister([FromSource] Player player, PlayerData data) {
            var identifier = "license:" + player.Identifiers["license"];
            data.LastPosition = Spawn.ToArray();
            await MySql.Execute($"INSERT INTO players VALUES ('{identifier}', '{Guid.NewGuid()}', {data.Sex}, {data.Height}, '{data.FirstName}', '{data.LastName}', '{data.Birth}', {data.Skin}, '{data.LastPosition.Serialize()}')");
            
            LoadCharacter(player, data);
        }

        public static async void CharacterSelected([FromSource] Player player, string charId) {
            var data = await MySql.FetchOne<PlayerData>($"SELECT * FROM players WHERE CharId = '{charId}'");
            LoadCharacter(player, data);
        }

        private static void LoadCharacter(Player player, PlayerData data) {
            player.Character.Position = data.LastPosition.ToVector3();
            API.SetPlayerModel(player.Handle, data.Skin);
            
            PlayerDatas.Remove(player.ServerId());
            PlayerDatas.Add(player.ServerId(), data);
            API.SetPlayerInvincible(player.Handle, false);
            API.SetPlayerRoutingBucket(player.Handle, 0);
        }
        
    }
}