﻿#pragma warning disable CS1998

using System;
using System.Collections.Generic;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Framework.Server.Extensions;
using Framework.Server.Handlers;
using Framework.Shared.Abstraction;
using Framework.Shared.Extensions;
using Framework.Shared.Models;

namespace Framework.Server {
    public sealed class Server : ServerScript {
        private const int SaveInterval = 10 * 60 * 1000;
        private long _lastSave;

        public Server() {
            MySql.Initialize("Server=161.97.88.49;Database=s20_FiveM_Private_Test;Uid=u20_HDXeunXq92;Pwd=+LP=xeHyVw9oyJh@@BFPU3x2;");
            MySql.CreateTables(
                "CREATE TABLE IF NOT EXISTS players (Owner VARCHAR(100), CharId VARCHAR(100) PRIMARY KEY, Sex BOOLEAN, Height INT(3), FirstName VARCHAR(60), LastName VARCHAR(60), Birth VARCHAR(10), Skin INT(255), LastPosition VARCHAR(255))"
            );
            
            EventHandlers["playerJoining"] += new Action<Player>(JoinHandler.OnPlayerJoining);
            EventHandlers["playerDropped"] += new Action<Player>(SavePlayer);
            EventHandlers["server:spawn:register"] += EventHelper.CreateHandler<Player, PlayerData>(JoinHandler.OnRegister);
            EventHandlers["server:spawn:select"] += new Action<Player, string>(JoinHandler.CharacterSelected);
            EventHandlers["server:save"] += new Action<Player>(SavePlayer);
            EventHandlers["onResourceStart"] += new Action<string>(OnStart);

            Tick += async () => {
                var currentTime = API.GetGameTimer();

                if (currentTime - _lastSave > SaveInterval) {
                    SaveAllCmd();
                    _lastSave = currentTime;
                }
            };
        }

        private async void OnStart(string name) {
            if (API.GetCurrentResourceName() != name) return;
            await Delay(2000);
            
            foreach (var player in Players) {
                JoinHandler.OnPlayerJoining(player);
            }
        }

        public static async void SavePlayer([FromSource] Player player) {
            if (!JoinHandler.PlayerDatas.ContainsKey(player.ServerId())) return;
            var data = player.GetPlayerData();
            
            data.LastPosition = player.Character.Position.ToArray();

            await MySql.Execute($"UPDATE players SET LastPosition = '{data.LastPosition.Serialize()}', Skin = '{data.Skin}' WHERE CharId = '{data.CharId}'");
        }

        [Command("changechar")]
        private void ChangeCharCmd([FromSource] Player player) {
            SavePlayer(player);
            JoinHandler.OnPlayerJoining(player);
        }

        [Command("clear")]
        private void ClearCmd([FromSource] Player player) {
            player.TriggerEvent("chat:clear");
        }

        [Command("save")]
        private void SaveCmd([FromSource] Player player) {
            SavePlayer(player);
        }

        [Command("car", Restricted = true)]
        private void CarCmd([FromSource] Player player, string[] args) {
            var name = args.Length > 0 ? args[0] : "t20";
            var pos = player.Character.Position;

            var vehicle = API.CreateVehicle((uint)API.GetHashKey(name), pos.X, pos.Y, pos.Z, player.Character.Heading, true, true);
            API.SetPedIntoVehicle(player.Character.Handle, vehicle, -1);
        }

        [Command("dv", Restricted = true)]
        private void DvCmd([FromSource] Player player, string[] args) {
            var range = args.Length > 0 ? Convert.ToInt32(args[0]) : 5;
            var playerPos = player.Character.Position;

            var vehicles = API.GetAllVehicles() as List<object> ?? new List<object>();

            foreach (var obj in vehicles) {
                var vehicle = Convert.ToInt32(obj);
                var pos = API.GetEntityCoords(vehicle);
                if (pos.DistanceToSquared(playerPos) <= range)
                    API.DeleteEntity(vehicle);
            }
        }

        [Command("loadout", Restricted = true)]
        private void LoadoutCmd([FromSource] Player player) {
            player.TriggerEvent("client:utils:giveAllWeapons");
        }

        [Command("giveweapon", Restricted = true)]
        private void GiveWeaponCmd([FromSource] Player player, string[] args) {
            var weapon = args.Length > 0 ? args[0] : "WEAPON_PISTOL";
            var ammo = args.Length > 1 ? Convert.ToInt32(args[1]) : 999999;
            
            player.TriggerEvent("client:utils:giveWeapon", weapon, ammo);
        }

        [Command("tp", Restricted = true)]
        private void TpCmd([FromSource] Player player, string[] args) {
            if (args.Length < 3) return;
            var coords = new Vector3(Convert.ToSingle(args[0]), Convert.ToSingle(args[1]), Convert.ToSingle(args[2]));
            player.Character.Position = coords;
        }

        [Command("clearall", Restricted = true)]
        private void ClearAllCmd() {
            TriggerClientEvent("chat:clear");
        }

        [Command("saveall", Restricted = true)]
        private void SaveAllCmd() {
            foreach (var player in Players) {
                SavePlayer(player);
            }
        }

        [Command("goto", Restricted = true)]
        private void GotoCmd([FromSource] Player player, string[] args) {
            if (args.Length < 1) return;
            var other = Convert.ToInt32(args[0]);

            player.Character.Position = Players[other].Character.Position;
        }
        
        [Command("bring", Restricted = true)]
        private void BringCmd([FromSource] Player player, string[] args) {
            if (args.Length < 1) return;
            var other = Convert.ToInt32(args[0]);

            Players[other].Character.Position = player.Character.Position;
        }
        
        [Command("kick", Restricted = true)]
        private void KickCmd([FromSource] Player player, string[] args) {
            if (args.Length < 1) return;
            var other = Convert.ToInt32(args[0]);
            var reason = args.Length > 1 ? args[1] : "";

            Players[other].Drop(reason);
        }

    }
}