﻿#pragma warning disable CS1998

using System;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Framework.Client.Handler;
using Framework.Client.Utils;
using Framework.Shared.Abstraction;
using Framework.Shared.Models;
using NativeUI;

namespace Framework.Client {
    public sealed class Client : ClientScript {
        public static MenuPool Menus;

        public Client() {
            Menus = new MenuPool();
            
            JoinHandler.Init();
            SetupMapZooms();

            EventHandlers["client:spawn:register"] += new Action(JoinHandler.Register);
            EventHandlers["client:spawn:choose"] += EventHelper.CreateHandlerList<PlayerData>(JoinHandler.ChooseChar);
            EventHandlers["client:utils:giveAllWeapons"] += new Action(GiveAllWeapons);
            EventHandlers["client:utils:giveWeapon"] += new Action<string, int>(GiveWeapon);

            Tick += async () => {
                Menus.ProcessMenus();
                
                API.RestorePlayerStamina(Game.Player.Handle, 1.0f);
                API.DisableIdleCamera(true);
                Game.Player.WantedLevel = 0;
            };
        }

        private void SetupMapZooms() {
            API.SetMapZoomDataLevel(0, 0.96f, 0.9f, 0.08f, 0.0f, 0.0f);
            API.SetMapZoomDataLevel(1, 1.6f, 0.9f, 0.08f, 0.0f, 0.0f);
            API.SetMapZoomDataLevel(2, 8.6f, 0.9f, 0.08f, 0.0f, 0.0f);
            API.SetMapZoomDataLevel(3, 12.3f, 0.9f, 0.08f, 0.0f, 0.0f);
            API.SetMapZoomDataLevel(4, 22.3f, 0.9f, 0.08f, 0.0f, 0.0f);
            API.SetRadarZoom(1100);
            
            // Cayo Perico Blip
            var blip = API.AddBlipForCoord(6701.6851f, -6454.1108f, 328.1440f);
            API.SetBlipColour(blip, 10);
            API.SetBlipCategory(blip, 1);
            API.SetBlipScale(blip, 0.0f);
            API.SetBlipRotation(blip, 0);
            API.SetBlipSprite(blip, 806);
            API.SetBlipDisplay(blip, 3);
            
            API.BeginTextCommandSetBlipName("STRING");
            API.AddTextComponentString("???");
            API.EndTextCommandSetBlipName(blip);
        }

        private void GiveAllWeapons() {
            Game.PlayerPed.Weapons.RemoveAll();
            foreach (var hash in Util.GetEnumValues<WeaponHash>()) {
                Game.PlayerPed.Weapons.Give(hash, 999999, false, true);
            }
        }

        private void GiveWeapon(string weapon, int ammo) {
            Game.PlayerPed.Weapons.Give((WeaponHash)API.GetHashKey(weapon), ammo, true, true);
        }

    }
}