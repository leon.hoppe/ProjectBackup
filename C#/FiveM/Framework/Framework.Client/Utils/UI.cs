﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace Framework.Client.Utils {
    public static class UI {

        public static async Task<string> DisplayTextInput(string title, int maxLength) {
            API.AddTextEntry("CH_INPUT", title);
            API.DisplayOnscreenKeyboard(1, "CH_INPUT", "", "", "", "", "", maxLength);

            while (API.UpdateOnscreenKeyboard() == 0) {
                await BaseScript.Delay(0);
            }

            if (API.UpdateOnscreenKeyboard() != 2) {
                var result = API.GetOnscreenKeyboardResult();
                await BaseScript.Delay(0);
                return result;
            }
            
            await BaseScript.Delay(0);
            return null;
        }

        public static async Task<T> DisplayTextInput<T>(string title, int maxLength) {
            var result = await DisplayTextInput(title, maxLength);
            return (T)Convert.ChangeType(result, typeof(T));
        }

    }
}