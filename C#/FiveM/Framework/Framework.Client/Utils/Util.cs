﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace Framework.Client.Utils {
    public static class Util {
        public static IEnumerable<T> GetEnumValues<T>() => Enum.GetValues(typeof(T)).Cast<T>();

        public static async Task LoadModel(Model model) {
            API.RequestModel((uint)model);

            while (!model.IsLoaded) {
                await BaseScript.Delay(0);
            }
        }
    }
}