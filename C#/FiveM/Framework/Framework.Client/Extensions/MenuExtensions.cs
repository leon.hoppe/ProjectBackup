﻿using NativeUI;

namespace Framework.Client.Extensions {
    public static class MenuExtensions {

        public static void RemoveMenu(this MenuPool pool, UIMenu menu) => pool.ToList().Remove(menu);

    }
}