﻿using CitizenFX.Core;

namespace Framework.Client.Extensions {
    public static class VectorExtensions {
        public static Vector2 ToVector2(this float[] vector) => new Vector2(vector);
        public static Vector3 ToVector3(this float[] vector) => new Vector3(vector);
        public static Vector4 ToVector4(this float[] vector) => new Vector4(vector);
    }
}