﻿using System;
using System.Collections.Generic;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Framework.Client.Utils;
using Framework.Client.Extensions;
using Framework.Shared.Models;
using NativeUI;

namespace Framework.Client.Handler {
    public static class JoinHandler {
        private static UIMenu _registerMenu;
        private static UIMenu _skinMenu;
        private static PlayerData _data;

        public static void Init() {
            InitRegisterMenu();
        }

        private static void InitRegisterMenu() {
            _registerMenu = new UIMenu("PrincepRP", "Registrierung") {
                MouseControlsEnabled = false
            };
            Client.Menus.Add(_registerMenu);

            var sex = new UIMenuItem("Geschlecht setzen", "0 -> Männlich, 1 -> Weiblich");
            var height = new UIMenuItem("Größe setzen", "Wert in cm");
            var firstName = new UIMenuItem("Vornamen setzen");
            var lastName = new UIMenuItem("Nachnamen setzen");
            var birth = new UIMenuItem("Geburtsdatum setzen", "DD.MM.JJJJ, min. 18 Jahre");
            var done = new UIMenuItem("Charackter erstellen");
            
            _registerMenu.AddItem(sex);
            _registerMenu.AddItem(height);
            _registerMenu.AddItem(firstName);
            _registerMenu.AddItem(lastName);
            _registerMenu.AddItem(birth);
            _skinMenu = Client.Menus.AddSubMenu(_registerMenu, "Skin erstellen");
            _registerMenu.AddItem(done);
            Client.Menus.RefreshIndex();

            sex.Activated += async (sender, item) => {
                var input = await UI.DisplayTextInput<int>("Geschlecht setzen", 1);
                _data.Sex = input == 0;

                if (_data.Sex) sex.Description = "Männlich";
                else sex.Description = "Weiblich";
                    
                PopulateSkinMenu();
            };

            height.Activated += async (sender, item) => {
                var input = await UI.DisplayTextInput<int>("Größe setzen", 3);
                _data.Height = input;
                height.Description = _data.Height + "cm";
            };
            
            firstName.Activated += async (sender, item) => {
                var input = await UI.DisplayTextInput("Vornamen setzen", 60);
                _data.FirstName = input;
                firstName.Description = _data.FirstName;
            };
            
            lastName.Activated += async (sender, item) => {
                var input = await UI.DisplayTextInput("Nachnamen setzen", 60);
                _data.LastName = input;
                lastName.Description = _data.LastName;
            };

            birth.Activated += async (sender, item) => {
                var input = await UI.DisplayTextInput("Geburtsdatum setzen", 10);
                _data.Birth = input;
                birth.Description = _data.Birth;
            };

            done.Activated += (sender, item) => {
                // TODO: Checks
                _registerMenu.Visible = false;
                BaseScript.TriggerServerEvent("server:spawn:register", _data);
            };
        }

        private static void PopulateSkinMenu() {
            _skinMenu.MenuItems.Clear();
            foreach (var hash in Util.GetEnumValues<PedHash>()) {
                var name = Enum.GetName(typeof(PedHash), hash) ?? "";
                if (_data.Sex && !name.EndsWith("AMM")) continue;
                if (!_data.Sex && !name.EndsWith("AFM")) continue;
                
                var element = new UIMenuItem(name);

                element.Activated += async (sender, item) => {
                    await Util.LoadModel(new Model(hash));
                    API.SetPlayerModel(Game.Player.Handle, (uint)hash);
                    _data.Skin = (uint)hash;
                };
                
                _skinMenu.AddItem(element);
            }
        }

        public static void Register() {
            _data = new PlayerData();
            Client.Menus.CloseAllMenus();
            _registerMenu.Visible = true;
        }

        public static void ChooseChar(List<PlayerData> chars) {
            var menu = new UIMenu("PrincepRP", "Charakterauswahl") {
                MouseControlsEnabled = false
            };

            foreach (var data in chars) {
                var element = new UIMenuItem($"{data.FirstName} {data.LastName}");
                menu.AddItem(element);

                element.Activated += (sender, item) => {
                    BaseScript.TriggerServerEvent("server:spawn:select", data.CharId);
                    menu.Visible = false;
                    Client.Menus.RemoveMenu(menu);
                };
            }

            var createNew = new UIMenuItem("Neuen Charakter erstellen");
            menu.AddItem(createNew);

            createNew.Activated += (sender, item) => {
                menu.Visible = false;
                Client.Menus.RemoveMenu(menu);
                Register();
            };
            
            Client.Menus.Add(menu);
            Client.Menus.CloseAllMenus();
            menu.Visible = true;
        }
        
    }
}