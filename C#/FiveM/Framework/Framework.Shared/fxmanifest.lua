fx_version 'cerulean'
games { 'gta5' }

author 'Leon hoppe <leon@ladenbau-hoppe.de>'
description 'FiveM Framework in C#'
version '1.0.0'

this_is_a_map 'yes'

data_file "SCALEFORM_DLC_FILE" "stream/[map]/int3232302352.gfx"

files {
    'NativeUI.dll',
    'MySql.Data.dll',
    'Framework.Shared.dll'
}

client_scripts {
    'Framework.Client.net.dll'
}

server_scripts {
    'Framework.Server.net.dll'
}