﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Framework.Shared.Abstraction {
    public static class EventHelper {
        
        public static Action<ExpandoObject> CreateHandler<T1>(Action<T1> handler) where T1 : Packet, new() {
            return (obj1) => {
                handler.Invoke(ConvertToPacket<T1>(obj1));
            };
        }
        
        public static Action<List<object>> CreateHandlerList<T1>(Action<List<T1>> handler) where T1 : Packet, new() {
            return (obj1) => {
                handler.Invoke(ConvertToPacketList<T1>(obj1));
            };
        }
        
        public static Action<T1, ExpandoObject> CreateHandler<T1, T2>(Action<T1, T2> handler) where T2 : Packet, new() {
            return (obj1, obj2) => {
                handler.Invoke(obj1, ConvertToPacket<T2>(obj2));
            };
        }
        
        public static Action<T1, List<object>> CreateHandlerList<T1, T2>(Action<T1, List<T2>> handler) where T2 : Packet, new() {
            return (obj1, obj2) => {
                handler.Invoke(obj1, ConvertToPacketList<T2>(obj2));
            };
        }

        private static T ConvertToPacket<T>(ExpandoObject data) where T : Packet, new() {
            var packet = new T();
            packet.LoadData(data);
            return packet;
        }

        private static List<T> ConvertToPacketList<T>(List<object> data) where T : Packet, new() {
            var packets = new List<T>();

            foreach (var element in data) {
                var packet = new T();
                packet.LoadData((ExpandoObject)element);
                packets.Add(packet);
            }

            return packets;
        }

    }
}