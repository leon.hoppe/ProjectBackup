﻿using System.Dynamic;
using Framework.Shared.Abstraction;

namespace Framework.Shared.Models {
    public sealed class PlayerData : Packet {
        public string Owner { get; set; }
        public string CharId { get; set; }
        public bool Sex { get; set; }
        public int Height { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Birth { get; set; }
        public uint Skin { get; set; }
        public float[] LastPosition { get; set; }

        public override void LoadData(ExpandoObject data) {
            Owner = Convert<string>(data, nameof(Owner));
            CharId = Convert<string>(data, nameof(CharId));
            Sex = Convert<bool>(data, nameof(Sex));
            Height = Convert<int>(data, nameof(Height));
            FirstName = Convert<string>(data, nameof(FirstName));
            LastName = Convert<string>(data, nameof(LastName));
            Birth = Convert<string>(data, nameof(Birth));
            Skin = Convert<uint>(data, nameof(Skin));
            LastPosition = ConvertArray<float>(data, nameof(LastPosition));
        }
    }
}