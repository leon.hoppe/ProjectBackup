﻿namespace Framework.Shared.Extensions {
    public static class ArrayExtensions {
        public static string Serialize(this float[] vector) => $"[{string.Join(", ", vector)}]";
    }
}