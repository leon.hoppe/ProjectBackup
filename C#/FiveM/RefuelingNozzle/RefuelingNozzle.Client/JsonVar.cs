﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.JsonVar
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

namespace RefuelingNozzle {
    internal class JsonVar {
        public string name;
        public string var;
        public string json = "";

        public JsonVar() { }

        public JsonVar(string name, object var) => this.json = "\"" + name + "\":\"" + var.ToString() + "\"";

        public static string GenerateJsonString(params JsonVar[] vars) {
            string str = "{";
            foreach (JsonVar var in vars)
                str = str + var.json + ",";
            if (vars.Length != 0)
                str = str.Remove(str.Length - 1, 1);
            return str + "}";
        }
    }
}