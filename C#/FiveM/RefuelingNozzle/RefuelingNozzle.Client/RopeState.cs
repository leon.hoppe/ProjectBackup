﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.RopeState
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

namespace RefuelingNozzle {
    internal enum RopeState {
        Attached,
        Detached,
    }
}