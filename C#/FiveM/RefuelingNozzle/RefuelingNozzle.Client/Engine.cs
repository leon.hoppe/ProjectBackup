﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.Engine
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RefuelingNozzle {
    internal class Engine : BaseScript {
        private SectionScript config = new SectionScript("settings.ini", true);
        public int CurrentHandPumpId;
        public int TempPumpId;
        public int CurrentHandPumpNetId;
        public Vector3 CurrentPumpPosition = Vector3.Zero;
        public Vector3 CurrentPumpTankPosition = Vector3.Zero;
        private Dictionary<int, ACable> PlayerPumps = new Dictionary<int, ACable>();
        private Dictionary<int, int> PlayerParticles = new Dictionary<int, int>();
        private Dictionary<int, float> TankModels = new Dictionary<int, float>();

        private Dictionary<int, Tuple<Vector3, Vector3>>
            FuelCapOffsets = new Dictionary<int, Tuple<Vector3, Vector3>>();

        public static float RefuelingRopeLength = 3.5f;
        public static float RefuelingRopeMaxLength = 7f;
        public static bool RefuelExplodeAfterRopeBreak = false;
        public string PurchaseJerryCan = "[~g~G~w~] Purchase Jery Can";
        public string RefillJerryCan = "[~g~G~w~] Refill Jery Can";
        public string DismountFuelPump = "[~r~E~w~] Dismount Fuel Pump";
        public string DropNozzle = "Press ~r~E~w~ To Drop The Nozzle";
        public string MountFuelPump = "[~r~E~w~] Mount Fuel Pump";
        public string PlaceFuelPump = "[~r~E~w~] Place Fuel Pump";
        public string PickupFuelPump = "[~g~E~w~] Pickup Fuel Pump";
        public string UseJerryCan = "[~g~E~w~] Use Jerry Can";
        public string JerryCanEmpty = "~r~Jerry Can Empty";
        public string VehicleTankFull = "~g~Tank Full";
        public int InteractionKey = 38;
        public int InteractionKey2 = 47;
        public int JerryCanCost = 100;
        public int JerryCanRefillCost = 50;
        public bool isSpraying;
        public bool UnarmPedWhenHoldingNozzle = true;
        public int WaitTimeAfterRefuel = 3000;
        public bool CanUseNozzleDetached;
        public int CurrentServerID;
        public Tuple<Prop, float> CurrentPumpData;

        public Engine() {
            this.InteractionKey = this.config.GetValue<int>("[Info]", nameof(InteractionKey), 38);
            this.InteractionKey2 = this.config.GetValue<int>("[Info]", nameof(InteractionKey2), 47);
            this.JerryCanCost = this.config.GetValue<int>("[Info]", nameof(JerryCanCost), 100);
            this.JerryCanRefillCost = this.config.GetValue<int>("[Info]", nameof(JerryCanRefillCost), 50);
            this.UnarmPedWhenHoldingNozzle =
                this.config.GetValue<bool>("[Info]", nameof(UnarmPedWhenHoldingNozzle), true);
            this.WaitTimeAfterRefuel = this.config.GetValue<int>("[Info]", nameof(WaitTimeAfterRefuel), 500);
            this.CanUseNozzleDetached = this.config.GetValue<bool>("[Info]", nameof(CanUseNozzleDetached), false);
            this.DismountFuelPump =
                this.config.GetValue<string>("[Locales]", nameof(DismountFuelPump), "[~r~E~w~] Dismount Fuel Pump");
            this.DropNozzle =
                this.config.GetValue<string>("[Locales]", nameof(DropNozzle), "Press ~r~E~w~ To Drop The Nozzle");
            this.MountFuelPump =
                this.config.GetValue<string>("[Locales]", nameof(MountFuelPump), "[~g~E~w~] Mount Fuel Pump");
            this.PlaceFuelPump =
                this.config.GetValue<string>("[Locales]", nameof(PlaceFuelPump), "[~r~E~w~] Place Fuel Pump");
            this.PickupFuelPump =
                this.config.GetValue<string>("[Locales]", nameof(PickupFuelPump), "[~g~E~w~] Pickup Fuel Pump");
            this.PurchaseJerryCan =
                this.config.GetValue<string>("[Locales]", nameof(PurchaseJerryCan), "[~r~G~w~] Purchase Jerry Can");
            this.RefillJerryCan =
                this.config.GetValue<string>("[Locales]", nameof(RefillJerryCan), "[~r~G~w~] Refill Jerry Can For");
            this.UseJerryCan =
                this.config.GetValue<string>("[Locales]", nameof(UseJerryCan), "[~g~E~w~] Use Jerry Can");
            this.JerryCanEmpty = this.config.GetValue<string>("[Locales]", nameof(JerryCanEmpty), "~r~Jerry Can Empty");
            this.VehicleTankFull = this.config.GetValue<string>("[Locales]", nameof(VehicleTankFull), "~g~Tank Full");
            Engine.RefuelingRopeLength = this.config.GetValue<float>("[FuelTank]", "RopeLength", 3.5f);
            Engine.RefuelingRopeMaxLength = this.config.GetValue<float>("[FuelTank]", "RopeMaxLength", 7f);
            Engine.RefuelExplodeAfterRopeBreak =
                this.config.GetValue<bool>("[FuelTank]", "ExplodeAfterRopeBreak", false);
            foreach (Tuple<string, Dictionary<string, string>> allSection in this.config.GetAllSections()) {
                if (allSection.Item1 == "[FuelTankModels]") {
                    foreach (KeyValuePair<string, string> keyValuePair in allSection.Item2)
                        this.TankModels[API.GetHashKey(keyValuePair.Key)] = float.Parse(keyValuePair.Value);
                }

                if (allSection.Item1 == "[FuelCapOffsets]") {
                    foreach (KeyValuePair<string, string> keyValuePair in allSection.Item2) {
                        int hashKey = API.GetHashKey(keyValuePair.Key);
                        string[] strArray = keyValuePair.Value.Split(',');
                        float result1;
                        float result2;
                        float result3;
                        float result4;
                        float result5;
                        float result6;
                        if (strArray.Length == 6 && float.TryParse(strArray[0], out result1) &&
                            float.TryParse(strArray[1], out result2) && float.TryParse(strArray[2], out result3) &&
                            float.TryParse(strArray[3], out result4) && float.TryParse(strArray[4], out result5) &&
                            float.TryParse(strArray[5], out result6))
                            this.FuelCapOffsets[hashKey] = new Tuple<Vector3, Vector3>(
                                new Vector3(result1, result2, result3), new Vector3(result4, result5, result6));
                    }
                }
            }

            this.EventHandlers["pump:attachCable"] += new Action<int, Vector3, int>(this.CreatePlayerPumpCable);
            this.EventHandlers["pump:detachCable"] += new Action<int>(this.DeletePlayerPumpCable);
            this.EventHandlers["pump:triggerSpray"] += new Action<int, bool, int>((id, trigger, pump) => {
                if (trigger)
                    this.StartPlayerParticles(id, pump);
                else
                    this.StopPlayerParticles(id);
            });
            this.CurrentServerID = API.GetPlayerServerId(API.PlayerId());
            this.config.Dispose();
            this.CurrentPumpData = this.FindNearestFuelPump();
            this.Tick += new Func<Task>(this.OnTick);
            this.Tick += new Func<Task>(this.UpdateRopes);
        }

        private async Task UpdateRopes() {
            foreach (KeyValuePair<int, ACable> keyValuePair1 in this.PlayerPumps.ToArray<KeyValuePair<int, ACable>>()) {
                keyValuePair1.Value.Update();
                KeyValuePair<int, ACable> keyValuePair2 = new KeyValuePair<int, ACable>();
            }

            Ped playerPed = Game.PlayerPed;
            if (this.CurrentHandPumpId == 0)
                this.CurrentPumpData = this.FindNearestFuelPump();
            await BaseScript.Delay(500);
        }

        private async Task OnTick() {
            Engine engine = this;
            Ped playerPed = Game.PlayerPed;
            Ped ped;
            if ((Entity)playerPed.CurrentVehicle == (Entity)null && playerPed.IsOnFoot) {
                if (engine.CurrentHandPumpId != 0) {
                    if (!API.DoesEntityExist(engine.CurrentHandPumpId)) {
                        BaseScript.TriggerEvent("fuel:stopRefuelFromPump");
                        engine.CurrentHandPumpId = 0;
                        engine.CurrentPumpPosition = Vector3.Zero;
                        ped = (Ped)null;
                    }
                    else {
                        if (API.IsEntityAttached(engine.CurrentHandPumpId)) {
                            if (playerPed.IsAlive) {
                                if (engine.CurrentPumpPosition != Vector3.Zero) {
                                    Vector3 entityCoords = API.GetEntityCoords(engine.CurrentHandPumpId, true);
                                    if ((double)(entityCoords - playerPed.Position).Length() < 1.0) {
                                        engine.DrawText3D(entityCoords.X, entityCoords.Y, entityCoords.Z,
                                            engine.DismountFuelPump);
                                        if (API.IsControlJustReleased(0, engine.InteractionKey)) {
                                            BaseScript.TriggerEvent("fuel:stopRefuelFromPump");
                                            engine.DetachHandPump();
                                            await BaseScript.Delay(engine.WaitTimeAfterRefuel);
                                        }
                                    }
                                    else
                                        await BaseScript.Delay(500);

                                    Vector3 vector3 = new Vector3();
                                }
                                else {
                                    if (engine.UnarmPedWhenHoldingNozzle) {
                                        playerPed.Weapons.Select(WeaponHash.Unarmed);
                                        API.DisableControlAction(0, 24, true);
                                        API.DisableControlAction(0, 25, true);
                                    }

                                    API.DisableControlAction(0, 23, true);
                                    Vector3 pumpTankPosition = engine.CurrentPumpTankPosition;
                                    Vector3 position = playerPed.Position;
                                    if (!engine.PlayerPumps.ContainsKey(engine.CurrentServerID)) {
                                        Screen.ShowSubtitle(engine.DropNozzle);
                                        if (API.IsControlJustReleased(0, engine.InteractionKey))
                                            engine.DropHandPump();
                                    }
                                    else {
                                        if (engine.CanUseNozzleDetached) {
                                            API.DisableControlAction(0, 24, true);
                                            if (API.IsDisabledControlJustPressed(0, 24)) {
                                                BaseScript.TriggerServerEvent("pump:triggerSpray", (object)true,
                                                    (object)engine.CurrentHandPumpNetId);
                                                engine.isSpraying = true;
                                            }
                                            else if (API.IsDisabledControlJustReleased(0, 24)) {
                                                BaseScript.TriggerServerEvent("pump:triggerSpray", (object)false,
                                                    (object)0);
                                                engine.isSpraying = false;
                                            }
                                        }

                                        Tuple<Vehicle, Vector3, bool> nearestFuelCap = engine.GetNearestFuelCap();
                                        Vector3 vector3 = pumpTankPosition - position;
                                        if ((double)vector3.Length() < 2.0) {
                                            if ((Entity)nearestFuelCap.Item1 != (Entity)null) {
                                                float vehicleFuelLevel =
                                                    API.GetVehicleFuelLevel(nearestFuelCap.Item1.Handle);
                                                vector3 = nearestFuelCap.Item2 - position;
                                                double num1 = (double)vector3.Length();
                                                vector3 = pumpTankPosition - position;
                                                double num2 = (double)vector3.Length();
                                                if (num1 < num2) {
                                                    if ((double)vehicleFuelLevel >= 100.0) {
                                                        engine.DrawText3D(nearestFuelCap.Item2.X,
                                                            nearestFuelCap.Item2.Y, nearestFuelCap.Item2.Z,
                                                            engine.VehicleTankFull);
                                                    }
                                                    else {
                                                        engine.DrawText3D(nearestFuelCap.Item2.X,
                                                            nearestFuelCap.Item2.Y, nearestFuelCap.Item2.Z,
                                                            engine.MountFuelPump);
                                                        if (API.IsControlJustReleased(0, engine.InteractionKey)) {
                                                            if (engine.isSpraying)
                                                                BaseScript.TriggerServerEvent("pump:triggerSpray",
                                                                    (object)false, (object)0);
                                                            engine.AttachHandPump(nearestFuelCap.Item1,
                                                                nearestFuelCap.Item2, nearestFuelCap.Item3);
                                                            BaseScript.TriggerEvent("fuel:refuelFromPump",
                                                                (object)playerPed.Handle,
                                                                (object)nearestFuelCap.Item1.Handle);
                                                        }
                                                    }
                                                }
                                                else {
                                                    engine.DrawText3D(pumpTankPosition.X, pumpTankPosition.Y,
                                                        pumpTankPosition.Z + 1.2f, engine.PlaceFuelPump);
                                                    if (API.IsControlJustReleased(0, engine.InteractionKey)) {
                                                        if (engine.isSpraying)
                                                            BaseScript.TriggerServerEvent("pump:triggerSpray",
                                                                (object)false, (object)0);
                                                        engine.DeleteHandPump();
                                                    }
                                                }
                                            }
                                            else {
                                                engine.DrawText3D(pumpTankPosition.X, pumpTankPosition.Y,
                                                    pumpTankPosition.Z + 1.2f, engine.PlaceFuelPump);
                                                if (API.IsControlJustReleased(0, engine.InteractionKey)) {
                                                    if (engine.isSpraying)
                                                        BaseScript.TriggerServerEvent("pump:triggerSpray",
                                                            (object)false, (object)0);
                                                    engine.DeleteHandPump();
                                                }
                                            }
                                        }
                                        else if ((Entity)nearestFuelCap.Item1 != (Entity)null) {
                                            if ((double)API.GetVehicleFuelLevel(nearestFuelCap.Item1.Handle) >= 100.0) {
                                                engine.DrawText3D(nearestFuelCap.Item2.X, nearestFuelCap.Item2.Y,
                                                    nearestFuelCap.Item2.Z, engine.VehicleTankFull);
                                            }
                                            else {
                                                engine.DrawText3D(nearestFuelCap.Item2.X, nearestFuelCap.Item2.Y,
                                                    nearestFuelCap.Item2.Z, engine.MountFuelPump);
                                                if (API.IsControlJustReleased(0, engine.InteractionKey)) {
                                                    if (engine.isSpraying)
                                                        BaseScript.TriggerServerEvent("pump:triggerSpray",
                                                            (object)false, (object)0);
                                                    engine.AttachHandPump(nearestFuelCap.Item1, nearestFuelCap.Item2,
                                                        nearestFuelCap.Item3);
                                                    BaseScript.TriggerEvent("fuel:refuelFromPump",
                                                        (object)playerPed.Handle, (object)nearestFuelCap.Item1.Handle);
                                                }
                                            }
                                        }
                                    }

                                    Vector3 vector3_1 = new Vector3();
                                    Vector3 vector3_2 = new Vector3();
                                }
                            }
                            else {
                                if (engine.isSpraying)
                                    BaseScript.TriggerServerEvent("pump:triggerSpray", (object)false, (object)0);
                                engine.DeleteHandPump();
                            }
                        }
                        else {
                            BaseScript.TriggerEvent("fuel:stopRefuelFromPump");
                            engine.DeleteHandPump();
                        }

                        ped = (Ped)null;
                    }
                }
                else {
                    if (playerPed.IsAlive) {
                        if (API.GetSelectedPedWeapon(playerPed.Handle) == 883325847) {
                            Tuple<Vehicle, Vector3, bool> nearestFuelCap = engine.GetNearestFuelCap();
                            if ((Entity)nearestFuelCap.Item1 != (Entity)null) {
                                if ((double)API.GetVehicleFuelLevel(nearestFuelCap.Item1.Handle) >= 100.0)
                                    engine.DrawText3D(nearestFuelCap.Item2.X, nearestFuelCap.Item2.Y,
                                        nearestFuelCap.Item2.Z, engine.VehicleTankFull);
                                else if (API.GetAmmoInPedWeapon(playerPed.Handle, 883325847U) > 20) {
                                    engine.DrawText3D(nearestFuelCap.Item2.X, nearestFuelCap.Item2.Y,
                                        nearestFuelCap.Item2.Z, engine.UseJerryCan);
                                    if (API.IsControlJustReleased(0, engine.InteractionKey))
                                        BaseScript.TriggerEvent("fuel:refuelFromJerryCan", (object)playerPed.Handle,
                                            (object)nearestFuelCap.Item1.Handle);
                                }
                                else
                                    engine.DrawText3D(nearestFuelCap.Item2.X, nearestFuelCap.Item2.Y,
                                        nearestFuelCap.Item2.Z, engine.JerryCanEmpty);
                            }
                            else
                                await BaseScript.Delay(500);
                        }
                        else if (engine.CurrentPumpData.Item1.Handle != 0) {
                            Vector3 position = engine.CurrentPumpData.Item1.Position;
                            if (API.HasPedGotWeapon(playerPed.Handle, 883325847U, false)) {
                                double num = Math.Round((double)engine.JerryCanRefillCost *
                                                        (1.0 - (double)API.GetAmmoInPedWeapon(playerPed.Handle,
                                                            883325847U) / 4500.0));
                                engine.DrawText3D(position.X, position.Y, position.Z + 1.2f,
                                    engine.PickupFuelPump + "\n" + engine.RefillJerryCan + " ~g~$" + num.ToString());
                            }
                            else
                                engine.DrawText3D(position.X, position.Y, position.Z + 1.2f,
                                    engine.PickupFuelPump + "\n" + engine.PurchaseJerryCan + " ~g~$" +
                                    engine.JerryCanCost.ToString());

                            if (API.IsControlJustReleased(0, engine.InteractionKey)) {
                                engine.CreateHandPump(engine.CurrentPumpData);
                                engine.CurrentPumpTankPosition = position;
                            }

                            if (API.IsControlJustReleased(0, engine.InteractionKey2))
                                BaseScript.TriggerEvent("fuel:requestJerryCanPurchase");
                            Vector3 vector3 = new Vector3();
                        }
                        else
                            await BaseScript.Delay(500);
                    }
                    else
                        await BaseScript.Delay(500);

                    ped = (Ped)null;
                }
            }
            else
                await BaseScript.Delay(500);
        }

        public async void StartPlayerParticles(int id, int pumpID) {
            this.StopPlayerParticles(id);
            if (pumpID == 0 || API.GetPlayerFromServerId(id) == -1)
                return;
            int objID = API.NetToObj(pumpID);
            int timeout = 0;
            while (objID == 0) {
                timeout += 100;
                objID = API.NetToObj(pumpID);
                await BaseScript.Delay(100);
                if (timeout > 1000)
                    break;
            }

            if (objID == 0)
                return;
            string dic = "core";
            string part = "veh_petrol_leak";
            while (!API.HasNamedPtfxAssetLoaded(dic)) {
                API.RequestNamedPtfxAsset(dic);
                await BaseScript.Delay(1);
            }

            API.UseParticleFxAssetNextCall(dic);
            int num = API.StartParticleFxLoopedOnEntity(part, objID, 0.0f, 0.23f, 0.25f, 0.0f, 0.0f, -70f, 1f, true,
                true, true);
            this.PlayerParticles.Add(id, num);
            dic = (string)null;
            part = (string)null;
            dic = (string)null;
            part = (string)null;
        }

        public void StopPlayerParticles(int player) {
            if (!this.PlayerParticles.ContainsKey(player))
                return;
            API.StopParticleFxLooped(this.PlayerParticles[player], true);
            this.PlayerParticles.Remove(player);
        }

        public Tuple<Vehicle, Vector3, bool> GetNearestFuelCap() {
            Tuple<Vehicle, Vector3, bool> nearestFuelCap =
                new Tuple<Vehicle, Vector3, bool>((Vehicle)null, new Vector3(), false);
            Vehicle vehicle = (Vehicle)null;
            float num1 = 1000f;
            Vector3 position = Game.PlayerPed.Position;
            foreach (Vehicle allVehicle in World.GetAllVehicles()) {
                float num2 = (allVehicle.Position - position).Length();
                if ((double)num2 < 5.0 && API.NetworkHasControlOfEntity(allVehicle.Handle) &&
                    (double)num2 < (double)num1) {
                    num1 = num2;
                    vehicle = allVehicle;
                }
            }

            if (!((Entity)vehicle != (Entity)null))
                return nearestFuelCap;
            Tuple<Vector3, Vector3> vehicleRefuelPositions = this.GetVehicleRefuelPositions(vehicle);
            float num3 = (position - vehicleRefuelPositions.Item1).Length();
            Vector3 vector3 = position - vehicleRefuelPositions.Item2;
            float num4 = vector3.Length();
            if ((double)num3 >= (double)num4) {
                if ((double)num4 >= (double)num3) {
                    if ((double)num3 < 1.0)
                        return new Tuple<Vehicle, Vector3, bool>(vehicle, vehicleRefuelPositions.Item1, false);
                    vector3 = new Vector3();
                    return new Tuple<Vehicle, Vector3, bool>((Vehicle)null, vector3, false);
                }

                if ((double)num4 < 1.0)
                    return new Tuple<Vehicle, Vector3, bool>(vehicle, vehicleRefuelPositions.Item2, true);
                vector3 = new Vector3();
                return new Tuple<Vehicle, Vector3, bool>((Vehicle)null, vector3, false);
            }

            if ((double)num3 < 1.0)
                return new Tuple<Vehicle, Vector3, bool>(vehicle, vehicleRefuelPositions.Item1, false);
            vector3 = new Vector3();
            return new Tuple<Vehicle, Vector3, bool>((Vehicle)null, vector3, false);
        }

        public Tuple<Vector3, Vector3> GetVehicleRefuelPositions(Vehicle vehicle) {
            int hash = vehicle.Model.Hash;
            if (this.FuelCapOffsets.ContainsKey(hash)) {
                Vector3 offset1 = this.FuelCapOffsets[hash].Item1;
                Vector3 offset2 = this.FuelCapOffsets[hash].Item2;
                return new Tuple<Vector3, Vector3>(vehicle.GetOffsetPosition(offset1),
                    vehicle.GetOffsetPosition(offset2));
            }

            int entityBoneIndexByName1 = API.GetEntityBoneIndexByName(vehicle.Handle, "petroltank");
            if (entityBoneIndexByName1 != -1) {
                Vector3 position = vehicle.Bones[entityBoneIndexByName1].Position;
                Vector3 infrontOfPosition1 = this.GetPointInfrontOfPosition(
                    this.GetPointInfrontOfPosition(position, -vehicle.RightVector, 0.2f), -vehicle.ForwardVector,
                    -0.05f);
                Vector3 vector3_1 =
                    new Vector3(infrontOfPosition1.X, infrontOfPosition1.Y, infrontOfPosition1.Z + 0.15f);
                Vector3 infrontOfPosition2 = this.GetPointInfrontOfPosition(
                    this.GetPointInfrontOfPosition(position, vehicle.RightVector, 0.2f), -vehicle.ForwardVector,
                    -0.05f);
                Vector3 vector3_2 =
                    new Vector3(infrontOfPosition2.X, infrontOfPosition2.Y, infrontOfPosition2.Z + 0.15f);
                return new Tuple<Vector3, Vector3>(vector3_1, vector3_2);
            }

            int entityBoneIndexByName2 = API.GetEntityBoneIndexByName(vehicle.Handle, "wheel_lr");
            int entityBoneIndexByName3 = API.GetEntityBoneIndexByName(vehicle.Handle, "wheel_rr");
            Vector3 position1 = vehicle.Bones[entityBoneIndexByName2].Position;
            Vector3 position2 = vehicle.Bones[entityBoneIndexByName3].Position;
            Vector3 infrontOfPosition3 = this.GetPointInfrontOfPosition(
                this.GetPointInfrontOfPosition(position1, -vehicle.RightVector, 0.25f), -vehicle.ForwardVector, 0.2f);
            Vector3 vector3_3 = new Vector3(infrontOfPosition3.X, infrontOfPosition3.Y, infrontOfPosition3.Z + 0.6f);
            Vector3 infrontOfPosition4 = this.GetPointInfrontOfPosition(
                this.GetPointInfrontOfPosition(position2, vehicle.RightVector, 0.25f), -vehicle.ForwardVector, 0.2f);
            Vector3 vector3_4 = new Vector3(infrontOfPosition4.X, infrontOfPosition4.Y, infrontOfPosition4.Z + 0.6f);
            return new Tuple<Vector3, Vector3>(vector3_3, vector3_4);
        }

        public Tuple<Prop, float> FindNearestFuelPump() {
            Tuple<Prop, float> nearestFuelPump = new Tuple<Prop, float>(new Prop(0), 2f);
            Vector3 position = Game.PlayerPed.Position;
            foreach (Prop allProp in World.GetAllProps()) {
                if (this.TankModels.ContainsKey(API.GetEntityModel(allProp.Handle)) && allProp.IsAlive) {
                    float num = (position - allProp.Position).Length();
                    if ((double)num < (double)nearestFuelPump.Item2)
                        nearestFuelPump = new Tuple<Prop, float>(allProp, num);
                }
            }

            return nearestFuelPump;
        }

        public async void CreatePlayerPumpCable(int id, Vector3 position, int pumpID) {
            this.DeletePlayerPumpCable(id);
            if (pumpID == 0)
                ;
            else {
                int ped = API.GetPlayerFromServerId(id);
                if (ped == -1)
                    ;
                else {
                    int objID = API.NetToObj(pumpID);
                    int timeout = 0;
                    while (objID == 0) {
                        timeout += 100;
                        objID = API.NetToObj(pumpID);
                        await BaseScript.Delay(100);
                        if (timeout > 1000)
                            break;
                    }

                    if (objID == 0)
                        ;
                    else {
                        ACable acable = new ACable(position, Engine.RefuelingRopeLength, Engine.RefuelingRopeMaxLength,
                            Engine.RefuelExplodeAfterRopeBreak);
                        acable.Target = new Prop(objID);
                        acable.Create();
                        acable.OnCableDetach += (Action)(() => {
                            if (ped == API.PlayerId())
                                BaseScript.TriggerEvent("fuel:stopRefuelFromPump");
                            if (this.isSpraying)
                                BaseScript.TriggerServerEvent("pump:triggerSpray", (object)false, (object)0);
                            this.DeletePlayerPumpCable(id);
                        });
                        this.PlayerPumps.Add(id, acable);
                        if (ped == API.PlayerId()) {
                            this.CurrentHandPumpId = objID;
                            int handle = Game.PlayerPed.Handle;
                            API.AttachEntityToEntity(this.CurrentHandPumpId, handle, API.GetPedBoneIndex(handle, 60309),
                                0.055f, 0.05f, 0.0f, -50f, -90f, -50f, true, true, false, true, 0, true);
                        }
                    }
                }
            }
        }

        public void DeletePlayerPumpCable(int id) {
            if (!this.PlayerPumps.ContainsKey(id))
                return;
            ACable playerPump = this.PlayerPumps[id];
            playerPump.Target = (Prop)null;
            playerPump.DetachTarget();
            playerPump.DeleteRopes();
            this.PlayerPumps.Remove(id);
            if (API.GetPlayerFromServerId(id) != API.PlayerId())
                return;
            API.DetachEntity(this.TempPumpId, true, true);
            API.DeleteEntity(ref this.TempPumpId);
        }

        public void AttachHandPump(Vehicle veh, Vector3 position, bool toRight) {
            if (this.CurrentHandPumpId == 0)
                return;
            this.PlaySound("mount");
            API.DetachEntity(this.CurrentHandPumpId, true, true);
            this.CurrentPumpPosition = position;
            Vector3 positionOffset = veh.GetPositionOffset(position);
            int entityBoneIndexByName = API.GetEntityBoneIndexByName(veh.Handle, "petroltank");
            float xRot = -45f;
            if (entityBoneIndexByName != -1)
                xRot = -50f;
            if (toRight)
                API.AttachEntityToEntity(this.CurrentHandPumpId, veh.Handle, -1, positionOffset.X, positionOffset.Y,
                    positionOffset.Z, xRot, 0.0f, 90f, true, true, false, false, 0, true);
            else
                API.AttachEntityToEntity(this.CurrentHandPumpId, veh.Handle, -1, positionOffset.X, positionOffset.Y,
                    positionOffset.Z, xRot, 0.0f, -90f, true, true, false, false, 0, true);
        }

        public void DetachHandPump() {
            if (this.CurrentHandPumpId == 0 || !(this.CurrentPumpPosition != Vector3.Zero))
                return;
            this.PlaySound("unmount");
            API.DetachEntity(this.CurrentHandPumpId, true, true);
            this.CurrentPumpPosition = Vector3.Zero;
            Ped playerPed = Game.PlayerPed;
            API.AttachEntityToEntity(this.CurrentHandPumpId, playerPed.Handle,
                API.GetPedBoneIndex(playerPed.Handle, 60309), 0.055f, 0.05f, 0.0f, -50f, -90f, -50f, true, true, false,
                true, 0, true);
        }

        public void DropHandPump() {
            if (this.CurrentHandPumpId == 0)
                return;
            API.DetachEntity(this.CurrentHandPumpId, true, true);
            API.MarkObjectForDeletion(this.CurrentHandPumpId);
            this.CurrentPumpPosition = Vector3.Zero;
            this.CurrentHandPumpId = 0;
        }

        public void DeleteHandPump() {
            if (this.CurrentHandPumpId == 0)
                return;
            this.TempPumpId = this.CurrentHandPumpId;
            this.CurrentHandPumpId = 0;
            this.CurrentPumpPosition = Vector3.Zero;
            BaseScript.TriggerServerEvent("pump:detachCable");
            this.PlaySound("place");
        }

        public async void CreateHandPump(Tuple<Prop, float> pumpData) {
            Prop prop = pumpData.Item1;
            Vector3 position = prop.Position;
            Vector3 pumpPos = new Vector3(position.X, position.Y, position.Z + 2.3f);
            int hash = prop.Model.Hash;
            if (this.TankModels.ContainsKey(hash))
                pumpPos = new Vector3(position.X, position.Y, position.Z + this.TankModels[hash]);
            this.PlaySound("pickup");
            this.DeleteHandPump();
            int model = API.GetHashKey("prop_cs_fuel_nozle");
            while (!API.HasModelLoaded((uint)model)) {
                API.RequestModel((uint)model);
                await BaseScript.Delay(1);
            }

            Vector3 entityInWorldCoords = API.GetOffsetFromEntityInWorldCoords(Game.PlayerPed.Handle, 0.0f, 0.0f, -5f);
            this.CurrentPumpPosition = Vector3.Zero;
            this.CurrentHandPumpNetId = API.ObjToNet(API.CreateObject(model, entityInWorldCoords.X,
                entityInWorldCoords.Y, entityInWorldCoords.Z, true, true, false));
            API.SetNetworkIdExistsOnAllMachines(this.CurrentHandPumpNetId, true);
            API.NetworkSetNetworkIdDynamic(this.CurrentHandPumpNetId, true);
            API.SetNetworkIdCanMigrate(this.CurrentHandPumpNetId, false);
            BaseScript.TriggerServerEvent("pump:attachCable", (object)pumpPos, (object)this.CurrentHandPumpNetId);
        }

        public void DrawText3D(float x, float y, float z, string text) {
            float screenX = 0.0f;
            float screenY = 0.0f;
            if (!API.World3dToScreen2d(x, y, z, ref screenX, ref screenY))
                return;
            API.SetTextScale(0.35f, 0.35f);
            API.SetTextFont(4);
            API.SetTextProportional(true);
            API.SetTextColour((int)byte.MaxValue, (int)byte.MaxValue, (int)byte.MaxValue, 215);
            API.SetTextEntry("STRING");
            API.SetTextCentre(true);
            API.AddTextComponentString(text);
            API.DrawText(screenX, screenY);
        }

        public Vector3 GetPointInfrontOfPosition(Vector3 position, Vector3 forward, float distance) => new Vector3(
            position.X + forward.X * distance, position.Y + forward.Y * distance, position.Z + forward.Z * distance);

        public void PlaySound(string sound) => API.SendNuiMessage(
            JsonVar.GenerateJsonString(new JsonVar("Type", (object)"playSound"), new JsonVar("File", (object)sound)));
    }
}