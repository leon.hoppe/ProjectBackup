﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.SectionScript
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RefuelingNozzle {
    internal class SectionScript : IDisposable {
        public char SPLIT_CHAR = ' ';
        public string SECTION_START = "{";
        public string SECTION_END = "}";
        public string COMMENT_PREFIX = "//";
        public int LINES_BETWEEN_SECTIONS = 1;
        public string FileName = "";

        private List<Tuple<string, Dictionary<string, string>>> LoadedData =
            new List<Tuple<string, Dictionary<string, string>>>();

        private List<string> AllSections = new List<string>();

        public SectionScript() { }

        public SectionScript(string FileName, bool load = false) {
            this.FileName = FileName;
            if (!load)
                return;
            this.Read();
        }

        public string this[string section, string key, string result = ""] =>
            this.GetValue<string>(section, key, result);

        public T GetValue<T>(string section, string key, T result) {
            if (!this.SectionExists(section))
                return result;
            string data = this.GetData(section, key, result.ToString());
            if (data is T data1)
                return data1;
            try {
                return (T)Convert.ChangeType((object)data, typeof(T));
            }
            catch (InvalidCastException ex) {
                return result;
            }
        }

        public string GetData(string section, string key, string result) {
            foreach (Tuple<string, Dictionary<string, string>> tuple in this.LoadedData) {
                if (tuple.Item1.Equals(section))
                    return tuple.Item2.ContainsKey(key) ? tuple.Item2[key] : result;
            }

            return result;
        }

        public bool SectionExists(string section) => this.AllSections.Contains(section);

        public List<Tuple<string, Dictionary<string, string>>> GetAllSections() =>
            this.LoadedData.ToList<Tuple<string, Dictionary<string, string>>>();

        public List<string> GetAllSectionNames() => this.AllSections.ToList<string>();

        public void Read() {
            this.LoadedData.Clear();
            this.AllSections.Clear();
            string str1 = "";
            Dictionary<string, string> source = new Dictionary<string, string>();
            string str2 = API.LoadResourceFile(API.GetCurrentResourceName(), this.FileName);
            char[] chArray = new char[1] { '\n' };
            foreach (string str3 in str2.Split(chArray)) {
                string text = str3.Replace("\r", "");
                if (!string.IsNullOrEmpty(text) && !string.IsNullOrWhiteSpace(text)) {
                    if (text.StartsWith(this.SECTION_START)) {
                        str1 = text.Substring(1);
                        if (text.EndsWith(this.SECTION_END) && !str1.Contains(this.SECTION_START)) {
                            str1 = str1.Remove(str1.Length - 1, 1);
                            this.LoadedData.Add(
                                new Tuple<string, Dictionary<string, string>>(str1, new Dictionary<string, string>()));
                            if (!this.AllSections.Contains(str1))
                                this.AllSections.Add(str1);
                            source.Clear();
                        }
                    }
                    else if (text.EndsWith(this.SECTION_END) || text.StartsWith(this.SECTION_END)) {
                        Dictionary<string, string> dictionary =
                            source.ToDictionary<KeyValuePair<string, string>, string, string>(
                                (Func<KeyValuePair<string, string>, string>)(entry => entry.Key),
                                (Func<KeyValuePair<string, string>, string>)(entry => entry.Value));
                        this.LoadedData.Add(new Tuple<string, Dictionary<string, string>>(str1, dictionary));
                        if (!this.AllSections.Contains(str1))
                            this.AllSections.Add(str1);
                        source.Clear();
                    }
                    else if (!text.StartsWith(this.COMMENT_PREFIX)) {
                        Tuple<string, string> data = this.ConvertToData(text);
                        if (!source.ContainsKey(data.Item1))
                            source.Add(data.Item1, data.Item2);
                    }
                }
            }
        }

        public Tuple<string, string> ConvertToData(string text) {
            int length = text.IndexOf(this.SPLIT_CHAR);
            if (length == -1)
                return new Tuple<string, string>(text, "");
            string str = "";
            if (length - 1 < text.Length)
                str = text.Substring(length + 1, text.Length - length - 1);
            return new Tuple<string, string>(text.Substring(0, length), str);
        }

        public void Dispose() {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposing)
                return;
            this.FileName = "";
            this.LoadedData.Clear();
            this.AllSections.Clear();
        }
    }
}