﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.ACable
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;

namespace RefuelingNozzle {
    internal class ACable {
        public RopeState State = RopeState.Detached;
        public Vector3 Position;
        public int RopeID;
        public int RopeType = 1;
        public float MinLength = 0.5f;
        public float MaxLength = 1000f;
        public float InitLength = 3.5f;
        public Prop Target;
        public bool IsTargetCurrentPlayer;
        public uint model = (uint)API.GetHashKey("bkr_prop_weed_plantpot_stack_01c");
        public Prop DummyProp;
        public float ConstantReachHeight = 225f;
        public Action OnCableDetach;
        public float MaxLengthBeforeBreak = 1000f;
        public bool ExplodeOnBreak;

        public ACable(Vector3 position, float length, float maxLength, bool explodeOnBreak) {
            this.Position = position;
            this.InitLength = length;
            this.MaxLengthBeforeBreak = maxLength;
            this.ExplodeOnBreak = explodeOnBreak;
        }

        public void PrepareRopes() {
            if (!API.RopeAreTexturesLoaded())
                API.RopeLoadTextures();
            API.DeleteRope(ref this.RopeID);
            int unkPtr = 0;
            this.RopeID = API.AddRope(this.Position.X, this.Position.Y, this.Position.Z, 0.0f, 0.0f, 0.0f,
                this.InitLength, this.RopeType, this.MaxLength, this.MinLength, 1f, false, false, false, 5f, false,
                ref unkPtr);
        }

        public void DeleteRopes() {
            API.DeleteRope(ref this.RopeID);
            this.RopeID = 0;
            this.State = RopeState.Detached;
            if (!((Entity)this.DummyProp != (Entity)null))
                return;
            this.DummyProp.Delete();
        }

        public void DetachTarget() {
            if (!((Entity)this.Target != (Entity)null))
                return;
            this.State = RopeState.Detached;
            API.DetachRopeFromEntity(this.RopeID, this.Target.Handle);
            this.Target = (Prop)null;
        }

        public async void Create() {
            if ((Entity)this.DummyProp == (Entity)null) {
                while (!API.HasModelLoaded(this.model)) {
                    API.RequestModel(this.model);
                    await BaseScript.Delay(1);
                }

                int num = API.CreateObject((int)this.model, this.Position.X, this.Position.Y, this.Position.Z, false,
                    false, true);
                if (num > 0) {
                    API.SetEntityRecordsCollisions(num, false);
                    API.SetEntityLoadCollisionFlag(num, false);
                    this.DummyProp = new Prop(num);
                    this.DummyProp.Rotation = new Vector3(180f, 0.0f, 0.0f);
                    this.DummyProp.IsCollisionEnabled = false;
                    this.DummyProp.IsVisible = false;
                    this.DummyProp.Position = this.Position;
                    this.DummyProp.IsPositionFrozen = true;
                }
            }

            if ((Entity)this.Target != (Entity)null) {
                if (this.RopeID <= 0)
                    this.PrepareRopes();
                if (this.State != RopeState.Detached)
                    return;
                Vector3 entityInWorldCoords =
                    API.GetOffsetFromEntityInWorldCoords(this.Target.Handle, 0.0f, -0.02f, -0.175f);
                API.AttachEntitiesToRope(this.RopeID, this.Target.Handle, this.DummyProp.Handle, entityInWorldCoords.X,
                    entityInWorldCoords.Y, entityInWorldCoords.Z, this.Position.X, this.Position.Y, this.Position.Z,
                    this.MaxLength, false, false, "", "");
                this.State = RopeState.Attached;
                Vector3 vector3 = new Vector3();
            }
            else {
                if (this.RopeID <= 0)
                    return;
                this.DeleteRopes();
            }
        }

        public void Update() {
            if (this.State == RopeState.Detached || !((Entity)this.Target != (Entity)null) ||
                (double)(this.Target.Position - this.Position).Length() <= (double)this.MaxLengthBeforeBreak)
                return;
            if (this.ExplodeOnBreak && this.Target.Exists())
                World.AddExplosion(this.Position, ExplosionType.GasTank, 1f, 1f);
            if (this.OnCableDetach == null)
                return;
            this.OnCableDetach();
        }
    }
}