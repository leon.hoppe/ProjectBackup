﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.FrameTimerEventArgs
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

namespace RefuelingNozzle {
    internal class FrameTimerEventArgs {
        public double Difference;
        public double ErrorRate;
        public double Interval;

        public FrameTimerEventArgs(double Difference, double ErrorRate, double Interval) {
            this.Difference = Difference;
            this.ErrorRate = ErrorRate;
            this.Interval = Interval;
        }

        public FrameTimerEventArgs() {
            this.Difference = 0.0;
            this.ErrorRate = 0.0;
            this.Interval = 0.0;
        }
    }
}