﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzle.FrameTimer
// Assembly: RefuelingNozzle.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78F50B7E-6755-4A9F-896E-A83F58106523
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzle.net.dll

using CitizenFX.Core.Native;
using System;

namespace RefuelingNozzle {
    internal class FrameTimer : IDisposable {
        public int CurrentTime = API.GetGameTimer();
        private int OldTime = API.GetGameTimer();
        public int Interval;
        public bool IsRunning;
        public bool disposed;

        public int TimeElapsed => this.GetTimeElapsed();

        public double TimeLeft => this.GetTimeLeft();

        public event EventHandler<FrameTimerEventArgs> Elapsed;

        public FrameTimer(int Interval, bool start = false) {
            this.Interval = Interval;
            this.IsRunning = start;
        }

        public void Start() {
            this.OldTime = API.GetGameTimer();
            this.CurrentTime = API.GetGameTimer();
            this.IsRunning = true;
        }

        public void Update() {
            if (!this.IsRunning)
                return;
            this.CurrentTime = API.GetGameTimer();
            int Interval = this.CurrentTime - this.OldTime;
            if (Interval > this.Interval) {
                double ErrorRate = (double)(Interval - this.Interval) * 100.0 / (double)this.Interval;
                if (this.Elapsed != null)
                    this.Elapsed((object)this,
                        new FrameTimerEventArgs((double)(Interval - this.Interval), ErrorRate, (double)Interval));
                this.OldTime = this.CurrentTime;
            }
            else {
                if (Interval != this.Interval)
                    return;
                if (this.Elapsed != null)
                    this.Elapsed((object)this, new FrameTimerEventArgs(0.0, 0.0, (double)this.Interval));
                this.OldTime = this.CurrentTime;
            }
        }

        public int GetTimeElapsed() {
            this.Update();
            if (!this.IsRunning)
                return -1;
            this.CurrentTime = API.GetGameTimer();
            return this.CurrentTime - this.OldTime;
        }

        public double GetTimeLeft() {
            this.Update();
            return !this.IsRunning ? -1.0 : (double)(this.Interval - this.TimeElapsed) / 1000.0;
        }

        public void Stop() => this.IsRunning = false;

        public void Renew() {
            this.IsRunning = false;
            this.OldTime = API.GetGameTimer();
            this.CurrentTime = API.GetGameTimer();
            this.IsRunning = true;
        }

        public void Dispose() {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing) {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            this.disposed = true;
        }
    }
}