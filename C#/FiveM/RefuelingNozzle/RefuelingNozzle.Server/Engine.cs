﻿// Decompiled with JetBrains decompiler
// Type: RefuelingNozzleServer.Engine
// Assembly: RefuelingNozzleServer.net, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 471D9C27-7F42-406E-B0DD-213B04126191
// Assembly location: D:\Programmierstuff\C#\FiveM\RefuelingNozzle\Librarys\RefuelingNozzleServer.net.dll

using System;
using CitizenFX.Core;

namespace RefuelingNozzleServer {
    internal class Engine : BaseScript {
        public Engine() {
            this.EventHandlers.Add("pump:attachCable",
                (Delegate)new Action<Player, Vector3, int>(this.PumpAttachCable));
            this.EventHandlers.Add("pump:detachCable", (Delegate)new Action<Player>(this.PumpDettachCable));
            this.EventHandlers.Add("pump:triggerSpray", (Delegate)new Action<Player, bool, int>(this.TriggerPump));
            Debug.WriteLine(
                "refuelingnozzle".ToUpper() + " ^1Has Authenticated ^2Successfully! ^0By ^1ToxicScripts! ^7");
        }

        public void TriggerPump([FromSource] Player player, bool trigger, int pump) =>
            TriggerClientEvent("pump:triggerSpray", (object)int.Parse(player.Handle), (object)trigger,
                (object)pump);

        public void PumpAttachCable([FromSource] Player player, Vector3 position, int pump) =>
            TriggerClientEvent("pump:attachCable", (object)int.Parse(player.Handle), (object)position,
                (object)pump);

        public void PumpDettachCable([FromSource] Player player) =>
            TriggerClientEvent("pump:detachCable", (object)int.Parse(player.Handle));

        [EventHandler("playerDropped")]
        private void OnPlayerDropped([FromSource] Player player, string reason) {
            TriggerClientEvent("pump:triggerSpray", (object)int.Parse(player.Handle), (object)false,
                (object)0);
            TriggerClientEvent("pump:detachCable", (object)int.Parse(player.Handle));
        }
    }
}