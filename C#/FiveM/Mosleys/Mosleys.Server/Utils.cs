﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using CitizenFX.Core;
using Mosleys.Shared.Models;
using Newtonsoft.Json;
using Nexd.ESX.Server;

namespace Mosleys.Server {
    public static class Utils {

        public static async Task<ExhibitVehicle[]> GetAllExhibits() {
            var vehicles = new List<ExhibitVehicle>();
            var data = await MySql.FetchAll("SELECT * FROM cardealer");

            try {
                foreach (var exhibit in data) {
                    exhibit.vehicle = JsonConvert.DeserializeObject<ExpandoObject>(exhibit.vehicle as string);
                    var json = JsonConvert.SerializeObject(exhibit);
                    vehicles.Add(JsonConvert.DeserializeObject<ExhibitVehicle>(json));
                }
            }
            catch (Exception e) {
                Debug.WriteLine(e.ToString());
            }

            return vehicles.ToArray();
        }

        public static async Task<ExhibitVehicle> GetExhibitBySlot(int slot) {
            var data = await MySql.FetchOne($"SELECT * FROM cardealer WHERE slot = {slot}");
            data.vehicle = JsonConvert.DeserializeObject<ExpandoObject>(data.vehicle as string);
            var json = JsonConvert.SerializeObject(data);
            return JsonConvert.DeserializeObject<ExhibitVehicle>(json);
        }
        
        public static async Task<ExhibitVehicle> GetExhibitByUuid(string uuid) {
            var data = await MySql.FetchOne($"SELECT * FROM cardealer WHERE uuid = '{uuid}'");
            data.vehicle = JsonConvert.DeserializeObject<ExpandoObject>(data.vehicle as string);
            var json = JsonConvert.SerializeObject(data);
            return JsonConvert.DeserializeObject<ExhibitVehicle>(json);
        }

        public static async Task<int> FindFreeSlot() {
            var vehicles = await GetAllExhibits();

            int slot = 1;
            while (vehicles.Any(veh => veh.Slot == slot)) {
                slot++;
            }

            return slot;
        }

        public static async Task SaveExhibit(ExhibitVehicle exhibit) {
            if (string.IsNullOrEmpty(exhibit.Description))
                exhibit.Description = "Nicht angegeben";
            
            await MySql.Execute($"DELETE FROM cardealer WHERE uuid = '{exhibit.Uuid}'");
            await MySql.Execute("INSERT INTO cardealer VALUES (@uuid, @owner, @description, @price, @slot, @vehicle, @testdrive)", new Dictionary<string, object>() {
                {"@uuid", exhibit.Uuid},
                {"@owner", exhibit.Owner},
                {"@description", exhibit.Description},
                {"@price", exhibit.Price},
                {"@slot", exhibit.Slot},
                {"@vehicle", JsonConvert.SerializeObject(exhibit.Vehicle)},
                {"@testdrive", Convert.ToInt32(exhibit.TestDrive)}
            });
        }

        public static void UpdatePlayers(int slot) {
            BaseScript.TriggerClientEvent("mosleys:client:updateSlot", slot);
        }

        public static async void PaySeller(string identifier, int amount) {
            try {
                var seller = ESX.GetPlayerFromIdentifier(identifier);
                if (seller.IsOnline) seller.AddAccountMoney("bank", (int)(amount * (1 - ServerScript.Config.SellBill)));
                else {
                    var data = await MySql.FetchOne($"SELECT accounts FROM users WHERE identifier = '{identifier}'");
                    var accounts = JsonConvert.DeserializeObject<ExpandoObject>(data.accounts);
                    accounts.bank += amount;
                    await MySql.Execute($"UPDATE users SET accounts = @accounts WHERE identifier = @identifier", new Dictionary<string, object>() {
                        {"@accounts", JsonConvert.SerializeObject(accounts)},
                        {"@identifier", identifier}
                    });
                }
                
                BaseScript.TriggerEvent("esx_addonaccount:getSharedAccount", $"society_{ServerScript.Config.TargetSociety}", new Action<dynamic>(society => {
                    society.addMoney((int)(amount * ServerScript.Config.SellBill));
                }));
            }
            catch (Exception e) {
                Debug.WriteLine(e.ToString());
            }
        }

    }
}