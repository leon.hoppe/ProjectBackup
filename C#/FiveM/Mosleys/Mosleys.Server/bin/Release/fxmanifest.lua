fx_version 'cerulean'
games { 'gta5' }

--	details
author 'Leon Hoppe'
description "Mosley's"
version '2.0'

shared_scripts {
    'Nexd.ESX.Shared.dll',
    'Mosleys.Shared.dll',
    'settings.ini'
}

client_scripts {
    'Newtonsoft.Json.dll',
    'Nexd.ESX.Client.dll',
    'Mosleys.Client.net.dll'
}

server_scripts {
    '@mysql-async/lib/MySQL.lua',
}
