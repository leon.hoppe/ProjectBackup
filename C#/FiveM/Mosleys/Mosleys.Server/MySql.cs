﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mosleys.Server {
    public static class MySql {

        public static dynamic MySqlObject;
        
        public static Task Execute(string qry, IDictionary<string, object> parameters = null) {
            var source = new TaskCompletionSource<dynamic>();
            MySqlObject.query(qry, parameters, new Action<dynamic>(_ => {
                source.TrySetResult(null);
            }));
            return source.Task;
        }

        public static Task<dynamic[]> FetchAll(string qry, IDictionary<string, object> parameters = null) {
            var source = new TaskCompletionSource<dynamic[]>();
            MySqlObject.query(qry, parameters, new Action<dynamic>(data => {
                source.TrySetResult((data as List<dynamic>).ToArray());
            }));
            return source.Task;
        }

        public static async Task<dynamic> FetchOne(string qry, IDictionary<string, object> parameters = null) {
            var result = await FetchAll(qry, parameters);
            if (result.Length == 0) return null;
            return result[0];
        }

        public static async void InitialSetup() {
            await Execute("CREATE TABLE IF NOT EXISTS cardealer (uuid VARCHAR(36) PRIMARY KEY, owner VARCHAR(46), description VARCHAR(60), price INT(7), slot INT(2), vehicle LONGTEXT, testdrive TINYINT(1))");
            await Execute("UPDATE cardealer SET testdrive = 0");
        }
        
    }
}