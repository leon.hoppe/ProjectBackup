﻿using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Mosleys.Client.Extensions;
using Mosleys.Shared.Models;
using Nexd.ESX.Client;

namespace Mosleys.Client.Models {
    public class Exhibit {
        private static void DrawText3D(in Vector3 pos, in string text, float scale = 1, in bool rotate = true) {
            OutputArgument screenX = new OutputArgument(), screenY = new OutputArgument();
            bool onScreen = Function.Call<bool>(Hash.GET_SCREEN_COORD_FROM_WORLD_COORD , pos.X, pos.Y, pos.Z, screenX, screenY);
            if (onScreen) {
                Vector3 cam = API.GetGameplayCamCoord();
                float dist = API.GetDistanceBetweenCoords(cam.X, cam.Y, cam.Z, pos.X, pos.Y, pos.Z, true);

                if (rotate)
                    scale = (((1 / dist) * 2) * (1 / API.GetGameplayCamFov()) * 100) * scale;
                
                API.SetTextColour(220, 220, 220, 255);
                API.SetTextScale(0.0f * scale, 0.40f * scale);
                API.SetTextFont(4);
                API.SetTextProportional(true);
                API.SetTextCentre(true);

                API.SetTextEntry("STRING");
                API.AddTextComponentString(text);
                API.EndTextCommandDisplayText(screenX.GetResult<float>(), screenY.GetResult<float>());
            }
        }
        private static string HandleIntColor(int number) {
            if (number <= 0) return "Nein";
            return "~g~" + number;
        }

        public bool Ready { get; set; }
        public bool Spawned { get; private set; }
        public string Uuid { get; set; }
        public string Owner { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Slot { get; set; }
        public bool TestDrive { get; set; }
        public VehicleProperties Properties { get; set; }
        public Vehicle Vehicle { get; set; }
        public Vector4 Location { get; set; }

        public async void Spawn() {
            if (Spawned) Despawn();
            Vehicle = await Mosleys.SpawnVehicle(Properties.model, Location, true);
            ESX.Game.SetVehicleProperties(Vehicle, Properties);
            
            Vehicle.IsInvincible = true;
            Vehicle.SetDoorLockStatus(LockStatus.Locked);
            Vehicle.Freeze(true);
            Vehicle.PlaceOnGround();
            BaseScript.TriggerServerEvent("ls:mainCheck", Properties.plate, Vehicle.Handle, true);
            Spawned = true;
            Ready = true;
        }

        public void Despawn() {
            Spawned = false;
            Vehicle?.Delete();
        }

        public async void Update() {
            Ready = false;
            var exhibit = await Utils.GetExhibitBySlot(Slot);
            FromExhibit(exhibit);
        }

        public void FromExhibit(ExhibitVehicle exhibit) {
            Ready = false;
            Uuid = exhibit.Uuid;
            Owner = exhibit.Owner;
            Description = exhibit.Description;
            Price = exhibit.Price;
            Slot = exhibit.Slot;
            Properties = new VehicleProperties(exhibit.Vehicle);
            Location = Mosleys.Config.CarSlots[Slot];
            TestDrive = exhibit.TestDrive;
            
            if (Spawned || TestDrive) Despawn();
            if (!TestDrive) Spawn();
            else Ready = true;
        }
        
        public void FromExhibit(ExhibitVehicle exhibit, int slot) {
            Ready = false;
            Uuid = exhibit.Uuid;
            Owner = exhibit.Owner;
            Description = exhibit.Description;
            Price = exhibit.Price;
            Slot = slot;
            Properties = new VehicleProperties(exhibit.Vehicle);
            Location = Mosleys.Config.CarSlots[Slot];
            TestDrive = exhibit.TestDrive;
            
            if (Spawned || TestDrive) Despawn();
            if (!TestDrive) Spawn();
            else Ready = true;
        }

        public void ShowText(bool keyText = true, float scale = 1.0f, float spacing = 0.1f) {
            if (!Ready) return;
            float x = Location.X;
            float y = Location.Y;
            float z = Location.Z;
            
            if (TestDrive)
                DrawText3D(new Vector3(x, y, z + 1.1f), "[~r~Dieses Auto ist gerade bei einer Probefahrt~s~]");
            else {
                string turbo = "~r~Nein";
                if (Properties.modTurbo >= 1) turbo = "~g~Ja";
                
                string line1 = "[~b~" + Vehicle.DisplayName + "~s~]";
                string line2 = "[Turbo : " + turbo + "~s~] [Motor : ~r~" + HandleIntColor(Properties.modEngine) + "~s~] [Getriebe : ~r~" + HandleIntColor(Properties.modTransmission) + "~s~]";
                string line3 = "[Federung : ~r~" + HandleIntColor(Properties.modSuspension) + "~s~] [Panzerung : ~r~" + HandleIntColor(Properties.modArmor) + "~s~] [Bremsen : ~r~" + HandleIntColor(Properties.modBrakes) + "~s~]";
                string line4 = "[Beschreibung : ~r~" + Description + "~s~]";

                if (keyText) {
                    string line5 = "Drücke [~r~E~s~] zum kaufen [$~r~" + Price + "~s~]";
                    const string line6 = "Drücke [~r~F~s~] zum probefahren [~r~90 sek~s~]";
                    DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 1)), line6, scale);
                    DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 2)), line5, scale);
                }
                else {
                    DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 2)), "[Preis : ~r~" + Price +  "~s~]", scale);
                }
                
                DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 3)), line4, scale);
                DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 4)), line3, scale);
                DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 5)), line2, scale);
                DrawText3D(new Vector3(x, y, z + 1.0f + (spacing * 6)), line1, scale);
            }
        }

        public async Task StartTestDrive(bool notifyServer = true) {
            var lastPos = Game.Player.Character.Position;
            
            if (notifyServer) BaseScript.TriggerServerEvent("mosleys:server:testDrive", Slot, true);
            var vehicle = await Mosleys.SpawnVehicle(Properties.model, Mosleys.Config.TestDriveLocation);
            ESX.Game.SetVehicleProperties(vehicle, Properties);
            BaseScript.TriggerEvent("craftix:refuelVehicle", vehicle.Handle);
            Game.Player.Character.SetIntoVehicle(vehicle, VehicleSeat.Driver);

            var startTime = Game.GameTime;
            var lastSecond = 0;
            while (Game.GameTime - startTime <= Mosleys.Config.TestDriveTime) {
                int elapsedTime = (int)((Game.GameTime - startTime) / 1000.0f);
                
                if (elapsedTime > lastSecond) {
                    lastSecond++;
                    Mosleys.DoCustomHudText($"Probefahrt endet in {Mosleys.Config.TestDriveTime / 1000 - elapsedTime} Sekunden!", 1100);
                }
                
                if (Game.IsControlJustReleased(0, Control.VehicleExit)) break;
                
                await BaseScript.Delay(0);
            }
            
            vehicle.Delete();
            Game.Player.Character.Position = lastPos;
            if (notifyServer) BaseScript.TriggerServerEvent("mosleys:server:testDrive", Slot, false);
        }

        public async void Buy() {
            if (!await Mosleys.DisplayConfirmationDialog("Möchtest du dieses Auto wirklich kaufen?")) return;

            var taken = await Mosleys.IsPlateTaken(Properties.plate);
            if (taken) {
                Properties.plate = await Mosleys.GeneratePlate();
                Notify.Warning("Das Kennzeichen des Fahrzeugs ist bereits angemeldet. Es wurde ein neues Kennzeichen generiert.");
            }

            var hasMoney = await Mosleys.ServerCallback<bool>("mosleys:server:checkMoney", Price);
            if (!hasMoney) {
                Notify.Error("Du hast nicht genügend Geld!");
                return;
            }

            var success = await Mosleys.ServerCallback<bool>("mosleys:server:buyVehicle", new BuyData {
                Uuid = Uuid,
                Plate = Properties.plate
            });
            if (!success) {
                Notify.Error("Dieses Auto steht nicht mehr zum verkauf!");
                return;
            }

            var vehicle = await Mosleys.SpawnVehicle(Properties.model, Mosleys.Config.SpawnLocation);
            ESX.Game.SetVehicleProperties(vehicle, Properties);
            vehicle.EngineHealth = 1000;
            vehicle.Repair();
            vehicle.DirtLevel = 0;
            BaseScript.TriggerEvent("craftix:refuelVehicle", vehicle.Handle);
            
            Game.Player.Character.SetIntoVehicle(vehicle, VehicleSeat.Driver);
        }
    }
}