﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using CitizenFX.Core;
using Mosleys.Shared.Models;

namespace Mosleys.Client {
    public static class Utils {

        public static async Task<ExhibitVehicle[]> GetAllExhibits() {
            dynamic data = await Mosleys.ServerCallback<dynamic>("mosleys:server:getExhibits");
            
            try {
                List<ExhibitVehicle> vehicles = new List<ExhibitVehicle>();

                foreach (var exhibit in data.vehicles) {
                    vehicles.Add(ExhibitVehicle.FromDynamic(exhibit));
                }

                return vehicles.ToArray();
            }
            catch (Exception e) {
                Debug.WriteLine(e.ToString());
            }

            return Array.Empty<ExhibitVehicle>();
        }
        
        public static async Task<ExhibitVehicle[]> GetPlayerExhibits() {
            dynamic data = await Mosleys.ServerCallback<dynamic>("mosleys:server:getPlayerExhibits");
            
            try {
                List<ExhibitVehicle> vehicles = new List<ExhibitVehicle>();

                foreach (var exhibit in data.vehicles) {
                    vehicles.Add(ExhibitVehicle.FromDynamic(exhibit));
                }

                return vehicles.ToArray();
            }
            catch (Exception e) {
                Debug.WriteLine(e.ToString());
            }

            return Array.Empty<ExhibitVehicle>();
        }

        public static async Task<ExhibitVehicle> GetExhibitBySlot(int slot) {
            dynamic data = await Mosleys.ServerCallback<dynamic>("mosleys:server:getExhibitBySlot", slot);
            return ExhibitVehicle.FromDynamic(data);
        }

        public static void PrintDynamic(dynamic data, string prefix = "") {
            foreach (var element in (data as IDictionary<string, object>)) {
                Debug.WriteLine($"{prefix}{element.Key}: {element.Value}");
                
                if (element.Value.GetType() == typeof(ExpandoObject)) {
                    PrintDynamic(element.Value, prefix + "\t");
                }

                if (element.Value.GetType() == typeof(List<object>)) {
                    Debug.WriteLine(prefix + "\t", string.Join(", ", element.Value));
                }
            }
        }

    }
}