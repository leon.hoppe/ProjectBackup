﻿using System;
using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using Mosleys.Client.Extensions;
using Mosleys.Client.Models;
using Nexd.ESX.Client;

namespace Mosleys.Client {
    public static class ExhibitHandler {
        public static bool ExhibitsSpawned = false;
        public static bool CatalogOpen = false;

        private static Exhibit[] _realExhibits;

        public static void OnTick() {
            // Digital Exhibit
            var dist = World.GetDistance(Game.Player.Character.Position, Mosleys.Config.DigitalShowPoint);
            if (dist <= 2.0f && !CatalogOpen) {
                Screen.DisplayHelpTextThisFrame("Drücke ~INPUT_CONTEXT~ um den Katalog zu öffnen");
                
                if (Game.IsControlJustReleased(0, Control.Context))
                    OpenCatalog();
            }
            
            // Real Exhibits
            if (!ExhibitsSpawned) return;

            Exhibit closestExhibit = new Exhibit();
            float closestDist = float.MaxValue;
            bool foundClosest = false;
            foreach (var exhibit in _realExhibits) {
                if (exhibit?.Ready != true) continue;
                exhibit.Vehicle?.Repair();
                dist = World.GetDistance(Game.Player.Character.Position, exhibit.Location.ToVector3());
                if (dist <= 5.0f && dist < closestDist) {
                    closestDist = dist;
                    closestExhibit = exhibit;
                    foundClosest = true;
                }
            }
            
            if (!foundClosest) return;
            closestExhibit.ShowText();
            
            if (closestExhibit.TestDrive) return;
            if (Game.IsControlJustReleased(0, Control.Enter))
                closestExhibit.StartTestDrive();

            if (Game.IsControlJustReleased(0, Control.Context))
                closestExhibit.Buy();
        }

        public static async void SpawnExhibits() {
            var exhibits = await Utils.GetAllExhibits();
            _realExhibits = new Exhibit[Mosleys.Config.CarSlots.Length];

            for (int i = 0; i < exhibits.Length; i++) {
                var exhibit = exhibits[i];
                if (exhibit.Slot == 0) continue;

                var realExhibit = new Exhibit {
                    Uuid = exhibit.Uuid,
                    Owner = exhibit.Owner,
                    Description = exhibit.Description,
                    Price = exhibit.Price,
                    Slot = exhibit.Slot,
                    Location = Mosleys.Config.CarSlots[exhibit.Slot],
                    Properties = new VehicleProperties(exhibit.Vehicle),
                    TestDrive = exhibit.TestDrive
                };

                if (!exhibit.TestDrive)
                    realExhibit.Spawn();
                else realExhibit.Ready = true;
                _realExhibits[exhibit.Slot] = realExhibit;
            }

            ExhibitsSpawned = true;
        }

        public static void Cleanup() {
            ExhibitsSpawned = false;
            if (_realExhibits == null) return;
            
            foreach (var exhibit in _realExhibits) {
                exhibit?.Despawn();
            }

            _realExhibits = null;
        }

        public static void UpdateSlot(int slot) {
            if (!ExhibitsSpawned) return;
            if (_realExhibits[slot] == null)
                _realExhibits[slot] = new Exhibit { Slot = slot };
            
            _realExhibits[slot].Update();
        }

        public static void OnRemove(int slot) {
            if (!ExhibitsSpawned || _realExhibits[slot] == null) return;
            _realExhibits[slot].Despawn();
            _realExhibits[slot] = null;
        }

        private static async void OpenCatalog(string vehicleUuid = null) {
            if (CatalogOpen) return;
            CatalogOpen = true;
            Screen.ShowNotification("Lade Katalog...");
            var exhibits = (await Utils.GetAllExhibits()).Where(exhibit => exhibit.Slot == 0).ToArray();

            if (exhibits.Length == 0) {
                Notify.Error("Es gibt zur Zeit keine Autos im Katalog!");
                CatalogOpen = false;
                return;
            }
            
            ESX.UI.Menu.CloseAll();
            Game.Player.Character.Freeze(true);
            var camera = World.CreateCamera(Mosleys.Config.DigitalCamera.ToVector3(),
                new Vector3(0, 0, Mosleys.Config.DigitalCamera.W), API.GetGameplayCamFov());
            camera.PointAt(Mosleys.Config.CarSlots[0].ToVector3() + new Vector3(0, 0, 1.0f));
            camera.IsActive = true;
            API.RenderScriptCams(true, false, 0, true, true);

            var index = 0;
            if (vehicleUuid != null) {
                if (exhibits.Any(exhibit => exhibit.Uuid == vehicleUuid))
                    index = exhibits.ToList().FindIndex(exhibit => exhibit.Uuid == vehicleUuid);
            }

            var currentCar = new Exhibit { Slot = 0 };
            currentCar.FromExhibit(exhibits[index]);

            var carNames = exhibits.Select(exhibit => exhibit.DisplayName()).ToArray();
            var menuData = new ESX.UI.MenuData() {
                title = Mosleys.Config.MenuTitle,
                align = "top-left",
                elements = new List<ESX.UI.MenuElement> {
                    new ESX.UI.MenuElement {
                        label = "Auto",
                        name = "car",
                        value = index,
                        options = carNames,
                        type = "slider"
                    },
                    new ESX.UI.MenuElement {
                        label = "Probefahrt",
                        name = "testdrive",
                    },
                    new ESX.UI.MenuElement {
                        label = "Kaufen",
                        name = "buy"
                    }
                }
            };

            ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "digital_exhibits", menuData, async (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);
                var menu = new ESX.UI.Menu(dMenu);

                if (data.current.name == "testdrive") {
                    menu.Close();
                    CatalogOpen = false;
                    await currentCar.StartTestDrive(false);
                    OpenCatalog(currentCar.Uuid);
                }

                if (data.current.name == "buy") {
                    menu.Close();
                    CatalogOpen = false;
                    currentCar.Buy();
                }
            }, (dData, dMenu) => {
                var menu = new ESX.UI.Menu(dMenu);
                menu.Close();
                CatalogOpen = false;
            }, (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);

                if (data.current.name == "car") {
                    var newCar = exhibits[data.elements[0].value];
                    if (currentCar.Uuid == newCar.Uuid) return;
                    
                    currentCar.Despawn();
                    currentCar.FromExhibit(newCar);
                }
            });

            while (CatalogOpen) {
                currentCar.ShowText(false, 2.5f, 0.15f);
                await BaseScript.Delay(0);
            }
            
            API.RenderScriptCams(false, false, 0, true, true);
            camera.IsActive = false;
            camera.Delete();
            Game.Player.Character.Freeze(false);
            currentCar.Despawn();
        }

    }
}