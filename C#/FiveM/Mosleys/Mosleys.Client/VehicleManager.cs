﻿using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Mosleys.Client.Extensions;
using Mosleys.Client.Models;
using Mosleys.Shared.Models;
using Nexd.ESX.Client;

namespace Mosleys.Client {
    public static class VehicleManager {

        private static bool _menuOpen = false;
        private static Exhibit _currentCar;

        public static async void OpenMenu() {
            if (_menuOpen) _currentCar?.Despawn();
            var exhibits = await Utils.GetPlayerExhibits();
            
            if (exhibits.Length == 0) {
                Notify.Error("Du hast keine Autos ausgestellt!");
                _menuOpen = false;
                return;
            }

            Camera camera = null;
            if (!_menuOpen) {
                Game.Player.Character.Freeze(true);
                camera = World.CreateCamera(Mosleys.Config.DigitalCamera.ToVector3(),
                    new Vector3(0, 0, Mosleys.Config.DigitalCamera.W), API.GetGameplayCamFov());
                camera.PointAt(Mosleys.Config.CarSlots[0].ToVector3() + new Vector3(0, 0, 1.0f));
                camera.IsActive = true;
                API.RenderScriptCams(true, false, 0, true, true);
            }

            _menuOpen = true;
            _currentCar = new Exhibit { Slot = 0 };
            _currentCar.FromExhibit(exhibits[0], 0);

            var menuData = new ESX.UI.MenuData {
                title = Mosleys.Config.MenuTitle,
                align = "top-left",
                elements = exhibits.Select(exhibit => new ESX.UI.MenuElement {
                    name = exhibit.Uuid,
                    label = exhibit.DisplayName() + " - $" + exhibit.Price
                }).ToList()
            };

            ESX.UI.Menu.CloseAll();
            ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "owner_menu", menuData, (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);
                OpenSubMenu(exhibits.Single(exhibit => exhibit.Uuid == data.current.name));
            }, (dData, dMenu) => {
                var menu = new ESX.UI.Menu(dMenu);
                menu.Close();
                _menuOpen = false;
            }, (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);
                var index = data.elements.FindIndex(element => element.name == data.current.name);
                
                _currentCar.Despawn();
                _currentCar.FromExhibit(exhibits[index], 0);
            });

            while (_menuOpen) {
                _currentCar.ShowText(false, 5.0f, 0.15f);
                await BaseScript.Delay(0);
            }

            if (camera != null) {
                API.RenderScriptCams(false, false, 0, true, true);
                camera.IsActive = false;
                camera.Delete();
                Game.Player.Character.Freeze(false);
                _currentCar.Despawn();
            }
        }

        private static void OpenSubMenu(ExhibitVehicle exhibit) {
            var menuData = new ESX.UI.MenuData {
                title = Mosleys.Config.MenuTitle + " - " + exhibit.DisplayName(),
                align = "top-left",
                elements = new List<ESX.UI.MenuElement> {
                    new ESX.UI.MenuElement {
                        name = "change_price",
                        label = "Preis ändern"
                    },
                    new ESX.UI.MenuElement {
                        name = "change_description",
                        label = "Beschreibung ändern"
                    },
                    new ESX.UI.MenuElement {
                        name = "delete",
                        label = "Zurückziehen"
                    }
                }
            };

            if (exhibit.Slot == 0) {
                var elements = menuData.elements;
                elements.Insert(0, new ESX.UI.MenuElement {
                    name = "push",
                    label = $"Ausstellen (${Mosleys.Config.ExhibitPrice})"
                });
                menuData.elements = elements;
            }

            ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "owner_vehicle_menu", menuData, async (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);
                var menu = new ESX.UI.Menu(dMenu);

                if (data.current.name == "change_price") {
                    var sPrice = await Mosleys.DisplayTextDialog("Neuer Preis");
                    if (!int.TryParse(sPrice, out int price)) {
                        Notify.Error("Der eingegebene Wert ist keine Zahl!");
                        return;
                    }
                
                    if (sPrice.Length > 7) {
                        Notify.Error("Dein Auto darf maximal $9.999.999 kosten!");
                        return;
                    }

                    if (price < Mosleys.Config.MinSellPrice) {
                        Notify.Error($"Der Preis muss mindestens ${Mosleys.Config.MinSellPrice} betragen!");
                        return;
                    }

                    exhibit.Price = price;
                    var success = await Mosleys.ServerCallback<bool>("mosleys:server:updateExhibit", new ExhibitUpdate {
                        Action = UpdateAction.Update,
                        Exhibit = exhibit
                    });
                    
                    if (success) Notify.Success("Der Preis wurde geändert!");
                    else Notify.Error("Der Preis konnte nicht geändert werden!");
                }

                if (data.current.name == "change_description") {
                    var description = await Mosleys.DisplayTextDialog("Beschreibung");
                    if (description == "Beschreibung") return;
                    if (description.Length > 60) {
                        Notify.Error("Die Beschreibung darf maximal 60 Zeichen lang sein!");
                        return;
                    }

                    exhibit.Description = description;
                    var success = await Mosleys.ServerCallback<bool>("mosleys:server:updateExhibit", new ExhibitUpdate {
                        Action = UpdateAction.Update,
                        Exhibit = exhibit
                    });
                    
                    if (success) Notify.Success("Die Beschreibung wurde geändert!");
                    else Notify.Error("Die Beschreibung konnte nicht geändert werden!");
                }

                if (data.current.name == "delete") {
                    var confirmation = await Mosleys.DisplayConfirmationDialog("Möchtest du dieses Auto wirklich aus dem Sortiment nehmen?");
                    if (!confirmation) return;
                    
                    var success = await Mosleys.ServerCallback<bool>("mosleys:server:updateExhibit", new ExhibitUpdate {
                        Action = UpdateAction.Delete,
                        Exhibit = exhibit
                    });
                    
                    if (success) Notify.Success("Dein Auto wird wieder in die Garage geliefert!");
                    else Notify.Error("Das Auto konnte nicht zurückgezogen werden!");

                    if (success) {
                        ESX.UI.Menu.CloseAll();
                        OpenMenu();
                    }
                }

                if (data.current.name == "push") {
                    var hasMoney = await Mosleys.ServerCallback<bool>("mosleys:server:checkMoney", Mosleys.Config.ExhibitPrice);
                    if (!hasMoney) {
                        Notify.Error("Du hast nicht genügend Geld!");
                        return;
                    }
                    
                    var newSlot = await Mosleys.ServerCallback<int>("mosleys:server:push", exhibit.Uuid);
                    
                    if (newSlot != 0) Notify.Success("Auto ausgestellt");
                    else Notify.Error("Das Auto konnte nicht ausgestellt werden!");

                    if (newSlot != 0) {
                        menu.Close();
                        exhibit.Slot = newSlot;
                        OpenSubMenu(exhibit);
                    }
                }
            }, (dData, dMenu) => {
                OpenMenu();
            });
        }
        
    }
}