﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Mosleys.Client.Extensions;
using Mosleys.Shared;
using Nexd.ESX.Client;

namespace Mosleys.Client {
    public sealed class Mosleys : BaseScript {
        public static Config Config;
        public static bool InArea = false;
        
        private static TaskCompletionSource<bool> _dialogTask;
        private static Mosleys _instance;

        public Mosleys() {
            _instance = this;
            Config = Config.LoadConfig();
            CreateBlip();
            
            EventHandlers["mosleys:dialog_accept"] += new Action(() => _dialogTask?.TrySetResult(true));
            EventHandlers["mosleys:dialog_deny"] += new Action(() => _dialogTask?.TrySetResult(false));

            EventHandlers["esx:playerLoaded"] += new Action(OnStart);
            EventHandlers["onResourceStop"] += new Action<string>(OnStop);
            EventHandlers["mosleys:client:updateSlot"] += new Action<int>(ExhibitHandler.UpdateSlot);
            EventHandlers["mosleys:client:onRemove"] += new Action<int>(ExhibitHandler.OnRemove);
            EventHandlers["mosleys:client:openManageMenu"] += new Action(VehicleManager.OpenMenu);
        }
        
        public static Task<T> ServerCallback<T>(string name, [Optional] dynamic args) {
            var source = new TaskCompletionSource<T>();
            ESX.TriggerServerCallback(name, new Action<dynamic>(o => {
                source.TrySetResult((T)o);
            }), args);
            return source.Task;
        }
        public static Task<Vehicle> SpawnVehicle(int model, Vector4 coords, bool local = false) {
            var source = new TaskCompletionSource<Vehicle>();

            if (local) {
                ESX.Game.Raw.SpawnLocalVehicle(model, coords.ToVector3(), coords.W, new Action<int>(handle => {
                    source.TrySetResult(new Vehicle(handle));
                }));
            }
            else {
                ESX.Game.Raw.SpawnVehicle(model, coords.ToVector3(), coords.W, new Action<int>(handle => {
                    source.TrySetResult(new Vehicle(handle));
                }));
            }

            return source.Task;
        }
        public static Task<bool> DisplayConfirmationDialog(string title) {
            _dialogTask = new TaskCompletionSource<bool>();
            
            TriggerEvent("okokRequests:RequestMenuData", -1, Config.MenuTitle, title, "mosleys:dialog_accept", "client", "", 0, "mosleys:dialog_deny");
            
            return _dialogTask.Task;
        }
        public static Task<string> DisplayTextDialog(string placeholder) {
            var source = new TaskCompletionSource<string>();

            ESX.UI.Menu.Open("dialog", API.GetCurrentResourceName(), "test_dialog", new ESX.UI.MenuData() {
                title = placeholder,
                type = "default",
                align = "center"
            }, (dData, dMenu) => {
                source.TrySetResult(dData.value as string);
                ESX.UI.Menu.Close(new ESX.UI.Menu(dMenu));
            }, (dData, dMenu) => {
                source.TrySetResult(null);
                ESX.UI.Menu.Close(new ESX.UI.Menu(dMenu));
            });

            return source.Task;
        }

        public static void DoCustomHudText(string message, int duration) {
            _instance.Exports["mythic_notify"].DoCustomHudText("branco", message, duration, true);
        }

        public static async Task<bool> IsPlateTaken(string plate) {
            var taken = await _instance.Exports["esx_vehicleshop"].IsPlateTaken(plate);
            return Convert.ToBoolean(taken);
        }
        public static async Task<string> GeneratePlate() {
            var plate = await _instance.Exports["esx_vehicleshop"].GeneratePlate();
            return Convert.ToString(plate);
        }

        private void CreateBlip() {
            var blip = new Blip(API.AddBlipForCoord(Config.Blip.Position.X, Config.Blip.Position.Y, Config.Blip.Position.Z));
            blip.Sprite = Config.Blip.Sprite;
            blip.Color = Config.Blip.Color;
            blip.Scale = Config.Blip.Scale;
            blip.Name = Config.MenuTitle;
            blip.IsShortRange = true;
            API.SetBlipDisplay(blip.Handle, 4);
        }
        
        private void OnStart() {
            Tick += async () => {
                var dist = World.GetDistance(Game.Player.Character.Position, Config.Blip.Position);

                if (dist <= Config.SpawnRadius) {
                    if (!InArea) {
                        ExhibitHandler.SpawnExhibits();
                        InArea = true;
                    }
                    
                    SellHandler.OnTick();
                    ExhibitHandler.OnTick();
                }else if (InArea) {
                    ExhibitHandler.Cleanup();
                    InArea = false;
                }
            };
        }

        private void OnStop(string resourceName) {
            if (resourceName != API.GetCurrentResourceName()) return;
            if (ExhibitHandler.ExhibitsSpawned)
                ExhibitHandler.Cleanup();
        }

    }
}