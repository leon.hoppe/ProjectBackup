﻿using System;
using CitizenFX.Core.Native;
using Mosleys.Shared.Models;

namespace Mosleys.Client.Extensions {
    public static class ExhibitVehicleExtensions {

        public static string DisplayName(this ExhibitVehicle exhibit) =>
            API.GetDisplayNameFromVehicleModel(Convert.ToUInt32((exhibit.Vehicle as dynamic).model));

    }
}