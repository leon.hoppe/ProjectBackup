﻿using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace Mosleys.Client.Extensions {
    public static class VehicleExtensions {

        public static void Freeze(this Entity vehicle, bool toggle) => API.FreezeEntityPosition(vehicle.Handle, toggle);

        public static void SetDoorLockStatus(this Vehicle vehicle, LockStatus status) => API.SetVehicleDoorsLocked(vehicle.Handle, (int)status);

    }

    public enum LockStatus {
        None,
        Unlocked,
        Locked,
        LockForPlayers,
        LockForInVehicle,
        LockedInitially,
        ForceShutDoors,
        LockedButCanBeDamaged
    }
}