﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using Mosleys.Client.Extensions;
using Mosleys.Shared.Models;
using Nexd.ESX.Client;

namespace Mosleys.Client {
    public static class SellHandler {

        private static bool _menuOpen = false;
        private static SellData _data;

        public static void OnTick() {
            if (Game.Player.Character.IsInVehicle()) {
                World.DrawMarker(MarkerType.HorizontalCircleSkinny, Mosleys.Config.SellLocation, Vector3.Zero, Vector3.Zero, 
                    new Vector3(3.7f, 3.7f, 0.2f), Color.FromArgb(155, 229, 255));

                float distance = World.GetDistance(Game.Player.Character.Position, Mosleys.Config.SellLocation);
                if (distance <= 4.0f && !_menuOpen) {
                    Screen.DisplayHelpTextThisFrame("Drücke ~INPUT_CONTEXT~ um das Menü zu öffnen");

                    if (Game.IsControlJustReleased(0, Control.Context))
                        HandleSell();
                }
            }
        }

        private static async void HandleSell() {
            var vehicle = Game.Player.LastVehicle;
            var props = ESX.Game.GetVehicleProperties(vehicle);
            
            // Check Vehice Class
            if (Mosleys.Config.ForbiddenVehicleClasses.Contains((int)vehicle.ClassType)) {
                Notify.Error("Du kannst dieses Fahrzeug hier nicht verkaufen!");
                return;
            }
            
            // Check Ownership
            var owner = await Mosleys.ServerCallback<bool>("mosleys:server:checkVehicleOwnership", props.plate);
            if (!owner) {
                Notify.Error("Dieses Fahrzeug gehört dir nicht!");
                return;
            }
            
            vehicle.Freeze(true);
            ESX.UI.Menu.CloseAll();

            _data = new SellData();
            
            // Create Menu
            var menuData = new ESX.UI.MenuData {
                title = Mosleys.Config.MenuTitle,
                align = "top-left",
                elements = new List<ESX.UI.MenuElement>(new [] {
                    new ESX.UI.MenuElement {
                        label = "Stellplatztyp wählen",
                        name = "choose_parkingspace",
                        value = 0,
                        options = new [] { "Ausgestellt", "Katalog" },
                        type = "slider"
                    },
                    new ESX.UI.MenuElement {
                        label = "Beschreibung hinzufügen",
                        name = "choose_description"
                    },
                    new ESX.UI.MenuElement {
                        label = "Preis wählen",
                        name = "choose_price"
                    },
                    new ESX.UI.MenuElement {
                        label = "Fertig",
                        name = "done"
                    }
                })
            };

            ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "sell_menu", menuData, (dData, dMenu) => {
                HandleMenuInteraction(new ESX.UI.Menu(dMenu), new ESX.UI.MenuData(dData));
            }, (dData, dMenu) => {
                ESX.UI.Menu.Close(new ESX.UI.Menu(dMenu));
                vehicle.Freeze(false);
                _menuOpen = false;
            });

            _menuOpen = true;
        }

        private static async void HandleMenuInteraction(ESX.UI.Menu menu, ESX.UI.MenuData data) {
            if (data.current.name == "choose_description") {
                var description = await Mosleys.DisplayTextDialog("Beschreibung");
                if (description == "Beschreibung") return;
                if (description.Length > 60) {
                    Notify.Error("Die Beschreibung darf maximal 60 Zeichen lang sein!");
                    return;
                }
                
                _data.Description = description;
            }

            if (data.current.name == "choose_price") {
                var sPrice = await Mosleys.DisplayTextDialog("Preis");

                if (!int.TryParse(sPrice, out int price)) {
                    Notify.Error("Der eingegebene Wert ist keine Zahl!");
                    return;
                }
                
                if (sPrice.Length > 7) {
                    Notify.Error("Dein Auto darf maximal $9.999.999 kosten!");
                    return;
                }

                if (price < Mosleys.Config.MinSellPrice) {
                    Notify.Error($"Der Preis muss mindestens ${Mosleys.Config.MinSellPrice} betragen!");
                    return;
                }

                _data.Price = price;
            }

            if (data.current.name == "done") {
                if (_data.Price == 0) {
                    Notify.Error("Lege zuerst einen Preis fest!");
                    return;
                }
                
                if (!await Mosleys.DisplayConfirmationDialog("Möchtest du das Auto wirklich verkaufen?")) {
                    Game.Player.LastVehicle.Freeze(false);
                    menu.Close();
                    _menuOpen = false;
                    return;
                }

                menu.Close();
                _menuOpen = false;

                _data.ParkingSpace = data.elements[0].value;
                if (_data.ParkingSpace == 0) {
                    var hasMoney = await Mosleys.ServerCallback<bool>("mosleys:server:checkMoney", Mosleys.Config.ExhibitPrice);

                    if (!hasMoney) {
                        Notify.Error($"Du hast nicht genügend Geld! Ein Ausstellungsplatz kostet ${Mosleys.Config.ExhibitPrice}.");
                        return;
                    }
                }

                // Prepare Vehicle
                var vehicle = Game.Player.LastVehicle;
                vehicle.EngineHealth = 1000;
                vehicle.IsEngineRunning = false;
                vehicle.Repair();
                vehicle.DirtLevel = 0;

                var props = ESX.Game.GetVehicleProperties(vehicle);
                _data.VehicleProperties = props.GetRaw();
                BaseScript.TriggerServerEvent("esx_vehicleshop:deleteVehicle", props.plate);

                var success = await Mosleys.ServerCallback<bool>("mosleys:server:sellVehicle", _data) || _data.ParkingSpace == 1;
                
                vehicle.Delete();
                if (success)
                    Notify.Success("Auto ausgestellt!");
                else
                    Notify.Warning("Alle Stellplätze sind belegt, Dein Auto kommt in den Katalog, bis ein Stellplatz frei wird.");
                
                Notify.Info($"Beim Verkauf werden {Mosleys.Config.SellBill * 100}% Provision abgezogen.");
            }
        }
        
    }
}