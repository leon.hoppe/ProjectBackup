fx_version 'cerulean'
games { 'gta5' }

--	details
author 'Leon Hoppe'
description "Mosley's"
version '2.0'

files {
    'Newtonsoft.Json.dll',
    'Mosleys.Shared.dll',
    'settings.ini'
}

client_scripts {
    'Mosleys.Client.net.dll'
}

server_scripts {
    'Mosleys.Server.net.dll'
}
