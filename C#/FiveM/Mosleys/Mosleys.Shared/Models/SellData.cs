﻿using System.Dynamic;

namespace Mosleys.Shared.Models {
    public struct SellData {
        public int ParkingSpace { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public ExpandoObject VehicleProperties { get; set; }
    }
}