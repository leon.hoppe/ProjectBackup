﻿namespace Mosleys.Shared.Models {
    public struct ExhibitUpdate {
        public UpdateAction Action { get; set; }
        public ExhibitVehicle Exhibit { get; set; }
    }

    public enum UpdateAction {
        Update,
        Delete
    }
}