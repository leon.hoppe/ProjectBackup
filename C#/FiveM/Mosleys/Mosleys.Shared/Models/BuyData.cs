﻿namespace Mosleys.Shared.Models {
    public struct BuyData {
        public string Uuid { get; set; }
        public string Plate { get; set; }
    }
}