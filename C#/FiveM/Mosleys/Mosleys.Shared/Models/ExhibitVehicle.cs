﻿using System;
using System.Dynamic;
namespace Mosleys.Shared.Models {
    public class ExhibitVehicle {
        public string Uuid { get; set; }
        public string Owner { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Slot { get; set; }
        public ExpandoObject Vehicle { get; set; }
        public bool TestDrive { get; set; }

        public static ExhibitVehicle FromDynamic(dynamic data) {
            var vehicle = new ExhibitVehicle();
            vehicle.Uuid = data.Uuid;
            vehicle.Owner = data.Owner;
            vehicle.Description = data.Description;
            vehicle.Price = Convert.ToInt32(data.Price);
            vehicle.Slot = Convert.ToInt32(data.Slot);
            vehicle.Vehicle = data.Vehicle;
            vehicle.TestDrive = data.TestDrive;
            return vehicle;
        }
    }
}