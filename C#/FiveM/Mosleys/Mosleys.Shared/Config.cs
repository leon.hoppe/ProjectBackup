﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core.Native;

namespace Mosleys.Shared {
    public sealed class Config {
        private sealed class SectionScript {
            private const char SplitChar = ' ';
            private const string SectionStart = "{";
            private const string SectionEnd = "}";
            private const string CommentPrefix = "//";
            private readonly string _fileName;

            private readonly List<Tuple<string, Dictionary<string, string>>> _loadedData =
                new List<Tuple<string, Dictionary<string, string>>>();

            private readonly List<string> _allSections = new List<string>();

            public SectionScript(string fileName, bool load = false) {
                _fileName = fileName;
                if (!load)
                    return;
                Read();
            }

            public string this[string section, string key, string result = ""] =>
                GetValue(section, key, result);

            public T GetValue<T>(string section, string key, T result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                try {
                    return (T)Convert.ChangeType(data, typeof(T));
                }
                catch (InvalidCastException) {
                    return result;
                }
            }

            public Vector3 GetVector3(string section, string key, Vector3 result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                data = data.Replace("[", "").Replace("]", "").Replace(" ", "");
                return new Vector3(data.Split(',').Select(Convert.ToSingle).ToArray());
            }

            public Vector4 GetVector4(string section, string key, Vector4 result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                data = data.Replace("[", "").Replace("]", "").Replace(" ", "");
                return new Vector4(data.Split(',').Select(Convert.ToSingle).ToArray());
            }

            public int[] GetIntArray(string section, string key, int[] result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                data = data.Replace("[", "").Replace("]", "").Replace(" ", "");
                return data.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            }

            public Vector4[] GetVector4Array(string section) {
                List<Vector4> vectors = new List<Vector4>();

                foreach (Tuple<string, Dictionary<string, string>> tuple in _loadedData) {
                    if (!tuple.Item1.Equals(section)) continue;

                    foreach (var key in tuple.Item2.Values) {
                        var data = key.Replace("[", "").Replace("]", "").Replace(" ", "");
                        var vector = new Vector4(data.Split(',').Select(Convert.ToSingle).ToArray());
                        vectors.Add(vector);
                    }
                }

                return vectors.ToArray();
            }

            public string GetData(string section, string key, string result) {
                foreach (Tuple<string, Dictionary<string, string>> tuple in _loadedData) {
                    if (tuple.Item1.Equals(section))
                        return tuple.Item2.ContainsKey(key) ? tuple.Item2[key] : result;
                }

                return result;
            }

            public bool SectionExists(string section) => _allSections.Contains(section);

            public void Read() {
                _loadedData.Clear();
                _allSections.Clear();
                string str1 = "";
                Dictionary<string, string> source = new Dictionary<string, string>();
                string str2 = API.LoadResourceFile(API.GetCurrentResourceName(), _fileName);
                char[] chArray = { '\n' };
                foreach (string str3 in str2.Split(chArray).Select(str => str.Trim())) {
                    string text = str3.Replace("\r", "");
                    if (!string.IsNullOrEmpty(text) && !string.IsNullOrWhiteSpace(text)) {
                        if (text.StartsWith(SectionStart)) {
                            str1 = text.Substring(1);
                            if (text.EndsWith(SectionEnd) && !str1.Contains(SectionStart)) {
                                str1 = str1.Remove(str1.Length - 1, 1);
                                _loadedData.Add(
                                    new Tuple<string, Dictionary<string, string>>(str1,
                                        new Dictionary<string, string>()));
                                if (!_allSections.Contains(str1))
                                    _allSections.Add(str1);
                                source.Clear();
                            }
                        }
                        else if (text.EndsWith(SectionEnd) || text.StartsWith(SectionEnd)) {
                            Dictionary<string, string> dictionary =
                                source.ToDictionary(
                                    entry => entry.Key,
                                    entry => entry.Value);
                            _loadedData.Add(new Tuple<string, Dictionary<string, string>>(str1, dictionary));
                            if (!_allSections.Contains(str1))
                                _allSections.Add(str1);
                            source.Clear();
                        }
                        else if (!text.StartsWith(CommentPrefix)) {
                            Tuple<string, string> data = ConvertToData(text);
                            if (!source.ContainsKey(data.Item1))
                                source.Add(data.Item1, data.Item2);
                        }
                    }
                }
            }

            private Tuple<string, string> ConvertToData(string text) {
                int length = text.IndexOf(SplitChar);
                if (length == -1)
                    return new Tuple<string, string>(text, "");
                string str = "";
                if (length - 1 < text.Length)
                    str = text.Substring(length + 1, text.Length - length - 1);
                return new Tuple<string, string>(text.Substring(0, length), str);
            }
        }

        public static Config LoadConfig() {
            var script = new SectionScript("settings.ini", true);

            var config = new Config {
                Blip = new BlipConfig {
                    Position = script.GetVector3("[Blip]", "Position", Vector3.Zero),
                    Sprite = (BlipSprite)script.GetValue("[Blip]", "Sprite", 380),
                    Color = (BlipColor)script.GetValue("[Blip]", "Color", 44),
                    Scale = script.GetValue("[Blip]", "Scale", 1.0f)
                },
                MenuTitle = script.GetValue("[General]", "MenuTitle", "Mosley's"),
                ExhibitPrice = script.GetValue("[General]", "ExhibitPrice", 500),
                TestDriveTime = script.GetValue("[General]", "TestDriveTime", 90000),
                SpawnRadius = script.GetValue("[General]", "SpawnRadius", 100f),
                MinSellPrice = script.GetValue("[General]", "MinSellPrice", 1000),
                SellBill = script.GetValue("[General]", "SellBill", 0.1f),
                ForbiddenVehicleClasses =
                    script.GetIntArray("[General]", "ForbiddenVehicleClasses", Array.Empty<int>()),
                SellLocation = script.GetVector3("[General]", "SellLocation", Vector3.Zero),
                SpawnLocation = script.GetVector4("[General]", "SpawnLocation", Vector4.Zero),
                TestDriveLocation = script.GetVector4("[General]", "TestDriveLocation", Vector4.Zero),
                DigitalShowPoint = script.GetVector3("[General]", "DigitalShowPoint", Vector3.Zero),
                DigitalCamera = script.GetVector4("[General]", "DigitalCamera", Vector4.Zero),
                CarSlots = script.GetVector4Array("[CarSlots]")
            };

            return config;
        }

        public BlipConfig Blip;

        public string MenuTitle;
        public int ExhibitPrice;
        public int TestDriveTime;
        public float SpawnRadius;
        public int MinSellPrice;
        public float SellBill;

        public int[] ForbiddenVehicleClasses;

        public Vector3 SellLocation;
        public Vector4 SpawnLocation;
        public Vector4 TestDriveLocation;
        public Vector3 DigitalShowPoint;
        public Vector4 DigitalCamera;

        public Vector4[] CarSlots;
    }

    public struct BlipConfig {
        public Vector3 Position;
        public BlipSprite Sprite;
        public BlipColor Color;
        public float Scale;
    }
}