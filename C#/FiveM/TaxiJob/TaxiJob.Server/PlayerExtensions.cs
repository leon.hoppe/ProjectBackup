﻿using CitizenFX.Core;

namespace TaxiJob.Server {
    public static class PlayerExtensions {

        public static int ServerId(this Player player) => int.Parse(player.Handle);

    }
}