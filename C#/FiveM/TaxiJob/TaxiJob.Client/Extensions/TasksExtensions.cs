﻿using CitizenFX.Core;

namespace TaxiJob.Client.Extensions {
    public static class TasksExtensions {

        public static void EnterVehicleOnFreeSeat(this Tasks tasks, Vehicle vehicle, VehicleSeat preferedSeat = VehicleSeat.Any) {
            if (preferedSeat != VehicleSeat.Any && vehicle.IsSeatFree(preferedSeat))
                tasks.EnterVehicle(vehicle, preferedSeat);
            
            else if (vehicle.IsSeatFree(VehicleSeat.Driver))
                tasks.EnterVehicle(vehicle, VehicleSeat.Driver);
            
            else if (vehicle.IsSeatFree(VehicleSeat.Passenger))
                tasks.EnterVehicle(vehicle, VehicleSeat.Passenger);
            
            else if (vehicle.IsSeatFree(VehicleSeat.LeftRear))
                tasks.EnterVehicle(vehicle, VehicleSeat.LeftRear);
            
            else if (vehicle.IsSeatFree(VehicleSeat.RightRear))
                tasks.EnterVehicle(vehicle, VehicleSeat.RightRear);
        }
        
    }
}