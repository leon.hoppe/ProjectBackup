﻿using CitizenFX.Core;

namespace TaxiJob.Client.Extensions {
    public static class VectorExtensions {

        public static Vector3 ToVector3(this Vector4 vector) => new Vector3(vector.X, vector.Y, vector.Z);
        public static Vector4 ToVector4(this Vector3 vector3, float w) => new Vector4(vector3, w);

    }
}