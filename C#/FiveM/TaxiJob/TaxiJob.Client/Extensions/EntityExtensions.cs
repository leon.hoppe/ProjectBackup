﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace TaxiJob.Client.Extensions {
    public static class EntityExtensions {
        
        public static void Freeze(this Entity entity, bool toggle) => API.FreezeEntityPosition(entity.Handle, toggle);
        public static void AsMissionEntry(this Entity entity) => API.SetEntityAsMissionEntity(entity.Handle, true, false);
        public static void SetBlockingOfNonTemporaryEvents(this Entity entity, bool toggle) => API.SetBlockingOfNonTemporaryEvents(entity.Handle, toggle);
        public static void LootAtStreet(this Entity entity) {
            var coords = entity.Position;
            var position = entity.GetNearestStreet();
            position.X -= coords.X;
            position.Y -= coords.Y;
            var angle = Math.Atan2(position.X, position.Y);
            entity.Heading = (float)((180 / Math.PI) * angle);
        }
        public static Vector3 GetNearestStreet(this Entity entity, int nodeType = 1) {
            var coords = entity.Position;
            var position = Vector3.Zero;
            API.GetClosestVehicleNode(coords.X, coords.Y, coords.Z, ref position, nodeType, 3.0f, 0);
            return position;
        }

        public static async Task Load(this Model model) {
            if (model.IsLoaded) return;
            API.RequestModel((uint)model.Hash);
            while (!API.HasModelLoaded((uint)model.Hash)) {
                await BaseScript.Delay(50);
            }
        }

    }
}