﻿#pragma warning disable CS1998

using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Nexd.ESX.Client;
using TaxiJob.Client.Extensions;
using TaxiJob.Client.Handler;
using TaxiJob.Shared;

namespace TaxiJob.Client {
    public sealed class TaxiJob : BaseScript {
        private static TaxiJob _instance;
        public static readonly ClientConfig Config = new ClientConfig();
        public static bool InDuty = false;

        public TaxiJob() {
            _instance = this;
            EventHandlers["esx:playerLoaded"] += new Action(OnStart);
            EventHandlers["onResourceStop"] += new Action<string>(OnStop);
            EventHandlers["taxijob:client:job"] += new Action<Vector3, string>(JobHandler.RequestJob);
            EventHandlers["taxijob:client:npc"] += new Action(NpcTaxiHandler.StartNpcJob);
            EventHandlers["taxijob:client:sync_meter"] += new Action<ExpandoObject, bool>(Taximeter.Sync);
            EventHandlers["taxijob:client:set_passanger"] += new Action<int>(client => Taximeter.CurrentClient = client);
        }
        
        public static Task<T> ServerCallback<T>(string name, [Optional] dynamic args) {
            var source = new TaskCompletionSource<T>();
            ESX.TriggerServerCallback(name, new Action<dynamic>(o => {
                source.TrySetResult((T)o);
            }), args);
            return source.Task;
        }
        public static Task<dynamic> ServerCallback2Results(string name, [Optional] dynamic args) {
            var source = new TaskCompletionSource<dynamic>();
            ESX.Raw.TriggerServerCallback(name, new Action<dynamic, dynamic>((o, o1) => {
                dynamic data = new ExpandoObject();
                data.first = o as ExpandoObject;
                data.second = o1 as ExpandoObject;
                source.TrySetResult(data);
            }), args);
            return source.Task;
        }
        public static Task<Vehicle> SpawnVehicle(int model, Vector4 coords, bool local = false) {
            var source = new TaskCompletionSource<Vehicle>();

            if (local) {
                ESX.Game.Raw.SpawnLocalVehicle(model, coords.ToVector3(), coords.W, new Action<int>(handle => {
                    source.TrySetResult(new Vehicle(handle));
                }));
            }
            else {
                ESX.Game.Raw.SpawnVehicle(model, coords.ToVector3(), coords.W, new Action<int>(handle => {
                    source.TrySetResult(new Vehicle(handle));
                }));
            }

            return source.Task;
        }
        
        public static Task<string> DisplayTextDialog(string placeholder) {
            var source = new TaskCompletionSource<string>();

            ESX.UI.Menu.Open("dialog", API.GetCurrentResourceName(), "test_dialog", new ESX.UI.MenuData() {
                title = placeholder,
                type = "default",
                align = "center"
            }, (dData, dMenu) => {
                source.TrySetResult(dData.value as string);
                ESX.UI.Menu.Close(new ESX.UI.Menu(dMenu));
            }, (dData, dMenu) => {
                source.TrySetResult(null);
                ESX.UI.Menu.Close(new ESX.UI.Menu(dMenu));
            });

            return source.Task;
        }

        public static async Task<string> GeneratePlate() {
            var plate = await _instance.Exports["esx_vehicleshop"].GeneratePlate();
            return Convert.ToString(plate);
        }

        public static void SetFuel(Vehicle vehicle, float level) {
            _instance.Exports["LegacyFuel"].SetFuel(vehicle.Handle, level);
        }

        public static void PrintDynamic(dynamic data, string prefix = "") {
            foreach (var element in (data as IDictionary<string, object>)) {
                Debug.WriteLine($"{prefix}{element.Key}: {element.Value}");
                
                if (element.Value.GetType() == typeof(ExpandoObject)) {
                    PrintDynamic(element.Value, prefix + "\t");
                }

                if (element.Value.GetType() == typeof(List<object>)) {
                    Debug.WriteLine(prefix + "\t", string.Join(", ", element.Value));
                }
            }
        }

        public static void SetServiceStatus(bool onService) {
            TriggerServerEvent("taxijob:server:service", onService);
        }

        private void OnStart() {
            Tick += async () => {
                if (NpcTaxiHandler.IsCustomer) NpcTaxiHandler.OnTick();
                if (Taximeter.Attributes.MeterVisible && !Game.PlayerPed.IsInVehicle()) Taximeter.Reset();

                if (ESX.GetPlayerData().job.name != "taxi") {
                    CloakroomHandler.JobBlip?.Delete();
                    CloakroomHandler.JobBlip = null;
                    return;
                }
                CloakroomHandler.OnTick();
                
                if (!InDuty) return;
                JobHandler.OnTick();
            };
        }

        private void OnStop(string resourceName) {
            if (API.GetCurrentResourceName() != resourceName) return;
            TriggerServerEvent("taxijob:server:accept", false, JobHandler.JobId);
            JobHandler.StopJob(false);
            SetServiceStatus(false);
        }
    }
}