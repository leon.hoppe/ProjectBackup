﻿using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Nexd.ESX.Client;

namespace TaxiJob.Client.Handler {
    public class TaxiMenuHandler {
        public static bool MenuOpen = false;
        private static ESX.UI.Menu _currentMenu;

        public static void OpenMenu() {
            var elements = new List<ESX.UI.MenuElement>();

            if (JobHandler.InJob) {
                elements.Add(new ESX.UI.MenuElement {
                    label = "Auftrag beenden",
                    name = "cancel_job"
                });
            } else if (Game.PlayerPed.CurrentVehicle?.PassengerCount > 0) {
                elements.Add(new ESX.UI.MenuElement {
                    label = "Auftrag starten",
                    name = "start_job"
                });
            }
            
            elements.Add(new ESX.UI.MenuElement {
                label = Taximeter.Attributes.MeterVisible ? "Taximeter aus" : " Taximeter an",
                name = "toggle_taximeter"
            });

            if (Taximeter.Attributes.MeterVisible) {
                elements.AddRange(new [] {
                    new ESX.UI.MenuElement {
                        label = Taximeter.Attributes.MeterPause ? "Taximeter fortsetzen" : "Taximeter anhalten",
                        name = "pause_taximeter"
                    },
                    new ESX.UI.MenuElement {
                        label = "Taximeter zurücksetzen",
                        name = "reset_taximeter"
                    }
                });

                if (!JobHandler.IsNpcJob) {
                    elements.Add(new ESX.UI.MenuElement {
                        label = "Tarif ändern",
                        name = "change_rate"
                    });
                }
            }
            
            var menuData = new ESX.UI.MenuData {
                align = "top-left",
                title = "Taxi",
                elements = elements
            };

            ESX.UI.Menu.CloseAll();
            _currentMenu = ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "taxi_menu", menuData, (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);
                var menu = new ESX.UI.Menu(dMenu);

                if (data.current.name == "cancel_job") {
                    JobHandler.StopJob();
                    menu.Close();
                }

                if (data.current.name == "start_job") {
                    if (!Game.PlayerPed.IsInVehicle(JobHandler.Vehicle)) return;
                    var passanger = JobHandler.Vehicle.Passengers.First(ped => ped?.Handle != 0);
                    Taximeter.CurrentClient = API.GetPlayerServerId(API.NetworkGetPlayerIndexFromPed(passanger.Handle));
                    JobHandler.StartJob();
                }

                if (data.current.name == "toggle_taximeter") {
                    Taximeter.Toggle();
                    OpenMenu();
                }

                if (data.current.name == "pause_taximeter") {
                    Taximeter.Pause();
                    OpenMenu();
                }

                if (data.current.name == "reset_taximeter") {
                    Taximeter.Reset(Taximeter.Attributes.MeterVisible);
                }

                if (data.current.name == "change_rate") {
                    OpenRateMenu();
                }
                
            }, (dData, dMenu) => {
                new ESX.UI.Menu(dMenu).Close();
                MenuOpen = false;
            });

            MenuOpen = true;
        }

        public static void CloseMenu() {
            if (!MenuOpen) return;
            _currentMenu.Close();
            MenuOpen = false;
        }

        public static void OpenRateMenu() {
            var rates = new [] { 10, 15, 20 };

            var menuData = new ESX.UI.MenuData {
                align = "top-left",
                title = "Taxi - Tarif",
                elements = rates.Select(rate => new ESX.UI.MenuElement {
                    label = rate + "$",
                    name = rate
                }).ToList()
            };

            ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "taxi_rate_menu", menuData, (dData, dMenu) => {
                Taximeter.Attributes.RateAmount = (int)dData.current.name;
                Taximeter.Update();
                new ESX.UI.Menu(dMenu).Close();
            }, (dData, dMenu) => {
                new ESX.UI.Menu(dMenu).Close();
            });
        }
        
    }
}