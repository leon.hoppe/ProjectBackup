﻿using System.Dynamic;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace TaxiJob.Client.Handler {
    public static class Taximeter {
        public static TaximeterAttributes Attributes { get; private set; } = new TaximeterAttributes();
        public static int CurrentClient;
        private static bool _forClient;

        public static void Toggle() {
            Attributes.MeterVisible = !Attributes.MeterVisible;
            Update();
        }

        public static void SetVisible(bool visible) {
            Attributes.MeterVisible = visible;
            Update();
        }

        public static void Pause(bool pause) {
            Attributes.MeterPause = pause;
            Update();
        }

        public static void Pause() {
            Pause(!Attributes.MeterPause);
        }

        public static void Update() {
            BaseScript.TriggerEvent("taxijob:client:nuimessage", Attributes.ToString());
            
            if (CurrentClient != 0) BaseScript.TriggerServerEvent("taxijob:server:sync_meter", Attributes, CurrentClient, _forClient);
        }

        public static void Reset(bool visible = false) {
            Attributes = new TaximeterAttributes();
            Attributes.MeterVisible = visible;
            Update();
        }

        public static void ActivateForClient(bool activate) {
            if (CurrentClient == 0) return;
            
            _forClient = activate;
            BaseScript.TriggerServerEvent("taxijob:server:sync_meter", Attributes, CurrentClient, _forClient);
        }

        public static void Sync(ExpandoObject attributes, bool active) {
            Attributes = new TaximeterAttributes(attributes);
            Attributes.MeterVisible = active;
            Update();
        }

        public static void CalculatePrice(Vector3 start, Vector3 end) {
            if (Attributes.RateType != "distance" || Attributes.RateAmount == 0) return;

            float dist = API.CalculateTravelDistanceBetweenPoints(start.X, start.Y, start.Z, end.X, end.Y, end.Z);
            Attributes.DistanceTraveled += dist;

            float fare = Attributes.FareOnStop + (Attributes.DistanceTraveled / 1609.34f) * Attributes.RateAmount;
            Attributes.CurrentFare = (fare < 99.99f ? "0" : "") + (fare < 9.99f ? "0" : "") + fare.ToString("N2");
            
            Update();
        }
        
    }
    
    public sealed class TaximeterAttributes {
        public bool MeterVisible { get; set; } = false;
        public string RateType { get; set; } = "distance";
        public float RateAmount { get; set; } = 10;
        public string CurrencyPrefix { get; set; } = "$";
        public string RateSuffix { get; set; } = "/mi";
        public string CurrentFare { get; set; } = "000.00";
        public float DistanceTraveled { get; set; } = 0;
        public float FareOnStop { get; set; } = 0;
        public bool MeterPause { get; set; } = true;
        public bool IsMoving { get; set; } = true;

        public override string ToString() {
            return $"{{\"meterVisible\": {MeterVisible.ToString().ToLower()}, \"rateType\": \"{RateType}\", \"rateAmount\": {RateAmount}, \"currencyPrefix\": \"{CurrencyPrefix}\", \"rateSuffix\": \"{RateSuffix}\", \"currentFare\": \"{CurrentFare}\", \"distanceTraveled\": {DistanceTraveled}, \"fareOnStop\": {FareOnStop}, \"meterPause\": {MeterPause.ToString().ToLower()}, \"isMoving\": {IsMoving.ToString().ToLower()} }}";
        }
        
        public TaximeterAttributes() {}

        public TaximeterAttributes(dynamic data) {
            MeterVisible = data.MeterVisible;
            RateType = data.RateType;
            RateAmount = data.RateAmount;
            CurrencyPrefix = data.CurrencyPrefix;
            RateSuffix = data.RateSuffix;
            CurrentFare = data.CurrentFare;
            DistanceTraveled = data.DistanceTraveled;
            FareOnStop = data.FareOnStop;
            MeterPause = data.MeterPause;
            IsMoving = data.IsMoving;
        }
    }
}