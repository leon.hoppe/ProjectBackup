﻿using System.Collections.Generic;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using Nexd.ESX.Client;
using TaxiJob.Client.Extensions;

namespace TaxiJob.Client.Handler {
    public static class CloakroomHandler {
        public static Blip JobBlip;

        public static void OnTick() {
            if (JobBlip == null) {
                var pos = TaxiJob.Config.Cloakroom.Position;
                JobBlip = new Blip(API.AddBlipForCoord(pos.X, pos.Y, pos.Y));
                BaseScript.TriggerEvent("esx_jobs:setBlipStyle", JobBlip.Handle);
            }
            
            var dist = World.GetDistance(Game.PlayerPed.Position, TaxiJob.Config.Cloakroom.Position);
            if (dist >= TaxiJob.Config.DrawDistance) return;

            TaxiJob.Config.Cloakroom.Render();
            if (dist <= TaxiJob.Config.Cloakroom.Size.X) {
                Screen.DisplayHelpTextThisFrame("Drücke ~INPUT_CONTEXT~ um dich umzuziehen");
                
                if (Game.IsControlJustReleased(0, Control.Context))
                    OpenMenu();
            }
        }

        private static async void OpenMenu() {
            var skins = await TaxiJob.ServerCallback2Results("esx_skin:getPlayerSkin");
            var sex = (int)skins.first.sex;

            var menuData = new ESX.UI.MenuData {
                title = "Umkleide",
                align = "top-left",
                elements = new List<ESX.UI.MenuElement> {
                    new ESX.UI.MenuElement {
                        label = "Zivilkleidung",
                        name = "default"
                    },
                    new ESX.UI.MenuElement {
                        label = "Arbeitskleidung",
                        name = "work"
                    }
                }
            };

            Game.PlayerPed.Freeze(true);
            ESX.UI.Menu.CloseAll();
            ESX.UI.Menu.Open("default", API.GetCurrentResourceName(), "cloakroom", menuData, (dData, dMenu) => {
                var data = new ESX.UI.MenuData(dData);
                var menu = new ESX.UI.Menu(dMenu);

                if (data.current.name == "default") {
                    JobHandler.StopJob(false);
                    BaseScript.TriggerEvent("skinchanger:loadSkin", skins.first);
                    TaxiJob.InDuty = false;
                    TaxiJob.SetServiceStatus(false);
                }

                if (data.current.name == "work") {
                    if (sex == 0) {
                        BaseScript.TriggerEvent("skinchanger:loadSkin", skins.second.skin_male);
                    }

                    if (sex == 1) {
                        BaseScript.TriggerEvent("skinchanger:loadSkin", skins.second.skin_female);
                    }

                    TaxiJob.InDuty = true;
                }
                
                menu.Close();
                Game.PlayerPed.Freeze(false);
            }, (dData, dMenu) => {
                var menu = new ESX.UI.Menu(dMenu);
                menu.Close();
                Game.PlayerPed.Freeze(false);
            });
        }
        
    }
}