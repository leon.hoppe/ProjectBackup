﻿using CitizenFX.Core;

namespace TaxiJob.Client {
    public class Notify {
        private const string Title = "Taxi";

        public static void SendMessage(string title, string message, int duration, string type, bool noSound = false) {
            BaseScript.TriggerEvent("okokNotify:Alert", title, message, duration, type, noSound);
        }

        public static void Info(string message) => SendMessage(Title, message, 5000, "info");
        public static void Success(string message) => SendMessage(Title, message, 5000, "success");
        public static void Warning(string message) => SendMessage(Title, message, 5000, "warning");
        public static void Error(string message) => SendMessage(Title, message, 5000, "error");

    }
}