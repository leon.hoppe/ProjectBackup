﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace TaxiJob.Shared {
    public static class ArrayExtensions {

        public static T Random<T>(this IEnumerable<T> enumerable, [Optional] int seed) {
            var random = new Random(seed != 0 ? seed : Environment.TickCount);
            var array = enumerable.ToArray();
            var index = random.Next(array.Length);
            return array[index];
        }
        
    }
}