﻿using System;
using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core.Native;

namespace TaxiJob.Shared {
    public sealed class ServerConfig {
        private sealed class ConfigReader {
            private const char SplitChar = ' ';
            private const string SectionStart = "{";
            private const string SectionEnd = "}";
            private const string CommentPrefix = "//";
            private readonly string _fileName;

            private readonly List<Tuple<string, Dictionary<string, string>>> _loadedData =
                new List<Tuple<string, Dictionary<string, string>>>();

            private readonly List<string> _allSections = new List<string>();

            public ConfigReader(string fileName, bool load = false) {
                _fileName = fileName;
                if (!load)
                    return;
                Read();
            }

            public string this[string section, string key, string result = ""] =>
                GetValue(section, key, result);

            public T GetValue<T>(string section, string key, T result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                try {
                    return (T)Convert.ChangeType(data, typeof(T));
                }
                catch (InvalidCastException) {
                    return result;
                }
            }

            public int[] GetIntArray(string section, string key, int[] result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                data = data.Replace("[", "").Replace("]", "").Replace(" ", "");
                return data.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            }
            
            public float[] GetFloatArray(string section, string key, float[] result) {
                if (!SectionExists(section))
                    return result;
                string data = GetData(section, key, result.ToString());

                data = data.Replace("[", "").Replace("]", "").Replace(" ", "");
                return data.Split(',').Select(Convert.ToSingle).ToArray();
            }
            
            public float[][] GetFloatArray2d(string section) {
                List<float[]> floats = new List<float[]>();

                foreach (Tuple<string, Dictionary<string, string>> tuple in _loadedData) {
                    if (!tuple.Item1.Equals(section)) continue;

                    foreach (var key in tuple.Item2.Values) {
                        var data = key.Replace("[", "").Replace("]", "").Replace(" ", "");
                        var vector = data.Split(',').Select(Convert.ToSingle).ToArray();
                        floats.Add(vector);
                    }
                }

                return floats.ToArray();
            }

            public string GetData(string section, string key, string result) {
                foreach (Tuple<string, Dictionary<string, string>> tuple in _loadedData) {
                    if (tuple.Item1.Equals(section))
                        return tuple.Item2.ContainsKey(key) ? tuple.Item2[key] : result;
                }

                return result;
            }

            public bool SectionExists(string section) => _allSections.Contains(section);

            public void Read() {
                _loadedData.Clear();
                _allSections.Clear();
                string str1 = "";
                Dictionary<string, string> source = new Dictionary<string, string>();
                string str2 = API.LoadResourceFile(API.GetCurrentResourceName(), _fileName);
                char[] chArray = { '\n' };
                foreach (string str3 in str2.Split(chArray).Select(str => str.Trim())) {
                    string text = str3.Replace("\r", "");
                    if (!string.IsNullOrEmpty(text) && !string.IsNullOrWhiteSpace(text)) {
                        if (text.StartsWith(SectionStart)) {
                            str1 = text.Substring(1);
                            if (text.EndsWith(SectionEnd) && !str1.Contains(SectionStart)) {
                                str1 = str1.Remove(str1.Length - 1, 1);
                                _loadedData.Add(
                                    new Tuple<string, Dictionary<string, string>>(str1,
                                        new Dictionary<string, string>()));
                                if (!_allSections.Contains(str1))
                                    _allSections.Add(str1);
                                source.Clear();
                            }
                        }
                        else if (text.EndsWith(SectionEnd) || text.StartsWith(SectionEnd)) {
                            Dictionary<string, string> dictionary =
                                source.ToDictionary(
                                    entry => entry.Key,
                                    entry => entry.Value);
                            _loadedData.Add(new Tuple<string, Dictionary<string, string>>(str1, dictionary));
                            if (!_allSections.Contains(str1))
                                _allSections.Add(str1);
                            source.Clear();
                        }
                        else if (!text.StartsWith(CommentPrefix)) {
                            Tuple<string, string> data = ConvertToData(text);
                            if (!source.ContainsKey(data.Item1))
                                source.Add(data.Item1, data.Item2);
                        }
                    }
                }
            }

            public Tuple<string, string> ConvertToData(string text) {
                int length = text.IndexOf(SplitChar);
                if (length == -1)
                    return new Tuple<string, string>(text, "");
                string str = "";
                if (length - 1 < text.Length)
                    str = text.Substring(length + 1, text.Length - length - 1);
                return new Tuple<string, string>(text.Substring(0, length), str);
            }
        }
    }
}