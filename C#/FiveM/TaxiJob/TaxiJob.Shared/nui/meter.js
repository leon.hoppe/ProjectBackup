let meterVisible = false;
let currencyPrefix = '';
let rateSuffix = '';
let rateType = 'distance';
let rateAmount = null;
let currentFare = 5.00;
let pause = false;
let moving = false;

$('#greenlight').hide();
$('#redlight').hide();

window.addEventListener("message", function (event) {
    switch (event.data.type) {
        case 'show_config':
            showConfig();
            break;
        case 'hide_config':
            hideConfig();
            break;
        case 'update_meter':
            updateMeterAttributes(JSON.parse(event.data.attributes));
            break;
        //refreshMeterDisplay(); TESTEAR ESTO
    }
});

/*$('.toggle-meter').on('click', function () {
    let attrs = {'meterVisible': 'toggle'}
    $.post('http://esx_taximeter/updateAttrs', JSON.stringify(attrs));
});

$('.fare-distance').on('click', function () {
    let attrs = {'rateType': 'distance'}
    $.post('http://esx_taximeter/updateAttrs', JSON.stringify(attrs));
});

$('.fare-flat').on('click', function () {
    let attrs = {'rateType': 'flat'}
    $.post('http://esx_taximeter/updateAttrs', JSON.stringify(attrs));
});

$('.close').on('click', function () {
    $.post('http://esx_taximeter/closeConfig', JSON.stringify({}));
});

$('.set-rate').on('click', function () {
    $.post('http://esx_taximeter/setRate', JSON.stringify({}));
});

$('.reset-trip').on('click', function () {
    $.post('http://esx_taximeter/resetFare', JSON.stringify({}));
});*/


function showConfig() {
    $('#config').show();
}

function hideConfig() {
    $('#config').hide();
}

function showMeter() {
    $('#meter').show();
}

function hideMeter() {
    $('#meter').hide();
}

function setRate(rate) {
    updateFareType();
}

function updateMeterAttributes(attributes) {
    if (attributes) {
        meterVisible = attributes['meterVisible'];
        rateType = attributes['rateType'];

        rateAmount = attributes['rateAmount'];
        currentFare = attributes['currentFare'];
        currencyPrefix = attributes['currencyPrefix'];
        rateSuffix = attributes['rateSuffix'];
        pause = attributes['meterPause'];
        //moving = attributes['isMoving'];
        if (moving !== attributes['isMoving']) {
            moving = attributes['isMoving'];
            updateLight()
        }
        refreshMeterDisplay();
    }
}

function updateLight() {
    if (moving) {
        $('#greenlight').show();
        $('#redlight').hide();
    } else {
        $('#redlight').show();
        $('#greenlight').hide();
    }
}

function refreshMeterDisplay() {
    toggleMeterVisibility();
    updateRateType();
    updateCurrentFare();
    updatePauseState();
    //updateLight();
}

function toggleMeterVisibility() {
    if (meterVisible) {
        showMeter();
    } else {
        hideMeter();
    }
}

function updateCurrentFare() {
    let string;
    if (rateType === 'flat') {
        string = currencyPrefix + (rateAmount || '--');
    } else {
        string = currencyPrefix + (currentFare || '0.00');
    }

    $('.meter-field.fare').text(string);
}

function updateRateType() {
    let string;
    if (rateType === 'flat') {
        string = 'FLAT'
    } else {
        string = currencyPrefix + (rateAmount || '--') + rateSuffix
    }

    $('.meter-field.rate').text(string);
}

function updatePauseState() {
    let string;
    if (pause) {
        string = 'OFF'
    } else {
        string = 'ON'
    }

    $('.meter-field.state').text(string);
}