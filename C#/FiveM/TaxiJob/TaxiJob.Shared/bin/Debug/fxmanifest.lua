fx_version 'cerulean'
games { 'gta5' }

--	details
author 'Leon Hoppe'
description "Mosley's"
version '2.0'

ui_page "nui/meter.html"

files {
    "nui/digital-7.regular.ttf",
    "nui/OPTICalculator.otf",
    "nui/meter.html",
    "nui/meter.css",
    "nui/meter.js",
    'nui/img/phone.png',
    'nui/img/fare1.png',
    'nui/img/fare2.png',
    'nui/img/redlight.png',
    'nui/img/greenlight.png',
    'nui/img/offlight.png',
    
    'Newtonsoft.Json.dll',
    'TaxiJob.Shared.dll',
    'settings.ini'
}

client_scripts {
    'client.lua',
    'TaxiJob.Client.net.dll'
}

server_scripts {
    'TaxiJob.Server.net.dll'
}
