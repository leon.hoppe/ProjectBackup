﻿using OpenTK.Windowing.Common;

namespace OpenTKTutorial {
    class Program {
        static void Main(string[] args) {
            using var game = new Game(800, 600, "Test Game");
            game.VSync = VSyncMode.On;
            game.Run();
        }
    }
}