﻿using System.Collections.Generic;
using OpenTK.Graphics.ES30;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using Image = SixLabors.ImageSharp.Image;

namespace OpenTKTutorial {
    public class Game : GameWindow {
        private int _vbo;
        private int _vao;
        private Shader _shader;
        
        public Game(int width, int height, string title) : base(GameWindowSettings.Default, new NativeWindowSettings {Title = title, Size = new Vector2i(width, height)}) { }

        protected override void OnUpdateFrame(FrameEventArgs args) {
            if (KeyboardState.IsKeyDown(Keys.Escape)) Close();
            
            base.OnUpdateFrame(args);
        }

        protected override void OnLoad() {
            GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            _shader = new Shader(@"D:\Programmierstuff\C#\OpenTKTutorial\shader.vert", @"D:\Programmierstuff\C#\OpenTKTutorial\shader.frag");

            float[] vertices = {
                -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, //Bottom-left vertex
                0.5f, -0.5f, 0.0f, 1.0f, 0.0f,  //Bottom-right vertex
                0.0f,  0.5f, 0.0f, 0.0f, 1.0f   //Top vertex
            };
            _vbo = GL.GenBuffer();

            _vao = GL.GenVertexArray();
            GL.BindVertexArray(_vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 2 * sizeof(float));
            GL.EnableVertexAttribArray(1);

            base.OnLoad();
        }

        protected override void OnUnload() {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(_vbo);
            _shader.Dispose();
            base.OnUnload();
        }

        protected override void OnRenderFrame(FrameEventArgs args) {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            
            _shader.Use();
            GL.BindVertexArray(_vao);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
            
            Context.SwapBuffers();
            base.OnRenderFrame(args);
        }

        protected override void OnResize(ResizeEventArgs e) {
            GL.Viewport(0, 0, e.Width, e.Height);
            base.OnResize(e);
        }
    }
}