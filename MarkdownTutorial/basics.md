# Erste Schritte mit Markdown

Diese Datei enthält unsere ersten Schritte mit Markdown.

Wir benutzen hier nur CommonMark.

## Der erste Schritt: Überschriften

Unter- und Überschriften können mit dem `#` (oder beliebig mehr) notiert werden.

## Der zweite Schritt: Aufzählungen

Was sind Beispiele für Markup Languages:
- Markdown
  * CommonMark
  * GitHub-flavored Markdown
- LaTeX
- HTML

---

Anleitung wie man Markdown Dateien erstellt:
1. Editor öffnen
   * qvim
   * emacs
   * nano
2. Text mit Markdown schreiben
3. Datei mit Endung `.md` speichern.
4. Beispielsweise mit Pandoc in `html` konvertieren.

## Hervorhebungen

Wir schreiben einen normalen Text. Einzelne Wörter können *kursiv* oder **fett** und ***fett-kursiv***

``
**fett**
``

## Links

Die Standardisierung von Markdown heißt [CommonMark](https://commonmark.org).

Das Logo von Markdown findet sich in diesem [GitHub Repository][1]

![Markdown Logo][2]

[1]: https://github.com/dcurtis/markdown-mark/
[2]: https://github.com/dcurtis/markdown-mark/raw/master/png/208x128-solid.png