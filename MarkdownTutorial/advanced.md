# Einführung in Markdown Teil 2

Unsere Aufgaben in diesem Screencast:
- [x] Task Liste
- [ ] Block Quotes (Zitate)
- [ ] Tabellen

## Zitate kennzeichnen

>Markdown ist eine vereinfachte Auszeichnungssprache,
>die von John Gruber und Aaron Swartz entworfen und im Dezember 2004 mit Version 1.0.1 spezifiziert wurde.
>Ein Ziel von Markdown ist eine leicht lesbare Ausgangsform bereits vor der Konvertierung

[Quelle](https://de.wikipedia.org/wiki/Markdown#:~:text=Markdown%20ist%20eine%20vereinfachte%20Auszeichnungssprache,Ausgangsform%20bereits%20vor%20der%20Konvertierung.)

## Tabellen

Typische Markdown Befehle

| Formatierung | Syntax | Beispiel |
| ------------ | ------ | -------- |
| Fett | `**Fett**` | **Fett** |
| | `__Fett__` | __Fett__ |
| Kursiv | `*Kursiv*` | *Kursiv* |
| | `_Kursiv_` | _Kursiv_ |
| Durchgestrichen | `~~durchgestrichen~~` | ~~durchgestrichen~~ |