import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  email: string = "leon@ladenbau-hoppe.de";
  pass: string = "1234567890";

  constructor() { }

  ngOnInit(): void {
  }

  onRegisterClick(): void {
    const email = document.getElementById("email") as HTMLInputElement;
    const password = document.getElementById("pass") as HTMLInputElement;
    if (email.value == this.email && password.value == this.pass) {
      window.alert("Erfolgreich eingeloggt!");
    }
  }
}
