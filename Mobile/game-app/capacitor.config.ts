import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'de.leonhoppe.game',
  appName: 'GameApp',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
