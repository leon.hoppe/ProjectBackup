import { Component } from '@angular/core';

const rows: number = 10;
const cols: number = 10;

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  public fields: {isBomb: boolean, isRevealed: boolean, bombCount: number, isFlag: boolean}[][] = [];
  public difficulty: string = "normal";
  public gameResult: string = "";
  public mode: string = "aufdecken"
  private firstMove: boolean = true;

  constructor() {
    this.reset();
  }

  public reset(): void {
    this.fields = [];
    this.gameResult = "";
    this.firstMove = true;

    for (let i = 0; i < cols; i++) {
      this.fields[i] = [];
      for (let j = 0; j < rows; j++) {
        const num = Math.random();

        let isBomb: boolean = false;
        if (this.difficulty == "einfach") isBomb = num > 0.9;
        if (this.difficulty == "normal") isBomb = num > 0.8;
        if (this.difficulty == "schwer") isBomb = num > 0.7;

        this.fields[i][j] = {isBomb, isRevealed: false, bombCount: 0, isFlag: false};
      }
    }

    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        let bombCount = 0;

        if (this.checkBomb(i - 1,     j)) bombCount++;
        if (this.checkBomb(i + 1,     j)) bombCount++;
        if (this.checkBomb(i,         j + 1)) bombCount++;
        if (this.checkBomb(i,         j - 1)) bombCount++;
        if (this.checkBomb(i - 1,  j - 1)) bombCount++;
        if (this.checkBomb(i + 1,  j + 1)) bombCount++;
        if (this.checkBomb(i - 1,  j + 1)) bombCount++;
        if (this.checkBomb(i + 1,  j - 1)) bombCount++;

        this.fields[i][j].bombCount = bombCount;
      }
    }
  }

  private checkBomb(x: number, y: number): boolean {
    if (x < 0 || x >= rows) return false;
    if (y < 0 || y >= cols) return false;
    return this.fields[x][y].isBomb;
  }

  public click(x: number, y: number): void {
    if (this.gameResult != "") return;

    if (this.mode == "flagge") {
      if (this.fields[x][y].isRevealed) return;
      this.fields[x][y].isFlag = !this.fields[x][y].isFlag;
      return;
    }

    if (this.firstMove) {
      while (this.fields[x][y].isBomb || this.fields[x][y].bombCount != 0) this.reset();
      this.firstMove = false;
    }

    if (this.fields[x][y].isBomb) {
      this.gameResult = "Verloren";
      this.fields.forEach(field => field.forEach(f => f.isRevealed = true));
      return;
    }

    if (this.fields[x][y].bombCount == 0) this.revealZeros(x, y);

    this.fields[x][y].isRevealed = true;

    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        if (!(this.fields[i][j].isBomb || this.fields[i][j].isRevealed)) return;
      }
    }
    this.gameResult = "Gewonnen";
  }

  public revealZeros(x: number, y: number): void {
    if (x < 0 || x >= rows) return;
    if (y < 0 || y >= cols) return;
    if (this.fields[x][y].isRevealed || this.fields[x][y].isBomb) return;
    if (this.fields[x][y].bombCount > 1) return;

    this.fields[x][y].isRevealed = true;

    if (this.fields[x][y].bombCount != 0) return;
    this.revealZeros(x - 1,     y);
    this.revealZeros(x + 1,     y);
    this.revealZeros(x,         y + 1);
    this.revealZeros(x,         y - 1);
    this.revealZeros(x - 1,  y - 1);
    this.revealZeros(x + 1,  y + 1);
    this.revealZeros(x - 1,  y + 1);
    this.revealZeros(x + 1,  y - 1);
  }

  public changeDifficulty(): void {
    switch (this.difficulty) {
      case "einfach":
        this.difficulty = "normal";
        break;

      case "normal":
        this.difficulty = "schwer";
        break;

      case "schwer":
      default:
        this.difficulty = "einfach";
        break;
    }
    this.reset();
  }

  public changeMode(): void {
    switch (this.mode) {
      case "flagge":
        this.mode = "aufdecken";
        break;

      case "aufdecken":
      default:
        this.mode = "flagge";
        break;
    }
  }

}
