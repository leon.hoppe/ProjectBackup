import {Component} from '@angular/core';

const gridSize = 19;

interface Point {
  x: number;
  y: number;
}

interface Snake {
  direction: Point;
  head: Point;
  tail: Point[];
}

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public gameResult: string = "";
  public grid: string[][] = [];
  public snake: Snake = {direction: {x: 0, y: 0}, head: {x: 0, y: 0}, tail: []};
  public fruit: Point = {x: 0, y: 0};
  public timer: number = 0;

  constructor() {
    this.reset();
  }

  public reset(): void {
    this.gameResult = "";
    this.grid = [];

    for (let i = 0; i < gridSize; i++) {
      this.grid[i] = [];
      for (let j = 0; j < gridSize; j++) {
        this.grid[i][j] = 'unset';
      }
    }

    this.snake = {direction: {x: 0, y: 0}, head: {x: Math.floor(gridSize / 2), y: Math.floor(gridSize / 2)}, tail: []};
    this.generateTailTile(3);
    this.fruit = this.generateFruitPos();

    clearInterval(this.timer);
    this.timer = 0;

    this.grid[this.snake.head.x][this.snake.head.y] = 'darkgreen';
    this.grid[this.fruit.x][this.fruit.y] = 'red';
  }

  public generateFruitPos(): Point {
    let x = Math.floor(Math.random() * gridSize);
    let y = Math.floor(Math.random() * gridSize);

    while (this.isInTail({x, y}) || (this.snake.head.x == x && this.snake.head.y == y)) {
      x = Math.floor(Math.random() * gridSize);
      y = Math.floor(Math.random() * gridSize);
    }

    return {x, y};
  }

  public generateTailTile(i: number): void {
    for (let j = 0; j < i; j++) {
      let pos = {...this.snake.head};
      if (this.snake.tail.length != 0)
        pos = {...this.snake.tail[this.snake.tail.length - 1]};

      this.snake.tail.push(pos);
    }
  }

  public clearBoard(): void {
    for (let i = 0; i < gridSize; i++) {
      for (let j = 0; j < gridSize; j++) {
        this.grid[i][j] = 'unset';
      }
    }
  }

  public isInTail(point: Point): boolean {
    for (let i = 0; i < this.snake.tail.length; i++) {
      if (this.snake.tail[i].x == point.x && this.snake.tail[i].y == point.y)
        return true;
    }
    return false;
  }

  public startGame(): void {
    if (this.timer != 0) return;
    this.timer = setInterval(this.update(), 200);
  }

  public changeDirection(dir: Point): void {
    this.snake.direction = dir;
    if (this.timer == 0) this.startGame();
  }

  public stopGame(): void {
    clearInterval(this.timer);
    this.timer = 0;
    this.gameResult = "Verloren";
  }

  public update(): TimerHandler {
    return () => {
      // Snake movement
      for (let i = this.snake.tail.length - 1; i >= 0; i--) {
        if (i == 0) this.snake.tail[i] = {x: this.snake.head.x, y: this.snake.head.y};
        else this.snake.tail[i] = {x: this.snake.tail[i - 1].x, y: this.snake.tail[i - 1].y};
      }
      this.snake.head = {x: this.snake.head.x + this.snake.direction.x, y: this.snake.head.y + this.snake.direction.y};

      // Fruit detection
      if (this.snake.head.x == this.fruit.x && this.snake.head.y == this.fruit.y) {
        this.generateTailTile(2);
        this.fruit = this.generateFruitPos();
      }

      // Death detection
      if (this.isInTail(this.snake.head)) {
        this.stopGame();
        return;
      }
      if (
        this.snake.head.x > this.grid.length - 1 ||
        this.snake.head.x < 0 ||
        this.snake.head.y > this.grid[0].length - 1 ||
        this.snake.head.y < 0
      ) {
        this.stopGame();
        return;
      }

      // Draw
      this.clearBoard();
      this.grid[this.fruit.x][this.fruit.y] = 'red';
      this.grid[this.snake.head.x][this.snake.head.y] = 'darkgreen';
      for (let tile of this.snake.tail) {
        this.grid[tile.x][tile.y] = 'green';
      }
    }
  }

}
