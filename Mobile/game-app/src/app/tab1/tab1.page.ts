import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public fields: string[] = [];
  public current: string = "X";
  public gameResult: string = "";
  public difficulty: string = "normal";

  constructor() {
    this.reset();
  }

  public reset(): void {
    this.fields = [];
    this.current = "X";
    this.gameResult = "";

    for (let i = 0; i < 9; i++) this.fields[i] = "";
  }

  public changeDifficulty(): void {
    switch (this.difficulty) {
      case "einfach":
        this.difficulty = "normal";
        break;

      case "normal":
        this.difficulty = "schwer";
        break;

      case "schwer":
      default:
        this.difficulty = "einfach";
        break;
    }
  }

  public click(index: number): void {
    if (this.gameResult != "") return;
    if (this.current != "X") return;
    if (this.fields[index] != "") return;
    this.fields[index] = "X";
    this.current = "O";
    this.checkWinner();
    this.ai();
    this.current = "X";
    this.checkWinner();
  }

  private ai(): void {
    let diff = this.difficulty;
    if (diff == "normal") {
      const num = Math.random();
      if (num > 0.6) diff = "schwer";
      else diff = "einfach";
    }

    const freeFields: {index: number, isFree: boolean}[] = []
    for (let i = 0; i < 9; i++) {
      freeFields[i] = {index: i, isFree: this.fields[i] == ""};
    }

    if (diff == "einfach") {
      const onlyFrees = freeFields.filter(field => field.isFree);
      if (onlyFrees.length == 0) return;
      const num = Math.floor(Math.random() * onlyFrees.length);
      this.fields[onlyFrees[num].index] = "O";
    }

    if (diff == "schwer") {
      if (this.completeRows("O")) return;
      if (this.completeRows("X")) return;

      const onlyFrees = freeFields.filter(field => field.isFree);
      if (onlyFrees.length == 0) return;
      const num = Math.floor(Math.random() * onlyFrees.length);
      this.fields[onlyFrees[num].index] = "O";
    }
  }

  private completeRows(key: string): boolean {
    if (this.placeInFreeRowSpace(0, 1, 2, key)) return true;
    if (this.placeInFreeRowSpace(3, 4, 5, key)) return true;
    if (this.placeInFreeRowSpace(6, 7, 8, key)) return true;

    if (this.placeInFreeRowSpace(0, 3, 6, key)) return true;
    if (this.placeInFreeRowSpace(1, 4, 7, key)) return true;
    if (this.placeInFreeRowSpace(2, 5, 8, key)) return true;

    if (this.placeInFreeRowSpace(0, 4, 8, key)) return true;
    if (this.placeInFreeRowSpace(2, 4, 6, key)) return true;
    return false;
  }

  private placeInFreeRowSpace(i1: number, i2: number, i3: number, key: string): boolean {
    if (this.fields[i1] != key && this.fields[i2] != key && this.fields[i3] != key) return false;
    if (this.fields[i1] == this.fields[i2] && this.fields[i1] != this.fields[i3] && this.fields[i3] == "") {
      this.fields[i3] = "O";
      return true;
    }
    if (this.fields[i1] == this.fields[i3] && this.fields[i1] != this.fields[i2] && this.fields[i2] == "") {
      this.fields[i2] = "O";
      return true;
    }
    if (this.fields[i2] == this.fields[i3] && this.fields[i2] != this.fields[i1] && this.fields[i1] == "") {
      this.fields[i1] = "O";
      return true;
    }
    return false;
  }

  private checkWinner(): void {
    if (this.fields[0] == this.fields[1] && this.fields[0] == this.fields[2] && this.fields[0] != "") this.gameResult = this.fields[0] + " hat gewonnen";
    else if (this.fields[3] == this.fields[4] && this.fields[3] == this.fields[5] && this.fields[3] != "") this.gameResult = this.fields[3] + " hat gewonnen";
    else if (this.fields[6] == this.fields[7] && this.fields[6] == this.fields[8] && this.fields[6] != "") this.gameResult = this.fields[6] + " hat gewonnen";

    else if (this.fields[0] == this.fields[3] && this.fields[0] == this.fields[6] && this.fields[0] != "") this.gameResult = this.fields[0] + " hat gewonnen";
    else if (this.fields[1] == this.fields[4] && this.fields[1] == this.fields[7] && this.fields[1] != "") this.gameResult = this.fields[1] + " hat gewonnen";
    else if (this.fields[2] == this.fields[5] && this.fields[2] == this.fields[8] && this.fields[2] != "") this.gameResult = this.fields[2] + " hat gewonnen";

    else if (this.fields[0] == this.fields[4] && this.fields[0] == this.fields[8] && this.fields[0] != "") this.gameResult = this.fields[0] + " hat gewonnen";
    else if (this.fields[2] == this.fields[4] && this.fields[2] == this.fields[6] && this.fields[2] != "") this.gameResult = this.fields[2] + " hat gewonnen";

    else if (this.fields.filter(field => field == "").length == 0) this.gameResult = "unentschieden";
  }

}
