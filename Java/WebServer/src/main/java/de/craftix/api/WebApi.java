package de.craftix.api;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import de.craftix.WebServer;

import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;

public class WebApi {

    protected void sendDataAsJson(HttpExchange exchange, Serializable data) {
        if (data instanceof String) sendData(exchange, ((String) data).getBytes());
        else sendData(exchange, new Gson().toJson(data).getBytes());
    }

    protected void sendData(HttpExchange exchange, byte[] data) {
        try {
            exchange.sendResponseHeaders(200, data.length);
            OutputStream out = exchange.getResponseBody();
            out.write(data);
            out.close();
        }catch (Exception e) { e.printStackTrace(); }
    }

    protected <T> T getDataFromBody(HttpExchange exchange, Type type) {
        byte[] data = WebServer.getBytesFromStream(exchange.getRequestBody());
        Gson gson = new Gson();
        return gson.fromJson(new String(data), type);
    }
}
