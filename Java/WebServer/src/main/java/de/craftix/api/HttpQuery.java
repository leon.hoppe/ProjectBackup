package de.craftix.api;

import com.sun.net.httpserver.HttpExchange;

import java.util.HashMap;
import java.util.Map;

public class HttpQuery {
    private final Map<String, String> variables = new HashMap<>();

    public HttpQuery(HttpExchange exchange) {
        if (!exchange.getRequestURI().toString().contains("?")) return;
        String raw = exchange.getRequestURI().getQuery();
        String[] vars = raw.split("&");
        for (String var : vars)
            variables.put(var.split("=")[0].toLowerCase(), var.split("=")[1]);
    }

    public String getVariable(String name) { return variables.get(name.toLowerCase()) != null ? variables.get(name.toLowerCase()) : ""; }
}
