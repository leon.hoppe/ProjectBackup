package de.craftix.test;

import de.craftix.WebServer;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        WebServer.start(40, new File("src/test/resources/assets"), true);
        WebServer.addApi(new TestAPI());
    }
}
