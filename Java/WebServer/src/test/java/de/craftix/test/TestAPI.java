package de.craftix.test;

import com.sun.net.httpserver.HttpExchange;
import de.craftix.WebServer;
import de.craftix.api.*;

import java.io.Serializable;

@Route("/api/test")
public class TestAPI extends WebApi {

    public void sendWorkData(HttpExchange exchange) {
        sendData(exchange, "<h1>Test API works!</h1>".getBytes());
    }

    @Route("/time")
    public void getTime(HttpExchange exchange) {
        sendData(exchange, String.valueOf(System.currentTimeMillis()).getBytes());
    }

    @Route("/user")
    public void getUser(HttpExchange exchange) {
        sendDataAsJson(exchange, new User("Leon", "Hoppe", "leon.hoppe", "admin@leon-hoppe.de"));
    }

    @Route("/del")
    @SetMethod(RequestMethod.DELETE)
    public void delUser(HttpExchange exchange) {
        sendData(exchange, "user deleted!".getBytes());
    }

    @Route("/users")
    public void getUsers(HttpExchange exchange) {
        HttpQuery query = new HttpQuery(exchange);
        sendData(exchange, query.getVariable("userId").getBytes());
    }

    @Route("/add")
    @SetMethod(RequestMethod.POST)
    public void addUser(HttpExchange exchange) {
        String body = new String(WebServer.getBytesFromStream(exchange.getRequestBody()));
        sendData(exchange, body.getBytes());
    }

    private static class User implements Serializable {
        public String firstName;
        public String lastName;
        public String username;
        public String email;

        public User(String firstName, String lastName, String username, String email) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.username = username;
            this.email = email;
        }
    }

}
