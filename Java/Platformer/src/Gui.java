import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Gui extends JPanel implements ActionListener {

    Image img;
    int key = 0;
    int move = 0;

    public Gui() {
        setFocusable(true);
        ImageIcon u = new ImageIcon("rsc/background.jpg");
        img = u.getImage();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(img, 0, 0, null);
    }

    private class AL extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            key = e.getKeyCode();
            if (key == KeyEvent.VK_A) move = +1;
            if (key == KeyEvent.VK_D) move = -1;
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (key != KeyEvent.VK_A && key != KeyEvent.VK_D) return;
            move = 0;
            key = -1;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        requestFocus();
    }
}
