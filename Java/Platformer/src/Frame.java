import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame implements ActionListener {
    public static Frame frame;

    private JButton close;
    private JButton settings;
    private JButton info;
    private JButton end;

    public static void main(String[] args) {
        frame = new Frame("Menü");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setResizable(false);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public Frame(String title) {
        super(title);

        close = new JButton("Spiel starten");
        close.setBounds(120, 40, 160, 40);
        close.addActionListener(this);
        add(close);

        settings = new JButton("Einstellungen");
        settings.setBounds(120, 120, 160, 40);
        settings.addActionListener(this);
        add(settings);

        info = new JButton("Credits");
        info.setBounds(120, 200, 160, 40);
        info.addActionListener(this);
        add(info);

        end = new JButton("Spiel beenden");
        end.setBounds(120, 280, 160, 40);
        end.addActionListener(this);
        add(end);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(close)) {
            game();
        }
        if (e.getSource().equals(settings)) {
            selection();
        }
        if (e.getSource().equals(info)) {
            Object[] options = { "OK" };
            JOptionPane.showOptionDialog(null, "Programmiert von deiner Mum", "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        }
        if (e.getSource().equals(end)) {
            System.exit(0);
        }
    }

    public static void game() {
        frame.setVisible(false);
        JFrame game = new JFrame();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setSize(650, 350);
        game.setLocationRelativeTo(null);
        game.setResizable(false);
        game.setVisible(true);
        game.add(new Gui());
    }

    public static void selection() {

    }
}
