package de.craftix.engine;

import de.craftix.engine.objects.ColliderObject;
import de.craftix.engine.objects.GameObject;
import de.craftix.engine.objects.Texture;
import de.craftix.engine.ui.UIManager;
import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Scene {

    private BufferedImage backgroundImage;
    private Color backgroundColor;
    private final ArrayList<GameObject> objects = new ArrayList<>();
    private final ArrayList<ColliderObject> colObjects = new ArrayList<>();
    private final ArrayList<Texture> textures = new ArrayList<>();
    private final UIManager uiManager = new UIManager();

    public void setBackground(BufferedImage image) { this.backgroundImage = image; }
    public void setBackground(Color color) { this.backgroundColor = color; }
    public BufferedImage getBackgroundAsImage() { return backgroundImage; }
    public Color getBackgroundAsColor() { return backgroundColor; }

    public void clearScene() { objects.clear(); colObjects.clear(); }

    public void addGameObject(GameObject object) { objects.add(object); object.start(); }
    public void removeGameObject(GameObject object) { objects.remove(object); object.stop(); }
    public boolean containsGameObject(GameObject object) { return objects.contains(object); }
    public GameObject[] getGameObjects() { return objects.toArray(new GameObject[0]); }
    public GameObject[] getGameObjects(Vector2 pos) { ArrayList<GameObject> a = new ArrayList<>(); for (GameObject object : objects) if (object.getPosition().equals(pos)) a.add(object); return a.toArray(new GameObject[0]); }
    public GameObject[] getGameObjects(float layer) { ArrayList<GameObject> a = new ArrayList<>(); for (GameObject object : objects) if (object.getLayer() == layer) a.add(object); return a.toArray(new GameObject[0]); }
    public GameObject getGameObject(Vector2 pos) { for (GameObject object : objects) if (object.getPosition().equals(pos)) return object; return null; }
    public GameObject getGameObjectByRect(Vector2 pos) { for (GameObject object : objects) if (object.getRectangle().contains(pos.convert())) return object; return null; }

    public void addColliderObject(ColliderObject object) { colObjects.add(object); object.start(); }
    public void removeColliderObject(ColliderObject object) { colObjects.remove(object); object.stop(); }
    public boolean containsColliderObject(ColliderObject object) { return colObjects.contains(object); }
    public ColliderObject[] getColliderObjects() { return colObjects.toArray(new ColliderObject[0]); }
    public ColliderObject[] getColliderObjects(Vector2 pos) { ArrayList<ColliderObject> a = new ArrayList<>(); for (ColliderObject object : colObjects) if (object.getPosition().equals(pos)) a.add(object); return a.toArray(new ColliderObject[0]); }
    public ColliderObject getColliderObject(Vector2 pos) { for (ColliderObject object : colObjects) if (object.getPosition().equals(pos)) return object; return null; }

    public void addTexture(Texture object) { textures.add(object); }
    public void removeTexture(Texture object) { textures.remove(object); }
    public boolean containsTexture(Texture object) { return textures.contains(object); }
    public Texture[] getTextures() { return textures.toArray(new Texture[0]); }
    public Texture[] getTextures(Vector2 pos) { ArrayList<Texture> a = new ArrayList<>(); for (Texture object : textures) if (object.getPosition().equals(pos)) a.add(object); return a.toArray(new Texture[0]); }
    public Texture[] getTextures(float layer) { ArrayList<Texture> a = new ArrayList<>(); for (Texture object : textures) if (object.getLayer().equals(layer)) a.add(object); return a.toArray(new Texture[0]); }
    public Texture getTexture(Vector2 pos) { for (Texture object : textures) if (object.getPosition().equals(pos)) return object; return null; }
    public Texture getTextureByRect(Vector2 pos) { for (Texture object : textures) if (object.getRectangle().contains(pos.convert())) return object; return null; }

    public UIManager getUIManager() { return uiManager; }

    public void renderComponents(Graphics2D g) {
        for (float layer : GameEngine.getSortedLayers()) {
            for (Texture texture : getTextures(layer)) {
                if (Display.containsObject(texture.getScreenRect())) texture.render(g);
            }
            for (GameObject object : getGameObjects(layer)) {
                if (Display.containsObject(object.getScreenRect())) object.render(g);
            }
        }
        uiManager.render(g);
    }

    protected void fixedUpdate() {
        for (GameObject object : objects) object.fixedUpdate();
        for (ColliderObject object : colObjects) object.fixedUpdate();
    }
    protected void update() {
        for (GameObject object : objects) {
            object.update();
            if (object.getCollider() != null) object.getCollider().calculateCollisions();
        }
        for (ColliderObject object : colObjects) {
            object.update();
            object.calculateCollisions();
        }
    }
    protected void start() {
        for (GameObject object : objects) object.start();
        for (ColliderObject object : colObjects) object.start();
    }
    protected void stop() {
        for (GameObject object : objects) object.stop();
        for (ColliderObject object : colObjects) object.stop();
    }

}
