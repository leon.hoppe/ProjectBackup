package de.craftix.engine.ui;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;

public class UIText extends UIComponent {

    private String text;
    private Font font;
    private Color TC;

    public UIText() { this(0, 0, null, null, null); }

    public UIText(int x, int y, String text, Font font, Color color) { this.dimensions = new Dimensions(new Point(x, y), new Point()); this.text = text; this.font = font; this.TC = color; }
    public UIText(int x, int y, String text, Color color) { this(x, y, text, null, color); }
    public UIText(int x, int y, String text, Font font) { this(x, y, text, font, Color.BLACK); }
    public UIText(int x, int y, String text) { this(x, y, text, null, Color.BLACK); }

    public UIText(String text, Font font, Color color) { this(0, 0, text, font, color); }
    public UIText(String text, Color color) { this(text, null, color); }
    public UIText(String text, Font font) { this(text, font, Color.BLACK); }
    public UIText(String text) { this(text, null, Color.BLACK); }

    @Override
    public void render(Graphics2D g) {
        if (text != null) {
            g.setColor(TC);
            if (font != null) g.setFont(font);
            FontRenderContext render = g.getFontRenderContext();
            GlyphVector vector = g.getFont().createGlyphVector(render, text);
            Rectangle bounds = vector.getPixelBounds(null, 0, 0);
            int x = calculatePosition().x - (bounds.width / 2);
            int y = calculatePosition().y + (bounds.height / 2);
            g.drawString(text, x, y);
        }
    }

    public void setText(String text) { this.text = text; }
    public void setFont(Font font) { this.font = font; }
    public void setColor(Color color) { this.TC = color; }
    public String getText() { return text; }
    public Font getFont() { return font; }
    public Color getColor() { return TC; }

}
