package de.craftix.engine.ui;

import de.craftix.engine.var.Inputs;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UIManager extends Inputs {

    private final ArrayList<Integer> layers = new ArrayList<>();
    private final ArrayList<UIComponent> components = new ArrayList<>();

    public UIManager() { layers.add(-10); layers.add(0); layers.add(10); }

    public void render(Graphics2D g) {
        List<Integer> sortLayer = new ArrayList<>(layers);
        Collections.sort(sortLayer);
        for (Integer layer : sortLayer) {
            for (UIComponent component : components) {
                if (!component.getLayer().equals(layer)) continue;
                component.update();
                component.render(g);
            }
        }
    }

    public void addComponent(UIComponent component) { components.add(component); }
    public void removeComponent(UIComponent component) { components.remove(component); }
    public void addLayer(int layer) { layers.add(layer); }
    public void removeLayer(int layer) { if (layer != 0) layers.remove((Object)layer); }
    public UIComponent[] getComponents() { return components.toArray(new UIComponent[0]); }
    public Integer[] getLayers() { return layers.toArray(new Integer[0]); }

}
