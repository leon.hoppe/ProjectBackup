package de.craftix.engine.ui;

import de.craftix.engine.InputManager;
import de.craftix.engine.Resizer;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class UIButton extends UIComponent {
    private static final int offset = 2;
    private static final Color NBG = Color.DARK_GRAY;
    private static final Color HBG = Color.LIGHT_GRAY;
    private static final Color PBG = Color.WHITE;
    private static final Color FG = Color.GRAY;

    private boolean hover = false;
    private boolean pressed = false;
    private boolean renderPressed = false;
    private final ActionListener al;
    private final UIText text;

    private BufferedImage NIBG;
    private BufferedImage HIBG;
    private BufferedImage PBTX;

    public UIButton(Dimensions dimensions, UIText text, ActionListener al) { this.dimensions = dimensions; this.al = al; this.text = text; }
    public UIButton(Dimensions dimensions, ActionListener al) { this(dimensions, null, al); }

    @Override
    public void update() {
        Dimensions screen = new Dimensions(calculatePosition().x, calculatePosition().y, dimensions.getWidth(), dimensions.getHeight());
        hover = screen.contains(InputManager.getMouseRaw());
        if (InputManager.isMouseClicked(MouseEvent.BUTTON1) && hover && !pressed) {
            pressed = true;
            if (al != null) al.actionPerformed(new ActionEvent(this, 0, "Button Clicked"));
            renderPressed = true;
            new Thread(() -> { try { Thread.sleep(200); } catch (Exception ignored) {} renderPressed = false; }).start();
        }
        if (!InputManager.isMouseClicked(MouseEvent.BUTTON1)) pressed = false;
    }

    @Override
    public void render(Graphics2D g) {
        Point pos = calculatePosition();
        if (HIBG == null || NIBG == null || PBTX == null) {
            if (renderPressed) g.setColor(PBG);
            else if (hover) g.setColor(HBG);
            else g.setColor(NBG);
            g.fill(new Rectangle(pos.x, pos.y, dimensions.getWidth(), dimensions.getHeight()));
            pos.x += offset;
            pos.y += offset;
            g.setColor(FG);
            g.fill(new Rectangle(pos.x, pos.y, dimensions.getWidth() - offset * 2, dimensions.getHeight() - offset * 2));
        }else {
            if (NIBG.getWidth() != dimensions.getWidth() || NIBG.getHeight() != dimensions.getHeight())
                NIBG = Resizer.AVERAGE.resize(NIBG, dimensions.getWidth(), dimensions.getHeight());
            if (HIBG.getWidth() != dimensions.getWidth() || HIBG.getHeight() != dimensions.getHeight())
                HIBG = Resizer.AVERAGE.resize(HIBG, dimensions.getWidth(), dimensions.getHeight());
            if (PBTX.getWidth() != dimensions.getWidth() || PBTX.getHeight() != dimensions.getHeight())
                PBTX = Resizer.AVERAGE.resize(PBTX, dimensions.getWidth(), dimensions.getHeight());
            if (renderPressed) g.drawImage(PBTX, pos.x, pos.y, null);
            else if (!hover) g.drawImage(NIBG, pos.x, pos.y, null);
            else g.drawImage(HIBG, pos.x, pos.y, null);
        }
        if (text != null) {
            text.setDimensions(new Dimensions(dimensions.getX(), dimensions.getY(), 0, 0));
            text.render(g);
        }
    }

    public UIText getText() { return text; }
    public void setTexture(BufferedImage normal, BufferedImage hover, BufferedImage pressed) { this.NIBG = normal; this.HIBG = hover; this.PBTX = pressed; }
}
