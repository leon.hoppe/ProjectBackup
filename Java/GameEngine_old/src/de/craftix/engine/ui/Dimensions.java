package de.craftix.engine.ui;

import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

public class Dimensions implements Serializable {
    public static Dimensions parse(String str) {
        String[] parts = str.replace("[", "").replace("]", "").split(" - ");
        String[] pos = parts[0].split(":");
        String[] size = parts[1].split(":");
        return new Dimensions(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(size[0]), Integer.parseInt(size[1]));
    }

    private int x;
    private int y;
    private int width;
    private int height;

    public Dimensions(int x, int y, int width, int height) { this.x = x; this.y = y; this.width = width; this.height = height; }
    public Dimensions(Point pos, Point size) { this(pos.x, pos.y, size.x, size.y); }
    public Dimensions(Rectangle rect) { this(rect.x, rect.y, rect.width, rect.height); }

    public boolean contains(Vector2 other) { return convert().contains(other.convert()); }
    public boolean intersects(Dimensions other) { return convert().intersects(other.convert()); }

    public Rectangle convert() { return new Rectangle(x, y, width, height); }
    public Dimensions copy() { return new Dimensions(x, y, width, height); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dimensions that = (Dimensions) o;
        return x == that.x && y == that.y && width == that.width && height == that.height;
    }

    @Override
    public int hashCode() { return Objects.hash(x, y, width, height); }

    public void setX(int x) { this.x = x; }
    public void setY(int y) { this.y = y; }
    public void setWidth(int width) { this.width = width; }
    public void setHeight(int height) { this.height = height; }

    public int getX() { return x; }
    public int getY() { return y; }
    public int getWidth() { return width; }
    public int getHeight() { return height; }

    @Override
    public String toString() { return "[" + x + ":" + y + " - " + width + ":" + height + "]"; }
}
