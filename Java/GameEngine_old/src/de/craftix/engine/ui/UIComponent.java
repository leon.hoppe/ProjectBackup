package de.craftix.engine.ui;

import de.craftix.engine.Display;
import de.craftix.engine.Resizer;
import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.awt.image.BufferedImage;

public class UIComponent {

    protected Dimensions dimensions;
    protected BufferedImage texture;
    protected Integer layer;

    public UIComponent() { this(new Dimensions(0, 0, 50, 50), null, 0); }
    public UIComponent(Dimensions dimensions, BufferedImage texture, int layer) { this.dimensions = dimensions; this.texture = texture; this.layer = layer; }
    public UIComponent(int x, int y, int width, int height, BufferedImage texture) { this(new Dimensions(x, y, width, height), texture, 0); }
    public UIComponent(int x, int y, BufferedImage texture) { this(x, y, texture.getWidth(), texture.getHeight(), texture); }

    public void render(Graphics2D g) {
        g.setColor(Color.RED);
        if (texture != null) {
            if (texture.getHeight() != dimensions.getHeight() || texture.getWidth() != dimensions.getWidth())
                texture = Resizer.AVERAGE.resize(texture, dimensions.getWidth(), dimensions.getHeight());
            Point pos = calculatePosition();
            g.drawImage(texture, pos.x, pos.y, null);
        }else g.draw(dimensions.convert());
    }

    protected Point calculatePosition() {
        Point mid = new Vector2(Display.getInstance().getWidth(), Display.getInstance().getHeight()).divide(2, 2).convert();
        Point p = new Point(dimensions.getX() + (dimensions.getWidth() / 2), dimensions.getY() + (dimensions.getHeight() / 2));
        p.x -= mid.x;
        p.y -= mid.y;
        p.x *= -1;
        p.y *= -1;
        return p;
    }

    public void setDimensions(Dimensions dimensions) { this.dimensions = dimensions; }
    public void setTexture(BufferedImage texture) { this.texture = texture; }
    public void setLayer(int layer) { this.layer = layer; }

    public Dimensions getDimensions() { return dimensions; }
    public BufferedImage getTexture() { return texture; }
    public Integer getLayer() { return layer; }

    public void update() {}

}
