package de.craftix.engine;

import de.craftix.engine.var.Inputs;
import de.craftix.engine.var.Vector2;

import javax.swing.*;
import java.awt.*;

public class Display extends JLabel {
    private static Display instance;
    private static int framesD;
    private final int width, height;
    public static Timer framesT;

    public Display(int width, int height) {
        this.width = width;
        this.height = height;
        instance = this;
        framesD = 0;
        framesT = new Timer(1000, e -> {
            GameEngine.frames = framesD;
            framesD = 0;
        });
        InputManager inputManager = new InputManager();
        setSize(width, height);
        setFocusable(true);
        addKeyListener(inputManager);
        addMouseListener(inputManager);
        requestFocus();
    }

    public void addInputs(Inputs inputs) {
        addKeyListener(inputs);
        addMouseListener(inputs);
        addMouseMotionListener(inputs);
        addMouseWheelListener(inputs);
    }

    @Override
    public void paintComponent(Graphics g) {
        framesD++;
        GameEngine.getInstance().update();
        GameEngine.getScene().update();
        super.paintComponent(g);

        if (GameEngine.antialiasing()) ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        else ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        if (GameEngine.getScene().getBackgroundAsImage() != null) {
            if (GameEngine.getScene().getBackgroundAsImage().getWidth() == getWidth() && GameEngine.getScene().getBackgroundAsImage().getHeight() == getHeight())
                g.drawImage(GameEngine.getScene().getBackgroundAsImage(), 0, 0, null);
            else g.drawImage(Resizer.AVERAGE.resize(GameEngine.getScene().getBackgroundAsImage(), getWidth(), getHeight()), 0, 0, null);
        }
        if (GameEngine.getScene().getBackgroundAsColor() != null) { g.setColor(GameEngine.getScene().getBackgroundAsColor()); g.fillRect(0, 0, getWidth(), getHeight()); }

        g.setColor(Color.BLACK);
        if (GameEngine.showGrid()) {
            for (int x = 0; x < (getWidth() / GameEngine.getGridSize()) + 1; x++) g.drawLine(x * GameEngine.getGridSize(), 0, x * GameEngine.getGridSize(), getHeight());
            for (int y = 0; y < (getHeight() / GameEngine.getGridSize()) + 1; y++) g.drawLine(0, y * GameEngine.getGridSize(), getWidth(), y * GameEngine.getGridSize());
        }

        GameEngine.getScene().renderComponents((Graphics2D) g);

        if (GameEngine.showFrames()) {
            g.setColor(Color.DARK_GRAY);
            g.fillRect(9, 2, 50, 9);
            g.setColor(Color.WHITE);
            g.setFont(new Font("Tacoma", Font.BOLD, 10));
            g.drawString("FPS: " + GameEngine.frames, 10, 10);
        }

        repaint();
    }

    public static Rectangle calculateDisplayPosition(Vector2 pos, Vector2 size) {
        Vector2 res = new Vector2();
        Vector2 mid = new Vector2(instance.getWidth(), instance.getHeight()).divide(2, 2);
        Vector2 s = size.copy().divide(2, 2);
        res.subtract(GameEngine.getCamera().x, -GameEngine.getCamera().y);
        res.subtract(-pos.x, pos.y);
        res.subtract(s);
        res.add(mid);
        return new Rectangle(res.convert().x, res.convert().y, size.convert().x, size.convert().y);
    }

    public static Vector2 calculateVirtualPosition(Vector2 pos) {
        Vector2 res = new Vector2();
        Vector2 mid = new Vector2(instance.getWidth(), instance.getHeight()).divide(2, 2);
        res.add(GameEngine.getCamera().x, -GameEngine.getCamera().y);
        res.add(pos.x, pos.y);
        res.subtract(mid);
        res.multiply(1, -1);
        return res;
    }

    public static boolean containsObject(Rectangle screenRect) {
        Rectangle self = new Rectangle(0, 0, instance.getWidth(), instance.getHeight());
        return self.intersects(screenRect);
    }

    public static Display getInstance() { return instance; }
    public int getWidth() { return width; }
    public int getHeight() { return height; }
}
