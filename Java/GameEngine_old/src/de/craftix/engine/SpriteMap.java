package de.craftix.engine;

import java.awt.image.BufferedImage;

public class SpriteMap {

    private final int cols;
    private final int width;
    private final int height;
    private final BufferedImage sprite;

    public SpriteMap(int cols, BufferedImage sprite, int width, int height) {
        this.width = width;
        this.height = height;
        this.cols = cols;
        this.sprite = sprite;
    }

    public BufferedImage getTexture(int id){
        int row = (id / cols);
        int col = (id % cols);
        return getTexture(col, row);
    }

    public BufferedImage getTexture(int col, int row){
        return sprite.getSubimage(col * width, row * height, width, height);
    }

    public int getCols() { return cols; }

}
