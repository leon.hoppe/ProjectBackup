package de.craftix.engine;

import de.craftix.engine.var.Inputs;
import de.craftix.engine.var.Vector2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class InputManager extends Inputs {

    private static final boolean[] keys = new boolean[256];
    private static final boolean[] mouseButtons = new boolean[MouseInfo.getNumberOfButtons()];

    @Override
    public void mousePressed(MouseEvent e) { mouseButtons[e.getButton() - 1] = true; }
    @Override
    public void mouseReleased(MouseEvent e) { mouseButtons[e.getButton() - 1] = false; }
    @Override
    public void keyPressed(KeyEvent e) { keys[e.getKeyCode()] = true; }
    @Override
    public void keyReleased(KeyEvent e) { keys[e.getKeyCode()] = false; }

    public static Vector2 getMouseRaw() {
        Point mouse = new Point(MouseInfo.getPointerInfo().getLocation());
        SwingUtilities.convertPointFromScreen(mouse, GameEngine.getScreen());
        return new Vector2(mouse);
    }
    public static Vector2 getMousePos() { return Display.calculateVirtualPosition(getMouseRaw()); }
    public static boolean isKeyPressed(int key) { return keys[key]; }
    public static boolean isMouseClicked(int button) { return mouseButtons[button - 1]; }
}
