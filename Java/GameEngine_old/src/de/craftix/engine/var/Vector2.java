package de.craftix.engine.var;

import java.awt.*;
import java.io.Serializable;

public class Vector2 implements Serializable {

    public static Vector2 parseVector2(String vector) {
        vector = vector.replace("[", "");
        vector = vector.replace("]", "");
        String[] vars = vector.split(":");
        return new Vector2(Float.parseFloat(vars[0]), Float.parseFloat(vars[2]));
    }

    public float x;
    public float y;

    public Vector2(float x, float y) { this.x = x; this.y = y; }
    public Vector2(Vector2 v) { this(v.x, v.y); }
    public Vector2(Point p) { this(p.x, p.y); }
    public Vector2() { this(0, 0); }

    public Vector2 copy() { return new Vector2(x, y); }
    public boolean equals(Object v) { if (!(v instanceof Vector2)) return false; Vector2 v2 = (Vector2) v; return (v2.x == x && v2.y == y); }
    public String toString() { return "[" + x + ":" + y + "]"; }

    public Vector2 add(Vector2 v) { this.x += v.x; this.y += v.y; return this; }
    public Vector2 add(float x, float y) { this.x += x; this.y += y; return this; }
    public Vector2 subtract(Vector2 v) { this.x -= v.x; this.y -= v.y; return this; }
    public Vector2 subtract(float x, float y) { this.x -= x; this.y -= y; return this; }
    public Vector2 multiply(Vector2 v) { this.x *= v.x; this.y *= v.y; return this; }
    public Vector2 multiply(float x, float y) { this.x *= x; this.y *= y; return this; }
    public Vector2 divide(Vector2 v) { this.x /= v.x; this.y /= v.y; return this; }
    public Vector2 divide(float x, float y) { this.x /= x; this.y /= y; return this; }

    public Point convert() { return new Point(Mathf.round(x), Mathf.round(y)); }

}
