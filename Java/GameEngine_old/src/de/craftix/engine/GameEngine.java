package de.craftix.engine;

import de.craftix.engine.objects.GameObject;
import de.craftix.engine.ui.UIManager;
import de.craftix.engine.var.Inputs;
import de.craftix.engine.var.Vector2;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.List;

public class GameEngine extends JFrame {
    public static int frames = 0;
    private static GameEngine instance;
    private static boolean showFrames;
    private static boolean showGrid;
    private static boolean antialiasing;
    private static int gridSize;
    private static Timer timer;
    private static Timer physics;
    private static boolean running = false;
    private static Vector2 camera;
    private static HashMap<String, Float> layer;

    private static Display screen;
    private static Scene scene;
    private static Inputs inputs;

    public static void setup(int width, int height, String title, int tps, GameEngine instance) {
        GameEngine.instance = instance;
        instance.setSize(width + 17, height + 40);
        instance.setResizable(false);
        instance.setBackground(Color.BLACK);
        instance.setTitle(title);
        instance.setLocationRelativeTo(null);
        instance.setDefaultCloseOperation(EXIT_ON_CLOSE);
        scene = new Scene();
        screen = new Display(width, height);
        camera = new Vector2();
        layer = new HashMap<>();
        layer.put("Background", -10f);
        layer.put("Default", 0f);
        layer.put("Foreground", 10f);
        showFrames(false);
        showGrid(false);
        setAntialiasing(false);
        setGridSize(100);

        instance.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) { }
            @Override
            public void windowClosing(WindowEvent e) { stopGame(); }
            @Override
            public void windowClosed(WindowEvent e) { }
            @Override
            public void windowIconified(WindowEvent e) { }
            @Override
            public void windowDeiconified(WindowEvent e) { }
            @Override
            public void windowActivated(WindowEvent e) { }
            @Override
            public void windowDeactivated(WindowEvent e) { }
        });

        timer = new Timer(1000 / tps, e -> { scene.fixedUpdate(); instance.fixedUpdate(); });
        physics = new Timer(5, e -> instance.physicsUpdate());
    }

    public static void startGame() {
        instance.initialise();
        running = true;
        timer.start();
        physics.start();
        Display.framesT.start();
        instance.setContentPane(screen);
        instance.start();
        scene.start();
        instance.setVisible(true);
    }

    public static void stopGame() {
        instance.setVisible(false);
        running = false;
        timer.stop();
        physics.stop();
        Display.framesT.stop();
        instance.stop();
        scene.stop();
        System.exit(0);
    }

    public static void setIcon(BufferedImage image) {
        ImageIcon icon = new ImageIcon(image);
        instance.setIconImage(icon.getImage());
    }

    public static File loadFile(String path) {
        try {
            return new File(GameEngine.class.getClassLoader().getResource(path).getPath());
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }
    public static File loadExternalFile(String path) {
        try {
            return new File(path);
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    public static BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(Objects.requireNonNull(GameEngine.class.getClassLoader().getResource(path)));
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }
    public static BufferedImage loadExternalImage(String path) {
        try {
            return ImageIO.read(new File(path));
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    private void physicsUpdate() {
        for (GameObject object : getScene().getGameObjects()) {
            if (object.getPhysics() != null) object.getPhysics().calculatePhysics();
        }
    }

    public static GameEngine getInstance() { return instance; }
    public static boolean isRunning() { return running; }
    public static Scene getScene() { return scene; }
    public static UIManager getUIManager() { return scene.getUIManager(); }
    public static Display getScreen() { return screen; }
    public static Inputs getInputs() { return inputs; }
    public static boolean showFrames() { return showFrames; }
    public static boolean showGrid() { return showGrid; }
    public static boolean antialiasing() { return antialiasing; }
    public static int getGridSize() { return gridSize; }
    public static Vector2 getCamera() { return camera; }
    public static float getLayer(String name) { return layer.get(name); }
    public static List<Float> getSortedLayers() {
        List<Float> mapValues = new ArrayList<>(layer.values());
        Collections.sort(mapValues);
        return mapValues;
    }

    public static void setScene(Scene scene) { GameEngine.scene = scene; }
    public static void showFrames(boolean value) { showFrames = value; }
    public static void showGrid(boolean value) { showGrid = value; }
    public static void setAntialiasing(boolean value) { antialiasing = value; }
    public static void setGridSize(int gridSize) { GameEngine.gridSize = gridSize; }
    public static void setCamera(float x, float y) { camera = new Vector2(x, y); }
    public static void setInputs(Inputs inputs) { GameEngine.inputs = inputs; screen.addInputs(inputs); }
    public static void addLayer(String name, float layer) { GameEngine.layer.put(name, layer); }
    public static void removeLayer(String name) { if (!name.equalsIgnoreCase("Default")) GameEngine.layer.remove(name); }

    protected void fixedUpdate() {}
    protected void update() {}
    protected void start() {}
    protected void stop() {}
    protected void initialise() {}
}
