package de.craftix.engine.objects.particles;

import de.craftix.engine.var.Vector2;

public class ParticleSystem {
    private final Particle[] particles;
    private final int maxAmount;
    private final Vector2 spawn;
    private final Vector2 globalVelocity;

    public ParticleSystem(Vector2 spawn, int maxAmount) {
        this.spawn = spawn;
        this.maxAmount = maxAmount;
        globalVelocity = new Vector2();
        particles = new Particle[maxAmount];
    }

    public Particle[] getParticles() { return particles; }
    public int getMaxAmount() { return maxAmount; }
    public Vector2 getSpawn() { return spawn; }
    public Vector2 getGlobalVelocity() { return globalVelocity; }

    public void setSpawn(float x, float y) { spawn.x = x; spawn.y = y; }
    public void setGlobalVelocity(float x, float y) { globalVelocity.x = x; globalVelocity.y = y; }

}
