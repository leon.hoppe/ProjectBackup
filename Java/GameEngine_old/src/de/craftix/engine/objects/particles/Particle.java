package de.craftix.engine.objects.particles;

import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Particle {
    private final Vector2 velocity = new Vector2();
    private final Vector2 position;
    private final Vector2 size;
    private Color color;
    private BufferedImage texture;

    public Particle(Vector2 position, Vector2 size, Color color, BufferedImage texture) {
        this.position = position;
        this.size = size;
        this.color = color;
        this.texture = texture;
    }

    public Vector2 getVelocity() { return velocity; }
    public Vector2 getPosition() { return position; }
    public Vector2 getSize() { return size; }
    public Color getColor() { return color; }
    public BufferedImage getTexture() { return texture; }
    public void setVelocity(float x, float y) { velocity.x = x; velocity.y = y; }
    public void setPosition(float x, float y) { position.x = x; position.y = y; }
    public void setSize(float width, float height) { size.x = width; size.y = height; }
    public void setColor(Color c) { this.color = c; }
    public void setTexture(BufferedImage img) { this.texture = img; }

}
