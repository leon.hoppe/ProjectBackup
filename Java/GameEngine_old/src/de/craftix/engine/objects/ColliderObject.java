package de.craftix.engine.objects;

import de.craftix.engine.Display;
import de.craftix.engine.GameEngine;
import de.craftix.engine.var.Vector2;

import java.awt.*;

public class ColliderObject {

    private Vector2 position;
    private Vector2 size;
    private final boolean trigger;
    private boolean colliding;

    public ColliderObject(Vector2 position, Vector2 size, boolean isTrigger) { this.position = position; this.size = size; this.trigger = isTrigger; }

    public void calculateCollisions() {
        boolean isColliding = false;
        Rectangle self = getRectangle();
        for (GameObject object : GameEngine.getScene().getGameObjects()) {
            if (object.getCollider() == null) continue;
            Collider otherCol = object.getCollider();
            Rectangle other = object.getRectangle();
            if (self.intersects(other)) {
                if (!otherCol.isTrigger()) isColliding = true;
                if (otherCol.isTrigger()) onTrigger(otherCol);
                else onCollision(otherCol);
            }
        }
        for (ColliderObject object : GameEngine.getScene().getColliderObjects()) {
            if (object.equals(this)) continue;
            Rectangle other = object.getRectangle();
            if (other.intersects(self)) {
                if (!object.isTrigger()) isColliding = true;
                if (object.isTrigger()) onTrigger(object);
                else onCollision(object);
            }
        }
        colliding = isColliding;
    }

    public void setPosition(Vector2 position) { this.position = position; }
    public void setPosition(float x, float y) { this.position = new Vector2(x, y); }
    public void setSize(Vector2 size) { this.size = size; }
    public void setSize(float width, float height) { this.size = new Vector2(width, height); }

    public Vector2 getPosition() { return position; }
    public Vector2 getSize() { return size; }
    public Rectangle getRectangle() { return new Rectangle(position.convert().x, position.convert().y, size.convert().x, size.convert().y); }
    public Rectangle getScreenRect() { return Display.calculateDisplayPosition(position, size); }
    public boolean isTrigger() { return trigger; }
    public boolean isColliding() { return colliding; }

    public void fixedUpdate() {}
    public void update() {}
    public void start() {}
    public void stop() {}
    public void onCollision(Collider other) {}
    public void onTrigger(Collider other) {}
    public void onCollision(ColliderObject other) {}
    public void onTrigger(ColliderObject other) {}

}
