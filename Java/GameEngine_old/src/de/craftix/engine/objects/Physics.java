package de.craftix.engine.objects;

import de.craftix.engine.Display;
import de.craftix.engine.GameEngine;
import de.craftix.engine.var.Vector2;

import java.awt.*;

public class Physics {
    private static final float defaultGravity = 1.5f;
    private static final float maxForce = 5;

    private final GameObject gameObject;
    private final boolean cinematic;
    private final float gravity;
    private boolean onGround;

    private final Vector2 force = new Vector2();

    public Physics(GameObject gameObject, float gravity, boolean cinematic) { this.gameObject = gameObject; this.gravity = gravity; this.cinematic = cinematic; }
    public Physics(GameObject gameObject, boolean cinematic) { this(gameObject, defaultGravity, cinematic); }
    public Physics(GameObject gameObject, float gravity) { this(gameObject, gravity, false); }

    public void calculatePhysics() {
        if (cinematic) return;
        float dist = calculateDistanceToObjectBelow();
        Vector2 result = new Vector2();
        if (dist > 0) force.add(0, -gravity);

        if (force.y > maxForce || force.y < -maxForce) {
            if (force.y < 0) {
                result.y -= maxForce;
                force.y += maxForce;
            }else {
                result.y += maxForce;
                force.y -= maxForce;
            }
        }else {
            result.y += force.y;
            force.y = 0;
        }

        if (force.x > maxForce || force.x < -maxForce) {
            if (force.x < 0) {
                result.x -= maxForce;
                force.x += maxForce;
            }else {
                result.x += maxForce;
                force.x -= maxForce;
            }
        }else {
            result.x += force.x;
            force.x = 0;
        }
        if (precalculateCollision(result.copy(), gameObject.getSize())) result = new Vector2(0, 0);

        onGround = (dist <= 0);

        gameObject.getPosition().add(result);
    }

    private float calculateDistanceToObjectBelow() {
        float dist = Integer.MAX_VALUE;
        float sl = gameObject.getPosition().x - (gameObject.getSize().x / 2);
        float sr = gameObject.getPosition().x + (gameObject.getSize().x / 2);
        for (GameObject object : GameEngine.getScene().getGameObjects()) {
            if (object.equals(gameObject) || object.getPosition().y > gameObject.getPosition().y) continue;
            if (object.getCollider() == null || object.getCollider().isTrigger()) continue;
            float ol = object.getPosition().x - (object.getSize().x / 2);
            float or = object.getPosition().x + (object.getSize().x / 2);
            if (or < sl || ol > sr) continue;
            float d = (gameObject.getPosition().y - (gameObject.getSize().y / 2)) - (object.getPosition().y + (object.getSize().y / 2));
            if (d < dist) dist = d;
        }
        for (ColliderObject object : GameEngine.getScene().getColliderObjects()) {
            if (object.getPosition().y >= gameObject.getPosition().y) continue;
            if (object.isTrigger()) continue;
            float ol = object.getPosition().x - (object.getSize().x / 2);
            float or = object.getPosition().x + (object.getSize().x / 2);
            if (or < sl || ol > sr) continue;
            float d = (gameObject.getPosition().y - (gameObject.getSize().y / 2)) - (object.getPosition().y + (object.getSize().y / 2));
            if (d < dist) dist = d;
        }
        return dist;
    }

    private boolean precalculateCollision(Vector2 pos, Vector2 size) {
        pos.add(gameObject.getPosition());
        Rectangle self = Display.calculateDisplayPosition(pos, size);
        for (GameObject object : GameEngine.getScene().getGameObjects()) {
            if (object.getCollider() == null) continue;
            if (object.equals(gameObject)) continue;
            if (object.getCollider().isTrigger()) continue;
            Rectangle other = object.getScreenRect();
            if (self.intersects(other)) return true;
        }
        for (ColliderObject object : GameEngine.getScene().getColliderObjects()) {
            if (object.isTrigger()) continue;
            Rectangle other = object.getScreenRect();
            if (self.intersects(other)) return true;
        }
        return false;
    }

    public GameObject getGameObject() { return gameObject; }
    public boolean isCinematic() { return cinematic; }
    public float getGravity() { return gravity; }
    public boolean isOnGround() { return onGround; }

    public void addForce(float x, float y) { force.add(x, y); }
    public void addForce(Vector2 force) { this.force.add(force); }

}
