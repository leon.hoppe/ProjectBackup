package de.craftix.engine.objects;

import de.craftix.engine.Display;
import de.craftix.engine.Resizer;
import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.awt.image.BufferedImage;

public class GameObject {

    private Vector2 position;
    private Vector2 size;
    private Collider collider;
    private Physics physics;
    private BufferedImage texture;
    private Animation animation;
    private Color color;
    private float layer;

    public GameObject() { this(new Vector2(), new Vector2(), null, null, null, null, null, 0f); }
    public GameObject(Vector2 position, Vector2 size) { this(position, size, null, null, null, null, null, 0f); }
    public GameObject(Vector2 position, Vector2 size, float layer) { this(position, size, null, null, null, null, null, layer); }
    public GameObject(Vector2 position, Vector2 size, Color color) { this(position, size, null, null, null, null, color, 0f); }
    public GameObject(Vector2 position, Vector2 size, BufferedImage texture) { this(position, size, null, null, texture, null, null, 0f); }
    public GameObject(Vector2 position, Vector2 size, Animation animation) { this(position, size, null, null, null, animation, null, 0f); }
    public GameObject(Vector2 position, Vector2 size, boolean isCinematic) { this(position, size); this.physics = new Physics(this, isCinematic); }
    public GameObject(Vector2 position, Vector2 size, boolean isCinematic, boolean isTrigger, boolean considerLayer) { this(position, size, isCinematic); this.collider = new Collider(this, isTrigger, considerLayer); }

    private GameObject(Vector2 position, Vector2 size, Collider collider, Physics physics, BufferedImage texture, Animation animation, Color color, float layer) {
        this.position = position;
        this.size = size;
        this.collider = collider;
        this.physics = physics;
        this.texture = texture;
        this.animation = animation;
        this.color = color;
        this.layer = layer;
    }

    public void render(Graphics2D g) {
        if (animation != null) animation.update();
        Rectangle pos = Display.calculateDisplayPosition(position, size);
        if (animation != null) {
            if (animation.getImage().getWidth() == size.convert().x && animation.getImage().getHeight() == size.convert().y)
                g.drawImage(animation.getImage(), pos.x, pos.y, null);
            else g.drawImage(Resizer.AVERAGE.resize(animation.getImage(), size.convert().x, size.convert().y), pos.x, pos.y, null);
        }
        else if (texture != null) {
            if (texture.getWidth() == size.convert().x && texture.getHeight() == size.convert().y)
                g.drawImage(texture, pos.x, pos.y, null);
            else g.drawImage(Resizer.AVERAGE.resize(texture, size.convert().x, size.convert().y), pos.x, pos.y, null);
        }
        else if (color != null) { g.setColor(Color.WHITE); g.fill(pos);}
        else { g.setColor(Color.RED); g.draw(pos); }
    }

    public GameObject copy() { return new GameObject(position, size, collider, physics, texture, animation, color, layer); }

    public void setPosition(Vector2 position) { this.position = position; }
    public void setPosition(float x, float y) { this.position = new Vector2(x, y); }
    public void setSize(Vector2 size) { this.size = size; }
    public void setSize(float width, float height) { this.size = new Vector2(width, height); }
    public void setTexture(BufferedImage texture) { this.texture = texture; }
    public void setAnimation(Animation animation) { this.animation = animation; }
    public void setColor(Color color) { this.color = color; }
    public void setCollider(Collider collider) { this.collider = collider; }
    public void setPhysics(Physics physics) { this.physics = physics; }
    public void setLayer(float layer) { this.layer = layer; }

    public Vector2 getPosition() { return position; }
    public Vector2 getSize() { return size; }
    public Rectangle getRectangle() { return new Rectangle(position.convert().x, position.convert().y, size.convert().x, size.convert().y); }
    public Rectangle getScreenRect() { return Display.calculateDisplayPosition(position, size); }
    public BufferedImage getTexture() { return texture; }
    public Animation getAnimation() { return animation; }
    public Color getColor() { return color; }
    public Collider getCollider() { return collider; }
    public Physics getPhysics() { return physics; }
    public float getLayer() { return layer; }

    public void fixedUpdate() {}
    public void update() {}
    public void start() {}
    public void stop() {}
    public void onCollision(Collider other) {}
    public void onTrigger(Collider other) {}
    public void onCollision(ColliderObject other) {}
    public void onTrigger(ColliderObject other) {}

}
