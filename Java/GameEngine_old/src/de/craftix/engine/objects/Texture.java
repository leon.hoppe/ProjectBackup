package de.craftix.engine.objects;

import de.craftix.engine.Display;
import de.craftix.engine.Resizer;
import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Texture {

    private final BufferedImage texture;
    private final Color color;
    private final Vector2 position;
    private final Vector2 size;
    private float layer;

    public Texture(BufferedImage texture) { this(texture, new Vector2(), new Vector2(texture.getWidth(), texture.getHeight())); }
    public Texture(BufferedImage texture, Vector2 position, Vector2 size, float layer) { this.texture = texture; this.color = null; this.position = position; this.size = size; this.layer = layer; }
    public Texture(BufferedImage texture, Vector2 position, Vector2 size) { this(texture, position, size, 0); }
    public Texture(BufferedImage texture, Vector2 position) { this(texture, position, new Vector2(texture.getWidth(), texture.getHeight())); }
    public Texture(Color color, Vector2 position, Vector2 size, float layer) { this.color = color; this.texture = null; this.position = position; this.size = size; this.layer = layer; }
    public Texture(Color color, Vector2 position, Vector2 size) { this(color, position, size, 0); }

    private Texture(BufferedImage texture, Color color, Vector2 position, Vector2 size, float layer) {
        this.texture =texture;
        this.color = color;
        this.position = position;
        this.size = size;
        this.layer = layer;
    }

    public void render(Graphics2D g) {
        if (color != null) g.setColor(color);
        Rectangle dimensions = Display.calculateDisplayPosition(position, size);
        if (texture != null) {
            if (texture.getWidth() == size.convert().x && texture.getHeight() == size.convert().y)
                g.drawImage(texture, dimensions.x, dimensions.y, null);
            else g.drawImage(Resizer.AVERAGE.resize(texture, size.convert().x, size.convert().y), dimensions.x, dimensions.y, null);
        }
        else g.fill(dimensions);
    }

    public Texture copy() { return new Texture(texture, color, position, size, layer); }

    public void setPosition(int x, int y) { this.position.x = x; this.position.y = y; }
    public void setPosition(Vector2 v) { this.position.x = v.x; this.position.y = v.y; }
    public void setSize(int width, int height) { this.size.x = width; this.size.y = height; }
    public void setSize(Vector2 v) { this.size.x = v.x; this.size.y = v.y; }
    public void setLayer(float layer) { this.layer = layer; }

    public Rectangle getRectangle() { return new Rectangle(position.convert().x, position.convert().y, size.convert().x, size.convert().y); }
    public Rectangle getScreenRect() { return Display.calculateDisplayPosition(position, size); }
    public BufferedImage getTexture() { return texture; }
    public Color getColor() { return color; }
    public Vector2 getPosition() { return position; }
    public Vector2 getSize() { return size; }
    public Float getLayer() { return layer; }

}
