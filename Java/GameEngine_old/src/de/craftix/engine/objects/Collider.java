package de.craftix.engine.objects;

import de.craftix.engine.GameEngine;

import java.awt.*;

public class Collider {

    private final GameObject gameObject;
    private final boolean trigger;
    private final boolean considerLayers;
    private boolean colliding;

    public Collider(GameObject gameObject, boolean trigger, boolean considerLayers) { this.gameObject = gameObject; this.trigger = trigger; this.colliding = false; this.considerLayers = considerLayers; }
    public Collider(GameObject gameObject, boolean trigger) { this.gameObject = gameObject; this.trigger = trigger; this.colliding = false; this.considerLayers = false; }


    public void calculateCollisions() {
        boolean isColliding = false;
        Rectangle self = gameObject.getScreenRect();
        for (GameObject object : GameEngine.getScene().getGameObjects()) {
            if (object.getCollider() == null) continue;
            if (object.equals(gameObject)) continue;
            if (object.getCollider().considerLayers() && object.getLayer() != gameObject.getLayer()) continue;
            Collider otherCol = object.getCollider();
            Rectangle other = object.getScreenRect();
            if (self.intersects(other)) {
                if (!otherCol.isTrigger()) isColliding = true;
                if (otherCol.isTrigger()) gameObject.onTrigger(otherCol);
                else gameObject.onCollision(otherCol);
            }
        }
        for (ColliderObject object : GameEngine.getScene().getColliderObjects()) {
            Rectangle other = object.getScreenRect();
            if (other.intersects(self)) {
                if (!object.isTrigger()) isColliding = true;
                if (object.isTrigger()) gameObject.onTrigger(object);
                else gameObject.onCollision(object);
            }
        }
        colliding = isColliding;
    }

    public GameObject getGameObject() { return gameObject; }
    public boolean isTrigger() { return trigger; }
    public boolean isColliding() { return colliding; }
    public boolean considerLayers() { return considerLayers; }

}
