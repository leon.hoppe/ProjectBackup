package test;

import de.craftix.engine.GameEngine;
import de.craftix.engine.InputManager;
import de.craftix.engine.SpriteMap;
import de.craftix.engine.objects.ColliderObject;
import de.craftix.engine.objects.GameObject;
import de.craftix.engine.objects.Texture;
import de.craftix.engine.ui.Dimensions;
import de.craftix.engine.ui.UIButton;
import de.craftix.engine.ui.UIText;
import de.craftix.engine.var.Vector2;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Main extends GameEngine {
    static SpriteMap blocks = new SpriteMap(5, loadImage("test/terrain.png"), 16, 16);
    static GameObject player = new GameObject(new Vector2(0, 0), new Vector2(50, 50), false, false, false);

    public static void main(String[] args) {
        player.setTexture(blocks.getTexture(3));
        setup(800, 600, "GameEngine", 60, new Main());
        startGame();
    }

    @Override
    protected void initialise() {
        setIcon(blocks.getTexture(4));
        setAntialiasing(true);
        showFrames(true);
        getScene().setBackground(new Color(146, 189, 221));
        getScene().addTexture(new Texture(blocks.getTexture(1), new Vector2(0, -200), new Vector2(getScreen().getWidth(), 200)));
        getScene().addColliderObject(new ColliderObject(new Vector2(0, -200), new Vector2(getScreen().getWidth(), 0), false));
        getScene().addGameObject(player);

        UIButton button = new UIButton(new Dimensions(0, 0, 50, 50), new UIText("Test", new Font("Calibre", Font.BOLD, 20)), null);
        button.setTexture(blocks.getTexture(1), blocks.getTexture(2), blocks.getTexture(3));
        getUIManager().addComponent(button);
        getUIManager().addComponent(new UIText(0, 200, "Ur Mom Gae", new Font("Comic Sans MS", Font.PLAIN, 50)));
    }

    @Override
    protected void fixedUpdate() {
        if (InputManager.isKeyPressed(KeyEvent.VK_D)) player.getPhysics().addForce(5, 0);
        if (InputManager.isKeyPressed(KeyEvent.VK_A)) player.getPhysics().addForce(-5, 0);
        if (InputManager.isKeyPressed(KeyEvent.VK_SPACE) && player.getPhysics().isOnGround()) player.getPhysics().addForce(0, 200);
    }
}
