package de.hoppe.examples;

import edu.stanford.nlp.pipeline.CoreDocument;

public class SentimentAnalysis {

    public static void main(String[] args) {
        var pipeline = Pipeline.getPipeline();

        var text = "Hello this is Leon. I really don't like this place.";

        var document = new CoreDocument(text);
        pipeline.annotate(document);

        var sentences = document.sentences();
        for (var sentence : sentences) {
            var sentiment = sentence.sentiment();
            System.out.println(sentence + " = " + sentiment);
        }
    }

}
