package de.hoppe.examples;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.Properties;

public final class Pipeline {

    private final static Properties properties;
    private final static String propertiesName = "tokenize, ssplit, pos, lemma, ner, parse, sentiment";
    private static StanfordCoreNLP coreNLP;

    private Pipeline() {}

    static {
        properties = new Properties();
        properties.setProperty("annotators", propertiesName);
    }

    public static StanfordCoreNLP getPipeline() {
        if (coreNLP == null) {
            coreNLP = new StanfordCoreNLP(properties);
        }

        return coreNLP;
    }

}
