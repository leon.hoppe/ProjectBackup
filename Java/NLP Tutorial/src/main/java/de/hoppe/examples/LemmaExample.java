package de.hoppe.examples;

import edu.stanford.nlp.pipeline.CoreDocument;

public class LemmaExample {

    public static void main(String[] args) {
        var pipeline = Pipeline.getPipeline();

        var text = "I am trying to learn new technologies";

        var document = new CoreDocument(text);
        pipeline.annotate(document);

        var tokens = document.tokens();
        for (var token : tokens) {
            var lemma = token.lemma();
            System.out.println(token.originalText() + " = " + lemma);
        }
    }

}
