package de.hoppe.examples;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.CoreDocument;

public class POSExample {

    public static void main(String[] args) {
        var pipeline = Pipeline.getPipeline();

        var text = "Hey! I am Leon Hoppe.";

        var document = new CoreDocument(text);
        pipeline.annotate(document);

        var tokens = document.tokens();
        for (var token : tokens) {
            var pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
            System.out.println(token.originalText() + " = " + pos);
        }
    }

}
