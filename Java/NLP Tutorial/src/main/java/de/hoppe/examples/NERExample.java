package de.hoppe.examples;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.CoreDocument;

public class NERExample {

    public static void main(String[] args) {
        var pipeline = Pipeline.getPipeline();

        var text = "Hey! My name is Leon Hoppe and I have a friend, his name is Timo. We both are living in Lügde";

        var document = new CoreDocument(text);
        pipeline.annotate(document);

        var tokens = document.tokens();
        for (var token : tokens) {
            var ner = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
            System.out.println(token.originalText() + " = " + ner);
        }
    }

}
