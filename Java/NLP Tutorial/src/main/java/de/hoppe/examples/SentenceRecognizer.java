package de.hoppe.examples;

import edu.stanford.nlp.pipeline.CoreDocument;

public class SentenceRecognizer {

    public static void main(String[] args) {
        var pipeline = Pipeline.getPipeline();

        var text = "Hey! I am Leon Hoppe. I am a software Developer and Student.";

        var document = new CoreDocument(text);
        pipeline.annotate(document);

        var sentences = document.sentences();
        for (var sentence : sentences) {
            System.out.println(sentence);
        }
    }

}
