package de.hoppe.examples;

import edu.stanford.nlp.pipeline.CoreDocument;

public class TokenizeExample {

    public static void main(String[] args) {
        var pipeline = Pipeline.getPipeline();

        var text = "Hey! This is Leon Hoppe";

        var document = new CoreDocument(text);
        pipeline.annotate(document);

        var tokens = document.tokens();
        for (var token : tokens) {
            System.out.println(token.originalText());
        }
    }

}
