package de.craftix.engine.var;

import de.craftix.engine.GameEngine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class SqLite {

    protected String file;
    protected Connection con;

    public SqLite(String file) {
        this.file = file;
    }

    public void connect() {
        if (isConnected()) return;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:" + file);
        }catch (Exception e) {
            GameEngine.throwError(e);
        }
    }

    public void disconnect() {
        if (!isConnected()) return;
        try {
            con.close();
            con = null;
        }catch (Exception e) { GameEngine.throwError(e); }
    }

    public boolean isConnected() { return con != null; }

    public void insert(String qry) {
        if (!isConnected()) throw new NullPointerException("SqLite not connected");
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (Exception e) { GameEngine.throwError(e); }
    }

    public ResultSet getData(String qry) {
        if (!isConnected()) throw new NullPointerException("SqLite not connected");
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (Exception e) { GameEngine.throwError(e); }
        return null;
    }

}
