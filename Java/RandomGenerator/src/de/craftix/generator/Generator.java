package de.craftix.generator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Generator {
    public static MySQL sql;
    public static int id = 0;

    public static void main(String[] args) {
        //TODO: Add MySQL Connection
        sql = new MySQL("server.craftgang.de", 3306, "randomiser", "randomiser", "Kz.iJfkEmi.EBu2O");
        sql.connect();
        sql.insert("CREATE TABLE IF NOT EXISTS Results (ID INT(10), Timestamp VARCHAR(50), Number VARCHAR(100), Trys BIGINT(255))");
        startRandomLoop();
    }

    private static void startRandomLoop() {
        long trys = 0;
        Random rand = new Random();
        while (!Thread.currentThread().isInterrupted()) {
            float value = rand.nextFloat();
            System.out.print(value + "; ");
            if (value > 0.9999999f) {
                success(trys, value);
                trys = 0;
                id++;
            }
            trys++;
        }
    }

    private static void success(long trys, float number) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        sql.insert("INSERT INTO Results (ID, Timestamp, Trys, Number) VALUES (" + id + ", \"" + dtf.format(now) + "\", " + trys + ", \"" + number + "\")");
    }

}
