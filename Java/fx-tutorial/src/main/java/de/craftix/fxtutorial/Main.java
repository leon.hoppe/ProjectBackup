package de.craftix.fxtutorial;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        var root = new Group();
        var scene = new Scene(root, 1080, 720, Color.LIGHTSKYBLUE);

        var icon = new Image("logo.png");
        stage.getIcons().add(icon);
        stage.setTitle("Stage Demo Program");

        var text = new Text("Ein Text");
        text.setX(50);
        text.setY(50);
        text.setFont(Font.font("Verdana", 50));
        text.setFill(Color.LIMEGREEN);
        root.getChildren().add(text);

        var line = new Line();
        line.setStartX(200);
        line.setStartY(200);
        line.setEndX(500);
        line.setEndY(200);
        line.setStrokeWidth(5);
        line.setStroke(Color.RED);
        line.setOpacity(0.5);
        root.getChildren().add(line);

        stage.setScene(scene);
        stage.show();
    }
}