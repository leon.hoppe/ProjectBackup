module de.craftix.fxtutorial {
    requires javafx.controls;
    requires javafx.fxml;


    opens de.craftix.fxtutorial to javafx.fxml;
    exports de.craftix.fxtutorial;
}