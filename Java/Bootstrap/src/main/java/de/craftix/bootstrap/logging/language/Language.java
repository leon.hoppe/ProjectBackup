package de.craftix.bootstrap.logging.language;

import de.craftix.bootstrap.Bootstrap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public abstract class Language implements Serializable {

    protected Map<String, String> messages = new HashMap<>();

    protected void setMessage(Object key, String message) {
        messages.remove(key.toString());
        messages.put(key.toString(), message);
    }

    public String getMessage(String key) {
        String message = messages.get(key);
        if (message == null) return Bootstrap.getInstance().getLangManager().getMessage(Lang.MESSAGE_NOT_FOUND.toString(), new String[] {key});
        else return message;
    }

    public Language() {
        setMessage(Lang.MESSAGE_NOT_FOUND, "[" + getGlobalName() + "] Could not find message with Key '{0}'");
    }

    protected abstract void setupLanguage();

    public abstract String getLangKey();
    public abstract String getLocalName();
    public abstract String getGlobalName();

    protected boolean testLanguage() {
        for (Lang value : Lang.values()) {
            if (messages.get(value.toString()) == null) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Language{" + getLangKey() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Language language = (Language) o;
        return Objects.equals(getLangKey(), language.getLangKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLangKey());
    }
}
