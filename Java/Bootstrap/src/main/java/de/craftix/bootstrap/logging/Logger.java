package de.craftix.bootstrap.logging;

import de.craftix.bootstrap.Bootstrap;

import java.io.PrintStream;

public class Logger {

    private PrintStream infoStream;
    private PrintStream warningStream;
    private PrintStream errorStream;

    public Logger(PrintStream infoStream, PrintStream warningStream, PrintStream errorStream) {
        this.infoStream = infoStream;
        this.warningStream = warningStream;
        this.errorStream = errorStream;
    }

    public void info(Object key, String... variables) {
        String message = Bootstrap.getInstance().getLangManager().getMessage(key.toString(), variables);
        message = convertColorCodes(message);
        infoStream.println(message);
    }

    public void warning(Object key, String... variables) {
        String message = Bootstrap.getInstance().getLangManager().getMessage(key.toString(), variables);
        message = convertColorCodes(message);
        warningStream.println(message);
    }

    public void error(Object key, String... variables) {
        String message = Bootstrap.getInstance().getLangManager().getMessage(key.toString(), variables);
        message = convertColorCodes(message);
        errorStream.println(message);
    }

    public void throwException(Exception e) {
        e.printStackTrace(errorStream);
        System.exit(0);
    }

    private String convertColorCodes(String message) {
        //TODO: CONVERT COLOR CODES
        return message;
    }

}
