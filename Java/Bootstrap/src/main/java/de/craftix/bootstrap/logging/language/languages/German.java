package de.craftix.bootstrap.logging.language.languages;

import de.craftix.bootstrap.logging.language.Lang;
import de.craftix.bootstrap.logging.language.Language;

public final class German extends Language {
    @Override
    protected void setupLanguage() {
        setMessage(Lang.LOGGER_INITIALIZED, "Der Logger wurde erfolgreich initialisiert.");
        setMessage(Lang.DEFAULT_LANGUAGE, "Die Standartsprache ist jetzt {0}.");
        setMessage(Lang.LANG_MANAGER_INITIALIZED, "Der Language Manager wurde erfolgreich initialisiert.");
        setMessage(Lang.LANG_MANAGER_INITIALIZE_LANGUAGE, "Die Sprache {0} wurde erfolgreich initialisiert.");
        setMessage(Lang.LANG_MANAGER_INITIALIZE_LANGUAGES, "Alle Sprachen wurden erfolgreich initialisiert.");
        setMessage(Lang.LANG_MANAGER_TEST_LANGUAGE, "Die Sprache {0} wurde auf Vollständigkeit überprüft.");
        setMessage(Lang.LANG_MANAGER_TEST_LANGUAGES, "Alle Sprachen wurden auf Vollständigkeit überprüft.");
    }

    public String getLangKey() {
        return "de-DE";
    }
    public String getLocalName() {
        return "Deutsch";
    }
    public String getGlobalName() {
        return "German";
    }
}
