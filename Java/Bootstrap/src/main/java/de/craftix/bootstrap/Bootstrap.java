package de.craftix.bootstrap;

import de.craftix.bootstrap.logging.Logger;
import de.craftix.bootstrap.logging.language.Lang;
import de.craftix.bootstrap.logging.language.LangManager;
import de.craftix.bootstrap.logging.language.languages.German;

public final class Bootstrap {
    private static Bootstrap instance;
    private LangManager langManager;
    private Logger logger;

    public static void main(String[] args) {
        try {
            instance = new Bootstrap();
            instance.initialize();
        }catch (Exception e) {
            instance.logger.throwException(e);
        }
    }

    public static Bootstrap getInstance() {
        return instance;
    }

    private void initialize() {
        logger = new Logger(System.out, System.out, System.err);
        langManager = new LangManager("de-DE");
        langManager.addLanguages(new German());
        logger.info(Lang.LOGGER_INITIALIZED);
        logger.info(Lang.LANG_MANAGER_INITIALIZED);
        logger.info(Lang.DEFAULT_LANGUAGE, langManager.getCurrentLanguage().getLocalName());
    }

    public LangManager getLangManager() {
        return langManager;
    }

    public Logger getLogger() {
        return logger;
    }

}
