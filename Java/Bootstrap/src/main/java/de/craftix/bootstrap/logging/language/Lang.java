package de.craftix.bootstrap.logging.language;

public enum Lang {
    MESSAGE_NOT_FOUND("message-not-found"),
    LOGGER_INITIALIZED("logger-loaded"),
    LANG_MANAGER_INITIALIZED("lang-manager-initialized"),
    LANG_MANAGER_INITIALIZE_LANGUAGES("lang-manager-languages-initialize"),
    LANG_MANAGER_INITIALIZE_LANGUAGE("lang-manager-language-initialize"),
    LANG_MANAGER_TEST_LANGUAGES("lang-manager-languages-test"),
    LANG_MANAGER_TEST_LANGUAGE("lang-manager-language-test"),
    DEFAULT_LANGUAGE("language-default");

    private final String key;
    Lang(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }
}
