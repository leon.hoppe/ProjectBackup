package de.craftix.bootstrap.logging.language;

import de.craftix.bootstrap.Bootstrap;

import java.util.HashMap;
import java.util.Map;

public class LangManager {

    private final Map<String, Language> languages = new HashMap<>();
    private String currentLang;

    public LangManager(String defaultLanguage) {
        currentLang = defaultLanguage;
    }

    public String getMessage(String key, String[] variables) {
        String raw = languages.get(currentLang).getMessage(key);
        for (int i = 0; i < variables.length; i++) {
            raw = raw.replace("{" + i + "}", variables[i]);
        }
        return raw;
    }

    public Language getLanguage(String langKey) {
        return languages.get(langKey);
    }

    public void setLanguage(String langKey) {
        if (!languages.containsKey(langKey))
            throw new NullPointerException("Language [" + langKey + "] not found!");
        currentLang = langKey;
    }

    public void addLanguages(Language... languages) {
        for (Language language : languages) {
            language.setupLanguage();
            if (!language.testLanguage())
                throw new IllegalStateException("Language [" + language.getLangKey() + "] does not implement all Messages");
            this.languages.put(language.getLangKey(), language);
            Bootstrap.getInstance().getLogger().info(Lang.LANG_MANAGER_TEST_LANGUAGE, language.getGlobalName());
            Bootstrap.getInstance().getLogger().info(Lang.LANG_MANAGER_INITIALIZE_LANGUAGE, language.getGlobalName());
        }
        Bootstrap.getInstance().getLogger().info(Lang.LANG_MANAGER_TEST_LANGUAGES);
        Bootstrap.getInstance().getLogger().info(Lang.LANG_MANAGER_INITIALIZE_LANGUAGES);
    }

    public Language getCurrentLanguage() {
        return languages.get(currentLang);
    }

}
