package de.leon.cloud.util;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CloudManager {

    private final File configFile = new File("config.json");

    public void initSetup() {
        System.out.println("Setup wird gestartet...");

        var scanner = new Scanner(System.in);
        int port;
        while (!Thread.currentThread().isInterrupted()) {
            System.out.print("Bitte geb hier den SocketPort ein: ");
            port = scanner.nextInt();

            var gson = new Gson();
            var jsonString = gson.toJson(new Config(port), Config.class);

            try (FileWriter writer = new FileWriter(configFile)) {
                writer.write(jsonString);
                writer.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
