package de.leon.cloud.util;

public class Config {

    private int port;

    public Config(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }
}
