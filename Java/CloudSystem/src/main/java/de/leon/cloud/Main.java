package de.leon.cloud;

import de.leon.cloud.util.CloudManager;

import java.util.Scanner;

public class Main {

    private final CloudManager cloudManager;

    public static void main(String[] args) {
        var main = new Main();
        main.initCloud();
    }

    public Main() {
        cloudManager = new CloudManager();
    }

    public void initCloud() {
        System.out.println("Die Cloud wurde gestartet");
        var scanner = new Scanner(System.in);
        String answer;
        String[] args;

        System.out.println();
        while (!Thread.currentThread().isInterrupted()) {
            answer = scanner.nextLine();
            var array = answer.split(" ");
            var command = array[0];
            args = dropFirstString(array);

            if (command.equalsIgnoreCase("help")) {
                sendHelp();
            }

            else if (command.equalsIgnoreCase("stop")) {
                System.out.println("Stoppe cloud...");
                System.exit(0);
                return;
            }

            else if (command.equalsIgnoreCase("setup")) {
                cloudManager.initSetup();
            }

            else {
                sendHelp();
            }
        }
    }

    private void sendHelp() {
        System.out.println("""
                        Alle Commands der Cloud:
                        help - Zeigt alle Commands der Cloud an
                        stop - Stoppt die Cloud
                        setup - Startet das Setup""");
    }

    private String[] dropFirstString(String[] array) {
        var result = new String[array.length - 1];
        System.arraycopy(array, 1, result, 0, array.length - 1);
        return result;
    }

}
