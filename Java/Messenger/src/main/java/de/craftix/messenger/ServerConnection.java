package de.craftix.messenger;

import de.craftix.server.Packet;
import de.craftix.server.Server;
import sun.misc.Unsafe;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ServerConnection {

    private static ObjectInputStream in;
    private static ObjectOutputStream out;
    private static Thread clientThread;

    public static void connect() {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(Server.IP, Server.PORT), 5000);
            out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            out.flush();
            in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));

            disableWarning();

            System.out.println("Connected to Server");

            startThread();
        }catch (Exception e) { e.printStackTrace(); }
    }

    private static void startThread() {
        clientThread = new Thread(() -> {
            while (!clientThread.isInterrupted()) {
                try {
                    Object o = in.readObject();
                    if (o instanceof Packet) {
                        Packet packet = (Packet) o;
                        Packet output = handlePacket(packet);
                        out.writeObject(output);
                        out.flush();
                    }
                }catch (Exception e) { e.printStackTrace(); }
            }
        });
        clientThread.start();
    }

    private static Packet handlePacket(Packet packet) {
        if (packet.getType() == Packet.PacketType.PING)
            return new Packet(Packet.PacketType.PING, null);

        return new Packet(Packet.PacketType.BOOLEAN, false);
    }

    public static Packet sendRequest(Packet packet) {
        Packet answer = new Packet(Packet.PacketType.BOOLEAN, false);
        try {
            //Kill Thread
            Method m = Thread.class.getDeclaredMethod("stop0", Object.class);
            m.setAccessible(true);
            m.invoke(clientThread, new ThreadDeath());

            out.writeObject(packet);
            out.flush();
            Object o = in.readObject();
            if (o instanceof Packet)
                answer = (Packet) o;
        }catch (Exception e) { e.printStackTrace(); }
        finally { startThread(); }
        return answer;
    }

    private static void disableWarning() {
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            Unsafe u = (Unsafe) theUnsafe.get(null);

            Class<?> cls = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field logger = cls.getDeclaredField("logger");
            u.putObjectVolatile(cls, u.staticFieldOffset(logger), null);
        } catch (Exception ignored) {}
    }
}
