package de.craftix.messenger;

import de.craftix.engine.GameEngine;
import de.craftix.engine.render.Screen;
import de.craftix.messenger.onscreen.ComponentManager;
import de.craftix.server.User;

import java.util.Base64;

public class Messenger extends GameEngine {
    private static User user;

    public static void main(String[] args) {
        Screen.antialiasing(true);
        Screen.showFrames(true);
        Screen.setResizeable(true);
        Screen.setFramesPerSecond(120);
        Screen.setAntialiasingEffectTextures(false);
        setup(800, 600, "Messenger v0.1", new Messenger(), 20);

        //ServerConnection.connect();
    }

    @Override
    public void initialise() {
        ComponentManager.setup();
    }

    public static User getUser() { return user; }

    public static String decode(String msg) {
        return new String(Base64.getDecoder().decode(msg));
    }
    public static String encode(String msg) {
        return Base64.getEncoder().encodeToString(msg.getBytes());
    }
}
