package de.craftix.messenger.onscreen;

import de.craftix.engine.render.Sprite;
import de.craftix.engine.var.*;
import de.craftix.messenger.Messenger;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class ComponentManager {
    private final static Scene mainScene = new Scene();

    public static void setup() {
        mainScene.setBackground(new Sprite(new BufferedImage(800, 600, Image.SCALE_DEFAULT), false), false);

        // TODO: Add Components

        Messenger.setActiveScene(mainScene);
    }

}
