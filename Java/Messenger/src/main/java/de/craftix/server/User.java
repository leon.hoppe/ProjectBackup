package de.craftix.server;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

public class User implements Serializable {
    public static User getUser(UUID uuid) {
        if (!MySQL.isConnected()) throw new NullPointerException("MySQL not connected!");
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Users WHERE UUID = \"" + uuid + "\"");
            assert rs != null;
            if (rs.next()) {
                int id = rs.getInt("ID");
                Rank rank = Rank.getRankByID(rs.getInt("Rank"));
                String username = rs.getString("Username");
                return new User(id, uuid, rank, username);
            }
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }
    public static User getUser(String username) {
        if (!MySQL.isConnected()) throw new NullPointerException("MySQL not connected!");
        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Users WHERE Username = \"" + username + "\"");
            assert rs != null;
            if (rs.next()) {
                int id = rs.getInt("ID");
                UUID uuid = UUID.fromString(rs.getString("UUID"));
                Rank rank = Rank.getRankByID(rs.getInt("Rank"));
                return new User(id, uuid, rank, username);
            }
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }
    public static User[] getUsers() {
        if (!MySQL.isConnected()) throw new NullPointerException("MySQL not connected!");
        ArrayList<User> users = new ArrayList<>();

        try {
            ResultSet rs = MySQL.getData("SELECT * FROM Users");
            assert rs != null;
            if (rs.next()) {
                int id = rs.getInt("ID");
                UUID uuid = UUID.fromString(rs.getString("UUID"));
                Rank rank = Rank.getRankByID(rs.getInt("Rank"));
                String username = rs.getString("Username");
                users.add(new User(id, uuid, rank, username));
            }
        }catch (Exception e) { e.printStackTrace(); }

        return users.toArray(new User[0]);
    }

    protected static User createUser(Rank rank, String username, String password) {
        User[] users = getUsers();

        int id = 0;
        boolean foundID = false;
        while (!foundID) {
            for (User u : users) {
                if (u.id == id) {
                    id++;
                    break;
                }
            }
            foundID = true;
        }

        UUID uuid = UUID.randomUUID();
        boolean foundUUID = false;
        while (!foundUUID) {
            for (User u : users) {
                if (u.uuid.equals(uuid)) {
                    uuid = UUID.randomUUID();
                    break;
                }
            }
            foundUUID = true;
        }

        User user = new User(id, uuid, rank, username);
        MySQL.insert("INSERT INTO Users (Rank, ID, UUID, Username, Password) VALUES (" + rank.getRankID() + ", " + id + ", \"" + uuid + "\", \"" +
                                                                                         username + "\", \"" + password + "\")");

        return user;
    }

    private final int id;
    private final UUID uuid;
    protected Rank rank;
    protected String username;

    private User(int id, UUID uuid, Rank rank, String username) {
        this.id = id;
        this.uuid = uuid;
        this.rank = rank;
        this.username = username;
    }

    protected void saveUser() {
        if (!MySQL.isConnected()) throw new NullPointerException("MySQL not connected!");
        MySQL.insert("UPDATE Users SET Username = \"" + username + "\", Rank = " + rank + " WHERE UUID = \"" + uuid + "\"");
    }

    public int getID() { return id; }
    public UUID getUniqueID() { return uuid; }
    public Rank getRank() { return rank; }
    public String getUsername() { return username; }

    public enum Rank implements Serializable {
        USER(0),
        PREMIUM(1),
        MODERATOR(2),
        ADMINISTRATOR(3);

        private final int rankID;
        Rank(int rank) { this.rankID = rank; }
        public int getRankID() { return rankID; }

        public static Rank getRankByID(int id) {
            for (Rank rank : values()) {
                if (rank.getRankID() == id)
                    return rank;
            }
            return null;
        }
    }
}
