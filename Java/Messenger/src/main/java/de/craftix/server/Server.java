package de.craftix.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Server {
    public static final String IP = "localhost";
    public static final int PORT = 187;

    protected static ServerSocket socket;
    protected static List<ClientHandler> clients;

    public static void main(String[] args) throws Exception {
        MySQL.server = "213.136.89.237";
        MySQL.port = 3306;
        MySQL.database = "Messenger";
        MySQL.username = "Messenger";
        MySQL.password = "v0dn2Ml60AcaX@VN";

        MySQL.connect();
        createMySQLTables();

        startServerLoop();
    }

    private static void createMySQLTables() {
        MySQL.insert("CREATE TABLE IF NOT EXISTS Users (Rank INT(10), ID INT(10), UUID VARCHAR(100), Username VARCHAR(100), Password VARCHAR(100))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS Chats (ID INT(10), Owner VARCHAR(100))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS ChatUser (ChatID INT(10), Member VARCHAR(100))");
        MySQL.insert("CREATE TABLE IF NOT EXISTS Messages (Chat INT(10), Sender VARCHAR(100), Timestamp INT(255), Message VARCHAR(2000))");
    }

    private static void startServerLoop() throws Exception {
        socket = new ServerSocket(PORT);
        clients = new ArrayList<>();

        System.out.println("Server started successfully");

        while (!Thread.currentThread().isInterrupted()) {
            Socket client = socket.accept();
            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
            out.flush();
            ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
            clients.add(new ClientHandler(client, in, out));
        }
    }

    protected static boolean checkPassword(UUID user, String password) {
        try {
            ResultSet rs = MySQL.getData("SELECT Password FROM Users WHERE UUID = \"" + user + "\"");
            assert rs != null;
            if (rs.next()) {
                String truePassword = rs.getString("Password");
                return password.equals(truePassword);
            }
        }catch (Exception e) { e.printStackTrace(); }
        return false;
    }
}
