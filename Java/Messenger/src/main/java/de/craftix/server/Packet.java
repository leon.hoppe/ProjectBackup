package de.craftix.server;

import java.io.Serializable;

public class Packet implements Serializable {

    public enum PacketType {
        PING(0),
        UPDATE(1),
        GET_USER(2),
        LOGIN(3),
        REGISTER(4),
        BOOLEAN(5);

        private final byte id;
        PacketType(int id)
        { this.id = (byte) id; }

        public byte getID()
        { return id; }

        public static PacketType getByID(byte id) {
            for (PacketType type : values()) {
                if (type.getID() == id)
                    return type;
            }
            return PING;
        }
    }

    private final Object value;
    private final byte type;

    public Packet(PacketType type, Object value) {
        this.value = value;
        this.type = type.getID();
    }

    public PacketType getType()
    { return PacketType.getByID(type); }
    public Object getValue()
    { return value; }
}
