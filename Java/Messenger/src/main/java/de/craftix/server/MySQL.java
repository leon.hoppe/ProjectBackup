package de.craftix.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class MySQL {

    protected static String server;
    protected static int port;
    protected static String database;
    protected static String username;
    protected static String password;
    protected static Connection con;

    public static void connect() {
        if (isConnected()) return;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://" + server + ":" + port + "/" + database, username, password);
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("MySQL connected");
    }

    public static void disconnect() {
        if (!isConnected()) return;
        try {
            con.close();
            con = null;
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("MySQL disconnected");
    }

    public static boolean isConnected() {
        return con != null;
    }

    public static void insert(String qry) {
        if (!isConnected()) throw new NullPointerException("MySQL not connected");
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ResultSet getData(String qry) {
        if (!isConnected()) throw new NullPointerException("MySQL not connected");
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
