package de.craftix.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientHandler {
    protected String address;
    protected Socket con;
    protected ObjectInputStream in;
    protected ObjectOutputStream out;
    protected Thread clientThread;
    protected User user;

    protected ClientHandler(Socket con, ObjectInputStream in, ObjectOutputStream out) {
        this.con = con;
        this.in = in;
        this.out = out;
        this.address = con.getInetAddress().getHostAddress() + ":" + con.getPort();

        System.out.println("Client connected [" + address + "]");

        startClientThread();
    }

    private void startClientThread() {
        clientThread = new Thread(() -> {
            while (!clientThread.isInterrupted()) {
                try {
                    Object o = in.readObject();
                    if (o instanceof Packet) {
                        Packet output = handlePacket((Packet) o);
                        sendPacket(output);
                    }
                }catch (Exception e) {
                    disconnect();
                }
            }
        });
        clientThread.start();
    }

    private void disconnect() {
        clientThread.interrupt();
        Server.clients.remove(this);

        System.out.println("Client disconnected [" + address + "]");
    }

    public void sendPacket(Packet packet) {
        try {
            out.writeObject(packet);
            out.flush();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Packet handlePacket(Packet packet) {
        if (packet.getType() == Packet.PacketType.PING)
            return new Packet(Packet.PacketType.PING, null);

        if (packet.getType() == Packet.PacketType.LOGIN) {
            String[] arr = (String[]) packet.getValue();
            String username = arr[0];
            String password = arr[1];
            User user = User.getUser(username);
            assert user != null;
            if (Server.checkPassword(user.getUniqueID(), password)) {
                this.user = user;
                return new Packet(Packet.PacketType.GET_USER, user);
            }
            else
                return new Packet(Packet.PacketType.BOOLEAN, false);
        }

        if (packet.getType() == Packet.PacketType.REGISTER) {
            String[] arr = (String[]) packet.getValue();
            String username = arr[0];
            String password = arr[1];
            User[] users = User.getUsers();
            for (User u : users) {
                if (u.username.equals(username))
                    return new Packet(Packet.PacketType.BOOLEAN, false);
            }
            User.createUser(User.Rank.USER, username, password);
            User user = User.getUser(username);
            this.user = user;
            return new Packet(Packet.PacketType.GET_USER, user);
        }

        return new Packet(Packet.PacketType.BOOLEAN, false);
    }

}
