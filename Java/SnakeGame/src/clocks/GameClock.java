package clocks;

import actions.Collision;
import game.Snake;

public class GameClock extends Thread {

    public static boolean running = true;
    public static int sleep = 150;

    public void start() {
        try {
            while (true) {
                sleep(sleep);
                if (!running) continue;
                Snake.move();
                Snake.waitToMove = false;
                Collision.collidePickUp();
                if (Collision.collideSelf()) {
                    Snake.tails.clear();
                    Snake.score = 0;
                    GameClock.sleep = 150;
                }
                if (Collision.collideWall()) {
                    Snake.tails.clear();
                    Snake.head.setX(7);
                    Snake.head.setY(7);
                    Snake.score = 0;
                    GameClock.sleep = 150;
                }
            }
        }catch (Exception e) { e.printStackTrace(); }
    }

}
