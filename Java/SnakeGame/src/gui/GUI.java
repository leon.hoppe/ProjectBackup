package gui;

import actions.KeyHandler;

import javax.swing.*;

public class GUI {
    JFrame jf;
    Draw d;

    public static int with = 800;
    public static int height = 600;
    public static int xOff = 170;
    public static int yOff = 20;

    public void create() {
        jf = new JFrame("Snake");
        jf.setSize(with, height);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setLocationRelativeTo(null);
        jf.setLayout(null);
        jf.setResizable(false);
        jf.addKeyListener(new KeyHandler());

        d = new Draw();
        d.setBounds(0, 0, with, height);
        d.setVisible(true);
        jf.add(d);

        jf.requestFocus();
        jf.setVisible(true);
    }

}
