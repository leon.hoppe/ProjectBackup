package gui;

import clocks.GameClock;
import game.Snake;

import javax.swing.*;
import java.awt.*;

public class Draw extends JLabel {

    Point p;

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, GUI.with, GUI.height);

        g.setColor(new Color(51, 204, 51));
        for (int i = 0; i < Snake.tails.size(); i++) {
            p = Snake.ptc(Snake.tails.get(i).getX(), Snake.tails.get(i).getY());
            g.fillRect(p.x, p.y, 32, 32);
        }

        g.setColor(new Color(0, 153, 0));
        p = Snake.ptc(Snake.head.getX(), Snake.head.getY());
        g.fillRect(p.x, p.y, 32, 32);

        g.setColor(new Color(204, 51, 0));
        p = Snake.ptc(Snake.pickUp.getX(), Snake.pickUp.getY());
        g.fillRect(p.x, p.y, 32, 32);

        g.setColor(Color.GRAY);
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                g.drawRect(x * 32 + GUI.xOff, y * 32 + GUI.yOff, 32, 32);
            }
        }

        g.setColor(Color.BLACK);
        g.drawRect(GUI.xOff, GUI.yOff, 512, 512);

        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.drawString("Score: " + Snake.score, 5, 25);
        g.drawString("Highscore: " + Snake.highscore, 5, 50);

        if (!GameClock.running) {
            g.drawString("Game Paused...", 5, 75);
        }

        repaint();
    }

}
