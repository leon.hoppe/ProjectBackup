package actions;

import clocks.GameClock;
import game.Snake;
import gui.GUI;

public class Main {

    public static void main(String[] args) {
        GUI gui = new GUI();
        GameClock gc = new GameClock();
        Snake.highscore = Snake.getHighscore();

        gui.create();
        gc.start();
    }

}
