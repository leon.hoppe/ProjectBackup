package game;

import gui.GUI;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;

public class Snake {

    public static Head head = new Head(7, 7);
    public static ArrayList<Tail> tails = new ArrayList<>();

    public static boolean waitToMove = false;

    public static PickUp pickUp = new PickUp();

    public static int score = 0;
    public static int highscore = 0;

    public static void addTail() {
        if (tails.size() < 1) {
            tails.add(new Tail(head.getX(), head.getY()));
        }else {
            tails.add(new Tail(tails.get(tails.size() - 1).getX(), tails.get(tails.size() - 1).getY()));
        }
    }

    public static void move() {
        if (tails.size() >= 2) {
            for (int i = tails.size() - 1; i >= 1; i--) {
                if (tails.get(i).isWait()) {
                    tails.get(i).setWait(false);
                }else {
                    tails.get(i).setX(tails.get(i - 1).getX());
                    tails.get(i).setY(tails.get(i - 1).getY());
                }
            }
        }

        if (tails.size() >= 1) {
            if (tails.get(0).isWait()) {
                tails.get(0).setWait(false);
            }else {
                tails.get(0).setX(head.getX());
                tails.get(0).setY(head.getY());
            }
        }

        switch (head.getDir()) {
            case RIGHT:
                head.setX(head.getX() + 1);
                break;
            case LEFT:
                head.setX(head.getX() - 1);
                break;
            case UP:
                head.setY(head.getY() - 1);
                break;
            case DOWN:
                head.setY(head.getY() + 1);
                break;
        }
    }

    public static Point ptc(int x, int y) {
        Point p = new Point(0, 0);
        p.x = x * 32 + GUI.xOff;
        p.y = y * 32 + GUI.yOff;
        return p;
    }

    public static void updateHighscore(int value) {
        try {
            highscore = value;
            File file = new File("data.save");
            file.delete();
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(String.valueOf(highscore));
            writer.close();
        }catch (IOException e) { e.printStackTrace(); }
    }

    public static int getHighscore() {
        try {
            File file = new File("data.save");
            if (!file.exists()) {
                file.createNewFile();
                return 0;
            }
            BufferedReader reader = new BufferedReader(new FileReader(file));
            int output = Integer.parseInt(reader.readLine());
            reader.close();
            return  output;
        }catch (IOException e) { e.printStackTrace(); }
        return 0;
    }

}
