package game;

import java.util.concurrent.ThreadLocalRandom;

public class PickUp {

    private int x;
    private int y;

    public PickUp() {
        this.x = ThreadLocalRandom.current().nextInt(0, 15);
        this.y = ThreadLocalRandom.current().nextInt(0, 15);
        System.out.println("Spawned Fruit at " + this.x + ":" + this.y);
    }

    public void reset() {
        this.x = ThreadLocalRandom.current().nextInt(0, 15);
        this.y = ThreadLocalRandom.current().nextInt(0, 15);
        System.out.println("Spawned Fruit at " + this.x + ":" + this.y);
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
