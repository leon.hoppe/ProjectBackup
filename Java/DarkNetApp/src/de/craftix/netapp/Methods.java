package de.craftix.netapp;

import java.util.Random;

public class Methods {

    public static void sleep(int time) { try { Thread.sleep(time); } catch (Exception ignored) {} }

    public static String encrypt(String msg) {
        long time = System.currentTimeMillis();
        Random rand = new Random(time);
        char[] chars = msg.toCharArray();
        StringBuilder out = new StringBuilder(time + " - ");
        for (int i = 0; i < chars.length; i++) out.append(chars[i] += rand.nextInt());
        return out.toString();
    }

    public static String decrypt(String msg) {
        String[] seq = msg.split(" - ");
        Random rand = new Random(Long.parseLong(seq[0]));
        char[] chars = seq[1].toCharArray();
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < chars.length; i++) out.append(chars[i] -= rand.nextInt());
        return out.toString();
    }

    public static void clearConsole() {
        for (int i = 0; i < 1000; i++) System.out.println(" ");
    }

}
