package de.craftix.netapp;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Chat implements Serializable {

    private final ArrayList<Message> messages = new ArrayList<>();
    private final String serverIP;
    private final String name;

    public Chat(String serverIP, String name) { this.serverIP = Methods.encrypt(serverIP); this.name = Methods.encrypt(name); }

    public void updateChat() {
        Methods.clearConsole();
        System.out.println("---------------------------" + getName() + "---------------------------");
        System.out.println("Server: " + getServerIP());
        for (Message message : messages) {
            String time = DateTimeFormatter.ofPattern("HH:mm:ss").format(new Timestamp(message.getTime()).toLocalDateTime());
            System.out.println("[" + time + "] - " + message.getSender().getUser() + ": " + message.getText() + "\n");
        }
    }

    public void addMessage(Message message) { messages.add(message); }
    public void removeMessage(Message message) { messages.add(message); }
    public Message[] getMessages() { return messages.toArray(new Message[0]); }

    public String getServerIP() { return Methods.decrypt(serverIP); }
    public String getName() { return Methods.decrypt(name); }

}
