package de.craftix.netapp;

import java.io.Serializable;

public class User implements Serializable {

    private String username;
    private String password;
    private String adminPass;
    public boolean admin = false;

    public User() {}
    public User(String username, String password) { setUser(username); setPass(password); }
    public User(String username, String password, boolean admin) { setUser(username); setPass(password); this.admin = admin; }
    public User(String username, String password, String adminPass, boolean admin) { setUser(username); setPass(password); setAdmin(adminPass); this.admin = admin; }

    public void setUser(String user) { this.username = Methods.encrypt(user); }
    public void setPass(String pass) { this.password = Methods.encrypt(pass); }
    public void setAdmin(String admin) { this.adminPass = Methods.encrypt(admin); }

    public String getUser() { return Methods.decrypt(username); }
    public String getPass() { return Methods.decrypt(password); }
    public String getAdmin() { return Methods.decrypt(adminPass); }

}
