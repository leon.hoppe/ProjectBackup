package de.craftix.netapp.client;

import de.craftix.netapp.*;
import de.craftix.netapp.client.commands.*;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;
import de.craftix.netapp.server.Server;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Client implements Runnable {
    public static String serverAddress;
    public static User user;
    public static Chat currentChat;

    private static final List<Command> commands = new ArrayList<>();
    private static Logger commandLog;
    private static Thread commandThread;
    private static Thread updateThread;

    public static void main(String[] args) {
        Logger.globalInfo("Starting Client...");
        registerCommands();
        commandLog = new Logger("CommandThread");
        commandThread = new Thread(new Client());
        updateThread = new Thread(getUpdateRunnable());

        commandThread.start();
        Logger.globalInfo("Client started");
    }

    private static void registerCommands() {
        addCommand(new StopCmd("Stop", "Shutdown", "End", "Close"));
        addCommand(new SendPacketCmd("SendPacket", "Send", "Packet"));
        addCommand(new ConnectCmd("Connect", "Server"));
        addCommand(new RegisterCmd("Register"));
        addCommand(new LoginCmd("Login"));
        addCommand(new CreateAdminCmd("CreateAdmin", "cadmin"));
        addCommand(new JoinChatCmd("JoinChat", "join", "chat"));
        addCommand(new CreateChat("CreateChat", "create"));
    }

    public static Packet sendObjectToServer(Object data, String ip) {
        try {
            Socket server = new Socket();
            server.connect(new InetSocketAddress(ip, Server.getPort()), 500);
            ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(server.getOutputStream()));
            stream.writeObject(data);
            stream.flush();
            ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(server.getInputStream()));
            Packet out = (Packet) input.readObject();
            input.close();
            stream.close();
            server.close();
            return out;
        }catch (Exception e) { new Logger("Server Connection").error(e.getMessage()); }
        return null;
    }

    public static void stop() {
        commandThread.interrupt();
        updateThread.interrupt();
        System.exit(0);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String qry = reader.readLine();
                handleCommand(qry);
            }catch (Exception e) { commandLog.error(e.getMessage()); }
        }
    }

    private static void addCommand(Command cmd) { commands.add(cmd); }
    private static void handleCommand(String qry) {
        if (currentChat == null) {
            String[] raw = qry.split(" ");
            String cmd = raw[0];
            String[] args = new String[raw.length - 1];
            for (int i = 1; i < raw.length; i++) args[i - 1] = raw[i];
            boolean success = false;
            for (Command c : commands) {
                if (c.getName().equalsIgnoreCase(cmd)) {
                    c.onCommand(cmd, args);
                    success = true;
                }
                if (c.getAlias() == null || c.getAlias().length == 0) continue;
                for (String alias : c.getAlias()) if (alias.equalsIgnoreCase(cmd)) {
                    c.onCommand(cmd, args);
                    success = true;
                }
            }
            if (!success) commandLog.error("This command does not exists");
        }else {
            if (qry.startsWith("/")) {
                qry = qry.replace("/", "");
                String[] raw = qry.split(" ");
                String cmd = raw[0];
                String[] args = new String[raw.length - 1];
                for (int i = 1; i < raw.length; i++) args[i - 1] = raw[i];
                boolean success = false;
                for (Command c : commands) {
                    if (c.getName().equalsIgnoreCase(cmd)) {
                        c.onCommand(cmd, args);
                        success = true;
                    }
                    if (c.getAlias() == null || c.getAlias().length == 0) continue;
                    for (String alias : c.getAlias()) if (alias.equalsIgnoreCase(cmd)) {
                        c.onCommand(cmd, args);
                        success = true;
                    }
                }
                if (!success) commandLog.error("This command does not exists");
            }else {
                Packet answer = sendObjectToServer(new Packet(new Message(user, qry), currentChat.getName()), serverAddress);
                assert answer != null;
                if (!answer.value) Logger.globalError("Something went wrong");
            }
        }
    }
    private static Runnable getUpdateRunnable() {
        return () -> {
            while (!Thread.currentThread().isInterrupted()) {
                Methods.sleep(1000);
                if (user != null && currentChat != null) {
                    Packet answer = sendObjectToServer(new Packet(currentChat, user, PacketType.CHATUPDATE), serverAddress);
                    assert answer != null;
                    if (answer.chat.getMessages().length != currentChat.getMessages().length && answer.value) {
                        currentChat = answer.chat;
                        currentChat.updateChat();
                    }
                }
            }
        };
    }

    public static Thread getCommandThread() { return commandThread; }
    public static Thread getUpdateThread() { return updateThread; }
}
