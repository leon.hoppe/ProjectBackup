package de.craftix.netapp.client.commands;

import de.craftix.netapp.Chat;
import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;

public class CreateChat extends Command {
    public CreateChat(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (Client.user != null && Client.user.admin) {

            if (args.length > 0) {
                Packet answer = Client.sendObjectToServer(new Packet(new Chat(Client.serverAddress, args[0]), Client.user), Client.serverAddress);
                assert answer != null;
                if (answer.value) log.info("Chat created successfully");
                else log.error("Chat could not be created");
            }else log.warning("Usage: createchat [name]");

        }else log.warning("You don't have the permission to do that");
    }
}
