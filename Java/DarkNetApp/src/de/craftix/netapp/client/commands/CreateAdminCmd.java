package de.craftix.netapp.client.commands;

import de.craftix.netapp.User;
import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;

public class CreateAdminCmd extends Command {
    public CreateAdminCmd(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (args.length < 3) log.error("Usage: createadmin [username] [password] [adminpassword]");
        else if (Client.serverAddress != null) {
            log.info("Sending Register request...");
            Packet answer = Client.sendObjectToServer(new Packet(PacketType.REGISTER, new User(args[0], args[1], args[2], true)), Client.serverAddress);
            assert answer != null;
            if (answer.value) log.info("Account successfully registered");
            else log.error("Registration failed");
        }else log.warning("No server set");
    }
}
