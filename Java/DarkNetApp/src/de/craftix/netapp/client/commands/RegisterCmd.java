package de.craftix.netapp.client.commands;

import de.craftix.netapp.User;
import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;

public class RegisterCmd extends Command {
    public RegisterCmd(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (args.length > 2) log.warning("Usage: register [user] [pass]");
        else if (Client.serverAddress != null) {
            log.info("Sending Register request...");
            Packet answer = Client.sendObjectToServer(new Packet(PacketType.REGISTER, new User(args[0], args[1])), Client.serverAddress);
            assert answer != null;
            if (answer.value) log.info("Register successfully, you can login now");
            else log.error("Register failed");
        }else log.warning("No server set");
    }
}
