package de.craftix.netapp.client.commands;

import de.craftix.netapp.User;
import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;

public class LoginCmd extends Command {
    public LoginCmd(String name, String... alias) { super(name, alias); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (args.length > 2) log.warning("Usage: login [user] [pass]");
        else if (Client.serverAddress != null) {
            log.info("Sending Login request...");
            Packet answer = Client.sendObjectToServer(new Packet(PacketType.LOGIN, new User(args[0], args[1])), Client.serverAddress);
            assert answer != null;
            if (answer.value) {
                Client.user = answer.user;
                log.info("Login successfully");
            }
            else log.error("Login failed");
        }else log.warning("No server set");
    }
}
