package de.craftix.netapp.client.commands;

import de.craftix.netapp.Chat;
import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;

public class JoinChatCmd extends Command {
    public JoinChatCmd(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (Client.serverAddress == null) log.warning("No Server set!");
        else if (Client.user == null) log.warning("You aren't logged in!");
        else {
            if (args.length > 0) {

                Packet answer = Client.sendObjectToServer(new Packet(new Chat(Client.serverAddress, args[0])), Client.serverAddress);
                assert answer != null;
                if (answer.value) {
                    Client.currentChat = answer.chat;
                    Client.currentChat.updateChat();
                    Client.getUpdateThread().start();
                }else log.error("That room doesn't exists");

            }else log.warning("Usage: joinchat [name]");
        }
    }
}
