package de.craftix.netapp.client.commands;

import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;

public class SendPacketCmd extends Command {
    public SendPacketCmd(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (args.length < 1) { log.warning("You must say, what type of Packet you want to send"); return; }
        if (Client.serverAddress == null) { log.warning("No Server selected"); return; }

        if (args[0].equalsIgnoreCase("PING")) {
            log.info("Sending Packet...");
            Client.sendObjectToServer(new Packet(PacketType.PING), Client.serverAddress);
        }
    }
}
