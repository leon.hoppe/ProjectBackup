package de.craftix.netapp.client.commands;

import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;

public class StopCmd extends Command {
    public StopCmd(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        log.info("Initialising stop...");
        Client.stop();
    }
}
