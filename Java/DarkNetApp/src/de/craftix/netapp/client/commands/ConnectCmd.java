package de.craftix.netapp.client.commands;

import de.craftix.netapp.client.Client;
import de.craftix.netapp.client.Command;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;

public class ConnectCmd extends Command {
    public ConnectCmd(String name, String... args) { super(name, args); }

    @Override
    public void onCommand(String cmd, String[] args) {
        if (args.length < 1) log.warning("Usage: connect [ip]");
        Client.user = null;
        Packet answer = Client.sendObjectToServer(new Packet(PacketType.PING), args[0]);
        if (answer == null) log.error("Server Not Available");
        else {
            Client.serverAddress = args[0];
            log.info("Server successfully set");
        }
    }
}
