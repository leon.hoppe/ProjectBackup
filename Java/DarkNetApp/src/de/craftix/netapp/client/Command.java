package de.craftix.netapp.client;

import de.craftix.netapp.Logger;

public abstract class Command {

    protected final String name;
    protected final String[] alias;
    protected final Logger log;

    public String getName() { return name; }
    public String[] getAlias() { return alias; }

    public Command(String name, String... alias) {
        this.name = name;
        this.alias = alias;
        this.log = new Logger("Command_" + name);
    }

    public abstract void onCommand(String cmd, String[] args);

}
