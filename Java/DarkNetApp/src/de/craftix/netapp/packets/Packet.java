package de.craftix.netapp.packets;

import de.craftix.netapp.Chat;
import de.craftix.netapp.Message;
import de.craftix.netapp.User;

import java.io.Serializable;

public class Packet implements Serializable {

    public PacketType type;
    public boolean value;

    public Packet() { this.type = PacketType.BOOLEAN; }
    public Packet(Boolean value) { super(); this.value = value; }
    public Packet(PacketType type) { this.type = type; }

    //LOGIN - REGISTER
    public User user;
    public Packet(PacketType type, User user) {
        this.type = type;
        this.user = user;
    }
    public Packet(User user, boolean value) { this(value); this.user = user; }

    //CHAT
    public String chatName;
    public Chat chat;
    public Message message;
    public Packet(Message message, String chatName) { this.message = message; this.chatName = chatName; this.type = PacketType.CHATMESSAGE; }
    public Packet(Chat chat) { this.chat = chat; type = PacketType.CHATCONNECT; }
    public Packet(Chat chat, User user) { this.chat = chat; type = PacketType.CHATCREATE; this.user = user; }
    public Packet(Chat chat, boolean value) { this(value); this.chat = chat; }
    public Packet(Chat chat, User user, PacketType type) { this.chat = chat; this.user = user; this.type = type; }

}
