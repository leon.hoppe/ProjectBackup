package de.craftix.netapp.packets;

public enum PacketType {

    REGISTER,
    LOGIN,
    CHATCONNECT,
    CHATCREATE,
    CHATMESSAGE,
    CHATUPDATE,
    BOOLEAN,
    PING;

}
