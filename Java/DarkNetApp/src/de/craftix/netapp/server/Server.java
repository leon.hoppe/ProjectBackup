package de.craftix.netapp.server;

import de.craftix.netapp.Chat;
import de.craftix.netapp.Logger;
import de.craftix.netapp.packets.Packet;
import de.craftix.netapp.packets.PacketType;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    private static Thread serverThread;
    private static Logger log;
    private static ServerSocket server;
    private static final int port = 187;
    private static final String adminPassword = "Admin";

    public static void main(String[] args) throws IOException {
        log = new Logger("ServerThread");
        log.info("Starting Server...");
        FileManager.setup();
        server = new ServerSocket(port);
        serverThread = new Thread(new Server());

        serverThread.start();
    }

    @Override
    public void run() {
        log.info("Server started");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Socket client = server.accept();
                log.info("Client connected [" + client.getRemoteSocketAddress() + "]");
                ObjectInputStream stream = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
                Object data = stream.readObject();
                Object answer = onConnection(client, data);
                if (answer != null) {
                    ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
                    out.writeObject(answer);
                    out.flush();
                    out.close();
                }
                stream.close();
                client.close();
            }catch (Exception e) { log.error("Something went wrong while server is running: " + e.getMessage()); }
        }
    }

    private static Object onConnection(Socket client, Object data) {
        if (!(data instanceof Packet)) return null;
        Packet packet = (Packet) data;

        if (packet.type.equals(PacketType.PING)) return new Packet(PacketType.PING);

        if (packet.type.equals(PacketType.REGISTER)) {
            assert packet.user != null;
            if (packet.user.admin) {
                if (FileManager.userExists(packet.user.getUser())) return new Packet(false);
                if (!adminPassword.equals(packet.user.getAdmin())) return new Packet(false);
                FileManager.saveUser(packet.user);
                return new Packet(true);
            }else {
                if (FileManager.userExists(packet.user.getUser())) return new Packet(false);
                FileManager.saveUser(packet.user);
                return new Packet(true);
            }
        }

        if (packet.type.equals(PacketType.LOGIN)) {
            assert  packet.user != null;
            if (!FileManager.userExists(packet.user.getUser())) return new Packet(false);
            if (FileManager.getUser(packet.user.getUser()).getPass().equals(packet.user.getPass())) return new Packet(FileManager.getUser(packet.user.getUser()), true);
            else return new Packet(false);
        }

        if (packet.type.equals(PacketType.CHATCONNECT)) {
            assert packet.chat != null;
            if (FileManager.chatExists(packet.chat.getName())) return new Packet(FileManager.getChat(packet.chat.getName()), true);
            else return new Packet(false);
        }

        if (packet.type.equals(PacketType.CHATCREATE)) {
            assert packet.chat != null;
            if (FileManager.chatExists(packet.chat.getName())) return new Packet(false);
            if (!packet.user.admin) return new Packet(false);
            FileManager.saveChat(packet.chat);
            return new Packet(true);
        }

        if (packet.type.equals(PacketType.CHATUPDATE)) {
            assert packet.chat != null;
            return new Packet(FileManager.getChat(packet.chat.getName()), true);
        }

        if (packet.type.equals(PacketType.CHATMESSAGE)) {
            assert packet.chat != null;
            Chat chat = FileManager.getChat(packet.chatName);
            if (chat == null) return new Packet(false);
            chat.addMessage(packet.message);
            FileManager.saveChat(chat);
            return new Packet(true);
        }

        return null;
    }

    public static Thread getServerThread() { return serverThread; }
    public static int getPort() { return port; }
}
