package de.craftix.netapp.server;

import de.craftix.netapp.Chat;
import de.craftix.netapp.User;

import java.io.*;

public class FileManager {

    private static final String uPrefix = "./users/";
    private static final String cPrefix = "./chats/";

    public static void setup() {
        new File(uPrefix).mkdirs();
        new File(cPrefix).mkdirs();
    }

    public static boolean userExists(String name) {
        return new File(uPrefix + name + ".user").exists();
    }
    public static User getUser(String name) {
        return (User) ReadObjectFromFile(uPrefix + name + ".user");
    }
    public static void saveUser(User user) {
        File file = new File(uPrefix + user.getUser() + ".user");
        if (file.exists()) file.delete();
        WriteObjectToFile(user, uPrefix + user.getUser() + ".user");
    }

    public static boolean chatExists(String name) { return new File(cPrefix + name + ".chat").exists(); }
    public static Chat getChat(String name) { return (Chat) ReadObjectFromFile(cPrefix + name + ".chat"); }
    public static void saveChat(Chat chat) {
        File file = new File(cPrefix + chat.getName() + ".chat");
        if (file.exists()) file.delete();
        WriteObjectToFile(chat, cPrefix + chat.getName() + ".chat");
    }

    private static void WriteObjectToFile(Object serObj, String filepath) {

        try {

            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();
            //System.out.println("[Arena] The Object  was succesfully written to a file");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static Object ReadObjectFromFile(String filepath) {

        try {

            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Object obj = objectIn.readObject();

            //System.out.println("[Arena] The Object has been read from the file");
            objectIn.close();
            return obj;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
