package de.craftix.netapp;

import java.io.Serializable;

public class Message implements Serializable {

    private User sender;
    private String text;
    private long time;

    public Message(User sender, String text) { setTime(System.currentTimeMillis()); setSender(sender); setText(text); }

    public void setSender(User sender) { this.sender = sender; }
    public void setText(String text) { this.text = Methods.encrypt(text); }
    public void setTime(long time) { this.time = time; }

    public User getSender() { return sender; }
    public String getText() { return Methods.decrypt(text); }
    public long getTime() { return time; }

}
