package de.princep.ts3.bot;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import de.princep.ts3.bot.events.Event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Load {

    public static final TS3Config config = new TS3Config();
    public static final TS3Query query = new TS3Query(config);
    public static final TS3Api api = query.getApi();
    public static Channel supQuery;

    public static void main(String[] args) throws IOException {
        config.setHost("161.97.88.49");
        config.setFloodRate(TS3Query.FloodRate.UNLIMITED);
        config.setEnableCommunicationsLogging(true);
        query.connect();
        api.login("Bot", "B8DLtWov");
        api.selectVirtualServerByPort(9987);
        Event.loadEvents();
        supQuery = api.getChannelByNameExact("╔ Support Warteraum", true);
        System.out.println("Bot ping: " + api.getConnectionInfo().getPing());
        System.out.println("Bot started successfully");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String message = br.readLine();
            if (message.equalsIgnoreCase("stop")) {
                api.logout();
                query.exit();
                return;
            }
        }

    }

}
