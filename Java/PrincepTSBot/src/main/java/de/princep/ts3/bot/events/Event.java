package de.princep.ts3.bot.events;

import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import com.sun.security.ntlm.Server;
import de.princep.ts3.bot.Load;

import java.util.List;

public class Event {

    public static void loadEvents() {
        Load.api.registerAllEvents();
        Load.api.addTS3Listeners(new TS3Listener() {
            public void onTextMessage(TextMessageEvent event) {

            }

            public void onClientJoin(ClientJoinEvent event) {

            }

            public void onClientLeave(ClientLeaveEvent event) {

            }

            public void onServerEdit(ServerEditedEvent event) {

            }

            public void onChannelEdit(ChannelEditedEvent event) {

            }

            public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent event) {

            }

            public void onClientMoved(ClientMovedEvent event) {
                if (Load.api.getChannelByNameExact("╔ Support Warteraum", true).getId() == event.getTargetChannelId()) {
                    Client c = Load.api.getClientInfo(event.getClientId());
                    ServerGroup activeSup = null;
                    for (ServerGroup all : Load.api.getServerGroups()) if (all.getName().equalsIgnoreCase("Aktiver Support")) activeSup = all;
                    if (c.isInServerGroup(activeSup)) return;
                    for (Client all : Load.api.getClients()) {
                        if (all.isInServerGroup(activeSup)) {
                            Load.api.pokeClient(all.getId(), "Ein Spieler ist im Support Warteraum");
                        }
                    }
                    System.out.println("Player joined ╔ Support Warteraum");
                }

            }

            public void onChannelCreate(ChannelCreateEvent event) {

            }

            public void onChannelDeleted(ChannelDeletedEvent event) {

            }

            public void onChannelMoved(ChannelMovedEvent event) {

            }

            public void onChannelPasswordChanged(ChannelPasswordChangedEvent event) {

            }

            public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent event) {

            }
        });
    }

}
