package de.craftix.server;

import de.craftix.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class MySQL {

    public String server;
    public int port = 3306;
    public String database;
    public String username;
    public String password;

    protected Connection con;
    protected Logger log;

    public MySQL(String server, String database, String username, String password) {
        this.server = server;
        this.database = database;
        this.username = username;
        this.password = password;
        log = new Logger("MySQL");
    }

    public MySQL(String server, int port, String database, String username, String password) {
        this.server = server;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        log = new Logger("MySQL");
    }

    public boolean connect() {
        if (con != null) return true;
        String connection = "jdbc:mysql://" + server + ":" + port + "/" + database;
        try {
            con = DriverManager.getConnection(connection, username, password);
            log.info("Connected successfully");
            return true;
        }catch (Exception e) { e.printStackTrace(); }
        return false;
    }

    public boolean disconnect() {
        if (!isConnected()) return true;
        try {
            con.close();
            con = null;
            log.info("Disconnected successfully");
            return true;
        }catch (Exception e) { e.printStackTrace(); }
        return false;
    }

    public boolean isConnected() {
        return con != null;
    }

    public void insert(String qry) {
        try {
            con.prepareStatement(qry).executeUpdate();
        }catch (Exception e) { e.printStackTrace(); }
    }

    public ResultSet getData(String qry) {
        try {
            return con.prepareStatement(qry).executeQuery();
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

}
