package de.craftix.server;

import de.craftix.Logger;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    protected String ip;
    protected int port;
    protected Logger log;
    protected ServerSocket server;

    public Server(int port) {
        try {
            this.port = port;
            this.ip = Inet4Address.getLocalHost().getHostAddress();
            this.log = new Logger("Server");
            server = new ServerSocket(port);
            log.info("Server started on " + ip + ":" + port);
        }catch (Exception e) { e.printStackTrace(); }
    }

    public void start() {
        if (server == null) return;
        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    log.info("Awaiting connection...");
                    Socket client = server.accept();
                    log.info("Client connected [" + client.getRemoteSocketAddress() + "]");
                    ObjectInputStream stream = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
                    Object data = stream.readObject();
                    Object answer = onConnection(client, data);
                    if (answer != null) {
                        ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
                        out.writeObject(answer);
                        out.flush();
                        out.close();
                    }
                    log.info("Data processed");
                    stream.close();
                    client.close();
                    log.info("Task completed");
                } catch (Exception e) { e.printStackTrace(); }
            }
        }).start();
    }

    public Object sendDataToClient(Object data, boolean answer, InetSocketAddress client) {
        try {
            log.info("Connecting to Client... [" + client.getHostName() + ":" + client.getPort() + "]");
            Socket server = new Socket();
            server.connect(client, 10000);
            log.info("Client connected");

            log.info("Sending data...");
            ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(server.getOutputStream()));
            stream.writeObject(data);
            stream.flush();
            log.info("Data send");

            Object out = null;
            if (answer) {
                ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(server.getInputStream()));
                out = input.readObject();
                input.close();
                log.info("Data received");
            }

            stream.close();
            server.close();
            log.info("Task completed");

            return out;
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    protected Object onConnection(Socket client, Object data) { return null; }

}
