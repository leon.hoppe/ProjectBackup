package de.craftix.client;

import de.craftix.server.MySQL;

import java.sql.ResultSet;

public class SqlHelper {

    public static class Data {
        public String type;
        public String name;
        public int length;

        public Data(String name, String type, int length) {
            this.name = name;
            this.type = type;
            this.length = length;
        }

        public Data(String name, Type type, int length) {
            this.name = name;
            this.type = type.name;
            this.length = length;
        }
    }

    public enum Type {
        NUMBER("INT"),
        TEXT("VARCHAR");

        public final String name;
        Type(String name) { this.name = name; }
    }

    public static void createTable(MySQL con, String name, Data... variables) {
        String qry = "CREATE TABLE IF NOT EXISTS " + name + " (";
        for (Data var : variables) {
            if (variables[variables.length - 1].equals(var)) qry += var.name + " " + var.type + "(" + var.length + "))";
            else qry += var.name + " " + var.type + "(" + var.length + "), ";
        }
        con.insert(qry);
    }

    public static ResultSet getDataset(MySQL con, String db) {
        return con.getData("SELECT * FROM " + db);
    }

    public static ResultSet getDataset(MySQL con, String db, String var, Object value) {
        if (value instanceof String) return con.getData("SELECT * FROM " + db + " WHERE " + var + " = \"" + value + "\"");
        return con.getData("SELECT * FROM " + db + " WHERE " + var + " = " + value.toString());
    }

}
