package de.craftix.client;

import de.craftix.Logger;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Client {
    protected InetSocketAddress serverAddress;
    protected InetSocketAddress clientAddress;
    protected Logger log;
    protected ServerSocket clientServer;

    public Client(String server, int port, int localPort) {
        try {
            serverAddress = new InetSocketAddress(server, port);
            clientAddress = new InetSocketAddress(Inet4Address.getLocalHost().getHostAddress(), localPort);
            clientServer = new ServerSocket(localPort);
            log = new Logger("Client");
            startServerThread();
        }catch (Exception e) { e.printStackTrace(); }
    }

    public Object sendObject(Object data, boolean answer) {
        if (serverAddress == null) return null;
        try {
            log.info("Connecting to Server... [" + serverAddress.getHostName() + ":" + serverAddress.getPort() + "]");
            Socket server = new Socket();
            server.connect(serverAddress, 10000);
            log.info("Server connected");

            log.info("Sending data...");
            ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(server.getOutputStream()));
            stream.writeObject(data);
            stream.flush();
            log.info("Data send");

            Object out = null;
            if (answer) {
                ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(server.getInputStream()));
                out = input.readObject();
                input.close();
                log.info("Data received");
            }

            stream.close();
            server.close();
            log.info("Task completed");

            return out;
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    private void startServerThread() {
        if (clientServer == null) return;
        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    log.info("Awaiting Server connection...");
                    Socket client = clientServer.accept();
                    log.info("Server connected [" + client.getRemoteSocketAddress() + "]");
                    ObjectInputStream stream = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
                    Object data = stream.readObject();
                    Object answer = onServerMessage(client, data);
                    if (answer != null) {
                        ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
                        out.writeObject(answer);
                        out.flush();
                        out.close();
                    }
                    log.info("Data processed");
                    stream.close();
                    client.close();
                    log.info("Task completed");
                } catch (Exception e) { e.printStackTrace(); }
            }
        }).start();
    }

    protected Object onServerMessage(Socket server, Object data) { return null; }

}
