package de.craftix;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    protected String applicationName;

    public Logger(String applicationName) {
        this.applicationName = applicationName;
    }

    public void info(String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println("[" + dtf.format(now) + "] [INFO] [" + applicationName + "] " + message);
    }

    public void warning(String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println("\u001B[33m" + "[" + dtf.format(now) + "] [WARNING] [" + applicationName + "] " + message);
    }

    public void error(String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.err.println("[" + dtf.format(now) + "] [ERROR] [" + applicationName + "] " + message);
    }

    public static void globalInfo(String message) { new Logger("Global").info(message); }
    public static void globalWarning(String message) { new Logger("Global").warning(message); }
    public static void globalError(String message) { new Logger("Global").error(message); }

}
