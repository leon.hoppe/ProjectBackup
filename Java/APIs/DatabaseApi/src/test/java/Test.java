import de.craftix.api.database.Table;
import de.craftix.api.database.Column;
import de.craftix.api.database.driver.Database;
import de.craftix.api.database.driver.SqLite;
import de.craftix.api.database.types.Varchar;

public class Test {

    public static void main(String[] args) {
        Database database = new SqLite("test/test.db");
        database.create("test",
                new Column("Name", Varchar.class, 20),
                new Column("City", Varchar.class, 50)
        );
        Table table = database.getTable("test");
        table.add("Leon", "Lügde");
    }

}
