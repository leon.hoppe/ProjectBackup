package de.craftix.api.database;

import de.craftix.api.database.types.VariableType;

public class Column {

    public final String name;
    public VariableType type;
    public final int size;

    public Column(String name, Class<? extends  VariableType> type, int size) {
        this.size = size;
        this.name = name;

        try {
            this.type = type.newInstance();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
