package de.craftix.api.database.types;

public interface VariableType {
    String getName();
}
