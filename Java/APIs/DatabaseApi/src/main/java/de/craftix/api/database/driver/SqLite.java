package de.craftix.api.database.driver;

import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;

public class SqLite extends Database {
    public final String path;

    public SqLite(String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.path = file.getAbsolutePath();

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + path);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
