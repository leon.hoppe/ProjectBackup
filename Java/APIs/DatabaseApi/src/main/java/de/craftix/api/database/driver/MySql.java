package de.craftix.api.database.driver;

import java.sql.DriverManager;

public class MySql extends Database {
    public final String host;
    public final int port;
    public final String database;
    public final String username;
    public final String password;

    public MySql(String host, int port, String database, String username, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
