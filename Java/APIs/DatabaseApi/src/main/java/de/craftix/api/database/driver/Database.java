package de.craftix.api.database.driver;

import de.craftix.api.database.Table;
import de.craftix.api.database.Column;
import de.craftix.api.database.types.Varchar;
import de.craftix.api.database.types.VariableType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

public abstract class Database {

    protected Connection connection;

    protected Database() {}

    public boolean isConnected() { return connection != null; }

    public void disconnect() {
        try {
            connection.close();
            connection = null;
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update(String qry) {
        if (!isConnected()) throw new NullPointerException("Database not connected");
        try {
            connection.prepareStatement(qry).executeUpdate();
        }catch (Exception e) { e.printStackTrace(); }
    }

    public ResultSet query(String qry) {
        if (!isConnected()) throw new NullPointerException("Database not connected");
        try {
            return connection.prepareStatement(qry).executeQuery();
        }catch (Exception e) { e.printStackTrace(); }
        return null;
    }

    public Table create(String name, Column... variables) {
        StringBuilder qry = new StringBuilder("CREATE TABLE IF NOT EXISTS " + name + " (");
        for (Column column : variables) {
            if (!qry.toString().equals("CREATE TABLE IF NOT EXISTS " + name + " ("))
                qry.append(", ");
            qry.append(column.name)
                    .append(" ")
                    .append(column.type.getName())
                    .append("(")
                    .append(column.size)
                    .append(")");
        }
        qry.append(")");
        update(qry.toString());
        return new Table(name, variables, this);
    }

    public void delete(String table) {
        update("DROP TABLE " + table);
    }

    public Table getTable(String name) {
        try {
            ResultSet rs = query("SELECT * FROM " + name);
            ResultSetMetaData data = rs.getMetaData();
            ArrayList<Column> variables = new ArrayList<>();
            for (int i = 1; i <= data.getColumnCount(); i++) {
                String varName = data.getColumnLabel(i);
                String varType = data.getColumnTypeName(i);

                variables.add(new Column(varName, getType(varType), 0));
            }
            return new Table(name, variables.toArray(new Column[0]), this);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Class<? extends VariableType> getType(String name) {
        if (name == null) return VariableType.class;

        if (name.equals("VARCHAR"))
            return Varchar.class;

        return VariableType.class;
    }

}
