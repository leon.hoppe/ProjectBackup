package de.craftix.api.database.types;

public class Varchar implements VariableType {
    @Override
    public String getName() {
        return "VARCHAR";
    }
}
