package de.craftix.api.database;

import de.craftix.api.database.driver.Database;
import de.craftix.api.database.types.Varchar;

import java.io.Serializable;

public class Table {

    public final String name;
    public final Column[] columns;
    public final Database database;

    public Table(String name, Column[] columns, Database database) {
        this.name = name;
        this.columns = columns;
        this.database = database;
    }

    public void add(Serializable... data) {
        StringBuilder qry = new StringBuilder("INSERT INTO " + name + " VALUES (");
        for (int i = 0; i < data.length; i++) {
            if (i != 0)
                qry.append(", ");

            if (columns[i].type.getClass() == Varchar.class)
                qry.append("\"");

            qry.append(data[i].toString());

            if (columns[i].type.getClass() == Varchar.class)
                qry.append("\"");
        }
        qry.append(")");
        database.update(qry.toString());
    }

}
