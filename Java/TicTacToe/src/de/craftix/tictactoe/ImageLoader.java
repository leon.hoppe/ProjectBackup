package de.craftix.tictactoe;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

public class ImageLoader {

    public static BufferedImage imgX;
    public static BufferedImage imgO;

    public void loadImages() {
        try {
            /*imgX = ImageIO.read(new File("res/x.png"));
            imgO = ImageIO.read(new File("res/o.png"));*/
            imgX = ImageIO.read(Objects.requireNonNull(BufferedImage.class.getClassLoader().getResource("x.png")));
            imgO = ImageIO.read(Objects.requireNonNull(BufferedImage.class.getClassLoader().getResource("o.png")));
        }catch (Exception e) { e.printStackTrace(); }
    }

}
