package de.craftix.tictactoe;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Gui {

    public static JButton[] button = new JButton[9];
    public static int[] state = new int[9];
    public static int player = 0;
    public static int winner = 0;
    public static boolean useKI = false;
    public static int diffLevel = 1;

    public static int scorePlayer1 = 0;
    public static int scorePlayer2 = 0;

    public void setup(ImageLoader imgLoader) {
        player = new Random().nextBoolean() ? 1 : 2;
        imgLoader.loadImages();

        JFrame frame = new JFrame();
        Draw draw = new Draw();
        JButton reset = new JButton("Spiel zurücksetzten");
        JButton mode = new JButton("Modus: 2 Spieler");
        JButton difficulty = new JButton("Einfach");

        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setTitle("TicTacToe");

        for (int i = 0; i < button.length; i++) {
            button[i] = new JButton();
            button[i].setVisible(true);
            button[i].addActionListener(new ActionHandler());
            button[i].setFocusPainted(false);
            button[i].setContentAreaFilled(false);
            button[i].setBorder(null);
            frame.add(button[i]);
        }

        ButtonPlacement.place();

        reset.setBounds(630, 500, 150, 40);
        reset.setVisible(true);
        reset.setBackground(new Color(51, 102, 153));
        reset.setForeground(Color.WHITE);
        reset.setFocusPainted(false);
        reset.addActionListener(e -> Function.reset());
        frame.add(reset);

        mode.setBounds(630, 450, 150, 40);
        mode.setVisible(true);
        mode.setBackground(new Color(51, 102, 153));
        mode.setForeground(Color.WHITE);
        mode.setFocusPainted(false);
        mode.addActionListener(e -> {
            if (!useKI) {
                mode.setText("Modus: 1 Spieler");
                difficulty.setVisible(true);
            }else {
                mode.setText("Modus: 2 Spieler");
                difficulty.setVisible(false);
            }
            useKI = !useKI;
            if (player == 2) Function.calculateNextField(useKI);
        });
        frame.add(mode);

        difficulty.setBounds(630, 400, 150, 40);
        difficulty.setVisible(false);
        difficulty.setBackground(new Color(51, 102, 153));
        difficulty.setForeground(Color.WHITE);
        difficulty.setFocusPainted(false);
        difficulty.addActionListener(e -> {
            diffLevel++;
            if (diffLevel > 3) diffLevel = 1;
            if (diffLevel == 1) {
                difficulty.setText("Einfach");
            }
            else if (diffLevel == 2) {
                difficulty.setText("Mittel");
            }
            else if (diffLevel == 3) {
                difficulty.setText("Schwer");
            }
        });
        frame.add(difficulty);

        draw.setBounds(0, 0, 800, 600);
        draw.setVisible(true);
        frame.add(draw);

        frame.setVisible(true);
    }

}
