package de.craftix.tictactoe;

import javax.swing.*;
import java.awt.*;

public class Draw extends JLabel {

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        g.setColor(new Color(146, 146, 146, 150));
        g.fillRect(0, 0, 800, 600);

        g.setColor(Color.BLACK);
        g.drawLine(325, 50, 325, 500);
        g.drawLine(475, 50, 475, 500);
        g.drawLine(175, 200, 625, 200);
        g.drawLine(175, 350, 625, 350);

        g.setColor(Color.BLACK);
        if (Gui.player == 1) g.drawString("Spieler: X", 25, 50);
        if (Gui.player == 2) g.drawString("Spieler: O", 25, 50);

        g.drawString("Wins X: " + Gui.scorePlayer1, 25, 65);
        g.drawString("Wins O: " + Gui.scorePlayer2, 25, 80);

        if (Gui.winner == 1) g.drawString("Gewinner: X", 25, 100);
        if (Gui.winner == 2) g.drawString("Gewinner: O", 25, 100);
        if (Gui.winner == 3) g.drawString("Unentschieden", 25, 100);

        for (int i = 0; i < Gui.state.length; i++) {
            if (Gui.state[i] == 1) {
                g.drawImage(ImageLoader.imgX, Gui.button[i].getX(), Gui.button[i].getY(), null);
            }else if (Gui.state[i] == 2) {
                g.drawImage(ImageLoader.imgO, Gui.button[i].getX(), Gui.button[i].getY(), null);
            }
        }

        repaint();
    }

}
