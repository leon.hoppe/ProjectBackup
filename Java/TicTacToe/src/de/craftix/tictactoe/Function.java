package de.craftix.tictactoe;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Function {

    public static void reset() {
        Arrays.fill(Gui.state, 0);
        Gui.player = new Random().nextBoolean() ? 1 : 2;
        Gui.winner = 0;
        if (Gui.player == 2) calculateNextField(Gui.useKI);
    }

    public static boolean checkWinning() {
        ArrayList<Point> points = new ArrayList<>();
        for (int i = 0; i < Gui.state.length; i++) if (Gui.state[i] == Gui.player) points.add(getCoordinates(i + 1));

        if (points.contains(new Point(1, 1)) && points.contains(new Point(1, 2)) && points.contains(new Point(1, 3))) return true;
        if (points.contains(new Point(2, 1)) && points.contains(new Point(2, 2)) && points.contains(new Point(2, 3))) return true;
        if (points.contains(new Point(3, 1)) && points.contains(new Point(3, 2)) && points.contains(new Point(3, 3))) return true;

        if (points.contains(new Point(1, 1)) && points.contains(new Point(2, 1)) && points.contains(new Point(3, 1))) return true;
        if (points.contains(new Point(1, 2)) && points.contains(new Point(2, 2)) && points.contains(new Point(3, 2))) return true;
        if (points.contains(new Point(1, 3)) && points.contains(new Point(2, 3)) && points.contains(new Point(3, 3))) return true;

        if (points.contains(new Point(1, 1)) && points.contains(new Point(2, 2)) && points.contains(new Point(3, 3))) return true;
        return points.contains(new Point(1, 3)) && points.contains(new Point(2, 2)) && points.contains(new Point(3, 1));
    }

    public static boolean checkWinning(int player) {
        ArrayList<Point> points = new ArrayList<>();
        for (int i = 0; i < Gui.state.length; i++) if (Gui.state[i] == player) points.add(getCoordinates(i + 1));

        if (points.contains(new Point(1, 1)) && points.contains(new Point(1, 2)) && points.contains(new Point(1, 3))) return true;
        if (points.contains(new Point(2, 1)) && points.contains(new Point(2, 2)) && points.contains(new Point(2, 3))) return true;
        if (points.contains(new Point(3, 1)) && points.contains(new Point(3, 2)) && points.contains(new Point(3, 3))) return true;

        if (points.contains(new Point(1, 1)) && points.contains(new Point(2, 1)) && points.contains(new Point(3, 1))) return true;
        if (points.contains(new Point(1, 2)) && points.contains(new Point(2, 2)) && points.contains(new Point(3, 2))) return true;
        if (points.contains(new Point(1, 3)) && points.contains(new Point(2, 3)) && points.contains(new Point(3, 3))) return true;

        if (points.contains(new Point(1, 1)) && points.contains(new Point(2, 2)) && points.contains(new Point(3, 3))) return true;
        return points.contains(new Point(1, 3)) && points.contains(new Point(2, 2)) && points.contains(new Point(3, 1));
    }

    private static Point getCoordinates(int btnIndex) {
        Point p = new Point(0, 0);
        if (btnIndex <= 3) p.y = 1;
        if (btnIndex >= 4 && btnIndex <= 6) p.y = 2;
        if (btnIndex >= 7) p.y = 3;
        if (btnIndex == 1 || btnIndex == 4 || btnIndex == 7) p.x = 1;
        if (btnIndex == 2 || btnIndex == 5 || btnIndex == 8) p.x = 2;
        if (btnIndex == 3 || btnIndex == 6 || btnIndex == 9) p.x = 3;
        return p;
    }

    public static void calculateNextField(boolean useKI) {
        if (!useKI) return;
        ArrayList<Integer> freeFields = new ArrayList<>();
        for (int i = 0; i < Gui.state.length; i++) if (Gui.state[i] == 0) freeFields.add(i);
        if (freeFields.size() == 0) return;
        if (freeFields.size() == 1) {
            Gui.state[freeFields.get(0)] = 2;
            return;
        }
        if (Gui.diffLevel == 1) {
            int rand = new Random().nextInt(freeFields.size() - 1);
            Gui.state[freeFields.get(rand)] = 2;
            if (Function.checkWinning()) Gui.winner = Gui.player;
            Gui.player = 1;
        }
        else if (Gui.diffLevel == 2) {
            if (new Random().nextBoolean()) {
                if (calculateWinning()) return;
                if (checkPositions(1, 2, 3)) return;
                if (checkPositions(4, 5, 6)) return;
                if (checkPositions(7, 8, 9)) return;
                if (checkPositions(1, 4, 7)) return;
                if (checkPositions(2, 5, 8)) return;
                if (checkPositions(3, 6, 9)) return;
                if (checkPositions(1, 5, 9)) return;
                if (checkPositions(7, 5, 3)) return;
                int rand = new Random().nextInt(freeFields.size() - 1);
                Gui.state[freeFields.get(rand)] = 2;
            }else {
                int rand = new Random().nextInt(freeFields.size() - 1);
                Gui.state[freeFields.get(rand)] = 2;
            }
            if (Function.checkWinning()) Gui.winner = Gui.player;
            Gui.player = 1;
        }
        else if (Gui.diffLevel == 3) {
            if (calculateWinning()) return;
            if (checkPositions(1, 2, 3)) return;
            if (checkPositions(4, 5, 6)) return;
            if (checkPositions(7, 8, 9)) return;
            if (checkPositions(1, 4, 7)) return;
            if (checkPositions(2, 5, 8)) return;
            if (checkPositions(3, 6, 9)) return;
            if (checkPositions(1, 5, 9)) return;
            if (checkPositions(7, 5, 3)) return;
            int rand = new Random().nextInt(freeFields.size() - 1);
            Gui.state[freeFields.get(rand)] = 2;
            if (Function.checkWinning()) Gui.winner = Gui.player;
            Gui.player = 1;
        }
    }

    private static boolean checkPositions(int index1, int index2, int index3) {
        int fieldsPlayer1 = 0;
        int freeIndex = -1;
        if (Gui.state[index1 - 1] == 1) fieldsPlayer1++;
        if (Gui.state[index2 - 1] == 1) fieldsPlayer1++;
        if (Gui.state[index3 - 1] == 1) fieldsPlayer1++;
        if (Gui.state[index1 - 1] == 0) freeIndex = index1 - 1;
        if (Gui.state[index2 - 1] == 0) freeIndex = index2 - 1;
        if (Gui.state[index3 - 1] == 0) freeIndex = index3 - 1;
        if (fieldsPlayer1 == 2 && freeIndex != -1) {
            Gui.state[freeIndex] = 2;
            Gui.player = 1;
            return true;
        }
        return false;
    }

    private static boolean calculateWinning() {
        if (checkPositionsOwn(1, 2, 3)) return true;
        if (checkPositionsOwn(4, 5, 6)) return true;
        if (checkPositionsOwn(7, 8, 9)) return true;
        if (checkPositionsOwn(1, 4, 7)) return true;
        if (checkPositionsOwn(2, 5, 8)) return true;
        if (checkPositionsOwn(3, 6, 9)) return true;
        if (checkPositionsOwn(1, 5, 9)) return true;
        return checkPositionsOwn(3, 5, 7);
    }

    private static boolean checkPositionsOwn(int index1, int index2, int index3) {
        int fieldsPlayer2 = 0;
        int freeIndex = -1;
        if (Gui.state[index1 - 1] == 2) fieldsPlayer2++;
        if (Gui.state[index2 - 1] == 2) fieldsPlayer2++;
        if (Gui.state[index3 - 1] == 2) fieldsPlayer2++;
        if (Gui.state[index1 - 1] == 0) freeIndex = index1 - 1;
        if (Gui.state[index2 - 1] == 0) freeIndex = index2 - 1;
        if (Gui.state[index3 - 1] == 0) freeIndex = index3 - 1;
        if (fieldsPlayer2 == 2 && freeIndex != -1) {
            Gui.state[freeIndex] = 2;
            Gui.player = 1;
            return true;
        }
        return false;
    }

}
