package de.craftix.tictactoe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ActionHandler implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {

        if (Gui.winner == 0) {
            int btn = -1;
            for (int i = 0; i < Gui.button.length; i++) if (e.getSource() == Gui.button[i]) btn = i;
            if (btn == -1) return;
            if (Gui.state[btn] == 0 && Gui.player == 1) {
                Gui.state[btn] = 1;
                if (Function.checkWinning()) Gui.winner = Gui.player;
                Gui.player = 2;
                if (Gui.winner == 0) {
                    Function.calculateNextField(Gui.useKI);
                    if (Function.checkWinning(2)) Gui.winner = 2;
                }
            }else if (Gui.state[btn] == 0 && Gui.player == 2) {
                Gui.state[btn] = 2;
                if (Function.checkWinning()) Gui.winner = Gui.player;
                Gui.player = 1;
            }
            ArrayList<Integer> freeFields = new ArrayList<>();
            for (int i = 0; i < Gui.state.length; i++) if (Gui.state[i] == 0) freeFields.add(i);
            if (freeFields.size() == 0) Gui.winner = 3;
            if (Gui.winner == 1) Gui.scorePlayer1++;
            if (Gui.winner == 2) Gui.scorePlayer2++;
        }

    }
}
