package de.craftix.tictactoe;

public class Main {

    public static void main(String[] args) {
        Gui gui = new Gui();
        gui.setup(new ImageLoader());
    }

}
