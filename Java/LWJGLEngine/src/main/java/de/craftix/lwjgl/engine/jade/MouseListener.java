package de.craftix.lwjgl.engine.jade;

import java.awt.*;
import java.util.Arrays;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class MouseListener {
    private static final MouseListener instance = new MouseListener();
    private double scrollX, scrollY, xPos, yPos, lastX, lastY;
    private final boolean mouseButtonPressed[] = new boolean[MouseInfo.getNumberOfButtons()];
    private boolean isDragging;

    private MouseListener() {
        this.scrollX = 0;
        this.scrollY = 0;
        this.xPos = 0;
        this.yPos = 0;
        this.lastX = 0;
        this.lastY = 0;
        this.isDragging = false;
    }

    public static MouseListener get() { return instance; }

    public static void mousePosCallback(long window, double xPos, double yPos) {
        get().lastX = get().xPos;
        get().lastY = get().yPos;
        get().xPos = xPos;
        get().yPos = yPos;
        for (boolean button : get().mouseButtonPressed) { if (button) { get().isDragging = true; break; } }
    }

    public static void mouseButtonCallback(long window, int button, int action, int mods) {
        if (action == GLFW_PRESS) {
            get().mouseButtonPressed[button] = true;
        }
        if (action == GLFW_RELEASE) {
            get().mouseButtonPressed[button] = false;
            get().isDragging = false;
        }
    }

    public static void mouseScrollCallback(long window, double xOffset, double yOffset) {
        get().scrollX = xOffset;
        get().scrollY = yOffset;
    }

    public static void endFrame() {
        get().scrollX = 0;
        get().scrollY = 0;
        get().lastX = get().xPos;
        get().lastY = get().yPos;
    }

    public static float getX() { return (float)get().xPos; }
    public static float getY() { return (float)get().yPos; }
    public static float getDX() { return (float)(get().lastX - get().xPos); }
    public static float getDY() { return (float)(get().lastY - get().yPos); }
    public static float getScrollX() { return (float)get().scrollX; }
    public static float getScrollY() { return (float)get().scrollY; }
    public static boolean isDragging() { return get().isDragging; }
    public static boolean mouseButtonDown(int button) { return get().mouseButtonPressed[button]; }
}
