package de.craftix.test;

import de.craftix.lwjgl.engine.jade.Window;
import de.craftix.test.scenes.LevelEditorScene;

public class Main {

    public static void main(String[] args) {
        Window window = Window.get();
        window.changeScene(new LevelEditorScene());
        window.run(1280, 720, "GLFW Engine");
    }

}
