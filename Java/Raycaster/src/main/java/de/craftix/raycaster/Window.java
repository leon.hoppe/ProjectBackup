package de.craftix.raycaster;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Window extends Canvas {
    public static JFrame frame;
    private final BufferStrategy buffer;
    private final Player player;

    public static BufferedImage map;
    private static Graphics2D currentGraphics;

    public static float deltaTime;
    private int fps;

    private final int scalingFactor = 1;
    private final boolean minimap = true;

    public Window(String title, Dimension dimensions) throws Exception {
        frame = new JFrame();
        frame.setVisible(false);
        frame.setTitle(title);
        frame.setSize(dimensions);
        frame.setLocationRelativeTo(null);
        frame.add(this);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        createBufferStrategy(3);
        buffer = getBufferStrategy();

        map = ImageIO.read(Raycaster.class.getClassLoader().getResourceAsStream("map.png"));
        scaledMap = map.getScaledInstance(map.getWidth() / scalingFactor, map.getHeight() / scalingFactor, Image.SCALE_DEFAULT);
        player = new Player();
        addKeyListener(player);
        frame.addWindowStateListener(e -> {
            Player.updateRays = true;
        });
        requestFocus();
        new Thread(this::UpdateThread).start();
    }

    private void UpdateThread() {
        long lastFrame = System.nanoTime();
        long lastFPSUpdate = System.currentTimeMillis();
        int frameCount = 0;

        while (!Thread.currentThread().isInterrupted()) {
            long now = System.nanoTime();
            deltaTime = (System.nanoTime() - lastFrame) / 1000000000f;
            lastFrame = now;

            Update();
            Graphics2D g = (Graphics2D) buffer.getDrawGraphics();
            currentGraphics = g;
            Render(g);

            if (now - lastFPSUpdate >= 1000000000) {
                fps = frameCount;
                lastFPSUpdate = now;
                frameCount = 0;
                System.out.println("FPS: " + fps);
            }

            buffer.show();
            frameCount++;
        }

        buffer.dispose();
    }

    private void Update() {
        player.Update();
    }

    private Image scaledMap;
    private void Render(Graphics2D g) {
        currentGraphics = g;
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, frame.getWidth(), frame.getHeight());

        int middle = frame.getHeight() / 2;
        for (int y = 0; y < frame.getHeight(); y++) {
            int dist = Math.abs(middle - y);
            int brightness = (int) Raycaster.map(dist, 0, middle, 0, 100);
            g.setColor(new Color(brightness, brightness, brightness, 255));
            g.drawLine(0, y, frame.getWidth(), y);
        }

        player.Render(g);

        //Render Minimap
        if (minimap) {
            int yOffset = frame.getHeight() - scaledMap.getHeight(null) - 50;
            g.drawImage(scaledMap, 0, yOffset, null);
            g.setColor(Color.RED);
            g.fillRect((int) Player.position.getX() / scalingFactor - 2, (int) Player.position.getY() / scalingFactor + yOffset - 2, 4, 4);
        }

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, 100, 50);
        g.setColor(Color.BLACK);
        g.drawString("FPS: " + fps, 5, 15);
        g.drawString("Resolution: " + Raycaster.resolution, 5, 30);
        g.drawString("FOV: " + Raycaster.fov, 5, 45);
        g.dispose();
    }

    public static Graphics2D getCurrentGraphics() {
        return currentGraphics;
    }
}
