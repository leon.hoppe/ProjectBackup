package de.craftix.raycaster;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Point2D;

public class Player implements KeyListener {

    public static Point2D position;
    public static double rotation;

    public static boolean updateRays = false;

    private float horizontal = 0;
    private float vertical = 0;

    public Player() {
        position = new Point(150, 150);
        rotation = 0;
        Raycaster.Raycast(position, rotation);
    }

    public void Update() {
        Point2D orig = new Point2D.Double(position.getX(), position.getY());
        int speed = 20;
        if (horizontal > 0) {
            double x = Math.sin(rotation) * Window.deltaTime * speed;
            double y = -Math.cos(rotation) * Window.deltaTime * speed;
            position = new Point2D.Double(position.getX() + x, position.getY() + y);
            updateRays = true;
        } else if (horizontal < 0) {
            double x = Math.sin(rotation) * Window.deltaTime * speed;
            double y = -Math.cos(rotation) * Window.deltaTime * speed;
            position = new Point2D.Double(position.getX() - x, position.getY() - y);
            updateRays = true;
        }

        if (vertical > 0) {
            rotation -= Window.deltaTime * 1.5;
            updateRays = true;
        } else if (vertical < 0) {
            rotation += Window.deltaTime * 1.5;
            updateRays = true;
        }

        int intersection = Window.map.getRGB((int) position.getX(), (int) position.getY());
        if (intersection != 0) {
            updateRays = true;
            position = orig;
        }

        if (updateRays) {
            Raycaster.Raycast(position, rotation);
            updateRays = false;
        }
    }

    public void Render(Graphics2D g) {
        Raycaster.RenderRays(g);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_W)
            horizontal = 1;
        if (e.getKeyCode() == KeyEvent.VK_S)
            horizontal = -1;

        if (e.getKeyCode() == KeyEvent.VK_A)
            vertical = 1;
        if (e.getKeyCode() == KeyEvent.VK_D)
            vertical = -1;

        if (e.getKeyCode() == KeyEvent.VK_UP) {
            Raycaster.resolution++;
            Raycaster.resolution = clamp(Raycaster.resolution, 5, Window.frame.getWidth());
            updateRays = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            Raycaster.resolution--;
            Raycaster.resolution = clamp(Raycaster.resolution, 5, Window.frame.getWidth());
            updateRays = true;
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            Raycaster.fov++;
            Raycaster.fov = clamp(Raycaster.fov, 30, 120);
            updateRays = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            Raycaster.fov--;
            Raycaster.fov = clamp(Raycaster.fov, 30, 120);
            updateRays = true;
        }

        if (e.getKeyCode() == KeyEvent.VK_SPACE)
            Raycaster.curate = !Raycaster.curate;
    }


    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_W)
            horizontal = 0;
        if (e.getKeyCode() == KeyEvent.VK_S)
            horizontal = 0;

        if (e.getKeyCode() == KeyEvent.VK_A)
            vertical = 0;
        if (e.getKeyCode() == KeyEvent.VK_D)
            vertical = 0;
    }

    private int clamp(int value, int min, int max) {
        return Math.max(Math.min(value, max), min);
    }
}
