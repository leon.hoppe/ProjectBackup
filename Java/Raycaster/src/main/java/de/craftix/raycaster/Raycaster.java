package de.craftix.raycaster;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.TreeMap;

public class Raycaster {
    private static TreeMap<Float, Double> rays = new TreeMap<>();

    public static void main(String[] args) throws Exception {
        new Window("Raycaster", new Dimension(1280, 720));
    }

    public static int fov = 60;
    public static int resolution = 500;
    public static boolean curate = false;

    private static boolean updating = false;

    private static BufferedImage scaledMap;
    private static final int scalingFactor = 10;

    public static void Raycast(Point2D origin, double direction) {
        updating = true;
        if (scaledMap == null) {
            scaledMap = Resizer.BICUBIC.resize(Window.map, Window.map.getWidth() * scalingFactor, Window.map.getHeight() * scalingFactor);
        }

        rays = new TreeMap<>();
        double step = fov / (double) resolution;
        double halfFOV = fov / 2d;

        for (double i = -halfFOV; i < halfFOV; i += step) {
            double relativeDirection = Math.toRadians(i);
            double dx = Math.sin(direction + relativeDirection) / scalingFactor;
            double dy = -Math.cos(direction + relativeDirection) / scalingFactor;
            Point2D ray = SingleRay(origin.getX(), origin.getY(), dx, dy);
            double dist = origin.distance(ray);
            rays.put(map((float) (i + halfFOV), 0, fov, 0, Window.frame.getWidth()), dist);
        }
        updating = false;
    }

    public static void RenderRays(Graphics2D g) {
        if (updating) return;
        float width = Window.frame.getWidth() / (float) resolution;
        Float[] xValues = rays.keySet().toArray(new Float[0]);
        Double[] distValues = rays.values().toArray(new Double[0]);

        for (int i = 0; i < rays.size(); i++) {
            float x = xValues[i];
            double dist = distValues[i];
            double nextDist = i == rays.size() - 1 || !curate ? dist : distValues[i + 1];
            Shape shape = GenerateScreenObject(x, (float) dist, (float) nextDist, width, i);
            int brightness = clamp((int)map((float) dist, 100, 0, 0, 255), 0, 255);
            g.setColor(new Color(brightness, brightness, brightness));
            g.fill(shape);
        }
    }

    private static Point2D SingleRay(double x, double y, double dx, double dy) {
        boolean intersected = false;
        while (!intersected) {
            try {
                int bit = scaledMap.getRGB((int) (x * scalingFactor), (int) (y * scalingFactor));
                if (bit == -0x1000000) {
                    x = x + dx;
                    y = y + dy;
                }else intersected = true;
            }catch (Exception e) {
                intersected = true;
            }
        }
        return new Point2D.Double(x, y);
    }

    private static Shape GenerateScreenObject(float x, float dist, float nextDist, float width, int interrator) {
        float height = 4000f / dist;
        float nextHeight = 4000f / nextDist;
        if (height - nextHeight > 100) nextHeight = height;
        float middle = Window.frame.getHeight() / 2f;
        return new Polygon2D(
                new float[] {
                        x, x,         x + width,
                        x, x + width, x + width
                },
                new float[] {
                        middle - height, middle + height, middle + nextHeight,
                        middle - height, middle + nextHeight, middle - nextHeight
                },
                6
        );
    }

    public static float normalise(float value, float min, float max) {
        return (value - min) / (max - min);
    }

    public static float map(float value, float min, float max, float mMin, float mMax) {
        float norm = normalise(value, min, max);
        return (mMax - mMin) * norm + mMin;
    }

    private static int clamp(int value, int min, int max) {
        return Math.max(Math.min(value, max), min);
    }

    public enum Resizer implements Serializable {

        NEAREST_NEIGHBOR {
            @Override
            public BufferedImage resize(BufferedImage source,
                                        int width, int height) {
                return commonResize(source, width, height,
                        RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
            }
        },
        BILINEAR {
            @Override
            public BufferedImage resize(BufferedImage source,
                                        int width, int height) {
                return commonResize(source, width, height,
                        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            }
        },
        BICUBIC {
            @Override
            public BufferedImage resize(BufferedImage source,
                                        int width, int height) {
                return commonResize(source, width, height,
                        RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            }
        },
        PROGRESSIVE_BILINEAR {
            @Override
            public BufferedImage resize(BufferedImage source,
                                        int width, int height) {
                return progressiveResize(source, width, height,
                        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            }
        },
        PROGRESSIVE_BICUBIC {
            @Override
            public BufferedImage resize(BufferedImage source,
                                        int width, int height) {
                return progressiveResize(source, width, height,
                        RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            }
        },
        AVERAGE {
            @Override
            public BufferedImage resize(BufferedImage source,
                                        int width, int height) {
                Image img2 = source.getScaledInstance(width, height,
                        Image.SCALE_AREA_AVERAGING);
                BufferedImage img = new BufferedImage(width, height,
                        source.getType());
                Graphics2D g = img.createGraphics();
                try {
                    g.drawImage(img2, 0, 0, width, height, null);
                } finally {
                    g.dispose();
                }
                return img;
            }
        };

        public abstract BufferedImage resize(BufferedImage source,
                                             int width, int height);

        private static BufferedImage progressiveResize(BufferedImage source,
                                                       int width, int height, Object hint) {
            int w = Math.max(source.getWidth()/2, width);
            int h = Math.max(source.getHeight()/2, height);
            BufferedImage img = commonResize(source, w, h, hint);
            while (w != width || h != height) {
                BufferedImage prev = img;
                w = Math.max(w/2, width);
                h = Math.max(h/2, height);
                img = commonResize(prev, w, h, hint);
                prev.flush();
            }
            return img;
        }

        private static BufferedImage commonResize(BufferedImage source,
                                                  int width, int height, Object hint) {
            BufferedImage img = new BufferedImage(width, height,
                    source.getType());
            Graphics2D g = img.createGraphics();
            try {
                g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
                g.drawImage(source, 0, 0, width, height, null);
            } finally {
                g.dispose();
            }
            return img;
        }

    }

}
