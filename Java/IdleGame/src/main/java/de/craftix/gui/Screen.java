package de.craftix.gui;

import de.craftix.IdleGame;
import de.craftix.Log;
import de.craftix.engine.Save;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Screen extends JFrame {
    public static int width = 800;
    public static int height = 600;

    public Screen(String title) {
        setSize(width, height);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle(title);
        setVisible(true);
        setBackground(Color.GRAY);

        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                IdleGame.data.money = IdleGame.money;
                IdleGame.data.convertGenerators(IdleGame.engine.getGeneratorsAsList());
                Save.save(IdleGame.data);
                Log.info("Game stopped successfully");
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

}
