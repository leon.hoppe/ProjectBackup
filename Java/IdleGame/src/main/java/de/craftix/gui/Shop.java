package de.craftix.gui;

import de.craftix.IdleGame;

import java.awt.*;

public class Shop extends ContentBar {

    public Shop(int width, int height, int x, int y) {
        super(width, height, x, y);
        IdleGame.shop = this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        repaint();
    }
}
