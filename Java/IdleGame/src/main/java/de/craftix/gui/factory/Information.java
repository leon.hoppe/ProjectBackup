package de.craftix.gui.factory;

import de.craftix.IdleGame;
import de.craftix.engine.Generator;
import de.craftix.gui.Factory;

import javax.swing.*;
import java.awt.*;

public class Information extends JLabel {
    private final int width;
    private final int height;

    public Information(int width, int height, int x, int y) {
        this.width = width;
        this.height = height;
        setSize(width, height);
        setBounds(x, y, width, height);
        setVisible(true);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, width, height);

        g.setColor(Color.GRAY);
        g.fill3DRect(150, 150, 100, 100, true);
        g.fillRect(300, 150, 100, 100);

        if (Factory.selectedGen != null) {
            Generator gen = Factory.selectedGen;
            g.setColor(Color.BLACK);
            g.drawString("Generator: " + gen.name, 10, 15);
            g.drawString("Owned: " + IdleGame.convertCost(gen.owned), 10, 30);
            g.drawString("Base Revenue: " + IdleGame.convertCost(gen.baseRevenue), 10, 45);
            g.drawString("Base Cost: " + IdleGame.convertCost(gen.baseCost), 10, 60);
            g.drawString("Next Generator costs: " + IdleGame.convertCost(Math.round(gen.calculateNextBuildingCosts(1))), 10, 75);
        }

        repaint();
    }
}
