package de.craftix.gui;

import de.craftix.IdleGame;
import de.craftix.engine.Generator;
import de.craftix.gui.factory.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

public class Factory extends ContentBar implements ListSelectionListener {
    public static Generator selectedGen;

    public Factory(int width, int height, int x, int y) {
        super(width, height, x, y);
        IdleGame.factory = this;

        add(new GenList());
        add(new Information(550, 490, 200, 10));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        repaint();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        DefaultListSelectionModel list = (DefaultListSelectionModel) e.getSource();
        if (list.getSelectedIndices().length == 0) return;
        selectedGen = IdleGame.engine.getGeneratorByName(IdleGame.engine.getGeneratorNames()[list.getSelectedIndices()[0]]);
    }
}
