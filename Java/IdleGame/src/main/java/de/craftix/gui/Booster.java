package de.craftix.gui;

import de.craftix.IdleGame;

import java.awt.*;

public class Booster extends ContentBar {
    public Booster(int width, int height, int x, int y) {
        super(width, height, x, y);
        IdleGame.booster = this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        repaint();
    }
}
