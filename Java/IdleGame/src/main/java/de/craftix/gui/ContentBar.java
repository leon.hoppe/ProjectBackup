package de.craftix.gui;

import de.craftix.IdleGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ContentBar extends JLabel implements ActionListener {

    public boolean isVisible;
    protected int width;
    protected int height;
    protected int x;
    protected int y;
    public JButton button;

    public ContentBar(int width, int height, int x, int y) {
        setVisible(false);
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.isVisible = false;
        setSize(width, height);
        setBounds(x, y, width, height);
        IdleGame.contentMenus.add(this);
        IdleGame.screen.add(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        isVisible = !isVisible;
        IdleGame.setVisible(this, isVisible);
        updateButton();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.BLACK);
    }

    public void updateButton() {
        if (isVisible) button.setBackground(Color.GRAY);
        else button.setBackground(Color.DARK_GRAY);
    }

    public void setButton(JButton button) {
        this.button = button;
    }
}
