package de.craftix.gui;

import de.craftix.IdleGame;

import javax.swing.*;
import java.awt.*;

public class MenuBar extends JLabel {
    private final int width;
    private final int height;
    private final int x;
    private final int y;

    private final int contentX;
    private final int contentY;
    private final int contentWidth;
    private final int contentHeight;

    public MenuBar(int width, int height, int x, int y) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        contentWidth = Screen.width;
        contentHeight = Screen.height - height;
        contentX = 0;
        contentY = y + height;
        setSize(width, height);
        setBounds(x, y, width, height);
        IdleGame.menuBar = this;
        add(buttonSetup("Factory", 0, new Factory(contentWidth, contentHeight, contentX, contentY)));
        add(buttonSetup("Shop", 1, new Shop(contentWidth, contentHeight, contentX, contentY)));
        add(buttonSetup("Upgrades", 2, new Upgrades(contentWidth, contentHeight, contentX, contentY)));
        add(buttonSetup("Booster", 3, new Booster(contentWidth, contentHeight, contentX, contentY)));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, width, height);

        g.setColor(Color.BLACK);
        g.drawString("Money: " + IdleGame.convertMoney(), 690, 25);

        repaint();
    }

    private Rectangle getButtonPosition(int buttonID) {
        Rectangle rec = new Rectangle();
        rec.x = 10 + (110 * buttonID);
        rec.y = 10;
        rec.width = 100;
        rec.height = 30;
        return rec;
    }

    private JButton buttonSetup(String title, int buttonID, ContentBar bar) {
        JButton button = new JButton(title);
        bar.setButton(button);
        button.setBounds(getButtonPosition(buttonID));
        button.setVisible(true);
        button.setBackground(Color.DARK_GRAY);
        button.setForeground(Color.WHITE);
        button.setFocusPainted(false);
        button.addActionListener(bar);
        return button;
    }
}
