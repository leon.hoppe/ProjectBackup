package de.craftix.gui.factory;

import de.craftix.IdleGame;

import javax.swing.*;
import java.awt.*;

public class GenList extends JList {

    public GenList() {
        setSize(150, 490);
        setBounds(10, 10, 150, 490);
        setVisible(true);
        setVisible(true);
        setBackground(Color.LIGHT_GRAY);
        setSelectionBackground(Color.WHITE);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        getSelectionModel().addListSelectionListener(IdleGame.factory);
        setListData(IdleGame.engine.getGeneratorNames());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        repaint();
        updateUI();
    }
}
