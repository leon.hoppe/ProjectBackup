package de.craftix.commands;

import de.craftix.CommandManager;
import de.craftix.IdleGame;
import de.craftix.Log;
import de.craftix.engine.Generator;

public class GeneratorCmd implements CommandManager.Command {
    @Override
    public void onCommand(String cmd, String[] args) {
        if (args[0].equalsIgnoreCase("add")) {
            Generator gen = IdleGame.engine.getGenerator(args[1]);
            if (gen == null) {
                Log.error("This generator does not exists");
                return;
            }
            if (args.length == 3) gen.owned += Integer.parseInt(args[2]);
            else gen.owned++;
        }
        if (args[0].equalsIgnoreCase("remove")) {
            Generator gen = IdleGame.engine.getGenerator(args[1]);
            if (gen == null) {
                Log.error("This generator does not exists");
                return;
            }
            if (args.length == 3) gen.owned -= Integer.parseInt(args[2]);
            else gen.owned--;
        }
        if (args[0].equalsIgnoreCase("set")) {
            Generator gen = IdleGame.engine.getGenerator(args[1]);
            if (gen == null) {
                Log.error("This generator does not exists");
                return;
            }
            if (args.length == 3) gen.owned = Integer.parseInt(args[2]);
            else gen.owned = 1;
        }
    }
}
