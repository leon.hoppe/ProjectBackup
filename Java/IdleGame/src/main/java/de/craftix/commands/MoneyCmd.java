package de.craftix.commands;

import de.craftix.CommandManager;
import de.craftix.IdleGame;

public class MoneyCmd implements CommandManager.Command {
    @Override
    public void onCommand(String cmd, String[] args) {
        if (args.length != 2) return;
        if (args[0].equalsIgnoreCase("set")) IdleGame.money = Long.parseLong(args[1]);
        if (args[0].equalsIgnoreCase("add")) IdleGame.money += Long.parseLong(args[1]);
        if (args[0].equalsIgnoreCase("remove")) IdleGame.money -= Long.parseLong(args[1]);
    }
}
