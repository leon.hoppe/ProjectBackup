package de.craftix.engine;

import de.craftix.IdleGame;

public class Generator {

    public int owned;
    public double baseCost;
    public double baseRevenue;
    public float baseProductionTimeInSeconds;
    public float costFactor;
    public float productionTimeInSeconds;
    public double productionCycleInSeconds;
    public String name;

    public double calculateNextBuildingCosts(int times) {
        double kOverR = Math.pow(costFactor, owned);
        double kPlusNOverR = Math.pow(costFactor, owned + times);
        return baseCost + ((kOverR - kPlusNOverR) / (1 - costFactor));
    }

    public boolean canBeBuild() { return IdleGame.money >= calculateNextBuildingCosts(1); }

    public void Build() {
        if (!canBeBuild()) return;
        IdleGame.money -= calculateNextBuildingCosts(1);
        owned++;
        updateProductionTime();
    }

    public void produce(long deltaTimeInSeconds) {
        if (owned == 0) return;
        productionCycleInSeconds += deltaTimeInSeconds;
        double calculatedSum = 0;
        while (productionCycleInSeconds >= productionTimeInSeconds)
        {
            calculatedSum += baseRevenue * owned;
            productionCycleInSeconds -= productionTimeInSeconds;
        }
        IdleGame.money += calculatedSum;
    }

    public void updateProductionTime() { productionTimeInSeconds = baseProductionTimeInSeconds; }

}
