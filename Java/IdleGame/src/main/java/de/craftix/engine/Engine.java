package de.craftix.engine;

import java.util.ArrayList;

public class Engine extends Thread {

    private ArrayList<Generator> generatorTypes = new ArrayList<>();
    public static ArrayList<Generator> genPresets = new ArrayList<>();

    public void calculate(int timeInterval) {
        for (Generator all : generatorTypes) {
            all.produce(timeInterval);
        }
    }

    public String[] getGeneratorNames() {
        String[] array = new String[generatorTypes.size()];
        for (int i = 0; i < array.length; i++) array[i] = generatorTypes.get(i).name;
        return array;
    }

    public Generator getGeneratorByPeace(String name) {
        char[] chars = name.toCharArray();
        StringBuilder countString = new StringBuilder();
        for (char c : chars) {
            if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9') {
                countString.append(c);
            }
        }
        name = name.replace(" - " + countString.toString(), "");
        for (Generator all : generatorTypes) if (all.name.equals(name)) return all;
        return null;
    }

    public Generator[] getGenerators() { return generatorTypes.toArray(new Generator[0]); }

    public void addGenerator(Generator gen) { generatorTypes.add(gen); }

    public void setGenerators(ArrayList<Generator> gens) { generatorTypes = gens; }
    public ArrayList<Generator> getGeneratorsAsList() { return generatorTypes; }

    public Generator getGeneratorByName(String name) {
        for (Generator all : generatorTypes) {
            if (all.name.equals(name)) return all;
        }
        return null;
    }

    public static Generator getGenerator(String name) {
        for (Generator all : genPresets) {
            if (all.name.equals(name)) return all;
        }
        return null;
    }

}
