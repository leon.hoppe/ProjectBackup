package de.craftix.engine;

import de.craftix.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Save implements Serializable {

    public static void save(Save instance) { writeObjectToFile(instance, "data.save"); }
    public static Save getData() { return (Save) readObjectFromFile("data.save"); }

    private static void writeObjectToFile(Object serObj, String filepath) {

        try {

            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();
            Log.info("The Object was successfully written to a file");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static Object readObjectFromFile(String filepath) {

        try {

            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Object obj = objectIn.readObject();

            Log.info("The Object has been read from the file");
            objectIn.close();
            return obj;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //nonStatic
    public long money;
    public long lastCalculationTime;
    public HashMap<String, Integer> generators = new HashMap<>();

    public Save() {}

    public ArrayList<Generator> convertGenerators() {
        ArrayList<Generator> gens = new ArrayList<>();
        for (String name : generators.keySet()) {
            Generator gen = Engine.getGenerator(name);
            if (gen == null) continue;
            gen.owned = generators.get(name);
            gens.add(gen);
        }
        return gens;
    }

    public void convertGenerators(ArrayList<Generator> gens) {
        for (Generator all : gens) generators.put(all.name, all.owned);
    }

}
