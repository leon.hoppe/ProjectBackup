package de.craftix;

import java.util.HashMap;

public class CommandManager {
    private static HashMap<String, Command> cmds = new HashMap<>();

    public static void input(String qry) {
        if (qry.contains(" ")) {
            String[] argsRaw = qry.split(" ");
            String cmd = argsRaw[0];
            String[] args = new String[argsRaw.length - 1];
            for (int i = 1; i < argsRaw.length; i++) args[i - 1] = argsRaw[i];
            Command executor = cmds.get(cmd.toLowerCase());
            if (executor != null) executor.onCommand(cmd, args);
        }else {
            Command executor = cmds.get(qry);
            if (executor != null) executor.onCommand(qry, new String[0]);
        }
    }

    public static void addCommand(String cmd, Command executor) {
        cmds.put(cmd.toLowerCase(), executor);
    }

    public interface Command {
        void onCommand(String cmd, String[] args);
    }

}
