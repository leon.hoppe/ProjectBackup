package de.craftix;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log {

    public static void info(String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println("[" + dtf.format(now) + "] [INFO] " + message);
    }

    public static void warning(String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println("\u001B[33m" + "[" + dtf.format(now) + "] [WARNING] " + message);
    }

    public static void error(String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.err.println("[" + dtf.format(now) + "] [ERROR] " + message);
    }

}
