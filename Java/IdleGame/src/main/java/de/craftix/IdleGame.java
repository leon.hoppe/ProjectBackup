package de.craftix;

import de.craftix.commands.*;
import de.craftix.engine.*;
import de.craftix.gui.*;
import de.craftix.gui.factory.Information;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class IdleGame implements ActionListener {

    public static long money;
    public static Engine engine;
    public static Timer timer;
    public static Save data;

    public static Screen screen;
    public static MenuBar menuBar;
    public static Shop shop;
    public static Factory factory;
    public static Upgrades upgrades;
    public static Booster booster;

    public static ArrayList<ContentBar> contentMenus = new ArrayList<>();

    public static void main(String[] args) {
        screen = new Screen("IdleGame");
        setGenerators();
        File file = new File("data.save");
        if (file.exists()) {
            data = Save.getData();
            money = data.money;
            engine.setGenerators(data.convertGenerators());
            engine.calculate((int) (System.currentTimeMillis() - data.lastCalculationTime) / 1000);
        }else data = new Save();

        screen.add(new MenuBar(screen.getWidth(), 50, 0, 0));

        CommandManager.addCommand("money", new MoneyCmd());
        CommandManager.addCommand("generator", new GeneratorCmd());

        //Commands
        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    CommandManager.input(reader.readLine());
                }catch (Exception ignored) {}
            }
        }).start();

        timer = new Timer(1000, new IdleGame());
        timer.start();

        Log.info("Game started successfully");
    }

    private static void setGenerators() {
        engine = new Engine();

        Generator genDefault = new Generator();
        genDefault.owned = 1;
        genDefault.baseCost = 10;
        genDefault.baseRevenue = 5;
        genDefault.baseProductionTimeInSeconds = 5;
        genDefault.costFactor = 1.01f;
        genDefault.name = "Default";
        genDefault.updateProductionTime();
        Engine.genPresets.add(genDefault);
        engine.addGenerator(genDefault);
    }

    public static String convertMoney() {
        String output = String.valueOf(money);
        if (output.length() < 4) return output + " $";
        if (output.length() > 3 && output.length() <= 6) {
            output = money / 1000 + "K $";
            return output;
        }
        if (output.length() > 6 && output.length() <= 9) {
            output = money / 1000000 + "M $";
            return output;
        }
        if (output.length() > 9 && output.length() <= 12) {
            output = money / 1000000000 + "B $";
            return output;
        }
        if (output.length() > 12 && output.length() <= 15) {
            output = money / 1000000000000L + "T $";
            return output;
        }
        if (output.length() > 15) {
            output = money / 1000000000000000L + "Q $";
        }
        return output + " $";
    }

    public static String convertCost(double cost) {
        String output = String.valueOf(cost);
        if (cost < 1000) return output;
        if (cost > 999 && cost <= 1000000) {
            output = cost / 1000 + "K";
            return output;
        }
        if (cost > 9999999 && cost <= 1000000000) {
            output = cost / 1000000 + "M";
            return output;
        }
        if (cost > 9999999999L && cost <= 1000000000000L) {
            output = cost / 1000000000 + "B";
            return output;
        }
        if (cost > 9999999999999L && cost <= 1000000000000000L) {
            output = cost / 1000000000000L + "T";
            return output;
        }
        if (cost > 9999999999999999L) {
            output = cost / 1000000000000000L + "Q";
        }
        return output;
    }

    public static void setVisible(ContentBar label, boolean value) {
        for (ContentBar all : contentMenus) {
            if (label == all) continue;
            all.setVisible(false);
            all.isVisible = false;
            if (label.button != null) all.updateButton();
        }
        label.setVisible(value);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        engine.calculate(1);
        data.lastCalculationTime = System.currentTimeMillis();
    }
}
