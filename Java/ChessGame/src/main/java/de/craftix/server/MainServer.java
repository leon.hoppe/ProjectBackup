package de.craftix.server;

import de.craftix.engine.Logger;
import de.craftix.engine.var.MySQL;
import de.craftix.engine.var.SqLite;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.UUID;

public class MainServer {
    public static final int PORT = 23010;
    public static final String IP = "localhost"; //"213.136.89.237";

    public static UUID serverUUID = UUID.randomUUID();
    public static ServerSocket socket;
    public static ArrayList<Client> clients = new ArrayList<>();
    public static Logger logger;

    public static SqLite sql;

    public static void main(String[] args) {
        sql = new SqLite(":memory:");
        sql.connect();
        createTables();
        try {
            logger = new Logger("Server");
            socket = new ServerSocket(PORT);
            logger.info("Server started");
            logger.info("Server ready for connections");

            while (!Thread.currentThread().isInterrupted())
                new Client(socket.accept());
        }catch (Exception e) { e.printStackTrace(); }
    }

    private static void createTables() {
        sql.insert("CREATE TABLE IF NOT EXISTS Users (UUID VARCHAR(100), Username VARCHAR(100), Password VARCHAR(100))");
        sql.insert("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(100), StatName VARCHAR(20), StatValue INT(100))");
    }
}


