package de.craftix.server;

import de.craftix.engine.Logger;
import de.craftix.game.Stats;
import de.craftix.game.User;
import de.craftix.game.configs.UserConfig;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Client implements Serializable {
    public transient ObjectInputStream in;
    public transient ObjectOutputStream out;
    public transient Socket socket;
    public transient Thread thread;
    public InetAddress address;
    public Logger logger;
    public UUID uuid;

    public Client(Socket socket) throws IOException {
        this.socket = socket;
        this.address = socket.getInetAddress();
        this.out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        out.flush();
        this.in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        logger = new Logger(address.getHostAddress());
        MainServer.clients.add(this);
        logger.info("Connected.");

        while (uuid == null) {
            UUID ram = UUID.randomUUID();
            if (ram.equals(MainServer.serverUUID)) continue;
            boolean equals = false;
            for (Client c : MainServer.clients) {
                if (c.uuid != null && c.uuid.equals(ram)) {
                    equals = true;
                    break;
                }
            }
            if (!equals)
                uuid = ram;
        }

        thread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    packetHandler(awaitPacket());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public Packet awaitPacket() {
        try {
            return (Packet) in.readObject();
        } catch (Exception e) {
            disconnect();
        }
        return null;
    }

    public void sendPacket(Packet packet) {
        try {
            out.writeObject(packet);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        thread.interrupt();
        try {
            in.close();
            out.close();
            socket.close();
        } catch (Exception ignored) {
        }
        MainServer.clients.remove(this);
        logger.info("Disconnected.");
    }

    private void packetHandler(Packet p) throws Exception {
        if (p == null) return;
        //HandlePackets
        if (p.getType() == 0) { //Ping
            sendPacket(new Packet(0));
        }
        if (p.getType() == 1) { //Login
            String username = (String) p.getData()[0];
            String password = (String) p.getData()[1];
            if (!checkPassword(password)) {
                sendPacket(new Packet(3, false));
                return;
            }
            ResultSet rs = MainServer.sql.getData("SELECT * FROM Users WHERE Username = \"" + username + "\"");
            if (rs.next()) {
                String realPass = rs.getString("Password");
                if (realPass.equals(password)) {
                    UUID uuid = UUID.fromString(rs.getString("UUID"));
                    sendPacket(new Packet(1, true, new User(uuid, username, this.uuid, getStats(uuid))));
                }else {
                    sendPacket(new Packet(1, false));
                }
            }else {
                sendPacket(new Packet(1, false));
            }
        }
        if (p.getType() == 2) { //Register
            String username = (String) p.getData()[0];
            String password = (String) p.getData()[1];
            if (!checkPassword(password)) {
                sendPacket(new Packet(3, false));
                return;
            }
            UUID uuid = UUID.randomUUID();
            Object[] users = getUsers();
            List<UUID> uuids = (List<UUID>) users[0];
            List<String> names = (List<String>) users[1];
            while (uuids.contains(uuid))
                uuid = UUID.randomUUID();

            if (names.contains(username)) {
                sendPacket(new Packet(2, false));
                return;
            }

            MainServer.sql.insert("INSERT INTO Users (UUID, Username, Password) VALUES (\"" + uuid + "\", \"" + username + "\", \"" + password + "\")");
            saveStats(uuid, new Stats());

            sendPacket(new Packet(2, true, new User(uuid, username, this.uuid, new Stats())));
        }
        if (p.getType() == 3) { //Change Password
            User user = (User) p.getData()[0];
            String password = (String) p.getData()[1];
            if (!checkPassword(password)) {
                sendPacket(new Packet(3, false));
                return;
            }
            MainServer.sql.insert("DELETE FROM Users WHERE UUID = \"" + user.getUUID() + "\"");
            MainServer.sql.insert("INSERT INTO Users (UUID, Username, Password) VALUES (\"" + user.getUUID() + "\", \"" + user.getName() + "\", \"" + password + "\")");
            sendPacket(new Packet(3, true, user));
        }
        if (p.getType() == 4) { //Save Stats
            UUID user = (UUID) p.getData()[0];
            Stats stats = (Stats) p.getData()[1];
            saveStats(user, stats);
            sendPacket(new Packet(0));
        }
    }

    private boolean checkPassword(String password) {
        password = UserConfig.decode(password);
        return password.length() > 8 && !password.contains(" ");
    }

    private Object[] getUsers() {
        ArrayList<UUID> uuids = new ArrayList<>();
        ArrayList<String> users = new ArrayList<>();
        ArrayList<String> pass = new ArrayList<>();
        try {
            ResultSet rs = MainServer.sql.getData("SELECT * FROM Users");
            while (rs.next()) {
                uuids.add(UUID.fromString(rs.getString("UUID")));
                users.add(rs.getString("Username"));
                pass.add(rs.getString("Password"));
            }
        }catch (Exception e) { e.printStackTrace(); }
        return new Object[] { uuids, users, pass };
    }

    private Stats getStats(UUID user) {
        try {
            Stats stats = new Stats();
            ResultSet rs = MainServer.sql.getData("SELECT * FROM Stats WHERE UUID = \"" + user + "\"");
            while (rs.next()) {
                String name = rs.getString("StatName");
                int value = rs.getInt("StatValue");

                switch (name) {
                    case "GamesPlayed":
                        stats.gamesPlayed = value;
                        break;
                    case "Wins":
                        stats.wins = value;
                        break;
                    case "Looses":
                        stats.looses = value;
                        break;
                    case "FiguresStolen":
                        stats.figuresStolen = value;
                        break;
                    case "PawnReachEnd":
                        stats.pawnReachEnd = value;
                        break;
                }
            }
            return stats;
        }catch (Exception e) { e.printStackTrace(); }
        return new Stats();
    }

    private void saveStats(UUID user, Stats stats) {
        String syntax = "INSERT INTO Stats (UUID, StatName, StatValue) VALUES (\"" + uuid + "\", \"";

        MainServer.sql.insert("DELETE FROM Stats WHERE UUID = \"" + user + "\";");
        MainServer.sql.insert(syntax + "GamesPlayed\", " + stats.gamesPlayed + ");");
        MainServer.sql.insert(syntax + "Wins\", " + stats.wins + ");");
        MainServer.sql.insert(syntax + "Looses\", " + stats.looses + ");");
        MainServer.sql.insert(syntax + "FiguresStolen\", " + stats.figuresStolen + ");");
        MainServer.sql.insert(syntax + "PawnReachEnd\", " + stats.pawnReachEnd + ");");
    }

}

