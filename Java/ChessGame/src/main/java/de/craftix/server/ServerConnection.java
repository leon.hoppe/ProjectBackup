package de.craftix.server;

import de.craftix.engine.Logger;
import de.craftix.game.Chess;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ServerConnection {
    public static Socket socket;
    public static ObjectInputStream in;
    public static ObjectOutputStream out;
    public static InetSocketAddress address;
    public static Logger logger;

    public static void connect() {
        try {
            address = new InetSocketAddress(MainServer.IP, MainServer.PORT);
            socket = new Socket();
            logger = new Logger("ServerConnection");
            logger.info("Connecting to Server...");
            socket.connect(address, 10000);
            logger.info("connected");
            out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            out.flush();
            in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        }catch (Exception e) {
            Chess.throwError(e);
        }
    }

    public static Packet sendPacket(Packet packet) {
        try {
            out.writeObject(packet);
            out.flush();
            return (Packet) in.readObject();
        }catch (Exception e) { Chess.throwError(e); }
        return new Packet(0, null);
    }

    public static void disconnect() {
        try {
            in.close();
            out.close();
            socket.close();
        }catch (Exception ignored) {}
        logger.info("Disconnected.");
    }

    public static int getPing() {
        long startTime = System.currentTimeMillis();
        Packet ping = sendPacket(new Packet(0));
        long endTime = System.currentTimeMillis();
        return (int) (endTime - startTime);
    }
}

