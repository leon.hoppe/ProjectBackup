package de.craftix.server;

import java.io.Serializable;

public class Packet implements Serializable {

    private final byte type;
    private final Object[] data;

    public Packet(int type, Object... data) {
        this.type = (byte) type;
        this.data = data;
    }

    public int getType() { return type; }
    public Object[] getData() { return data; }

}
