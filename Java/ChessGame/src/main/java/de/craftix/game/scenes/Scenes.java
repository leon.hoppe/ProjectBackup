package de.craftix.game.scenes;

public final class Scenes {
    private Scenes() {}

    public static final LoginScene loginScene = new LoginScene();
    public static final MenuScene menuScene = new MenuScene();
    public static final PlayScene playScene = new PlayScene();
    public static final ProfileScene profileScene = new ProfileScene();
    public static final SettingsScene settingsScene = new SettingsScene();

}
