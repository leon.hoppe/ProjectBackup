package de.craftix.game.scenes;

import de.craftix.engine.InputManager;
import de.craftix.engine.render.Screen;
import de.craftix.engine.ui.UIAlignment;
import de.craftix.engine.ui.UIManager;
import de.craftix.engine.ui.elements.UIButton;
import de.craftix.engine.ui.elements.UISlider;
import de.craftix.engine.ui.elements.UIText;
import de.craftix.engine.var.Dimension;
import de.craftix.engine.var.Scene;
import de.craftix.engine.var.Transform;
import de.craftix.engine.var.Vector2;
import de.craftix.game.Chess;
import de.craftix.game.SoundManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SettingsScene extends Scene implements ActionListener {
    UISlider masterVolume;
    UISlider musicVolume;
    UIButton back;

    @Override
    public void onStart() {
        InputManager.setCursor(Cursor.DEFAULT_CURSOR);
        setBackgroundColor(Chess.menuBackground);
        Font font = new Font("Arial", Font.BOLD, 20);

        UIText sounds = new UIText("Sounds", new Font("Arial", Font.BOLD, 30), Color.WHITE, new Transform(new Vector2(0, 250)), UIAlignment.CENTER);
        UIText mvText = new UIText("Gesamtlautstärke", font, Color.WHITE, new Transform(new Vector2(-130, 200)), UIAlignment.CENTER);
        UIText muvText = new UIText("Musik", font, Color.WHITE, new Transform(new Vector2(-130, 150)), UIAlignment.CENTER);

        masterVolume = new UISlider(true, new Dimension(20), new Transform(new Vector2(70, 200), new Dimension(200, 10)), UIAlignment.CENTER);
        musicVolume = new UISlider(true, new Dimension(20), new Transform(new Vector2(70, 150), new Dimension(200, 10)), UIAlignment.CENTER);

        masterVolume.setValue(SoundManager.getMasterVolume());
        musicVolume.setValue(SoundManager.getMusicVolume());

        masterVolume.setValueChangedListener(this);
        musicVolume.setValueChangedListener(this);

        back = new UIButton("Zurück", new Transform(new Vector2(90, 50), new Dimension(150, 60)), UIAlignment.BOTTOM_LEFT);
        back.setFont(Chess.menuFont);
        back.setClickListener(this);

        UIManager manager = getUIManager();
        manager.removeElements();
        manager.addElement(sounds);
        manager.addElement(mvText);
        manager.addElement(muvText);
        manager.addElement(masterVolume);
        manager.addElement(musicVolume);
        manager.addElement(back);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == back) Chess.setScene(Scenes.menuScene);
        else if (e.getSource() == masterVolume)
            SoundManager.setMasterVolume((int) masterVolume.getValue());
        else if (e.getSource() == musicVolume)
            SoundManager.setMusicVolume((int) musicVolume.getValue());
    }
}
