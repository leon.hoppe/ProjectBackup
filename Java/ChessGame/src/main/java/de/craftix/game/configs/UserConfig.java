package de.craftix.game.configs;

import de.craftix.engine.var.configuration.utils.Configurable;
import de.craftix.engine.var.configuration.utils.ConfigurationManager;
import de.craftix.game.Chess;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class UserConfig {

    public static Configurable<String> username = new Configurable<>("user.username", "");
    public static Configurable<String> password = new Configurable<>("user.password", "");

    public static ConfigurationManager manager = new ConfigurationManager(new File(Chess.root + "userdata.yml"), username, password);

    public static String decode(String password) { return new String(Base64.getDecoder().decode(password)); }
    public static String encode(String password) { return Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8)); }

}
