package de.craftix.game;

import de.craftix.engine.GameEngine;
import de.craftix.engine.var.Mathf;
import de.craftix.engine.var.Sound;

public class SoundManager {
    private static boolean initialised = false;
    private static int masterVolume = 100;
    private static int musicVolume = 100;

    private static Sound backgroundMusic;

    public static void initialise() {
        backgroundMusic = new Sound(GameEngine.loadFile("backgroundmusic.wav"));

        initialised = true;

        backgroundMusic.getClip().loop(Integer.MAX_VALUE);

        updateSounds();
        backgroundMusic.play();
    }

    private static void updateSounds() {
        if (!initialised) return;
        float musicVolume = Mathf.map(SoundManager.musicVolume * masterVolume, 0, 10000, 0, 100);

        backgroundMusic.setVolume(musicVolume);
    }

    public static void setMasterVolume(int volume) { masterVolume = volume; updateSounds(); }
    public static void setMusicVolume(int volume) { musicVolume = volume; updateSounds(); }

    public static int getMasterVolume() { return masterVolume; }
    public static int getMusicVolume() { return musicVolume; }

}
