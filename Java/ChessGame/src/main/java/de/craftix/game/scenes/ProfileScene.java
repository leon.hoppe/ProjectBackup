package de.craftix.game.scenes;

import de.craftix.engine.InputManager;
import de.craftix.engine.render.Screen;
import de.craftix.engine.render.Sprite;
import de.craftix.engine.ui.UIAlignment;
import de.craftix.engine.ui.UIManager;
import de.craftix.engine.ui.elements.UIButton;
import de.craftix.engine.ui.elements.UIText;
import de.craftix.engine.ui.elements.UITextBox;
import de.craftix.engine.var.Dimension;
import de.craftix.engine.var.Scene;
import de.craftix.engine.var.Transform;
import de.craftix.engine.var.Vector2;
import de.craftix.game.Chess;
import de.craftix.game.UserManagement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProfileScene extends Scene implements ActionListener {

    private UIButton changePassword;
    private UIButton logout;
    private UIButton back;

    private UITextBox password;
    private UIButton confirmChange;

    @Override
    public void onStart() {
        InputManager.setCursor(Cursor.DEFAULT_CURSOR);
        setBackgroundColor(Chess.menuBackground);

        //Stats
        UIText statsTitle = new UIText("Statistiken:", new Font("Arial", Font.BOLD, 30), Color.WHITE, new Transform(new Vector2(0, 200)), UIAlignment.CENTER);

        changePassword = new UIButton("Passwort ändern", new Transform(new Vector2(-180, 120), new Dimension(300, 60)), UIAlignment.BOTTOM_RIGHT);
        changePassword.setFont(Chess.menuFont);
        changePassword.setClickListener(this);

        logout = new UIButton("Ausloggen", new Transform(new Vector2(-180, 50), new Dimension(300, 60)), UIAlignment.BOTTOM_RIGHT);
        logout.setFont(Chess.menuFont);
        logout.setClickListener(this);

        back = new UIButton("Zurück", new Transform(new Vector2(90, 50), new Dimension(150, 60)), UIAlignment.BOTTOM_LEFT);
        back.setFont(Chess.menuFont);
        back.setClickListener(this);

        password = new UITextBox("Neues Passwort", new Transform(new Vector2(-200, 50), new Dimension(380, 60)), UIAlignment.BOTTOM, UITextBox.Type.PASSWORD);
        password.setFont(Chess.menuFont);
        password.setMaxlength(30);
        password.setVisible(false);

        confirmChange = new UIButton("Bestätigen", new Transform(new Vector2(80, 50), new Dimension(150, 60)), UIAlignment.BOTTOM);
        confirmChange.setFont(Chess.menuFont);
        confirmChange.setVisible(false);
        confirmChange.setClickListener(this);

        UIManager manager = getUIManager();
        manager.removeElements();
        manager.addElement(statsTitle);
        manager.addElement(changePassword);
        manager.addElement(logout);
        manager.addElement(back);
        manager.addElement(password);
        manager.addElement(confirmChange);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == back) Chess.setScene(Scenes.menuScene);
        if (e.getSource() == logout) {
            UserManagement.logout();
            Chess.setScene(Scenes.loginScene);
        }
        if (e.getSource() == changePassword) {
            boolean visible = !password.isVisible();
            password.setVisible(visible);
            confirmChange.setVisible(visible);
        }
        if (e.getSource() == confirmChange) {
            String password = this.password.getText();
            boolean result = UserManagement.changePassword(password);
            JOptionPane.showMessageDialog(Screen.getDisplay(), result ? "Passwort geändert" : "Passwort konnte nicht geändert werden", "ChessGame", result ? JOptionPane.INFORMATION_MESSAGE : JOptionPane.ERROR_MESSAGE);
        }
    }
}
