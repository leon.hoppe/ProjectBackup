package de.craftix.game;

import java.io.Serializable;
import java.util.UUID;

public class User implements Serializable {

    private final UUID uuid;
    private final UUID serverID;
    private final String name;
    private final Stats stats;

    public User(UUID uuid, String name, UUID serverID, Stats stats) {
        this.uuid = uuid;
        this.name = name;
        this.serverID = serverID;
        this.stats = stats;
    }

    public UUID getUUID() { return uuid; }
    public UUID getServerID() { return serverID; }
    public String getName() { return name; }
    public Stats getStats() { return stats; }

}
