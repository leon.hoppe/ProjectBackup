package de.craftix.game.scenes;

import de.craftix.engine.var.Scene;
import de.craftix.game.Chess;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayScene extends Scene implements ActionListener {

    @Override
    public void onStart() {
        setBackgroundColor(Chess.menuBackground);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
