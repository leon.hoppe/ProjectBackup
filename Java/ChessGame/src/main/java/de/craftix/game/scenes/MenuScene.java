package de.craftix.game.scenes;

import de.craftix.engine.InputManager;
import de.craftix.engine.ui.UIAlignment;
import de.craftix.engine.ui.UIManager;
import de.craftix.engine.ui.elements.UIButton;
import de.craftix.engine.ui.elements.UIText;
import de.craftix.engine.var.Dimension;
import de.craftix.engine.var.Scene;
import de.craftix.engine.var.Transform;
import de.craftix.engine.var.Vector2;
import de.craftix.game.Chess;
import de.craftix.game.UserManagement;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuScene extends Scene implements ActionListener {

    private UIButton play;
    private UIButton settings;
    private UIButton profile;
    private UIButton close;

    @Override
    public void onStart() {
        InputManager.setCursor(Cursor.DEFAULT_CURSOR);
        setBackgroundColor(Chess.menuBackground);

        UIText title = new UIText("Schach Online", new Font("Arial", Font.BOLD, 80), Color.WHITE, new Transform(new Vector2(0, 200)), UIAlignment.CENTER);
        UIText user = new UIText("Angemeldet als: " + UserManagement.user.getName(), new Font("Arial", Font.PLAIN, 15), Color.WHITE, new Transform(new Vector2(0, 120)), UIAlignment.CENTER);

        play = new UIButton("Spielen", new Transform(new Vector2(0, 70), new Dimension(300, 60)), UIAlignment.CENTER);
        play.setFont(Chess.menuFont);
        play.setClickListener(this);

        settings = new UIButton("Einstellungen", new Transform(new Vector2(0, 0), new Dimension(300, 60)), UIAlignment.CENTER);
        settings.setFont(Chess.menuFont);
        settings.setClickListener(this);

        profile = new UIButton("Profil", new Transform(new Vector2(-77, -70), new Dimension(148, 60)), UIAlignment.CENTER);
        profile.setFont(Chess.menuFont);
        profile.setClickListener(this);

        close = new UIButton("Beenden", new Transform(new Vector2(77, -70), new Dimension(148, 60)), UIAlignment.CENTER);
        close.setFont(Chess.menuFont);
        close.setClickListener(this);

        UIManager manager = getUIManager();
        manager.removeElements();
        manager.addElement(title);
        manager.addElement(user);
        manager.addElement(play);
        manager.addElement(settings);
        manager.addElement(profile);
        manager.addElement(close);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == close) Chess.shutdown();
        if (e.getSource() == play) Chess.setScene(Scenes.playScene);
        if (e.getSource() == settings) Chess.setScene(Scenes.settingsScene);
        if (e.getSource() == profile) Chess.setScene(Scenes.profileScene);
    }
}
