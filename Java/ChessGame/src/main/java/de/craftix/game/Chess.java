package de.craftix.game;

import de.craftix.engine.EngineSettings;
import de.craftix.engine.GameEngine;
import de.craftix.engine.render.Sprite;
import de.craftix.game.scenes.Scenes;
import de.craftix.server.MainServer;
import de.craftix.server.ServerConnection;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;

public class Chess extends GameEngine {
    public static final String root = System.getenv("APPDATA") + File.separator + "ChessGame" + File.separator;
    static { new File(root).mkdirs(); }

    public static final Color menuBackground = Color.GRAY;
    public static final Font menuFont = new Font("Arial", Font.PLAIN, 30);

    public static void main(String[] args) throws Exception {
        //TODO: REMOVE THE FIRST TWO LINES
        new Thread(() -> MainServer.main(args)).start();
        Thread.sleep(1000);

        ServerConnection.connect();
        SoundManager.setMusicVolume(0);
        EngineSettings.setResizable(false);
        EngineSettings.printSystemLog(false);
        EngineSettings.setWindowIcon(Sprite.load("icon.png"));
        EngineSettings.setAntialiasing(true);
        EngineSettings.setFullscreenKey(KeyEvent.VK_F11);
        EngineSettings.setCloseKey(KeyEvent.VK_ESCAPE);
        setup(1280, 720, "Schach Online", new Chess(), 1);
    }

    @Override
    public void initialise() {
        if (!UserManagement.isLoggedIn()) setScene(Scenes.loginScene);
        else setScene(Scenes.menuScene);
        SoundManager.initialise();
    }

    @Override
    public void stop() {
        ServerConnection.disconnect();
    }
}
