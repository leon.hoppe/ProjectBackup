package de.craftix.game;

import de.craftix.server.Packet;
import de.craftix.server.ServerConnection;

import java.io.Serializable;

public class Stats implements Serializable {

    public int gamesPlayed;
    public int wins;
    public int looses;
    public int figuresStolen;
    public int pawnReachEnd;

    public Stats() {
        this.gamesPlayed = 0;
        this.wins = 0;
        this.looses = 0;
        this.figuresStolen = 0;
        this.pawnReachEnd = 0;
    }

    public void save() {
        ServerConnection.sendPacket(new Packet(4, UserManagement.user.getUUID(), this));
    }

}
