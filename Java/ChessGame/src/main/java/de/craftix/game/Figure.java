package de.craftix.game;

import de.craftix.engine.render.Sprite;
import de.craftix.engine.render.SpriteMap;

public enum Figure {
    KING(0, 0, 6, "König"),
    QUEEN(1, 1, 7, "Königin"),
    BISHOP(2, 2, 8, "Läufer"),
    KNIGHT(3, 3, 9, "Pferd"),
    ROOK(4, 4, 10, "Turm"),
    PAWN(5, 5, 11, "Bauer");

    private static final SpriteMap img = new SpriteMap(6, Sprite.load("figures.png"), 95, 95);

    private final int id;
    private final int texIDWhite;
    private final int texIDBlack;
    private final String name;

    Figure(int id, int texIDWhite, int texIDBlack, String name) {
        this.id = id;
        this.texIDWhite = texIDWhite;
        this.texIDBlack = texIDBlack;
        this.name = name;
    }

    public int getID() { return id; }
    public Sprite getSprite(int color) { return color == 0 ? img.getSprite(texIDWhite) : img.getSprite(texIDBlack); }
    public String getName() { return name; }

    public static Figure getByID(int id) { return values()[id]; }
}
