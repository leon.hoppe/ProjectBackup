package de.craftix.game.scenes;

import de.craftix.engine.InputManager;
import de.craftix.engine.ui.UIAlignment;
import de.craftix.engine.ui.UIManager;
import de.craftix.engine.ui.elements.UIButton;
import de.craftix.engine.ui.elements.UIText;
import de.craftix.engine.ui.elements.UITextBox;
import de.craftix.engine.var.Scene;
import de.craftix.engine.var.Transform;
import de.craftix.engine.var.Vector2;
import de.craftix.engine.var.Dimension;
import de.craftix.game.Chess;
import de.craftix.game.UserManagement;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginScene extends Scene implements ActionListener {

    private UITextBox username;
    private UITextBox password;
    private UIButton login;
    private UIButton register;
    private UIText error;

    @Override
    public void onStart() {
        InputManager.setCursor(Cursor.DEFAULT_CURSOR);
        setBackgroundColor(Chess.menuBackground);

        UIText title = new UIText("Login / Register", new Font("Arial", Font.BOLD, 50), Color.WHITE, new Transform(new Vector2(0, 150)), UIAlignment.CENTER);
        username = new UITextBox("Username", new Transform(new Vector2(0, 50), new Dimension(400, 60)), UIAlignment.CENTER, UITextBox.Type.TEXT);
        password = new UITextBox("Password", new Transform(new Vector2(0, -50), new Dimension(400, 60)), UIAlignment.CENTER, UITextBox.Type.PASSWORD);
        login = new UIButton("Login", new Transform(new Vector2(-110, -150), new Dimension(185, 40)), UIAlignment.CENTER);
        register = new UIButton("Register", new Transform(new Vector2(110, -150), new Dimension(185, 40)), UIAlignment.CENTER);
        error = new UIText("Error Message", new Font("Arial", Font.PLAIN, 20), Color.RED, new Transform(new Vector2(0, 100)), UIAlignment.CENTER);

        username.setFont(Chess.menuFont);
        password.setFont(Chess.menuFont);
        login.setFont(Chess.menuFont);
        register.setFont(Chess.menuFont);

        username.setMaxlength(30);
        password.setMaxlength(30);

        login.setClickListener(this);
        register.setClickListener(this);

        error.setVisible(false);

        UIManager manager = getUIManager();
        manager.addElement(title);
        manager.addElement(username);
        manager.addElement(password);
        manager.addElement(login);
        manager.addElement(register);
        manager.addElement(error);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String username = this.username.getText();
        String password = this.password.getText();
        boolean success = false;
        if (e.getSource() == login) success = UserManagement.login(username, password);
        if (e.getSource() == register) success = UserManagement.register(username, password);

        if (success) Chess.setScene(Scenes.menuScene);
        else {
            if (e.getSource() == login)
                error.setText("Username oder Passwort stimmt nicht überein!");
            else if (e.getSource() == register)
                error.setText("Dieser Username existiert schon!");
            error.setVisible(true);
        }
    }
}
