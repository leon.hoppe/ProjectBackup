package de.craftix.game;

import de.craftix.game.configs.UserConfig;
import de.craftix.server.Packet;
import de.craftix.server.ServerConnection;

public class UserManagement {
    public static User user;

    private static void setUserData(Packet packet, String username, String password) {
        if ((boolean) packet.getData()[0]) {
            UserConfig.password.setValue(password);
            UserConfig.username.setValue(username);
            UserConfig.manager.saveConfig();
            user = (User) packet.getData()[1];
        }
    }

    public static boolean isLoggedIn() {
        String username = UserConfig.username.getValue();
        String password = UserConfig.password.getValue();

        Packet answer = ServerConnection.sendPacket(new Packet(1, username, password));
        setUserData(answer, username, password);

        return (boolean) answer.getData()[0];
    }

    public static boolean login(String username, String password) {
        password = UserConfig.encode(password);

        Packet answer = ServerConnection.sendPacket(new Packet(1, username, password));
        setUserData(answer, username, password);

        return (boolean) answer.getData()[0];
    }

    public static boolean register(String username, String password) {
        password = UserConfig.encode(password);

        Packet answer = ServerConnection.sendPacket(new Packet(2, username, password));
        setUserData(answer, username, password);

        return (boolean) answer.getData()[0];
    }

    public static boolean changePassword(String password) {
        password = UserConfig.encode(password);

        Packet answer = ServerConnection.sendPacket(new Packet(3, user, password));
        setUserData(answer, user.getName(), password);

        return (boolean) answer.getData()[0];
    }

    public static void logout() {
        UserConfig.password.setValue("");
        UserConfig.username.setValue("");
        UserConfig.manager.saveConfig();
        user = null;
    }

}
