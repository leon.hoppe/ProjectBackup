package de.craftix.game.block;

import de.craftix.game.Game;

import java.awt.image.BufferedImage;

public enum Material {

    AIR(0, true, 0),
    STONE(1, false, 2000l),
    DIRT(2, false, 1000l),
    GRASS(3, false, 1200l),
    SAND(4, false, 750l);

    private int id;
    private long destroyDuration;
    private boolean walkable;
    private BufferedImage image;

    Material(int id, boolean walkable, long destroyDuration){
        this.id = id;
        this.walkable = walkable;
        this.destroyDuration = destroyDuration;
        this.image = Game.terrain.getTexture(id);
    }

    public int getId() {return id;}
    public boolean isWalkable() {return walkable;}
    public BufferedImage getImage() {return image;}
    public long getDestroyDuration() {return destroyDuration;}

}
