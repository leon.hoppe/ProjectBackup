package de.craftix.game.block;

import de.craftix.game.*;

import java.awt.*;

public class Block extends GameObject {

    public static Spritesheed destroy = new Spritesheed(10, ImageLoader.load("img/destroy.png"), 16, 16);

    //DestroyAnimation
    private long destroyDuration;
    private long destroyStartTime;
    private boolean destroying;

    private Animation animation;
    private Material material;

    public Block(Material material, float x, float y, int width, int height) {
        super(x, y, width, height);
        this.material = material;
        destroyDuration = material.getDestroyDuration();
        long dt = destroyDuration / destroy.getCols();
        animation = new Animation(destroy, dt);
    }

    public void update() {
        //BLOCK DESTROYING
        if (destroying) {
            int mx = GamePanel.mouse.conX;
            int my = GamePanel.mouse.conY;

            animation.update();

            //CHECK INTERRUPTION
            if (!GamePanel.mouse.pressed || !getBox().contains(new Point(mx, my))) {
                destroying = false;
                animation.stop();
            }

            //DESTROYING BLOCK AFTER DURATION
            if (System.currentTimeMillis() - destroyStartTime >= destroyDuration) {
                material = Material.AIR;
                destroying = false;
                animation.stop();
            }
        }
    }

    public void destroy() {
        if (!destroying && material != Material.AIR) {
            destroying = true;
            destroyStartTime = System.currentTimeMillis();
            animation.start(0, destroy.getCols());
        }
    }

    public void render(Graphics2D g) {
        g.drawImage(material.getImage(), (int)x - Playstate.camera.getCamX(), (int)y - Playstate.camera.getCamY(), null);

        //DRAW BLOCK DESTROYING
        if (destroying) g.drawImage(animation.getImage(), (int)x - Playstate.camera.getCamX(), (int)y - Playstate.camera.getCamY(), null);
    }

    public Material getMaterial() {return material;}
}
