package de.craftix.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.VolatileImage;

public class GamePanel extends JPanel implements KeyListener, MouseListener, ActionListener, ComponentListener {

    private static final long serialVersionUID = 1L;

    public static int width;
    public static int height;
    public static final int SCALE = 3;
    public static Mouse mouse;

    private Timer timer;
    private GameStateManager gsm;
    private VolatileImage image;

    public GamePanel() {
        super();
        setPreferredSize(new Dimension(Game.width, Game.height));
        addComponentListener(this);
        addMouseListener(this);
        addKeyListener(this);
        setBackground(Color.BLACK);
        setFocusable(true);
        requestFocus();

        mouse = new Mouse(this);
        width = getPreferredSize().width;
        height = getPreferredSize().height;
        gsm = new GameStateManager(GameStateManager.PLAYSTATE);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        image = createVolatileImage(width / SCALE, height / SCALE);
        timer = new Timer(1000 / Game.FPS, this::actionPerformed);
        timer.start();
    }

    private void update() {
        gsm.update();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = image.createGraphics();
        g2.setBackground(new Color(146, 189, 221));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        gsm.render(g2);
        g.drawImage(image, 0, 0, width, height, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        update();
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        gsm.keyPressed(e, e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        gsm.keyReleased(e, e.getKeyCode());
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        gsm.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        gsm.mouseReleased(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void componentResized(ComponentEvent e) {
        if (!e.getSource().equals(this)) return;
        width = getWidth();
        height = getHeight();
        image = createVolatileImage(width / SCALE, height / SCALE);
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }
}
