package de.craftix.game.entity;

import de.craftix.game.*;

import java.awt.*;

public abstract class Entity extends GameObject {

    //CONSTANTS
    private final float GRAVITY = 0.2f;
    private final float MAX_FALLING_SPEED = 2.5f;
    private final float JUMP_START = -3.5f;

    //MOVEMENT
    protected float speed;
    protected float dx;
    protected float dy;
    protected boolean left;
    protected boolean right;
    protected boolean falling;
    protected boolean jumping;

    //COLLISION
    protected boolean topLeft;
    protected boolean topRight;
    protected boolean midLeft;
    protected boolean midRight;
    protected boolean bottomLeft;
    protected boolean bottomRight;

    //ANIMATION
    protected int[] frames = { 1, 1, 2, 2 };
    protected int idle;
    protected final int IDLE_LEFT = 0;
    protected final int IDLE_RIGHT = 1;
    protected final int LEFT = 2;
    protected final int RIGHT = 3;
    protected Animation animation;

    public Entity(float x, float y, int width, int height, float speed, Spritesheed sprite) {
        super(x, y, width, height);
        this.speed = speed;
        animation = new Animation(sprite, IDLE_LEFT, frames[IDLE_LEFT], 150l);
        this.idle = IDLE_LEFT;
    }

    public void render(Graphics2D g) {
        g.drawImage(animation.getImage(), (int)x, (int)y, null);
    }

    public void update() {
        calculateMovement();
        calculateCollisions();
        calculateAnimations();
        move();
    }

    private void calculateAnimations(){
        animation.update();

        if (left && animation.getState() != LEFT) {
            animation.setImages(LEFT, frames[LEFT]);
            idle = IDLE_LEFT;
        }else if (right && animation.getState() != RIGHT){
            animation.setImages(RIGHT, frames[RIGHT]);
            idle = IDLE_RIGHT;
        }

        if (!left && !right){
            animation.setImages(idle, frames[idle]);
        }
    }

    private void calculateMovement() {
        if (left) dx = -speed;
        if (right) dx = speed;
        if (falling && !jumping) {
            dy += GRAVITY;
            if (dy > MAX_FALLING_SPEED) dy = MAX_FALLING_SPEED;
        }
        if (jumping && !falling) {
            dy = JUMP_START;
            jumping = false;
            falling = true;
        }
    }

    private void calculateCollisions() {
        float toX = x + dx;
        float toY = y + dy;

        //COLLISION LEFT AND RIGHT
        calculateCorners(toX, y - 1);
        if (dx < 0) if (topLeft || midLeft || bottomLeft) dx = 0;
        if (dx > 0) if (topRight || midRight || bottomRight) dx = 0;

        //COLLISION TOP AND BOTTOM
        calculateCorners(x, toY);

        if (topLeft || topLeft) {
            dy = 0;
            falling = true;
            int playerRow = Playstate.world.getRowTile((int)toY);
            y = (playerRow + 1) * Game.BLOCKSIZE;
        }

        if (bottomLeft || bottomRight && falling) {
            falling = false;
            dy = 0;
            int playerRow = Playstate.world.getRowTile((int)toY + height);
            y = playerRow * Game.BLOCKSIZE - height;
        }
        if (!bottomLeft && !bottomRight) falling = true;
    }

    private void calculateCorners(float x, float y) {
        try {
            World world = Playstate.world;
            int leftTile = world.getColTile((int)x);
            int rightTile = world.getColTile((int)x + width - 1);
            int topTile = world.getRowTile((int)y);
            int midTile = world.getRowTile((int)y + height / 2);
            int bottomTile = world.getRowTile((int)y + height);
            topLeft = !world.getBlocks()[topTile][leftTile].getMaterial().isWalkable();
            topRight = !world.getBlocks()[topTile][rightTile].getMaterial().isWalkable();
            midLeft = !world.getBlocks()[midTile][leftTile].getMaterial().isWalkable();
            midRight = !world.getBlocks()[midTile][rightTile].getMaterial().isWalkable();
            bottomLeft = !world.getBlocks()[bottomTile][leftTile].getMaterial().isWalkable();
            bottomRight = !world.getBlocks()[bottomTile][rightTile].getMaterial().isWalkable();
        }catch (Exception e) {topLeft = topRight = midLeft = midRight = bottomLeft = bottomRight = false;}

    }

    private void move() {
        x += dx;
        y += dy;

        dx = 0;
    }
}
