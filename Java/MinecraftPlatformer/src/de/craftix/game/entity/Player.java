package de.craftix.game.entity;

import de.craftix.game.GamePanel;
import de.craftix.game.ImageLoader;
import de.craftix.game.Spritesheed;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Player extends Entity {
    public Player(float x, float y, int width, int height, float speed) {
        super(x, y, width, height, speed, new Spritesheed(2, ImageLoader.load("img/player.png"), 20, 32));
    }

    public void keyPressed(KeyEvent e, int k){
        switch (k){
            case KeyEvent.VK_A: left = true; break;
            case KeyEvent.VK_D: right = true; break;
            case KeyEvent.VK_SPACE: if (!falling && !jumping) jumping = true; break;
        }
    }

    public void keyReleased(KeyEvent e, int k){
        switch (k){
            case KeyEvent.VK_A: left = false; break;
            case KeyEvent.VK_D: right = false; break;
        }
    }

    @Override
    public void render(Graphics2D g){
        g.drawImage(animation.getImage(), GamePanel.width / 2 / GamePanel.SCALE - width / 2, GamePanel.height / 2 / GamePanel.SCALE - height / 2, null);
    }
}
