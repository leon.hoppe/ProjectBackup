package de.craftix.game;

import de.craftix.game.entity.Player;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Playstate extends State {

    public static World world;
    public static Player player;
    public static Camera camera;

    public Playstate(GameStateManager gsm) {
        super(gsm);
        world = new World("world.txt");
        player = new Player(10, 5, 20, 31, 0.75f);
        camera = new Camera(player);
    }

    @Override
    public void update() {
        player.update();
        world.update();
    }

    @Override
    public void render(Graphics2D g) {
        g.clearRect(0, 0, GamePanel.width, GamePanel.height);
        world.render(g);
        player.render(g);
    }

    @Override
    public void keyPressed(KeyEvent e, int k) {
        player.keyPressed(e, k);
    }

    @Override
    public void keyReleased(KeyEvent e, int k) {
        player.keyReleased(e, k);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }
}
