package de.craftix.game;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class ImageLoader {

    public static BufferedImage load(String path){
        if (path == null) return null;
        try {
            return ImageIO.read(new File(path));
        }catch (Exception e) {e.printStackTrace();}
        return null;
    }

}
