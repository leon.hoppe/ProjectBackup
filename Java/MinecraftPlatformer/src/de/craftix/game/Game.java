package de.craftix.game;

import javax.swing.*;
import java.awt.*;

public class Game extends JFrame {

    public static final int width = 600;
    public static final int height = 400;
    public static final int BLOCKSIZE = 16;
    public static final int FPS = 60;

    private static final long serialVersionUID = 1L;
    public static Spritesheed terrain = new Spritesheed(5, ImageLoader.load("img/terrain.png"), 16, 16);

    public Game(){
        super("Minecraft Sky Survival Platformer of Awesomeness");
        setLayout(new BorderLayout());
        add(new GamePanel(), BorderLayout.CENTER);
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Game();
    }

}
