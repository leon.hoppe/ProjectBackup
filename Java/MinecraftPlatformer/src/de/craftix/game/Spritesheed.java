package de.craftix.game;

import java.awt.image.BufferedImage;

public class Spritesheed {

    private int cols;
    private int width;
    private int height;
    private BufferedImage sprite;

    public Spritesheed(int cols, BufferedImage sprite, int width, int height) {
        this.width = width;
        this.height = height;
        this.cols = cols;
        this.sprite = sprite;
    }

    public BufferedImage getTexture(int id){
        int row = (id / cols);
        int col = (id % cols);
        return sprite.getSubimage(col * width, row * height, width, height);
    }

    public BufferedImage getTexture(int col, int row){
        return sprite.getSubimage(col * width, row * height, width, height);
    }

    public int getCols() {return cols;}

}
