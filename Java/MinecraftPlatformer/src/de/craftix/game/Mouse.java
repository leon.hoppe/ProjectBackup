package de.craftix.game;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Mouse implements MouseListener, MouseMotionListener {

    public int x;
    public int y;
    public int conX;
    public int conY;
    public boolean pressed;

    public Mouse(GamePanel panel) {
        panel.addMouseListener(this);
        panel.addMouseMotionListener(this);
    }

    private void setCoordinates(Point p) {
        x = p.x;
        y = p.y;
    }

    private void setConvertedCoordinates(Point p) {
        conX = p.x / GamePanel.SCALE + Playstate.camera.getCamX();
        conY = p.y / GamePanel.SCALE + Playstate.camera.getCamY();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        setConvertedCoordinates(e.getPoint());
        setCoordinates(e.getPoint());

        Playstate.world.getBlock(conX, conY).destroy();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        setConvertedCoordinates(e.getPoint());
        setCoordinates(e.getPoint());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        setConvertedCoordinates(e.getPoint());
        setCoordinates(e.getPoint());
        pressed = true;

        Playstate.world.getBlock(conX, conY).destroy();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        setConvertedCoordinates(e.getPoint());
        setCoordinates(e.getPoint());
        pressed = false;
    }

    //UNUSED
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }


}
