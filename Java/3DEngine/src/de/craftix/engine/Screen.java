package de.craftix.engine;

import javax.swing.*;
import java.awt.*;

public class Screen extends JPanel {
    static double[] viewFrom = new double[]{ 10, 10, 10 };
    static double[] viewTo = new double[]{ 0, 0, 0 };

    public static int numberOfPolygons = 0;
    public static PolygonObject[] polygons = new PolygonObject[100];

    Polygon3D poly3D;

    public Screen() {
        poly3D = new Polygon3D(new double[]{2, 4, 2}, new double[]{2, 4, 6}, new double[]{5, 5, 5}, Color.BLACK);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int i = 0; i < numberOfPolygons; i++) polygons[i].drawPolygon(g);
    }
}
