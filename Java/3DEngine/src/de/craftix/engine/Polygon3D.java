package de.craftix.engine;

import java.awt.*;

public class Polygon3D {
    private Color c;
    private double[] x, y, z;

    public Polygon3D(double[] x, double[] y, double[] z, Color c) {
        this.c = c;
        this.x = x;
        this.y = y;
        this.z = z;
        createPolygon();
    }

    void createPolygon() {
        double[] newX = new double[x.length];
        double[] newY = new double[y.length];
        for (int i = 0; i < x.length; i++) {
            newX[i] = Calculator.CalculatePositionX(Screen.viewFrom, Screen.viewTo, x[i], y[i], z[i]);
            newY[i] = Calculator.CalculatePositionY(Screen.viewFrom, Screen.viewTo, x[i], y[i], z[i]);
        }
        Screen.polygons[Screen.numberOfPolygons] = new PolygonObject(newX, newY, c);
    }

}
