package de.craftix.engine;

import javax.swing.*;
import java.awt.*;

public class Engine extends JFrame {
    private Screen screen = new Screen();

    public Engine() {
        setUndecorated(true);
        setSize(Toolkit.getDefaultToolkit().getScreenSize());
        add(screen);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

}
