package de.craftix.engine;

import java.awt.*;

public class PolygonObject {
    private Polygon p;
    private Color c;

    public PolygonObject(double[] x, double[] y, Color c) {
        Screen.numberOfPolygons++;
        this.c = c;
        p = new Polygon();
        for (int i = 0; i < x.length; i++) p.addPoint((int) x[i], (int) y[i]);
    }

    void  drawPolygon(Graphics g) {
        g.setColor(c);
        g.drawPolygon(p);
    }

}
