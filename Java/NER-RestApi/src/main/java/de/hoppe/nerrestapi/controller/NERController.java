package de.hoppe.nerrestapi.controller;

import de.hoppe.nerrestapi.core.Pipeline;
import de.hoppe.nerrestapi.model.Type;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.CoreDocument;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/v1")
public class NERController {

    @PostMapping
    @RequestMapping("/ner")
    public Set<String> ner(@RequestBody final String input, @RequestParam final Type type) {
        var document = new CoreDocument(input);
        Pipeline.getPipeline().annotate(document);
        var tokens = document.tokens();

        return new HashSet<>(collectList(tokens, type));
    }

    private List<String> collectList(List<CoreLabel> labels, final Type type) {
        return labels.stream()
                .filter(label -> type.getName().equalsIgnoreCase(label.get(CoreAnnotations.NamedEntityTagAnnotation.class)))
                .map(CoreLabel::originalText)
                .collect(Collectors.toList());
    }

}
