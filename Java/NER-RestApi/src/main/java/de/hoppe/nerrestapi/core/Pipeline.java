package de.hoppe.nerrestapi.core;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.Properties;

public final class Pipeline {

    private final static Properties properties = new Properties();
    private final static String propertiesName = "tokenize, ssplit, pos, lemma, ner";
    private static StanfordCoreNLP pipeline;

    private Pipeline() {}

    public static StanfordCoreNLP getPipeline() {
        if (pipeline == null) {
            properties.setProperty("annotators", propertiesName);
            pipeline = new StanfordCoreNLP(properties);
        }

        return pipeline;
    }

}
