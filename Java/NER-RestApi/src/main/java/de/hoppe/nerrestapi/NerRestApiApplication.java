package de.hoppe.nerrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NerRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(NerRestApiApplication.class, args);
    }

}
