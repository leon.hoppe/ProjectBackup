package de.hoppe.nerrestapi.model;

public enum Type {

    PERSON("Person"),
    CITY("City"),
    STATE_OR_PROVINCE("State or Probince"),
    COUNTRY("Country"),
    EMAIL("Email"),
    TITLE("Job title");

    private final String type;

    Type(String type) {
        this.type = type;
    }

    public String getName() {
        return type;
    }
}
