docker-compose down
docker image rm leonhoppe/web-desktop-frontend
docker image rm leonhoppe/web-desktop-backend
git pull
docker build -t leonhoppe/web-desktop-frontend WebDesktopFrontend
docker build -t leonhoppe/web-desktop-backend WebDesktopBackend
docker-compose up -d