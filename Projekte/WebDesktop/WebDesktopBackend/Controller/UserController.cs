﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebDesktopBackend.Contract;
using WebDesktopBackend.Contract.Logic;
using WebDesktopBackend.Entitys.Tokens;
using WebDesktopBackend.Entitys.User;
using WebDesktopBackend.LogicResults;
using WebDesktopBackend.Security;
using WebDesktopBackend.Security.Authorization;

namespace WebDesktopBackend.Controller {
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase {
        private readonly IUserLogic _logic;
        private readonly ITokenContext _context;

        public UserController(IUserLogic logic, ITokenContext context) {
            _logic = logic;
            _context = context;
        }
        
        [HttpPut("login")]
        public ActionResult<AccessTokenResponse> Login([FromBody] UserLogin login) {
            ILogicResult<Tokens> result = _logic.Login(login);
            if (result.State == LogicResultState.Ok) SetRefreshToken(result.Data.refreshToken);
            return this.FromLogicResult(new LogicResult<AccessTokenResponse> {State = result.State, Data = new AccessTokenResponse {Id = result.Data?.accessToken.Id}});
        }

        [HttpPost("register")]
        public ActionResult<AccessTokenResponse> Register([FromBody] UserEditor editor) {
            ILogicResult<Tokens> result = _logic.Register(editor);
            SetRefreshToken(result.Data.refreshToken);
            return this.FromLogicResult(new LogicResult<AccessTokenResponse> {State = result.State, Data = new AccessTokenResponse {Id = result.Data.accessToken.Id}});
        }

        [HttpDelete("logout")]
        [Authorized]
        public ActionResult Logout() {
            DeleteRefreshToken();
            return this.FromLogicResult(_logic.Logout());
        }

        [HttpPut("{id}")]
        [Authorized(Permissions.EditUsers)]
        public ActionResult EditUser(string id, [FromBody] UserEditor editor) {
            return this.FromLogicResult(_logic.EditUser(id, editor));
        }

        [HttpDelete("{id}")]
        [Authorized(Permissions.DeleteUsers)]
        public ActionResult DeleteUser(string id) {
            return this.FromLogicResult(_logic.DeleteUser(id));
        }

        [HttpGet("{id}")]
        [Authorized(Permissions.ShowUsers)]
        public ActionResult<User> GetUser(string id) {
            return this.FromLogicResult(_logic.GetUser(id));
        }

        [HttpGet]
        [Authorized(Permissions.ShowUsers)]
        public ActionResult<User[]> GetUsers() {
            return this.FromLogicResult(_logic.GetUsers());
        }

        [HttpGet("validate")]
        [Authorized]
        public ActionResult Validate() {
            return this.FromLogicResult(_logic.Valdiate());
        }

        [HttpGet("token")]
        public ActionResult<AccessTokenResponse> GetToken() {
            return this.FromLogicResult(_logic.GetToken(GetRefreshToken()));
        }

        [HttpGet("ownuser")]
        [Authorized]
        public ActionResult<User> GetOwnUser() {
            return this.FromLogicResult(_logic.GetOwnUser());
        }
        
        [HttpPut("ownuser")]
        [Authorized]
        public ActionResult<User> EditOwnUser([FromBody] UserEditor editor) {
            return this.FromLogicResult(_logic.EditUser(_context.UserId, editor));
        }
        
        [HttpDelete("ownuser")]
        [Authorized]
        public ActionResult<User> DeleteOwnUser() {
            Logout();
            return this.FromLogicResult(_logic.DeleteUser(_context.UserId));
        }

        [HttpGet("{id}/permissions")]
        [Authorized(Permissions.EditUserPermissions)]
        public ActionResult<string[]> GetPermissions(string id) {
            return this.FromLogicResult(_logic.GetPermissions(id));
        }
        
        [HttpGet("{id}/permissions/raw")]
        [Authorized(Permissions.EditUserPermissions)]
        public ActionResult<string[]> GetRawPermissions(string id) {
            return this.FromLogicResult(_logic.GetRawPermissions(id));
        }
        
        [HttpGet("permissions")]
        [Authorized]
        public ActionResult<string[]> GetPermissions() {
            return this.FromLogicResult(_logic.GetPermissions(HttpContext.User.GetUserId()));
        }

        [HttpPost("{id}/permissions/{permission}")]
        [Authorized(Permissions.EditUserPermissions)]
        public ActionResult AddPermission(string id, string permission) {
            return this.FromLogicResult(_logic.AddPermission(id, permission));
        }

        [HttpDelete("{id}/permissions/{permission}")]
        [Authorized(Permissions.EditUserPermissions)]
        public ActionResult DeletePermission(string id, string permission) {
            return this.FromLogicResult(_logic.DeletePermission(id, permission));
        }

        private void DeleteRefreshToken()
        {
            HttpContext.Response.Cookies.Delete("refresh_token");
        }
        private void SetRefreshToken(RefreshToken token)
        {
            HttpContext.Response.Cookies.Append("refresh_token", token.Id, new CookieOptions()
            {
                MaxAge = token.ExpirationDate - DateTime.Now,
                HttpOnly = true,
                Secure = true
            });
        }

        private string GetRefreshToken() {
            return HttpContext.Request.Cookies["refresh_token"];
        }
    }
}