﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebDesktopBackend.Extentions;
using WebDesktopBackend.Security.Authorization;

namespace WebDesktopBackend.Controller {
    
    [ApiController]
    [Route("update")]
    public class UpdateController : ControllerBase {

        [HttpGet("test")]
        public ActionResult Test() {
            return Ok("Authorized");
        }

        [HttpGet]
        [Authorized("group.admin")]
        public async Task Update() {
            if (HttpContext.WebSockets.IsWebSocketRequest) {
                using var socket = await HttpContext.WebSockets.AcceptWebSocketAsync();
                using var target = await new ClientWebSocket().ConnectAsync(new Uri("ws://213.136.89.237:4042"));

                var t1 = socket.AddMessageEventHandler(msg => {
                    target.SendMessage(msg);
                });
                var t2 = target.AddMessageEventHandler(msg => {
                    socket.SendMessage(msg);
                });

                while (!socket.CloseStatus.HasValue) {
                    await Task.Delay(500);
                }

                t1.Cancel();
                t2.Cancel();
                await target.CloseAsync(WebSocketCloseStatus.NormalClosure, null, CancellationToken.None);
            } else {
                HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
        }
        
    }
    
}