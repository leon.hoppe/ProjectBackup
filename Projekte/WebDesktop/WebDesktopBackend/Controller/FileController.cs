﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebDesktopBackend.Contract.Logic;
using WebDesktopBackend.Entitys.Files;
using WebDesktopBackend.LogicResults;
using WebDesktopBackend.Security.Authorization;
using FileShare = WebDesktopBackend.Entitys.Files.FileShare;

namespace WebDesktopBackend.Controller {
    [ApiController]
    [Route("files")]
    public class FileController : ControllerBase {
        private readonly IFileLogic _fileLogic;

        public FileController(IFileLogic fileLogic) {
            _fileLogic = fileLogic;
        }

        [HttpPost("upload/directory")]
        [Authorized]
        public ActionResult CreateDirectory([FromQuery] string directory, [FromQuery] string name) {
            return this.FromLogicResult(_fileLogic.CreateDirectory(directory, name));
        }
        
        [HttpPost("upload/file")]
        [Authorized]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> UploadFile() {
            try {
                return this.FromLogicResult(await _fileLogic.UploadFile(Request.Form));
            } catch (Exception) {
                return StatusCode((int)HttpStatusCode.BadRequest, "File upload Interupted");
            }
        }

        [HttpPost("upload/json")]
        [Authorized]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> UploadJson([FromQuery] string directory, [FromQuery] string name) {
            using var reader = new StreamReader(Request.Body, Encoding.UTF8);
            string content = await reader.ReadToEndAsync();
            return this.FromLogicResult(await _fileLogic.UploadJson(directory, name, content));
        }

        [HttpGet("download/file")]
        [Authorized]
        public IActionResult DownloadFile([FromQuery] string directory, [FromQuery] string file) {
            var result = _fileLogic.DownloadFile(directory, file);
            if (!result.IsSuccessful)
                return this.FromLogicResult(result);

            return File(result.Data, "APPLICATION/octet-stream", file);
        }

        [HttpGet("download/json")]
        [Authorized]
        public async Task<ActionResult<string>> DownloadJson([FromQuery] string file) {
            return this.FromLogicResult(await _fileLogic.DownloadJson(file));
        }

        [HttpGet("content")]
        [Authorized]
        public ActionResult<DirectoryContent> GetDirectoryContent([FromQuery] string directory) {
            return this.FromLogicResult(_fileLogic.GetDirectory(directory));
        }

        [HttpGet("info/directory")]
        [Authorized]
        public ActionResult<DirectoryInformation> GetDirectoryInformation([FromQuery] string directory) {
            return this.FromLogicResult(_fileLogic.GetDirectoryInformation(directory));
        }
        
        [HttpGet("info/file")]
        [Authorized]
        public ActionResult<DirectoryInformation> GetFileInformation([FromQuery] string directory, [FromQuery] string file) {
            return this.FromLogicResult(_fileLogic.GetFileInformation(directory, file));
        }

        [HttpPut("move/directory")]
        [Authorized]
        public ActionResult MoveDirectory([FromQuery] string directory, [FromQuery] string name, [FromQuery] string to) {
            return this.FromLogicResult(_fileLogic.MoveDirectory(directory, name, to));
        }
        
        [HttpPut("move/file")]
        [Authorized]
        public ActionResult MoveFile([FromQuery] string directory, [FromQuery] string file, [FromQuery] string to) {
            return this.FromLogicResult(_fileLogic.MoveFile(directory, file, to));
        }

        [HttpDelete("delete")]
        [Authorized]
        public ActionResult DeleteFile([FromQuery] string url) {
            return this.FromLogicResult(_fileLogic.Delete(url));
        }

        [HttpGet("share")]
        [Authorized]
        public ActionResult<FileShare> ShareFile([FromQuery] string url) {
            return this.FromLogicResult(_fileLogic.Share(url));
        }

    }
}