using Microsoft.AspNetCore.Http;
using WebDesktopBackend.Security.Authorization;

namespace WebDesktopBackend.Security
{
    internal class TokenContext : ITokenContext
    {
        private readonly IHttpContextAccessor _accessor;

        public TokenContext(IHttpContextAccessor accessor) {
            _accessor = accessor;
        }

        public bool IsAuthenticated => _accessor.HttpContext.User.Identity.IsAuthenticated;

        public string UserId => _accessor.HttpContext?.User.GetUserId();

        public string AccessTokenId => _accessor.HttpContext?.User.GetAccessTokenId();

        public string RefreshTokenId => _accessor.HttpContext?.User.GetRefreshTokenId();

        public string[] Permissions => _accessor.HttpContext?.User.GetPermissions();
    }
}