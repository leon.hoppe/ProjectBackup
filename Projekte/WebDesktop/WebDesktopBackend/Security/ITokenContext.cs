namespace WebDesktopBackend.Security
{
    public interface ITokenContext {
        bool IsAuthenticated {get;}
        string UserId {get;}
        string AccessTokenId {get;}
        string RefreshTokenId {get;}
        string[] Permissions { get; }
    }
}