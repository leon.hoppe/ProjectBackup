﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace WebDesktopBackend.Security.Authorization
{
    public class AuthorizedFilter : IAuthorizationFilter
    {
        private readonly string[] _permissions;
        
        public AuthorizedFilter(params string[] permissions)
        {
            _permissions = permissions;
        }
        
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (EndpointHasAllowAnonymousFilter(context))
            {
                return;
            }

            if (!IsAuthenticated(context))
            {
                context.Result = new UnauthorizedResult();
                return;
            }
            
            if (!ContainsRequiredRole(context))
            {
                context.Result = new ForbidResult();
                return;
            }
        }

        private static bool EndpointHasAllowAnonymousFilter(AuthorizationFilterContext context)
        {
            return context.Filters.Any(item => item is IAllowAnonymousFilter);
        }

        private bool IsAuthenticated(AuthorizationFilterContext context)
        {
            return context.HttpContext.User.Identity.IsAuthenticated;
        }
        
        private bool ContainsRequiredRole(AuthorizationFilterContext context) {
            if (_permissions.Length == 0)
                return true;
            
            if (context.HttpContext.User.HasClaim(CustomClaimTypes.Permission, "*"))
                return true;

            foreach (var permission in _permissions) {
                string[] splice = permission.Split(".");
                string cache = "";
                foreach (var s in splice) {
                    cache += s + ".";
                    if (context.HttpContext.User.HasClaim(CustomClaimTypes.Permission, cache + "*"))
                        return true;
                }
            }
            
            return false;
        }
    }
}