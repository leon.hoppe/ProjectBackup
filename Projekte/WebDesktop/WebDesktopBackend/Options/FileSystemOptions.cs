﻿namespace WebDesktopBackend.Options {
    public class FileSystemOptions : OptionsFromConfiguration {
        public override string Position => "FileSystem";
        
        public string RootDirectory { get; set; }
        public int MaxSizePerUserInMb { get; set; }
    }
}