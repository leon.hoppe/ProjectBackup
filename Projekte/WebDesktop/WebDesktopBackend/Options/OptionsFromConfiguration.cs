﻿namespace WebDesktopBackend.Options
{
    public abstract class OptionsFromConfiguration
    {
        public abstract string Position { get; }
    }
}