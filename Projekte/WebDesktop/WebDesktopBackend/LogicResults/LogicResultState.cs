﻿namespace WebDesktopBackend.LogicResults {
    public enum LogicResultState {
        Ok,
        BadRequest,
        Forbidden,
        NotFound,
        Conflict
    }
}