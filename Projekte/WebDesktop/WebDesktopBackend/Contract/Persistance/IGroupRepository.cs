﻿using WebDesktopBackend.Entitys.Permissions;

namespace WebDesktopBackend.Contract.Persistance {
    public interface IGroupRepository {
        PermissionGroup GetPermissionGroup(string name);
        PermissionGroup[] GetGroupsFromUser(string userId);
        PermissionGroup[] ExtractGroups(Permission[] permissions);
        Permission[] GetUserPermissions(string id);
    }
}