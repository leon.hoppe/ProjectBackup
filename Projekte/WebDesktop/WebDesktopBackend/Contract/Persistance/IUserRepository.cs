﻿using WebDesktopBackend.Entitys.User;

namespace WebDesktopBackend.Contract.Persistance {
    public interface IUserRepository {
        User AddUser(UserEditor editor);
        void EditUser(string id, UserEditor editor);
        void DeleteUser(string id);
        User GetUser(string id);
        User GetUserByUsername(string username);
        User GetUserByEmail(string email);
        User GetUserFromLogin(UserLogin login);
        User[] GetUsers();
        bool Login(UserLogin login);
    }
}