﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebDesktopBackend.Entitys.Files;

namespace WebDesktopBackend.Contract.Persistance {
    public interface IFileRepository {
        void InitUser(string userId);
        void DeleteUserFolder(string userId);
        bool CreateDirectory(string directory, string name);
        Task UploadFile(IFormFile file, string directory);
        Task UploadJson(string directory, string name, string data);
        FileStream DownloadFile(string path);
        Task<string> DownloadJson(string file);
        DirectoryContent GetDirectory(string directory);
        DirectoryInformation GetDirectoryInformation(string directory);
        FileInformation GetFileInformation(string file);
        void MoveDirectory(string directory, string to);
        void MoveFile(string file, string to);
        void Delete(string url);
        string GenerateShareId(string url, string owner);
    }
}