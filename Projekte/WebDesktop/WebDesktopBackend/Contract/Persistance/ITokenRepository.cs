﻿using WebDesktopBackend.Entitys.Permissions;
using WebDesktopBackend.Entitys.Tokens;

namespace WebDesktopBackend.Contract.Persistance {
    public interface ITokenRepository {
        RefreshToken GetRefreshToken(string id);
        AccessToken GetAccessToken(string id);
        bool ValidateAccessToken(string id);
        bool ValidateRefreshToken(string id);
        RefreshToken CreateRefreshToken(string userId);
        AccessToken CreateAccessToken(string refreshTokenId);
        void DeleteUserTokens(string id);
        void DeleteRefreshToken(string id);
        Permission[] GetUserPermissions(string id);
        void AddPermission(string id, string permission);
        void DeletePermission(string id, string permission);
    }
}