﻿using WebDesktopBackend.Entitys.Tokens;
using WebDesktopBackend.Entitys.User;
using WebDesktopBackend.LogicResults;

namespace WebDesktopBackend.Contract.Logic {
    public interface IUserLogic {
        ILogicResult<Tokens> Login(UserLogin login);
        ILogicResult<Tokens> Register(UserEditor editor);
        ILogicResult Logout();
        ILogicResult EditUser(string id, UserEditor editor);
        ILogicResult DeleteUser(string id);
        ILogicResult<User> GetUser(string id);
        ILogicResult<User[]> GetUsers();
        ILogicResult Valdiate();
        ILogicResult<AccessTokenResponse> GetToken(string refreshTokenId);
        ILogicResult<User> GetOwnUser();
        ILogicResult<string[]> GetPermissions(string id);
        ILogicResult<string[]> GetRawPermissions(string id);
        ILogicResult AddPermission(string id, string permission);
        ILogicResult DeletePermission(string id, string permission);
    }
}