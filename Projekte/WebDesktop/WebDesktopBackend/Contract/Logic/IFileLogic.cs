﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebDesktopBackend.Entitys.Files;
using WebDesktopBackend.LogicResults;
using FileShare = WebDesktopBackend.Entitys.Files.FileShare;

namespace WebDesktopBackend.Contract.Logic {
    public interface IFileLogic {
        ILogicResult CreateDirectory(string directory, string name);
        Task<ILogicResult> UploadFile(IFormCollection data);
        Task<ILogicResult> UploadJson(string directory, string name, string content);
        ILogicResult<FileStream> DownloadFile(string directory, string file);
        Task<ILogicResult<string>> DownloadJson(string file);
        ILogicResult<DirectoryContent> GetDirectory(string directory);
        ILogicResult<DirectoryInformation> GetDirectoryInformation(string directory);
        ILogicResult<FileInformation> GetFileInformation(string directory, string file);
        ILogicResult MoveDirectory(string directory, string name, string to);
        ILogicResult MoveFile(string directory, string file, string to);
        ILogicResult Delete(string url);
        ILogicResult<FileShare> Share(string url);
    }
}