﻿namespace WebDesktopBackend.Contract {
    public class Permissions {
        // --- USERS ---
        public const string ShowUsers = "users.show";
        public const string EditUsers = "users.edit";
        public const string DeleteUsers = "users.delete";
        public const string EditUserPermissions = "users.permissions";
        
        // --- PROGRAMS ---
        public const string UseAdminProgram = "app.admin.use";
    }
}