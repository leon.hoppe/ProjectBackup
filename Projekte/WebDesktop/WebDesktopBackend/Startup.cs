using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using WebDesktopBackend.Contract.Logic;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Controller;
using WebDesktopBackend.Logic;
using WebDesktopBackend.Options;
using WebDesktopBackend.Persistance;
using WebDesktopBackend.Security;
using WebDesktopBackend.Security.Authentication;

namespace WebDesktopBackend {
    public class Startup {

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
            CreateTables();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddDbContext<DatabaseContext>();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ITokenContext, TokenContext>();

            //Repositorys
            services.AddScoped<ITokenRepository, TokenRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IGroupRepository, GroupRepository>();

            //Logic
            services.AddScoped<IUserLogic, UserLogic>();
            services.AddScoped<IFileLogic, FileLogic>();

            services.AddOptionsFromConfiguration<JwtTokenAuthenticationOptions>(Configuration);
            services.AddOptionsFromConfiguration<FileSystemOptions>(Configuration);
            services.AddCors();
            services.AddAuthentication(JwtTokenAuthentication.Scheme).AddJwtTokenAuthentication(Configuration);
            services.AddControllers();
            services.Configure<FormOptions>(x => {
                x.KeyLengthLimit = int.MaxValue;
                x.ValueCountLimit = int.MaxValue;
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = long.MaxValue;
                x.BufferBodyLengthLimit = long.MaxValue;
                x.MultipartBoundaryLengthLimit = int.MaxValue;
                x.MultipartHeadersCountLimit = int.MaxValue;
                x.MultipartHeadersLengthLimit = int.MaxValue;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsProduction()) {
                app.UseHttpsRedirection();
            }

            app.UseCors(
                options => options
                    .WithOrigins(Configuration.GetSection("WebServer:Origins").Get<string[]>())
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
            );

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseWebSockets();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private void CreateTables() {
            using var mySql = new MySql();
            mySql.Insert("CREATE TABLE IF NOT EXISTS Users (Id VARCHAR(50) PRIMARY KEY, FirstName VARCHAR(255), LastName VARCHAR(255), Email VARCHAR(255), Username VARCHAR(255), Password VARCHAR(255), Created TIMESTAMP)");
            mySql.Insert("CREATE TABLE IF NOT EXISTS RefreshTokens (Id VARCHAR(50) PRIMARY KEY, UserId VARCHAR(50), ExpirationDate TIMESTAMP)");
            mySql.Insert("CREATE TABLE IF NOT EXISTS AccessTokens (Id VARCHAR(50) PRIMARY KEY, RefreshTokenId VARCHAR(50), ExpirationDate TIMESTAMP)");
            mySql.Insert("CREATE TABLE IF NOT EXISTS Permissions (Id INT PRIMARY KEY AUTO_INCREMENT, UserId VARCHAR(50), PermissionName VARCHAR(100), Type INT(1))");
            mySql.Insert("CREATE TABLE IF NOT EXISTS FileShares (Id VARCHAR(50) PRIMARY KEY, Owner VARCHAR(50), Files TEXT)");
        }
        
    }
}