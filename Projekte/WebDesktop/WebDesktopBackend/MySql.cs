﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace WebDesktopBackend {
    public class MySql : IDisposable {
        public const string ConnectionString = "SERVER=213.136.89.237;DATABASE=WebDesktop;UID=WebDesktop;PASSWORD=Hft6bP@V3IkYvqS1";
        private readonly MySqlConnection _connection;
        private readonly List<string> _querys;

        public MySql() {
            _querys = new List<string>();
            _connection = new MySqlConnection(ConnectionString);
            _connection.Open();
        }

        public void Insert(string qry) {
            if (!qry.EndsWith(";")) qry += ";";
            _querys.Add(qry);
        }

        public void Dispose() {
            MySqlCommand cmd = new MySqlCommand(string.Join(" ", _querys), _connection);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            
            _connection?.Dispose();
        }
    }
}