﻿namespace WebDesktopBackend.Entitys.User {
    public class UserEditor {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public void EditUser(User user) {
            Trim();
            user.FirstName = string.IsNullOrEmpty(FirstName) ? user.FirstName : FirstName;
            user.LastName = string.IsNullOrEmpty(LastName) ? user.LastName : LastName;
            user.Email = string.IsNullOrEmpty(Email) ? user.Email : Email;
            user.Username = string.IsNullOrEmpty(Username) ? user.Username : Username;
            user.Password = string.IsNullOrEmpty(Password) ? user.Password : Password;
        }

        public void Trim() {
            FirstName = FirstName.Trim();
            LastName = LastName.Trim();
            Email = Email.Trim();
            Username = Username.Trim();
            Password = Password.Trim();
        }
    }
}