﻿using System;

namespace WebDesktopBackend.Entitys.User {
    public class User : UserEditor {
        public string Id { get; set; }
        public DateTime Created { get; set; }

        public User CreateCopy() {
            return new User {
                Id = Id,
                Created = Created,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                Username = Username,
                Password = "ENCRYPTED"
            };
        }
    }
}