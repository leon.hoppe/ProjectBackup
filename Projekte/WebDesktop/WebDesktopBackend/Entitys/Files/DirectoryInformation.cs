﻿using System;

namespace WebDesktopBackend.Entitys.Files {
    public class DirectoryInformation {
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public long Size { get; set; }
    }
}