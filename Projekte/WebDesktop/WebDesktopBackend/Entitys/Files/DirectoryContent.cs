﻿namespace WebDesktopBackend.Entitys.Files {
    public class DirectoryContent {
        public string[] Files { get; set; }
        public string[] Directories { get; set; }
    }
}