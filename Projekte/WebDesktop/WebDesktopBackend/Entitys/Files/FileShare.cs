namespace WebDesktopBackend.Entitys.Files {
    public class FileShare {
        public string Id { get; set; }
        public string Owner { get; set; }
        public string File { get; set; }
    }
}