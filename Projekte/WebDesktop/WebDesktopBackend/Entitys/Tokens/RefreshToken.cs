﻿using System;

namespace WebDesktopBackend.Entitys.Tokens {
    public class RefreshToken {
        public string Id { get; set; }
        public string UserId { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}