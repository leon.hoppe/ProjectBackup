﻿namespace WebDesktopBackend.Entitys.Tokens {
    public class Tokens {
        public RefreshToken refreshToken { get; set; }
        public AccessToken accessToken { get; set; }
    }
}