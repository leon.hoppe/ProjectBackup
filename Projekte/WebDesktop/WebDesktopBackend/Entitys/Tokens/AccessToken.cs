﻿using System;

namespace WebDesktopBackend.Entitys.Tokens {
    public class AccessToken {
        public string Id { get; set; }
        public string RefreshTokenId { get; set; }
        public DateTime ExpirationDate { get; set; }
    }

    public class AccessTokenResponse {
        public string Id { get; set; }
    }
}