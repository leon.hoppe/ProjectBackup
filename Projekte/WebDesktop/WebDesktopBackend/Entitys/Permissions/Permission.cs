﻿namespace WebDesktopBackend.Entitys.Permissions {
    public class Permission {
        public const int NotSet = 0;
        public const int Allow = 1;
        public const int Deny = 2;
        
        public int Id { get; set; }
        public string UserId { get; set; }
        public string PermissionName { get; set; }
        public int Type { get; set; }
    }
}