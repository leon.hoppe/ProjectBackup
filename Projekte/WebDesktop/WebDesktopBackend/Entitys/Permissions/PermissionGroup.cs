﻿namespace WebDesktopBackend.Entitys.Permissions {
    public class PermissionGroup {
        public string Permission { get; set; }
        public string Name { get; set; }
        public string[] Permissions { get; set; }
        public string[] Inherits { get; set; }
    }
}