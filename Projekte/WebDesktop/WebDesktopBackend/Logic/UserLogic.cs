﻿using System.Linq;
using WebDesktopBackend.Contract.Logic;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Entitys.Tokens;
using WebDesktopBackend.Entitys.User;
using WebDesktopBackend.LogicResults;
using WebDesktopBackend.Security;

namespace WebDesktopBackend.Logic {
    public class UserLogic : IUserLogic {
        private readonly IUserRepository _users;
        private readonly ITokenRepository _tokens;
        private readonly IGroupRepository _groups;
        private readonly IFileRepository _files;
        private readonly ITokenContext _context;

        public UserLogic(IUserRepository users, ITokenRepository tokens, ITokenContext context, IGroupRepository groups, IFileRepository files) {
            _users = users;
            _tokens = tokens;
            _context = context;
            _groups = groups;
            _files = files;
        }
        
        public ILogicResult<Tokens> Login(UserLogin login) {
            if (!_users.Login(login)) return LogicResult<Tokens>.Conflict();
            User user = _users.GetUserFromLogin(login);
            _tokens.DeleteUserTokens(_context.UserId);
            RefreshToken refreshToken = _tokens.CreateRefreshToken(user.Id);
            AccessToken accessToken = _tokens.CreateAccessToken(refreshToken.Id);
            return LogicResult<Tokens>.Ok(new Tokens {refreshToken = refreshToken, accessToken = accessToken});
        }

        public ILogicResult<Tokens> Register(UserEditor editor) {
            editor.Trim();
            if (!ValidateUserdata(editor)) return LogicResult<Tokens>.BadRequest();
            User user = _users.AddUser(editor);
            _files.InitUser(user.Id);
            RefreshToken refreshToken = _tokens.CreateRefreshToken(user.Id);
            AccessToken accessToken = _tokens.CreateAccessToken(refreshToken.Id);
            return LogicResult<Tokens>.Ok(new Tokens {refreshToken = refreshToken, accessToken = accessToken});
        }

        public ILogicResult Logout() {
            _tokens.DeleteRefreshToken(_context.RefreshTokenId);
            return LogicResult.Ok();
        }

        public ILogicResult EditUser(string id, UserEditor editor) {
            editor.Trim();
            if (!ValidateEdit(editor)) return LogicResult.BadRequest();
            if (_users.GetUser(id) == null) return LogicResult.NotFound();
            _users.EditUser(id, editor);
            return LogicResult.Ok();
        }

        public ILogicResult DeleteUser(string id) {
            _tokens.DeleteUserTokens(id);
            _users.DeleteUser(id);
            _files.DeleteUserFolder(id);
            return LogicResult.Ok();
        }

        public ILogicResult<User> GetUser(string id) {
            User user = _users.GetUser(id);
            if (user == null) return LogicResult<User>.NotFound();
            return LogicResult<User>.Ok(user.CreateCopy());
        }

        public ILogicResult<User[]> GetUsers() {
            User[] users = _users.GetUsers();
            User[] exports = new User[users.Length];
            for (var i = 0; i < users.Length; i++) {
                exports[i] = users[i].CreateCopy();
            }
            
            return LogicResult<User[]>.Ok(exports);
        }

        public ILogicResult Valdiate() {
            if (string.IsNullOrEmpty(_context.RefreshTokenId) || string.IsNullOrEmpty(_context.AccessTokenId)) return LogicResult.Forbidden();
            if (!_tokens.ValidateRefreshToken(_context.RefreshTokenId)) {
                _tokens.DeleteRefreshToken(_context.RefreshTokenId);
                return LogicResult.Forbidden();
            }
            return _tokens.ValidateAccessToken(_context.AccessTokenId) ? LogicResult.Ok() : LogicResult.Forbidden();
        }

        public ILogicResult<AccessTokenResponse> GetToken(string refreshTokenId) {
            if (refreshTokenId == null) return LogicResult<AccessTokenResponse>.Forbidden();
            if (!_tokens.ValidateRefreshToken(refreshTokenId)) {
                _tokens.DeleteRefreshToken(refreshTokenId);
                return LogicResult<AccessTokenResponse>.Forbidden();
            }
            return LogicResult<AccessTokenResponse>.Ok(new AccessTokenResponse {Id = _tokens.CreateAccessToken(refreshTokenId).Id});
        }

        public ILogicResult<User> GetOwnUser() {
            return LogicResult<User>.Ok(_users.GetUser(_context.UserId).CreateCopy());
        }

        public ILogicResult<string[]> GetPermissions(string id) {
            return LogicResult<string[]>.Ok(_groups.GetUserPermissions(id).Select(perm => perm.PermissionName).ToArray());
        }
        
        public ILogicResult<string[]> GetRawPermissions(string id) {
            return LogicResult<string[]>.Ok(_tokens.GetUserPermissions(id).Select(perm => perm.PermissionName).ToArray());
        }

        public ILogicResult AddPermission(string id, string permission) {
            _tokens.AddPermission(id, permission);
            return LogicResult.Ok();
        }

        public ILogicResult DeletePermission(string id, string permission) {
            _tokens.DeletePermission(id, permission);
            return LogicResult.Ok();
        }

        private bool ValidateUserdata(UserEditor editor) {
            if (string.IsNullOrEmpty(editor.FirstName)) return false;
            if (string.IsNullOrEmpty(editor.LastName)) return false;
            if (string.IsNullOrEmpty(editor.Email)) return false;
            if (string.IsNullOrEmpty(editor.Username)) return false;
            if (string.IsNullOrEmpty(editor.Password)) return false;

            if (editor.FirstName.Length > 255) return false;
            if (editor.LastName.Length > 255) return false;
            if (editor.Email.Length > 255) return false;
            if (editor.Username.Length > 255) return false;
            if (editor.Password.Length > 255) return false;

            if (!editor.Email.Contains('@') || !editor.Email.Contains('.')) return false;
            if (editor.Username.Contains('@')) return false;
            if (editor.Password.Length < 8) return false;
            if (_users.GetUserByUsername(editor.Username) != null) return false;
            if (_users.GetUserByEmail(editor.Email) != null) return false;

            return true;
        }

        private bool ValidateEdit(UserEditor editor) {
            if (editor.FirstName.Length > 255) return false;
            if (editor.LastName.Length > 255) return false;
            if (editor.Email.Length > 255) return false;
            if (editor.Username.Length > 255) return false;
            if (editor.Password.Length > 255) return false;

            if (!string.IsNullOrEmpty(editor.Email)) {
                if (!editor.Email.Contains('@') || !editor.Email.Contains('.')) return false;
                if (_users.GetUserByEmail(editor.Email) != null) return false;
            }

            if (!string.IsNullOrEmpty(editor.Username)) {
                if (editor.Username.Contains('@')) return false;
                if (_users.GetUserByUsername(editor.Username) != null) return false;
            }

            if (!string.IsNullOrEmpty(editor.Password)) {
                if (editor.Password.Length < 8) return false;
            }

            return true;
        }
    }
}