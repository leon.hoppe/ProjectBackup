﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using WebDesktopBackend.Contract.Logic;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Entitys.Files;
using WebDesktopBackend.Extentions;
using WebDesktopBackend.LogicResults;
using WebDesktopBackend.Options;
using WebDesktopBackend.Security;
using FileShare = WebDesktopBackend.Entitys.Files.FileShare;

namespace WebDesktopBackend.Logic {
    public class FileLogic : IFileLogic {
        private readonly IFileRepository _fileRepository;
        private readonly ITokenContext _context;
        private readonly FileSystemOptions _options;

        public FileLogic(IFileRepository fileRepository, ITokenContext context, IOptions<FileSystemOptions> options) {
            _fileRepository = fileRepository;
            _context = context;
            _options = options.Value;
        }

        public ILogicResult CreateDirectory(string directory, string name) {
            bool success = _fileRepository.CreateDirectory(_options.RootDirectory + _context.UserId + Clean(directory), Clean(name));
            if (success) return LogicResult.Ok();
            return LogicResult.Conflict();
        }

        public async Task<ILogicResult> UploadFile(IFormCollection data) {
            IFormFile file = data.Files[0];
            if (!CheckUserDirectorySize(file.Length)) return LogicResult.Forbidden("Max Directory size reached");
            
            string dir = _options.RootDirectory + _context.UserId + Clean(data["directory"]);
            await _fileRepository.UploadFile(file, dir);
            return LogicResult.Ok();
        }

        public async Task<ILogicResult> UploadJson(string directory, string name, string content) {
            await _fileRepository.UploadJson(_options.RootDirectory + _context.UserId + Clean(directory), Clean(name) + ".json", content);
            return LogicResult.Ok();
        }

        public ILogicResult<FileStream> DownloadFile(string directory, string file) {
            string path = _options.RootDirectory + _context.UserId + Clean(directory) + "/" + Clean(file);
            if (!new FileInfo(path).Exists)
                return LogicResult<FileStream>.NotFound();

            return LogicResult<FileStream>.Ok(_fileRepository.DownloadFile(path));
        }

        public async Task<ILogicResult<string>> DownloadJson(string file) {
            string path = _options.RootDirectory + _context.UserId + Clean(file);
            if (!new FileInfo(path).Exists)
                return LogicResult<string>.NotFound();

            return LogicResult<string>.Ok(await _fileRepository.DownloadJson(path));
        }

        public ILogicResult<DirectoryContent> GetDirectory(string directory) {
            string path = _options.RootDirectory + _context.UserId + Clean(directory);
            if (!new DirectoryInfo(path).Exists)
                return LogicResult<DirectoryContent>.NotFound();
            
            return LogicResult<DirectoryContent>.Ok(_fileRepository.GetDirectory(path));
        }

        public ILogicResult<DirectoryInformation> GetDirectoryInformation(string directory) {
            string path = _options.RootDirectory + _context.UserId + Clean(directory);
            if (!new DirectoryInfo(path).Exists)
                return LogicResult<DirectoryInformation>.NotFound();

            return LogicResult<DirectoryInformation>.Ok(_fileRepository.GetDirectoryInformation(path));
        }

        public ILogicResult<FileInformation> GetFileInformation(string directory, string file) {
            string path = _options.RootDirectory + _context.UserId + Clean(directory) + "/" + Clean(file);
            if (!new FileInfo(path).Exists)
                return LogicResult<FileInformation>.NotFound();
            
            return LogicResult<FileInformation>.Ok(_fileRepository.GetFileInformation(path));
        }

        public ILogicResult MoveDirectory(string directory, string name, string to) {
            string path = _options.RootDirectory + _context.UserId + Clean(directory) + Clean(name);
            if (!new DirectoryInfo(path).Exists)
                return LogicResult.NotFound();
            
            to = _options.RootDirectory + _context.UserId + to;
            if (!new DirectoryInfo(to).Exists)
                return LogicResult.NotFound();

            _fileRepository.MoveDirectory(path, to + "/" + Clean(name));
            return LogicResult.Ok();
        }

        public ILogicResult MoveFile(string directory, string file, string to) {
            string path = _options.RootDirectory + _context.UserId + Clean(directory) + Clean(file);
            if (!new FileInfo(path).Exists)
                return LogicResult.NotFound();

            to = _options.RootDirectory + _context.UserId + to;
            if (!new DirectoryInfo(to).Exists)
                return LogicResult.NotFound();
            
            _fileRepository.MoveFile(path, to + "/" + Clean(file));
            return LogicResult.Ok();
        }

        public ILogicResult Delete(string url) {
            _fileRepository.Delete(_options.RootDirectory + _context.UserId + Clean(url));
            return LogicResult.Ok();
        }

        public ILogicResult<FileShare> Share(string url) {
            string share = _fileRepository.GenerateShareId(Clean(url), _context.UserId);

            if (share != null) {
                var result = new FileShare() {
                    Id = share,
                    Owner = _context.UserId,
                    File = url
                };

                return LogicResult<FileShare>.Ok(result);
            }
            
            return LogicResult<FileShare>.Conflict();
        }

        private bool CheckUserDirectorySize(long fileSize = 0) {
            DirectoryInfo info = new DirectoryInfo(_options.RootDirectory + _context.UserId);
            if (!info.Exists) return true;
            if (info.GetDirectorySize() > _options.MaxSizePerUserInMb * 1000000 - fileSize) return false;
            return true;
        }

        private string Clean(in string input) {
            return input.Replace("../", "/").Replace("./", "/");
        }
    }
}