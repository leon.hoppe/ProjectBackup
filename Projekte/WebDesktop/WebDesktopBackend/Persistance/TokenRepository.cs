﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Entitys.Permissions;
using WebDesktopBackend.Entitys.Tokens;
using WebDesktopBackend.Security.Authentication;

namespace WebDesktopBackend.Persistance {
    public class TokenRepository : ITokenRepository {
        private readonly JwtTokenAuthenticationOptions _options;
        private readonly DatabaseContext _context;

        public TokenRepository(IOptions<JwtTokenAuthenticationOptions> options, DatabaseContext context) {
            _options = options.Value;
            _context = context;
        }
        
        public RefreshToken GetRefreshToken(string id) {
            if (string.IsNullOrEmpty(id)) return null;
            return _context.RefreshTokens.Where(token => token.Id == id).SingleOrDefault();
        }

        public AccessToken GetAccessToken(string id) {
            if (string.IsNullOrEmpty(id)) return null;
            return _context.AccessTokens.Where(token => token.Id == id).SingleOrDefault();
        }

        public bool ValidateAccessToken(string id) {
            AccessToken token = GetAccessToken(id);
            if (token == null) return false;
            TimeSpan span = token.ExpirationDate - DateTime.Now;
            return span.TotalMilliseconds > 0;
        }

        public bool ValidateRefreshToken(string id) {
            RefreshToken token = GetRefreshToken(id);
            if (token == null) return false;
            TimeSpan span = token.ExpirationDate - DateTime.Now;
            return span.TotalMilliseconds > 0;
        }

        public RefreshToken CreateRefreshToken(string userId) {
            RefreshToken token = new RefreshToken { UserId = userId, Id = Guid.NewGuid().ToString(), ExpirationDate = DateTime.Now.Add(new TimeSpan(int.Parse(_options.RefreshTokenExpirationTimeInHours), 0, 0)) };
            _context.RefreshTokens.Add(token);
            _context.SaveChanges();
            return token;
        }

        public AccessToken CreateAccessToken(string refreshTokenId) {
            AccessToken token = new AccessToken { RefreshTokenId = refreshTokenId, Id = Guid.NewGuid().ToString(), ExpirationDate = DateTime.Now.Add(new TimeSpan(0, int.Parse(_options.AccessTokenExpirationTimeInMinutes), 0)) };
            _context.AccessTokens.Add(token);
            _context.SaveChanges();
            return token;
        }

        public void DeleteUserTokens(string id) {
            List<RefreshToken> refreshTokens = _context.RefreshTokens.Where(token => token.UserId == id).ToList();
            refreshTokens.ForEach(token => DeleteRefreshToken(token.Id));
        }

        public void DeleteRefreshToken(string id) {
            _context.RefreshTokens.RemoveRange(_context.RefreshTokens.Where(token => token.Id == id));
            _context.AccessTokens.RemoveRange(_context.AccessTokens.Where(token => token.RefreshTokenId == id));
        }

        public Permission[] GetUserPermissions(string id) {
            return _context.Permissions.Where(permission => permission.UserId == id).ToArray();
        }

        public void AddPermission(string id, string permission) {
            _context.Permissions.Add(new Permission
                { PermissionName = permission, UserId = id, Type = Permission.Allow });
            _context.SaveChanges();
        }

        public void DeletePermission(string id, string permission) {
            _context.Permissions.Remove(_context.Permissions.Single(perm =>
                perm.UserId == id && perm.PermissionName == permission));
            _context.SaveChanges();
        }
    }
}