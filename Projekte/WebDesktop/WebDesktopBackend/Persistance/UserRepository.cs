﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Entitys.Permissions;
using WebDesktopBackend.Entitys.User;

namespace WebDesktopBackend.Persistance {
    public class UserRepository : IUserRepository {
        private readonly DatabaseContext _context;
        private readonly ITokenRepository _tokens;
        private readonly IConfiguration _configuration;

        public UserRepository(DatabaseContext context, ITokenRepository tokens, IConfiguration configuration) {
            _context = context;
            _tokens = tokens;
            _configuration = configuration;
        }
        
        public User AddUser(UserEditor editor) {
            User user = new User { Id = Guid.NewGuid().ToString(), Created = DateTime.Now };
            editor.EditUser(user);
            user.Password = Hash128(user.Password);
            _context.Users.Add(user);
            _context.Permissions.Add(new Permission()
                { PermissionName = "group.user", UserId = user.Id, Type = Permission.Allow });
            _context.SaveChanges();
            return user;
        }

        public void EditUser(string id, UserEditor editor) {
            User user = GetUser(id);
            if (!string.IsNullOrEmpty(editor.Password))
                editor.Password = Hash128(editor.Password);
            editor.EditUser(user);
            _context.SaveChanges();
        }

        public void DeleteUser(string id) {
            _context.Users.RemoveRange(_context.Users.Where(user => user.Id == id));
            _context.Permissions.RemoveRange(_context.Permissions.Where(permission => permission.UserId == id));
            _tokens.DeleteUserTokens(id);
            _context.SaveChanges();
        }

        public User GetUser(string id) {
            return _context.Users.SingleOrDefault(user => user.Id == id);
        }

        public User GetUserByUsername(string username) {
            return _context.Users.SingleOrDefault(user => user.Username == username);
        }

        public User GetUserByEmail(string email) {
            return _context.Users.SingleOrDefault(user => user.Email == email);
        }

        public User GetUserFromLogin(UserLogin login) {
            if (!string.IsNullOrEmpty(login.Username)) return GetUserByUsername(login.Username);
            if (!string.IsNullOrEmpty(login.Email)) return GetUserByEmail(login.Email);
            return null;
        }

        public User[] GetUsers() {
            return _context.Users.OrderBy(user => user.Created).ToArray();
        }

        public bool Login(UserLogin login) {
            User user = GetUserFromLogin(login);
            if (user == null || string.IsNullOrEmpty(user.Password)) return false;
            return user.Password.Equals(Hash128(login.Password));
        }
        
        private string Hash128(string plainText) {
            try {
                byte[] salt = _configuration.GetSection("PasswordSalt").Get<byte[]>();

                string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: plainText,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8
                ));

                return hashed;
            } catch (Exception) { return ""; }
        }
    }
}