﻿using System.Collections.Generic;
using System.Linq;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Entitys.Permissions;

namespace WebDesktopBackend.Persistance {
    public class GroupRepository : IGroupRepository {
        private readonly ITokenRepository _tokens;
        private readonly PermissionGroup[] _groups;

        public GroupRepository(ITokenRepository tokens) {
            _tokens = tokens;
            _groups = Program.Groups;
        }
        
        public PermissionGroup GetPermissionGroup(string name) {
            return _groups.SingleOrDefault(group => group.Permission.Equals(name));
        }

        public PermissionGroup[] GetGroupsFromUser(string userId) {
            Permission[] permissions = _tokens.GetUserPermissions(userId);
            return ExtractGroups(permissions);
        }

        public PermissionGroup[] ExtractGroups(Permission[] permissions) {
            List<PermissionGroup> permissionGroups = new List<PermissionGroup>();
            foreach (var permission in permissions) {
                if (permission.PermissionName.StartsWith("group.")) {
                    foreach (var permissionGroup in _groups) {
                        if (permission.PermissionName.Equals(permissionGroup.Permission)) {
                            permissionGroups.Add(permissionGroup);
                            
                            if (permissionGroup.Inherits is not null) {
                                foreach (var inherit in permissionGroup.Inherits) {
                                    permissionGroups.Add(GetPermissionGroup(inherit));
                                }
                            }
                        }
                    }
                }
            }
            return permissionGroups.ToArray();
        }

        public Permission[] GetUserPermissions(string id) {
            List<Permission> permissions = _tokens.GetUserPermissions(id)
                .Where(perm => perm.Type == Permission.Allow).ToList();

            PermissionGroup[] groups = ExtractGroups(permissions.ToArray());
            foreach (var group in groups) {
                if (group.Permissions is null) continue;
                permissions.AddRange(group.Permissions
                    .Select(perm => new Permission {Id = -1, UserId = id, Type = Permission.Allow, PermissionName = perm}));
            }

            return permissions.ToArray();
        }
    }
}