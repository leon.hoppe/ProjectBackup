﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using WebDesktopBackend.Contract.Persistance;
using WebDesktopBackend.Entitys.Files;
using WebDesktopBackend.Extentions;
using WebDesktopBackend.Options;
using FileShare = WebDesktopBackend.Entitys.Files.FileShare;

namespace WebDesktopBackend.Persistance {
    public class FileRepository : IFileRepository {
        private readonly FileSystemOptions _options;
        private readonly DatabaseContext _context;

        public FileRepository(IOptions<FileSystemOptions> options, DatabaseContext context) {
            _options = options.Value;
            _context = context;
        }
        
        public void InitUser(string userId) {
            CreateDirectory(_options.RootDirectory, userId);
        }

        public void DeleteUserFolder(string userId) {
            Delete(_options.RootDirectory + userId);
        }

        public bool CreateDirectory(string directory, string name) {
            DirectoryInfo info = new DirectoryInfo(directory + "/" + name);
            if (info.Exists) return false;
            info.Create();
            return true;
        }

        public async Task UploadFile(IFormFile file, string directory) {
            DirectoryInfo dir = new DirectoryInfo(directory);
            if (!dir.Exists)
                dir.Create();

            FileInfo fileInfo = new FileInfo(dir + "/" + file.FileName);
            if (fileInfo.Exists)
                fileInfo.Delete();

            FileStream stream = fileInfo.OpenWrite();
            await file.CopyToAsync(stream);
            stream.Close();
        }

        public async Task UploadJson(string directory, string name, string data) {
            DirectoryInfo dir = new DirectoryInfo(directory);
            if (!dir.Exists)
                dir.Create();
            
            FileInfo file = new FileInfo(directory + "/" + name);
            if (file.Exists)
                file.Delete();

            byte[] bytes = Encoding.UTF8.GetBytes(data);
            FileStream stream = file.Create();
            await stream.WriteAsync(bytes, 0, bytes.Length);
            stream.Close();
        }

        public FileStream DownloadFile(string path) {
            FileInfo file = new FileInfo(path);
            return file.OpenRead();
        }

        public Task<string> DownloadJson(string file) {
            return File.ReadAllTextAsync(file);
        }

        public DirectoryContent GetDirectory(string directory) {
            DirectoryInfo dir = new DirectoryInfo(directory);

            return new DirectoryContent() {
                Files = dir.GetFiles().Select(file => file.Name).ToArray(),
                Directories = dir.GetDirectories().Select(info => info.Name).ToArray()
            };
        }

        public DirectoryInformation GetDirectoryInformation(string directory) {
            DirectoryInfo info = new DirectoryInfo(directory);
            return new DirectoryInformation {
                Name = info.Name,
                Created = Directory.GetCreationTime(directory),
                Size = info.GetDirectorySize()
            };
        }

        public FileInformation GetFileInformation(string file) {
            FileInfo info = new FileInfo(file);
            return new FileInformation() {
                Name = info.Name,
                Created = File.GetCreationTime(file),
                Size = info.Length
            };
        }

        public void MoveDirectory(string directory, string to) {
            DirectoryInfo info = new DirectoryInfo(directory);
            info.MoveTo(to);
        }

        public void MoveFile(string file, string to) {
            FileInfo info = new FileInfo(file);
            info.MoveTo(to);
        }

        public void Delete(string url) {
            if (File.Exists(url))
                File.Delete(url);
            
            if (Directory.Exists(url))
                Directory.Delete(url, true);
        }

        public string GenerateShareId(string url, string owner) {
            FileShare share = new FileShare();
            share.File = url;
            share.Owner = owner;
            share.Id = Guid.NewGuid().ToString();

            _context.FileShares.Add(share);
            _context.SaveChanges();

            return share.Id;
        }
    }
}