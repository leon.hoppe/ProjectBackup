﻿using Microsoft.EntityFrameworkCore;
using WebDesktopBackend.Entitys.Files;
using WebDesktopBackend.Entitys.Permissions;
using WebDesktopBackend.Entitys.Tokens;
using WebDesktopBackend.Entitys.User;

namespace WebDesktopBackend {
    public class DatabaseContext : DbContext {
        
        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<AccessToken> AccessTokens { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        
        public DbSet<FileShare> FileShares { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseMySQL(MySql.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(entry => {
                entry.HasKey(e => e.Id);
                entry.Property(e => e.FirstName);
                entry.Property(e => e.LastName);
                entry.Property(e => e.Email);
                entry.Property(e => e.Username);
                entry.Property(e => e.Password);
                entry.Property(e => e.Created);
            });

            modelBuilder.Entity<RefreshToken>(entry => {
                entry.HasKey(e => e.Id);
                entry.Property(e => e.UserId);
                entry.Property(e => e.ExpirationDate);
            });

            modelBuilder.Entity<AccessToken>(entry => {
                entry.HasKey(e => e.Id);
                entry.Property(e => e.RefreshTokenId);
                entry.Property(e => e.ExpirationDate);
            });

            modelBuilder.Entity<Permission>(entry => {
                entry.HasKey(e => e.Id);
                entry.Property(e => e.Id).ValueGeneratedOnAdd();
                
                entry.Property(e => e.UserId);
                entry.Property(e => e.PermissionName);
                entry.Property(e => e.Type);
            });

            modelBuilder.Entity<FileShare>(entry => {
                entry.HasKey(e => e.Id);
                entry.Property(e => e.Owner);
                entry.Property(e => e.File);
            });
        }
    }
}