using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using WebDesktopBackend.Entitys.Permissions;

namespace WebDesktopBackend {
    public class Program {
        public static PermissionGroup[] Groups;
        
        public static void Main(string[] args) {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .Build();
            
            //COMPILE GROUPS
            var groupsSections = configuration.GetSection("Groups").GetChildren();
            List<PermissionGroup> groups = new List<PermissionGroup>();
            foreach (var section in groupsSections) {
                PermissionGroup group = new PermissionGroup();
                group.Name = section.GetValue<string>("Name");
                group.Permission = section.GetValue<string>("Permission");
                group.Permissions = section.GetSection("Permissions").Get<string[]>();
                group.Inherits = section.GetSection("Inherits").Get<string[]>();
                groups.Add(group);
            }
            Groups = groups.ToArray();
            
            CreateHostBuilder(args, configuration)
                .Build()
                .Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args, IConfiguration configuration) {
            var config = configuration.GetSection("WebServer");
            
            return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => {
                webBuilder.UseStartup<Startup>();
                webBuilder.UseUrls("http://0.0.0.0:" + config.GetValue<int>("Port"));
            });
        }
    }
}