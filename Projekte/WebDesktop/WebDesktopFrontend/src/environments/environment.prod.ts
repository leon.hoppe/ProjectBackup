export const environment = {
  production: true,
  backendHost: "https://api.leon-hoppe.de/",
  updateUrl: "wss://api.leon-hoppe.de/update"
};
