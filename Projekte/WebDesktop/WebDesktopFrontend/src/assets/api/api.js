var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export class WindowAPI {
    constructor() {
        this.handlers = [];
        this.properties = new WindowProperties();
        window.addEventListener("message", (event) => {
            const data = event.data;
            const removeHandler = [];
            for (let i = 0; i < this.handlers.length; i++) {
                const handler = this.handlers[i];
                if (handler.type !== data.type)
                    continue;
                handler.handler.call(this, data);
                if (handler.oneTimeCall)
                    removeHandler.push(i);
            }
            removeHandler.forEach(index => this.handlers.splice(index, 1));
            if (data.type === "triggerListener") {
                this.properties.triggerEventListeners(data.data.type, data.data.event);
            }
        });
    }
    postMessage(packet) {
        parent.postMessage(packet, '*');
    }
    getWindowTypes() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve => {
                this.addMessageHandler((data) => {
                    resolve(data.data);
                }, "windowTypes", true);
                this.postMessage({ type: "windowTypes" });
            }));
        });
    }
    openWindow(id, args) {
        windowApi.postMessage({ type: "openWindow", data: { id, args } });
    }
    closeWindow(windowId) {
        windowApi.postMessage({ type: "closeWindow", data: windowId });
    }
    requestFocus() {
        windowApi.postMessage({ type: "requestFocus" });
    }
    toggleMaximize() {
        windowApi.postMessage({ type: "toggleMaximize" });
    }
    minimize() {
        windowApi.postMessage({ type: "minimize" });
    }
    close() {
        windowApi.postMessage({ type: "close" });
    }
    addMessageHandler(handler, type, oneTimeCall = false) {
        this.handlers.push({ handler, type, oneTimeCall });
    }
    removeMessageHandler(handler) {
        for (let i = 0; i < this.handlers.length; i++) {
            const h = this.handlers[i];
            if (h.handler == handler) {
                this.handlers.splice(i, 1);
                return;
            }
        }
    }
}
export class WindowProperties {
    constructor() {
        this.eventListeners = [];
    }
    setTitle(value) {
        windowApi.postMessage({ type: "properties", data: { name: "title", value } });
    }
    setHeaderMessage(value) {
        windowApi.postMessage({ type: "properties", data: { name: "headerMessage", value } });
    }
    setIcon(value) {
        windowApi.postMessage({ type: "properties", data: { name: "icon", value } });
    }
    setResizable(value) {
        windowApi.postMessage({ type: "properties", data: { name: "resizable", value } });
    }
    setMinSize(width, height) {
        windowApi.postMessage({ type: "properties", data: { name: "minSize", value: { width, height } } });
    }
    setMaximized(value) {
        windowApi.postMessage({ type: "properties", data: { name: "maximized", value } });
    }
    setSize(width, height) {
        windowApi.postMessage({ type: "properties", data: { name: "size", value: { width, height } } });
    }
    setPosition(x, y) {
        windowApi.postMessage({ type: "properties", data: { name: "position", value: { x, y } } });
    }
    getTitle() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "title", true);
            windowApi.postMessage({ type: "title" });
        }));
    }
    getHeaderMessage() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "headerMessage", true);
            windowApi.postMessage({ type: "headerMessage" });
        }));
    }
    getIcon() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "icon", true);
            windowApi.postMessage({ type: "icon" });
        }));
    }
    isResizable() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "resizable", true);
            windowApi.postMessage({ type: "resizable" });
        }));
    }
    getSize() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "size", true);
            windowApi.postMessage({ type: "size" });
        }));
    }
    getMinSize() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "minSize", true);
            windowApi.postMessage({ type: "minSize" });
        }));
    }
    getPosition() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "position", true);
            windowApi.postMessage({ type: "position" });
        }));
    }
    getWindowType() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "windowType", true);
            windowApi.postMessage({ type: "windowType" });
        }));
    }
    getAdditionalArgs() {
        return new Promise((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "additionalArgs", true);
            windowApi.postMessage({ type: "additionalArgs" });
        }));
    }
    updateWindow() {
        windowApi.postMessage({ type: "update" });
    }
    addEventListener(listener) {
        this.eventListeners.push(listener);
    }
    triggerEventListeners(type, event) {
        this.eventListeners.forEach(listener => {
            switch (type) {
                case "click":
                    listener.onClick(event);
                    return;
                case "down":
                    listener.onMouseDown(event);
                    return;
                case "up":
                    listener.onMouseUp(event);
                    return;
                case "move":
                    listener.onMouseMove(event);
                    return;
            }
        });
    }
}
export const windowApi = new WindowAPI();
//# sourceMappingURL=api.js.map