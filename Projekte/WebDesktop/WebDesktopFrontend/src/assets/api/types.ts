﻿
export interface WindowType {
    type: string;
    id: string,
    name: string,
    icon: string,
    permission?: string;
    args?: string[];
    canOpen?: (number | string)[];
    url?: string;
}

export interface WindowEvents {
    onClick(event: MouseEvent): void;
    onMouseDown(event: MouseEvent): void;
    onMouseUp(event: MouseEvent): void;
    onMouseMove(event: MouseEvent): void;
}