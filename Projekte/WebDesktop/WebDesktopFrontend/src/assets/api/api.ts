﻿import {WindowEvents, WindowType} from "./types";

export type MessageEventHandler = (packet: Packet) => void;
export type PromiseType = "toggleMaximize" | "minimize" | "close" | "windowTypes" | "openWindow" | "closeWindow" | "requestFocus" | "properties" | "update" | "triggerListener" |
                          "title" | "headerMessage" | "icon" | "resizable" | "size" | "minSize" | "position" | "windowType" | "additionalArgs";

export class WindowAPI {
    private readonly handlers: {
        handler: MessageEventHandler,
        type: PromiseType,
        oneTimeCall: boolean
    }[];
    public properties: WindowProperties;
    
    public constructor() {
        this.handlers = [];
        this.properties = new WindowProperties();
        window.addEventListener("message", (event: MessageEvent) => {
            const data: Packet = event.data;
            const removeHandler: number[] = [];
            for (let i = 0; i < this.handlers.length; i++) {
                const handler = this.handlers[i];
                if (handler.type !== data.type) continue;
                handler.handler.call(this, data);
                if (handler.oneTimeCall)
                    removeHandler.push(i);
            }
            removeHandler.forEach(index => this.handlers.splice(index, 1));
            
            if (data.type === "triggerListener") {
                this.properties.triggerEventListeners(data.data.type, data.data.event);
            }
        });
    }
    
    public postMessage(packet: Packet) {
        parent.postMessage(packet, '*');
    }

    public async getWindowTypes(): Promise<WindowType[]> {
        return new Promise<WindowType[]>((resolve => {
            this.addMessageHandler((data) => {
                resolve(data.data);
            }, "windowTypes", true);
            
            this.postMessage({type: "windowTypes"});
        }));
    }

    public openWindow(id: string, args?: string[]) {
        windowApi.postMessage({type: "openWindow", data: {id, args}});
    }
    public closeWindow(windowId: number) {
        windowApi.postMessage({type: "closeWindow", data: windowId});
    }
    public requestFocus() {
        windowApi.postMessage({type: "requestFocus"});
    }
    public toggleMaximize() {
        windowApi.postMessage({type: "toggleMaximize"});
    }
    public minimize() {
        windowApi.postMessage({type: "minimize"});
    }
    public close() {
        windowApi.postMessage({type: "close"});
    }
    
    public addMessageHandler(handler: MessageEventHandler, type: PromiseType, oneTimeCall: boolean = false) {
        this.handlers.push({handler, type, oneTimeCall});
    }
    public removeMessageHandler(handler: MessageEventHandler) {
        for (let i = 0; i < this.handlers.length; i++) {
            const h = this.handlers[i];
            if (h.handler == handler) {
                this.handlers.splice(i, 1);
                return;
            }
        }
    }
    
}

export class WindowProperties {
    private eventListeners: WindowEvents[] = [];
    
    public setTitle(value: string) {
        windowApi.postMessage({type: "properties", data: {name: "title", value}});
    }
    public setHeaderMessage(value: string) {
        windowApi.postMessage({type: "properties", data: {name: "headerMessage", value}});
    }
    public setIcon(value: string) {
        windowApi.postMessage({type: "properties", data: {name: "icon", value}});
    }
    public setResizable(value: boolean) {
        windowApi.postMessage({type: "properties", data: {name: "resizable", value}});
    }
    public setMinSize(width: number, height: number) {
        windowApi.postMessage({type: "properties", data: {name: "minSize", value: {width, height}}});
    }
    public setMaximized(value: boolean): void {
        windowApi.postMessage({type: "properties", data: {name: "maximized", value}});
    }
    public setSize(width: number, height: number): void {
        windowApi.postMessage({type: "properties", data: {name: "size", value: {width, height}}});
    }
    public setPosition(x: number, y: number): void {
        windowApi.postMessage({type: "properties", data: {name: "position", value: {x, y}}});
    }
    
    public getTitle(): Promise<string> {
        return new Promise<string>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "title", true);

            windowApi.postMessage({type: "title"});
        }));
    }
    public getHeaderMessage(): Promise<string> {
        return new Promise<string>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "headerMessage", true);

            windowApi.postMessage({type: "headerMessage"});
        }));
    }
    public getIcon(): Promise<string> {
        return new Promise<string>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "icon", true);

            windowApi.postMessage({type: "icon"});
        }));
    }
    public isResizable(): Promise<boolean> {
        return new Promise<boolean>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "resizable", true);

            windowApi.postMessage({type: "resizable"});
        }));
    }
    public getSize(): Promise<{width: number, height: number}> {
        return new Promise<{width: number, height: number}>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "size", true);

            windowApi.postMessage({type: "size"});
        }));
    }
    public getMinSize(): Promise<{width: number, height: number}> {
        return new Promise<{width: number, height: number}>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "minSize", true);

            windowApi.postMessage({type: "minSize"});
        }));
    }
    public getPosition(): Promise<{x: number, y: number}> {
        return new Promise<{x: number, y: number}>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "position", true);

            windowApi.postMessage({type: "position"});
        }));
    }
    public getWindowType(): Promise<WindowType> {
        return new Promise<WindowType>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "windowType", true);

            windowApi.postMessage({type: "windowType"});
        }));
    }
    public getAdditionalArgs(): Promise<any[]> {
        return new Promise<any[]>((resolve => {
            windowApi.addMessageHandler((data) => {
                resolve(data.data);
            }, "additionalArgs", true);

            windowApi.postMessage({type: "additionalArgs"});
        }));
    }

    public updateWindow(): void {
        windowApi.postMessage({type: "update"});
    }

    public addEventListener(listener: WindowEvents): void {
        this.eventListeners.push(listener);
    }
    public triggerEventListeners(type: "click" | "down" | "up" | "move", event: MouseEvent) {
        this.eventListeners.forEach(listener => {
            switch (type) {
                case "click":
                    listener.onClick(event); return;
                case "down":
                    listener.onMouseDown(event); return;
                case "up":
                    listener.onMouseUp(event); return;
                case "move":
                    listener.onMouseMove(event); return;
            }
        })
    }
}

export interface Packet {
    type: PromiseType;
    data?: any;
}

export const windowApi = new WindowAPI();
