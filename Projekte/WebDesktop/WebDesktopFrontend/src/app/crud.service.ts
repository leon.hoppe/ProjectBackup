import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {firstValueFrom} from 'rxjs';
import { Router } from '@angular/router';
import { Token } from './entitys/token';
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  public endpoint: string;
  public authKey: string;

  constructor(private httpClient: HttpClient) {
    this.endpoint = environment.backendHost;
  }

  public httpHeader = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  setAccessToken(key?: string, contentType?: string, additionalHeaders?: [{name: string, value: string}]) {
    if (key === undefined) return;

    this.httpHeader = new HttpHeaders({
      'Accept': '*/*',
      'Authorization': key
    });

    if (contentType !== null) {
      if (contentType === undefined) contentType = 'application/json';
      this.httpHeader.append("Content-Type", contentType);
    }

    if (additionalHeaders !== undefined) {
      for (let i = 0; i < additionalHeaders.length; i++) {
        const header = additionalHeaders[i];
        this.httpHeader = this.httpHeader.append(header.name, header.value);
      }
    }

    this.authKey = key;
  }

  async sendGetRequest<T>(url: string, options?: Options): Promise<T> {
    try {
      this.setAccessToken(this.authKey, options?.contentType, options?.additionalHeaders);
      return await firstValueFrom(this.httpClient.get<T>(this.endpoint + url, {headers: this.httpHeader, withCredentials: options?.withCredentials}));
    }catch(e) {
      const error = e as HttpErrorResponse;
      if (options?.authorized && error.status == 401) {
        const tokenResponse = await this.getNewToken();
        if (!options.dontResendOnExpiration)
          return await firstValueFrom(this.httpClient.get<T>(this.endpoint + url, {headers: this.httpHeader, withCredentials: options?.withCredentials}));

        if (options?.returnNewTokenResponse)
          return tokenResponse as unknown as T;
        else throw e;
      }else {
        throw e;
      }
    }
  }

  async sendPostRequest<T>(url: string, body?: any, options?: Options): Promise<T> {
    try {
      this.setAccessToken(this.authKey, options?.contentType, options?.additionalHeaders);
      return  await firstValueFrom(this.httpClient.post<T>(this.endpoint + url, body, {headers: this.httpHeader, withCredentials: options?.withCredentials}));
    }catch(e) {
      const error = e as HttpErrorResponse;
      if (options?.authorized && error.status == 401) {
        await this.getNewToken();
        if (!options.dontResendOnExpiration)
          return await firstValueFrom(this.httpClient.post<T>(this.endpoint + url, body, {headers: this.httpHeader, withCredentials: options?.withCredentials}));
        else throw e;
      }else {
        throw e;
      }
    }
  }

  async sendPutRequest<T>(url: string, body?: any, options?: Options): Promise<T> {
    try {
      this.setAccessToken(this.authKey, options?.contentType, options?.additionalHeaders);
      return await firstValueFrom(this.httpClient.put<T>(this.endpoint + url, body, {headers: this.httpHeader, withCredentials: true}));
    }catch(e) {
      const error = e as HttpErrorResponse;
      if (options?.authorized && error.status == 401) {
        await this.getNewToken();
        if (!options.dontResendOnExpiration)
          return await firstValueFrom(this.httpClient.put<T>(this.endpoint + url, body, {headers: this.httpHeader, withCredentials: options?.withCredentials}));
        else throw e;
      }else {
        throw e;
      }
    }
  }

  async sendDeleteRequest<T>(url: string, options?: Options): Promise<T> {
    try {
      this.setAccessToken(this.authKey, options?.contentType, options?.additionalHeaders);
      return await firstValueFrom(this.httpClient.delete<T>(this.endpoint + url, {headers: this.httpHeader, withCredentials: options?.withCredentials}));
    }catch(e) {
      const error = e as HttpErrorResponse;
      if (options?.authorized && error.status == 401) {
        await this.getNewToken();
        if (!options.dontResendOnExpiration)
          return await firstValueFrom(this.httpClient.delete<T>(this.endpoint + url, {headers: this.httpHeader, withCredentials: options?.withCredentials}));
        else throw e;
      }else {
        throw e;
      }
    }
  }

  async createWebSocketConnection(url: string): Promise<WebSocket> {
    await this.getNewToken();
    return new WebSocket(url + "?token=" + this.authKey);
  }

  async getNewToken(): Promise<boolean> {
    try {
      const token: Token = await firstValueFrom(this.httpClient.get<Token>(this.endpoint + "users/token", {headers: this.httpHeader, withCredentials: true}));
      this.setAccessToken(token.id);
      return true;
    }catch {
      return false;
    }
  }

  public get HttpClient(): HttpClient { return this.httpClient; }
}

export interface Options {
  authorized?: boolean;
  withCredentials?: boolean;
  dontResendOnExpiration?: boolean;
  contentType?: string;
  additionalHeaders?: [{name: string, value: string}];
  returnNewTokenResponse?: boolean;
}
