import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UserAPI } from '../api/userapi.service';
import { UserLogin } from '../entitys/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('identifier') identifier: ElementRef;
  @ViewChild('password') password: ElementRef;
  @ViewChild('snackbar') snackbar: ElementRef;

  private failedAttempts: number = 0;
  private isResetting: boolean = false;
  private readonly resetTimeout: number = 20000;

  constructor(private users: UserAPI, private router: Router) { }

  ngOnInit(): void {
  }

  async login(event: MouseEvent) {
    event.preventDefault();

    if (!this.identifier.nativeElement.reportValidity() ||
        !this.password.nativeElement.reportValidity())
      return;

    if (this.failedAttempts >= 3 || this.handleResetWithLocalStorage()) {
      await Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Bitte warte einen moment, bevor du dich einloggst!',
      });
      if (!this.isResetting) {
        this.isResetting = true;
        localStorage.setItem("login_timeout", String(new Date().getTime()));
        setTimeout(() => {
          this.isResetting = false;
          this.failedAttempts = 0;
          localStorage.removeItem("\"login_timeout\"");
        }, this.resetTimeout);
      }
      return;
    }

    const identifier = this.identifier.nativeElement as HTMLInputElement;
    const password = this.password.nativeElement as HTMLInputElement;
    if (identifier.value == "" || password.value == "") this.showSnackbar();
    else {
      const login: UserLogin = {password: password.value};

      if (identifier.value.includes('@')) {
        login.email = identifier.value;
      }else {
        login.username = identifier.value;
      }

      const success = await this.users.login(login);
      if (success) {
        await Swal.fire({
          icon: 'success',
          title: 'Erfolgreich eingeloggt',
          text: 'Du wist gleich weitergeleitet',
          timer: 2000,
          showConfirmButton: false
        });
        await this.users.getCurrentUser(true);
        await this.router.navigate([""]);
      }
      else this.showSnackbar();
    }
  }

  async register() {
    await this.router.navigate(["/register"]);
  }

  showSnackbar() {
    this.failedAttempts++;
    this.snackbar.nativeElement.classList.add('show');
    setTimeout(() => {this.snackbar.nativeElement.classList.remove('show');}, 4000);
  }

  handleResetWithLocalStorage(): boolean {
    const lsEntry = localStorage.getItem("login_timeout");
    if (lsEntry === null) return false;
    const time = Number(lsEntry);
    const now = new Date().getTime();
    if (now - time >= this.resetTimeout) {
      localStorage.removeItem("login_timeout");
      return false;
    }
    return true;
  }

}
