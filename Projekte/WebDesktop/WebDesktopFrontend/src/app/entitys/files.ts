export interface DirectoryContent {
  files: string[];
  directories: string[];
}

export interface DirectoryInformation {
  name: string;
  created: Date;
  size: number;
}

export interface FileInformation {
  name: string;
  created: Date;
  size: number;
}

export enum FileType {
  FILE,
  IMAGE,
  VIDEO,
  AUDIO,
  COMPRESSED,
  DISK,
  DATABASE,
  DATA,
  EMAIL,
  EXECUTABLE,
  FONT,
  INTERNET,
  PRESENTATION,
  SPREADSHEET,
  SYSTEM,
  WORD
}
