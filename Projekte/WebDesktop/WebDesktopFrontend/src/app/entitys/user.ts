export interface User extends UserEditor {
    id: string;
    created: Date;
}

export interface UserEditor {
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    password: string;
}

export interface UserLogin {
    email?: string;
    username?: string;
    password: string;
}
