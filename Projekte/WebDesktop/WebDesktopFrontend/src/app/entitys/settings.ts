export interface Personalization {
  theme?: string;
  taskbar?: string;
  taskbar_icons?: string;
  background?: string;
}

export const defaultPersonalization: Personalization = {
  theme: "theme-dark",
  taskbar: "small",
  taskbar_icons: "left",
  background: "assets/images/moon.jpg"
};
