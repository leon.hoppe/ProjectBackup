import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { Desktop } from './desktop/desktop.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { WindowRoot } from './desktop/windows/window.component';
import { SettingsWindow } from './desktop/windows/settings/settings.component';

import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { DesktopIconComponent } from './desktop/components/desktop-icon/desktop-icon.component';
import { TaskbarIconComponent } from './desktop/components/taskbar-icon/taskbar-icon.component';
import { ExplorerWindow } from './desktop/windows/explorer/explorer.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AdminWindow } from './desktop/windows/admin/admin.component';
import { MatTabsModule } from "@angular/material/tabs";
import { CodeWindow } from './desktop/windows/code/code.component';
import { MatSelectModule } from "@angular/material/select";
import {MatTableModule} from "@angular/material/table";
import { SearchComponent } from './desktop/windows/search/search.component';
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { EditorWindow } from './desktop/windows/editor/editor.component';
import {CodemirrorModule} from "@ctrl/ngx-codemirror";
import { Notifications } from './desktop/components/notifications/notifications.component';
import { NotificationWrapper } from './desktop/components/notifications/notification/notification.component';
import { ApiWindow } from './desktop/windows/api-window/api-window.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Desktop,
    RegisterComponent,
    WindowRoot,
    DesktopIconComponent,
    TaskbarIconComponent,
    SettingsWindow,
    ExplorerWindow,
    AdminWindow,
    CodeWindow,
    SearchComponent,
    EditorWindow,
    Notifications,
    NotificationWrapper,
    ApiWindow
  ],
  entryComponents: [
    SettingsWindow
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        MatButtonModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTabsModule,
        MatDividerModule,
        MatSelectModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            registrationStrategy: 'registerWhenStable:30000'
        }),
        MatTableModule,
        MatTooltipModule,
        MatProgressBarModule,
        CodemirrorModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
