import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserAPI } from './api/userapi.service';
import {CrudService} from "./crud.service";
import {defaultPersonalization, Personalization} from "./entitys/settings";
import {SettingsService} from "./api/settings.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'WebDesktop';
  ready = false;

  constructor(private users: UserAPI, private router: Router, private service: CrudService, private settings: SettingsService) {}

  async ngOnInit() {
    const personalization = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);

    const theme = personalization.theme;
    document.body.classList.add(theme);

    const background = personalization.background;
    document.body.style.setProperty("--background", "url(" + background + ")");

    if (this.router.url == "/register" || location.href.endsWith("/register")) this.ready = true;
    else if (this.router.url == "/login" || location.href.endsWith("/login")) this.ready = true;
    else {
      this.ready = await this.users.loadUser();
      if (!this.ready) {
        await this.router.navigate(["/login"]);
        this.ready = true;
      }
    }
  }
}
