import {AfterViewInit, ChangeDetectorRef, Component, Type, ViewChild, ViewContainerRef} from '@angular/core';
import {SettingsWindow} from './windows/settings/settings.component';
import {Window, WindowEvents, WindowRoot} from './windows/window.component';
import {DesktopIconComponent} from "./components/desktop-icon/desktop-icon.component";
import {TaskbarIconComponent} from "./components/taskbar-icon/taskbar-icon.component";
import {ExplorerWindow} from "./windows/explorer/explorer.component";
import {UserAPI} from "../api/userapi.service";
import {AdminWindow} from "./windows/admin/admin.component";
import {CodeWindow} from "./windows/code/code.component";
import {SearchComponent} from "./windows/search/search.component";
import {FileType} from "../entitys/files";
import {EditorWindow} from "./windows/editor/editor.component";
import {SettingsService} from "../api/settings.service";
import {defaultPersonalization, Personalization} from "../entitys/settings";
import {ApiWindow} from "./windows/api-window/api-window.component";

export interface WindowType {
  type: Type<Window>;
  id: string,
  name: string,
  icon: string,
  permission?: string;
  args?: string[];
  canOpen?: (FileType | string)[];
  url?: string;
}

export interface DesktopIcon {
  type: WindowType;
  desktopRef: Desktop;
  focus: boolean;
  instance: HTMLElement;

  onFocus(): void;
  onLostFocus(): void;
  onOpen(): void;
  onClose(): void;
}

export const windowTypes: WindowType[] = [
  {type: SettingsWindow, id: "settings", name: "Einstellungen", icon: "/assets/icons/settings.png"},
  {type: undefined, id: "browser", name: "Web Browser", icon: "/assets/icons/browser.png"},
  {type: EditorWindow, id: "editor", name: "Editor", icon: "/assets/icons/editor.png", canOpen: [FileType.DATA, FileType.INTERNET]},
  {type: ExplorerWindow, id: "explorer", name: "Explorer", icon: "/assets/icons/explorer.png"},
  {type: undefined, id: "messenger", name: "Messenger", icon: "/assets/icons/messenger.png"},
  {type: AdminWindow, id: "admin", name: "Admin Panel", icon: "/assets/icons/admin.png", permission: "app.admin.use"},
  {type: CodeWindow, id: "code", name: "Code Editor", icon: "/assets/icons/code.png", permission: "app.code.use"},
  {type: ApiWindow, id: "cloud", name: "WindowAPI Test", icon: "/assets/images/logo.png", url: "/assets/apiTest/iframe.html"}
];

@Component({
  selector: 'app-desktop',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desktop.component.scss']
})
export class Desktop implements AfterViewInit, WindowEvents {
  private static _instance: Desktop;

  public static get instance(): Desktop { return this._instance; }
  public static get pinnedIcons(): DesktopIcon[] { return this._instance.pinnedIcons; }

  @ViewChild('windows', {read: ViewContainerRef}) windowsContainerRef: ViewContainerRef;
  @ViewChild('desktopIcons', {read: ViewContainerRef}) desktopIconsRef: ViewContainerRef;
  @ViewChild('taskbarIcons', {read: ViewContainerRef}) taskbarIconsRef: ViewContainerRef;
  @ViewChild('notifications', {read: ViewContainerRef}) notificationsRef: ViewContainerRef;
  @ViewChild('search') search: SearchComponent;

  private nextWindowId: number = 0;
  private windows: WindowRoot[] = [];
  private pinnedIcons: DesktopIcon[] = [];
  private openedProgramIcons: DesktopIcon[] = [];
  private focused: WindowRoot;

  constructor(private userApi: UserAPI, public cdr: ChangeDetectorRef, private settings: SettingsService) { }

  ngAfterViewInit(): void {
    Desktop._instance = this;

    document.body.style.backgroundImage = "var(--background)";
    setTimeout(this.setupDesktop.bind(this), 0);

    const settings = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);
    document.querySelector(".taskbar").classList.toggle("wide", settings.taskbar == "big");
    document.body.style.setProperty("--taskbar-left", !document.querySelector(".taskbar").classList.contains("wide") ? "15%" : "0px");
    const icons = document.querySelector(".taskbar-icons") as HTMLElement;
    if (settings.taskbar_icons == "left") icons.style.marginRight = "auto";
    if (settings.taskbar_icons == "right") icons.style.marginLeft = "auto";
  }

  public addDesktopIcon(windowType: WindowType, index?: number): DesktopIcon {
    let icon;
    if (index != undefined)
      icon = this.desktopIconsRef.createComponent(DesktopIconComponent, {index: index});
    else
      icon = this.desktopIconsRef.createComponent(DesktopIconComponent);
    icon.instance.type = windowType;
    icon.instance.desktopRef = this;
    this.pinnedIcons.push(icon.instance);
    return icon.instance;
  }
  public addTaskbarIcon(windowType: WindowType, index?: number): DesktopIcon {
    let icon;
    if (index != undefined)
      icon = this.taskbarIconsRef.createComponent(TaskbarIconComponent, {index: index});
    else
      icon = this.taskbarIconsRef.createComponent(TaskbarIconComponent);
    icon.instance.type = windowType;
    icon.instance.desktopRef = this;
    this.pinnedIcons.push(icon.instance);
    return icon.instance;
  }

  public async addIcon(id: string, options: {taskbar?: boolean, desktop?: boolean, index?: {taskbar?: number, desktop?: number}}): Promise<DesktopIcon[]> {
    const windowType = this.getWindowTypeRef(id);

    if (windowType.permission != undefined && !(await this.userApi.checkForPermission(windowType.permission))) return undefined;

    const icons: DesktopIcon[] = [];

    if (options?.taskbar) {
      icons.push(this.addTaskbarIcon(windowType, options?.index?.taskbar));
    }

    if (options?.desktop) {
      icons.push(this.addDesktopIcon(windowType, options?.index?.desktop));
    }

    return icons;
  }

  public async addTempTaskbarIcon(id: string) {
    if (this.openedProgramIcons.filter(icon => icon.type.id === id).length != 0) return;

    const icon = (await this.addIcon(id, {taskbar: true}))[0];
    this.pinnedIcons.splice(this.pinnedIcons.indexOf(icon), 1);
    this.openedProgramIcons.push(icon);
  }

  public removeTaskbarIcon(id: string) {
    const icon = this.pinnedIcons.filter(icon => icon.type.id === id && icon.instance.tagName === "APP-TASKBAR-ICON")[0] as TaskbarIconComponent;
    icon.instance.parentElement.removeChild(icon.instance);
    this.pinnedIcons.splice(this.pinnedIcons.indexOf(icon), 1);
  }
  public removeDesktopIcon(id: string) {
    const icon = this.pinnedIcons.filter(icon => icon.type.id === id && icon.instance.tagName === "APP-DESKTOP-ICON")[0] as DesktopIconComponent;
    icon.instance.parentElement.removeChild(icon.instance);
    this.pinnedIcons.splice(this.pinnedIcons.indexOf(icon), 1);
  }

  public getWindow(windowId: number): WindowRoot {
    for (let window of this.windows) {
      if (window.windowId === windowId)
        return window;
    }
    return undefined;
  }
  public getWindowReferences(id: string): WindowRoot[] {
    const windows: WindowRoot[] = [];
    for (let window of this.windows) {
      if (window.contentType.id === id)
        windows.push(window);
    }
    return windows;
  }
  public async openWindow(id: string, args?: string[]): Promise<WindowRoot> {
    const type = this.getWindowTypeRef(id);
    if (type.type === undefined) {
      alert("This Application is not yet implemented!");
      return undefined;
    }

    if (this.getWindowTypeIcons(id).filter(icon => icon.instance.tagName === "APP-TASKBAR-ICON").length == 0) {
      await this.addTempTaskbarIcon(id);
    }

    const window = this.windowsContainerRef.createComponent(WindowRoot);
    window.instance.desktopRef = this;
    window.instance.windowId = this.nextWindowId;
    this.nextWindowId++;
    window.instance.contentType = type;
    window.instance.windowArgs = args === undefined ? [] : args;
    this.cdr.detectChanges();
    await window.instance.onOpen();
    this.windows.push(window.instance);

    const icons = this.getWindowTypeIcons(id);
    icons.forEach((icon) => {
      icon.focus = true;
      icon.onOpen();
    });
    this.openedProgramIcons.filter(icon => icon.type.id == window.instance.contentType.id).forEach((icon: DesktopIcon) => {
      icon.focus = true;
      icon.onOpen();
    });
    this.requestFocus(window.instance);
    return window.instance;
  }
  public async closeWindow(windowId: number) {
    const window = this.getWindow(windowId);
    if (!await window.contentRef.onClose()) return;
    window.object.parentElement?.removeChild(window.object);
    this.windows.splice(this.windows.indexOf(window), 1);

    const icons = this.getWindowTypeIcons(window.id);
    icons.forEach((icon) => {
      icon.focus = false;
      icon.onClose();
    });

    if (icons.filter(icon => icon.instance.tagName === "APP-TASKBAR-ICON").length == 0) {
      if (this.isWindowTypeOpen(window.contentType.id)) return;
      const icon = this.openedProgramIcons.filter(icon => icon.type.id == window.contentType.id)[0];
      icon.instance.parentElement.removeChild(icon.instance);
      this.openedProgramIcons.splice(this.openedProgramIcons.indexOf(icon), 1);
    }
  }
  public isWindowTypeOpen(id: string): boolean {
    for (let window of this.windows) {
      if (window.id === id)
        return true;
    }
    return false;
  }
  public doesUnfocusedWindowExist(id: string): WindowRoot {
    for (let window of this.windows) {
      if (window.id !== id) continue;
      if (window !== this.focused)
        return window;
    }
    return undefined;
  }
  public requestFocus(window: WindowRoot): void {
    delete this.focused;
    this.windows.forEach((window: WindowRoot) => window.onFocusLost());
    this.pinnedIcons.forEach((icon: DesktopIcon) => {
      icon.focus = false;
      icon.onLostFocus();
    });
    this.openedProgramIcons.forEach((icon: DesktopIcon) => {
      icon.focus = false;
      icon.onLostFocus();
    });
    if (window !== undefined) {
      window.onFocusGranted();
      const icons = this.getWindowTypeIcons(window.id);
      icons.forEach((icon: DesktopIcon) => {
        icon.focus = true;
        icon.onFocus();
      });
      this.openedProgramIcons.filter(icon => icon.type.id == window.contentType.id).forEach((icon: DesktopIcon) => {
        icon.focus = true;
        icon.onFocus();
      });
    }
    this.focused = window;
  }

  public getWindowTypeRef(id: string): WindowType {
    for (let window of windowTypes) {
      if (window.id === id)
        return window;
    }
    return undefined;
  }
  private getWindowTypeIcons(id: string): DesktopIcon[] {
    const icons: DesktopIcon[] = [];
    for (let icon of this.pinnedIcons) {
      if (icon.type.id === id)
        icons.push(icon);
    }
    return icons;
  }

  public get currentlyFocused(): WindowRoot { return this.focused; }

  private setupDesktop() {
    this.addIcon("settings", {taskbar: true, desktop: true});
    this.addIcon("explorer", {taskbar: true, desktop: true});
    this.addIcon("code", {taskbar: true, desktop: true});
    this.addIcon("admin", {taskbar: true, index: {taskbar: 0}});
  }

  onClick(event: MouseEvent): void {
    if (!this.search.mouseOver)
      this.search.toggleSearch(false);

    let focused = false;
    for (let window of this.windows) {
      window.onClick(event);
      if (window.isMouseOver)
        focused = true;
    }
    if (!focused)
      this.requestFocus(undefined);
  }
  onMouseDown(event: MouseEvent): void {
    for (let window of this.windows)
      window.onMouseDown(event);
  }
  onMouseUp(event: MouseEvent): void {
    for (let window of this.windows)
      window.onMouseUp(event);
  }
  onMouseMove(event: MouseEvent): void {
    for (let window of this.windows) {
      window.onMouseMove(event);
      window.onGlobalMove(event);

    }
  }
}
