import {Component, ElementRef} from '@angular/core';
import {Desktop, DesktopIcon, WindowType} from "../../desktop.component";

@Component({
  selector: 'app-taskbar-icon',
  templateUrl: './taskbar-icon.component.html',
  styleUrls: ['./taskbar-icon.component.scss']
})
export class TaskbarIconComponent implements DesktopIcon {
  type: WindowType;
  desktopRef: Desktop;
  focus: boolean;

  instance: HTMLElement;

  constructor(object: ElementRef) {
    this.instance = object.nativeElement;
  }

  onClick(): void {
    //Click Animation
    const container = this.instance.children.item(0).children.item(0) as HTMLElement;
    container.classList.add("click");
    setTimeout(() => container.classList.remove("click"), 100);

    const unfocusedWindow = this.desktopRef.doesUnfocusedWindowExist(this.type.id);
    if (unfocusedWindow !== undefined) {
      unfocusedWindow.focusable = true;
      unfocusedWindow.object.style.opacity = "100";
      unfocusedWindow.isMinimized = false;
      this.desktopRef.requestFocus(unfocusedWindow);
    }else this.desktopRef.openWindow(this.type.id, this.type.args);
  }

  public onFocus() {
    const focusBar = this.instance.children.item(0).children.item(2) as HTMLElement;
    focusBar.style.width = "var(--focused)";
  }
  public onLostFocus() {
    const focusBar = this.instance.children.item(0).children.item(2) as HTMLElement;
    focusBar.style.width = "var(--opened)";
  }
  public onClose() {
    if (this.desktopRef.isWindowTypeOpen(this.type.id)) return;
    const focusBar = this.instance.children.item(0).children.item(2) as HTMLElement;
    focusBar.style.opacity = "0";
  }
  public onOpen() {
    const focusBar = this.instance.children.item(0).children.item(2) as HTMLElement;
    focusBar.style.opacity = "100";
  }

}
