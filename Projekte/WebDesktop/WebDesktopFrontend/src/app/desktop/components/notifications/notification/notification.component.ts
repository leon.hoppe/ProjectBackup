import {ChangeDetectorRef, Component, ElementRef, ViewChild} from '@angular/core';
import {WindowType} from "../../../desktop.component";
import {Notifications} from "../notifications.component";

export interface Notifier {
  title: string;
  message: string;
  icon?: string;
  buttons?: {name: string, color?: "primary" | "accent" | "warn", onClick(): void}[];
}

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationWrapper {
  @ViewChild('element') element: ElementRef;
  @ViewChild('content') content: ElementRef;

  type: WindowType;
  message: Notifier;
  animate: {speed: number, duration: number};
  collapsed: boolean;
  collapseIcon: string = "expand_more";
  collapsable: boolean;
  height: number;
  private closed: boolean = false;

  constructor(private cdr: ChangeDetectorRef) { }

  public init(type: WindowType, message: Notifier, animate?: {speed: number, duration: number}, collapsed?: boolean) {
    this.type = type;
    this.message = message;
    this.animate = animate;
    this.collapsed = collapsed || false;
    this.collapsable = collapsed !== undefined;

    if (animate !== undefined) {
      this.element.nativeElement.style.transform = "translateX(150%)";
      this.element.nativeElement.style.transition = `transform ${animate.speed}ms ease-in-out, opacity ${animate.speed}ms linear`;
      setTimeout(() => {
        if (this.closed == true) return;
        this.element.nativeElement.style.transform = "translateX(0)"

        if (message.buttons === undefined || message.buttons?.length == 0)
          setTimeout(() => this.simpleClose(), animate.duration);
      }, animate.speed);
    }
    this.element.nativeElement.style.display = "flex";
    this.cdr.detectChanges();
    this.height = this.content.nativeElement.clientHeight;
    this.content.nativeElement.style.maxHeight = this.height + "px";

    if (collapsed == true) {
      this.collapseIcon = "chevron_right";
      this.content.nativeElement.style.maxHeight = "0";
      this.content.nativeElement.style.paddingBlock = "0";
    }
  }

  public close() {
    if (this.closed == true) return;
    this.closed = true;
    if (this.animate !== undefined) {
      this.element.nativeElement.style.transition = `transform ${this.animate.speed / 2}ms ease-in-out, opacity ${this.animate.speed / 2}ms linear`;
      this.element.nativeElement.style.transform = "scale(0.75)";
      setTimeout(() => {
        this.element.nativeElement.style.transform = "scale(0.75) translateY(-100%)";
        this.element.nativeElement.style.opacity = "0";
      }, this.animate.speed / 2);
      setTimeout(() => this.element.nativeElement.parentElement.parentElement.removeChild(this.element.nativeElement.parentElement), this.animate.speed);
    }else this.element.nativeElement.parentElement.parentElement.removeChild(this.element.nativeElement.parentElement);
    Notifications.remove(this);
  }

  public simpleClose() {
    if (this.closed == true) return;
    this.element.nativeElement.style.transform = "translateX(150%)"

    setTimeout(() => {
      if (this.closed == true) return;
      this.closed = true;
      this.element.nativeElement.parentElement.parentElement.removeChild(this.element.nativeElement.parentElement);
    }, this.animate.speed + 50);
  }

  public collapse(toggle?: boolean) {
    this.collapsed = toggle || !this.collapsed;

    if (this.collapsed) {
      this.collapseIcon = "chevron_right";
      this.content.nativeElement.style.maxHeight = "0";
      this.content.nativeElement.style.paddingBlock = "0";
    }else {
      this.collapseIcon = "expand_more";
      this.content.nativeElement.style.maxHeight = this.height + "px";
      this.content.nativeElement.style.paddingBlock = "5px";
    }
  }

}
