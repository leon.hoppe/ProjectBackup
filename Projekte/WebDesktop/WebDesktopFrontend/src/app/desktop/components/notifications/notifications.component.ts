import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {NotificationWrapper, Notifier} from "./notification/notification.component";
import {Window} from "../../windows/window.component";
import {Desktop} from "../../desktop.component";

@Component({
  selector: 'notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class Notifications implements OnInit {
  private static _instance: Notifications;
  private closed: boolean = false;
  private trace: boolean = false;

  @ViewChild('notificationsRef', {read: ViewContainerRef}) notificationsRef: ViewContainerRef;
  @ViewChild('trace') traceRef: ElementRef;
  notifications: { desktop: NotificationWrapper, trace: NotificationWrapper }[] = [];
  time: string;
  date: string;

  constructor(private cdr: ChangeDetectorRef) {
    Notifications._instance = this;
  }

  ngOnInit(): void {
    setInterval(() => {
      const dt = new Date();
      this.time = dt.toLocaleTimeString();
      this.date = dt.toLocaleDateString();
    }, 200);
  }

  public static async send(window: Window, notification: Notifier, animationSpeed: number = 300, animationDuration: number = 5000): Promise<NotificationWrapper> {
    const type = window.properties.windowType;

    const notify = Desktop.instance.notificationsRef.createComponent(NotificationWrapper);
    Desktop.instance.cdr.detectChanges();
    await notify.instance.init(type, notification, {speed: animationSpeed, duration: animationDuration});

    // TRACE
    const trace = this._instance.notificationsRef.createComponent(NotificationWrapper);
    this._instance.cdr.detectChanges();
    await trace.instance.init(type, notification, undefined, this._instance.notifications.length >= 2);

    this._instance.notifications.push({desktop: notify.instance, trace: trace.instance});

    if (this._instance.notifications.length > 2 && !this._instance.closed) {
      this._instance.closed = true;
      setTimeout(() => {
        this._instance.notifications.forEach((element) => {
          element.trace.collapse(true);
        })
      }, 1);
    }

    return notify.instance;
  }

  public static remove(notification: NotificationWrapper) {
    for (let i = 0; i < this._instance.notifications.length; i++) {
      const data = this._instance.notifications[i];
      if (data.desktop === notification) {
        this._instance.notifications.splice(i, 1);
        data.trace?.close();
        break;
      }
      if (data.trace === notification) {
        this._instance.notifications.splice(i, 1);
        data.desktop?.close();
        break;
      }
    }

    if (this._instance.notifications.length <= 2)
      this._instance.closed = false;

    if (this._instance.notifications.length == 0)
      this._instance.toggleTrace(false);
  }

  public toggleTrace(toggle?: boolean) {
    if (this.trace == toggle) return;
    this.trace = toggle || !this.trace;

    if (this.trace) {
      this.traceRef.nativeElement.style.opacity = '1';
      this.traceRef.nativeElement.style.transform = "translateY(0)";
    }else {
      this.traceRef.nativeElement.style.transform = "translateY(570px)";
      setTimeout(() => this.traceRef.nativeElement.style.opacity = '0', 250);
    }
  }

}
