import {Component, ElementRef} from '@angular/core';
import {Desktop, DesktopIcon, WindowType} from "../../desktop.component";

@Component({
  selector: 'app-desktop-icon',
  templateUrl: './desktop-icon.component.html',
  styleUrls: ['./desktop-icon.component.scss']
})
export class DesktopIconComponent implements DesktopIcon {
  desktopRef: Desktop;
  type: WindowType;
  focus: boolean;

  instance: HTMLElement;

  constructor(object: ElementRef) {
    this.instance = object.nativeElement;
  }

  public onFocus() {}
  public onLostFocus() {}
  public onClose() {}
  public onOpen() {}

  onDoubleClick(): void {
    this.desktopRef.openWindow(this.type.id, this.type.args);
  }

}
