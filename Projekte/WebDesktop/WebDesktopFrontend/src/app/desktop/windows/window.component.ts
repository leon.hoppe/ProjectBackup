import {ChangeDetectorRef, Component, ElementRef, Type, ViewChild, ViewContainerRef} from '@angular/core';
import {Desktop, WindowType} from "../desktop.component";

export interface WindowProperties {
  title: string;
  headerMessage: string;
  icon: string;
  resizable: boolean;
  size: {width: number, height: number};
  minSize: {width: number, height: number};
  position: {x: number, y: number};
  htmlWindowRef: HTMLElement;
  htmlContentRef: HTMLElement;
  windowRef: WindowRoot;
  headerActions: HeaderAction[];
  windowType: WindowType;
  dialog: {open: boolean, toggle(element: HTMLElement, toggle?: boolean): void};
  additionalArgs: any[];
  setMaximized(value: boolean): void;
  setSize(width: number, height: number): void;
  setPosition(x: number, y: number): void;
  updateWindow(): void;
  getWindow(): HTMLElement;
  addEventListener(listener: WindowEvents): void;
  requestFocus(): void;
}

export interface Window {
  properties: WindowProperties;
  onOpen(args: string[]): void;
  onClose(): boolean;
}

export interface WindowEvents {
  onClick(event: MouseEvent): void;
  onMouseDown(event: MouseEvent): void;
  onMouseUp(event: MouseEvent): void;
  onMouseMove(event: MouseEvent): void;
}

export interface HeaderAction {
  icon: string;
  name: string;
  click(event: MouseEvent): void;
}

@Component({
  selector: 'app-window',
  templateUrl: './window.component.html',
  styleUrls: ['./window.component.scss']
})
export class WindowRoot implements WindowEvents {
  @ViewChild('content', {read: ViewContainerRef}) content: ViewContainerRef;

  resizeIcon: string = "fullscreen";

  public contentType: WindowType;
  public object: HTMLElement;
  public contentRef: Window;
  public desktopRef: Desktop;
  public windowId: number;
  public isMinimized: boolean = false;
  public focusable: boolean = true;
  public windowArgs: string[];
  private hover: boolean = false;
  private windowEventHandler: WindowEvents[] = [];

  public get windowType(): WindowType { return this.contentRef?.properties.windowType };
  public get id(): string { return this.windowType.id; }

  constructor(object: ElementRef, private cdr: ChangeDetectorRef) {
    this.object = object.nativeElement;
    this.object.style.visibility = "hidden";
  }

  async onOpen() {
    const element = this.content.createComponent(this.contentType.type);
    this.cdr.detectChanges();
    this.contentRef = element.instance;

    this.object.style.position = "absolute";

    const offset = this.desktopRef.getWindowReferences(this.contentType.id).length * 10;

    this.contentRef.properties = {
      title: '',
      headerMessage: '',
      icon: '/assets/images/logo.png',
      resizable: true,
      size: {width: 800, height: 600},
      minSize: {width: 400, height: 300},
      position: {x: 200 + offset, y: 200 + offset},
      htmlContentRef: this.object.children.item(0).children.item(1) as HTMLElement,
      windowRef: this,
      headerActions: [],
      windowType: this.contentType,
      dialog: {open: false, toggle: this.toggleDialog.bind(this)},
      additionalArgs: [],
      setMaximized: this.setMaximized.bind(this),
      setSize: this.setSize.bind(this),
      setPosition: this.setPosition.bind(this),
      htmlWindowRef: this.object,
      updateWindow: this.updateWindow.bind(this),
      getWindow: this.getWindow.bind(this),
      addEventListener: this.addEventListener.bind(this),
      requestFocus: this.requestFocus.bind(this)
    };

    this.object.style.width = this.contentRef.properties.size.width + 'px';
    this.object.style.height = this.contentRef.properties.size.height + 'px';
    this.object.style.top = this.contentRef.properties.position.y + 'px';
    this.object.style.left = this.contentRef.properties.position.x + 'px';

    await this.contentRef.onOpen(this.windowArgs);
    this.object.style.visibility = "visible";
  }

  private setMaximized(value: boolean): void {
    this.maximized = !value;
    this.toggleMaximize();
  }
  private setSize(width: number, height: number): void {
    this.contentRef.properties.size.width = width;
    this.contentRef.properties.size.height = height;
    this.object.style.width = (this.contentRef.properties.size.width + 4) + "px";
    this.object.style.height = (this.contentRef.properties.size.height + 34) + "px";
  }
  private setPosition(x: number, y: number): void {
    this.contentRef.properties.position.x = x;
    this.contentRef.properties.position.y = y;
    this.object.style.top = this.contentRef.properties.position.x + 'px';
    this.object.style.left = this.contentRef.properties.position.y + 'px';
  }
  private updateWindow(): void {
    this.object.style.width = (this.contentRef.properties.size.width + 4) + "px";
    this.object.style.height = (this.contentRef.properties.size.height + 34) + "px";
    this.object.style.top = this.contentRef.properties.position.x + 'px';
    this.object.style.left = this.contentRef.properties.position.y + 'px';
  }
  private getWindow(): HTMLElement {
    return this.object.children.item(0) as HTMLElement;
  }
  private addEventListener(listener: WindowEvents): void {
    this.windowEventHandler.push(listener);
  }
  private toggleDialog(element: HTMLElement, toggle?: boolean) {
    this.contentRef.properties.dialog.open = element.classList.toggle("visible", toggle);
  }
  private requestFocus() {
    Desktop.instance.requestFocus(this);
  }

  public addAdditionalArgs(...args: any[]) {
    for (let i = 0; i < args.length; i++) {
      this.contentRef.properties.additionalArgs.push(args[i]);
    }
  }

  // WINDOW FUNCTIONS
  public mouseEnter(): void { this.hover = true; }
  public mouseLeave(): void { this.hover = false; }
  public get isMouseOver(): boolean { return this.hover; }

  private offsetX: number;
  private offsetY: number;
  private drag: boolean;
  private tempSize: {width: number, height: number};
  private windowDrag(event: MouseEvent): void {
    if (!this.drag || this.isResizing) return;
    const x = event.clientX - this.offsetX;
    const y = event.clientY - this.offsetY;
    this.object.style.left = x + 'px';
    this.object.style.top = y + 'px';
  }
  public windowDragStart(event: MouseEvent): void {
    if (this.maximized) return;
    this.offsetX = event.clientX - this.object.offsetLeft;
    this.offsetY = event.clientY - this.object.offsetTop;
    this.drag = true;
    this.object.classList.add("unselectable");
  }
  public windowDragStop(): void {
    if (!this.drag) return;
    this.drag = false;
    this.contentRef.properties.position.x = this.object.offsetLeft;
    this.contentRef.properties.position.y = this.object.offsetTop;
    this.object.classList.remove("unselectable");
  }

  private maximized: boolean = false;
  public toggleMaximize(): void {
    if (!this.contentRef.properties.resizable) return;
    this.maximized = !this.maximized;
    this.object.style.transition = 'all 300ms';
    if (this.maximized) {
      this.tempSize = {width: this.contentRef.properties.size.width, height: this.contentRef.properties.size.height};
      this.contentRef.properties.position.x = this.object.offsetLeft;
      this.contentRef.properties.position.y = this.object.offsetTop;
      this.object.style.width = "100%";
      this.object.style.height = "calc(100% - 50px)";
      this.object.style.top = '0';
      this.object.style.left = '0';
      this.contentRef.properties.size.width = this.object.clientWidth;
      this.contentRef.properties.size.height = this.object.clientHeight;

      //img.src = "/assets/icons/window/minimize.png";
      this.resizeIcon = "fullscreen_exit";
    }else {
      this.setSize(this.tempSize.width, this.tempSize.height);
      this.object.style.top = this.contentRef.properties.position.y + 'px';
      this.object.style.left = this.contentRef.properties.position.x + 'px';

      //img.src = "/assets/icons/window/maximize.png";
      this.resizeIcon = "fullscreen";
    }
    setTimeout(() => this.object.style.transition = 'none', 310);
  }
  public minimize(): void {
    this.focusable = false;
    this.object.style.opacity = "0";
    this.isMinimized = true;
    setTimeout(() => this.desktopRef.requestFocus(undefined), 0);
  }
  public close(): void {
    this.focusable = false;
    this.desktopRef.closeWindow(this.windowId);
  }

  private resizeOrigin: {x: number, y: number, width: number, height: number};
  private isResizing: boolean = false;
  private readonly resizingArea: number = 10;
  private lastResizeManager: string;
  private handleResizeCursor(event: MouseEvent) {
    if (this.desktopRef.currentlyFocused !== this) return;
    if (this.maximized) return;
    if (!this.contentRef.properties.resizable) return;
    const window = this.getWindow();
    const c = this.isHoverBorder(event);

    if(c) {
      window.style.cursor = c + "-resize";
      this.lastResizeManager = c;
    }
    else
     window.style.cursor = "auto";

    if (this.isResizing) this.handleResizing(event);
  }
  private startResizing(event: MouseEvent): void {
    if (this.desktopRef.currentlyFocused !== this) return;
    if (this.maximized) return;
    if (!this.isHoverBorder(event)) return;
    this.isResizing = true;
    this.resizeOrigin = {
      x: this.object.offsetLeft,
      y: this.object.offsetTop,
      width: this.object.offsetWidth,
      height: this.object.offsetHeight
    };
    this.object.classList.add("unselectable");
  }
  private stopResizing(): void {
    if (!this.isResizing) return;
    this.isResizing = false;
    this.contentRef.properties.position = {x: this.resizeOrigin.x, y: this.resizeOrigin.y};
    this.contentRef.properties.size = {width: this.resizeOrigin.width, height: this.resizeOrigin.height};
    delete this.resizeOrigin;
    this.object.classList.remove("unselectable");
  }
  private handleResizing(event: MouseEvent) {
    const window = this.getWindow().getBoundingClientRect();
    const threshold = 20;

    if (this.lastResizeManager.includes("n") && event.clientY <= window.y + threshold) { //TOP
      this.object.style.top = (this.resizeOrigin.y + event.movementY) + "px";
      this.object.style.height = (this.resizeOrigin.height - event.movementY) + "px";
    }else if (this.lastResizeManager.includes("s") && event.clientY >= window.bottom - threshold) { //BOTTOM
      this.object.style.height = (this.resizeOrigin.height + event.movementY) + "px";
    }if (this.lastResizeManager.includes("w") && event.clientX <= window.x + threshold) { //LEFT
      this.object.style.left = (this.resizeOrigin.x + event.movementX) + "px";
      this.object.style.width = (this.resizeOrigin.width - event.movementX) + "px";
    }else if (this.lastResizeManager.includes("e") && event.clientX >= window.right - threshold) { //RIGHT
      this.object.style.width = (this.resizeOrigin.width + event.movementX) + "px";
    }

    if (this.object.offsetWidth < this.contentRef.properties.minSize.width)
      this.object.style.width = this.contentRef.properties.minSize.width + "px";
    if (this.object.offsetHeight < this.contentRef.properties.minSize.height)
      this.object.style.height = this.contentRef.properties.minSize.height + "px";

    this.resizeOrigin = {
      x: this.object.offsetLeft,
      y: this.object.offsetTop,
      width: this.object.offsetWidth,
      height: this.object.offsetHeight
    };
  }

  private isHoverBorder(event: MouseEvent): string {
    const window = this.getWindow();
    const delta = this.resizingArea;                      // the thickness of the hovered border area

    const rect = window.getBoundingClientRect();
    const x = event.clientX - rect.left,   // the relative mouse position to the element
      y = event.clientY - rect.top,        // ...
      w = rect.right - rect.left,          // width of the element
      h = rect.bottom - rect.top;          // height of the element

    let c = "";                            // which cursor to use
    if(y < delta) c += "n";                // north
    else if( y > h - delta) c += "s";      // south
    if(x < delta) c += "w";                // west
    else if(x > w - delta) c += "e";       // east

    return c;
  }

  public onClick(event: MouseEvent): void {
    if (this.desktopRef.currentlyFocused !== this) return;
    if (!this.hover) return;
    for (let listener of this.windowEventHandler)
      listener.onClick(event);
  }
  public onMouseDown(event: MouseEvent): void {
    if (this.desktopRef.currentlyFocused !== this) return;
    if (!this.hover) return;
    this.startResizing(event);
    for (let listener of this.windowEventHandler)
      listener.onMouseDown(event);
  }
  public onMouseUp(event: MouseEvent): void {
    this.stopResizing();
    if (this.desktopRef.currentlyFocused !== this) return;
    if (!this.hover) return;
    for (let listener of this.windowEventHandler)
      listener.onMouseUp(event);
  }
  public onMouseMove(event: MouseEvent): void {
    if (this.desktopRef.currentlyFocused !== this) return;
    if (!this.hover) return;
    for (let listener of this.windowEventHandler)
      listener.onMouseMove(event);
  }
  public onGlobalMove(event: MouseEvent) {
    this.windowDrag(event);
    if (this.desktopRef.currentlyFocused !== this) return;
    this.handleResizeCursor(event);
  }
  public onFocusGranted(): void {
    if (!this.focusable) return;
    const container = this.object.children.item(0) as HTMLElement;
    container.style.zIndex = "var(--focused)";
    this.getWindow().style.boxShadow = "var(--selected-shadow)";
  }
  public onFocusLost(): void {
    this.stopResizing();
    this.windowDragStop();
    const container = this.object.children.item(0) as HTMLElement;
    container.style.zIndex = "var(--opened)";
    const window = this.getWindow();
    window.style.cursor = "default";
    window.style.boxShadow = "none";
  }
}
