import {Component, ElementRef, ViewChild} from '@angular/core';
import {Window, WindowProperties} from "../window.component";
import {UserAPI} from "../../../api/userapi.service";
import {User, UserEditor} from "../../../entitys/user";
import Swal from "sweetalert2";
import {environment} from "../../../../environments/environment";
import {CrudService} from "../../../crud.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminWindow implements Window {
  @ViewChild('console') console: ElementRef;
  @ViewChild('edit') edit: ElementRef;
  @ViewChild('permissions') permissions: ElementRef;

  @ViewChild('firstname') firstname: ElementRef;
  @ViewChild('lastname') lastname: ElementRef;
  @ViewChild('username') username: ElementRef;
  @ViewChild('email') email: ElementRef;
  @ViewChild('password') password: ElementRef;

  @ViewChild('currPermission') currentPermission: ElementRef;

  constructor(public userApi: UserAPI, private service: CrudService) { }

  properties: WindowProperties;
  updater: WebSocket;
  users: User[];
  current: User;
  userPermissions: string[];

  onClose(): boolean {
    this.updater.close();
    return true;
  }

  async onOpen() {
    this.properties.title = "Admin Panel";
    this.properties.icon = "/assets/icons/admin.png";
    this.properties.resizable = false;
    this.properties.setSize(1200, 800);

    //Update
    this.updater = await this.service.createWebSocketConnection(environment.updateUrl);
    this.updater.onopen = () => this.updater.send("Ping");
    this.updater.onmessage = (e) => {
      this.console.nativeElement.innerText += e.data + '\n';
      this.console.nativeElement.scrollTop = this.console.nativeElement.scrollHeight;
    }

    this.users = await this.userApi.getUsers();
  }

  onUpdate() {
    this.updater.send("Start");
  }

  async onEdit(event: MouseEvent) {
    event.preventDefault();
    let user: UserEditor = {firstName: "", lastName: "", username: "", email: "", password: ""};
    user.firstName = this.firstname.nativeElement.value == this.current.firstName ? "" : this.firstname.nativeElement.value;
    user.lastName = this.lastname.nativeElement.value == this.current.lastName ? "" : this.lastname.nativeElement.value;
    user.username = this.username.nativeElement.value == this.current.username ? "" : this.username.nativeElement.value;
    user.email = this.email.nativeElement.value == this.current.email ? "" : this.email.nativeElement.value;
    user.password = this.password.nativeElement.value == this.current.password ? "" : this.password.nativeElement.value;
    const success = await this.userApi.editUser(this.current.id, user);
    if (success) {
      await Swal.fire({
        icon: 'success',
        title: 'User edited',
        text: 'The User was successfully edited',
        timer: 2000,
        showConfirmButton: false
      });
      this.cancelEdit(event);
      this.users = await this.userApi.getUsers();
    }else {
      await Swal.fire({
        icon: 'error',
        title: 'Editing failed',
        text: 'The User could not be edited',
        timer: 2000,
        showConfirmButton: false
      });
    }
  }

  cancelEdit(event: MouseEvent) {
    event.preventDefault();
    delete this.current;
    this.edit.nativeElement.classList.remove("enable");
  }

  editUser(user: User) {
    this.current = user;
    this.edit.nativeElement.classList.add("enable");
  }

  async deleteUser(user: User) {
    const confirm = await Swal.fire({
      title: 'Do you really want to DELETE this User?',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonText: 'Delete',
    });
    if (!confirm.isConfirmed) return;

    const success = await this.userApi.deleteUser(user.id);
    if (success) {
      await Swal.fire({
        icon: 'success',
        title: 'User deleted',
        text: 'The User was successfully deleted',
        timer: 2000,
        showConfirmButton: false
      });
      this.users = await this.userApi.getUsers();
    }else {
      await Swal.fire({
        icon: 'error',
        title: 'Deleting failed',
        text: 'The User could not be deleted',
        timer: 2000,
        showConfirmButton: false
      });
    }
  }

  async editPermissions(user: User) {
    this.current = user;
    this.userPermissions = await this.userApi.getPermissions(user.id);
    this.permissions.nativeElement.classList.add("enable");
  }

  async deletePermission(permission: string) {
    const success = await this.userApi.removePermission(this.current.id, permission);

    if (success) {
      await Swal.fire({
        icon: 'success',
        title: 'Permission deleted',
        text: 'The Permission was successfully deleted',
        timer: 2000,
        showConfirmButton: false
      });
      this.userPermissions = await this.userApi.getPermissions(this.current.id);
    }else {
      await Swal.fire({
        icon: 'error',
        title: 'Deleting failed',
        text: 'The Permission could not be deleted',
        timer: 2000,
        showConfirmButton: false
      });
    }
  }

  async addPermission() {
    const success = await this.userApi.addPermission(this.current.id, this.currentPermission.nativeElement.value);

    if (success) {
      await Swal.fire({
        icon: 'success',
        title: 'Permission added',
        text: 'The Permission was successfully added',
        timer: 2000,
        showConfirmButton: false
      });
      this.userPermissions = await this.userApi.getPermissions(this.current.id);
    }else {
      await Swal.fire({
        icon: 'error',
        title: 'Adding failed',
        text: 'The Permission could not be added',
        timer: 2000,
        showConfirmButton: false
      });
    }
  }

  cancelPermission() {
    delete this.current;
    delete this.userPermissions;
    this.permissions.nativeElement.classList.remove("enable");
  }

}
