import {Component, ElementRef, ViewChild} from '@angular/core';
import {Window, WindowProperties} from "../window.component";
import {Desktop, windowTypes} from "../../desktop.component";

type PromiseType = "toggleMaximize" | "minimize" | "close" | "windowTypes" | "openWindow" | "closeWindow" | "requestFocus" | "properties" | "update" | "triggerListener" |
  "title" | "headerMessage" | "icon" | "resizable" | "size" | "minSize" | "position" | "windowType" | "additionalArgs";

interface Packet {
  type: PromiseType;
  data?: any;
}

@Component({
  selector: 'app-api-window',
  templateUrl: './api-window.component.html',
  styleUrls: ['./api-window.component.scss']
})
export class ApiWindow implements Window {
  @ViewChild('frame') frameRef: ElementRef;
  frame: HTMLIFrameElement;
  properties: WindowProperties;
  eventListener: any;

  constructor() { }

  onOpen(args: string[]): void {
    this.frame = this.frameRef.nativeElement as HTMLIFrameElement;
    this.frame.src = this.properties.windowType.url;

    this.eventListener = (event: MessageEvent) => {
      this.handleEvent(event.data.type, event.data.data);
    };
    window.addEventListener("message", this.eventListener, false);
  }

  onClose(): boolean {
    window.removeEventListener("message", this.eventListener);
    return true;
  }

  private sendMessage(packet: Packet): void {
    this.frame.contentWindow.postMessage(packet, new URL(this.frame.src).origin)
  }

  private handleEvent(type: PromiseType, data: any): void {
    switch (type) {

      case "windowTypes":
        const types = [];
        for (let w of windowTypes) {
          types.push({
            type: w.type?.name,
            id: w.id,
            name: w.name,
            icon: w.icon,
            permission: w.permission,
            args: w.args,
            canOpen: w.canOpen,
            url: w.url
          });
        }
        this.sendMessage({type, data: types});
        break;

      case "openWindow":
        Desktop.instance.openWindow(data.id, data.args);
        break;

      case "closeWindow":
        Desktop.instance.closeWindow(data)
        break;

      case "requestFocus":
        this.properties.requestFocus();
        break;

      case "minimize":
        this.properties.windowRef.minimize();
        break;

      case "close":
        this.properties.windowRef.close();
        break;

      case "properties":
        this.handlePropertyChange(data.name, data.value)
        break;

      case "title":
        this.sendMessage({type, data: this.properties.title});
        break;

      case "headerMessage":
        this.sendMessage({type, data: this.properties.headerMessage});
        break;

      case "icon":
        this.sendMessage({type, data: this.properties.icon});
        break;

      case "resizable":
        this.sendMessage({type, data: this.properties.resizable});
        break;

      case "size":
        this.sendMessage({type, data: this.properties.size});
        break;

      case "minSize":
        this.sendMessage({type, data: this.properties.minSize});
        break;

      case "position":
        this.sendMessage({type, data: this.properties.position});
        break;

      case "windowType":
        this.sendMessage({type, data: this.properties.windowType});
        break;

      case "additionalArgs":
        this.sendMessage({type, data: this.properties.additionalArgs});
        break;

      case "update":
        this.properties.updateWindow();
        break;

    }
  }

  private handlePropertyChange(property: string, value: any): void {
    switch (property) {

      case "title":
        this.properties.title = value;
        break;

      case "headerMessage":
        this.properties.headerMessage = value;
        break;

      case "icon":
        this.properties.icon = value;
        break;

      case "resizable":
        this.properties.resizable = value;
        break;

      case "minSize":
        this.properties.minSize = value;
        break;

      case "maximized":
        this.properties.setMaximized(value);
        break;

      case "size":
        this.properties.setSize(value.width, value.heigth);
        break;

      case "position":
        this.properties.setPosition(value.x, value.y);
        break;

    }
  }

}
