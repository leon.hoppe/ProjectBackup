import { Component } from '@angular/core';
import {Window, WindowProperties} from "../window.component";

@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss']
})
export class CodeWindow implements Window {

  properties: WindowProperties;

  constructor() { }

  onClose(): boolean { return true; }

  onOpen(): void {
    this.properties.title = "Code Editor";
    this.properties.headerMessage = "Bearbeite deine Datein in der Cloud";
    this.properties.icon = "/assets/icons/code.png";
    this.properties.setMaximized(true);
  }

}
