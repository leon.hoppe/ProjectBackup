import {Component, ElementRef, OnInit, Type, ViewChild} from '@angular/core';
import {Desktop, WindowType, windowTypes} from "../../desktop.component";
import {UserAPI} from "../../../api/userapi.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @ViewChild('container') container: ElementRef;
  @ViewChild('searchInput') searchInput: ElementRef;

  programs: WindowType[];
  permissions: string[];
  mouseOver: boolean = false;
  selected: number = -1;

  constructor(private users: UserAPI) { }

  ngOnInit() {
    setTimeout( async () => {
      this.searchInput.nativeElement.value = "";
      this.permissions = await this.users.getOwnPermissions();
      this.programs = windowTypes.filter(type => this.hasPermission(type.permission)).sort((n1, n2) => n1.name < n2.name ? -1 : 1);
    }, 0);
  }

  toggleSearch(toggle?: boolean) {
    const open = this.container.nativeElement.classList.toggle("show", toggle);

    if (open) {
      this.ngOnInit();

      setTimeout(() => this.searchInput.nativeElement.focus(), 201);
    }

    const focusBar = document.querySelector("#search-opened") as HTMLElement;
    focusBar.style.opacity = open ? "100" : "0";
  }

  playClickAnimation() {
    const container = document.querySelector("#search-icon").children.item(0) as HTMLElement;
    container.classList.add("click");
    setTimeout(() => container.classList.remove("click"), 100);
  }

  search(key: string, keyCode: string) {
    if (keyCode !== "ArrowDown" && keyCode !== "ArrowUp" && keyCode !== "Enter")
      this.selected = -1;

    if (key == "") {
      this.programs = windowTypes.filter(type => this.hasPermission(type.permission)).sort((n1, n2) => n1.name < n2.name ? -1 : 1);
      return;
    }

    this.programs = windowTypes.filter(type =>
      this.hasPermission(type.permission) &&
      (type.name.toLowerCase().includes(key.toLowerCase()) || type.id.toLowerCase().includes(key.toLowerCase()))
    ).sort((n1, n2) => n1.name < n2.name ? -1 : 1);
  }

  hasPermission(permission: string): boolean {
    if (permission == undefined) return true;

    if (this.permissions.includes("*") || this.permissions.includes(permission))
      return true;

    const splice = permission.split(".");
    let builder: string = "";
    for (let pp of splice) {
      builder += pp + ".";
      if (this.permissions.includes(builder + "*"))
        return true;
    }

    return false;
  }

  openProgram(id: string) {
    Desktop.instance.openWindow(id);
    this.toggleSearch();
  }

  togglePin(type: WindowType) {
    const hasIcon = this.isPinned(type);

    if (hasIcon)
      Desktop.instance.removeTaskbarIcon(type.id);
    else
      Desktop.instance.addTaskbarIcon(type);
  }

  isPinned(type: WindowType): boolean {
    return Desktop.pinnedIcons.filter(icon => icon.type.id === type.id && icon.instance.tagName === "APP-TASKBAR-ICON").length != 0;
  }

  moveUp() {
    this.selected++;
    if (this.selected >= this.programs.length)
      this.selected = 0;
  }

  moveDown() {
    this.selected--;
    if (this.selected < 0)
      this.selected = this.programs.length - 1;
  }
}
