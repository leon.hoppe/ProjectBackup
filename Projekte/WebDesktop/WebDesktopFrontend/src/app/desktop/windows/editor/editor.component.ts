import {Component, ElementRef, ViewChild} from '@angular/core';
import {Window, WindowProperties} from "../window.component";
import {EditorConfiguration} from "codemirror";
import * as CodeMirror from "codemirror";
import {FileAPI} from "../../../api/fileapi.service";
import {ExplorerWindow} from "../explorer/explorer.component";
import {HttpEventType} from "@angular/common/http";
import Swal from "sweetalert2";
import {SettingsService} from "../../../api/settings.service";
import {defaultPersonalization, Personalization} from "../../../entitys/settings";
import {CodemirrorComponent} from "@ctrl/ngx-codemirror";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorWindow implements Window {
  @ViewChild('editor') editor: CodemirrorComponent;
  @ViewChild('dialog') saveChanges: ElementRef;

  constructor(private files: FileAPI, private settings: SettingsService) { }

  properties: WindowProperties;
  options: EditorConfiguration;
  file: {name: string, directory: string, ext: string, url: string};
  eventListener: EventListenerOrEventListenerObject;
  saved: boolean = true;

  onClose(): boolean {
    document.removeEventListener(SettingsService.eventChange.type, this.eventListener);

    if (!this.saved) {
      this.properties.dialog.toggle(this.saveChanges.nativeElement, true);
      return false;
    }

    return true;
  }

  async onOpen(args: string[]) {
    this.properties.title = "Editor";
    this.properties.headerMessage = "untitled.txt";
    this.properties.icon = "/assets/icons/editor.png";
    this.properties.minSize = {width: 800, height: 600};

    this.options = {
      lineNumbers: true,
      theme: this.getThemeBySystemTheme(),
      mode: "text",
      value: "",
    }
    this.file = undefined;
    this.editor.codeMirror?.setValue("");

    if (args.filter((arg) => arg.includes("--file=")).length !== 0) {
      const url = args.filter((arg) => arg.includes("--file="))[0].replace("--file=", "");
      const {file, directory, ext} = ExplorerWindow.extractFileAndFolderFromURL(url);
      this.properties.headerMessage = url;
      this.file = {name: file, directory: directory, ext: ext, url: url};

      const download = await this.files.downloadFile(directory, file);
      download.subscribe(async (event) => {
        if (event.type === HttpEventType.Response) {
          const blob = event.body as Blob;

          this.options.value = await blob.text();
          this.options.mode = CodeMirror.findModeByExtension(ext).mode || "text";
        }
      });
    }

    this.eventListener = (() => this.options.theme = this.getThemeBySystemTheme()).bind(this);
    document.addEventListener(SettingsService.eventChange.type, this.eventListener);
  }

  handleDrop(event: DragEvent) {
    if (event === undefined) return;
    if (ExplorerWindow.dragItem === undefined ||
      ExplorerWindow.dragItem?.isDirectory === true) {
      Swal.fire({
        icon: 'error',
        title: 'Ungültiger Inhalt',
        text: 'Du kannst nur Dateien öffnen',
        timer: 2000,
        showConfirmButton: false
      });
      return;
    }

    this.onOpen(["--file=" + ExplorerWindow.dragItem.url]);
  }

  private getThemeBySystemTheme(): string {
    const theme = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization).theme;

    if (theme === "theme-dark")
      return "material";
    if (theme === "theme-light")
      return "idea";

    return "material";
  }

  async saveAs() {
    return new Promise((resolve => {
      ExplorerWindow.openPopup("save-file", async (url: string) => {
        const file = ExplorerWindow.extractFileAndFolderFromURL(url);
        const data = this.files.toFile(this.editor.value as string, file.file);
        await this.files.quickUpload(data, file.directory);
        this.properties.headerMessage = url;
        this.file = {
          name: file.file,
          directory: file.directory,
          ext: file.ext,
          url: url
        };
        this.saved = true;
        resolve(null);
      }, this.file?.name || "untitled.txt", this.file?.directory || "/");
    }))
  }

  async save() {
    if (this.file == undefined) await this.saveAs();
    else {
      const data = this.files.toFile(this.editor.value as string, this.file.name);
      await this.files.quickUpload(data, this.file.directory);
      this.properties.headerMessage = this.file.url;
      this.saved = true;
    }
  }

  open() {
    ExplorerWindow.openPopup("choose-file", async (url: string) => {
      await this.onOpen(["--file=" + url]);
    }, "");
  }

  edit() {
    if (!this.properties.headerMessage.endsWith("*"))
      this.properties.headerMessage += "*";
    this.saved = false;
  }

  async saveOnClose() {
    await this.save();
    this.properties.windowRef.close();
  }
}
