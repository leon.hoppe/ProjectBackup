import {Component, ElementRef, ViewChild} from '@angular/core';
import {Window, WindowProperties} from "../window.component";
import {FileAPI} from "../../../api/fileapi.service";
import {DirectoryContent, DirectoryInformation, FileType} from "../../../entitys/files";
import {HttpDownloadProgressEvent, HttpEventType, HttpUploadProgressEvent} from "@angular/common/http";
import {saveAs} from "file-saver";
import {Desktop, WindowType, windowTypes} from "../../desktop.component";
import Swal from "sweetalert2";

export type HandlerFunction = (url: string) => void;

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.scss']
})
export class ExplorerWindow implements Window {
  public static dragItem: {name: string, url: string, isDirectory: boolean};

  @ViewChild('search') search: ElementRef;
  @ViewChild('content') content: ElementRef;
  @ViewChild('selectProgram') selectProgram: ElementRef;
  @ViewChild('override') override: ElementRef;

  public properties: WindowProperties;
  public currentDirectory: string = "/";
  public currentDirectoryInfo: DirectoryContent;
  public currentDirectoryInformation: DirectoryInformation;
  public currentItem: {name: string, isDirectory: boolean};
  public progress: number = 0;
  public openPrograms: WindowType[];
  public nameBarOptions: {enabled: boolean, value: string, type: string} = {enabled: false, value: "", type: ""};

  public constructor(public files: FileAPI) {}

  public async onOpen(args: string[]) {
    this.properties.title = "Explorer";
    this.properties.headerMessage = "Verwalte deine Dateien";
    this.properties.icon = "/assets/icons/explorer.png";
    this.properties.minSize = {width: 800, height: 600};

    await this.loadDirectory();

    if (args.includes("--save-file") || args.includes("--choose-file") || args.includes("--choose-folder")) {
      this.nameBarOptions.enabled = true;

      setTimeout(() => {
        this.nameBarOptions.value = this.properties.additionalArgs[0];
        this.nameBarOptions.type = this.properties.additionalArgs[2];
        this.changeDir(this.properties.additionalArgs[3]);
      }, 0);
    }
  }

  public onClose(): boolean { return true; }

  humanFileSize(size: number): string {
    if (size === 0) return '0 B';
    let i = Math.floor( Math.log(size) / Math.log(1024) );
    return ( size / Math.pow(1024, i) ).toFixed(2) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
  };

  async loadDirectory() {
    this.search.nativeElement.value = "";
    this.currentDirectoryInformation = undefined;
    this.currentDirectoryInfo = await this.files.getDirectoryContent(this.currentDirectory);
    this.currentDirectoryInfoSave = this.currentDirectoryInfo;

    for (let i = 0; i < this.currentDirectoryInfo.directories.length; i++) {
      if (this.currentDirectoryInfo.directories[i].startsWith("~"))
        this.currentDirectoryInfo.directories.splice(i, 1);
    }

    this.currentDirectoryInformation = await this.files.getDirectoryInfo(this.currentDirectory);
  }

  async changeDir(directory: string) {
    this.currentDirectory = directory;

    if (!this.currentDirectory.endsWith("/"))
      this.currentDirectory += "/";

    await this.loadDirectory();
  }

  getUpFolder(): string {
    let copy = this.currentDirectory;
    if (copy.replace("/", "") === "") return "/";

    if (copy.endsWith("/")) copy = copy.substr(0, copy.length - 1);
    let result = "/";

    const folders = copy.split("/");
    for (let i = 0; i < folders.length - 1; i++) {
      if (folders[i] === "") continue;
      result += folders[i] + "/";
    }

    return result;
  }

  public currentDirectoryInfoSave: DirectoryContent;
  async searchFiles(keyword: string) {
    if (keyword === "") {
      this.currentDirectoryInfo = this.currentDirectoryInfoSave;
      return;
    }

    const files = [];
    for (let file of this.currentDirectoryInfoSave.files) {
      if (file.includes(keyword))
        files.push(file);
    }

    const directories = [];
    for (let directory of this.currentDirectoryInfoSave.directories) {
      if (directory.includes(keyword))
        directories.push(directory);
    }

    this.currentDirectoryInfo = {files: files, directories: directories};
  }

  selectItem(element: HTMLDivElement, item: string, isDirectory: boolean) {
    for (let child of this.content.nativeElement.children)
      child.classList.remove("selected");
    element?.classList.add("selected");

    this.currentItem = {name: item, isDirectory: isDirectory};

    if (this.nameBarOptions.enabled) {
      if (this.nameBarOptions.type == "choose-folder" && !isDirectory) return;
      if (this.nameBarOptions.type != "choose-folder" && isDirectory) return;

      this.nameBarOptions.value = item;
    }
  }

  async deleteItem(name: string) {
    const confirm = await Swal.fire({
      title: 'Möchtest du diese Datei wirklich löschen?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Löschen',
      denyButtonText: 'Abbrechen',
    });

    if (confirm.isConfirmed) {
      await this.files.delete(this.currentDirectory + name);
      await this.loadDirectory();
    }
  }

  async createDirectory(name: string) {
    await this.files.createDirectory(this.currentDirectory, name);
    await this.loadDirectory();
  }

  async uploadFiles(input: HTMLInputElement) {
    const file = input.files[0];
    const observer = await this.files.uploadFile(file, this.currentDirectory);
    observer.subscribe(async event => {
      if (event.type === HttpEventType.UploadProgress) {
        const e = event as HttpUploadProgressEvent;
        this.progress = e.loaded / e.total * 100;
      }
      if (event.type === HttpEventType.Response) {
        input.value = "";
        await this.loadDirectory();
        setTimeout(() => this.progress = 0, 2000);
      }
    });
  }

  async downloadFile(name: string) {
    const observer = await this.files.downloadFile(this.currentDirectory, name);
    observer.subscribe(event => {
      if (event.type === HttpEventType.DownloadProgress) {
        const e = event as HttpDownloadProgressEvent;
        this.progress = e.loaded / e.total * 100;
      }
      if (event.type === HttpEventType.Response) {
        saveAs(event.body as Blob, name);
        setTimeout(() => this.progress = 0, 2000);
      }
    })
  }

  onDrag(item: string, isDirectory: boolean) {
    ExplorerWindow.dragItem = {name: item, url: this.currentDirectory + item, isDirectory: isDirectory};
  }

  async onDrop(event: DragEvent, directory: string) {
    await this.onDropUp(event, this.currentDirectory + directory);
  }

  onDragOver(event: DragEvent, directory: string) {
    if (directory != ExplorerWindow.dragItem.name)
      event.preventDefault();
  }

  onDragUpOver(event: DragEvent) {
    if (this.currentDirectory == "/") return;
    event.preventDefault();
  }

  async onDropUp(event: DragEvent, directory: string) {
    event.preventDefault();

    if (ExplorerWindow.dragItem.isDirectory) {
      await this.files.moveDirectory(this.currentDirectory, ExplorerWindow.dragItem.name, directory);
    }else {
      await this.files.moveFile(this.currentDirectory, ExplorerWindow.dragItem.name, directory);
    }

    delete ExplorerWindow.dragItem;
    await this.loadDirectory();
  }

  getFileImage(name: string): string {
    const type = ExplorerWindow.getFileType(name);

    //IMAGES
    if (type == FileType.IMAGE)
      return "image";

    //VIDEO
    if (type == FileType.VIDEO)
      return "video_file";

    //AUDIO
    if (type == FileType.AUDIO)
      return "audio_file";

    //COMPRESSED
    if (type == FileType.COMPRESSED)
      return "folder_zip";

    //DISK
    if (type == FileType.DISK)
      return "album";

    //DATABASE
    if (type == FileType.DATABASE)
      return "storage";

    //DATA
    if (type == FileType.DATA)
      return "description";

    //EMAIL
    if (type == FileType.EMAIL)
      return "email";

    //EXECUTABLE
    if (type == FileType.EXECUTABLE)
      return "terminal";

    //FONTS
    if (type == FileType.FONT)
      return "segment";

    //INTERNET
    if (type == FileType.INTERNET)
      return "language";

    //PRESENTATION
    if (type == FileType.PRESENTATION)
      return "co_present";

    //SPREADSHEET
    if (type == FileType.SPREADSHEET)
      return "view_week";

    //SYSTEM
    if (type == FileType.SYSTEM)
      return "settings";

    //WORD
    if (type == FileType.WORD)
      return "description";

    return "insert_drive_file";
  }

  openWith(name: string) {
    const type = ExplorerWindow.getFileType(name);

    const programs = windowTypes.filter(wType => wType.canOpen?.includes(type));
    if (programs.length != 0) {
      if (programs.length == 1) {
        this.currentItem = {name: name, isDirectory: false};
        this.openProgram(programs[0]);
      }else {
        this.openPrograms = programs;
        this.properties.dialog.toggle(this.selectProgram.nativeElement, true);
      }
    }
  }

  openProgram(program: WindowType) {
    Desktop.instance.openWindow(program.id, ["--file=" + this.currentDirectory + this.currentItem.name]);
  }

  onSave(url: string, checked: boolean = false) {
    if (
      (this.currentDirectoryInfoSave.files.includes(url.replace(this.currentDirectory, "")) ||
      this.currentDirectoryInfoSave.directories.includes(url.replace(this.currentDirectory, "")))
      && !checked && this.nameBarOptions.type == "save-file") {
      this.properties.dialog.toggle(this.override.nativeElement, true);
      return;
    }

    this.properties.windowRef.close();
    this.properties.additionalArgs[1](url);
  }

  public static getFileType(name: string): FileType {
    const includes = (extensions: string[]) => {
      let found: boolean = false;
      extensions.forEach(ext => {
        if (ext === extension)
          found = true;
      })
      return found;
    }

    const extension = (() => {
      const split = name.split(".");
      return split[split.length - 1].toLowerCase();
    })();

    //IMAGES
    if (includes(["png", "jpg", "gif", "webp", "tiff", "tif", "ps", "psd", "raw", "bmp", "heif", "indd", "jpeg", "svg", "ai", "eps", "ico"]))
      return FileType.IMAGE;

    //VIDEO
    if (includes(["3g2", "3gp", "avi", "flv", "h264", "m4v", "mkv", "mov", "mp4", "mpg", "mpeg", "rm", "swf", "vob", "wmv"]))
      return FileType.VIDEO;

    //AUDIO
    if (includes(["aif", "cda", "mid", "midi", "mp3", "mpa", "ogg", "wav", "wma", "wpl"]))
      return FileType.AUDIO;

    //COMPRESSED
    if (includes(["7z", "arj", "deb", "pkg", "rar", "rpm", "gz", "z", "zip"]))
      return FileType.COMPRESSED;

    //DISK
    if (includes(["bin", "dmg", "iso", "toast", "vcd"]))
      return FileType.DISK;

    //DATABASE
    if (includes(["db", "dbf", "mdb", "sql"]))
      return FileType.DATABASE;

    //DATA
    if (includes(["c", "cgi", "pl", "class", "cpp", "cs", "h", "java", "php", "py", "sh", "swift", "vb", "xml", "json", "js", "jsx", "ts", "tsx", "md", "txt"]))
      return FileType.DATA;

    //EMAIL
    if (includes(["email", "eml", "emlx", "msg", "oft", "ost", "pst", "vcf"]))
      return FileType.EMAIL;

    //EXECUTABLE
    if (includes(["apk", "bat", "bin", "com", "exe", "gadget", "jar", "msi", "wsf"]))
      return FileType.EXECUTABLE;

    //FONTS
    if (includes(["fnt", "fon", "otf", "ttf"]))
      return FileType.FONT;

    //INTERNET
    if (includes(["asp", "aspx", "cer", "cfm", "css", "htm", "html", "js", "jsp", "part", "rss", "xhtml"]))
      return FileType.INTERNET;

    //PRESENTATION
    if (includes(["key", "odp", "pps", "ppt", "pptx"]))
      return FileType.PRESENTATION;

    //SPREADSHEET
    if (includes(["ods", "xls", "xlsm", "xlsx"]))
      return FileType.SPREADSHEET;

    //SYSTEM
    if (includes(["bak", "cab", "cfg", "cpl", "cur", "dll", "bmp", "drv", "icns", "ini", "lnk", "msi", "sys", "tmp"]))
      return FileType.SYSTEM;

    //WORD
    if (includes(["doc", "docs", "odt", "pdf", "rtf", "tex", "wpd"]))
      return FileType.WORD;

    return FileType.FILE;
  }

  public static extractFileAndFolderFromURL(url: string): {file: string, directory: string, ext: string} {
    const split = url.split("/");
    const file = split[split.length - 1];
    const directory = url.replace(file, "");

    const fileSplit = file.split(".");

    return {file, directory, ext: fileSplit[fileSplit.length - 1]};
  }

  public static async openPopup(type: "choose-file" | "choose-folder" | "save-file", onSelected: HandlerFunction, defaultFileName: string, defaultDirectory: string = "/") {
    const window = await Desktop.instance.openWindow("explorer", ["--" + type]);

    window.addAdditionalArgs(defaultFileName, onSelected, type, defaultDirectory);
  }
}
