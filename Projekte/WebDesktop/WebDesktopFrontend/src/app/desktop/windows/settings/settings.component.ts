import {Component, ElementRef, ViewChild} from '@angular/core';
import { Window, WindowProperties } from '../window.component';
import {UserAPI} from "../../../api/userapi.service";
import {User, UserEditor} from "../../../entitys/user";
import Swal from "sweetalert2";
import {Router} from "@angular/router";
import {defaultPersonalization, Personalization} from "../../../entitys/settings";
import {SettingsService} from "../../../api/settings.service";
import {Desktop} from "../../desktop.component";
import {Notifications} from "../../components/notifications/notifications.component";

@Component({
  selector: 'window-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsWindow implements Window {
  @ViewChild('userEditor') userEditor: ElementRef;
  @ViewChild('userInfo') userInfo: ElementRef;

  constructor(public userApi: UserAPI, public router: Router, private settings: SettingsService) { }
  properties: WindowProperties;
  user: User;
  userInfoDesc: string = "Siehe deine Accountinformationen ein";
  themes: {name: string, key: string}[] = [
    {name: "Dunkel", key: "theme-dark"},
    {name: "Hell", key: "theme-light"}
  ];
  lastTheme: string;
  currentTheme: string;
  taskbar: string;
  taskbar_icons: string;
  background: string;

  async onOpen() {
    this.properties.icon = "/assets/icons/settings.png"
    this.properties.title = "Einstellungen";
    this.properties.minSize = {width: 800, height: 600};

    this.user = await this.userApi.getCurrentUser();
    this.currentTheme = document.body.classList.item(1);
    this.lastTheme = this.currentTheme;

    const personalization = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);
    this.taskbar = personalization.taskbar;
    this.background = personalization.background;
    this.taskbar_icons = personalization.taskbar_icons;
  }

  onClose(): boolean { return true; }

  //USER EDIT
  toggleUserEditor(): void {
    this.userInfo.nativeElement.classList.toggle("hidden");
    this.userEditor.nativeElement.classList.toggle("hidden");

    if (this.userInfo.nativeElement.classList.contains("hidden"))
      this.userInfoDesc = "Bearbeite nur das, was verändert werden soll!";
    else
      this.userInfoDesc = "Siehe deine Accountinformationen ein";
  }
  cancelEdit(event: MouseEvent): void {
    event.preventDefault();
    this.toggleUserEditor();
  }
  defaultEditState(): void {
    this.userInfo.nativeElement.classList.toggle("hidden", false);
    this.userEditor.nativeElement.classList.toggle("hidden", true);

    this.userInfoDesc = "Siehe deine Accountinformationen ein";
  }
  async onEdit(event: MouseEvent) {
    event.preventDefault();
    const form = this.userEditor.nativeElement.children[0] as HTMLFormElement;
    if (form.reportValidity()) {
      const firstName = form.querySelector("#firstname") as HTMLInputElement;
      const lastName = form.querySelector("#lastname") as HTMLInputElement;
      const username = form.querySelector("#username") as HTMLInputElement;
      const email = form.querySelector("#email") as HTMLInputElement;
      const password = form.querySelector("#password") as HTMLInputElement;
      const passwordRepeat = form.querySelector("#passwordRepeat") as HTMLInputElement;

      if (passwordRepeat.value != password.value) {
        await Swal.fire({
          icon: 'error',
          title: 'Bearbeitung fehlgeschlagen',
          text: 'Die Passwörter stimmen nicht überein',
          timer: 2000,
          showConfirmButton: false
        });
        return
      }

      const edit: UserEditor = {
        firstName: firstName.value,
        lastName: lastName.value,
        username: username.value,
        email: email.value,
        password: password.value
      }

      const success = await this.userApi.editOwnUser(edit);

      if (success) {
        await Swal.fire({
          icon: 'success',
          title: 'Accountdaten Bearbeitet',
          text: 'Du hast deine Accountdaten erfolgreich bearbeitet',
          timer: 2000,
          showConfirmButton: false
        });
        this.user = await this.userApi.getCurrentUser(true);
        this.cancelEdit(event);
      }else {
        await Swal.fire({
          icon: 'error',
          title: 'Bearbeitung fehlgeschlagen',
          text: 'Die Bearbeitung der Accountdaten ist fehlgeschlagen',
          timer: 2000,
          showConfirmButton: false
        });
      }
    }
  }
  async deleteUser() {
    const result = await Swal.fire({
      title: 'Möchtest du deinen Account wirklich löschen?',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonText: 'Ja',
    });

    if (result.isConfirmed) {
      await this.userApi.deleteOwnUser();
      await this.router.navigate(["/register"]);
    }
  }
  async logout() {
    await this.userApi.logout();
    await this.router.navigate(["/login"]);
  }

  //PERSONALIZATION
  setTheme() {
    document.body.classList.remove(this.lastTheme);
    document.body.classList.add(this.currentTheme);
    this.lastTheme = this.currentTheme;
    //localStorage.setItem("theme", this.currentTheme);
    const personalization = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);
    personalization.theme = this.currentTheme;
    this.settings.setSetting("settings.personalization", personalization);
  }
  setTaskbar() {
    const taskbar = document.querySelector(".taskbar") as HTMLElement;
    taskbar.classList.toggle("wide", this.taskbar == "big");
    const personalization = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);
    personalization.taskbar = this.taskbar;
    this.settings.setSetting("settings.personalization", personalization);

    document.body.style.setProperty("--taskbar-left", !taskbar.classList.contains("wide") ? "15%" : "0px");
  }
  setTaskbarIcons() {
    const icons = document.querySelector(".taskbar-icons") as HTMLElement;

    const personalization = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);
    personalization.taskbar_icons = this.taskbar_icons;
    this.settings.setSetting("settings.personalization", personalization);

    icons.style.marginLeft = "0";
    icons.style.marginRight = "0";
    if (this.taskbar_icons == "left") icons.style.marginRight = "auto";
    if (this.taskbar_icons == "right") icons.style.marginLeft = "auto";
  }
  setBackground() {
    document.body.style.setProperty("--background", "url(" + this.background + ")");
    //localStorage.setItem("background", this.background);
    const personalization = this.settings.getSetting<Personalization>("settings.personalization", defaultPersonalization);
    personalization.background = this.background;
    this.settings.setSetting("settings.personalization", personalization);
  }
}
