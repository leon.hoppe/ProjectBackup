import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { UserAPI } from '../api/userapi.service';
import { UserEditor } from '../entitys/user';
import Swal from "sweetalert2";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild('firstname') firstname: ElementRef;
  @ViewChild('lastname') lastname: ElementRef;
  @ViewChild('email') email: ElementRef;
  @ViewChild('username') username: ElementRef;
  @ViewChild('password') password: ElementRef;
  @ViewChild('passwordRepeat') passwordRepeat: ElementRef;

  @ViewChild('snackbar') snackbar: ElementRef;

  constructor(private users: UserAPI, private router: Router) { }

  ngOnInit(): void {
  }

  async register(event: MouseEvent) {
    event.preventDefault();
    const firstname = this.firstname.nativeElement as HTMLInputElement;
    const lastname = this.lastname.nativeElement as HTMLInputElement;
    const email = this.email.nativeElement as HTMLInputElement;
    const username = this.username.nativeElement as HTMLInputElement;
    const password = this.password.nativeElement as HTMLInputElement;
    const passwordRepeat = this.passwordRepeat.nativeElement as HTMLInputElement;

    if (!firstname.reportValidity() ||
        !lastname.reportValidity() ||
        !email.reportValidity() ||
        !username.reportValidity() ||
        !password.reportValidity() ||
        !passwordRepeat.reportValidity())
      return;

    if (firstname.value == "" ||
        lastname.value == "" ||
        email.value == "" ||
        username.value == "" ||
        password.value == "" ||
        passwordRepeat.value == "" ||
        password.value != passwordRepeat.value) this.showSnackbar();
    else {
      const register: UserEditor = {firstName: firstname.value, lastName: lastname.value, email: email.value, username: username.value, password: password.value};
      const success = await this.users.register(register);
      if (success) {
        await Swal.fire({
          icon: 'success',
          title: 'Erfolgreich registriert',
          text: 'Du wist gleich weitergeleitet',
          timer: 2000,
          showConfirmButton: false
        });
        await this.users.getCurrentUser(true);
        await this.router.navigate([""]);
      }
      else this.showSnackbar();
    }
  }

  async login() {
    await this.router.navigate(["/login"]);
  }

  showSnackbar() {
    this.snackbar.nativeElement.classList.add('show');
    setTimeout(() => {this.snackbar.nativeElement.classList.remove('show');}, 4000);
  }
}
