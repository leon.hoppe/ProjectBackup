import { Injectable } from '@angular/core';
import { CrudService } from '../crud.service';
import { Token } from '../entitys/token';
import { User, UserEditor, UserLogin } from '../entitys/user';

@Injectable({
  providedIn: 'root'
})
export class UserAPI {
  private user: User;

  constructor(private service: CrudService) { }

  async login(login: UserLogin): Promise<boolean> {
    try {
      const accessToken = await this.service.sendPutRequest<Token>("users/login", JSON.stringify(login), {withCredentials: true});
      this.service.setAccessToken(accessToken.id);
      return true;
    }catch(err) {
      return false;
    }
  }

  async register(editor: UserEditor): Promise<boolean> {
    try {
      const accessToken = await this.service.sendPostRequest<Token>("users/register", JSON.stringify(editor), {withCredentials: true});
      this.service.setAccessToken(accessToken.id);
      return true;
    }catch {
      return false;
    }
  }

  async logout(): Promise<void> {
    await this.service.sendDeleteRequest("users/logout", {authorized: true, withCredentials: true});
  }

  async editUser(id: string, editor: UserEditor): Promise<boolean> {
    try {
      await this.service.sendPutRequest("users/" + id, JSON.stringify(editor), {authorized: true});
      return true;
    }catch {
      return false;
    }
  }

  async editOwnUser(editor: UserEditor) {
    try {
      await this.service.sendPutRequest("users/ownuser", JSON.stringify(editor), {authorized: true});
      return true;
    }catch {
      return false;
    }
  }

  async deleteOwnUser() {
    try {
      await this.service.sendDeleteRequest("users/ownuser", {authorized: true, withCredentials: true});
      localStorage.clear();
      return true;
    }catch {
      return false;
    }
  }

  async deleteUser(id: string): Promise<boolean> {
    try {
      await this.service.sendDeleteRequest("users/" + id, {authorized: true});
      return true;
    }catch {
      return false;
    }
  }

  async getUser(data: {id?: string, username?: string, email?: string}): Promise<User | null> {
    if (data.id !== undefined) {
      return this.service.sendGetRequest("users/" + data.id);
    }
    const users = await this.getUsers();
    if (data.username !== undefined) {
      return users.find(user => user.username == data.username) as User;
    }
    if (data.email !== undefined) {
      return users.find(user => user.email == data.email) as User;
    }
    return null;
  }

  async getUsers(): Promise<User[]> {
    return this.service.sendGetRequest<User[]>("users");
  }

  async isLoggedIn(): Promise<boolean> {
    try {
      return await this.service.sendGetRequest("users/validate", {authorized: true, withCredentials: true, dontResendOnExpiration: true, returnNewTokenResponse: true}) === true;
    }catch {
      return false;
    }
  }

  async loadUser(): Promise<boolean> {
    if (!(await this.isLoggedIn())) return false;
    this.user = await this.service.sendGetRequest<User>("users/ownuser", {authorized: true});
    this.user.created = new Date(this.user.created.toString());
    return true;
  }

  async checkForPermission(permission: string): Promise<boolean> {
    await this.getCurrentUser();
    const permissions = await this.service.sendGetRequest<string[]>("users/permissions", {authorized: true});
    if (permissions.includes("*") || permissions.includes(permission))
      return true;

    const splice = permission.split(".");
    let builder: string = "";
    for (let pp of splice) {
      builder += pp + ".";
      if (permissions.includes(builder + "*"))
        return true;
    }

    return false;
  }

  async getOwnPermissions(): Promise<string[]> {
    await this.getCurrentUser();
    return this.service.sendGetRequest<string[]>("users/permissions", {authorized: true});
  }

  async getPermissions(id: string): Promise<string[]> {
    return this.service.sendGetRequest<string[]>("users/" + id + "/permissions/raw", {authorized: true});
  }

  async addPermission(id: string, permission: string): Promise<boolean> {
    try {
      await this.service.sendPostRequest("users/" + id + "/permissions/" + permission, undefined, {authorized: true});
      return true;
    }catch {
      return false;
    }
  }

  async removePermission(id: string, permission: string): Promise<boolean> {
    try {
      await this.service.sendDeleteRequest("users/" + id + "/permissions/" + permission, {authorized: true});
      return true;
    }catch {
      return false;
    }
  }

  public async getCurrentUser(forceReload: boolean = false): Promise<User> {
    if (this.user != undefined && !forceReload)
      return this.user;
    else {
      await this.loadUser();
      return this.user;
    }
  }
}
