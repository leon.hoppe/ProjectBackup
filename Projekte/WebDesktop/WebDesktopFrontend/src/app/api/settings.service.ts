import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  public static eventChange: CustomEvent = new CustomEvent<any>("settings_change");

  constructor() { }

  public setSetting(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));

    document.dispatchEvent(SettingsService.eventChange);
  }

  public getSetting<T>(key: string, defaultValue?: T): T {
    return JSON.parse(localStorage.getItem(key)) || defaultValue;
  }
}
