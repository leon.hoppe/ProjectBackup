import { Injectable } from '@angular/core';
import { CrudService } from "../crud.service";
import {DirectoryContent, DirectoryInformation, FileInformation} from "../entitys/files";
import {Observable} from "rxjs";
import {HttpEvent} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FileAPI {

  constructor(private service: CrudService) {}

  public async createDirectory(directory: string, name: string): Promise<boolean> {
    try {
      await this.service.sendPostRequest<boolean>("files/upload/directory?directory=" + directory + "&name=" + name, null, {authorized: true});
      return true;
    }catch {
      return false;
    }
  }

  public async uploadFile(file: File, directory: string): Promise<Observable<HttpEvent<object>>> {
    const form = new FormData();
    form.append("file", file, file.name);
    form.append("directory", directory);

    await this.service.getNewToken();
    return this.service.HttpClient.post(this.service.endpoint + "files/upload/file", form, {headers: this.service.httpHeader, reportProgress: true, observe: "events"});
  }

  public async quickUpload(file: File, directory: string) {
    const form = new FormData();
    form.append("file", file, file.name);
    form.append("directory", directory);

    await this.service.sendPostRequest("files/upload/file", form);
  }

  public async uploadJson(directory: string, name: string, content: any) {
    await this.service.sendPostRequest("files/upload/json?directory=" + directory + "&name=" + name, content, {authorized: true});
  }

  public async downloadJson<T>(file: string) {
    return await this.service.sendGetRequest<T>("files/download/json?file=" + file, {authorized: true});
  }

  public async downloadFile(directory: string, file: string): Promise<Observable<HttpEvent<object>>> {
    await this.service.getNewToken();
    return this.service.HttpClient.get(this.service.endpoint + "files/download/file?directory=" + directory + "&file=" + file, {headers: this.service.httpHeader, responseType: 'blob', reportProgress: true, observe: "events"});
  }

  public async delete(url: string) {
    await this.service.sendDeleteRequest("files/delete?url=" + url, {authorized: true});
  }

  public async getDirectoryContent(directory: string): Promise<DirectoryContent> {
    try {
      return await this.service.sendGetRequest<DirectoryContent>("files/content?directory=" + directory, {authorized: true});
    } catch {
      return { files: [], directories: [] };
    }
  }

  public async getDirectoryInfo(directory: string): Promise<DirectoryInformation> {
    const info = await this.service.sendGetRequest<DirectoryInformation>("files/info/directory?directory=" + directory, {authorized: true});
    info.created = new Date(info.created);
    return info;
  }

  public async getFileInfo(directory: string, file: string): Promise<FileInformation> {
    const info = await this.service.sendGetRequest<FileInformation>("files/info/files?directory=" + directory + "&file=" + file, {authorized: true});
    info.created = new Date(info.created);
    return info;
  }

  public async moveDirectory(directory: string, name: string, to: string) {
    await this.service.sendPutRequest("files/move/directory?directory=" + directory + "&name=" + name + "&to=" + to, null, {authorized: true});
  }

  public async moveFile(directory: string, file: string, to: string) {
    await this.service.sendPutRequest("files/move/file?directory=" + directory + "&file=" + file + "&to=" + to, null, {authorized: true});
  }

  public toFile(content: string, name: string): File {
    const blob = new Blob([content], {type: "text/plain"});
    return new File([blob], name, {type: "text/plain"});
  }

}
