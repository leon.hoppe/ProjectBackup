﻿using UnityEngine;

namespace Assets.Scripts {
    [CreateAssetMenu(fileName = "BiomeAttributes", menuName = "Minecraft/Biome/Attributes", order = 0)]
    public class BiomeAttributes : ScriptableObject {
        public string biomeName;

        [Header("Generator Settings")]
        public int solidGroundHeight;
        public int terrainHeight;
        public float terrainScale;
        public Lode[] lodes;
    }

    [System.Serializable]
    public class Lode {
        public string lodeName;
        public byte blockID;
        
        [Header("Generator Settings")]
        public int minHeight;
        public int maxHeight;
        public float scale;
        public float threshold;
        public float offset;
    }
}