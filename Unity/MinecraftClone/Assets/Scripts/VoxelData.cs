using UnityEngine;

public static class VoxelData {
    public static readonly int ChunkWidth = 16;
    public static readonly int ChunkHeight = 255;
    public static readonly int WorldSizeInChunks = 10;

    public static readonly int WorldSizeInBlocks = WorldSizeInChunks * ChunkWidth;

    public static readonly int ViewDistance = 5;

    public static readonly int TextureAtlasSizeInBlocks = 4;
    public static readonly float NormalizedBlockTextureSize = 1f / TextureAtlasSizeInBlocks;

    public static readonly Vector3[] VoxelVerts = {
        new Vector3(0, 0, 0),
        new Vector3(1, 0, 0),
        new Vector3(1, 1, 0),
        new Vector3(0, 1, 0),
        new Vector3(0, 0, 1),
        new Vector3(1, 0, 1),
        new Vector3(1, 1, 1),
        new Vector3(0, 1, 1)
    };

    public static readonly Vector3[] FaceChecks = {
        new Vector3(0, 0, -1),
        new Vector3(0, 0, 1),
        new Vector3(0, 1, 0),
        new Vector3(0, -1, 0),
        new Vector3(-1, 0, 0),
        new Vector3(1, 0, 0)
    };

    public static readonly int[,] VoxelTris = {
        //Back - Front - Top - Bottom - Left - Right
        
        {0, 3, 1, 2}, //Back
        {5, 6, 4, 7}, //Front
        {3, 7, 2, 6}, //Top
        {1, 5, 0, 4}, //Bottom
        {4, 7, 0, 3}, //Left
        {1, 2, 5, 6}  //Right
    };

    public static readonly Vector2[] VoxelUVs = {
        new Vector2(0, 0),
        new Vector2(0, 1),
        new Vector2(1, 0),
        new Vector2(1, 1)
    };
}