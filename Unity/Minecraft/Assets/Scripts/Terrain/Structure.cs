﻿using System.Collections.Generic;
using UnityEngine;

namespace Terrain {
    public static class Structure {

        public static Queue<BlockMod> GenerateMajorFlora(in int index, in Vector3 position, in int minTrunkHeight, in int maxTrunkHeight) {
            switch (index) {
                
                case 0:
                    return GenerateTree(position, minTrunkHeight, maxTrunkHeight);
                
                case 1:
                    return GenerateCactus(position, minTrunkHeight, maxTrunkHeight);
                
                default:
                    Debug.LogError("Invalid index for major flora");
                    return new Queue<BlockMod>();
                
            }
        }

        private static Queue<BlockMod> GenerateTree(in Vector3 position, in int minTrunkHeight, in int maxTrunkHeight) {
            Queue<BlockMod> modifications = new Queue<BlockMod>();

            int height = (int)((maxTrunkHeight - minTrunkHeight) * Noise.Get2dPerlin(new Vector2(position.x, position.z), 250, 3)) + minTrunkHeight;

            for (int i = 1; i < height; i++)
                modifications.Enqueue(new BlockMod(new Vector3(position.x, position.y + i, position.z), BlockType.Log));
        
            modifications.Enqueue(new BlockMod(new Vector3(position.x, position.y + height, position.z), BlockType.Leaves));
        
            for (int x = -3; x < 4; x++)
            for (int y = 0; y < 7; y++)
            for (int z = -3; z < 4; z++) {
                modifications.Enqueue(new BlockMod(new Vector3(position.x + x, position.y + height + y, position.z + z), BlockType.Leaves));
            }

            return modifications;
        }
        
        private static Queue<BlockMod> GenerateCactus(in Vector3 position, in int minTrunkHeight, in int maxTrunkHeight) {
            Queue<BlockMod> modifications = new Queue<BlockMod>();

            int height = (int)((maxTrunkHeight - minTrunkHeight) * Noise.Get2dPerlin(new Vector2(position.x, position.z), 23456, 2)) + minTrunkHeight;

            for (int i = 1; i < height; i++)
                modifications.Enqueue(new BlockMod(new Vector3(position.x, position.y + i, position.z), BlockType.Cactus));
            
            modifications.Enqueue(new BlockMod(new Vector3(position.x, position.y + height, position.z), BlockType.CactusTop));

            return modifications;
        }
    
    }
}