using System;
using System.IO;
using UnityEngine;

namespace Terrain {
    public static class WorldData {
        public static readonly float Gravity = -9.81f;

        public static readonly Vector2Int ChunkSize = new Vector2Int(16, 128);
        public static readonly int WorldSizeInChunks = 100;
        public static readonly int WorldSizeInBlocks = WorldSizeInChunks * ChunkSize.x;

        public static readonly float MinLightLevel = 0.1f;
        public static readonly float MaxLightLevel = 0.9f;
        public static readonly float LightFalloff = 0.08f;

        public static readonly int TextureAtlasSizeInBlocks = 16;
        public static readonly float NormalizedBlockTextureSize = 1.0f / TextureAtlasSizeInBlocks;

        public static int Seed = Int32.MaxValue;
        
        public static int WorldCenter => (WorldSizeInChunks / 2) * ChunkSize.x;

        public static readonly Vector3[] BlockVerts = {
            new Vector3(0.0f, 0.0f, 0.0f),
            new Vector3(1.0f, 0.0f, 0.0f),
            new Vector3(1.0f, 1.0f, 0.0f),
            new Vector3(0.0f, 1.0f, 0.0f),
            new Vector3(0.0f, 0.0f, 1.0f),
            new Vector3(1.0f, 0.0f, 1.0f),
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(0.0f, 1.0f, 1.0f)
        };

        public static readonly Vector3Int[] FaceChecks = {
            new Vector3Int(0, 0, -1),
            new Vector3Int(0, 0, 1),
            new Vector3Int(0, 1, 0),
            new Vector3Int(0, -1, 0),
            new Vector3Int(-1, 0, 0),
            new Vector3Int(1, 0, 0)
        };

        public static readonly int[,] BlockTris = {
            { 0, 3, 1, 2 }, // BACK FACE
            { 5, 6, 4, 7 }, // FRONT FACE
            { 3, 7, 2, 6 }, // TOP FACE
            { 1, 5, 0, 4 }, // BOTTOM FACE
            { 4, 7, 0, 3 }, // LEFT FACE
            { 1, 2, 5, 6 } // RIGHT FACE
        };

        public static Vector2Int GetChunkPositionFromWorldPosition(in Vector3 pos) {
            int x = Mathf.FloorToInt(pos.x / ChunkSize.x);
            int z = Mathf.FloorToInt(pos.z / ChunkSize.x);

            return new Vector2Int(x, z);
        }
    }

    [Serializable]
    public class BlockType {
        public const byte Air = 0;
        public const byte Bedrock = 1;
        public const byte Stone = 2;
        public const byte Grass = 3;
        public const byte Sand = 4;
        public const byte Dirt = 5;
        public const byte Log = 6;
        public const byte Planks = 7;
        public const byte Bricks = 8;
        public const byte Cobblestone = 9;
        public const byte Glass = 10;
        public const byte Leaves = 11;
        public const byte Cactus = 12;
        public const byte CactusTop = 13;

        public string blockName;
        public bool solid;
        public bool renderNeighbourFaces;
        public float transparency;
        public Sprite icon;

        [Header("Texture Values")] public short backFaceTexture;
        public short frontFaceTexture;
        public short topFaceTexture;
        public short bottomFaceTexture;
        public short leftFaceTexture;
        public short rightFaceTexture;

        public short GetTexture(in byte faceIndex) {
            switch (faceIndex) {
                case 0:
                    return backFaceTexture;

                case 1:
                    return frontFaceTexture;

                case 2:
                    return topFaceTexture;

                case 3:
                    return bottomFaceTexture;

                case 4:
                    return leftFaceTexture;

                case 5:
                    return rightFaceTexture;

                default:
                    Debug.LogError("Invalid face index");
                    return 0;
            }
        }
    }

    public struct BlockMod {

        public Vector3 Position;
        public byte Block;

        public BlockMod(in Vector3 position, in byte block) {
            Position = position;
            Block = block;
        }

    }

    public struct BlockState {
        
        public byte Block;
        public float GlobalLightPercent;

        public BlockState(in byte block) {
            Block = block;
            GlobalLightPercent = 0;
        }

    }

    [Serializable]
    public class Settings {
        public string version = "0.0.1";
        
        public int viewDistance = 9;
        public int clouds;
        public bool enableThreading = true;
        public bool enableChunkLoadingAnimation = true;
        
        public float mouseSensitivity = 3.0f;
        
        public void Save(in string path) {
            string jsonExport = JsonUtility.ToJson(this, true);
            File.WriteAllText(Application.dataPath + "/" + path, jsonExport);
        }

        public static Settings Load(in string name) {
            string path = Application.dataPath + "/" + name;

            if (File.Exists(path)) {
                string jsonImport = File.ReadAllText(path);
                return JsonUtility.FromJson<Settings>(jsonImport);
            }

            return new Settings();
        }

    }
}
