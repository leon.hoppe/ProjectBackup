﻿using System;
using UnityEngine;

namespace Terrain {
    [CreateAssetMenu(fileName = "BiomeAttributes", menuName = "Minecraft/Biome Attribute", order = 0)]
    public class BiomeAttributes : ScriptableObject {
    
        [Header("Biome Settings")]
        public string biomeName;
        public int offset;
        public float scale;
    
        public int terrainHeight;
        public float terrainScale;

        public byte surfaceBlock;
        public byte subSurfaceBlock;

        [Header("Major Flora")]
        public int majorFloraIndex = 0;
        public float majorFloraZoneScale = 1.3f;
        [Range(0.01f, 1f)] public float majorFloraZoneThreshold = 0.6f;
        public float majorFloraPlacementScale = 15.0f;
        [Range(0.01f, 1f)] public float majorFloraPlacementThreshold = 0.8f;
        public bool placeMajorFlora = true;
        public int maxHeight = 12;
        public int minHeight = 5;

        public Lode[] lodes;

    }

    [Serializable]
    public class Lode {
    
        public string name;
        public byte blockId;
        public int minHeight;
        public int maxHeight;
        public float scale;
        public float threshold;
        public float noiseOffset;

    }
}