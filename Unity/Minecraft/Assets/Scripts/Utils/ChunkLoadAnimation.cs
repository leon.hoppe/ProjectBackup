﻿using Terrain;
using UnityEngine;

namespace Utils {
    public class ChunkLoadAnimation : MonoBehaviour {
        private float speed = 3.5f;
        private Vector3 _targetPosition;
        
        private void Start() {
            _targetPosition = transform.position;
            transform.Translate(0, -WorldData.ChunkSize.y, 0);
        }

        private void Update() {
            transform.position = Vector3.Lerp(transform.position, _targetPosition, Time.deltaTime * speed);
            if (_targetPosition.y - transform.position.y < 0.05f) {
                transform.position = _targetPosition;
                Destroy(this);
            }
        }
    }
}