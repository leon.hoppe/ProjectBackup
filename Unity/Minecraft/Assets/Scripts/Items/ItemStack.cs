﻿namespace Items {
    public class ItemStack {

        public byte Block;
        public int Amount;

        public ItemStack(in byte block, in int amount) {
            Block = block;
            Amount = amount;
        }

    }
}