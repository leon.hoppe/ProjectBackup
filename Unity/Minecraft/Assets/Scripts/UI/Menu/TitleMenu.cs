﻿using Terrain;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Menu {
    public class TitleMenu : MonoBehaviour {

        private Settings _settings;

        public GameObject mainMenu;
        public GameObject settingsMenu;

        [Header("Main Menu UI Elements")]
        public TextMeshProUGUI seedField;
        
        [Header("Settings Menu UI Elements")]
        public Slider viewDistanceSlider;
        public TextMeshProUGUI viewDistanceText;
        public Slider mouseSensitivitySlider;
        public TextMeshProUGUI mouseSensitivityText;
        public Toggle threadingToggle;
        public Toggle chunkAnimateToggle;
        public TMP_Dropdown clouds;

        private void Awake() {
            _settings = Settings.Load("settings.json");
        }

        public void StartGame() {
            WorldData.Seed = Mathf.Abs(seedField.text.GetHashCode()) / WorldData.WorldSizeInChunks;
            
            SceneManager.LoadScene("Game");
        }

        public void QuitGame() {
            Application.Quit();
        }

        public void EnterSettings() {
            UpdateUI();
            threadingToggle.isOn = _settings.enableThreading;
            chunkAnimateToggle.isOn = _settings.enableChunkLoadingAnimation;
            viewDistanceSlider.value = _settings.viewDistance;
            mouseSensitivitySlider.value = _settings.mouseSensitivity;
            clouds.value = _settings.clouds;
            
            mainMenu.SetActive(false);
            settingsMenu.SetActive(true);
        }
        
        public void ExitSettings() {
            _settings.viewDistance = (int)viewDistanceSlider.value;
            _settings.mouseSensitivity = mouseSensitivitySlider.value;
            _settings.enableThreading = threadingToggle.isOn;
            _settings.enableChunkLoadingAnimation = chunkAnimateToggle.isOn;
            _settings.clouds = clouds.value;
            World.Settings = _settings;
            _settings.Save("settings.json");

            mainMenu.SetActive(true);
            settingsMenu.SetActive(false);
        }

        public void UpdateUI() {
            viewDistanceText.text = "View Distance: " + viewDistanceSlider.value;
            mouseSensitivityText.text = "Mouse Sensitivity: " + mouseSensitivitySlider.value.ToString("F1");
        }
        
    }
}