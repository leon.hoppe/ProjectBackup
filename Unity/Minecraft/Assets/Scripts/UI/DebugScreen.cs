﻿using Terrain;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class DebugScreen : MonoBehaviour {
        public World world;
        public Text text;

        private float _frameRate;
        private float _timer;

        private void Update() {
            string debugText = "Minecraft Clone (Tutorial from b3agz)\n";
            debugText += $"FPS: {_frameRate}\n\n";
            debugText += $"XYZ: {Mathf.FloorToInt(world.player.transform.position.x)} : {Mathf.FloorToInt(world.player.transform.position.y)} : {Mathf.FloorToInt(world.player.transform.position.z)}\n";
            debugText += $"Chunk: {world.lastPlayerChunk.x} : {world.lastPlayerChunk.y}\n";
            debugText += $"Biome: {GetPlayerBiome()}";

            text.text = debugText;

            if (_timer > 1.0f) {
                _frameRate = (int)(1.0f / Time.unscaledDeltaTime);
                _timer = 0;
            }

            _timer += Time.deltaTime;
        }

        private string GetPlayerBiome() {
            int index = world.BlockBiomes[Mathf.FloorToInt(world.player.position.x), Mathf.FloorToInt(world.player.position.z)];
            return world.biomes[index].biomeName;
        }
    }
}