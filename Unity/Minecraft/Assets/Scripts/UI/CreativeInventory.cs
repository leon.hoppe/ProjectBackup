﻿using System.Collections.Generic;
using Items;
using Terrain;
using UnityEngine;

namespace UI {
    public class CreativeInventory : MonoBehaviour {

        public GameObject slotPrefab;
        public World world;

        private readonly List<ItemSlot> _slots = new List<ItemSlot>();

        private void Start() {
            for (int i = 1; i < world.blockTypes.Length; i++) {
                GameObject obj = Instantiate(slotPrefab, transform);
                
                ItemStack stack = new ItemStack((byte) i, 64);
                ItemSlot slot = new ItemSlot(obj.GetComponent<UIItemSlot>(), stack);
                slot.creative = true;
                _slots.Add(slot);
            }
        }
    }
}