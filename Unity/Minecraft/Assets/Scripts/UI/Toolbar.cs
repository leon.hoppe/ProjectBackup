﻿using Items;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UI {
    public class Toolbar : MonoBehaviour {

        public UIItemSlot[] slots;
        public RectTransform highlight;
        public int slotIndex;

        private void Start() {
            for (var i = 0; i < slots.Length; i++) {
                ItemStack stack = new ItemStack((byte)(i + 1), Random.Range(2, 50));
                new ItemSlot(slots[i], stack);
            }
        }

        private void Update() {
            float scroll = Input.GetAxis("Mouse ScrollWheel");

            if (scroll != 0) {
                if (scroll > 0)
                    slotIndex--;
                else
                    slotIndex++;

                if (slotIndex >= slots.Length)
                    slotIndex = 0;
                if (slotIndex < 0)
                    slotIndex = slots.Length - 1;

                highlight.position = slots[slotIndex].slotIcon.transform.position;
            }
        }
    }
}