﻿using Terrain;
using UnityEngine;

public static class Noise {

    public static float Get2dPerlin(in Vector2 position, in float offset, in float scale) {
        return Mathf.PerlinNoise(
            (position.x + 0.1f) / WorldData.ChunkSize.x * scale + offset,
            (position.y + 0.1f) / WorldData.ChunkSize.x * scale + offset
        );
    }

    public static bool Get3dPerlin(in Vector3 position, in float offset, in float scale, in float threshold) {
        float x = (position.x + offset + 0.1f) * scale;
        float y = (position.y + offset + 0.1f) * scale;
        float z = (position.z + offset + 0.1f) * scale;

        float AB = Mathf.PerlinNoise(x, y);
        float BC = Mathf.PerlinNoise(y, z);
        float AC = Mathf.PerlinNoise(x, z);
        float BA = Mathf.PerlinNoise(y, x);
        float CB = Mathf.PerlinNoise(z, y);
        float CA = Mathf.PerlinNoise(z, x);

        return (AB + BC + AC + BA + CB + CA) / 6f > threshold;
    }
    
}