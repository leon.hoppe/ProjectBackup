﻿using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

namespace Terrain {
    public class Bush : Obstical {
        private readonly int bushId;
        
        public Bush(Vector2 position, Chunk chunk, int bushId) : base(position, chunk) {
            GameObject.name = $"Bush [{position.x}:{position.y}]";
            this.bushId = bushId;
            CreateMeshData();
        }
        
        public override void CreateMeshData() {
            Mesh mesh = new Mesh();
            List<Vector3> vertices = new List<Vector3>();
            int[] triangles;
            List<Vector2> uvs = new List<Vector2>();
            
            vertices.Add(new Vector2(0, 0));
            vertices.Add(new Vector2(0, 1));
            vertices.Add(new Vector2(1, 0));
            vertices.Add(new Vector2(1, 1));

            RectangleF texture = World.GetBushTextureManager().GetTile(bushId);
            uvs.Add(new Vector2(texture.X, texture.Y));
            uvs.Add(new Vector2(texture.X, texture.Y + texture.Height));
            uvs.Add(new Vector2(texture.X + texture.Width, texture.Y));
            uvs.Add(new Vector2(texture.X + texture.Width, texture.Y + texture.Height));

            triangles = new[] { 0, 1, 2, 2, 1, 3 };

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles;
            mesh.uv = uvs.ToArray();
            mesh.RecalculateNormals();

            MeshFilter filter = GameObject.AddComponent<MeshFilter>();
            MeshRenderer renderer = GameObject.AddComponent<MeshRenderer>();
            renderer.material = World.GetBushTextureManager().Material;
            renderer.sortingLayerName = "Vegetation";
            renderer.sortingOrder = Mathf.FloorToInt(World.ChunkSize - Position.y - Chunk.Position.y);
            filter.mesh = mesh;
        }
    }
}