﻿using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

namespace Terrain {
    public class Sapling : Obstical {
        public Sapling(Vector2 position, Chunk chunk) : base(position, chunk) {
            GameObject.transform.position = position + new Vector2(-0.5f, 0.0f);
            GameObject.name = $"Tree [{position.x}:{position.y}]";
            CreateMeshData();
        }

        public override void CreateMeshData() {
            Mesh mesh = new Mesh();
            List<Vector3> vertices = new List<Vector3>();
            int[] triangles;
            List<Vector2> uvs = new List<Vector2>();
            
            vertices.Add(new Vector2(0, 0));
            vertices.Add(new Vector2(0, 2));
            vertices.Add(new Vector2(2, 0));
            vertices.Add(new Vector2(2, 2));

            RectangleF texture = World.GetTreeTextureManager().GetTile(0, 1);
            uvs.Add(new Vector2(texture.X, texture.Y));
            uvs.Add(new Vector2(texture.X, texture.Y + texture.Height));
            uvs.Add(new Vector2(texture.X + texture.Width, texture.Y));
            uvs.Add(new Vector2(texture.X + texture.Width, texture.Y + texture.Height));

            triangles = new[] { 0, 1, 2, 2, 1, 3 };

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles;
            mesh.uv = uvs.ToArray();
            mesh.RecalculateNormals();

            MeshFilter filter = GameObject.AddComponent<MeshFilter>();
            MeshRenderer renderer = GameObject.AddComponent<MeshRenderer>();
            renderer.material = World.GetTreeTextureManager().Material;
            renderer.sortingLayerName = "Vegetation";
            renderer.sortingOrder = Mathf.FloorToInt(World.ChunkSize - Position.y - Chunk.Position.y);
            filter.mesh = mesh;
        }
    }
}