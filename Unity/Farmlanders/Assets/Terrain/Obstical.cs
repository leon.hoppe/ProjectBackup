﻿using UnityEngine;

namespace Terrain {
    public abstract class Obstical {
        public readonly Vector2 Position;
        public readonly Chunk Chunk;
        public readonly GameObject GameObject;
        protected readonly World World;
        
        
        protected Obstical(Vector2 position, Chunk chunk) {
            Position = position;
            Chunk = chunk;
            World = chunk.World;
            GameObject = new GameObject();
            GameObject.SetActive(false);
            GameObject.transform.SetParent(Chunk.GetObject().transform);
            GameObject.transform.position = position;
            GameObject.name = $"Object [{position.x}:{position.y}]";
        }

        public abstract void CreateMeshData();
        
        public void SetActive(bool value) { GameObject.SetActive(value); }

        public override string ToString() => GameObject.name;
    }
}