﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using General;
using UnityEngine;

namespace Terrain {
    public class Block {
        public static readonly Dictionary<string, Block> Blocks = new Dictionary<string, Block>();
        public static Block GetBlock(int id) { return Blocks[Blocks.Keys.ToList()[id]]; }

        // BLOCKS
        public static readonly Block Air = new Block("air", false, false, new[] { new Vector2Int(0, 0) });
        public static readonly Block Grass = new Block("grass", true, false, new[] { new Vector2Int(0, 1) });
        public static readonly Block Forest = new Block("dark_grass", true, false, new[] { new Vector2Int(1, 1) });
        public static readonly Block GrassForestBorder = new Block("border_grass_forest", true, false, new[] { new Vector2Int(5, 0), new Vector2Int(5, 1), new Vector2Int(5, 2), new Vector2Int(5, 3), new Vector2Int(6, 0), new Vector2Int(6, 1), new Vector2Int(6, 2), new Vector2Int(6, 3) });
        public static readonly Block Dirt = new Block("dirt", true, false, new[] { new Vector2Int(0, 2) });
        public static readonly Block Sand = new Block("sand", true, false, new[] { new Vector2Int(0, 3) });
        public static readonly Block WetSand = new Block("wet_sand", false, false, new[] { new Vector2Int(1, 3) });
        public static readonly Block SandWaterBorder = new Block("border_sand_water", false, true, new[] { new Vector2Int(3, 0), new Vector2Int(3, 1), new Vector2Int(3, 2), new Vector2Int(3, 3) });
        public static readonly Block Sandstone = new Block("sandstone", true, false, new[] { new Vector2Int(0, 4) });
        public static readonly Block Water = new Block("water", false, true, new[] { new Vector2Int(2, 0), new Vector2Int(2, 1), new Vector2Int(2, 2), new Vector2Int(2, 3) });
        
        // BLOCK PROPERTIES
        public readonly string Name;
        public readonly Vector2Int[] Tiles;
        public readonly bool Solid;
        public readonly int Id;
        public readonly bool Animatable;

        public Block(string name, bool solid, bool animatable, Vector2Int[] tiles) {
            Name = name;
            Solid = solid;
            Animatable = animatable;
            Tiles = tiles;
            Id = Blocks.Count;
            Blocks.Add(Name, this);
        }

        public RectangleF GetTile(TextureManager manager, int id = 0) {
            if (id >= Tiles.Length) id = 0;

            Vector2Int tile = Tiles[id];
            return manager.GetTile(tile.x, tile.y);
        }

        public override string ToString() => Name;
    }
}