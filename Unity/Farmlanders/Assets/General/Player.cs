using System;
using Buildings;
using UnityEngine;
using Terrain;

namespace General {
    public class Player : MonoBehaviour {
        public World world;
        public Camera view;
        public float speed = 2.0f;
        public float scaleSpeed = 200.0f;
        public BuildingGroup buildingGroup;

        private Vector3 _lastMousePos;
        private Plane[] _vievPlanes;
        private SpriteRenderer _objectInHand;

        private void Start() {
            SetItemInHand(buildingGroup.Buildings[0]);
            _objectInHand.gameObject.SetActive(false);
        }

        private void Update() {
            _vievPlanes = GeometryUtility.CalculateFrustumPlanes(view);

            if (!world.ready) return;
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            float scroll = Input.mouseScrollDelta.y * -1f;
            float totalScale = view.orthographicSize + scroll * scaleSpeed * Time.deltaTime * 3f;

            Vector3 translation = new Vector3(
                horizontal * speed * Time.deltaTime * totalScale,
                vertical * speed * Time.deltaTime * totalScale
            );

            transform.Translate(translation);

            view.orthographicSize = Mathf.Clamp(totalScale, 3, 40);

            Vector2 mouse = view.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButton(0) && _objectInHand == null) {
                float dragSpeed = 7.8f * totalScale;
                var newPosition = new Vector3();
                newPosition.x = Input.GetAxis("Mouse X") * dragSpeed * Time.deltaTime;
                newPosition.y = Input.GetAxis("Mouse Y") * dragSpeed * Time.deltaTime;
                transform.Translate(-newPosition);
            }

            if (_objectInHand != null) {
                _objectInHand.transform.position = mouse;
                
                //CHECH OBSTICALE INTERSECTION
                bool colides = false;
                Vector2 size = _objectInHand.bounds.size;
                for (int x = Mathf.FloorToInt(mouse.x - size.x / 2f); x < Mathf.FloorToInt(mouse.x + size.x / 2f); x++) {
                    if (colides) break;
                    for (int y = Mathf.FloorToInt(mouse.y - size.y / 2f); y < Mathf.FloorToInt(mouse.y + size.y / 2f); y++) {
                        Vector2 pos = new Vector2(x, y);
                        Chunk chunk = world.GetChunk(pos);
                        if (chunk == null) continue;
                        if (!chunk.Visible) continue;
                        Block block = chunk.GetBlock(pos);
                        if (chunk.GetObstical(pos) != null) {
                            colides = true;
                            break;
                        }
                        if (block != null)
                            if (!block.Solid) {
                                colides = true;
                                break;
                            }
                    }
                }
                _objectInHand.color = !colides ? new Color(1.0f, 1.0f, 1.0f, 0.3f) : new Color(1.0f, 0.0f, 0.0f, 0.3f);

                if (Input.GetMouseButtonUp(0) && !colides) {
                    _objectInHand.color = Color.white;
                    _objectInHand.material.shader = Shader.Find("Sprites/Default");
                    world.AddBuilding(_objectInHand.gameObject);
                    _objectInHand = null;
                }
            }
        }

        public bool CanSee(Bounds obj) {
            return GeometryUtility.TestPlanesAABB(_vievPlanes, obj);
        }

        public void OnWorldReady() {
            _objectInHand.gameObject.SetActive(true);
        }

        public void SetItemInHand(GameObject prefab) {
            _objectInHand = Instantiate(prefab).GetComponent<SpriteRenderer>();
            _objectInHand.transform.SetParent(world.transform);
            _objectInHand.material.shader = world.noOutline;
        }
    }
}
