﻿using System.Drawing;
using Terrain;
using UnityEngine;

namespace General {
    public class TextureManager {

        public readonly Material Material;
        private readonly Vector2Int _tileDimensions;
        private readonly Vector2 _tileUVs;
        private readonly Vector2Int _tileCount;

        public TextureManager(Material material, Vector2Int tileDimensions, World world) {
            material.shader = world.noOutline;
            Material = material;
            _tileDimensions = tileDimensions;

            _tileCount = new Vector2Int(
                material.mainTexture.width / tileDimensions.x,
                material.mainTexture.height / tileDimensions.y
            );
            _tileUVs = new Vector2(1f / _tileCount.x, 1f / _tileCount.y);
        }

        public RectangleF GetTile(int row, int col) {
            row = _tileCount.y - row - 1;
            return new RectangleF(col * _tileUVs.x + 0.0001f, row * _tileUVs.y + 0.0001f, _tileUVs.x - 0.0001f, _tileUVs.y - 0.0001f);
        }
        public RectangleF GetTile(int id) {
            int col = id % _tileCount.y;
            int row = id / _tileCount.x;
            return GetTile(row, col);
        }

    }
}