﻿using UnityEngine;

namespace Buildings {
    [CreateAssetMenu(fileName = "BuildingGroup", menuName = "Buildings", order = 0)]
    public class BuildingGroup : ScriptableObject {
        public GameObject[] Buildings;
    }
}