﻿using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    public GameObject agent;
    public GameObject target;
    public int population;
    public bool training;
    
    private int _generation;
    private readonly int[] _layers = { 1, 10, 10, 1 };
    private List<NeuralNetwork> _networks;
    private List<Boomerang> _agents;

    private void Timer() => training = false;

    private void Update() {
        if (!training) return;
        
        if (_generation == 0) InitNetworks();
        else {
            _networks.Sort();
            for (int i = 0; i < population / 2; i++) {
                _networks[i] = new NeuralNetwork(_networks[i + population / 2]);
                _networks[i].Mutate();

                _networks[i + population / 2] = new NeuralNetwork(_networks[i + population / 2]);
            }

            _networks.ForEach(network => network.Fitness = 0.0f);
        }

        _generation++;

        training = true;
        Invoke(nameof(Timer), 15.0f);
        CreateAgents();
    }

    private void CreateAgents() {
        if (_agents != null)
            _agents.ForEach(agent => Destroy(agent.gameObject));

        _agents = new List<Boomerang>();

        for (int i = 0; i < population; i++) {
            var fieldAgent = Instantiate(agent, new Vector3(Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f), 0), agent.transform.rotation).GetComponent<Boomerang>();
            fieldAgent.Initialize(_networks[i], target.transform);
            _agents.Add(fieldAgent);
        }
    }

    private void InitNetworks() {
        if (population % 2 != 0) population++;

        _networks = new List<NeuralNetwork>();

        for (int i = 0; i < population; i++) {
            var network = new NeuralNetwork(_layers);
            network.Mutate();
            _networks.Add(network);
        }
    }
}