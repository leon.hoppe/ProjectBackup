﻿using UnityEditor;
using UnityEngine;

namespace LandmassGeneration {
    [CustomEditor(typeof(MapGenerator))]
    public class MapGeneratorEditor : Editor {
        public override void OnInspectorGUI() {
            var mapGen = target as MapGenerator;

            if (DrawDefaultInspector() && mapGen.autoUpdate)
                mapGen!.GenerateMap();
            
            if (GUILayout.Button("Generate"))
                mapGen!.GenerateMap();
        }
    }
}